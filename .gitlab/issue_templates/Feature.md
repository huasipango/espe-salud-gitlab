**Yo como** miembro de [the development team](http://scrumguides.org/scrum-guide.html#team-dev),
**Quiero** contexto claro y metas **para poder** trabar eficientemente.

Descripción......


### Criterios de aceptación

- [ ] 1
....

### Notas adicionales:

1. Nota 1


/label ~feature
/assign @
/estimate 8h


### Recursos:

* [Style-guides and template for a user story](agile-user-story.md)
* [“Advantages of the “As a user, I want” user story template.”](http://www.mountaingoatsoftware.com/blog/advantages-of-the-as-a-user-i-want-user-story-template)
* [Scrum guide](http://scrumguides.org/scrum-guide.html)