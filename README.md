<img  height="60px"  width="60px"  style="float: left;"  src="src/assets/img/demo/logo_espe.png"  alt="Vex Logo">

# Salud Espe  Front-end
El proyecto salud espe front-end es un proyecto realizado en angular, tomando de base la plantilla Vex, procurando mantener un diseño de marca que se acople a las necesidades de la institución, ofreciendo una solución elegante, completa e intuituva.
  
 
## Tabla de contenido

-  [Instalación](#Instalación)
	- [Requisitos Software](##Requisitos)
	- [Instalación](##Instalacion)
  - [Ejecutar](##Ejecutar)
-  [Como Funciona](#Funcionamiento)
    - [Estructura](##Estructura)
    -  [Personalización](#Personalización)
    - [Crear Componentes](##Componentes)
    - [Realizar Pruebas](##Pruebas) 	
-  [Autores](#Autores)
-  [Licencia](#Licencia)


  

# Instalación
## Pre requisitos
Tener conocimientos de algunos comandos en terminal.

Tener instalados visual code en tu sistema operativo favorito
https://code.visualstudio.com/Download

Conocer almenos lo basico de HTML y Javascript

## Requisitos 

[NodeJs](https://nodejs.org/en/)
> Antes de nada primero debemos asegurarnos de tener instalado NodeJs y npm, esto lo podemos comprobar en la terminal mediante los comandos:
```sh
$ node -v
$ npm -v
```

En caso de no encontrarse [NodeJs](https://nodejs.org/en/) v10 o superior aqui hay un manual detallado de [como instalar NodeJS.](//docs.npmjs.com/getting-started/installing-node)


[Angular Cli](https://www.npmjs.com/package/@angular/cli)
>A continuación Instalar Angular-CLI, es tan simple como ejecutar

```sh
$ sudo npm install -g @angular/cli@latest
```

## Instalación

Asumiendo que ya tienen el repositorio clonado en su máquina, vamos a proceder a instalar.

Para ejecutar el proyecto primero es necesario cargar las dependencias o librerias, para lo cual, abrimos la carpeta salud-espe-front-end en el terminal y ejecutamos 

```sh
npm install
```
Es posible que las versiones de las dependencias generen errores, para forzar la correcta instalacion de las dependencias se puede utilizar los siguientes comandos

```sh
$ sudo rm -rf node_modules
$ npm cache clean --force
$ npm install @angular/cli@8.3.20
$ npm i @angular-devkit/build-angular@0.803.12
$ npm install --force
```

Otra alternativa es utilizar `yarn` como gestor de dependencias en lugar de `npm` , la instalacion atraves de yarn se realiza de la siguiente manera

```sh
$ sudo npm install yarn -g
$ sudo rm -rf node_modules
$ npm cache clean --force
$ yarn install
```
## Ejecutar

Para correr el proyecto debe ubicarse en la carpeta salud espe front end desde una terminal y ejecutar el comando 

```sh
ng serve --open
```

Comenzara a ejecutarse el servidor en su maquina personal, en ambiente de desarrollo. Para visualizar su funcionamiento navegar a `http://localhost:4200/`. Cada cambio que se realice en el proyecto se reflejara automaticamente en esta página.

>Si desea subir el proyecto a un ambiente de producción simplemente ejecute `npm run build` o `ng build --prod` y se generaran los archivos HTML y JS en la carpeta `/dist` listos para ser subidos a cualquier servidor.



# Funcionamiento
## Estructura
El archivo package.json contiene todas las librerías instaladas en el proyecto, el mismo que indica que en esta carpeta existe un proyecto. 
A continuación veremos rapidamente los archivos principales del proyecto

| Nombre                  | Descripcion |
| -------------:        |---------------
| `package.json`        | Contiene la lista de dependencias, permite cargar el proyecto. |
| `angular.json`        | Configuraciones especificas del proyecto. Aqui se añaden stilos scripts, environment y más. | 
| `node_modules`        | Se crea al correr `npm install` contiene las librerias descargadas     |
| `src`                 | Contiene toda la funcionalidad del proyecto, vistas y controladores. |

## Personalización
La personalizacion de la plantilla la podemos encontrar a mayor detalle en la [documentación de VEX](https://vex.visurel.com/documentation/installation) o en el README de la rama `demo` de este proyecto.

Sin embargo, veremos brevemente algunos aspectos.

`Para cambiar los colores`
> La mayoria de estilos de fuentes y colores se manejan de manera global, por lo que para modificar colores y estilos muchas veces basta con modificar `style.scss` o `variables.scss` 

`Para cambiar items del menu lateral`

>El menu lateral se genera de manera automatica basandose en un array de `items` dentro de AppComponent:

## Componentes
Ahora que hemos instalado todos los requisitos previos, es hora de comenzar a desarrollar nuestra aplicación. 

>`Angular-Cli` permite crear nuevos componentes de manera sencilla atraves de comandos de la terminal.[Puedes ver la lista de comandos aquí.](//github.com/angular/angular-cli/blob/master/packages/angular/cli/README.md#generating-components-directives-pipes-and-services)

Para generar nuestro primer component, simplemente abrimos un terminal y navegamos en nuestra aplicación angular y ejecutamos 
```
ng g component {carpeta}/{NombreDeTuComponente}
```
 los cambios se verán reflejados dentro de la carpeta `/src/app/` 

Cada carpeta de un componente cuenta con la siguiente estructura:
* `client.component.ts`
* `client.component.html`
* `client.component.css`
* `client.component.spec.ts`

>Los archivos `client.component.ts` y `client.component.spec.ts` contienen la mayor cantidad de código, los otros archivos solo contienen marcadores de posición.

## Pruebas
`Pruebas Unitarias`
>Corre `ng test` para ejecutar las pruebas unitarias a través de [Karma](https://karma-runner.github.io).

`Pruebas de extremo a extremo`
>Ejecutar `ng e2e` para ejecutar las pruebas de extremo a extremo a través de [Protractor](http://www.protractortest.org/).

# Colaboración
Colaborar en Salud Espe es muy sencillo, Actualmente el repositorio es privado, por lo que si estas leyendo esto significa que tienes acceso al mismo, para colaborar sigue estos 3 sencillos pasos:

* Clona el repositorio
* Crea una rama con el nombre feature_turama
* Realiza un push de tus cambios
* Realiza un pull request hacia la rama develop

>Se recomienda registrar los cambios en los `issues` del proyecto, y coordinar con el supervisor del proyecto, los avances se pueden visualizar en el tablero del proyecto `salud espe`

# Autores

Este proyecto no fuese posible, sin la ayuda y participacion de este excelente equipo de trabajo.
* [@diegoismael81](https://github.com/diegoismael81)
* [@shoniisrael](https://github.com/shoniisrael)
* [@israteneda](https://github.com/israteneda)
* [@lapillaga](https://github.com/lapillaga)
* [@Jrfranco2](https://github.com/Jrfranco2)
* [@damaigualca](https://github.com/damaigualca)
* [@Shelviton](https://github.com/Shelviton)
* [@Gabonauta](https://github.com/Gabonauta)





# Licencia
