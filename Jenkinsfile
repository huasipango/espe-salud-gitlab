pipeline {
  environment {
    imageName = '/tesis/espe-salud/espe-salud-ng/salud-espe-front-end'
    gitlabCredential = 'gitlab-deployment-user-access-token'
    argoCDFolderApp = 'salud/frontend'
    dockerImage = ''
  }

  agent {
    kubernetes {
      yaml '''
        apiVersion: v1
        kind: Pod
        spec:
          containers:
          - name: dind
            image: docker:19.03.1-dind
            securityContext:
              privileged: true
            env:
              - name: DOCKER_TLS_CERTDIR
                value: ""
          - name: kustomize
            image: k8s.gcr.io/kustomize/kustomize:v4.1.3
            command:
            - cat
            tty: true
          - name: node
            image: node:latest
            command:
            - cat
            resources:
              requests:
                memory: "2Gi"
                cpu: "2"
              limits:
                memory: "3Gi"
                cpu: "3"
            tty: true
            volumeMounts:
            - name: pvol-node
              mountPath: /usr/local/share/.cache
          volumes:
          - name: pvol-node
            persistentVolumeClaim:
              claimName: "jenkins-node-modules"
        '''
    }
  }
  stages {
    stage('Install dependencies') {
      when {
        expression { BRANCH_NAME ==~ /(master|develop)/ }
      }
      steps {
        container('node') {
          sh '''
            node --version
            yarn --version
            yarn install --frozen-lockfile
          '''
        }
      }
    }
    stage('Build project prod environment') {
      when {
        branch 'master'
      }
      steps {
        container('node') {
          sh '''
            npm run build -- --outputPath=./dist/out --configuration production
          '''
        }
      }
    }
    stage('Build project test environment') {
      when {
        branch 'develop'
      }
      steps {
        container('node') {
          sh '''
            npm run build -- --outputPath=./dist/out --configuration test
          '''
        }
      }
    }
    stage('Build docker image') {
      when {
        expression { BRANCH_NAME ==~ /(master|develop)/ }
      }
      steps {
        container('dind') {
          echo 'Building docker images..'
          script {
            dockerImage = docker.build env.REGISTRY + imageName + ":${env.GIT_COMMIT}"
          }
        }
      }
    }
    stage('Publish container') {
      when {
        expression { BRANCH_NAME ==~ /(master|develop)/ }
      }
      steps {
        container('dind') {
          echo 'Deploying docker image to registry..'
          script {
            docker.withRegistry('https://' + env.REGISTRY, gitlabCredential) {
              dockerImage.push()
            }
          }
        }
      }
    }
    stage('Deploy to test') {
      when {
        branch 'develop'
      }
      steps {
        container('kustomize') {
          echo 'Deploying to test environment..'
          checkout([$class: 'GitSCM',
            branches: [[name: '*/master' ]],
            extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'argocd-apps'], [$class: 'ScmName', name: 'argocd-apps']],
            userRemoteConfigs: [[
              url: 'https://gitlab.espe.edu.ec/devops/argocd-apps.git',
              credentialsId: gitlabCredential
            ]]
          ])
          dir('./argocd-apps/' + argoCDFolderApp + '/develop') {
            sh('kustomize edit set image ' + env.REGISTRY + imageName + ':' + env.GIT_COMMIT)
            sh('git config --global user.email jenkinsci@ci.com')
            sh('git config --global user.name Jenkins Pipeline')
            sh "git commit -am 'Publish new version'"
            withCredentials([usernamePassword(credentialsId: gitlabCredential, passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
              sh "git push https://${GIT_USERNAME}:${GIT_PASSWORD}@${env.ARGO_APPS_URL} HEAD:master || echo 'no changes'"
            }
          }
        }
      }
    }
    stage('Deploy to prod') {
      when {
        branch 'master'
      }
      steps {
        container('kustomize') {
          echo 'Deploying to prod environment..'
          checkout([$class: 'GitSCM',
            branches: [[name: '*/master' ]],
            extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'argocd-apps'], [$class: 'ScmName', name: 'argocd-apps']],
            userRemoteConfigs: [[
              url: 'https://gitlab.espe.edu.ec/devops/argocd-apps.git',
              credentialsId: gitlabCredential
            ]]
          ])
          dir('./argocd-apps/' + argoCDFolderApp + '/master') {
            sh('kustomize edit set image ' + env.REGISTRY + imageName + ':' + env.GIT_COMMIT)
            sh('git config --global user.email jenkinsci@ci.com')
            sh('git config --global user.name Jenkins Pipeline')
            sh "git commit -am 'Publish new version'"
            withCredentials([usernamePassword(credentialsId: gitlabCredential, passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
              sh "git push https://${GIT_USERNAME}:${GIT_PASSWORD}@${env.ARGO_APPS_URL} HEAD:master || echo 'no changes'"
            }
          }
        }
      }
    }
  }
}
