import { Icon } from '@visurel/iconify-angular';
import {RolEnum} from 'src/app/core/enums/rol.enum';

export type NavigationItem = NavigationLink | NavigationDropdown | NavigationSubheading;

export interface NavigationLink {
  type: 'link';
  route: string | any;
  fragment?: string;
  label: string;
  icon?: Icon;
  routerLinkActiveOptions?: { exact: boolean };
  badge?: {
    value: string;
    bgClass: string;
    textClass: string;
  };
  showByDefault?: boolean;
  roles?: Array<RolEnum>;
}

export interface NavigationDropdown {
  type: 'dropdown';
  label: string;
  icon?: Icon;
  children: Array<NavigationLink | NavigationDropdown>;
  badge?: {
    value: string;
    bgClass: string;
    textClass: string;
  };
  showByDefault?: boolean;
  roles?: Array<RolEnum>;
}

export interface NavigationSubheading {
  type: 'subheading';
  label: string;
  children: Array<NavigationLink | NavigationDropdown>;
  showByDefault?: boolean;
  roles?: Array<RolEnum>;
}
