export interface TableColumn<T> {
  label: string;
  property: keyof T | string;
  type: 'text' | 'image' | 'badge' | 'progress' | 'checkbox' | 'button' | 'object' | 'datetime' | 'date';
  visible?: boolean;
  cssClasses?: string[];
}
