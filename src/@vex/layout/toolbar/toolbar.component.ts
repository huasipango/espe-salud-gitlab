import {Component, ElementRef, HostBinding, Input, OnInit, ViewChild} from '@angular/core';
import { LayoutService } from '../../services/layout.service';
import icMenu from '@iconify/icons-ic/twotone-menu';
import { ConfigService } from '../../services/config.service';
import { map } from 'rxjs/operators';
import { NavigationService } from '../../services/navigation.service';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';
import { PopoverService } from '../../components/popover/popover.service';
import { MegaMenuComponent } from '../../components/mega-menu/mega-menu.component';
import icSearch from '@iconify/icons-ic/twotone-search';
import icInfo from '@iconify/icons-fa-solid/info-circle';
import icMedKit from '@iconify/icons-fa-solid/file-medical';
import icAdd from '@iconify/icons-ic/queue';
import icCollege from '@iconify/icons-fa-solid/user-graduate';
import icUsers from '@iconify/icons-fa-solid/users';
import icLink from '@iconify/icons-fa-solid/external-link-alt';
import icPaciente from '@iconify/icons-fa-solid/user-injured';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {Router} from '@angular/router';
import {PacienteSelectMenuComponent} from 'src/app/core/components/paciente-select-menu/paciente-select-menu.component';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ImagenUsuarioService} from 'src/app/core/services/imagen-usuario.service';

@Component({
  selector: 'vex-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  @Input() mobileQuery: boolean;

  @Input()
  @HostBinding('class.shadow-b')
  hasShadow: boolean;

  navigationItems = this.navigationService.items;

  isHorizontalLayout$ = this.configService.config$.pipe(map(config => config.layout === 'horizontal'));
  isVerticalLayout$ = this.configService.config$.pipe(map(config => config.layout === 'vertical'));
  isNavbarInToolbar$ = this.configService.config$.pipe(map(config => config.navbar.position === 'in-toolbar'));
  isNavbarBelowToolbar$ = this.configService.config$.pipe(map(config => config.navbar.position === 'below-toolbar'));

  icSearch = icSearch;
  icMenu = icMenu;
  icAdd = icAdd;
  icArrowDropDown = icArrowDropDown;
  icPaciente = icPaciente;
  icMedKit = icMedKit;
  icCollege = icCollege;
  icInfo = icInfo;
  icLink = icLink;
  icUsers = icUsers;
  paciente: Paciente;
  pacienteSimpleName;

  constructor(
    private layoutService: LayoutService,
    private router: Router,
    private configService: ConfigService,
    private navigationService: NavigationService,
    private popoverService: PopoverService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private pacienteGlobalService: PacienteGlobalService,
    private imagenUsuarioService: ImagenUsuarioService
  ) {
  }

  ngOnInit() {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        this.paciente = paciente;
        this.getToolbarText();
      });
  }

  getToolbarText(){
    if (this.paciente){
      this.pacienteSimpleName = this.paciente.primerNombre + ' ' + this.paciente.apellidoPaterno;
    }
  }

  openQuickpanel() {
    this.layoutService.openQuickpanel();
  }

  openSidenav() {
    this.layoutService.openSidenav();
  }

  openSelectPatientDialog(){
    this.dialog.open(PacienteSelectMenuComponent, {
      width: '900px',
      maxWidth: '100%',
    })
      .afterClosed();
  }

  openMegaMenu(origin: ElementRef | HTMLElement) {
    this.popoverService.open({
      content: MegaMenuComponent,
      origin,
      position: [
        {
          originX: 'start',
          originY: 'bottom',
          overlayX: 'start',
          overlayY: 'top'
        },
        {
          originX: 'end',
          originY: 'bottom',
          overlayX: 'end',
          overlayY: 'top',
        },
      ]
    });
  }

  openSearch() {
    this.layoutService.openSearch();
  }

  getUserImage(id: string) {
    if (!id) { return ; }
    return this.imagenUsuarioService.getUserImage(id);
  }

  navigateTo(route: string) {
    this.router.navigate([route]);
  }
}
