import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { PopoverService } from '../../../components/popover/popover.service';
import { ToolbarUserDropdownComponent } from './toolbar-user-dropdown/toolbar-user-dropdown.component';
import icPerson from '@iconify/icons-ic/twotone-person';
import {AuthService} from 'src/app/core/auth/auth.service';
import {UserService} from 'src/app/core/services/user.service';
import {UserInfo} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/user-info.interface';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Observable} from 'rxjs';
import {SaludUserService} from 'src/app/core/services/salud-user.service';
import {switchMap} from 'rxjs/operators';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'vex-toolbar-user',
  templateUrl: './toolbar-user.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToolbarUserComponent implements OnInit {

  dropdownOpen: boolean;
  icPerson = icPerson;

  public isLoggedIn = false;
  public userName: string;
  public userData: Observable<UserInfo>;

  constructor(private popover: PopoverService,
              private cd: ChangeDetectorRef,
              private authService: AuthService,
              private userService: UserService,
              private saludUserService: SaludUserService,
              private dialog: MatDialog,
              private snackbar: MatSnackBar) { }

  ngOnInit() {
    this.isLoggedIn = this.authService.isLoggedIn();
    this.userData = this.authService.userData$;
    if (this.isLoggedIn) {
      this.userName = this.authService.getUserName();
      this.getUserByUsername(this.userName);
    }
  }

  getUserByUsername(username: string){
    this.userService.getUserByUsername(username).pipe(
      switchMap(data => {
        if (data) {
          this.authService.saveUserInstance(data[0]);
          return this.saludUserService.getUser(data[0].pidm);
        }
      })
    ).subscribe((saludUser: SaludUser) => {
        if (saludUser && saludUser.activo){
          this.authService.saveSaludUserInstance(saludUser);
        } else {
          this.showInsufficientAccess();
        }
      }, error => {
        this.showInsufficientAccess();
      });
  }

  showInsufficientAccess() {
    this.showNotification('El usuario no tiene acceso a ESPE SALUD', 'CERRAR');
    setTimeout(() => {
      this.authService.logout();
    }, 3000);
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000,
    });
  }

  showPopover(originRef: HTMLElement) {
    this.dropdownOpen = true;
    this.cd.markForCheck();

    const popoverRef = this.popover.open({
      content: ToolbarUserDropdownComponent,
      origin: originRef,
      offsetY: 12,
      position: [
        {
          originX: 'center',
          originY: 'top',
          overlayX: 'center',
          overlayY: 'bottom'
        },
        {
          originX: 'end',
          originY: 'bottom',
          overlayX: 'end',
          overlayY: 'top',
        },
      ]
    });

    popoverRef.afterClosed$.subscribe(() => {
      this.dropdownOpen = false;
      this.cd.markForCheck();
    });
  }
}
