export interface UserInfo {
  id: string;
  cedula: string;
  correoInstitucional: string;
  correoPersonal: string;
  nombres: string;
  pidm: number;
  username: string;
  urlFoto: string;
}

export function getUserShortName(userFullName: string): string {
  const namesArr = userFullName.split(' ');
  const lastName = namesArr[0];
  const firstName = namesArr[2];
  return `${firstName} ${lastName}`;
}
