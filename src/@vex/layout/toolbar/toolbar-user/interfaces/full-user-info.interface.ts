export interface FullUserInfo {
  id: string;
  cedula: string;
  correoInstitucional: string;
  correoPersonal: string;
  nombres: string;
  apellidos: string;
  nombreCompleto: string;
  pidm: number;
  direccion: string;
  telefono: string;
  celular: string;
}
