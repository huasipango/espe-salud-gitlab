import {RolEnum} from 'src/app/core/enums/rol-enum';

export interface Rol {
  id: number;
  nombre: RolEnum;
}
