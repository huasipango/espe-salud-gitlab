import {AreaSalud} from 'src/app/core/models/catalogo/area-salud.model';
import {Dispensario} from 'src/app/core/models/catalogo/dispensario.model';
import {Rol} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/rol.interface';
import {FormacionProfesional} from 'src/app/core/models/catalogo/formacion-profesional.model';
import {EspecialidadProfesional} from 'src/app/core/models/catalogo/especialidad-profesional.model';
import {Etnia} from 'src/app/core/models/catalogo-banner/etnia.model';

export class SaludUser {
  id: string;
  pidm: number;
  username: string;
  idTipoIdentificacion: number;
  idIdentidadGenero: string;
  sexo: string;
  idFormacionProfesional: number;
  formacionProfesional: FormacionProfesional;
  idEspecialidadProfesional: number;
  especialidadProfesional: EspecialidadProfesional;
  idEtnia: number;
  etnia: Etnia;
  codigoNacionalidad: string;
  nacionalidad: string;
  idBanner: string;
  nombres: string;
  codigoMsp: string;
  apellidos: string;
  cedula: string;
  activo: boolean;
  correoInstitucional: string;
  fechaNacimiento: Date;
  idAreaSalud: number;
  areaSalud: AreaSalud;
  idDispensario: number;
  dispensario: Dispensario;
  urlFoto: string;
  firma: string;
  correoPersonal: string;
  roles: Array<Rol>;
}
