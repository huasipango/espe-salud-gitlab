import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {MenuItem} from '../interfaces/menu-item.interface';
import {trackById} from '../../../../utils/track-by';
import icPerson from '@iconify/icons-ic/twotone-person';
import icAccountCircle from '@iconify/icons-ic/twotone-account-circle';
import icChevronRight from '@iconify/icons-ic/twotone-chevron-right';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';
import {Icon} from '@visurel/iconify-angular';
import {PopoverRef} from 'src/@vex/components/popover/popover-ref';
import {AuthService} from 'src/app/core/auth/auth.service';
import {Observable} from 'rxjs';
import {getUserShortName, UserInfo} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/user-info.interface';
import {ImagenUsuarioService} from 'src/app/core/services/imagen-usuario.service';

export interface OnlineStatus {
  id: 'online' | 'away' | 'dnd' | 'offline';
  label: string;
  icon: Icon;
  colorClass: string;
}

@Component({
  selector: 'vex-toolbar-user-dropdown',
  templateUrl: './toolbar-user-dropdown.component.html',
  styleUrls: ['./toolbar-user-dropdown.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToolbarUserDropdownComponent implements OnInit {

  public userData: Observable<UserInfo>;

  items: MenuItem[] = [
    {
      id: '1',
      icon: icAccountCircle,
      label: 'Mi Perfil',
      description: 'Información personal',
      colorClass: 'text-teal',
      route: '/perfil-usuario'
    }
  ];

  trackById = trackById;
  icPerson = icPerson;
  icChevronRight = icChevronRight;
  icArrowDropDown = icArrowDropDown;

  constructor(private cd: ChangeDetectorRef,
              private popoverRef: PopoverRef<ToolbarUserDropdownComponent>,
              private authService: AuthService,
              private imagenUsuarioService: ImagenUsuarioService) {
  }

  ngOnInit() {
    this.userData = this.authService.userData$;
  }

  close() {
    this.popoverRef.close();
  }

  getShortName(userFullName: string): string {
    return getUserShortName(userFullName);
  }

  logout() {
    this.popoverRef.close();
    this.authService.logout();
  }

  getUserImage(id: string) {
    return this.imagenUsuarioService.getUserImage(id);
  }
}
