import { Component, Input, OnInit } from '@angular/core';
import { trackByRoute } from '../../utils/track-by';
import { NavigationService } from '../../services/navigation.service';
import icRadioButtonUnchecked from '@iconify/icons-ic/twotone-radio-button-unchecked';
import { LayoutService } from '../../services/layout.service';
import { ConfigService } from '../../services/config.service';
import { map } from 'rxjs/operators';
import icThumbtack from '@iconify/icons-fa-solid/thumbtack';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {PacienteSelectMenuComponent} from 'src/app/core/components/paciente-select-menu/paciente-select-menu.component';
import {MatDialog} from '@angular/material/dialog';
import {AuthService} from 'src/app/core/auth/auth.service';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import {Observable} from 'rxjs';

@Component({
  selector: 'vex-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  @Input() collapsed: boolean;
  collapsedOpen$ = this.layoutService.sidenavCollapsedOpen$;
  title$ = this.configService.config$.pipe(map(config => config.sidenav.title));
  imageUrl$ = this.configService.config$.pipe(map(config => config.sidenav.imageUrl));
  showCollapsePin$ = this.configService.config$.pipe(map(config => config.sidenav.showCollapsePin));
  isDesktop$ = this.layoutService.isDesktop$;

  items = this.navigationService.items;
  trackByRoute = trackByRoute;
  icRadioButtonUnchecked = icRadioButtonUnchecked;
  icThumbtack = icThumbtack;
  icArrowDropDown = icArrowDropDown;

  patient: Observable<Paciente>;

  constructor(private navigationService: NavigationService,
              private layoutService: LayoutService,
              private configService: ConfigService,
              private pacienteGlobalService: PacienteGlobalService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.patient = this.pacienteGlobalService.pacienteGlobal$;
  }

  onMouseEnter() {
    this.layoutService.collapseOpenSidenav();
  }

  onMouseLeave() {
    this.layoutService.collapseCloseSidenav();
  }

  toggleCollapse() {
    this.collapsed ? this.layoutService.expandSidenav() : this.layoutService.collapseSidenav();
  }

  buscarPaciente(){
    this.dialog.open(PacienteSelectMenuComponent, {
      width: '900px',
      maxWidth: '100%',
    }).afterClosed();
  }
}
