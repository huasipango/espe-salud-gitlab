import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartNgComponent } from './chart-ng.component';



@NgModule({
  declarations: [ChartNgComponent],
  exports: [
    ChartNgComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ChartNgModule { }
