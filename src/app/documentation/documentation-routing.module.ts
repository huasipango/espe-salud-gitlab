import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {VexRoutes} from 'src/@vex/interfaces/vex-route.interface';

const routes: VexRoutes = [
  {
    path: '',
    loadChildren: () => import('./changelog/changelog.module').then(m => m.ChangelogModule),
    data: {
      containerEnabled: true
    },
  },
  {
    path: '',
    loadChildren: () => import('./centro-ayuda/centro-ayuda.module').then(m => m.CentroAyudaModule),
    data: {
      containerEnabled: true,
      toolbarShadowEnabled: true
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentationRoutingModule { }
