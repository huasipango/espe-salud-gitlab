import { Component, OnInit } from '@angular/core';
import icSearch from '@iconify/icons-ic/twotone-search';
import icMail from '@iconify/icons-ic/twotone-mail';
import icPhoneInTalk from '@iconify/icons-ic/twotone-phone-in-talk';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';

@Component({
  selector: 'vex-centro-ayuda',
  templateUrl: './centro-ayuda.component.html',
  styleUrls: ['./centro-ayuda.component.scss'],
  animations: [
    fadeInUp400ms
  ]
})
export class CentroAyudaComponent implements OnInit {
  icSearch = icSearch;
  icMail = icMail;
  icPhoneInTalk = icPhoneInTalk;
  constructor() { }

  ngOnInit(): void {
  }
}
