import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CentroAyudaComponent} from 'src/app/documentation/centro-ayuda/centro-ayuda.component';

const routes: Routes = [
  {
    path: 'ayuda',
    component: CentroAyudaComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./pages/coleccion-ayuda/coleccion-ayuda.module')
          .then(m => m.ColeccionAyudaModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CentroAyudaRoutingModule { }
