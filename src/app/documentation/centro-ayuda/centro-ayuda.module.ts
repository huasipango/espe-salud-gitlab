import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CentroAyudaRoutingModule } from 'src/app/documentation/centro-ayuda/centro-ayuda-routing.module';
import { CentroAyudaComponent } from 'src/app/documentation/centro-ayuda/centro-ayuda.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {IconModule} from '@visurel/iconify-angular';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatIconModule} from '@angular/material/icon';


@NgModule({
  declarations: [CentroAyudaComponent],
  imports: [
    CommonModule,
    CentroAyudaRoutingModule,
    FlexLayoutModule,
    IconModule,
    MatButtonModule,
    MatTooltipModule,
    MatIconModule
  ]
})
export class CentroAyudaModule { }
