import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from 'src/environments/environment';
import {ColeccionAyuda} from 'src/app/documentation/centro-ayuda/models/coleccion-ayuda.model';

@Injectable({
  providedIn: 'root',
})
export class ColeccionAyudaService {
  private baseUrl: string = environment.baseUrl;
  private readonly commonUrl: string;

  constructor(private http: HttpClient) {
    this.commonUrl = this.baseUrl + 'colecciones-ayuda/';
  }

  getColeccionesAyuda(): Observable<ColeccionAyuda[]> {
    return this.http.get<ColeccionAyuda[]>(this.commonUrl);
  }

  createColeccionAyuda(data): Observable<ColeccionAyuda> {
    return this.http.post<ColeccionAyuda>(this.commonUrl, JSON.stringify(data));
  }

  updateColeccionAyuda(id, data): Observable<ColeccionAyuda> {
    return this.http.put<ColeccionAyuda>(this.commonUrl + id, JSON.stringify(data));
  }
}
