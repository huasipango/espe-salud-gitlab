import {Component, Inject, OnInit, Renderer2} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ColeccionAyuda} from 'src/app/documentation/centro-ayuda/models/coleccion-ayuda.model';
import {AuthService} from 'src/app/core/auth/auth.service';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import icClose from '@iconify/icons-ic/twotone-close';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {iconsFA} from 'src/static-data/icons-fa';
import {DOCUMENT} from '@angular/common';
import {debounceTime, distinctUntilChanged, map, startWith} from 'rxjs/operators';
import {ColeccionAyudaService} from 'src/app/documentation/centro-ayuda/services/coleccion-ayuda.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {LoadingService} from 'src/app/core/services/loading.service';

@Component({
  selector: 'vex-coleccion-ayuda-modal',
  templateUrl: './coleccion-ayuda-modal.component.html',
  styleUrls: ['./coleccion-ayuda-modal.component.scss']
})
export class ColeccionAyudaModalComponent implements OnInit {
  static fontLoaded: boolean;

  user: SaludUser;
  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  icClose = icClose;
  errorMessages = FORM_ERROR_MESSAGES;
  selectCtrl: FormControl;
  filteredIcons = iconsFA;
  userMessages = USER_MESSAGES;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: ColeccionAyuda,
    private dialogRef: MatDialogRef<ColeccionAyudaModalComponent>,
    private authService: AuthService,
    private fb: FormBuilder,
    private renderer: Renderer2,
    @Inject(DOCUMENT) private document: Document,
    private coleccionAyudaService: ColeccionAyudaService,
    private snackbar: MatSnackBar,
    private loadingService: LoadingService
  ) { }

  ngOnInit(): void {
    if (!ColeccionAyudaModalComponent.fontLoaded) {
      this.loadFont();
    }
    if (this.data) {
      this.mode = 'update';
    } else {
      this.data = {} as ColeccionAyuda;
    }
    this.selectCtrl = new FormControl(
      this.data.icono || null, Validators.required
    );
    this.form = this.fb.group({
      id: [this.data.id || null],
      titulo: [this.data.titulo || null, Validators.required],
      slug: [this.data.slug || null, Validators.required],
      icono: this.selectCtrl,
      creadoPorPidm: [this.data.creadoPorPidm || null, Validators.required]
    });
    this.authService.saludUserData$.subscribe(user => {
      if (user) {
        this.user = user;
        this.form.patchValue({creadoPorPidm: this.user.pidm});
      }
    });
    this.selectCtrl.valueChanges.pipe(
      startWith(''),
      debounceTime(400),
      distinctUntilChanged(),
    ).subscribe(value => this.filterIcon(value));
  }

  filterIcon(value: string) {
    if (value === '' || !value) {
      this.filteredIcons = iconsFA;
    }
    this.filteredIcons = this.filteredIcons.filter(iconName => iconName.includes((value || '').toLowerCase()));
  }

  loadFont() {
    ColeccionAyudaModalComponent.fontLoaded = true;
    const scriptElem = this.renderer.createElement('script');
    this.renderer.setAttribute(scriptElem, 'crossorigin', 'anonymous');
    this.renderer.setAttribute(scriptElem, 'src', 'https://kit.fontawesome.com/24a46da608.js');
    this.renderer.appendChild(this.document?.head, scriptElem);
  }

  save() {
    const data: ColeccionAyuda = this.form.value;
    if (this.mode === 'create') {
      this.create(data);
    } else if (this.mode === 'update') {
      this.update(data);
    }
  }

  create(data: ColeccionAyuda) {
    this.loadingService.showLoaderUntilCompleted(
      this.coleccionAyudaService.createColeccionAyuda(data)
    ).subscribe((response) => {
      if (response) {
        this.showNotification(this.userMessages.createdSuccessMessage)
        this.dialogRef.close(response);
      }
    }, error => {
      this.showNotification(this.userMessages.createdFailedMessage);
    });
  }

  update(data: ColeccionAyuda) {
    this.dialogRef.close(data);
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  showNotification(message: string) {
    this.snackbar.open(message, 'CERRAR', {
      duration: 5000
    });
  }
}
