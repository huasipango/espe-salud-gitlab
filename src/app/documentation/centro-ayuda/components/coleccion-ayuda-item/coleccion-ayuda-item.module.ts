import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ColeccionAyudaItemComponent } from './coleccion-ayuda-item.component';
import {RouterModule} from '@angular/router';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {MatTooltipModule} from '@angular/material/tooltip';



@NgModule({
  declarations: [
    ColeccionAyudaItemComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatIconModule,
    IconModule,
    MatTooltipModule
  ],
  exports: [
    ColeccionAyudaItemComponent
  ]
})
export class ColeccionAyudaItemModule { }
