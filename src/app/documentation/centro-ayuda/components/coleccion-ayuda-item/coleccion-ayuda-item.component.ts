import {Component, Inject, Input, OnInit, Renderer2} from '@angular/core';
import {ColeccionAyuda} from 'src/app/documentation/centro-ayuda/models/coleccion-ayuda.model';
import icAdd from '@iconify/icons-ic/twotone-add';
import {ImagenUsuarioService} from 'src/app/core/services/imagen-usuario.service';
import {DOCUMENT} from '@angular/common';

@Component({
  selector: 'vex-coleccion-ayuda-item',
  templateUrl: './coleccion-ayuda-item.component.html',
  styleUrls: ['./coleccion-ayuda-item.component.scss']
})
export class ColeccionAyudaItemComponent implements OnInit {
  static fontLoaded: boolean;

  @Input() collection: ColeccionAyuda;
  icAdd = icAdd;
  constructor(
    private imagenUsuarioService: ImagenUsuarioService,
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2
  ) { }

  ngOnInit(): void {
    if (!ColeccionAyudaItemComponent.fontLoaded) {
      this.loadFont();
    }
  }

  getUserImage(idBanner: string) {
    return this.imagenUsuarioService.getUserImage(idBanner);
  }

  loadFont() {
    ColeccionAyudaItemComponent.fontLoaded = true;
    const scriptElem = this.renderer.createElement('script');
    this.renderer.setAttribute(scriptElem, 'crossorigin', 'anonymous');
    this.renderer.setAttribute(scriptElem, 'src', 'https://kit.fontawesome.com/24a46da608.js');
    this.renderer.appendChild(this.document?.head, scriptElem);
  }
}
