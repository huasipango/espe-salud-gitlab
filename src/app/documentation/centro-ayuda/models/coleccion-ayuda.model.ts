import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';

export class ColeccionAyuda {
  id: number;
  titulo: string;
  slug: string;
  icono: string;
  creadoPorPidm: number;
  creadoPor: SaludUser;
}
