import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ColeccionAyudaRoutingModule } from './coleccion-ayuda-routing.module';
import { ColeccionAyudaComponent } from './coleccion-ayuda.component';
import {IconModule} from '@visurel/iconify-angular';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatIconModule} from '@angular/material/icon';
import {ColeccionAyudaItemModule} from 'src/app/documentation/centro-ayuda/components/coleccion-ayuda-item/coleccion-ayuda-item.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';


@NgModule({
  declarations: [
    ColeccionAyudaComponent
  ],
  imports: [
    CommonModule,
    ColeccionAyudaRoutingModule,
    IconModule,
    MatButtonModule,
    MatTooltipModule,
    MatIconModule,
    ColeccionAyudaItemModule,
    FlexLayoutModule,
    LoadingModule
  ]
})
export class ColeccionAyudaModule { }
