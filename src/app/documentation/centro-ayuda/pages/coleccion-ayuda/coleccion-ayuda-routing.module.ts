import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ColeccionAyudaComponent} from 'src/app/documentation/centro-ayuda/pages/coleccion-ayuda/coleccion-ayuda.component';

const routes: Routes = [
  {
    path: '',
    component: ColeccionAyudaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ColeccionAyudaRoutingModule { }
