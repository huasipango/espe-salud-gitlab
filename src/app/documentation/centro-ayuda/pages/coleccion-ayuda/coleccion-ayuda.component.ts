import { Component, OnInit } from '@angular/core';
import {ColeccionAyudaModalComponent} from 'src/app/documentation/centro-ayuda/components/coleccion-ayuda-modal/coleccion-ayuda-modal.component';
import icAdd from '@iconify/icons-ic/twotone-add';
import {MatDialog} from '@angular/material/dialog';
import {filter} from 'rxjs/operators';
import {ColeccionAyuda} from 'src/app/documentation/centro-ayuda/models/coleccion-ayuda.model';
import {ColeccionAyudaService} from 'src/app/documentation/centro-ayuda/services/coleccion-ayuda.service';
import {Observable} from 'rxjs';
import { trackById } from 'src/@vex/utils/track-by';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';

@Component({
  selector: 'vex-coleccion-ayuda',
  templateUrl: './coleccion-ayuda.component.html',
  styleUrls: ['./coleccion-ayuda.component.scss'],
  animations: [
    fadeInUp400ms
  ]
})
export class ColeccionAyudaComponent implements OnInit {
  icAdd = icAdd;
  coleccionesAyuda$: Observable<ColeccionAyuda[]>;
  trackById = trackById;

  constructor(
    private dialog: MatDialog,
    private coleccionAyudaService: ColeccionAyudaService
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.coleccionesAyuda$ = this.coleccionAyudaService.getColeccionesAyuda();
  }

  addNewCollection() {
    this.dialog.open(ColeccionAyudaModalComponent, {
      maxWidth: '100%',
      width: '600px'
    }).beforeClosed().pipe(
      filter<ColeccionAyuda>(Boolean)
    ).subscribe(value => {
      if (value) {
        this.getData();
      }
    });
  }
}
