import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ChangelogComponent} from 'src/app/documentation/changelog/changelog.component';
import {QuicklinkModule} from 'ngx-quicklink';

const routes: Routes = [
  {
    path: 'versiones',
    component: ChangelogComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule, QuicklinkModule]
})
export class ChangelogRoutingModule { }
