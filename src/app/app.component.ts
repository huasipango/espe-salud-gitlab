import {Component, Inject, LOCALE_ID, OnInit, Renderer2} from '@angular/core';
import {ConfigService} from '../@vex/services/config.service';
import {Settings} from 'luxon';
import {DOCUMENT} from '@angular/common';
import {ActivatedRoute, NavigationEnd} from '@angular/router';
import {Platform} from '@angular/cdk/platform';
import {NavigationService} from '../@vex/services/navigation.service';
import icContacts from '@iconify/icons-ic/twotone-contacts';
import icHospital from '@iconify/icons-fa-solid/hospital-symbol';
import icNotesMedical from '@iconify/icons-fa-solid/notes-medical';
import icHeartBeat from '@iconify/icons-fa-solid/heartbeat';
import icPatient from '@iconify/icons-fa-solid/user-injured';
import icBookMedical from '@iconify/icons-fa-solid/book-medical';
import icReceipt from '@iconify/icons-fa-solid/receipt';
import icHome from '@iconify/icons-fa-solid/home';
import icUniversalAccess from '@iconify/icons-fa-solid/universal-access';
import icMedKit from '@iconify/icons-fa-solid/medkit';
import icWarning from '@iconify/icons-ic/warning';
import icHeart from '@iconify/icons-fa-solid/heart';
import icChild from '@iconify/icons-fa-solid/child';
import icChartBar from '@iconify/icons-fa-solid/chart-bar';
import icBaby from '@iconify/icons-fa-solid/baby';
import icUserTie from '@iconify/icons-fa-solid/user-tie';
import icBook from '@iconify/icons-fa-solid/book';
import {LayoutService} from '../@vex/services/layout.service';
import {filter, map} from 'rxjs/operators';
import {coerceBooleanProperty} from '@angular/cdk/coercion';
import {SplashScreenService} from '../@vex/services/splash-screen.service';
import {Style, StyleService} from '../@vex/services/style.service';
import {ConfigName} from '../@vex/interfaces/config-name.model';
import {AuthService} from 'src/app/core/auth/auth.service';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {PacienteService} from 'src/app/core/services/paciente/paciente.service';
import {RolEnum} from 'src/app/core/enums/rol.enum';
import icMicroscope from '@iconify/icons-fa-solid/microscope';
import icUpdate from '@iconify/icons-ic/twotone-update';
import icCatalog from '@iconify/icons-ic/book';
import icContactSupport from '@iconify/icons-ic/twotone-contact-support';



import icVaccine from '@iconify/icons-ic/outline-colorize';
import {environment} from 'src/environments/environment';
import icSettings from '@iconify/icons-ic/twotone-settings';

declare let gtag;

@Component({
  selector: 'vex-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public isLoggedIn = false;
  public title = 'ESPE Salud';

  constructor(private configService: ConfigService,
              private styleService: StyleService,
              private renderer: Renderer2,
              private platform: Platform,
              @Inject(DOCUMENT) private document: Document,
              @Inject(LOCALE_ID) private localeId: string,
              private layoutService: LayoutService,
              private route: ActivatedRoute,
              private navigationService: NavigationService,
              private splashScreenService: SplashScreenService,
              private authService: AuthService,
              private pacienteService: PacienteService,
              private pacienteGlobalService: PacienteGlobalService) {
    Settings.defaultLocale = this.localeId;
    const navEndEvents$ = this.route.data
      .pipe(
        filter(event => event instanceof NavigationEnd)
      );
    navEndEvents$.subscribe((event: NavigationEnd) => {
      gtag('config', 'G-DBFZFCKJDV', {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        page_path: event.urlAfterRedirects
      });
    });

    if (this.platform.BLINK) {
      this.renderer.addClass(this.document.body, 'is-blink');
    }

    this.configService.updateConfig({
      sidenav: {
        title: 'MENÚ',
        imageUrl: 'assets/img/espe/Espe-Angular-Logo.png',
      },
      footer: {
        visible: true,
        fixed: true
      }
    });

    /**
     * Customize the template to your needs with the ConfigService
     * Example:
     *  this.configService.updateConfig({
     *    sidenav: {
     *      title: 'Custom App',
     *      imageUrl: '//placehold.it/100x100',
     *      showCollapsePin: false
     *    },
     *    showConfigButton: false,
     *    footer: {
     *      visible: false
     *    }
     *  });
     */

    /**
     * Config Related Subscriptions
     * You can remove this if you don't need the functionality of being able to enable specific configs with queryParams
     * Example: example.com/?layout=apollo&style=default
     */
    this.route.queryParamMap.pipe(
      filter(queryParamMap => queryParamMap.has('rtl')),
      map(queryParamMap => coerceBooleanProperty(queryParamMap.get('rtl'))),
    ).subscribe(isRtl => {
      this.configService.updateConfig({
        rtl: isRtl
      });
    });

    this.route.queryParamMap.pipe(
      filter(queryParamMap => queryParamMap.has('layout'))
    ).subscribe(queryParamMap => this.configService.setConfig(queryParamMap.get('layout') as ConfigName));

    this.route.queryParamMap.pipe(
      filter(queryParamMap => queryParamMap.has('style'))
    ).subscribe(queryParamMap => this.styleService.setStyle(queryParamMap.get('style') as Style));


    this.navigationService.items = [
      {
        type: 'link',
        label: 'Inicio',
        route: '/',
        icon: icHome,
        showByDefault: true,
        routerLinkActiveOptions: {exact: true}
      },
      {
        type: 'dropdown',
        label: 'Estadísticas',
        icon: icChartBar,
        showByDefault: true,
        roles: [
          RolEnum.ROLE_OCUPACIONAL,
          RolEnum.ROLE_ENFERMERO,
          RolEnum.ROLE_MEDICO,
          RolEnum.ROLE_ADMIN,
          RolEnum.ROLE_ODONTOLOGO
        ],
        children: [
          {
            type: 'link',
            label: 'General',
            route: '/dashboard/general',
            roles: [
              RolEnum.ROLE_OCUPACIONAL,
              RolEnum.ROLE_ENFERMERO,
              RolEnum.ROLE_MEDICO,
              RolEnum.ROLE_ADMIN,
              RolEnum.ROLE_ODONTOLOGO
            ],
            showByDefault: true,
            icon: icChartBar
          },
          {
            type: 'link',
            label: 'Personal',
            route: '/dashboard/personal',
            roles: [
              RolEnum.ROLE_OCUPACIONAL,
              RolEnum.ROLE_ENFERMERO,
              RolEnum.ROLE_MEDICO,
              RolEnum.ROLE_ODONTOLOGO
            ],
            showByDefault: true,
            icon: icChartBar
          },
        ]
      },

      {
        type: 'link',
        label: 'Datos del Paciente',
        route: '/medicina-general/perfil-paciente',
        icon: icPatient
      },
      {
        type: 'dropdown',
        label: 'Antecedentes',
        icon: icContacts,
        children: [
          {
            type: 'link',
            label: 'Antecedentes personales',
            route: '/medicina-general/antecedentes-personales',
            icon: icContacts
          },
          {
            type: 'link',
            label: 'Hospitalizaciones',
            route: '/medicina-general/hospitalizacion',
            icon: icHospital
          },
          {
            type: 'link',
            label: 'Estudios complementarios',
            route: '/medicina-general/estudio-complementario',
            icon: icNotesMedical
          },
          // {
          //   type: 'link',
          //   label: 'Laboratorio',
          //   route: '/medicina-general/antecedente-laboratorio',
          //   icon: icMicroscope,
          // },
        ]
      },
      {
        type: 'dropdown',
        label: 'Examen Físico',
        icon: icHeart,
        children: [
          {
            type: 'link',
            label: 'Órganos y sistemas',
            route: '/medicina-general/revision-organos-sistemas',
            icon: icHeart
          },
          {
            type: 'link',
            label: 'Examen físico regional',
            route: '/medicina-general/examenes-fisico-regionales',
            icon: icChild
          }
        ]
      },
      {
        type: 'dropdown',
        label: 'Enfermería',
        icon: icBookMedical,
        roles: [RolEnum.ROLE_OCUPACIONAL, RolEnum.ROLE_ENFERMERO, RolEnum.ROLE_MEDICO],
        children: [
          {
            type: 'link',
            label: 'Signos Vitales',
            route: '/enfermeria/notas-enfermeria',
            roles: [
              RolEnum.ROLE_ENFERMERO,
              RolEnum.ROLE_MEDICO
            ],
            icon: icBookMedical
          },
          {
            type: 'link',
            label: 'Actividades de Enfermeria',
            route: '/enfermeria/actividades-enfermeria',
            showByDefault: true,
            roles: [RolEnum.ROLE_ENFERMERO],
            icon: icHeartBeat
          },
          {
            type: 'link',
            label: 'Gestación/Lactancia',
            route: '/enfermeria/gestacion-lactancia',
            roles: [RolEnum.ROLE_ENFERMERO, RolEnum.ROLE_MEDICO],
            icon: icBaby
          }
        ]
      },
      {
        type: 'dropdown',
        label: 'Evolución',
        icon: icMedKit,
        roles: [
          RolEnum.ROLE_ENFERMERO,
          RolEnum.ROLE_MEDICO,
          RolEnum.ROLE_OCUPACIONAL,
          RolEnum.ROLE_ODONTOLOGO
        ],
        children: [
          {
            type: 'link',
            label: 'Evoluciones',
            route: '/medicina-general/evolucion',
            roles: [
              RolEnum.ROLE_ENFERMERO,
              RolEnum.ROLE_MEDICO,
              RolEnum.ROLE_OCUPACIONAL,
              RolEnum.ROLE_ODONTOLOGO
            ],
            icon: icMedKit
          },
        ]
      },
      {
        type: 'dropdown',
        label: 'Laboratorio clínico',
        icon: icMicroscope,
        roles: [RolEnum.ROLE_LABORATORISTA],
        children: [
          {
            type: 'link',
            label: 'Exámenes de laboratorio',
            route: '/medicina-general/pedidos-examen',
            roles: [RolEnum.ROLE_LABORATORISTA],
            icon: icMicroscope,
          },
        ]
      },
      {
        type: 'dropdown',
        label: 'Vacunación',
        icon: icVaccine,
        roles: [RolEnum.ROLE_MEDICO, RolEnum.ROLE_ENFERMERO],
        children: [
          {
            type: 'link',
            label: 'Vacunación',
            roles: [RolEnum.ROLE_MEDICO, RolEnum.ROLE_ENFERMERO],
            route: '/medicina-general/vacunacion',
            icon: icVaccine
          },
        ]
      },
      {
        type: 'dropdown',
        label: 'Certificados',
        icon: icReceipt,
        roles: [RolEnum.ROLE_MEDICO],
        children: [
          {
            type: 'link',
            label: 'Validación certificados',
            roles: [RolEnum.ROLE_MEDICO],
            route: '/medicina-general/certificados',
            icon: icReceipt
          }
        ]
      },
      {
        type: 'subheading',
        label: 'Ocupacional',
        roles: [RolEnum.ROLE_OCUPACIONAL],
        children: [
          {
            type: 'link',
            label: 'Antecedentes laborales',
            roles: [RolEnum.ROLE_OCUPACIONAL],
            route: '/salud-ocupacional/antecedente-laboral/antecedentes',
            icon: icWarning,
          },
          {
            type: 'dropdown',
            label: 'Puesto Trabajo',
            roles: [RolEnum.ROLE_OCUPACIONAL],
            icon: icUniversalAccess,
            children: [
              {
                type: 'link',
                label: 'Record Laboral',
                roles: [RolEnum.ROLE_OCUPACIONAL],
                route: '/salud-ocupacional/antecedente-laboral/record-laboral',
              }
            ]
          },
        ]
      },
      // {
      //   type: 'subheading',
      //   label: 'ODONTOLOGÍA',
      //   roles: [RolEnum.ROLE_ODONTOLOGO],
      //   children: [
      //     {
      //       type: 'link',
      //       label: 'Historia Clínica',
      //       roles: [RolEnum.ROLE_ODONTOLOGO],
      //       icon: icBookMedical,
      //       route: '/odontologia/historia-clinica',
      //     },
      //     {
      //       type: 'link',
      //       label: 'Salud Bucal',
      //       roles: [RolEnum.ROLE_ODONTOLOGO],
      //       icon: icTooth,
      //       route: '/odontologia/odontograma',
      //     }
      //   ]
      // },
      {
        type: 'dropdown',
        label: 'Documentación',
        icon: icBook,
        showByDefault: true,
        roles: [
          RolEnum.ROLE_ADMIN,
          RolEnum.ROLE_MEDICO,
          RolEnum.ROLE_ODONTOLOGO,
          RolEnum.ROLE_OCUPACIONAL,
          RolEnum.ROLE_ENFERMERO,
          RolEnum.ROLE_ADMISION,
          RolEnum.ROLE_SOCIAL
        ],
        children: [
          {
            type: 'link',
            label: 'Versiones',
            icon: icUpdate,
            showByDefault: true,
            roles: [
              RolEnum.ROLE_ADMIN,
              RolEnum.ROLE_MEDICO,
              RolEnum.ROLE_ODONTOLOGO,
              RolEnum.ROLE_OCUPACIONAL,
              RolEnum.ROLE_ENFERMERO,
              RolEnum.ROLE_ADMISION,
              RolEnum.ROLE_SOCIAL
            ],
            route: '/docs/versiones',
          },
          {
            type: 'link',
            label: 'Centro de ayuda',
            icon: icContactSupport,
            showByDefault: true,
            roles: [
              RolEnum.ROLE_ADMIN,
              RolEnum.ROLE_MEDICO,
              RolEnum.ROLE_ODONTOLOGO,
              RolEnum.ROLE_OCUPACIONAL,
              RolEnum.ROLE_ENFERMERO,
              RolEnum.ROLE_ADMISION,
              RolEnum.ROLE_SOCIAL
            ],
            route: '/docs/ayuda',
          }
        ]
      },
      {
        type: 'dropdown',
        label: 'Administración',
        icon: icSettings,
        showByDefault: true,
        roles: [RolEnum.ROLE_ADMIN],
        children: [
          {
            type: 'link',
            label: 'Usuarios',
            icon: icUserTie,
            showByDefault: true,
            roles: [RolEnum.ROLE_ADMIN],
            route: '/administracion/usuarios',
          }
        ]
      },
      {
        type: 'dropdown',
        label: 'Catálogos',
        icon: icCatalog,
        showByDefault: true,
        roles: [RolEnum.ROLE_ADMIN],
        children: [
          {
            type: 'link',
            label: 'Áreas',
            showByDefault: true,
            roles: [RolEnum.ROLE_ADMIN],
            route: '/catalogos/areas',
          },
          {
            type: 'link',
            label: 'Asociaciones',
            showByDefault: true,
            roles: [RolEnum.ROLE_ADMIN],
            route: '/catalogos/asociaciones',
          },
          {
            type: 'link',
            label: 'Dispensarios',
            showByDefault: true,
            roles: [RolEnum.ROLE_ADMIN],
            route: '/catalogos/dispensarios',
          }
        ]
      },
    ];
  }

  ngOnInit(): void {
    this.isLoggedIn = this.authService.isLoggedIn();
    if (!this.isLoggedIn) {
      this.authService.login();
    } else {
      const expirationDuration = new Date(this.authService.getAccessTokenExpiration()).getTime() - new Date().getTime();
      this.authService.autoLogout(expirationDuration);
      if (!environment.production) {
        const cedula = this.pacienteGlobalService.getLastPacienteFromLocalStorage();
        if (cedula) {
          this.pacienteService.getPacienteByNumeroArchivo(cedula)
            .subscribe((paciente) => {
              if (paciente) {
                this.pacienteGlobalService.setPacienteGlobal(paciente);
              }
            });
        }
      }
    }
  }
}
