import {Component, EventEmitter, ViewEncapsulation, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';
import {debounceTime} from 'rxjs/operators';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {Observable} from 'rxjs';
import {Dispensario} from 'src/app/core/models/catalogo/dispensario.model';

@UntilDestroy()
@Component({
  selector: 'vex-pick-filtro',
  templateUrl: './pick-filtro.component.html',
  styleUrls: ['./pick-filtro.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class PickFiltroComponent implements OnInit {

  @Output() searchEvent = new EventEmitter<Dispensario>();
  dispensarios: Observable<Dispensario[]>;
  icFilterList = icFilterList;
  searchCtrl = new FormControl();

  constructor(
    private catalogoService: CatalogoService
  ) { }

  ngOnInit(): void {
    this.dispensarios = this.catalogoService.getDispensarios();
    this.searchCtrl.valueChanges.pipe(
      debounceTime(400),
      untilDestroyed(this)
    ).subscribe(search => this.search(search));
  }

  search(value: Dispensario) {
    this.searchEvent.emit(value);
  }
}
