import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {IconModule} from '@visurel/iconify-angular';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {PickFiltroComponent} from 'src/app/shared/components/pick-filtro/pick-filtro.component';
import {MatSelectModule} from "@angular/material/select";
import {MatTooltipModule} from '@angular/material/tooltip';



@NgModule({
  declarations: [PickFiltroComponent],
  exports: [
    PickFiltroComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    IconModule,
    MatDatepickerModule,
    MatInputModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatSelectModule,
    MatTooltipModule
  ]
})
export class PickFiltroModule { }
