import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { PickYearComponent } from './pick-year.component';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {IconModule} from '@visurel/iconify-angular';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';

@NgModule({
  declarations: [PickYearComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    IconModule,
    MatDatepickerModule,
    MatInputModule,
    ReactiveFormsModule,
    FlexLayoutModule
  ],
  exports: [
    PickYearComponent
  ]
})
export class PickYearModule { }
