import {Component, EventEmitter, OnInit, Output} from '@angular/core';

import icSearch from '@iconify/icons-ic/twotone-search';
import {FormControl} from '@angular/forms';
import {debounceTime} from 'rxjs/operators';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'vex-pick-year',
  templateUrl: './pick-year.component.html',
  styleUrls: ['./pick-year.component.scss']
})
export class PickYearComponent implements OnInit {
  @Output() searchEvent = new EventEmitter<string>();

  icSearch = icSearch;
  searchCtrl = new FormControl();

  constructor() { }

  ngOnInit(): void {
    this.searchCtrl.valueChanges.pipe(
      debounceTime(400),
      untilDestroyed(this)
    ).subscribe(search => this.search(search));
  }

  search(value: string) {
    if (value.trim().length === 4 || value.trim().length === 0) {
      this.searchEvent.emit(value);
    }
  }
}
