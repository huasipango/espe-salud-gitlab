import {Directive, forwardRef, Injectable} from '@angular/core';
import {AbstractControl, AsyncValidator, NG_ASYNC_VALIDATORS, ValidationErrors} from '@angular/forms';
import {Observable, timer} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import {SaludUserService} from 'src/app/core/services/salud-user.service';

@Injectable({providedIn: 'root'})
export class UniqueUsernameValidator implements AsyncValidator {

  constructor(private usuarioService: SaludUserService) {
  }

  validate(ctrl: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    return timer(800).pipe(
      switchMap(() => {
        if (!ctrl.value) {
          return null;
        }
        return this.usuarioService.checkNumeroUsernameNotExists(ctrl.value).pipe(
          map(isTaken => (isTaken ? {uniqueUsername: true} : null)),
          catchError(() => null)
        );
      })
    );
  }
}

@Directive({
  selector: '[vexUniqueUsername]',
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: forwardRef(() => UniqueUsernameValidator),
      multi: true
    }
  ]
})
export class UniqueUsernameDirective {

  constructor(private validator: UniqueUsernameValidator) { }

  validate(control: AbstractControl) {
    this.validator.validate(control);
  }
}
