import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UniqueUsernameDirective } from './unique-username.directive';



@NgModule({
  declarations: [UniqueUsernameDirective],
  imports: [
    CommonModule
  ]
})
export class UniqueUsernameModule { }
