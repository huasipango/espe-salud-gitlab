import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OnlyNumbersDirective } from 'src/app/shared/directives/only-numbers/only-numbers.directive';



@NgModule({
  declarations: [OnlyNumbersDirective],
  exports: [
    OnlyNumbersDirective
  ],
  imports: [
    CommonModule
  ]
})
export class OnlyNumbersModule { }
