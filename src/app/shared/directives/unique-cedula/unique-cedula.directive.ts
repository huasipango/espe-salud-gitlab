import {Directive, forwardRef, Injectable} from '@angular/core';
import {AbstractControl, AsyncValidator, NG_ASYNC_VALIDATORS, ValidationErrors} from '@angular/forms';
import {Observable, timer} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import {PacienteService} from 'src/app/core/services/paciente/paciente.service';

@Injectable({providedIn: 'root'})
export class UniqueNumeroArchivoValidator implements AsyncValidator {

  constructor(private pacienteService: PacienteService) {
  }

  validate(ctrl: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    return timer(800).pipe(
      switchMap(() => {
        if (!ctrl.value) {
          return null;
        }
        return this.pacienteService.checkNumeroArchivoNotExists(ctrl.value).pipe(
          map(isTaken => (isTaken ? {uniqueNumeroArchivo: true} : null)),
          catchError(() => null)
        );
      })
    );
  }
}

@Directive({
  selector: '[vexAsyncNumeroArchivoExist]',
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: forwardRef(() => UniqueNumeroArchivoValidator),
      multi: true
    }
  ]
})

export class AsyncNumeroArchivoExistDirective {
  constructor(private validator: UniqueNumeroArchivoValidator) {
  }

  validate(control: AbstractControl) {
    this.validator.validate(control);
  }

}
