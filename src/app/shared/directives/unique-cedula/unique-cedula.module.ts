import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AsyncNumeroArchivoExistDirective} from './unique-cedula.directive';

@NgModule({
  declarations: [AsyncNumeroArchivoExistDirective],
  imports: [
    CommonModule
  ]
})
export class UniqueCedulaModule { }
