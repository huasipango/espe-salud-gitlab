import {Directive, ElementRef, EventEmitter, HostListener, Output} from '@angular/core';
import {NgControl} from '@angular/forms';

@Directive({
  selector: '[formControlName][vexInputUppercase]'
})
export class UppercaseDirective {
  constructor(
    private readonly control: NgControl
  ) { }

  @HostListener('input', ['$event'])
  public onInput(event): void {
    this.control.control.setValue(event.target.value.toUpperCase());
  }
}
