import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

export function forbiddenObjectValidator(control: AbstractControl): { forbiddenObject: boolean } {
  return typeof control.value !== 'object' || control.value === null ? { forbiddenObject: true } : null;
}
