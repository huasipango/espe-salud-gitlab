import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeleteModalComponent } from './components/delete-modal/delete-modal.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { IconModule } from '@visurel/iconify-angular';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { HighlightModule } from 'src/@vex/components/highlight/highlight.module';
import { ErrorDialogComponent } from './components/error-dialog/error-dialog.component';
import {MatDividerModule} from '@angular/material/divider';
import { AlertModalComponent } from './components/alert-modal/alert-modal.component';

@NgModule({
  declarations: [DeleteModalComponent, ErrorDialogComponent, AlertModalComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatDialogModule,
    HighlightModule,
    MatButtonModule,
    MatIconModule,
    IconModule,
    MatDividerModule
  ],
  entryComponents: [DeleteModalComponent]
})
export class SharedModule { }
