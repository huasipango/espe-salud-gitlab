import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DosisService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getCountDosis(idDosisVacuna: number): Observable<number>{
    const url = `${this.baseUrl}dosis/idVacuna?id=${idDosisVacuna}`;
    return this.http.get<number>(url);
  }
}
