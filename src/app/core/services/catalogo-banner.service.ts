import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Parentesco} from 'src/app/core/models/catalogo-banner/parentesco.model';
import {Religion} from 'src/app/core/models/catalogo-banner/religion.model';
import {Departamento} from 'src/app/core/models/catalogo-banner/departamento.model';
import {EstadoCivil} from 'src/app/core/models/catalogo-banner/estado-civil.model';
import {Genero} from 'src/app/core/models/catalogo-banner/genero.model';
import {NivelEstudio} from 'src/app/core/models/catalogo-banner/nivel-estudio.model';
import {Unidad} from 'src/app/core/models/catalogo-banner/unidad.model';
import {Canton} from 'src/app/core/models/catalogo-banner/canton.model';
import {Pais} from 'src/app/core/models/catalogo-banner/pais.model';
import {Provincia} from 'src/app/core/models/catalogo-banner/provincia,model';
import {Etnia} from 'src/app/core/models/catalogo-banner/etnia.model';
import {Nacionalidad} from 'src/app/core/models/catalogo-banner/nacionalidad.model';
import {Campus} from 'src/app/core/models/catalogo-banner/campus.model';
import {TipoSangre} from 'src/app/core/models/catalogo-banner/tipo-sangre.model';
import {Parroquia} from 'src/app/core/models/catalogo-banner/parroquia.model';
import {DiscapacidadBanner} from 'src/app/core/models/catalogo-banner/tipo-discapacidad.model';

@Injectable({
  providedIn: 'root'
})
export class CatalogoBannerService {
  private baseUrl: string = environment.catalogoWebServiceUrl;

  constructor(private http: HttpClient) { }

  getReligiones(): Observable<Religion[]> {
    return this.http.get<Religion[]>(this.baseUrl + 'religiones');
  }

  getParentescos(): Observable<Parentesco[]> {
    return this.http.get<Parentesco[]>(this.baseUrl + 'parentescos');
  }

  getCantones(): Observable<Canton[]> {
    return this.http.get<Canton[]>(this.baseUrl + 'cantones');
  }

  getCanton(idCanton: string): Observable<Canton[]> {
    return this.http.get<Canton[]>(this.baseUrl + 'cantones/' + idCanton);
  }

  getCantonesByProvincia(idProvincia: number): Observable<Canton[]> {
    return this.http.get<Canton[]>(this.baseUrl + 'cantones/provincia?idProvincia=' + idProvincia);
  }

  getParroquiasByCanton(idCanton: string): Observable<Parroquia[]> {
    return this.http.get<Parroquia[]>(this.baseUrl + 'parroquias/canton?idCanton=' + idCanton);
  }

  getDepartamentos(): Observable<Departamento[]> {
    return this.http.get<Departamento[]>(this.baseUrl + 'departamentos');
  }

  getEstadosCiviles(): Observable<EstadoCivil[]> {
    return this.http.get<EstadoCivil[]>(this.baseUrl + 'estados-civiles');
  }

  getGeneros(): Observable<Genero[]> {
    return this.http.get<Genero[]>(this.baseUrl + 'generos');
  }

  getPaises(): Observable<Pais[]> {
    return this.http.get<Pais[]>(this.baseUrl + 'paises');
  }

  getProvincias() {
    return this.http.get<Provincia[]>(this.baseUrl + 'provincias');
  }

  getProvincia(idProvincia: number): Observable<Provincia[]> {
    return this.http.get<Provincia[]>(this.baseUrl + 'provincias/' + idProvincia);
  }

  getTiposDiscapacidad(): Observable<DiscapacidadBanner[]> {
    return this.http.get<DiscapacidadBanner[]>(this.baseUrl + 'discapacidades');
  }

  getNivelesEstudio(): Observable<NivelEstudio[]> {
    return this.http.get<NivelEstudio[]>(this.baseUrl + 'niveles-estudio');
  }

  getUnidades(): Observable<Unidad[]> {
    return this.http.get<Unidad[]>(this.baseUrl + 'unidades');
  }

  // getEtnias(): Observable<Etnia[]> {
  //   return this.http.get<Etnia[]>(this.baseUrl + 'etnias');
  // }

  getNacionalidades(): Observable<Nacionalidad[]> {
    return this.http.get<Nacionalidad[]>(this.baseUrl + 'nacionalidades');
  }

  getCampus(): Observable<Campus[]> {
    return this.http.get<Campus[]>(this.baseUrl + 'campus');
  }

  getTiposSangre(): Observable<TipoSangre[]> {
    return this.http.get<TipoSangre[]>(this.baseUrl + 'tipos-sangre');
  }
}
