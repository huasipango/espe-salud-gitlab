import { Injectable } from '@angular/core';
import {HttpClient, HttpEvent, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {UserInfo} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/user-info.interface';
import {FullUserInfo} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/full-user-info.interface';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';


const URL = 'https://miespemovil.espe.edu.ec/reportes/reporteWs';
const API_URL = URL + '/usuarioByIdCard/';
const API_URL_COM = URL + '/usuarioFacturaByIdCard/';
const CED_URL = URL + '/cedulaById/';
const USER = URL + '/username/';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) { }

  getUserByIdCard(idCard: string): Observable<any> {
    return this.http.get(API_URL + idCard);
  }

  getUserByUsername(username: string): Observable<UserInfo[]> {
    return this.http.get<UserInfo[]>(USER + username);
  }

  getFullUserDataByIdCard(idCard: string): Observable<FullUserInfo> {
    return this.http.get<FullUserInfo>(API_URL_COM + idCard)
      .pipe(
        map((response: FullUserInfo) => response[0])
      );
  }
  getUsuarioBannerInfo(nombreUsuario: string): Observable<SaludUser[]> {
    return this.http.get<SaludUser[]>(USER + nombreUsuario);
  }
}
