import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Paciente} from '../models/paciente/paciente.model';

@Injectable({
  providedIn: 'root'
})
export class PacienteGlobalService {

  private ultimoPacienteKey = 'ultimoPaciente';

  private subject: BehaviorSubject<Paciente> =
    new BehaviorSubject<Paciente>(null);
  public pacienteGlobal$: Observable<Paciente> = this.subject.asObservable();

  savePacienteInstance(pacienteSeleccionado) {
    const paciente = localStorage.setItem('pacienteActual', JSON.stringify(pacienteSeleccionado));
  }

  getActualPaciente() {
    return JSON.parse(localStorage.getItem('pacienteActual'));
  }

  public setPacienteGlobal(paciente: Paciente): void {
    this.subject.next(paciente);
  }

  setLastPacienteToLocalStorage(cedula: string) {
    localStorage.setItem(this.ultimoPacienteKey, btoa(cedula));
  }

  getLastPacienteFromLocalStorage(): string {
    const cedula = localStorage.getItem(this.ultimoPacienteKey);
    return cedula ? atob(cedula) : '';
  }
}
