import {Injectable} from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {DashboardParams} from 'src/app/core/models/dashboard/dashboard-params.model';

@Injectable({
  providedIn: 'root'
})
export class ReportDashboardService {

  private baseUrl: string = environment.baseUrl + 'reportes/';
  constructor(private http: HttpClient) {
  }

  getReport(slugReport: string, params: DashboardParams) {
    const url = `${this.baseUrl}${slugReport}`;
    let queryString = '';
    for (const p of Object.keys(params)) {
      queryString += params[p] ? `&${p}=${params[p]}` : ``;
    }
    return this.getFileFromResponse(queryString, url);
  }

  getConsentimientoInformadoAceptado(
    nombres: string,
    apellidos: string,
    identificacion: string,
    pidmUsuario: number) {
    const url = `${this.baseUrl}pdf/consentimiento-informado-aceptado`;
    const params = {
      'nombres': nombres,
      'apellidos': apellidos,
      'identificacion': identificacion,
      'pidmUsuario': pidmUsuario
    };
    let queryString = '';
    for (const p of Object.keys(params)) {
      queryString += params[p] ? `&${p}=${params[p]}` : ``;
    }
    return this.getFileFromResponse(queryString, url);
  }

  getConsentimientoInformadoNegado(pidmUsuario: number) {
    const url = `${this.baseUrl}pdf/consentimiento-informado-negado`;
    const params = {'pidmUsuario': pidmUsuario};
    let queryString = '';
    for (const p of Object.keys(params)) {
      queryString += params[p] ? `&${p}=${params[p]}` : ``;
    }
    return this.getFileFromResponse(queryString, url);
  }

  getReportePorDispensario(parametro: DashboardParams) {
    const url = `${this.baseUrl}dispensario`;
    let queryString = '';
    for (const p of Object.keys(parametro)) {
      queryString += parametro[p] ? `&${p}=${parametro[p]}` : ``;
    }
    return this.getFileFromResponse(queryString, url);
  }


  getReportePorSeguroSalud(parametro: DashboardParams) {
    const url = `${this.baseUrl}seguro-salud`;
    let queryString = '';
    for (const p of Object.keys(parametro)) {
      queryString += parametro[p] ? `&${p}=${parametro[p]}` : ``;
    }
    return this.getFileFromResponse(queryString, url);
  }

  getReportePorRelacionEspe(parametro: DashboardParams) {
    const url = `${this.baseUrl}relaciones-espe`;
    let queryString = '';
    for (const p of Object.keys(parametro)) {
      queryString += parametro[p] ? `&${p}=${parametro[p]}` : ``;
    }
    return this.getFileFromResponse(queryString, url);
  }


  getReportePorTipoConsulta(parametro: DashboardParams) {
    const url = `${this.baseUrl}tipo-consulta`;
    let queryString = '';
    for (const p of Object.keys(parametro)) {
      queryString += parametro[p] ? `&${p}=${parametro[p]}` : ``;
    }
    return this.getFileFromResponse(queryString, url);
  }

  getReportePorSexo(parametro: DashboardParams) {
    const url = `${this.baseUrl}sexo-paciente`;
    let queryString = '';
    for (const p of Object.keys(parametro)) {
      queryString += parametro[p] ? `&${p}=${parametro[p]}` : ``;
    }
    return this.getFileFromResponse(queryString, url);
  }

  getReportePorMotivoAtencion(parametro: DashboardParams) {
    const url = `${this.baseUrl}motivo-atencion`;
    let queryString = '';
    for (const p of Object.keys(parametro)) {
      queryString += parametro[p] ? `&${p}=${parametro[p]}` : ``;
    }
    return this.getFileFromResponse(queryString, url);
  }

  getReportePorPrevencionVsMorbilidad(parametro: DashboardParams) {
    const url = `${this.baseUrl}prevencion-morbilidad`;
    let queryString = '';
    for (const p of Object.keys(parametro)) {
      queryString += parametro[p] ? `&${p}=${parametro[p]}` : ``;
    }
    return this.getFileFromResponse(queryString, url);
  }

  getReportePorMorbilidad(parametro: DashboardParams) {
    const url = `${this.baseUrl}morbilidad`;
    let queryString = '';
    for (const p of Object.keys(parametro)) {
      queryString += parametro[p] ? `&${p}=${parametro[p]}` : ``;
    }
    return this.getFileFromResponse(queryString, url);
  }

  getReportePorPrevencion(parametro: DashboardParams) {
    const url = `${this.baseUrl}prevencion`;
    let queryString = '';
    for (const p of Object.keys(parametro)) {
      queryString += parametro[p] ? `&${p}=${parametro[p]}` : ``;
    }
    return this.getFileFromResponse(queryString, url);
  }

  getReportePorCIE10(parametro: DashboardParams) {
    const url = `${this.baseUrl}codigo-cie10`;
    let queryString = '';
    for (const p of Object.keys(parametro)) {
      queryString += parametro[p] ? `&${p}=${parametro[p]}` : ``;
    }
    return this.getFileFromResponse(queryString, url);
  }

  getFileFromResponse(queryString: string, url: string) {
    return this.http.get(
      `${url}${
        queryString ? `?${queryString}` : ``
      }`,
      {observe: 'response', responseType: 'blob'}
    ).pipe(
      map((response) => {
        const type = response.body.type;
        const a = document.createElement('a');
        const generatedUrl = URL.createObjectURL(
          new Blob([response.body], { type })
        );
        a.href = generatedUrl;
        a.download = response.headers
          .get('content-disposition')
          .split('filename=')[1];
        document.body.appendChild(a);
        a.click();
        setTimeout(() => {
          document.body.removeChild(a);
          window.URL.revokeObjectURL(generatedUrl);
        }, 0);
        return response;
      })
    );
  }
}
