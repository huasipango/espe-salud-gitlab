import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {Observable} from 'rxjs';
import {PersonaBanner} from 'src/app/core/models/banner/persona-banner-model';

@Injectable({
  providedIn: 'root'
})
export class BannerService {
  private baseUrl: string = environment.bannerWebServiceUrl;
  private token: string = environment.bannerWebServiceToken;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Allow: 'GET, POST, HEAD',
      Authorization: this.token
    })
  };
  constructor(private http: HttpClient) {
  }

  getPersonaBannerInfo(cedula: string): Observable<PersonaBanner[]> {
    return this.http.get<PersonaBanner[]>(this.baseUrl + cedula, this.httpOptions);
  }
}
