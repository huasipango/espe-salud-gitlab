import {Injectable} from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {TipoExamenLaboratorio} from 'src/app/core/models/catalogo/tipo-examen-laboratorio.model';
import {TipoEnfermedadPersonal} from '../models/catalogo/tipo-enfermedad-personal.model';
import {TipoEnfermedadFamiliar} from '../models/catalogo/tipo-enfermedad-familiar.model';
import {Region} from '../../modules/medicina-general/evolucion/models/region.model';
import {Area} from '../../modules/medicina-general/evolucion/models/area.model';
import {OrganoSistema} from '../../modules/medicina-general/evolucion/models/organo-sistema';
import {TipoPlanificacionFamiliar} from 'src/app/core/models/catalogo/tipo-planificacion-familial.model';
import {TipoExamenSexual} from 'src/app/core/models/catalogo/tipo-examen-sexual.model';
import {CodigoCiuo} from 'src/app/core/models/catalogo/codigo-ciuo.model';
import {TipoEnfermedadCIE10} from '../models/catalogo/tipo-enfermedad-cie10.model';
import {TipoConsumoNocivo} from 'src/app/core/models/catalogo/tipo-consumo-nocivo.model';
import {TipoProcedimiento} from 'src/app/core/models/catalogo/tipo-procedimiento.model';
import {MotivoAtencion} from 'src/app/core/models/catalogo/motivo-atencion.model';
import { TipoActividadEnfermeria } from '../models/catalogo/tipo-actividad-enfermeria';
import {TipoEstudioComplementario} from 'src/app/core/models/catalogo/tipo-estudio-complementario.model';
import {Dispensario} from 'src/app/core/models/catalogo/dispensario.model';
import {SeguroSalud} from 'src/app/core/models/catalogo/seguro-salud.model';
import {Asociacion} from 'src/app/core/models/catalogo/asociacion.model';
import {NivelInstruccion} from 'src/app/core/models/catalogo/nivel-instruccion.model';
import {OrientacionSexual} from 'src/app/core/models/catalogo/orientacion-sexual.model';
import {IdentidadGenero} from 'src/app/core/models/catalogo/identidad-genero.model';
import {FrecuenciaAlimentacion} from 'src/app/core/models/catalogo/frecuencia-alimentacion.model';
import {RelacionEspe} from 'src/app/core/models/catalogo/relacion-espe.model';
import {Plurinacionalidad} from 'src/app/core/models/catalogo/plurinacionalidad.model';
import {DescripcionActividadEnfermeria} from '../models/catalogo/descripcion-actividad-enfermeria';
import {FrecuenciaMedicacionHabitual} from 'src/app/core/models/catalogo/frecuencia-medicacion-habitual.model';
import {Parentesco} from 'src/app/core/models/catalogo/parentesco.model';
import {TipoDiscapacidad} from 'src/app/core/models/catalogo/tipo-discapacidad.model';
import {TipoVacuna} from 'src/app/core/models/catalogo/tipo-vacunas.model';
import {Dosis} from 'src/app/core/models/catalogo/dosis.model';
import {AbstractControl} from '@angular/forms';
import {TipoIdentificacion} from 'src/app/core/models/catalogo/tipo-identificacion.model';
import {GrupoPrioritario} from 'src/app/core/models/catalogo/grupo-prioritario.model';
import {List} from 'postcss/lib/list';
import {Etnia} from 'src/app/core/models/catalogo-banner/etnia.model';
import {FormacionProfesional} from 'src/app/core/models/catalogo/formacion-profesional.model';
import {EspecialidadProfesional} from 'src/app/core/models/catalogo/especialidad-profesional.model';
import {Pueblo} from 'src/app/core/models/catalogo/pueblo.model';
import {ParroquiaSalud} from 'src/app/core/models/catalogo/parroquia-salud.model';
import {LugarAtencion} from 'src/app/core/models/catalogo/lugar-atencion.model';
import {TipoDetalleExamenLaboratorio} from 'src/app/core/models/catalogo/tipo-detalle-examen-laboratorio.model';


@Injectable({
  providedIn: 'root'
})
export class CatalogoService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getTiposExamenLaboratorio(): Observable<TipoExamenLaboratorio[]> {
    const url = this.baseUrl + 'tipos-examenes-laboratorio';
    return this.http.get<TipoExamenLaboratorio[]>(url);
  }

  getTipoDetalleExameneLaboratorio(idTipoExamen: number): Observable<TipoDetalleExamenLaboratorio[]> {
    const url = this.baseUrl + 'tipos-detalles-laboratorio/tipo-examen?idTipoExamen=' + idTipoExamen;
    return this.http.get<TipoDetalleExamenLaboratorio[]>(url);
  }

  getAllTiposExamenLaboratorio(): Observable<TipoDetalleExamenLaboratorio[]> {
    const url = this.baseUrl + 'tipos-detalles-laboratorio';
    return this.http.get<TipoDetalleExamenLaboratorio[]>(url);
  }

  getDescripcionesByTipo(id: number): Observable<DescripcionActividadEnfermeria[]>{
    const url = this.baseUrl + 'descripciones-actividad-enfermeria/tipoActividad?idTipoActividad=' + id;
    return this.http.get<DescripcionActividadEnfermeria[]>(url);
  }

  getEstudiosComplementario(): Observable<TipoEstudioComplementario[]> {
    return this.http.get<TipoEstudioComplementario[]>(this.baseUrl + 'tipos-estudio-complementario');
  }

  getDispensario(id: number): Observable<Dispensario> {
    const url = this.baseUrl + 'dispensarios/';
    return this.http.get<Dispensario>(url + id);
  }

  getDispensarioImagen(nombre: string): string {
    return this.baseUrl + 'dispensarios/images/' + nombre;
  }

  getDispensarios(): Observable<Dispensario[]> {
    const url = this.baseUrl;
    return this.http.get<Dispensario[]>(url + 'dispensarios');
  }

  getRegiones(): Observable<any> {
    return this.http.get<Region[]>(this.baseUrl + 'regiones');
  }

  getAreasByRegion(idRegion: number): Observable<any> {
    return this.http.get<Area[]>(this.baseUrl + 'areas/search/region?idRegion=' + idRegion);
  }

  getOrganoSistema(): Observable<any> {
    return this.http.get<OrganoSistema[]>(this.baseUrl + 'organos-sistema');
  }

  getTiposEnfermedadPersonal(): Observable<TipoEnfermedadPersonal[]> {
    const url = this.baseUrl;
    return this.http.get<TipoEnfermedadPersonal[]>(url + 'tipos-enfermedad-personal');
  }

  getTiposEnfermedadFamiliar(): Observable<TipoEnfermedadFamiliar[]> {
    const url = this.baseUrl;
    return this.http.get<TipoEnfermedadFamiliar[]>(url + 'tipos-enfermedades');
  }

  getSegurosSalud(): Observable<SeguroSalud[]> {
    const url = this.baseUrl + 'seguros-salud';
    return this.http.get<SeguroSalud[]>(url);
  }

  getParentesco(): Observable<Parentesco[]> {
    const url = this.baseUrl + 'parentescos';
    return this.http.get<Parentesco[]>(url);
  }

  getAsociaciones(): Observable<Asociacion[]> {
    const url = this.baseUrl + 'asociaciones';
    return this.http.get<Asociacion[]>(url);
  }

  createAsociacion(data: any): Observable<Asociacion> {
    const url = this.baseUrl + 'asociaciones/';
    return this.http.post<Asociacion>(url, JSON.stringify(data));
  }

  updateAsociacion(data: any, id: number): Observable<Asociacion> {
    const url = this.baseUrl + 'asociaciones/' + id;
    return this.http.put<Asociacion>(url, JSON.stringify(data));
  }

  deleteAsociacion(id: number): Observable<boolean> {
    const url = this.baseUrl + 'asociaciones/' + id;
    return this.http.delete<boolean>(url);
  }

  getCodigosCiuoByQuery(query: string): Observable<CodigoCiuo[]> {
    const url = this.baseUrl + 'codigos-ciuo/findByCodeOrDescription';
    return this.http.get<CodigoCiuo[]>(url + '?query=' + query);
  }

  getCodigosCIE10ByQuery(query: string): Observable<TipoEnfermedadCIE10[]> {
    const url = this.baseUrl + 'tipos-enfermedad-cie10/findByCodigoOrNombre';
    return this.http.get<TipoEnfermedadCIE10[]>(url + '?query=' + query);
  }

  getCodigoCIE10(codigo: string): Observable<TipoEnfermedadCIE10> {
    const url = this.baseUrl + 'tipos-enfermedad-cie10/';
    return this.http.get<TipoEnfermedadCIE10>(url + codigo);
  }

  getCodigosCiuo(): Observable<CodigoCiuo[]> {
    const url = this.baseUrl + 'codigos-ciuo';
    return this.http.get<CodigoCiuo[]>(url);
  }

  getTiposConsumoNocivo(): Observable<TipoConsumoNocivo[]> {
    const url = this.baseUrl + 'tipos-consumo-nocivo';
    return this.http.get<TipoConsumoNocivo[]>(url);
  }

  createConsumoNocivo(data: TipoConsumoNocivo): Observable<TipoConsumoNocivo> {
    const url = this.baseUrl + 'tipos-consumo-nocivo/';
    return this.http.post<TipoConsumoNocivo>(url, JSON.stringify(data));
  }

  getTiposExamenesSexualesHombres(): Observable<TipoExamenSexual[]> {
    const url = this.baseUrl + 'tipos-examenes-sexuales/findAllHombres';
    return this.http.get<TipoExamenSexual[]>(url);
  }

  getTipoExamenesSexualesMujeres(): Observable<TipoExamenSexual[]> {
    const url = this.baseUrl + 'tipos-examenes-sexuales/findAllMujeres';
    return this.http.get<TipoExamenSexual[]>(url);
  }

  getTiposPlanificacionHombres(): Observable<TipoPlanificacionFamiliar[]> {
    const url = this.baseUrl + 'tipos-planificacion-familiar/findAllHombres';
    return this.http.get<TipoPlanificacionFamiliar[]>(url);
  }

  getTiposPlanificacionMujeres(): Observable<TipoPlanificacionFamiliar[]> {
    const url = this.baseUrl + 'tipos-planificacion-familiar/findAllMujeres';
    return this.http.get<TipoPlanificacionFamiliar[]>(url);
  }

  getTiposProcedimiento(): Observable<TipoProcedimiento[]> {
    const url = this.baseUrl + 'tipos-procedimiento';
    return this.http.get<TipoProcedimiento[]>(url);
  }

  getMotivosAtencion(): Observable<MotivoAtencion[]> {
    return this.http.get<MotivoAtencion[]>(this.baseUrl + 'motivos-atencion');
  }

  getTipoActividad(): Observable<TipoActividadEnfermeria[]>{
    const url = this.baseUrl + 'tipos-actividad-enfermeria';
    return this.http.get<TipoActividadEnfermeria[]>(url);
  }

  getNivelesInstruccion(): Observable<NivelInstruccion[]> {
    return this.http.get<NivelInstruccion[]>(this.baseUrl + 'niveles-instruccion');
  }

  getOrientacionesSexuales(): Observable<OrientacionSexual[]> {
    return this.http.get<OrientacionSexual[]>(this.baseUrl + 'orientaciones-sexuales');
  }

  getIdentidadesGenero(): Observable<IdentidadGenero[]> {
    return this.http.get<IdentidadGenero[]>(this.baseUrl + 'identidades-genero');
  }

  getFrecuenciasAlimentacion(): Observable<FrecuenciaAlimentacion[]> {
    return this.http.get<FrecuenciaAlimentacion[]>(this.baseUrl + 'frecuencias-alimentacion');
  }

  getRelacionesEspe(): Observable<RelacionEspe[]> {
    return this.http.get<RelacionEspe[]>(this.baseUrl + 'relaciones-espe');
  }

  getPlurinacionalidades(): Observable<Plurinacionalidad[]> {
    return this.http.get<Plurinacionalidad[]>(this.baseUrl + 'plurinacionalidades');
  }

  getEtnias(): Observable<Etnia[]> {
    const url = `${this.baseUrl}etnias`;
    return this.http.get<Etnia[]>(url);
  }

  getFormacionesProfesionales(): Observable<FormacionProfesional[]> {
    const url = `${this.baseUrl}formacion-de-profesionales`;
    return this.http.get<FormacionProfesional[]>(url);
  }

  getFormacionProfesionalById(idFormacion: number): Observable<FormacionProfesional> {
    const url = `${this.baseUrl}formacion-de-profesionales?id=${idFormacion}`;
    return this.http.get<FormacionProfesional>(url);
  }

  getEspecialidadesProfesionalesByFormacion(idFormacionProfesional: number): Observable<EspecialidadProfesional[]> {
    const url = `${this.baseUrl}especialidad-de-profesionales/formacionProfesional?id=${idFormacionProfesional}`;
    return this.http.get<EspecialidadProfesional[]>(url);
  }

  getEspecialidadesProfesionales(): Observable<EspecialidadProfesional[]> {
    const url = `${this.baseUrl}especialidad-de-profesionales`;
    return this.http.get<EspecialidadProfesional[]>(url);
  }

  getNacionalidadesByEtnia(idEtnia: number): Observable<Plurinacionalidad[]> {
    const url = `${this.baseUrl}nacionalidades/etnia?id=${idEtnia}`;
    return this.http.get<Plurinacionalidad[]>(url);
  }

  getPueblosByNacionalidad(idNacionalidad: number): Observable<Pueblo[]> {
    const url = `${this.baseUrl}pueblos/nacionalidad?id=${idNacionalidad}`;
    return this.http.get<Pueblo[]>(url);
  }

  getParroquiasByCanton(idCanton: string): Observable<ParroquiaSalud[]> {
    const url = `${this.baseUrl}parroquias/canton?idCanton=${idCanton}`;
    return this.http.get<ParroquiaSalud[]>(url);
  }

  getFrecuenciasMedicacionHabitual(): Observable<FrecuenciaMedicacionHabitual[]> {
    return this.http.get<FrecuenciaMedicacionHabitual[]>(this.baseUrl + 'frecuencias-medicacion-habitual');
  }

  getTiposDiscapacidad(): Observable<TipoDiscapacidad[]> {
    return this.http.get<TipoDiscapacidad[]>(this.baseUrl + 'tipos-discapacidad');
  }

  getAreas(): Observable<Area[]> {
    return this.http.get<Area[]>(this.baseUrl + 'areas');
  }

  getTipoVacuna(): Observable<TipoVacuna[]> {
    return this.http.get<TipoVacuna[]>(this.baseUrl + 'tipos-vacunas');
  }

  getDosis(): Observable<Dosis[]> {
    return this.http.get<Dosis[]>(this.baseUrl + 'dosis');
  }
  getDosisById(id: number): Observable<Dosis> {
    return this.http.get<Dosis>(this.baseUrl + 'dosis/' + id);
  }

  getDosisByIdTipoVacuna(id: number): Observable<Dosis[]> {
    return this.http.get<Dosis[]>(this.baseUrl + 'dosis/tipoVacuna?id=' + id);
  }

  getTipoVacunaById(id: AbstractControl): Observable<TipoVacuna> {
    return this.http.get<TipoVacuna>(this.baseUrl + 'tipos-vacunas/' + id);
  }

  createDispensario(data: any): Observable<Asociacion> {
    const url = this.baseUrl + 'dispensarios/';
    return this.http.post<Asociacion>(url, JSON.stringify(data));
  }

  updateDispensario(data: any, id: number): Observable<Asociacion> {
    const url = this.baseUrl + 'dispensarios/' + id;
    return this.http.put<Asociacion>(url, JSON.stringify(data));
  }

  deleteDispensario(id: number): Observable<boolean> {
    const url = this.baseUrl + 'dispensarios/' + id;
    return this.http.delete<boolean>(url);
  }

  getDispensarioImage(nombreImagen: string): string {
    return this.baseUrl + 'dispensarios/images/' + nombreImagen;
  }

  getDispensarioNmesImage(): any {
    return this.http.get<string[]>(this.baseUrl + 'dispensarios/namesImage');
  }

  getTiposIdentificacion(): Observable<TipoIdentificacion[]> {
    const url = this.baseUrl + 'tipos-identificacion';
    return this.http.get<TipoIdentificacion[]>(url);
  }

  getGruposPrioritarios(): Observable<GrupoPrioritario[]> {
    const url = this.baseUrl + 'grupos-prioritarios';
    return this.http.get<GrupoPrioritario[]>(url);
  }

  createSeguroSalud(data: any): Observable<Asociacion> {
    const url = this.baseUrl + 'seguros-salud/';
    return this.http.post<Asociacion>(url, JSON.stringify(data));
  }

  updateSeguroSalud(data: any, id: number): Observable<Asociacion> {
    const url = this.baseUrl + 'seguros-salud/' + id;
    return this.http.put<Asociacion>(url, JSON.stringify(data));
  }

  deleteSeguroSalud(id: number): Observable<boolean> {
    const url = this.baseUrl + 'seguros-salud/' + id;
    return this.http.delete<boolean>(url);
  }

  getLugaresAtencion(): Observable<LugarAtencion[]> {
    const url = this.baseUrl + 'lugares-atencion';
    return this.http.get<LugarAtencion[]>(url);
  }
}
