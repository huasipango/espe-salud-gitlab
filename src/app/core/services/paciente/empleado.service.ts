import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Empleado} from '../../models/empleado/empleado.model';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {
  baseUrl: string = environment.baseUrl;
  commonUrl: string;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Allow: 'GET, POST, HEAD'
    })
  };

  constructor(private http: HttpClient) {
    this.commonUrl = this.baseUrl + 'pacientes/'
  }

  // POST
  createEmpleado(data: Empleado, idPaciente: number): Observable<Empleado>{
    return this.http.post<Empleado>(
      this.commonUrl + idPaciente + '/empleados/', JSON.stringify(data), this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }

  // UPDATE
  updateEmpleado(data: Empleado, id: number): Observable<Empleado>{
    return this.http.put<Empleado>(
      this.commonUrl + id + '/empleados/' + id, JSON.stringify(data), this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }

  private handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
