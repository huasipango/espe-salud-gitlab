import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ContactoEmergencia} from '../../models/paciente/contacto-emergencia.model';

@Injectable({
  providedIn: 'root'
})
export class ContactoEmergenciaService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getContactosEmergenciaByPaciente(idPaciente: number): Observable<ContactoEmergencia[]> {
    return this.http.get<ContactoEmergencia[]>(this.baseUrl + 'contactos-emergencia?idPaciente' + idPaciente);
  }

  createContactoEmergencia(data: ContactoEmergencia, idPaciente: number): Observable<ContactoEmergencia>{
    return this.http.post<ContactoEmergencia>(this.baseUrl + 'contactos-emergencia/?idPaciente=' + idPaciente, JSON.stringify(data));
  }

  updateContactoEmergencia(data: ContactoEmergencia, id: number, idPaciente: number): Observable<ContactoEmergencia>{
    return this.http.put<ContactoEmergencia>(
      this.baseUrl + 'contactos-emergencia/' + id + '/paciente?idPaciente=' + idPaciente, JSON.stringify(data));
  }

  deleteContactoEmergencia(id: number): Observable<boolean>{
    return this.http.delete<boolean>(this.baseUrl + 'contactos-emergencia/' + id);
  }
}
