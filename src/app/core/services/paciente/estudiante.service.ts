import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Estudiante} from '../../models/estudiante/estudiante.model';

@Injectable({
  providedIn: 'root'
})
export class EstudianteService {
  baseUrl: string = environment.baseUrl;
  commonUrl: string;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Allow: 'GET, POST, HEAD'
    })
  };

  constructor(private http: HttpClient) {
    this.commonUrl = this.baseUrl + 'pacientes/'
  }

  // POST
  createEstudiante(data: Estudiante, idPaciente: number): Observable<Estudiante>{
    return this.http.post<Estudiante>(
      this.commonUrl + idPaciente + '/estudiantes/', JSON.stringify(data), this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }

  // UPDATE
  updateEstudiante(data: Estudiante, id: number): Observable<Estudiante>{
    return this.http.put<Estudiante>(
      this.commonUrl + id + '/estudiantes/' + id, JSON.stringify(data), this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }


  private handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
