import {Injectable} from '@angular/core';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Paciente} from '../../models/paciente/paciente.model';
import {Observable, of} from 'rxjs';
import {environment} from 'src/environments/environment';
import {PacienteInterno} from 'src/app/core/models/paciente/paciente-interno.model';
import {PacienteExterno} from 'src/app/core/models/paciente/paciente-externo.model';
import {GrupoPrioritarioPaciente} from 'src/app/core/models/paciente/grupo-prioritario-paciente.model';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PacienteService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  searchPacienteByCedula(numeroArchivo: string): Observable<Paciente[]> {
    const url = this.baseUrl + 'pacientes/search/numero-archivo?numeroArchivo=';
    return this.http.get<Paciente[]>(url + numeroArchivo);
  }

  searchPacienteByFullName(nombres: string): Observable<Paciente[]> {
    nombres = nombres.toLocaleUpperCase();
    const url = this.baseUrl + 'pacientes/search/fullname?nombre=';
    return this.http.get<Paciente[]>(url + nombres);
  }

  checkNumeroArchivoNotExists(numeroArchivo: string) {
    return this.http.get<Paciente>(
      this.baseUrl + 'pacientes/verify-exist?numeroArchivo=' + numeroArchivo);
  }

  getPacienteByNumeroArchivo(numeroArchivo: string): Observable<Paciente> {
    return this.http.get<Paciente>(this.baseUrl + 'pacientes/?numeroArchivo=' + numeroArchivo);
  }

  createPacienteInterno(pacienteInterno: PacienteInterno): Observable<Paciente> {
    return this.http.post<Paciente>(this.baseUrl + 'pacientes/save/banner/', JSON.stringify(pacienteInterno));
  }

  createPacienteExterno(pacienteExterno: PacienteExterno): Observable<Paciente> {
    return this.http.post<Paciente>(this.baseUrl + 'pacientes/save/external/', JSON.stringify(pacienteExterno));
  }

  updatePaciente(paciente: PacienteExterno, id: number): Observable<Paciente> {
    return this.http.put<Paciente>(this.baseUrl + 'pacientes/external/' + id, JSON.stringify(paciente));
  }

  updatePacienteInterno(paciente: PacienteInterno, id: number): Observable<Paciente> {
    return this.http.put<Paciente>(this.baseUrl + 'pacientes/banner/' + id, JSON.stringify(paciente));
  }

  activateAsStudent(idPaciente: number): Observable<Paciente> {
    const url = this.baseUrl + 'pacientes/' + idPaciente + '/activar-estudiante';
    return this.http.post<Paciente>(url, null);
  }

  activateAsEmployee(idPaciente: number): Observable<Paciente> {
    const url = this.baseUrl + 'pacientes/' + idPaciente + '/activar-empleado';
    return this.http.post<Paciente>(url, null);
  }

  getGruposPrioritarios(idPaciente: number): Observable<GrupoPrioritarioPaciente[]> {
    const url = this.baseUrl + 'pacientes/' + idPaciente + '/grupos-prioritarios';
    return this.http.get<GrupoPrioritarioPaciente[]>(url);
  }

  createGrupoPrioritario(idPaciente: number, data: GrupoPrioritarioPaciente[]): Observable<GrupoPrioritarioPaciente[]> {
    const url = this.baseUrl + 'pacientes/' + idPaciente + '/grupos-prioritarios/';
    return this.http.post<GrupoPrioritarioPaciente[]>(url, JSON.stringify(data));
  }

  revokeConsentimientoInformado(idPaciente: number, pidmUsuario: number) {
    const url = this.baseUrl + 'pacientes/' + idPaciente + '/revocar-consentimiento-informado?pidmUsuario=' + pidmUsuario;
    return this.getFileFromResponse(url);

  }

  acceptConsentimientoInformado(idPaciente: number, pidmUsuario: number) {
    const url = this.baseUrl + 'pacientes/' + idPaciente + '/aceptar-consentimiento-informado?pidmUsuario=' + pidmUsuario;
    return this.getFileFromResponse(url);
  }

  getFileFromResponse(url: string) {
    return this.http.get(url, {observe: 'response', responseType: 'blob'})
      .pipe(
        map((response) => {
          const type = response.body.type;
          const a = document.createElement('a');
          const generatedUrl = URL.createObjectURL(
            new Blob([response.body], { type })
          );
          a.href = generatedUrl;
          a.download = response.headers
            .get('content-disposition')
            .split('filename=')[1];
          document.body.appendChild(a);
          a.click();
          setTimeout(() => {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(generatedUrl);
          }, 0);
          return response;
        })
      );
  }
}
