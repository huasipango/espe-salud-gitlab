import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import {AreaSalud} from 'src/app/core/models/catalogo/area-salud.model';

@Injectable({
  providedIn: 'root'
})
export class SaludUserService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getUser(pidm: number): Observable<SaludUser> {
    return this.http.get<SaludUser>(this.baseUrl + 'usuarios/' + pidm);
  }

  getAllUsers(): Observable<SaludUser[]> {
    return this.http.get<SaludUser[]>(this.baseUrl + 'usuarios');
  }

  getAreasSalud(): Observable<AreaSalud[]> {
    return this.http.get<AreaSalud[]>(this.baseUrl + 'areas-salud');
  }

  createUsuario(data: SaludUser): Observable<SaludUser>  {
    const url = this.baseUrl + 'usuarios/';
    return this.http.post<SaludUser> (url, JSON.stringify(data));
  }

  updateUsuario(pidm: number, data: SaludUser): Observable<SaludUser>  {
    const url = this.baseUrl + 'usuarios/' + pidm;
    return this.http.put<SaludUser>(url, JSON.stringify(data));
  }

  checkNumeroUsernameNotExists(username: string) {
    return this.http.get<SaludUser>(
      this.baseUrl + 'usuarios/verify-exist?username=' + username);
  }

  disableUser(pidm: number): Observable<SaludUser>  {
    const url = this.baseUrl + 'usuarios/' + pidm + '/disable';
    return this.http.post<SaludUser>(url, null);
  }

  enableUser(pidm: number): Observable<SaludUser>  {
    const url = this.baseUrl + 'usuarios/' + pidm + '/active';
    return this.http.post<SaludUser>(url, null);
  }

  getUsuariosByIdDispensario(idDispensario: number): Observable<SaludUser[]>{
    return this.http.get<SaludUser[]>(this.baseUrl + 'usuarios/dispensario/' + idDispensario);
  }

  uploadFirma(pidm: number, data: any): Observable<boolean>  {
    const url = `${this.baseUrl}usuarios/${pidm}/upload-firma`
    return this.http.post<boolean> (url, data);
  }
}
