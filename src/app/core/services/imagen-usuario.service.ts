import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ImagenUsuarioService {
  private baseUrl: string = environment.imagesWebService;
  constructor() { }

  getUserImage(idBanner: string): string {
    return this.baseUrl + idBanner;
  }
}
