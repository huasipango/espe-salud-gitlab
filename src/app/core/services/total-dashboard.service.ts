import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {DashboardParams} from 'src/app/core/models/dashboard/dashboard-params.model';
import {PersonalDashboard} from 'src/app/core/models/dashboard/personal-dashboard.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TotalDashboardService {
  private baseUrl: string = environment.baseUrl;
  constructor(
    private http: HttpClient) {
  }

  getDashboardData(slugReport: string, params: DashboardParams) {
    const url = `${this.baseUrl}dashboards/evoluciones/${slugReport}`;

    let queryString = '';
    for (const p of Object.keys(params)) {
      queryString += params[p] ? `&${p}=${params[p]}` : ``;
    }
    return this.http.get<any>(
      `${url}${
        queryString ? `?${queryString}` : ``
      }`,
    );
  }

  getPersonalUserDashboard(pidm: number): Observable<PersonalDashboard> {
    const url = `${this.baseUrl}dashboards/usuarios/personal?pidm=${pidm}`;
    return this.http.get<PersonalDashboard>(url);
  }

  getTotalAtencionesPorRelacionEspe(params: DashboardParams): any{
    const url = `${this.baseUrl}dashboards/evoluciones/relaciones-espe`;

    let queryString = '';
    for (const p of Object.keys(params)) {
      queryString += params[p] ? `&${p}=${params[p]}` : ``;
    }
    return this.http.get<any>(
      `${url}${
        queryString ? `?${queryString}` : ``
      }`,
    );
  }

  getTotalAtencionesPorSeguroSalud(params: DashboardParams): any{
    const url = `${this.baseUrl}dashboards/evoluciones/seguro-salud`;

    let queryString = '';
    for (const p of Object.keys(params)) {
      queryString += params[p] ? `&${p}=${params[p]}` : ``;
    }
    return this.http.get<any>(
      `${url}${
        queryString ? `?${queryString}` : ``
      }`,
    );
  }

  getTotalAtencionesPorCie10(params: DashboardParams): any{
    const url = `${this.baseUrl}dashboards/evoluciones/codigo-cie10`;

    let queryString = '';
    for (const p of Object.keys(params)) {
      queryString += params[p] ? `&${p}=${params[p]}` : ``;
    }
    return this.http.get<any>(
      `${url}${
        queryString ? `?${queryString}` : ``
      }`,
    );
  }

  getTotalAtencionesPorTipoConsulta(params: DashboardParams): any{
    const url = `${this.baseUrl}dashboards/evoluciones/tipo-consulta`;

    let queryString = '';
    for (const p of Object.keys(params)) {
      queryString += params[p] ? `&${p}=${params[p]}` : ``;
    }
    return this.http.get<any>(
      `${url}${
        queryString ? `?${queryString}` : ``
      }`,
    );
  }

  getTotalAtencionesPorPrevencion(params: DashboardParams): any{
    const url = `${this.baseUrl}dashboards/evoluciones/prevencion`;

    let queryString = '';
    for (const p of Object.keys(params)) {
      queryString += params[p] ? `&${p}=${params[p]}` : ``;
    }
    return this.http.get<any>(
      `${url}${
        queryString ? `?${queryString}` : ``
      }`,
    );
  }

  getTotalAtencionesPorMorbilidad(params: DashboardParams): any{
    const url = `${this.baseUrl}dashboards/evoluciones/morbilidad`;

    let queryString = '';
    for (const p of Object.keys(params)) {
      queryString += params[p] ? `&${p}=${params[p]}` : ``;
    }
    return this.http.get<any>(
      `${url}${
        queryString ? `?${queryString}` : ``
      }`,
    );
  }

  getTotalAtencionesPorSexo(params: DashboardParams): any{
    const url = `${this.baseUrl}dashboards/evoluciones/sexo`;

    let queryString = '';
    for (const p of Object.keys(params)) {
      queryString += params[p] ? `&${p}=${params[p]}` : ``;
    }
    return this.http.get<any>(
      `${url}${
        queryString ? `?${queryString}` : ``
      }`,
    );
  }

  getTotalAtencionesPorMotivoAtencion(params: DashboardParams): any{
    const url = `${this.baseUrl}dashboards/evoluciones/motivo-atencion`;

    let queryString = '';
    for (const p of Object.keys(params)) {
      queryString += params[p] ? `&${p}=${params[p]}` : ``;
    }
    return this.http.get<any>(
      `${url}${
        queryString ? `?${queryString}` : ``
      }`,
    );
  }

  getTotalAtencionesPorPrevencionVsMorbilidad(params: DashboardParams): any{
    const url = `${this.baseUrl}dashboards/evoluciones/prevencion-morbilidad`;

    let queryString = '';
    for (const p of Object.keys(params)) {
      queryString += params[p] ? `&${p}=${params[p]}` : ``;
    }
    return this.http.get<any>(
      `${url}${
        queryString ? `?${queryString}` : ``
      }`,
    );
  }

}
