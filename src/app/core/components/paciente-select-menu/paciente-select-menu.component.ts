import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatTableDataSource} from '@angular/material/table';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {FormControl} from '@angular/forms';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icNewTab from '@iconify/icons-fa-solid/external-link-alt';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {SelectionModel} from '@angular/cdk/collections';
import {PacienteService} from '../../services/paciente/paciente.service';
import {Title} from '@angular/platform-browser';
import {LoadingService} from '../../services/loading.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {isNumeric} from 'rxjs/internal-compatibility';
import {SeleccionTipoPacienteModalComponent} from 'src/app/modules/medicina-general/registro-paciente/components/seleccion-tipo-paciente-modal/seleccion-tipo-paciente-modal.component';

@UntilDestroy()
@Component({
  selector: 'vex-paciente-select-menu',
  templateUrl: './paciente-select-menu.component.html',
  styleUrls: ['./paciente-select-menu.component.scss'],
  animations: [fadeInUp400ms, stagger40ms],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]

})
export class PacienteSelectMenuComponent implements OnInit, AfterViewInit {

  pacientes: Paciente [] = [];
  columns: string[] = ['checkbox', 'tipoIdentificacion', 'numeroIdentificacion', 'paciente', 'tipoPaciente'];

  pageSize = 6;
  pageSizeOptions: number[] = [6];
  dataSource: MatTableDataSource<Paciente> | null;
  selection = new SelectionModel<Paciente>(true, []);
  searchCtrl = new FormControl();

  icSearch = icSearch;
  icFilterList = icFilterList;
  icAdd = icAdd;
  icNewTab = icNewTab;
  pacienteActual: Paciente;

  preventSingleClick = false;
  timer: any;
  delay: number;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    public dialogRef: MatDialogRef<PacienteSelectMenuComponent>,
    private router: Router,
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private pacienteService: PacienteService,
    private pacienteGlobalService: PacienteGlobalService,
    private titleService: Title,
    private loadingService: LoadingService
  ) {
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.searchCtrl.valueChanges.pipe(
      debounceTime(400),
      distinctUntilChanged(),
      untilDestroyed(this)
    ).subscribe(value => this.searchPaciente(value));
  }

  searchPaciente(value: string) {
    this.selection.clear();
    value = value.trim();
    if (value.trim() === '') {
      this.dataSource.data = [];
    } else if (value.trim().length >= 4) {
      if (isNumeric(value)) {
        this.loadingService.showLoaderUntilCompleted(
          this.pacienteService.searchPacienteByCedula(value)
        ).subscribe((resp) => {
          this.pacientes = resp;
          this.dataSource.data = resp;
        });
      } else {
        this.loadingService.showLoaderUntilCompleted(
          this.pacienteService.searchPacienteByFullName(value)
        ).subscribe((resp) => {
          this.pacientes = resp;
          this.dataSource.data = resp;
        });
      }
    }
  }

  createPaciente() {
    this.dialogRef.close();
    this.dialog.open(SeleccionTipoPacienteModalComponent, {
      width: '600px',
      maxWidth: '100%'
    }).afterClosed();
  }

  getPatientFullName(paciente: Paciente): string {
    return `${paciente.primerNombre} ${paciente.segundoNombre} ${paciente.apellidoPaterno} ${paciente.apellidoMaterno}`;
  }

  openPaciente(pacienteSeleccionado: Paciente) {
    if (pacienteSeleccionado) {
      this.snackbar.open('Abriendo archivo del paciente', 'OK', {
        duration: 2000
      });
      this.pacienteGlobalService.setPacienteGlobal(pacienteSeleccionado);
      this.pacienteGlobalService.setLastPacienteToLocalStorage(pacienteSeleccionado.numeroArchivo);
      const pacienteName = pacienteSeleccionado.primerNombre + ' ' + pacienteSeleccionado.apellidoPaterno;
      this.titleService.setTitle(pacienteName);
      this.dialogRef.close();
    }
  }

  oneClick(event: any, paciente: Paciente) {
    this.preventSingleClick = false;
    const delay = 200;
    this.timer = setTimeout(() => {
      if (!this.preventSingleClick) {
        this.selectItem(paciente);
      }
    }, delay);
  }

  doubleClick(event: any, paciente: Paciente) {
    this.preventSingleClick = true;
    clearTimeout(this.timer);
    this.openPaciente(paciente);
  }

  // openNewTab(pacienteSeleccionado: Paciente) {
  //   const url = this.router.serializeUrl(
  //     this.router.createUrlTree(['/medicina-general/perfil-paciente'],{
  //       state: {paciente: pacienteSeleccionado}
  //     })
  //   );
  //   window.open(url, '_blank');
  // }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  onFilterChange(value: string) {
    if (!this.dataSource) {
      return;
    }
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    !this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  selectItem(row) {
    this.masterToggle();
    this.selection.toggle(row);
  }
}
