import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoPacienteSidenavComponent } from './info-paciente-sidenav.component';
import {MatListModule} from '@angular/material/list';
import {RouterModule} from '@angular/router';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatRippleModule} from '@angular/material/core';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';



@NgModule({
  declarations: [InfoPacienteSidenavComponent],
  exports: [
    InfoPacienteSidenavComponent
  ],
  imports: [
    CommonModule,
    MatListModule,
    RouterModule,
    MatProgressBarModule,
    MatRippleModule,
    MatIconModule,
    IconModule
  ]
})
export class InfoPacienteSidenavModule { }
