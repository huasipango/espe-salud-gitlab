import { Component, OnInit } from '@angular/core';
import {DateTime} from 'luxon';
import {localString, zoneString} from 'src/app/core/constants/constants';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import icUser from '@iconify/icons-fa-solid/user-injured';
import icCedula from '@iconify/icons-fa-solid/address-card';
import icAllergies from '@iconify/icons-fa-solid/allergies';
import icBlind from '@iconify/icons-fa-solid/blind';
import icCalendar from '@iconify/icons-fa-solid/calendar-day';
import icBlood from '@iconify/icons-fa-solid/syringe';
import icLateralidad from '@iconify/icons-fa-solid/arrows-alt-h';
import icSex from '@iconify/icons-fa-solid/transgender';
import {
  calculateAge,
  calculateDaysDiff, calculateDaysEmb,
  calculateMonthsDiff,
  calculateYearsDiff, getFullAge
} from 'src/app/core/utils/operations.util';
import {AntecedentePersonalService} from 'src/app/modules/medicina-general/antecedente-clinico/services/antecedente-personal.service';
import {DiscapacidadService} from 'src/app/modules/medicina-general/antecedente-clinico/services/discapacidad.service';
import {AntecedentePersonal} from 'src/app/modules/medicina-general/antecedente-clinico/models/antecedente-personal.model';
import {Discapacidad} from 'src/app/modules/medicina-general/antecedente-clinico/models/discapacidad.model';
import {Observable} from 'rxjs';

@Component({
  selector: 'vex-info-paciente-sidenav',
  templateUrl: './info-paciente-sidenav.component.html',
  styleUrls: ['./info-paciente-sidenav.component.scss']
})
export class InfoPacienteSidenavComponent implements OnInit {
  date = DateTime.local().setZone(zoneString).setLocale(localString).toFormat('D');
  dayName = DateTime.local().setZone(zoneString).setLocale(localString).toFormat('EEEE').toLocaleUpperCase();
  pacienteActual: Paciente;
  antecedentePersonal: Observable<AntecedentePersonal>;
  discapacidades: Discapacidad[] = [];
  icCedula = icCedula;
  icUser = icUser;
  icAllergies = icAllergies;
  icBlind = icBlind;
  icCalendar = icCalendar;
  icBlood = icBlood;
  icLateralidad = icLateralidad;
  icSex = icSex;
  constructor(
    private pacienteGlobalService: PacienteGlobalService,
    private antecedentePersonalService: AntecedentePersonalService,
    private discapacidadService: DiscapacidadService
  ) { }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getAntecedentePersonal();
          this.getDiscapacidades();
        }
      });
  }

  getAntecedentePersonal(): void {
    this.antecedentePersonal = this.antecedentePersonalService
      .getAntecedentePersonal(this.pacienteActual.id);
  }

  getDiscapacidades(): void {
    this.discapacidadService.getDiscapacidades(this.pacienteActual.id)
      .subscribe((discapacidades) => {
        if (discapacidades.length > 0) {
          this.discapacidades = discapacidades;
        }
      });
  }

  getEdad(fechaNacimiento: string): string {
    return getFullAge(fechaNacimiento);
  }
}
