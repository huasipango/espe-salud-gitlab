import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FlexLayoutModule} from '@angular/flex-layout';
import {IconModule} from '@visurel/iconify-angular';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import { EmptyPacienteComponent } from './empty-paciente.component';


@NgModule({
  declarations: [EmptyPacienteComponent],
  exports: [
    EmptyPacienteComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    IconModule,
    MatButtonModule,
    MatIconModule
  ]
})
export class EmptyPacienteModule { }
