import {Component, OnInit} from '@angular/core';
import icSearch from '@iconify/icons-fa-solid/search';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {PacienteSelectMenuComponent} from '../paciente-select-menu/paciente-select-menu.component';

@Component({
  selector: 'vex-empty-paciente',
  templateUrl: './empty-paciente.component.html',
  styleUrls: ['./empty-paciente.component.scss']
})
export class EmptyPacienteComponent implements OnInit {

  icSearch = icSearch;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
  }

  seleccionarPaciente(): void {
    this.dialog.open(PacienteSelectMenuComponent, {
      width: '900px',
      maxWidth: '100%'
    })
      .afterClosed()
  }

}
