import {Component, OnDestroy, OnInit} from '@angular/core';
import icClose from '@iconify/icons-ic/twotone-close';
import {AuthService} from 'src/app/core/auth/auth.service';
import {LayoutService} from 'src/@vex/services/layout.service';
import {interval, Subscription} from 'rxjs';

@Component({
  selector: 'vex-session-time-remaining',
  templateUrl: './session-time-remaining.component.html',
  styleUrls: ['./session-time-remaining.component.scss']
})
export class SessionTimeRemainingComponent implements OnInit, OnDestroy {
  private subscription: Subscription;

  icClose = icClose;
  isLoggedIn = false;
  tokenExpiration: number;
  isOpen$ = this.layoutService.sessionTimeVisible$;

  timeDifference;
  dDay = new Date(this.authService.getAccessTokenExpiration());

  milliSecondsInASecond = 1000;
  minutesInAnHour = 60;
  SecondsInAMinute  = 60;

  public secondsToDday;
  public minutesToDday;

  constructor(
    private authService: AuthService,
    private layoutService: LayoutService
  ) { }

  private getTimeDifference() {
    this.timeDifference = this.dDay.getTime() - new  Date().getTime();
    this.allocateTimeUnits(this.timeDifference);
  }

  private allocateTimeUnits(timeDifference) {
    this.secondsToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond) % this.SecondsInAMinute);
    this.minutesToDday = Math.floor((timeDifference) /
      (this.milliSecondsInASecond * this.minutesInAnHour) % this.SecondsInAMinute);
  }

  ngOnInit(): void {
    this.isLoggedIn = this.authService.isLoggedIn();
    this.tokenExpiration = this.authService.getAccessTokenExpiration();
    this.subscription = interval(1000)
      .subscribe(x => { this.getTimeDifference(); });
  }

  close() {
    this.authService.refreshToken();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  closeMenu() {
    this.layoutService.closeSessionTime()
  }
}
