import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SessionTimeRemainingComponent } from './session-time-remaining.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';



@NgModule({
  declarations: [SessionTimeRemainingComponent],
  exports: [
    SessionTimeRemainingComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    ContainerModule,
    MatButtonModule,
    MatIconModule,
    IconModule
  ]
})
export class SessionTimeRemainingModule { }
