import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from 'src/app/core/auth/auth.service';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    this.authService.saludUserData$
      .subscribe(saludUser => {
        if (saludUser && saludUser.roles && next.data.roles) {
          const roles = saludUser.roles.filter(rol => next.data.roles.includes(rol.nombre));
          if (roles.length > 0) {
            return true;
          } else {
            this.router.navigate(['/error-403']);
            return false;
          }
        }
        return true;
      });
    return true;
  }
}
