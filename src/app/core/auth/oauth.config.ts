import {AuthConfig} from 'angular-oauth2-oidc';
import {environment} from 'src/environments/environment';

export const authConfig: AuthConfig = {
  issuer: environment.sso.serverUrl.concat('/oauth2endpoints/token'),
  clientId: environment.sso.clientId,
  redirectUri: window.location.origin,
  postLogoutRedirectUri: window.location.origin + '/index.html',
  scope: 'openid profile email',
  tokenEndpoint: environment.sso.serverUrl.concat('/oauth2/token'),
  userinfoEndpoint: environment.sso.serverUrl.concat('/oauth2/userinfo'),
  showDebugInformation: true,
  loginUrl: environment.sso.serverUrl.concat('/oauth2/authorize'),
  logoutUrl: environment.sso.serverUrl.concat('/oidc/logout'),
  requireHttps: environment.sso.requireHttps,
  disableAtHashCheck: true,
  timeoutFactor: 0.95,
  silentRefreshRedirectUri: window.location.origin + '/silent-refresh.html',
  responseType: 'id_token token'
};
