import {Injectable} from '@angular/core';
import {OAuthErrorEvent, OAuthService} from 'angular-oauth2-oidc';
import {authConfig} from 'src/app/core/auth/oauth.config';
import {BehaviorSubject, Observable} from 'rxjs';
import {UserInfo} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/user-info.interface';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import {filter} from 'rxjs/operators';
import {LayoutService} from 'src/@vex/services/layout.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private userSubject: BehaviorSubject<UserInfo> = new BehaviorSubject<UserInfo>(null);
  public userData$: Observable<UserInfo> = this.userSubject.asObservable();

  private saludUserSubject: BehaviorSubject<SaludUser> = new BehaviorSubject<SaludUser>(null);
  public saludUserData$: Observable<SaludUser> = this.saludUserSubject.asObservable();

  private tokenExpirationTimer: any;

  constructor(private oAuthService: OAuthService, private layoutService: LayoutService) {
    this.configureOauthService();
  }

  public getUserName(): string {
    const claims = this.getUserClaims();
    this.getUserInfo();
    if (claims === null) {
      // window.location.reload()
    } else {
      return claims['sub'].split('@')[0];
    }
  }

  public getUserInfo(): string {
    const idToken = this.oAuthService.getIdToken();
    if (idToken === null) {
      return;
    } else {
      return typeof idToken['sub'] !== 'undefined' ? idToken['sub'].toString() : '';
    }
  }

  public getAccessTokenExpiration(): number {
    return this.oAuthService.getAccessTokenExpiration();
  }

  public getUserClaims(): object {
    return this.oAuthService.getIdentityClaims();
  }

  public isLoggedIn(): boolean {
    return this.oAuthService.getAccessToken() !== null;
  }

  public hasValidAccessToken(): boolean {
    return this.oAuthService.hasValidAccessToken();
  }

  public hasValidIdToken(): boolean {
    return this.oAuthService.hasValidIdToken();
  }

  public login(): void {
    this.obtainAccessToken();
  }

  public autoLogout(expirationDuration: number): void {
    this.tokenExpirationTimer = setTimeout(() => {
      this.logout();
    }, expirationDuration)
  }

  public saveUserInstance(userData: UserInfo){
    this.userSubject.next(userData);
  }

  public saveSaludUserInstance(saludUser: SaludUser) {
    this.saludUserSubject.next(saludUser);
  }

  public logout(): void {
    if (this.tokenExpirationTimer) {
      clearTimeout(this.tokenExpirationTimer);
    }
    this.tokenExpirationTimer = null;
    this.oAuthService.logOut();
  }

  public refreshToken(): void {
    this.oAuthService.refreshToken();
  }

  public silentRefresh() {
    this
      .oAuthService
      .silentRefresh()
      .then(info => console.warn('refresh ok', info))
      .catch(err => console.error('refresh error', err));
  }

  public task(): any {
    this.oAuthService.getAccessToken();
  }

  private configureOauthService(): void {
    this.oAuthService.configure(authConfig);
    this.oAuthService.tryLogin();
    this.oAuthService.events.subscribe(e => (e instanceof OAuthErrorEvent) ? console.error(e) : console.warn(e));
    this.oAuthService.events.pipe(filter(e => e.type === 'token_expires'))
      .subscribe(e => {
        this.layoutService.openSessionTime();
      });
  }

  private obtainAccessToken(): void {
    this.oAuthService.initLoginFlow();
  }
}
