import {Component, OnInit} from '@angular/core';
import {AuthService} from 'src/app/core/auth/auth.service';
import {UserInfo} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/user-info.interface';
import {UserService} from 'src/app/core/services/user.service';
import {Observable} from 'rxjs';
import {FullUserInfo} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/full-user-info.interface';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import icMail from '@iconify/icons-ic/twotone-mail';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icCheck from '@iconify/icons-ic/twotone-check';
import icMobile from '@iconify/icons-ic/phone-iphone';
import icAddress from '@iconify/icons-ic/location-on';
import icIdCard from '@iconify/icons-fa-solid/id-card';
import icIdEspe from '@iconify/icons-fa-solid/id-card-alt';
import icAccount from '@iconify/icons-ic/account-circle';

@Component({
  selector: 'vex-informacion-personal-usuario',
  templateUrl: './informacion-personal-usuario.component.html',
  styleUrls: ['./informacion-personal-usuario.component.scss'],
  animations: [
    fadeInUp400ms,
    fadeInRight400ms,
    scaleIn400ms,
    stagger40ms
  ]
})
export class InformacionPersonalUsuarioComponent implements OnInit {

  public userFullData: Observable<FullUserInfo>;

  icPhone = icPhone;
  icCheck = icCheck;
  icMail = icMail;
  icMobile = icMobile;
  icAddress = icAddress;
  icIdCard = icIdCard;
  icIdEspe = icIdEspe;
  icAccount = icAccount;

  constructor(private authService: AuthService,
              private userService: UserService) {
  }

  ngOnInit(): void {
    this.authService.userData$
      .subscribe((userData: UserInfo) => {
        if (userData) {
          this.userFullData = this.userService.getFullUserDataByIdCard(userData.cedula);
        }
      });
  }

}
