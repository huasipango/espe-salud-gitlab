import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {InformacionPersonalUsuarioComponent} from 'src/app/core/auth/perfil-usuario/informacion-personal-usuario/informacion-personal-usuario.component';


const routes: Routes = [
  {
    path: '',
    component: InformacionPersonalUsuarioComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InformacionPersonalUsuarioRoutingModule {
}
