import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InformacionPersonalUsuarioComponent } from './informacion-personal-usuario.component';
import {InformacionPersonalUsuarioRoutingModule} from 'src/app/core/auth/perfil-usuario/informacion-personal-usuario/informacion-personal-usuario-routing.module';
import {FlexModule, GridModule} from '@angular/flex-layout';
import {IconModule} from '@visurel/iconify-angular';



@NgModule({
  declarations: [InformacionPersonalUsuarioComponent],
  imports: [
    CommonModule,
    InformacionPersonalUsuarioRoutingModule,
    GridModule,
    IconModule,
    FlexModule
  ]
})
export class InformacionPersonalUsuarioModule { }
