import { Component, OnInit } from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {AuthService} from 'src/app/core/auth/auth.service';
import {switchMap} from 'rxjs/operators';
import {SaludUserService} from 'src/app/core/services/salud-user.service';
import {Observable} from 'rxjs';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import icMail from '@iconify/icons-ic/twotone-mail';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icCheck from '@iconify/icons-ic/twotone-check';
import icStethoscope from '@iconify/icons-fa-solid/stethoscope';
import icClinic from '@iconify/icons-fa-solid/clinic-medical';
import icIdCard from '@iconify/icons-fa-solid/id-card';
import icIdEspe from '@iconify/icons-fa-solid/id-card-alt';
import icIdUser from '@iconify/icons-fa-solid/id-badge';
import icIdMsp from '@iconify/icons-fa-solid/credit-card';
import icAccount from '@iconify/icons-ic/account-circle';
import icToggleOn from '@iconify/icons-fa-solid/toggle-on';
import icToggleOff from '@iconify/icons-fa-solid/toggle-off';
import icSexo from '@iconify/icons-fa-solid/transgender-alt';
import icGenderless from '@iconify/icons-fa-solid/genderless';
import {FormacionProfesional} from 'src/app/core/models/catalogo/formacion-profesional.model';

@Component({
  selector: 'vex-informacion-usuario-salud',
  templateUrl: './informacion-usuario-salud.component.html',
  styleUrls: ['./informacion-usuario-salud.component.scss'],
  animations: [
    fadeInUp400ms,
    fadeInRight400ms,
    scaleIn400ms,
    stagger40ms
  ]
})
export class InformacionUsuarioSaludComponent implements OnInit {
  saludUser: Observable<SaludUser>;
  formacionProfesional: FormacionProfesional;
  icPhone = icPhone;
  icCheck = icCheck;
  icMail = icMail;
  icStethoscope = icStethoscope;
  icClinic = icClinic;
  icIdCard = icIdCard;
  icIdEspe = icIdEspe;
  icAccount = icAccount;
  icToggleOn = icToggleOn;
  icToggleOff = icToggleOff;
  icIdUser = icIdUser;
  icIdMsp = icIdMsp;
  icSexo = icSexo;
  icGenderless = icGenderless;

  constructor(
    private authService: AuthService,
    private saludUserService: SaludUserService) { }

  ngOnInit(): void {
    this.saludUser = this.authService.userData$.pipe(
      switchMap((data) => {
        if (data) {
          return this.saludUserService.getUser(data.pidm);
        }
      })
    );
  }
}
