import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InformacionUsuarioSaludRoutingModule } from './informacion-usuario-salud-routing.module';
import { InformacionUsuarioSaludComponent } from './informacion-usuario-salud.component';
import {IconModule} from '@visurel/iconify-angular';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FirmaUsuarioModule} from 'src/app/core/auth/perfil-usuario/components/firma-usuario/firma-usuario.module';


@NgModule({
  declarations: [InformacionUsuarioSaludComponent],
  imports: [
    CommonModule,
    InformacionUsuarioSaludRoutingModule,
    IconModule,
    FlexLayoutModule,
    FirmaUsuarioModule
  ]
})
export class InformacionUsuarioSaludModule { }
