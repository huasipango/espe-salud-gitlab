import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {InformacionUsuarioSaludComponent} from 'src/app/core/auth/perfil-usuario/informacion-usuario-salud/informacion-usuario-salud.component';

const routes: Routes = [
  {
    path: '',
    component: InformacionUsuarioSaludComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InformacionUsuarioSaludRoutingModule { }
