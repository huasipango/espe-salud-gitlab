import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FirmaUsuarioComponent } from './firma-usuario.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {IconModule} from '@visurel/iconify-angular';
import {MatButtonModule} from '@angular/material/button';
import {ReactiveFormsModule} from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';



@NgModule({
  declarations: [
    FirmaUsuarioComponent
  ],
  exports: [
    FirmaUsuarioComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    IconModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatIconModule,
    LoadingModule
  ]
})
export class FirmaUsuarioModule { }
