import {Component, Input, OnInit} from '@angular/core';
import icUpload from '@iconify/icons-ic/cloud-upload';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SaludUserService} from 'src/app/core/services/salud-user.service';
import {scaleInOutAnimation} from 'src/@vex/animations/scale-in-out.animation';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {splitImageFile} from 'src/app/core/utils/operations.util';
import {LoadingService} from 'src/app/core/services/loading.service';

@Component({
  selector: 'vex-firma-usuario',
  templateUrl: './firma-usuario.component.html',
  styleUrls: ['./firma-usuario.component.scss'],
  animations: [
    stagger40ms,
    scaleIn400ms,
    scaleInOutAnimation
  ]
})
export class FirmaUsuarioComponent implements OnInit {
  form: FormGroup;

  @Input() firma: string | ArrayBuffer;
  @Input() pidm: number;
  showButton: boolean;

  icUpload = icUpload;
  imageUrl: string;
  userMessages = USER_MESSAGES;
  constructor(
    private fb: FormBuilder,
    private snackbar: MatSnackBar,
    private usuarioSaludService: SaludUserService,
    private loadingService: LoadingService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      firma: [this.firma || null, Validators.required],
    });
  }

  selectImage(event: any): void {
    if (!event.target.files[0] || event.target.files[0].length === 0) {
      this.showMessage('Debe seleccionar una imagen');
      return;
    }
    const mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.showMessage('El tipo de archivo seleccionado no es valido');
      return;
    }

    const reader = new FileReader();
    const file = event.target.files[0];
    reader.readAsDataURL(file);

    reader.onload = (_event) => {
      this.firma = splitImageFile(reader.result as string);
      this.imageUrl = this.firma
      this.form.patchValue({
        firma: this.firma
      });
    };
  }

  showMessage(message: string) {
    this.snackbar.open(message, 'CERRAR', {
      duration: 5000,
      horizontalPosition: 'center'
    });
  }

  upload() {
    if (!this.firma) {
      this.showMessage('Debe seleccionar una imagen.');
      return;
    }
    this.loadingService.showLoaderUntilCompleted(
      this.usuarioSaludService.uploadFirma(this.pidm, this.form.getRawValue())
    ).subscribe((response) => {
        if (response) {
          this.showMessage(this.userMessages.uploadSuccessfulMessage);
        } else {
          this.showMessage(this.userMessages.uploadFailedMessage);
        }
      }, error => {
        this.showMessage(this.userMessages.uploadFailedMessage);
      })
  }

  delete() {
    this.loadingService.showLoaderUntilCompleted(
      this.usuarioSaludService.uploadFirma(this.pidm, {firma: null})
    ).subscribe((response) => {
      if (response) {
        this.firma = null;
        this.form.reset();
        this.showMessage(this.userMessages.deletedImageSuccessMessage);
      } else {
        this.showMessage(this.userMessages.deletedImageFailedMessage);
      }
    }, error => {
      this.showMessage(this.userMessages.deletedImageFailedMessage);
    })
  }
}
