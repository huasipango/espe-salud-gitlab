import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfilUsuarioComponent } from 'src/app/core/auth/perfil-usuario/perfil-usuario.component';
import {PerfilUsuarioRoutingModule} from 'src/app/core/auth/perfil-usuario/perfil-usuario-routing.module';
import {MatTabsModule} from '@angular/material/tabs';



@NgModule({
  declarations: [PerfilUsuarioComponent],
  imports: [
    CommonModule,
    PerfilUsuarioRoutingModule,
    MatTabsModule
  ]
})
export class PerfilUsuarioModule { }
