import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {VexRoutes} from 'src/@vex/interfaces/vex-route.interface';
import {PerfilUsuarioComponent} from 'src/app/core/auth/perfil-usuario/perfil-usuario.component';

const routes: VexRoutes = [
  {
    path: '',
    component: PerfilUsuarioComponent,
    data: {
      toolbarShadowEnabled: true,
      containerEnabled: true
    },
    children: [
      {
        path: '',
        loadChildren: () => import('./informacion-personal-usuario/informacion-personal-usuario.module')
          .then(m => m.InformacionPersonalUsuarioModule)
      },
      {
        path: 'perfil-salud',
        loadChildren: () => import('./informacion-usuario-salud/informacion-usuario-salud.module')
          .then(m => m.InformacionUsuarioSaludModule)
      }
    ]
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PerfilUsuarioRoutingModule {
}
