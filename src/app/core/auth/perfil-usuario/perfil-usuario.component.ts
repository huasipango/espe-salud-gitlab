import { Component, OnInit } from '@angular/core';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {Link} from 'src/@vex/interfaces/link.interface';
import {AuthService} from 'src/app/core/auth/auth.service';
import {Observable} from 'rxjs';
import {getUserShortName, UserInfo} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/user-info.interface';
import {ImagenUsuarioService} from 'src/app/core/services/imagen-usuario.service';

@Component({
  selector: 'vex-perfil-usuario',
  templateUrl: './perfil-usuario.component.html',
  styleUrls: ['./perfil-usuario.component.scss'],
  animations: [
    scaleIn400ms,
    fadeInRight400ms
  ]
})
export class PerfilUsuarioComponent implements OnInit {

  public userData: Observable<UserInfo>;

  links: Link[] = [
    {
      label: 'MI PERFIL',
      route: './',
      routerLinkActiveOptions: { exact: true }
    },
    {
      label: 'PERFIL SALUD',
      route: 'perfil-salud'
    }
  ];

  constructor(
    private authService: AuthService,
    private imagenUsuarioService: ImagenUsuarioService) { }

  ngOnInit(): void {
    this.userData = this.authService.userData$;
  }

  getShortName(userFullName: string): string{
    return getUserShortName(userFullName);
  }

  getUserImage(idBanner: string): string {
    return this.imagenUsuarioService.getUserImage(idBanner);
  }
}
