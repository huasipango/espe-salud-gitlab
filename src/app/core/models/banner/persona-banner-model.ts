export class PersonaBanner {
  id_banner: string;
  pidm: number;
  nombres: string;
  apellidos: string;
  cedula: string;
}
