export class Estudiante {
  id: number;
  fechaIngresoEspe: string;
  nivel: string;
  carrera: string;

  escuela: string;
  campus: string;
  departamento: string;
  actividadesRelevantes: string;
  provinciaEstudio: string;
  cantonEstudio: string;
  parroquiaEstudio: string;
  direccionLugarEstudio: string;
}
