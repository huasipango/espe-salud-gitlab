export class RecordLaboral {
  id: number;
  fechaRegistro: string;
  causa: string;
  edad: number;
  fechaSalidad: string; // TODO Arreglar en el backend el nombre del campo
  fechaReingreso: string;
  diasEntreSalidoIngreso: number;
}
