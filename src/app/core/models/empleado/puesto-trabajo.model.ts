import {CodigoCiuo} from 'src/app/core/models/catalogo/codigo-ciuo.model';

export class PuestoTrabajo {
  id: number;
  actividadesRelevantes: string;
  horarioLaboralActual: string;
  horasTrabajoDia: number;
  horasTrabajoSemana: number;
  horasTrabajoMes: number;
  cumpleComisionServicio: string;
  observacionesHorario: string;
  ciuo: string;
  codigoCIUO: CodigoCiuo;
  idPaciente: number;
}
