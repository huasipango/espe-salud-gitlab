import {PuestoTrabajo} from 'src/app/core/models/empleado/puesto-trabajo.model';

export class Empleado {
  id: number;
  campus: string;
  fechaIngresoLaboral: Date;
  seccion: string;
  departamento: string;
  cantonTrabajo: string;
  provinciaTrabajo: string;
  parroquiaTrabajo: string;
  direccionLaboral: string;
  areaTrabajo: string;
  cargoTrabajoActual: string;

  situacionAdministrativa: string;

  puestoTrabajo: PuestoTrabajo;
}
