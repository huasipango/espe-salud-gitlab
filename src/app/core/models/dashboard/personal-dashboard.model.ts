import {LabelSeries} from 'src/app/core/models/dashboard/label-series.model';
import {PacienteSimple} from 'src/app/core/models/paciente/paciente-simple.model';

export class PersonalDashboard {
  totalPatients: number;
  totalPatientsCurrentMonth: number;
  totalExternPatients: number;
  totalInternPatients: number;
  totalActivePatients: number;
  totalAttentions: number;
  totalAttentionsAtCurrentMonth: number;
  totalAttentionsAtPastMonth: number;
  totalPatientsByDispensary: LabelSeries;
  attentionsDailyAtCurrentMonth: LabelSeries;
  patientsRegisteredDailyAtCurrentMonth: LabelSeries;
  lastFiveRegisteredPatients: PacienteSimple[];
}
