export class DashboardParams {
  year?: number;
  month?: number;
  idDispensario?: number;
  pidm?: number;
}
