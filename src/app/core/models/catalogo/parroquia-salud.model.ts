export class ParroquiaSalud {
  id: string;
  nombre: string;
  codigoProvincia: string;
  codigoCanton: string;
}
