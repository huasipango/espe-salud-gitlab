import {Dosis} from 'src/app/core/models/catalogo/dosis.model';

export class TipoVacuna{
  id: number;
  nombre: string;
  grupoProgramatico: string;
  dosis: Dosis[];
}
