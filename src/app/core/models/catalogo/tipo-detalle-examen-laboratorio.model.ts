export class TipoDetalleExamenLaboratorio {
  id: number;
  nombre: string;
  unidad: string;
  idTipoExamenLaboratorio: number;
}
