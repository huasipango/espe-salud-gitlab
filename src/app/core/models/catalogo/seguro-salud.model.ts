export class SeguroSalud{
    id: number;
    nombre: string;

    constructor(seguroSaludCatalogo : SeguroSalud){
        this.id=seguroSaludCatalogo.id;
        this.nombre=seguroSaludCatalogo.nombre;
    }

}