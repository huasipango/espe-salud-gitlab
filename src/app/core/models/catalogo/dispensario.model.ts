export class Dispensario {
  id: number;
  institucionSistema: string;
  uniCodigo: string;
  unidadOperativa: string;
  nombreImagen: string;
  campus: string;
  direccion: string;
  telefono: string;
  canton: string;
  provincia: string;
  fax: string;
  correo: string;
}
