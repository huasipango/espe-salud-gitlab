import { DescripcionActividadEnfermeria } from './descripcion-actividad-enfermeria';

export class TipoActividadEnfermeria {
    id: number;
    nombre: string;
    descripciones: DescripcionActividadEnfermeria[];
  }
