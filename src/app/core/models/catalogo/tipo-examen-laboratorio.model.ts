import {TipoDetalleExamenLaboratorio} from 'src/app/core/models/catalogo/tipo-detalle-examen-laboratorio.model';

export class TipoExamenLaboratorio {
  id: number;
  nombre: string;
  tipoDetalleExamenLaboratorios: TipoDetalleExamenLaboratorio[];
}
