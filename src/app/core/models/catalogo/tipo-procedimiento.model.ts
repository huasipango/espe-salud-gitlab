export class TipoProcedimiento {
  id: number;
  nombre: string;

  constructor(procedimiento) {
    this.id = procedimiento.id;
    this.nombre = procedimiento.nombre;
  }
}
