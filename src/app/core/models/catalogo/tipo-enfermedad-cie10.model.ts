import {GrupoEnfermedadCIE10} from './grupo-enfermedad-cie10.model';

export class TipoEnfermedadCIE10 {
  codigo: string;
  nombre: string;
  grupoEnfermedadCIE10: GrupoEnfermedadCIE10;
}
