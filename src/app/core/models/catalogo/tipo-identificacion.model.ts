export class TipoIdentificacion {
  id: number;
  nombre: string;
  esRequerido: boolean;
  porDefecto: boolean;
}
