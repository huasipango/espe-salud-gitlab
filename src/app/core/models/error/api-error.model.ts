export class ApiError{
  status: string;
  timestamp: string;
  message: string;
  debugMessage: string;
  subErrors: any
}
