export class Parroquia {
  idPais: string;
  idProvincia: string;
  idCanton: string;
  idParroquia: string;
}
