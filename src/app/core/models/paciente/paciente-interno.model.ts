export class PacienteInterno {
  numeroArchivo: string;
  pidm: number;
  aceptaTransfucion: boolean;
  lateralidad: string;
  idDispensario: number;
  esEmpleado: boolean;
  esEstudiante: boolean;
  idVinculadoEspe: number;
  idSeguroSalud: number;
  idAsociacionAfiliada: number;
  idInstruccion: number;
}
