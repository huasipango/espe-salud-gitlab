export class PacienteSimple {
  id: number;
  numeroArchivo: string;
  nombres: string;
  apellidos: string;
  dispensario: string;
}
