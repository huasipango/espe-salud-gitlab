export class ContactoEmergencia {
  id: number;
  nombreContacto: string;
  direccion: string;
  telefonoCelular: string;
  telefonoConvencional: string;
  parentesco: string;
  idPaciente: number;
}
