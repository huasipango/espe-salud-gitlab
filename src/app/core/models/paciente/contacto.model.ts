export class Contacto {
  callePrincipal: string;
  calleSecundaria: string;
  zonaGeografica: string;
  direccionReferencia: string;
  numeroCelular: string;
  numeroConvencional: string;
  extension: string;
  correoPersonal: string;
  correoInstitucional: string;
  parroquiaResidencia: string;
  barrio: string;
  cantonResidencia: string;
  idCantonResidencia: string;
  provinciaResidencia: string;
  idProvinciaResidencia: number;
  idParroquiaResidencia: string;
}
