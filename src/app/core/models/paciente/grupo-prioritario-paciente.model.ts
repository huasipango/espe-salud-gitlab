import {GrupoPrioritario} from 'src/app/core/models/catalogo/grupo-prioritario.model';

export class GrupoPrioritarioPaciente {
  id: number;
  grupoPrioritario: GrupoPrioritario;
}
