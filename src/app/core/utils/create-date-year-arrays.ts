import { DateTime } from 'luxon';

export function createDateYear(length: number) {
  const dates: number[] = [];
  for (let i = 1; i < length; i++) {
    dates.push(+DateTime.local().minus({ year: i }).toJSDate());
  }
  return dates.reverse();
}

