import {getEnumKeyByEnumValue} from 'src/app/core/utils/enum-to-string-util';
import {EstadoPedidoExamenEnum} from 'src/app/core/enums/estado-pedido-examen.enum';
import {EstadoEvolucionEnum} from 'src/app/core/enums/estado-evolucion.enum';

export const ColorUtil = {
  labelColors() {
    return [
      {
        textClass: 'text-green',
        bgClass: 'bg-green-light',
      },
      {
        textClass: 'text-cyan',
        bgClass: 'bg-cyan-light',
      },
      {
        textClass: 'text-teal',
        bgClass: 'bg-teal-light',
      },
      {
        textClass: 'text-purple',
        bgClass: 'bg-purple-light',
      },
      {
        bgClass: 'bg-red',
        textClass: 'text-red-contrast',
      },
      {
        bgClass: 'bg-green',
        textClass: 'text-green-contrast',
      },
      {
        bgClass: 'bg-cyan',
        textClass: 'text-cyan-contrast',
      },
    ];
  },
  labStateColors() {
    return [
      {
        state: getEnumKeyByEnumValue(EstadoPedidoExamenEnum, EstadoPedidoExamenEnum.SOLICITADO),
        color: {
          label: 'deep-purple',
          backgroundColor: 'bg-deep-purple',
          backgroundContrastColor: 'text-contrast-white',
          textColor: 'text-deep-purple',
          bgClass: 'bg-deep-purple',
          textClass: 'text-deep-purple-contrast',
        }
      },
      {
        state: getEnumKeyByEnumValue(EstadoPedidoExamenEnum, EstadoPedidoExamenEnum.REPORTADO),
        color: {
          label: 'green',
          backgroundColor: 'bg-green',
          backgroundContrastColor: 'text-contrast-white',
          textColor: 'text-green',
          bgClass: 'bg-green',
          textClass: 'text-green-contrast',
        }
      },
      {
        state: getEnumKeyByEnumValue(EstadoPedidoExamenEnum, EstadoPedidoExamenEnum.FINALIZADO),
        color: {
          label: 'green',
          backgroundColor: 'bg-green',
          backgroundContrastColor: 'text-contrast-white',
          textColor: 'text-green',
          bgClass: 'bg-green',
          textClass: 'text-green-contrast',
        }
      }
    ];
  },
  evolucionStateColors() {
    return [
      {
        state: getEnumKeyByEnumValue(EstadoEvolucionEnum, EstadoEvolucionEnum.BORRADOR),
        color: {
          label: 'bg-red',
          backgroundColor: 'bg-red',
          backgroundContrastColor: 'text-contrast-white',
          textColor: 'text-red-contrast',
          bgClass: 'bg-red',
          textClass: 'text-contrast-white',
        }
      },
      {
        state: getEnumKeyByEnumValue(EstadoEvolucionEnum, EstadoEvolucionEnum.ATENDIENDO),
        color: {
          label: 'bg-orange',
          backgroundColor: 'bg-orange-light',
          backgroundContrastColor: 'text-contrast-white',
          textColor: 'text-orange',
          bgClass: 'bg-orange-light',
          textClass: 'text-orange-contrast',
        }
      },
      {
        state: getEnumKeyByEnumValue(EstadoEvolucionEnum, EstadoEvolucionEnum.FINALIZADO),
        color: {
          label: 'green',
          backgroundColor: 'bg-green',
          backgroundContrastColor: 'text-contrast-white',
          textColor: 'text-green',
          bgClass: 'bg-green',
          textClass: 'text-green-contrast',
        }
      }
    ];
  }
};
