export const SimpleCatalogUtil = {
  zonas: [
    'URBANA',
    'RURAL'
  ],
  tipo: [
    'PREVENCION',
    'MORBILIDAD',
  ],
  prevencionCatalogo: [
    'PRIMERAS',
    'SUBSECUENTES'
  ],
  morbilidadCatalogo: [
    {
      nombre: 'PRIMERAS',
      condicionesDiagnostico: [
        'PRESUNTIVO',
        'DEFINITIVO INICIAL',
        'DEFINITIVO INICIAL CONFIRMADO POR LABORATORIO',
        'DEFINITIVO CONTROL'
      ]
    },
    {
      nombre: 'SUBSECUENTES',
      condicionesDiagnostico: [
        'PRESUNTIVO',
        'DEFINITIVO INICIAL',
        'DEFINITIVO INICIAL CONFIRMADO POR LABORATORIO',
        'DEFINITIVO CONTROL'
      ]
    }
  ],
  tresOpcionesCatalogo: [
    'SI',
    'NO',
    'SIN RESPUESTA'
  ],
  frecuenciaMedicacionCatalogo: [
    'NUNCA',
    'MUY POCAS VECES',
    'ALGUNAS VECES',
    'CASI SIEMPRE',
    'SIEMPRE',
  ],
  frecuenciaPlanificacion: [
    'SIEMPRE',
    'CASI SIEMPRE',
    'ALGUNAS VECES',
    'MUY POCAS VECES',
    'NUNCA',
  ],
  gradoDiscapacidadCatalogo: [
    'NO HAY PROBLEMA',
    'LIGERO',
    'MODERADO',
    'GRAVE',
    'COMPLETO'
  ],
  rangoPorcentajeDiscapacidad: [
    {
      porcentaje: '0 A 4%',
      grado: 'NO HAY PROBLEMA',
    },
    {
      porcentaje: '5 A 24%',
      grado: 'LIGERO',
    },
    {
      porcentaje: '25 A 49%',
      grado: 'MODERADO',
    },
    {
      porcentaje: '50 A 95%',
      grado: 'GRAVE',
    },
    {
      porcentaje: '96 A 100%',
      grado: 'COMPLETO',
    }
  ],
  frecuenciaTiempo: [
    'DIARIO',
    'SEMANAL',
    'MENSUAL',
    'ANUAL',
    'OCASIONAL'
  ],
  dinamicaFamiliar: [
    'Nuclear Biparental  (papá, mamá / dos, tres o más  hijos) ',
    'Nuclear Biparental Ampliada (Tíos, abuelos, primos)',
    'Nuclear Biparental Reconstituida (Unidos y viven con hijos ajenos)',
    'Nuclear Monoparental (Un solo cónyuge e hijos)',
    'Personas Sin Familia (Personas Solteras)',
    'Familia Extensa (Agrupación numerosa, dirigida por los abuelos)'
  ],
  nombreNecesidad: [
    '¿CUÁNDO CONSUME ALCOHOL, LLEGA A LA EMBRIAGUEZ ?',
    '¿ALGUIEN CERCANO A USTED LE HA SUGERIDO BUSCAR AYUDA POR EL CONSUMO EXCESIVO DE ALCOHOL?',
    '¿CONSUME EN EXCESO TABACO U OTRAS DROGAS ?',
    '¿ALGUIEN CERCANO A USTED LE HA SUGERIDO BUSCAR AYUDA POR EL CONSUMO EXCESIVO DE TABACO?',
    '¿DESEARÍA PERTENECER A UN GRUPO DE APOYO PARA APRENDER A MANEJAR LAS DROGAS SOCIALES?'
  ],
  tenenciaAspectoVivienda: [
    'Arrendada',
    'Propia',
    'Antícresis',
    'Prestada',
    'Por servicio'
  ],
  tipoAspectoVivienda: [
    'Departamento',
    'Casa',
    'Media agua',
    'Cuarto'
  ],
  estructuraAspectoVivienda: [
    'Hormigón',
    'Ladrillo',
    'Adobe',
    'Madera',
    'Mixta'
  ],
  detalleProblemaDetectado: [
    'Ninguno',
    'Discapacidad',
    'Núcleo familiar en riesgo',
    'Abandono del hogar',
    'Subempleo',
    'Deseempleo',
    'Promiscuidad',
    'Violencia intra familiar',
    'Maltrato físico',
    'Maltrato psicológico',
    'Maltrato sexual',
    'Migración',
    'Consumo de drogas sociales',
    'Hacinamiento',
    'Ausencia de roles',
    'Carencia de recursos económicos'
  ],
  codigosRegion: [
    '',
    '02',
    '03',
    '04',
    '05',
    '06',
    '07'
  ],
  jornadasLaborales: [
    'TIEMPO COMPLETO',
    'TIEMPO PARCIAL',
    'MEDIO TIEMPO'
  ],
  situacionesAdministrativas: [
    'En servicio activo',
    'Inactivo',
    'En licencia por enfermedad',
    'En licencia por embarazo o maternidad',
    'En licencia por asuntos propios',
    'En licencia por estudios',
    'En licencia por cooperación y ayuda humanitaria',
    'En licencia por participación en programas gubernamentales',
    'En licencia por participación en programas gubernamentales',
    'En permiso familiar personal, familiar laboral',
    'En comisión de servicios',
    'En comisión de adelanto de estudios',
    'En comisión para desempeñar un cargo de libre nombramiento y remoción',
    'En comisión para atender invitaciones',
    'Encargo',
    'Préstamo de servicio militar',
    'Vacaciones',
    'Suspendido en ejercicio de funciones',
    'Otras',
  ],
  razonConsultaCatalogo: [
    'Enfermedad Común',
    'Enfermedad Profesional',
    'Accidente Común',
    'Accidente Laboral',
    'Seguimiento',
    'Exploración de rutina',
    'Ficha Ocupacional - Inicio',
    'Ficha Ocupacional - Periódica',
    'Ficha Ocupacional - Final',
    'Ficha Ocupacional - Reubicación',
    'Ficha Ocupacional - Reingreso'
  ],
  unidadesRequirientes: [
    'Medicina general',
    'Salud ocupacional',
    'Odontología',
    'Psicología'
  ],
  tiposReferencia: [
    'No aplica',
    'Referencia',
    'Contrareferencia'
  ],
  tiposInterconsulta: [
    'No aplica',
    'Solicitada',
    'Recibida'
  ],
  ciclosMenstruacion: [
    'REGULARES',
    'IRREGULARES'
  ],
  frecuenciaPrescripcionFarmacologica: [
    '1 vez al día',
    '1 vez al día por la mañana',
    '1 vez al día por la tarde',
    '1 vez al día al acostarse',
    '2 veces al día',
    '3 veces al día',
    '3 veces al día, antes de las comidas',
    '3 veces al día, con las comidas',
    '4 veces al día',
    'Cada 2 horas',
    'Cada 4 horas',
    'Cada 6 horas',
    'Cada 12 horas',
    'Cada 2 días',
    '1 vez por semana',
    'Otro',
  ],
  presentacionMedicamentos: [
    'AMPOLLA',
    'AMPOLLA BEBIBLE',
    'CÁPSULA',
    'COMPRIMIDO',
    'CREMA',
    'GRANULADO',
    'INHALADOR',
    'JARABE',
    'LINIMENTO',
    'PARCHE',
    'PASTA',
    'POLVO',
    'TABLETA',
    'TABLETA SOLUBLE',
    'SOBRE',
    'SOLUCIÓN ORAL',
    'SOLUCIÓN INYECTABLE',
    'SUPOSITORIO',
    'FRASCO',
    'CAJA',
    'ÓVULO'
  ],
};
