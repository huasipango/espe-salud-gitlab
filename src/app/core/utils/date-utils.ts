import {DatePipe} from '@angular/common';

const pipe = new DatePipe('es-419');

export const DateUtil = {

  customFormatDate(date: string): string {
    return pipe.transform(date, 'yyyy-MM-dd');
  },
  showDateFormat(date: string): string {
    return pipe.transform(date, 'longDate');
  },
  showDateTimeFormat(date: string): string {
    return pipe.transform(date, 'medium');
  },

  fechaReal(date: string): Date {
    if (date) {
      return new Date(date.toLocaleString().replace(/-/g, '/'));
    } else {
      return new Date();
    }
  },

  getTime(date: string): string {
    return pipe.transform(date, 'HH:mm:ss');
  }
};

export function setCustomFormatDateTime(date: Date): string {
  return pipe.transform(date, 'yyyy-MM-dd HH:mm:ss');
}

export function getShortDateTime(date: Date): string {
  return pipe.transform(date, 'short');
}

export function getLongDate(date: Date): string {
  return pipe.transform(date, 'longDate');
}

export function getShortDate(date: Date): string {
  return pipe.transform(date, 'shortDate');
}

export function getMediumDate(date: Date): string {
  return pipe.transform(date, 'medium');
}

export function isToday(fechaInicio: Date): boolean {
  const today = new Date()
  const date = new Date(fechaInicio);
  return date.getDate() === today.getDate() &&
    date.getMonth() === today.getMonth() &&
    date.getFullYear() === today.getFullYear()
}
