import {MONTHS_OF_YEAR_ABR} from 'src/app/core/constants/constants';
import {ApexOptions} from 'src/@vex/components/chart/chart.component';
import {mergeDeep} from 'src/@vex/utils/merge-deep';

export const defaultDashboardChartOptions = (options: Partial<ApexOptions> = {}): ApexOptions => mergeDeep({
  grid: {
    show: true,
    strokeDashArray: 3,
    padding: {
      left: 16
    },
    yaxis: {
      lines: {
        show: true
      }
    }
  },
  chart: {
    parentHeightOffset: 0,
    height: 384,
    type: 'area',
    toolbar: {
      show: true,
    },
    zoom: {
      enabled: false
    },
    sparkline: {
      enabled: false
    }
  },
  labels: [],
  // dataLabels: {
  //   enabled: false
  // },
  stroke: {
    curve: 'smooth',
    width: 2.5
  },
  fill: {
    type: 'gradient',
    gradient: {
      shadeIntensity: 0.9,
      opacityFrom: 0.7,
      opacityTo: 0.5,
      stops: [0, 90, 100]
    }
  },
  xaxis: {
    type: 'category',
    labels: {
      show: true,
      style: {
        cssClass: 'text-secondary fill-current caption font-medium',
        fontFamily: 'inherit'
      }
    },
    axisBorder: {
      show: true,
      color: '#EEEEEE'
    },
    axisTicks: {
      show: false
    },
    floating: false,
    tooltip: {
      enabled: false,
    },
  },
  yaxis: {
    labels: {
      show: true,
      formatter(val) {
        return val.toFixed(0);
      },
      style: {
        cssClass: 'text-secondary fill-current caption font-medium',
        fontFamily: 'inherit'
      }
    },
    axisBorder: {
      show: false
    },
    axisTicks: {
      show: false
    },
  },
  noData: {
    text: 'Cargando...'
  },
  legend: {
    position: 'top',
    fontFamily: 'inherit',
    horizontalAlign: 'left',
    offsetX: -18,
    itemMargin: {
      horizontal: 4,
      vertical: 4
    },
    markers: {
      radius: 4,
      width: 12,
      height: 12
    },
    labels: {
      colors: ['var(--text-secondary-color)']
    }
  },
  tooltip: {
    x: { show: false }
  },
}, options);
