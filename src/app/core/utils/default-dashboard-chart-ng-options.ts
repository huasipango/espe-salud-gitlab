import {mergeDeep} from 'src/@vex/utils/merge-deep';
import {ApexOptions} from 'src/@vex/components/chart-ng/chart-ng.component';

export const defaultDashboardChartNgOptions = (options: Partial<ApexOptions> = {}): ApexOptions => mergeDeep({
  grid: {
    show: true,
    strokeDashArray: 3,
    padding: {
      left: 16
    },
    yaxis: {
      lines: {
        show: true
      }
    }
  },
  stroke: {
    curve: 'smooth',
    width: 2.5
  },
  chart: {
    parentHeightOffset: 0,
    type: 'bar',
    height: 350,
    stacked: false,
    toolbar: {
      show: true
    },
    zoom: {
      enabled: false
    },
    sparkline: {
      enabled: false
    }
  },
  responsive: [
    {
      breakpoint: 480,
      options: {
        legend: {
          position: 'bottom',
          offsetX: -10,
          offsetY: 0
        }
      }
    }
  ],
  plotOptions: {
    bar: {
      horizontal: false
    }
  },
  labels: [],
  dataLabels: {
    enabled: true
  },
  xaxis: {
    type: 'category',
    labels: {
      show: true,
      style: {
        cssClass: 'text-secondary fill-current caption font-medium',
        fontFamily: 'inherit'
      }
    },
    axisBorder: {
      show: true,
      color: '#EEEEEE'
    },
    axisTicks: {
      show: false
    },
    floating: false,
    tooltip: {
      enabled: false,
    },
  },
  yaxis: {
    labels: {
      show: true,
      formatter(val) {
        return val.toFixed(0);
      },
      style: {
        cssClass: 'text-secondary fill-current caption font-medium',
        fontFamily: 'inherit'
      }
    },
    axisBorder: {
      show: false
    },
    axisTicks: {
      show: false
    },
  },
  noData: {
    text: 'Cargando...'
  },
  legend: {
    position: 'top',
    fontFamily: 'inherit',
    horizontalAlign: 'left',
    offsetX: -18,
    itemMargin: {
      horizontal: 4,
      vertical: 4
    },
    markers: {
      radius: 4,
      width: 12,
      height: 12
    },
    labels: {
      colors: ['var(--text-secondary-color)']
    }
  },
  tooltip: {
    x: { show: false }
  },
}, options);

