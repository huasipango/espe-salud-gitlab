import { DateTime } from 'luxon';

export function createDateMonthArray(length: number, year?: number) {
  const dates: number[] = [];
  const date = DateTime.local(year + 1, 1, 1, 0, 0);
  for (let i = 1; i < length; i++) {
    dates.push(+date.minus({ month: i }).toJSDate());
  }
  return dates.reverse();
}

