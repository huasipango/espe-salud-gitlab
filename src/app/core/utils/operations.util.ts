import moment from 'moment';
import {BASE64SPLIT} from 'src/app/core/constants/constants';

export function calculateIMC(pesoKg: number, tallaCm: number): string {
  const tallaMetros = tallaCm / 100;
  return (pesoKg / (tallaMetros * tallaMetros)).toFixed(2);
}

export function calculateMonthsDiff(fechaInicio: any): number {
  const today = new Date();
  const initialDate = new Date(fechaInicio);
  return moment(today).diff(initialDate, 'months', true);
}

export function calculateAge(fechaNacimiento: any): number {
  const today = new Date();
  const birthDate = new Date(fechaNacimiento);
  let age = today.getFullYear() - birthDate.getFullYear();
  const m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }
  return age;
}

export function calculateDaysDiff(fechaInicio: any, fechaFin: any): number {
  let today = new Date();
  if (fechaFin) { today = fechaFin; }
  const initialDate = new Date(fechaInicio);
  return moment(today).diff(initialDate, 'days', true);
}

export function calculateDaysEmb(fechaInicio: any): number {
  const today = new Date();
  const initialDate = new Date(fechaInicio);
  return moment(today).diff(initialDate, 'days', true);
}

export function calculateWeeksDiff(fechaInicio: any): number {
  const today = new Date();
  const initialDate = new Date(fechaInicio);
  return moment(today).diff(initialDate, 'weeks', true);
}

export function calculateYearsDiff(fechaInicio: any): number {
  const today = new Date();
  const initialDate = new Date(fechaInicio);
  return moment(today).diff(initialDate, 'years', true);
}

export function checkIfAllObjectValuesAreNull(object: any): boolean {
  // Retorna true si el objeto tiene al menos un atributo !== null
  return !Object.values(object).every(o => o === null);
}

export function getFullAge(fechaNacimiento: any): string {
  const today = moment();
  const birthday = moment(fechaNacimiento);

  const years = today.diff(birthday, 'year');
  birthday.add(years, 'years');

  const months = today.diff(birthday, 'months');
  birthday.add(months, 'months');

  const days = today.diff(birthday, 'days');
  return years + ' año(s), ' + months + ' mes(es), '  + days + ' dia(s)';
}

export function calculatePercentageDifference(pastNumber: number, newNumber: number): number {
  if (pastNumber === 0) { return 0; }
  return Number((((newNumber - pastNumber) / pastNumber) * 100).toFixed(2));
}

export function addMonthToDate(months: number) {
  const date = new Date()
  date.setMonth(date.getMonth() + months)

  return date
}

export function splitImageFile(base64: string): string {
  const partSeparator = BASE64SPLIT;
  if (base64.includes(partSeparator)) {
    return base64.split(partSeparator)[1];
  }
  return '';
}
