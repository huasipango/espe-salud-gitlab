import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Error403RoutingModule } from './error-403-routing.module';
import {Error403Component} from 'src/app/core/errors/error-403/error-403.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {IconModule} from '@visurel/iconify-angular';
import {MatButtonModule} from "@angular/material/button";


@NgModule({
  declarations: [Error403Component],
    imports: [
        CommonModule,
        Error403RoutingModule,
        FlexLayoutModule,
        IconModule,
        MatButtonModule
    ]
})
export class Error403Module { }
