import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {VexRoutes} from 'src/@vex/interfaces/vex-route.interface';
import {Error403Component} from 'src/app/core/errors/error-403/error-403.component';

const routes: VexRoutes = [
  {
    path: '',
    component: Error403Component,
    data: {
      containerEnabled: true,
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Error403RoutingModule { }
