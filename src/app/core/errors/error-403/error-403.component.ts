import { Component, OnInit } from '@angular/core';
import icSearch from '@iconify/icons-ic/twotone-search';
import {Router} from '@angular/router';
import icHome from '@iconify/icons-fa-solid/home';

@Component({
  selector: 'vex-error403',
  templateUrl: './error-403.component.html',
  styleUrls: ['./error-403.component.scss']
})
export class Error403Component implements OnInit {

  icSearch = icSearch;
  icHome = icHome;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  gotToHome() {
    this.router.navigate(['/']);
  }
}
