import moment from 'moment';
import {Month} from 'src/app/core/models/dashboard/month.model';

export const FORM_ERROR_MESSAGES = {
  required: 'Este campo es requerido',
  min: 'Valor fuera del rango mínimo',
  max: 'Valor fuera del rango máximo',
  pattern: 'Valor no cumple con el formato',
  forbiddenObject: 'El valor ingresado no es válido'
};

export const DAYS_PLUS_TO_VAL_CERT = 1;
export const zoneString = 'America/Guayaquil';
export const localString = 'es-419';
export const MONTHS_OF_YEAR_ABR = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
export const ECUADOR = 'ECUADOR';
export const YEARS = () => {
  const years = [];
  const dateStart = moment().subtract(3, 'year');
  const dateEnd = moment().add(10, 'y');
  while (dateEnd.diff(dateStart, 'years') >= 0) {
    years.push(Number(dateStart.format('YYYY')));
    dateStart.add(1, 'year');
  }
  return years;
};
export const MONTHS: Month[] = [
  { id: 1, name: 'Enero' },
  { id: 2, name: 'Febrero' },
  { id: 3, name: 'Marzo' },
  { id: 4, name: 'Abril' },
  { id: 5, name: 'Mayo' },
  { id: 6, name: 'Junio' },
  { id: 7, name: 'Julio' },
  { id: 8, name: 'Agosto' },
  { id: 9, name: 'Septiembre' },
  { id: 10, name: 'Octubre' },
  { id: 11, name: 'Noviembre' },
  { id: 12, name: 'Diciembre' },
];
export const MAX_AGE_TO_CI_REPRESENANTE = 4;
export const ECUADOR_COUNTRY_NAME = 'ECUADOR';
export const ECUADOR_NACIONALIDAD_NAME = 'ECUATORIANA';
export const ONLY_NUMBERS_REGEX = '[0-9]*\\.?[0-9]{1}';
export const BASE64SPLIT = ',';
