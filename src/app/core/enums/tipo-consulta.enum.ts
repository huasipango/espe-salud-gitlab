export enum TipoConsulta {
  presencial = 'PRESENCIAL',
  virtual = 'VIRTUAL'
}

export const TipoConsultaColor = [
  {
    type: TipoConsulta.presencial,
    classes: ['bg-teal-light', 'text-teal']

  },
  {
    type: TipoConsulta.virtual,
    classes: ['bg-purple-light', 'text-purple']
  }
];
