export enum LateralidadEnum {
  diestro = 'DIESTRO',
  zurdo = 'ZURDO',
  ambidiestro = 'AMBIDIESTRO'
}
