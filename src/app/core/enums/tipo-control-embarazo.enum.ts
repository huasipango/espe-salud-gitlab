export enum TipoControlEmbarazoEnum {
  GESTACION = 'GESTACION',
  LACTANCIA = 'LACTANCIA'
}
