export enum RiesgoLaboral {
  fisico= 'FISICO',
  quimico= 'QUIMICO',
  mecanico= 'MECANICO',
  ergonomico= 'ERGONOMICO',
  biologico= 'BIOLOGICO',
  psicosocial= 'PSICOSOCIAL'
}
