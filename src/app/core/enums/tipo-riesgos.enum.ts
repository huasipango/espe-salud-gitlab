export enum RiesgoLaboral{
  fisico = 'FISICO',
  quimico = 'QUIMICO',
  mecanico= 'MECANICO',
  eggonomico= 'ERGONOMICO',
  biologico= 'BIOLOGICO',
  psicosocial= 'PSICOSOCIAL'
}
