export enum SexoEnum {
  MUJER = 'M',
  HOMBRE = 'H',
  LGBT = 'LGBT'
}
