export enum EstadoEvolucionEnum {
  BORRADOR = 'BORRADOR',
  ATENDIENDO = 'ATENDIENDO',
  FINALIZADO = 'FINALIZADO'
}
