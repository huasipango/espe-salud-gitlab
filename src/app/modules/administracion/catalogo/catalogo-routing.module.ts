import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {VexRoutes} from 'src/@vex/interfaces/vex-route.interface';
import { RoleGuard } from 'src/app/core/auth/role.guard';
import { RolEnum } from 'src/app/core/enums/rol-enum';

const routes: VexRoutes = [
  {
    path: 'areas',
    canActivate: [RoleGuard],
    loadChildren: () => import('./components/catalogo-area-table/catalogo-area-table.module').then(m => m.CatalogoAreaTableModule),
    data: {
      containerEnabled: true,
      roles: [RolEnum.ROLE_ADMIN]
    }
  },
  {
    path: 'asociaciones',
    canActivate: [RoleGuard],
    loadChildren: () => import('./components/catalogo-asociacion-table/catalogo-asociacion-table.module')
      .then(m => m.CatalogoAsociacionTableModule),
    data: {
      containerEnabled: true,
      roles: [RolEnum.ROLE_ADMIN]
    }
  },
  {
    path: 'dispensarios',
    canActivate: [RoleGuard],
    loadChildren: () => import('./components/catalogo-dispensario-table/catalogo-dispensario-table.module')
      .then(m => m.CatalogoDispensarioTableModule),
    data: {
      containerEnabled: true,
      roles: [RolEnum.ROLE_ADMIN]
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogoRoutingModule { }
