import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {CatalogoRoutingModule} from 'src/app/modules/administracion/catalogo/catalogo-routing.module';

@NgModule({
  declarations: [],
    imports: [
      CommonModule,
      CatalogoRoutingModule
    ]
})
export class CatalogoModule { }
