import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import icAdd from '@iconify/icons-ic/twotone-add';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icMap from '@iconify/icons-ic/twotone-map';
import icMail from '@iconify/icons-ic/twotone-mail';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import {CatalogoDispensarioModalComponent} from 'src/app/modules/administracion/catalogo/components/catalogo-dispensario-table/catalogo-dispensario-modal/catalogo-dispensario-modal.component';
import {Dispensario} from 'src/app/core/models/catalogo/dispensario.model';

@Component({
  selector: 'vex-catalogo-dispensario-table',
  templateUrl: './catalogo-dispensario-table.component.html',
  styleUrls: ['./catalogo-dispensario-table.component.scss']
})
export class CatalogoDispensarioTableComponent implements OnInit, AfterViewInit {

  data: Dispensario[];
  columns: TableColumn<Dispensario>[] = [
    {label: 'Imagen', property: 'nombreImagen', type: 'image', visible: true},
    {label: 'institucionSistema', property: 'institucionSistema', type: 'text', visible: false},
    {label: 'Unidad Operativa', property: 'unidadOperativa', type: 'text', visible: true},
    {label: 'Teléfono', property: 'telefono', type: 'button', visible: true },
    {label: 'Fax', property: 'fax', type: 'text', visible: true},
    {label: 'Provincia', property: 'provincia', type: 'text', visible: true},
    {label: 'Cantón', property: 'canton', type: 'text', visible: true},
    {label: 'Acciones', property: 'menu', type: 'button', visible: true}
  ];
  pageSize = 5;
  icMap = icMap;
  icMail = icMail;
  icPhone = icPhone;
  icAdd = icAdd;
  icEdit = icEdit;
  icMoreHoriz = icMoreHoriz;
  icDelete = icDelete;
  dataSource = new MatTableDataSource<Dispensario>();
  userMessages = USER_MESSAGES;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(
    private catalogoService: CatalogoService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    this.catalogoService.getDispensarios()
      .subscribe((response) => {
        this.data = response;
        this.dataSource.data = response;
      });
  }

  get visibleColumns() {
    return this.columns
      .filter(column => column.visible)
      .map(column => column.property);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  create(): void {
    this.dialog.open(CatalogoDispensarioModalComponent, {
      maxWidth: '100%',
      width: '700px',
      disableClose: true
    }).afterClosed()
      .subscribe(value => {
        if (value) {
          this.catalogoService.createDispensario(value)
            .subscribe((response) => {
              if (response){
                this.showNotification(this.userMessages.createdSuccessMessage);
                this.getData();
              }
            }, (error) => {
              this.showNotification(this.userMessages.createdFailedMessage);
            });
        }
      });
  }

  update(item: Dispensario): void {
    this.dialog.open(CatalogoDispensarioModalComponent, {
      data: item,
      maxWidth: '100%',
      width: '700px',
      disableClose: true
    }).afterClosed()
      .subscribe(value => {
        if (value) {
          this.catalogoService.updateDispensario(value, value.id)
            .subscribe((response) => {
              if (response){
                this.showNotification(this.userMessages.updatedSuccessMessage);
                this.getData();
              }
            }, (error) => {
              this.showNotification(this.userMessages.updatedFailedMessage);
            });
        }
      });
  }

  openDeleteDialog(item: Dispensario): void {
    this.dialog.open(DeleteModalComponent, {
      data: this.userMessages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') { this.delete(item); }
    });
  }

  delete(item: Dispensario): void {
    this.catalogoService.deleteDispensario(item.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification(this.userMessages.deletedSuccessMessage);
          this.data.splice(
            this.data.findIndex((existing) =>
              existing.id === item.id), 1
          );
          this.dataSource.connect().next(this.data);
          this.getData();
        } else {
          this.showNotification(this.userMessages.deleteFailMessage);
        }
      }, error => {
        this.showNotification(this.userMessages.deleteFailMessage);
      });
  }

  showNotification(message: string) {
    this.snackbar.open(message, 'CERRAR', {
      duration: 5000
    });
  }

  getUserImage(nombreImagen: string): string {
    return this.catalogoService.getDispensarioImage(nombreImagen);
  }

  sendMail(correo: string): void {
    const subject = 'CONTACTO';
    window.location.href = `mailto:${correo}?subject=${subject}&body=Hola`;
  }
}
