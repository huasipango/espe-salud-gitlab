import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogoDispensarioTableComponent } from './catalogo-dispensario-table.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {FlexLayoutModule, FlexModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {IconModule} from '@visurel/iconify-angular';
import {MatMenuModule} from '@angular/material/menu';
import { CatalogoDispensarioModalComponent } from './catalogo-dispensario-modal/catalogo-dispensario-modal.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {ReactiveFormsModule} from '@angular/forms';
import {MatDividerModule} from '@angular/material/divider';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {CatalogoDispensarioTableRoutingModule} from 'src/app/modules/administracion/catalogo/components/catalogo-dispensario-table/catalogo-dispensario-table-routing.module';
import {PageLayoutModule} from 'src/@vex/components/page-layout/page-layout.module';
import {SecondaryToolbarModule} from 'src/@vex/components/secondary-toolbar/secondary-toolbar.module';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {ContainerModule} from 'src/@vex/directives/container/container.module';



@NgModule({
    declarations: [CatalogoDispensarioTableComponent, CatalogoDispensarioModalComponent],
    exports: [
        CatalogoDispensarioTableComponent
    ],
  imports: [
    CommonModule,
    CatalogoDispensarioTableRoutingModule,
    MatPaginatorModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    MatSortModule,
    IconModule,
    MatMenuModule,
    MatTooltipModule,
    MatCardModule,
    MatDialogModule,
    FlexModule,
    ReactiveFormsModule,
    MatDividerModule,
    IconModule,
    MatInputModule,
    MatSelectModule,
    PageLayoutModule,
    SecondaryToolbarModule,
    BreadcrumbsModule,
    ContainerModule
  ]
})
export class CatalogoDispensarioTableModule { }
