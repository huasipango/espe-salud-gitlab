import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuicklinkModule } from 'ngx-quicklink';
import {CatalogoDispensarioTableComponent} from 'src/app/modules/administracion/catalogo/components/catalogo-dispensario-table/catalogo-dispensario-table.component';

const routes: Routes = [
  {
    path: '',
    component: CatalogoDispensarioTableComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule, QuicklinkModule]
})
export class CatalogoDispensarioTableRoutingModule {
}
