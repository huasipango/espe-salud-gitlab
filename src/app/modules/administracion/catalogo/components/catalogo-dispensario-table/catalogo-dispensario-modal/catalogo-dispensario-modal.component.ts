import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import icClose from '@iconify/icons-ic/twotone-close';
import icMail from '@iconify/icons-ic/twotone-mail';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icCheck from '@iconify/icons-ic/twotone-check';
import icMobile from '@iconify/icons-ic/phone-iphone';
import icAddress from '@iconify/icons-ic/location-on';
import icCollege from '@iconify/icons-fa-solid/school';
import icIdEspe from '@iconify/icons-fa-solid/id-card-alt';
import icLocationCity from '@iconify/icons-ic/twotone-location-city';
import icCap from '@iconify/icons-fa-solid/graduation-cap';
import icSecuridad from '@iconify/icons-ic/security';
import {Dispensario} from 'src/app/core/models/catalogo/dispensario.model';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {Observable} from 'rxjs';
import {CatalogoBannerService} from 'src/app/core/services/catalogo-banner.service';
import {Canton} from 'src/app/core/models/catalogo-banner/canton.model';
import {Provincia} from 'src/app/core/models/catalogo-banner/provincia,model';

@Component({
  selector: 'vex-catalogo-dispensario-modal',
  templateUrl: './catalogo-dispensario-modal.component.html',
  styleUrls: ['./catalogo-dispensario-modal.component.scss']
})
export class CatalogoDispensarioModalComponent implements OnInit {

  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  errorMessages = FORM_ERROR_MESSAGES;
  icPhone = icPhone;
  icIdEspe = icIdEspe;
  icSecuridad = icSecuridad;
  icCollege = icCollege;
  icMobile = icMobile;
  icMail = icMail;
  icLocationCity = icLocationCity;
  icAddress = icAddress;
  icCap = icCap;
  icClose = icClose;
  icCheck = icCheck;
  nameDispensario$: Observable<string>;
  cantonesContacto: Canton[] = [];
  provincias: Provincia[] = [];

  constructor(
    private dialogRef: MatDialogRef<CatalogoDispensarioModalComponent>,
    @Inject(MAT_DIALOG_DATA) public defaults: Dispensario,
    private fb: FormBuilder,
    private catalogoService: CatalogoService,
    private catalogoBannerService: CatalogoBannerService
  ) { }

  ngOnInit(): void {
    this.nameDispensario$ = this.catalogoService.getDispensarioNmesImage();
    this.catalogoBannerService.getProvincias()
      .subscribe((data) => {
        this.provincias = data;
      });
    this.catalogoBannerService.getCantones()
      .subscribe((data: Canton[]) => {
        this.cantonesContacto = data;
      });
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as Dispensario;
    }
    this.form = this.fb.group({
      id: this.defaults.id || null,
      institucionSistema: [this.defaults.institucionSistema || '', Validators.required],
      unidadOperativa: [this.defaults.unidadOperativa || '', Validators.required],
      uniCodigo: [this.defaults.uniCodigo || '', Validators.required],
      nombreImagen: [this.defaults.nombreImagen || '', Validators.required],
      direccion: [this.defaults.direccion || '', Validators.required],
      telefono: [this.defaults.telefono || '', Validators.required],
      fax: [this.defaults.fax || '', Validators.required],
      campus: [this.defaults.campus || '', Validators.required],
      canton: [this.defaults.canton || '', Validators.required],
      provincia: [this.defaults.provincia || '', Validators.required],
      correo: [this.defaults.correo || '', Validators.required],
    });
  }

  save() {
    const formObject = this.form.value;
    if (this.mode === 'create') {
      this.create(formObject);
    } else if (this.mode === 'update') {
      this.update(formObject);
    }
  }

  create(formObject: any) {
    this.dialogRef.close(formObject);
  }

  update(formObject: any) {
    this.dialogRef.close(formObject);
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  searchCantonesContacto(event: any) {
    const idProvincia: number = event;
    this.catalogoBannerService.getCantonesByProvincia(idProvincia)
      .subscribe((data) => {
        this.cantonesContacto = data;
      });
  }
}
