import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogoAreaTableComponent } from './catalogo-area-table.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {IconModule} from '@visurel/iconify-angular';
import {MatMenuModule} from '@angular/material/menu';
import {CatalogoAreaTableRoutingModule} from 'src/app/modules/administracion/catalogo/components/catalogo-area-table/catalogo-area-table-routing.module';
import {PageLayoutModule} from 'src/@vex/components/page-layout/page-layout.module';
import {SecondaryToolbarModule} from 'src/@vex/components/secondary-toolbar/secondary-toolbar.module';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {ContainerModule} from 'src/@vex/directives/container/container.module';


@NgModule({
  declarations: [CatalogoAreaTableComponent],
  exports: [
    CatalogoAreaTableComponent
  ],
  imports: [
    CommonModule,
    CatalogoAreaTableRoutingModule,
    MatPaginatorModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    MatSortModule,
    IconModule,
    MatMenuModule,
    PageLayoutModule,
    SecondaryToolbarModule,
    BreadcrumbsModule,
    ContainerModule
  ]
})
export class CatalogoAreaTableModule { }
