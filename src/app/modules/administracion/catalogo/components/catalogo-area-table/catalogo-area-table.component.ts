import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Area} from 'src/app/modules/medicina-general/evolucion/models/area.model';
import icAdd from '@iconify/icons-ic/twotone-add';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import {Region} from 'src/app/modules/medicina-general/evolucion/models/region.model';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger20ms} from 'src/@vex/animations/stagger.animation';

@Component({
  selector: 'vex-catalogo-area-table',
  templateUrl: './catalogo-area-table.component.html',
  styleUrls: ['./catalogo-area-table.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger20ms
  ]
})
export class CatalogoAreaTableComponent implements OnInit, AfterViewInit {
  data: Area[];
  columns: TableColumn<Area>[] = [
    {label: 'ÁREA', property: 'nombre', type: 'text', visible: true},
    {label: 'REGIÓN', property: 'region', type: 'object', visible: true},
    {label: 'ACCIONES', property: 'menu', type: 'button', visible: true}
  ];
  pageSize = 5;
  icAdd = icAdd;
  icEdit = icEdit;
  icMoreHoriz = icMoreHoriz;
  icDelete = icDelete;

  dataSource = new MatTableDataSource<Area>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private catalogoService: CatalogoService) {
  }

  ngOnInit(): void {
    this.catalogoService.getAreas()
      .subscribe((response) => {
        this.dataSource.data = response;
      });
  }

  get visibleColumns() {
    return this.columns
      .filter(column => column.visible)
      .map(column => column.property);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  update(item: any) {
  }

  delete(item: any) {
  }

  getRegion(rowElement: Region) {
    return rowElement ? rowElement.nombre : '';
  }
}
