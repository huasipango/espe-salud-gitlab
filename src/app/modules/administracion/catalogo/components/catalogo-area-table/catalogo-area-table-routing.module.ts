import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuicklinkModule } from 'ngx-quicklink';
import {CatalogoAreaTableComponent} from 'src/app/modules/administracion/catalogo/components/catalogo-area-table/catalogo-area-table.component';

const routes: Routes = [
  {
    path: '',
    component: CatalogoAreaTableComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule, QuicklinkModule]
})
export class CatalogoAreaTableRoutingModule {
}
