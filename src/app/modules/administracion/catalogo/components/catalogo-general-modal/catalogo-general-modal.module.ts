import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogoGeneralModalComponent } from './catalogo-general-modal.component';
import {MatDialogModule} from '@angular/material/dialog';
import {ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import {MatInputModule} from '@angular/material/input';
import {UppercaseModule} from 'src/app/shared/directives/uppercase/uppercase.module';



@NgModule({
  declarations: [CatalogoGeneralModalComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatIconModule,
    IconModule,
    MatButtonModule,
    MatDividerModule,
    MatInputModule,
    UppercaseModule
  ]
})
export class CatalogoGeneralModalModule { }
