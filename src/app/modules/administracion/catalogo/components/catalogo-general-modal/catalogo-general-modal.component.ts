import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActividadFisica} from 'src/app/modules/medicina-general/antecedente-clinico/models/actividad-fisica.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import icClose from '@iconify/icons-ic/twotone-close';

@Component({
  selector: 'vex-catalogo-general-modal',
  templateUrl: './catalogo-general-modal.component.html',
  styleUrls: ['./catalogo-general-modal.component.scss']
})
export class CatalogoGeneralModalComponent implements OnInit {
  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  errorMessages = FORM_ERROR_MESSAGES;
  icClose = icClose;
  constructor(
    private dialogRef: MatDialogRef<CatalogoGeneralModalComponent>,
    @Inject(MAT_DIALOG_DATA) public defaults: any,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as any;
    }
    this.form = this.fb.group({
      id: this.defaults.id || null,
      nombre: [this.defaults.nombre || '', Validators.required],
    });
  }

  save() {
    const formObject = this.form.value;
    if (this.mode === 'create') {
      this.create(formObject);
    } else if (this.mode === 'update') {
      this.update(formObject);
    }
  }

  create(formObject: any) {
    this.dialogRef.close(formObject);
  }

  update(formObject: any) {
    this.dialogRef.close(formObject);
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

}
