import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogoSeguroSaludTableComponent } from './catalogo-seguro-salud-table.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {FlexLayoutModule, FlexModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {IconModule} from '@visurel/iconify-angular';
import {MatMenuModule} from '@angular/material/menu';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {ReactiveFormsModule} from '@angular/forms';
import {MatDividerModule} from '@angular/material/divider';
import {MatInputModule} from '@angular/material/input';



@NgModule({
  declarations: [CatalogoSeguroSaludTableComponent],
  exports: [
    CatalogoSeguroSaludTableComponent
  ],
  imports: [
    CommonModule,
    MatPaginatorModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    MatSortModule,
    IconModule,
    MatMenuModule,
    MatTooltipModule,
    MatCardModule,
    MatDialogModule,
    FlexModule,
    ReactiveFormsModule,
    MatDividerModule,
    IconModule,
    MatInputModule,
  ]
})
export class CatalogoSeguroSaludTableModule { }
