import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
// eslint-disable-next-line max-len
import {CatalogoGeneralModalComponent} from 'src/app/modules/administracion/catalogo/components/catalogo-general-modal/catalogo-general-modal.component';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import icAdd from '@iconify/icons-ic/twotone-add';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import {SeguroSalud} from 'src/app/core/models/catalogo/seguro-salud.model';
@Component({
  selector: 'vex-catalogo-seguro-salud-table',
  templateUrl: './catalogo-seguro-salud-table.component.html',
  styleUrls: ['./catalogo-seguro-salud-table.component.scss']
})
export class CatalogoSeguroSaludTableComponent implements OnInit, AfterViewInit {

  data: SeguroSalud[];
  columns: TableColumn<SeguroSalud>[] = [
    {label: 'NOMBRE', property: 'nombre', type: 'text', visible: true},
    {label: 'ACCIONES', property: 'menu', type: 'button', visible: true}
  ];
  pageSize = 5;
  icAdd = icAdd;
  icEdit = icEdit;
  icMoreHoriz = icMoreHoriz;
  icDelete = icDelete;
  dataSource = new MatTableDataSource<SeguroSalud>();
  userMessages = USER_MESSAGES;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(
    private catalogoService: CatalogoService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    this.catalogoService.getSegurosSalud()
      .subscribe((response) => {
        this.data = response;
        this.dataSource.data = response;
      });
  }

  get visibleColumns() {
    return this.columns
      .filter(column => column.visible)
      .map(column => column.property);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  create(): void {
    this.dialog.open(CatalogoGeneralModalComponent, {
      maxWidth: '100%',
      width: '500px',
      disableClose: true
    }).afterClosed()
      .subscribe(value => {
        if(value) {
          this.catalogoService.createSeguroSalud(value)
            .subscribe((response) => {
              if (response){
                this.showNotification(this.userMessages.createdSuccessMessage);
                this.getData();
              }
            }, (error) => {
              this.showNotification(this.userMessages.createdFailedMessage);
            });
        }
      });
  }

  update(item: SeguroSalud): void {
    this.dialog.open(CatalogoGeneralModalComponent, {
      data: item,
      maxWidth: '100%',
      width: '500px',
      disableClose: true
    }).afterClosed()
      .subscribe(value => {
        if (value) {
          this.catalogoService.updateSeguroSalud(value, value.id)
            .subscribe((response) => {
              if (response){
                this.showNotification(this.userMessages.createdSuccessMessage);
                this.getData();
              }
            }, (error) => {
              this.showNotification(this.userMessages.updatedFailedMessage);
            });
        }
      });
  }

  openDeleteDialog(item: SeguroSalud): void {
    this.dialog.open(DeleteModalComponent, {
      data: this.userMessages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') { this.delete(item); }
    });
  }

  delete(item: SeguroSalud): void {
    this.catalogoService.deleteSeguroSalud(item.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification(this.userMessages.deletedSuccessMessage);
          this.data.splice(
            this.data.findIndex((existing) =>
              existing.id === item.id), 1
          );
          this.dataSource.connect().next(this.data);
        } else {
          this.showNotification(this.userMessages.deleteFailMessage);
        }
      }, error => {
        this.showNotification(this.userMessages.deleteFailMessage);
      });
  }

  showNotification(message: string) {
    this.snackbar.open(message, 'CERRAR', {
      duration: 5000
    });
  }

}
