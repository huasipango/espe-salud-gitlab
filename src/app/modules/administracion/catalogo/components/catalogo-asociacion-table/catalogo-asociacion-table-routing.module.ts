import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuicklinkModule } from 'ngx-quicklink';
import {CatalogoAsociacionTableComponent} from 'src/app/modules/administracion/catalogo/components/catalogo-asociacion-table/catalogo-asociacion-table.component';

const routes: Routes = [
  {
    path: '',
    component: CatalogoAsociacionTableComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule, QuicklinkModule]
})
export class CatalogoAsociacionTableRoutingModule {
}
