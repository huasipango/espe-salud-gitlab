import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuarioCreateComponent } from './components/usuario-create/usuario-create.component';
import {UsuarioRoutingModule} from 'src/app/modules/administracion/usuario/usuario-routing.module';
import {UsuarioComponent} from 'src/app/modules/administracion/usuario/usuario.component';
import {IconModule} from '@visurel/iconify-angular';
import {MatTabsModule} from '@angular/material/tabs';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatTooltipModule} from '@angular/material/tooltip';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {UsuarioCardModule} from 'src/app/modules/administracion/usuario/components/usuario-card/usuario-card.module';
import {MatDialogModule} from '@angular/material/dialog';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import { UsuarioSearchComponent } from './components/usuario-search/usuario-search.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {SecondaryToolbarModule} from 'src/@vex/components/secondary-toolbar/secondary-toolbar.module';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';

@NgModule({
  declarations: [UsuarioCreateComponent, UsuarioComponent, UsuarioSearchComponent],
  imports: [
    CommonModule,
    UsuarioRoutingModule,
    IconModule,
    MatTabsModule,
    MatIconModule,
    MatButtonModule,
    FlexLayoutModule,
    MatTooltipModule,
    ContainerModule,
    UsuarioCardModule,
    MatDialogModule,
    ReactiveFormsModule,
    FormsModule,
    MatDialogModule,
    MatInputModule,
    MatSelectModule,
    LoadingModule,
    MatBottomSheetModule,
    NgMultiSelectDropDownModule,
    MatDatepickerModule,
    SecondaryToolbarModule,
    BreadcrumbsModule,
  ]
})
export class UsuarioModule { }
