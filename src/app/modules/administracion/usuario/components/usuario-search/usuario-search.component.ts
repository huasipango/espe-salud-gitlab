import { Component, OnInit } from '@angular/core';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {FormControl} from '@angular/forms';
import {LoadingService} from 'src/app/core/services/loading.service';
import icSearch from '@iconify/icons-ic/twotone-search';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import icClose from '@iconify/icons-ic/twotone-close';
import {UserService} from 'src/app/core/services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UsuarioSharedService} from 'src/app/modules/administracion/usuario/services/usuario-shared.service';

export interface UsuarioBannerForm {
  saludUser?: SaludUser;
}
@Component({
  selector: 'vex-usuario-search',
  templateUrl: './usuario-search.component.html',
  styleUrls: ['./usuario-search.component.scss'],
  animations: [
  stagger40ms,
  fadeInUp400ms
]
})
export class UsuarioSearchComponent implements OnInit {
  icSearch = icSearch;
  icClose = icClose;
  busquedaCtrl = new FormControl('');
  data: SaludUser;
  constructor(private usuarioBannerService: UserService,
              private loadingService: LoadingService,
              private matSnackBar: MatSnackBar,
              private router: Router,
              private route: ActivatedRoute,
              private usuarioSharedService: UsuarioSharedService) { }

  ngOnInit(): void {
  }

  buscarUsuario(): void {
    if (this.busquedaCtrl.value === ''){ return; }
    this.loadingService.showLoaderUntilCompleted(
      this.usuarioBannerService.getUsuarioBannerInfo(this.busquedaCtrl.value)
    ).subscribe((data) => {
      if (data.length > 0) {
        this.data = data[0];
      } else {
        this.matSnackBar.open('No se encontraron resultados', 'CERRAR');
        this.data = null;
      }
    });
  }

  abrirFormUsuario(): void {
    const codigo = this.data.pidm;
    this.data.username = this.busquedaCtrl.value;
    this.usuarioSharedService.setUserInfo(this.data);
    this.router.navigate(['../administracion/usuarios/modal/code', codigo], {relativeTo: this.route});
  }
}
