import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icMail from '@iconify/icons-ic/twotone-mail';
import icChat from '@iconify/icons-ic/twotone-chat';
import icStar from '@iconify/icons-ic/twotone-star';
import icActive from '@iconify/icons-ic/outline-check-circle-outline';
import icIdCard from '@iconify/icons-fa-solid/id-card';
import icInactive from '@iconify/icons-ic/remove-circle-outline';

import {ImagenUsuarioService} from 'src/app/core/services/imagen-usuario.service';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import {SaludUserService} from 'src/app/core/services/salud-user.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
@Component({
  selector: 'vex-usuario-card',
  templateUrl: './usuario-card.component.html',
  styleUrls: ['./usuario-card.component.scss']
})
export class UsuarioCardComponent implements OnInit {

  @Input() usuario: SaludUser;
  @Output() openUsuario = new EventEmitter<SaludUser>();

  icActive = icActive;
  icPhone = icPhone;
  icMail = icMail;
  icChat = icChat;
  icStar = icStar;
  icIdCard = icIdCard;
  icInactive = icInactive;

  mailText = '';
  subject = 'CONTACTO';

  constructor(
    private imagenUsuarioService: ImagenUsuarioService,
    private usuarioSaludService: SaludUserService,
    private matSnackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
  }

  sendMail(): void {
    this.mailText = `mailto:${this.usuario.correoInstitucional}?subject=${this.subject}&body=Hola`;
    window.location.href = this.mailText;
  }

  getUserImage(idBanner: string): string {
    return this.imagenUsuarioService.getUserImage(idBanner);
  }

  disableUser() {
    this.usuarioSaludService.disableUser(this.usuario.pidm)
      .subscribe((saludUser) => {
        if (saludUser) {
          this.usuario = saludUser;
          this.showNotification('El paciente ha sido desabilitado con éxito');
        }
      }, (error) => {
        this.showNotification('El paciente no pudo ser actualizado');
      });
  }

  enableUser() {
    this.usuarioSaludService.enableUser(this.usuario.pidm)
      .subscribe((saludUser) => {
        if (saludUser) {
          this.usuario = saludUser;
          this.showNotification('El paciente ha sido habilitado con éxito');
        }
      }, (error) => {
        this.showNotification('El paciente no pudo ser actualizado');
      });
  }

  showNotification(message: string) {
    this.matSnackBar.open(message, 'CERRAR', {
      duration: 5000
    });
  }
}
