import {Input, NgModule, Output, EventEmitter} from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuarioCardComponent } from './usuario-card.component';
import {MatRippleModule} from "@angular/material/core";
import {IconModule} from "@visurel/iconify-angular";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {MatTooltipModule} from '@angular/material/tooltip';

@NgModule({
  declarations: [UsuarioCardComponent],
  imports: [
    CommonModule,
    MatRippleModule,
    IconModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule
  ],
  exports: [
    UsuarioCardComponent
  ]
})
export class UsuarioCardModule {

}
