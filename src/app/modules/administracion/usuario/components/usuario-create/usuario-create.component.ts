import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import icStar from '@iconify/icons-ic/twotone-star';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icClose from '@iconify/icons-ic/twotone-close';
import icPrint from '@iconify/icons-ic/twotone-print';
import icDownload from '@iconify/icons-ic/twotone-cloud-download';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icEmail from '@iconify/icons-ic/twotone-mail';
import icPerson from '@iconify/icons-ic/twotone-person';
import icStarBorder from '@iconify/icons-ic/twotone-star-border';
import icHeart from '@iconify/icons-fa-solid/heart';
import icGender from '@iconify/icons-fa-solid/transgender';
import icSave from '@iconify/icons-fa-solid/save';
import icIdCard from '@iconify/icons-fa-solid/id-card';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import {Observable} from 'rxjs';
import {AreaSalud} from 'src/app/core/models/catalogo/area-salud.model';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from 'src/app/core/auth/auth.service';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {LoadingService} from 'src/app/core/services/loading.service';
import {Dispensario} from 'src/app/core/models/catalogo/dispensario.model';
import {SaludUserService} from 'src/app/core/services/salud-user.service';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {RolService} from 'src/app/modules/administracion/usuario/services/rol.service';
import {Rol} from 'src/app/modules/administracion/usuario/models/rol.model';
import {UniqueUsernameValidator} from 'src/app/shared/directives/unique-username/unique-username.directive';
import {ImagenUsuarioService} from 'src/app/core/services/imagen-usuario.service';
import {UserService} from 'src/app/core/services/user.service';
import {IDropdownSettings} from 'ng-multiselect-dropdown';
import {switchMap} from 'rxjs/operators';
import {UserInfo} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/user-info.interface';
import { EMPTY } from 'rxjs';
import {TipoIdentificacion} from 'src/app/core/models/catalogo/tipo-identificacion.model';
import {UniqueNumeroArchivoValidator} from 'src/app/shared/directives/unique-cedula/unique-cedula.directive';
import {Genero} from 'src/app/core/models/catalogo-banner/genero.model';
import {CatalogoBannerService} from 'src/app/core/services/catalogo-banner.service';
import {Etnia} from 'src/app/core/models/catalogo-banner/etnia.model';
import {FormacionProfesional} from 'src/app/core/models/catalogo/formacion-profesional.model';
import {EspecialidadProfesional} from 'src/app/core/models/catalogo/especialidad-profesional.model';
import {stagger80ms} from 'src/@vex/animations/stagger.animation';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';

@Component({
  selector: 'vex-usuario-create',
  templateUrl: './usuario-create.component.html',
  styleUrls: ['./usuario-create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    stagger80ms,
    fadeInUp400ms,
    scaleIn400ms,
    fadeInRight400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class UsuarioCreateComponent implements OnInit {

  form: FormGroup;
  mode: 'create' | 'update' = 'create';

  icSave = icSave;
  icStar = icStar;
  icStarBorder = icStarBorder;
  icMoreVert = icMoreVert;
  icClose = icClose;
  icPrint = icPrint;
  icDownload = icDownload;
  icDelete = icDelete;
  usuario: SaludUser;
  icPerson = icPerson;
  icEmail = icEmail;
  icPhone = icPhone;
  icHeart = icHeart;
  icGender = icGender;
  icIdCard = icIdCard;
  today = new Date();
  disabled = false;
  dropdownSettings: IDropdownSettings = {};
  errorMessages = FORM_ERROR_MESSAGES;
  messages = USER_MESSAGES;
  busquedaCtrl = new FormControl('');
  identidadGeneroList: Observable<Genero[]>;
  tiposIdentifiacion: TipoIdentificacion[];
  areasSalud: Observable<AreaSalud[]>;
  dispensarios: Observable<Dispensario[]>;
  roles: Observable<Rol[]>;
  etnias: Observable<Etnia[]>;
  formacionesProfesionales: Observable<FormacionProfesional[]>;
  especialidadesProfesionales: Observable<EspecialidadProfesional[]>;
  loading = false;
  showButton: boolean;
  dropdownOpen = false;
  numeroIdentifiacionIsRequired = false;


  constructor(
              private fb: FormBuilder,
              private snackbar: MatSnackBar,
              private cd: ChangeDetectorRef,
              private route: ActivatedRoute,
              private router: Router,
              private authService: AuthService,
              private _bottomSheet: MatBottomSheet,
              private numeroArchivoValidator: UniqueNumeroArchivoValidator,
              private loadingService: LoadingService,
              private usuarioSaludService: SaludUserService,
              private imagenUsuarioService: ImagenUsuarioService,
              private usernameValidator: UniqueUsernameValidator,
              private catalogoService: CatalogoService,
              private catalogoBannerService: CatalogoBannerService,
              private userService: UserService,
              private matSnackBar: MatSnackBar,
              private rolService: RolService) { }

  ngOnInit(): void {
    this.areasSalud = this.usuarioSaludService.getAreasSalud();
    this.dispensarios = this.catalogoService.getDispensarios();
    this.identidadGeneroList = this.catalogoBannerService.getGeneros();
    this.etnias = this.catalogoService.getEtnias();
    this.formacionesProfesionales = this.catalogoService.getFormacionesProfesionales();
    this.roles = this.rolService.getRoles();
    this.catalogoService.getTiposIdentificacion()
      .subscribe((tipos) => {
        this.tiposIdentifiacion = tipos;
        if (this.mode === 'create') {
          const type = tipos.find(t => t.porDefecto === true);
          this.form.patchValue({idTipoIdentificacion: type ? type.id : null});
        }
      });
    this.multiselectSetting();
    this.route.paramMap.pipe(
      switchMap(params => {
        const codigo = params.get('codigo');
        return this.usuarioSaludService.getUser(Number(codigo));
      })
    ).subscribe((usuario) => {
      if (usuario) {
        this.usuario = usuario;
      }
    });
    if (this.usuario.roles) {
      this.mode = 'update';
      this.dropdownOpen = true;
      this.createFormUsuario();
      this.removeUniqueUsernameValidator();
    } else {
      this.usuario = {} as SaludUser;
      this.createFormUsuario();
    }
  }

  createFormUsuario(){
    this.form = this.fb.group({
      username: [this.usuario.username || null,
        Validators.required,
        [this.usernameValidator.validate.bind(this.usernameValidator)]
      ],
      idTipoIdentificacion: [this.usuario.idTipoIdentificacion || null, Validators.required],
      numeroIdentificacion: [
        {value: this.usuario.cedula, disabled: true},
      ],
      id: [this.usuario.id || null],
      pidm: [this.usuario.pidm || null, Validators.required],
      idBanner: this.usuario.idBanner || null,
      nombres: [this.usuario.nombres || '', Validators.required],
      apellidos: [this.usuario.apellidos || '', Validators.required],
      cedula: [this.usuario.cedula || '', Validators.required],
      idIdentidadGenero: [this.usuario.idIdentidadGenero || null, Validators.required],
      idFormacionProfesional: [this.usuario.idFormacionProfesional || null, Validators.required],
      codigoNacionalidad: [this.usuario.codigoNacionalidad || null, Validators.required],
      idEspecialidadProfesional: [this.usuario.idEspecialidadProfesional || null, Validators.required],
      idEtnia: [this.usuario.idEtnia || null, Validators.required],
      codigoMsp: [this.usuario.codigoMsp || null, Validators.required],
      correoInstitucional: [this.usuario.correoInstitucional || '', Validators.required],
      fechaNacimiento: [this.usuario.fechaNacimiento || null, Validators.required],
      idAreaSalud: [this.usuario.idAreaSalud ||  null, Validators.required],
      idDispensario: [this.usuario.idDispensario || null, Validators.required],
      roles: [this.usuario.roles || [], Validators.required]
    });
  }

  removeUniqueUsernameValidator() {
    this.form.get('username').clearAsyncValidators();
    this.form.get('username').updateValueAndValidity();
  }

  updateFormUsuario(data: UserInfo){
    this.form.patchValue({
      pidm: data.pidm,
      username: this.busquedaCtrl.value,
      idBanner: data.id,
      apellidos: data.nombres.split(',', 1),
      nombres: data.nombres.split(' ').slice(-2).join(' '),
      cedula: data.cedula,
      correoInstitucional: data.correoInstitucional,
    });
  }

  multiselectSetting() {
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'nombre',
      selectAllText: 'Seleccionar todos',
      unSelectAllText: 'Desmarcar todos',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
  }

  save() {
    const usuario: SaludUser = this.form.value;
    if (this.mode === 'create') {
      this.createSaludUser(usuario);
      usuario.username = this.busquedaCtrl.value;
    } else {
      this.updateSaludUser();
    }
  }
  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  createSaludUser(usuario: SaludUser) {
    this.loadingService.showLoaderUntilCompleted(
      this.usuarioSaludService.createUsuario(usuario)
    ).subscribe((response) => {
      if (response) {
        this.showNotification(this.messages.createdSuccessMessage, 'OK');
      }
    }, error => {
      this.showNotification(this.messages.createdFailedMessage, 'OK');
    });
  }

  updateSaludUser() {
    const usuario = this.form.getRawValue();
    this.loadingService.showLoaderUntilCompleted(
      this.usuarioSaludService.updateUsuario(usuario.pidm, usuario)
    ).subscribe((response) => {
      if (response) {
        this.showNotification(this.messages.updatedSuccessMessage, 'OK');
      }
    }, error => {
      this.showNotification(this.messages.updatedFailedMessage, 'OK');
      this.loading = false;
    });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  showMessage(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000,
      horizontalPosition: 'center'
    });
  }

  toggleDropdown() {
    if (this.busquedaCtrl.value === ''){ return; }
    this.loadingService.showLoaderUntilCompleted(
      this.usuarioSaludService.checkNumeroUsernameNotExists(this.busquedaCtrl.value).pipe(
        switchMap(exist => {
          if (exist) {
            this.showMessage('El usuario ya esta registrado', 'CERRAR');
            return EMPTY;
          }else {
            return this.userService.getUserByUsername(this.busquedaCtrl.value);
          }
        })
      )
    ).subscribe((data) => {
      if (data.length > 0) {
        if (!this.form.get('idBanner').value) {
          this.dropdownOpen = !this.dropdownOpen;
        }
        this.updateFormUsuario(data[0]);
      } else {
        this.matSnackBar.open('No se encontraron resultados', 'CERRAR');
      }
    }, (error) => {
      console.log(error.status);
    });
    this.cd.markForCheck();
  }

  getUserImage(idBanner: string) {
    return this.imagenUsuarioService.getUserImage(idBanner);
  }

  setRequiredParamToNumeroIdentificacion(idTipoIdentificacion: number) {
    const tipoIdentificacion = this.tiposIdentifiacion.find(x => x.id === idTipoIdentificacion);
    this.numeroIdentifiacionIsRequired = tipoIdentificacion ? tipoIdentificacion.esRequerido : false;
    if (tipoIdentificacion && tipoIdentificacion.esRequerido) {
      this.form.get('cedula').setValidators(
        [Validators.required]
      );
      this.form.get('cedula').setAsyncValidators(
        [this.numeroArchivoValidator.validate.bind(this.numeroArchivoValidator)]
      );
      this.form.get('cedula').updateValueAndValidity();
    } else {
      this.form.get('cedula').clearValidators();
      this.form.get('cedula').clearAsyncValidators();
      this.form.get('cedula').updateValueAndValidity();
    }
  }

  cargarEspecialidades() {
    const idFormacion = this.form.get('idFormacionProfesional').value;
    this.especialidadesProfesionales = this.catalogoService.getEspecialidadesProfesionalesByFormacion(idFormacion);
  }

  navigateBack() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }
}
