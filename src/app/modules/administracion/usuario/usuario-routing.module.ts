import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from 'src/@vex/interfaces/vex-route.interface';
import { RoleGuard } from 'src/app/core/auth/role.guard';
import { RolEnum } from 'src/app/core/enums/rol-enum';
import {UsuarioComponent} from 'src/app/modules/administracion/usuario/usuario.component';


const routes: VexRoutes = [
  {
    path: '',
    redirectTo: 'all'
  },
  {
    path: ':activeCategory',
    component: UsuarioComponent,
    canActivate: [RoleGuard],
    data: {
      scrollDisabled: true,
      toolbarShadowEnabled: false,
      roles: [RolEnum.ROLE_ADMIN]
    }
  },
  {
    path: 'modal/code/:codigo',
    loadChildren: () => import('./pages/create-update-usuario/create-update-usuario.module')
      .then(m => m.CreateUpdateUsuarioModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuarioRoutingModule { }
