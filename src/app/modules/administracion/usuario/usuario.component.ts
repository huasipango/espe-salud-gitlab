import { Component, OnInit } from '@angular/core';
import {Link} from 'src/@vex/interfaces/link.interface';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {Observable} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import icContacts from '@iconify/icons-ic/twotone-contacts';
import icCloudDownload from '@iconify/icons-ic/twotone-cloud-download';
import icSearch from '@iconify/icons-ic/twotone-search';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import {trackById} from 'src/@vex/utils/track-by';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import {MatSnackBar} from '@angular/material/snack-bar';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {filter} from 'rxjs/operators';
import icAdd from '@iconify/icons-fa-solid/plus';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {Dispensario} from 'src/app/core/models/catalogo/dispensario.model';
import {SaludUserService} from 'src/app/core/services/salud-user.service';
import {UsuarioSearchComponent} from 'src/app/modules/administracion/usuario/components/usuario-search/usuario-search.component';

@Component({
  selector: 'vex-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.scss'],
  animations: [
    scaleIn400ms,
    fadeInRight400ms,
    stagger40ms,
    fadeInUp400ms,
    scaleFadeIn400ms
  ]
})
export class UsuarioComponent implements OnInit {

  links: Link[] = [
    {
      label: 'TODOS',
      route: '../all'
    }
  ];
  nameLinks: string;
  usuarios: SaludUser[] = [];
  messages = USER_MESSAGES;
  pacienteActual: Paciente;
  icSearch = icSearch;
  icCloudDownload = icCloudDownload;
  icFilterList = icFilterList;
  icContacts = icContacts;
  icAdd = icAdd;
  dispensarios: Dispensario[] = [];

  users$: Observable<SaludUser[]>;
  trackById = trackById;
  constructor(
    private dialog: MatDialog,
    private usuarioService: SaludUserService,
    private router: Router,
    private route: ActivatedRoute,
    private snackbar: MatSnackBar,
    private catalogoService: CatalogoService
  ) { }

  ngOnInit(): void {
    this.setDispensarios();
    this.getUsuarios();
  }

  setDispensarios(): void {
    this.catalogoService.getDispensarios()
      .subscribe((dispensarios) => {
        this.dispensarios = dispensarios;
        this.dispensarios.forEach(d => {
          this.links.push({
            label: d.unidadOperativa,
            route: `../${d.id.toString()}`
          });
        });
      });
  }

  getUsuarios(){
    this.users$ = null;
    this.route.paramMap.subscribe((params: ParamMap) => {
      const activeCategory = params.get('activeCategory');
      if (activeCategory === 'all') {
        this.users$ = this.usuarioService.getAllUsers();
      } else {
        this.users$ = this.usuarioService.getUsuariosByIdDispensario(Number(activeCategory));
      }
    });
  }

  openUsuario(usuario?: SaludUser): void {
    const codigo = usuario.pidm;
    this.router.navigate(['../modal/code', codigo], {relativeTo: this.route});
  }

  createUsuario(usuario?: SaludUser): void {
    this.dialog.open(UsuarioSearchComponent, {
      data: usuario || null,
      width: '700px',
      maxWidth: '100%',
      disableClose: true
    }).beforeClosed().pipe(
      filter<SaludUser>(Boolean)
    ).subscribe();
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

}
