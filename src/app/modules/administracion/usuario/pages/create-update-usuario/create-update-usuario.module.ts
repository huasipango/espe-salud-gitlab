import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateUpdateUsuarioComponent } from './create-update-usuario.component';
import {CreateUpdateUsuarioRoutingModule} from 'src/app/modules/administracion/usuario/pages/create-update-usuario/create-update-usuario-routing.module';
import {IconModule} from '@visurel/iconify-angular';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {MatMomentDateModule} from '@angular/material-moment-adapter';



@NgModule({
  declarations: [CreateUpdateUsuarioComponent],
  imports: [
    CommonModule,
    CreateUpdateUsuarioRoutingModule,
    IconModule,
    LoadingModule,
    NgMultiSelectDropDownModule,
    MatFormFieldModule,
    MatIconModule,
    MatSelectModule,
    MatDatepickerModule,
    MatMomentDateModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatInputModule,
    MatButtonModule,
    ContainerModule
  ]
})
export class CreateUpdateUsuarioModule { }
