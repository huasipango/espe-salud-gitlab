import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {CreateUpdateUsuarioComponent} from 'src/app/modules/administracion/usuario/pages/create-update-usuario/create-update-usuario.component';
import {RoleGuard} from 'src/app/core/auth/role.guard';
import {RolEnum} from 'src/app/core/enums/rol.enum';

const routes: Routes = [
  {
    path: '',
    component: CreateUpdateUsuarioComponent,
    canActivate: [RoleGuard],
    data: {
      toolbarShadowEnabled: false,
      containerEnabled: true,
      roles: [RolEnum.ROLE_ADMIN]
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateUpdateUsuarioRoutingModule {
}
