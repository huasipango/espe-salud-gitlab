import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {stagger80ms} from 'src/@vex/animations/stagger.animation';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {IDropdownSettings} from 'ng-multiselect-dropdown';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {Observable} from 'rxjs';
import {Genero} from 'src/app/core/models/catalogo-banner/genero.model';
import {TipoIdentificacion} from 'src/app/core/models/catalogo/tipo-identificacion.model';
import {AreaSalud} from 'src/app/core/models/catalogo/area-salud.model';
import {Dispensario} from 'src/app/core/models/catalogo/dispensario.model';
import {Rol} from 'src/app/modules/administracion/usuario/models/rol.model';
import {Etnia} from 'src/app/core/models/catalogo-banner/etnia.model';
import {FormacionProfesional} from 'src/app/core/models/catalogo/formacion-profesional.model';
import {EspecialidadProfesional} from 'src/app/core/models/catalogo/especialidad-profesional.model';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {UniqueNumeroArchivoValidator} from 'src/app/shared/directives/unique-cedula/unique-cedula.directive';
import {LoadingService} from 'src/app/core/services/loading.service';
import {SaludUserService} from 'src/app/core/services/salud-user.service';
import {ImagenUsuarioService} from 'src/app/core/services/imagen-usuario.service';
import {UniqueUsernameValidator} from 'src/app/shared/directives/unique-username/unique-username.directive';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {CatalogoBannerService} from 'src/app/core/services/catalogo-banner.service';
import {UserService} from 'src/app/core/services/user.service';
import {RolService} from 'src/app/modules/administracion/usuario/services/rol.service';
import {switchMap} from 'rxjs/operators';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import {UserInfo} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/user-info.interface';
import icBack from '@iconify/icons-ic/arrow-back';

import {enumSelector} from 'src/app/core/utils/enum-to-string-util';
import {SexoEnum} from 'src/app/core/enums/sexo.enum';
import {Nacionalidad} from 'src/app/core/models/catalogo-banner/nacionalidad.model';
import {UsuarioSharedService} from 'src/app/modules/administracion/usuario/services/usuario-shared.service';

@Component({
  selector: 'vex-create-update-usuario',
  templateUrl: './create-update-usuario.component.html',
  styleUrls: ['./create-update-usuario.component.scss'],
  animations: [
    stagger80ms,
    fadeInUp400ms,
    scaleIn400ms,
    fadeInRight400ms
  ],
})
export class CreateUpdateUsuarioComponent implements OnInit {
  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  icBack = icBack;

  usuario: SaludUser;
  today = new Date();
  disabled = false;
  dropdownSettings: IDropdownSettings = {};
  errorMessages = FORM_ERROR_MESSAGES;
  messages = USER_MESSAGES;
  identidadGeneroList: Observable<Genero[]>;
  tiposIdentifiacion: TipoIdentificacion[];
  areasSalud: Observable<AreaSalud[]>;
  dispensarios: Observable<Dispensario[]>;
  roles: Observable<Rol[]>;
  etnias: Observable<Etnia[]>;
  formacionesProfesionales: Observable<FormacionProfesional[]>;
  especialidadesProfesionales: Observable<EspecialidadProfesional[]>;
  loading = false;
  nacionalidades: Observable<Nacionalidad[]>;
  numeroIdentifiacionIsRequired = false;
  sexoList = enumSelector(SexoEnum);
  codigo: number;
  userInfo: Observable<UserInfo>;

  constructor(
    private fb: FormBuilder,
    private snackbar: MatSnackBar,
    private cd: ChangeDetectorRef,
    private route: ActivatedRoute,
    private router: Router,
    private numeroArchivoValidator: UniqueNumeroArchivoValidator,
    private loadingService: LoadingService,
    private usuarioSaludService: SaludUserService,
    private imagenUsuarioService: ImagenUsuarioService,
    private usernameValidator: UniqueUsernameValidator,
    private catalogoService: CatalogoService,
    private catalogoBannerService: CatalogoBannerService,
    private userService: UserService,
    private rolService: RolService,
    private usuarioBannerService: UserService,
    private usuarioSharedService: UsuarioSharedService
  ) {
  }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap(params => {
        this.codigo = Number(params.get('codigo'));
        return this.usuarioSaludService.getUser(Number(this.codigo));
      })
    ).subscribe((usuario) => {
      if (usuario) {
        this.usuario = usuario;
        this.mode = 'update';
        this.createFormUsuario();
        this.removeUniqueUsernameValidator();
      } else {
        this.createFormUsuario();
      }
    }, error => {
      if (error.status === 404){
        this.usuario = {} as SaludUser;
        this.usuarioSharedService.sharingUserInfoObservable
          .subscribe((data: SaludUser) => {
            if (data) {
              this.usuario = data;
              this.createFormUsuario();
              this.updateFormUsuario(this.usuario);
            } else {
              this.snackbar.open('No se encontraron resultados', 'CERRAR');
              this.router.navigate(['../../../'], {relativeTo: this.route});
            }
          });
      }
    });
    this.areasSalud = this.usuarioSaludService.getAreasSalud();
    this.dispensarios = this.catalogoService.getDispensarios();
    this.nacionalidades = this.catalogoBannerService.getNacionalidades();
    this.identidadGeneroList = this.catalogoBannerService.getGeneros();
    this.etnias = this.catalogoService.getEtnias();
    this.formacionesProfesionales = this.catalogoService.getFormacionesProfesionales();
    this.especialidadesProfesionales = this.catalogoService.getEspecialidadesProfesionales();
    this.roles = this.rolService.getRoles();
    this.multiselectSetting();

  }

  createFormUsuario() {
    this.form = this.fb.group({
      username: [this.usuario.username || null
        , Validators.required, [this.usernameValidator.validate.bind(this.usernameValidator)]
      ],

      idTipoIdentificacion: [this.usuario.idTipoIdentificacion || null, Validators.required],
      cedula: [
        {value: this.usuario.cedula || null, disabled: false}
      ],
      id: [this.usuario.id || null],
      pidm: [this.usuario.pidm || null, Validators.required],
      sexo: [this.usuario.sexo || '', Validators.required],
      idFormacionProfesional: [this.usuario.idFormacionProfesional || null, Validators.required],
      idEspecialidadProfesional: [this.usuario.idEspecialidadProfesional || null],
      idEtnia: [this.usuario.idEtnia || null, Validators.required],
      nacionalidad: [this.usuario.nacionalidad || null, Validators.required],
      idBanner: [this.usuario.idBanner || null, Validators.required],
      nombres: [this.usuario.nombres || null, Validators.required],
      codigoMsp: [this.usuario.codigoMsp || null, Validators.required],
      codigoNacionalidad: [this.usuario.codigoNacionalidad || null],
      apellidos: [this.usuario.apellidos || null, Validators.required],
      activo: [this.usuario.activo || null],
      correoInstitucional: [this.usuario.correoInstitucional || null, Validators.required],
      fechaNacimiento: [this.usuario.fechaNacimiento || null, Validators.required],
      idAreaSalud: [this.usuario.idAreaSalud || null, Validators.required],
      idDispensario: [this.usuario.idDispensario || null, Validators.required],
      roles: [this.usuario.roles || [], Validators.required]
    });

    this.catalogoService.getTiposIdentificacion()
      .subscribe((tipos) => {
        this.tiposIdentifiacion = tipos;
        if (this.mode === 'create') {
          const type = tipos.find(t => t.porDefecto === true);
          this.form.patchValue({idTipoIdentificacion: type ? type.id : null});
        }
      });
  }

  removeUniqueUsernameValidator() {
    this.form.get('username').clearAsyncValidators();
    this.form.get('username').updateValueAndValidity();
  }

  updateFormUsuario(data: UserInfo){
    this.form.patchValue({
      id: data.id,
      pidm: data.pidm,
      username: data.username,
      cedula: data.cedula,
      idBanner: data.id,
      apellidos: String(data.nombres.split(',', 1)),
      nombres: data.nombres.split(' ').slice(-2).join(' '),
      correoInstitucional: data.correoInstitucional,
    });
  }

  multiselectSetting() {
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'nombre',
      selectAllText: 'Seleccionar todos',
      unSelectAllText: 'Desmarcar todos',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
  }

  save() {
    const codigoNac = this.form.get('nacionalidad').value.substr(0, 2);
    this.form.get('codigoNacionalidad').setValue(codigoNac);
    const usuario: SaludUser = this.form.value;
    if (this.mode === 'create') {
      this.createSaludUser(usuario);
    } else {
      this.updateSaludUser();
    }
  }
  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  createSaludUser(usuario: SaludUser) {
    this.form.get('activo').setValue(true);
    this.loadingService.showLoaderUntilCompleted(
      this.usuarioSaludService.createUsuario(usuario)
    ).subscribe((response) => {
      if (response) {
        this.showNotification(this.messages.createdSuccessMessage, 'OK');
        this.router.navigate(['../../../'], {relativeTo: this.route});
      }
    }, error => {
      this.showNotification(this.messages.createdFailedMessage, 'OK');
    });
  }

  updateSaludUser() {
    const usuario = this.form.getRawValue();
    this.loadingService.showLoaderUntilCompleted(
      this.usuarioSaludService.updateUsuario(usuario.pidm, usuario)
    ).subscribe((response) => {
      if (response) {
        this.showNotification(this.messages.updatedSuccessMessage, 'OK');
        this.router.navigate(['../../../'], {relativeTo: this.route});
      }
    }, error => {
      this.showNotification(this.messages.updatedFailedMessage, 'OK');
      this.loading = false;
    });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  showMessage(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000,
      horizontalPosition: 'center'
    });
  }

  getUserImage(idBanner: string) {
    return this.imagenUsuarioService.getUserImage(idBanner);
  }

  cargarEspecialidades() {
    const idFormacion = this.form.get('idFormacionProfesional').value;
    this.especialidadesProfesionales = this.catalogoService.getEspecialidadesProfesionalesByFormacion(idFormacion);
  }

  navigateBack() {
    this.router.navigate(['../../../'], {relativeTo: this.route});
  }
}
