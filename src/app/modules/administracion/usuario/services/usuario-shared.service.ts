import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';

@Injectable({
  providedIn: 'root'
})
export class UsuarioSharedService {
  constructor() { }
  private subject: BehaviorSubject<SaludUser> = new BehaviorSubject<SaludUser>(null);
  private subjectInforUser: BehaviorSubject<SaludUser> = new BehaviorSubject<SaludUser>(null);
  public setUsuario(saludUser: SaludUser): void{
    this.subject.next(saludUser);
  }
  public setUserInfo(userInfo: SaludUser): void{
    this.subjectInforUser.next(userInfo);
  }
  get sharingObservable(){
    return this.subject.asObservable();
  }
  get sharingUserInfoObservable(){
    return this.subjectInforUser.asObservable();
  }
}
