import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Rol} from 'src/app/modules/administracion/usuario/models/rol.model';

@Injectable({
  providedIn: 'root'
})
export class RolService {

  private baseUrl: string = environment.baseUrl;
  constructor(private http: HttpClient) {
  }

  // GET_ALL
  getRoles(): Observable<Rol[]>{
    return this.http.get<Rol[]>(this.baseUrl + 'roles');
  }
}
