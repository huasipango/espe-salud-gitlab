import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PageLayoutModule} from 'src/@vex/components/page-layout/page-layout.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {HttpClientModule} from '@angular/common/http';
import {IconModule} from '@visurel/iconify-angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDividerModule} from '@angular/material/divider';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MAT_DATE_LOCALE} from '@angular/material/core';
import {HospitalizacionRoutingModule} from './hospitalizacion-routing.module';
import {HospitalizacionCreateComponent} from './components/hospitalizacion-create/hospitalizacion-create.component';
import {HospitalizacionListComponent} from './pages/hospitalizacion-list/hospitalizacion-list.component';
import {EmptyPacienteModule} from '../../../core/components/empty-paciente/empty-paciente.module';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {MatTabsModule} from '@angular/material/tabs';
import {MatStepperModule} from '@angular/material/stepper';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';


@NgModule({
  declarations: [
    HospitalizacionCreateComponent,
    HospitalizacionListComponent,
  ],
  imports: [
    CommonModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    HospitalizacionRoutingModule,
    HttpClientModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatCheckboxModule,
    MatIconModule,
    MatButtonModule,
    MatSnackBarModule,
    MatMenuModule,
    MatDialogModule,
    IconModule,
    FormsModule,
    MatTooltipModule,
    ReactiveFormsModule,
    ContainerModule,
    MatSelectModule,
    MatButtonToggleModule,
    MatAutocompleteModule,
    MatDividerModule,
    MatInputModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatMomentDateModule,
    EmptyPacienteModule,
    MatTabsModule,
    MatStepperModule,
    LoadingModule
  ],
  entryComponents: [HospitalizacionCreateComponent],
  exports: [HospitalizacionCreateComponent],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'es-EC'},
  ],
})
export class HospitalizacionModule {
}
