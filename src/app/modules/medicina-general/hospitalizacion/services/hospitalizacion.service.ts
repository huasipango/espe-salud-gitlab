import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import {Observable} from 'rxjs';
import {Hospitalizacion} from 'src/app/modules/medicina-general/hospitalizacion/models/hospitalizacion.model';

@Injectable({
  providedIn: 'root'
})
export class HospitalizacionService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getHospitalizaciones(id: number): Observable<Hospitalizacion[]>{
    return this.http.get<Hospitalizacion[]>(this.baseUrl + 'hospitalizaciones' + '?idPaciente=' + id);
  }

  createHospitalizacion(data: Hospitalizacion): Observable<Hospitalizacion>{
    return this.http.post<Hospitalizacion>(this.baseUrl + 'hospitalizaciones/', JSON.stringify(data));
  }

  updateHospitalizacion(id: number, data: Hospitalizacion): Observable<Hospitalizacion>{
    return this.http.put<Hospitalizacion>(this.baseUrl + 'hospitalizaciones/' + id, JSON.stringify(data));
  }

  deleteHospitalizacion(id: number): Observable<Hospitalizacion>  {
    return this.http.delete<Hospitalizacion>(this.baseUrl + 'hospitalizaciones/' + id);
  }
}
