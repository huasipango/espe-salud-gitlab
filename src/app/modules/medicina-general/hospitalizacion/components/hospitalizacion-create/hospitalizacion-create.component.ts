import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icClose from '@iconify/icons-ic/twotone-close';
import icPrint from '@iconify/icons-ic/twotone-print';
import icDescription from '@iconify/icons-ic/outline-description';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icPerson from '@iconify/icons-ic/twotone-person';
import icLocationCity from '@iconify/icons-ic/twotone-location-city';
import icHospital from '@iconify/icons-fa-solid/hospital';
import icDoctor from '@iconify/icons-fa-solid/user-md';
import icEspecialidad from '@iconify/icons-fa-solid/stethoscope';
import icTime from '@iconify/icons-ic/access-time';
import icEgreso1 from '@iconify/icons-ic/baseline-how-to-reg';
import icDoneAll from '@iconify/icons-ic/twotone-done-all';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Hospitalizacion} from 'src/app/modules/medicina-general/hospitalizacion/models/hospitalizacion.model';
import {LoadingService} from 'src/app/core/services/loading.service';
import {HospitalizacionService} from 'src/app/modules/medicina-general/hospitalizacion/services/hospitalizacion.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {calculateDaysDiff, calculateMonthsDiff} from 'src/app/core/utils/operations.util';

@Component({
  selector: 'vex-hospitalizacion-create',
  templateUrl: './hospitalizacion-create.component.html',
  styleUrls: ['./hospitalizacion-create.component.scss'],
})
export class HospitalizacionCreateComponent implements OnInit {
  ingresoFormGroup: FormGroup;
  egresoFormGroup: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: any,
    private dialogRef: MatDialogRef<HospitalizacionCreateComponent>,
    private pacienteGlobalService: PacienteGlobalService,
    private fb: FormBuilder,
    private loadingService: LoadingService,
    private hospitalizacionService: HospitalizacionService,
    private snackbar: MatSnackBar
  ) {
  }

  maxDate = new Date();

  mode: 'create' | 'update' = 'create';
  paciente: Paciente;
  icMoreVert = icMoreVert;
  icClose = icClose;

  icPrint = icPrint;
  icDescription = icDescription;
  icDelete = icDelete;
  icHospital = icHospital;
  icDoctor = icDoctor;
  icEspecialidad = icEspecialidad;
  icTime = icTime;
  icEgreso1 = icEgreso1;
  icDoneAll = icDoneAll;

  icPerson = icPerson;
  icLocationCity = icLocationCity;
  icPhone = icPhone;

  messages = USER_MESSAGES;
  errorMessages = FORM_ERROR_MESSAGES;

  ngOnInit(): void {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as Hospitalizacion;
    }
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.paciente = paciente;
          this.initForm();
        }
      });
  }

  initForm(): void {
    this.ingresoFormGroup = this.fb.group({
      id: this.defaults.id || null,
      fechaIngreso: [this.defaults.fechaIngreso || this.maxDate, Validators.required],
      casaSalud: [this.defaults.casaSalud || '', Validators.required],
      diagnosticoIngreso: [this.defaults.diagnosticoIngreso || '', Validators.required],
      evolucionProcedimientoComplicaciones: [this.defaults.evolucionProcedimientoComplicaciones || '', Validators.required],
      medicoTratante: [this.defaults.medicoTratante || '', Validators.required],
      especialidadMedico: [this.defaults.especialidadMedico || '', Validators.required],
      idPaciente: [this.defaults.idPaciente || this.paciente.id, Validators.required],
    });

    this.egresoFormGroup = this.fb.group({
      fechaAlta: [this.defaults.fechaAlta || '', Validators.required],
      tipoEgreso: [this.defaults.tipoEgreso || ''],
      tiempoEstadiaDias: [{value: this.defaults.tiempoEstadiaDias || 0, disabled: true}],
      diasIncapacidad: [this.defaults.diasIncapacidad || '', Validators.required],
      diagnosticoEgreso: [this.defaults.diagnosticoEgreso || ''],
      indicacionesAlta: [this.defaults.indicacionesAlta || '', Validators.required],
    });
  }

  save() {
    const hospitalizacion = this.ingresoFormGroup.value;
    const datosEgreso = this.egresoFormGroup.getRawValue();
    hospitalizacion.fechaAlta = datosEgreso.fechaAlta;
    hospitalizacion.tipoEgreso = datosEgreso.tipoEgreso;
    hospitalizacion.tiempoEstadiaDias = datosEgreso.tiempoEstadiaDias;
    hospitalizacion.diasIncapacidad = datosEgreso.diasIncapacidad;
    hospitalizacion.diagnosticoEgreso = datosEgreso.diagnosticoEgreso;
    hospitalizacion.indicacionesAlta = datosEgreso.indicacionesAlta;
    if (this.mode === 'create') {
      this.createHospitalizacion(hospitalizacion);
    } else if (this.mode === 'update') {
      this.updateHospitalizacion(hospitalizacion);
    }
  }

  createHospitalizacion(hospitalizacion: Hospitalizacion) {
    this.loadingService.showLoaderUntilCompleted(
      this.hospitalizacionService.createHospitalizacion(hospitalizacion)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.createdFailedMessage, 'CERRAR');
    });
  }

  updateHospitalizacion(hospitalizacion: Hospitalizacion) {
    this.loadingService.showLoaderUntilCompleted(
      this.hospitalizacionService.updateHospitalizacion(hospitalizacion.id, hospitalizacion)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.updatedFailedMessage, 'CERRAR');
    });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  calculateTiempoEstadia() {
    const fechaIngreso = this.ingresoFormGroup.get('fechaIngreso').value;
    const fechaEgreso = this.egresoFormGroup.get('fechaAlta').value;
    if (!fechaEgreso || !fechaIngreso) { return; }
    let diff = Math.round(calculateDaysDiff(fechaIngreso, fechaEgreso));
    if (diff < 1) { diff = 0; }
    this.egresoFormGroup.get('tiempoEstadiaDias').setValue(diff);
  }
}

