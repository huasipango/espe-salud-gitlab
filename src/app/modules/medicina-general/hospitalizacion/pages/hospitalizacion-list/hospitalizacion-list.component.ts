import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {FormControl} from '@angular/forms';
import {ReplaySubject} from 'rxjs';
import {Hospitalizacion} from '../../models/hospitalizacion.model';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icHospital from '@iconify/icons-fa-solid/hospital-symbol';
import icDownload from '@iconify/icons-ic/cloud-download';
import icPrint from '@iconify/icons-ic/print';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {DateUtil, getShortDate} from 'src/app/core/utils/date-utils';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {HospitalizacionService} from '../../services/hospitalizacion.service';
import {Router} from '@angular/router';
import {HospitalizacionCreateComponent} from '../../components/hospitalizacion-create/hospitalizacion-create.component';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';

@UntilDestroy()
@Component({
  selector: 'vex-hospitalizacion-list',
  templateUrl: './hospitalizacion-list.component.html',
  styleUrls: ['./hospitalizacion-list.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms,
    scaleFadeIn400ms,
    scaleIn400ms,
    fadeInRight400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})

export class HospitalizacionListComponent implements OnInit, AfterViewInit {

  hospitalizaciones: Hospitalizacion[] = [];

  columns: TableColumn<Hospitalizacion>[] = [
    {
      label: 'Casa de salud tratante',
      property: 'casaSalud',
      type: 'text',
      visible: true
    },
    {
      label: 'Especialidad',
      property: 'especialidadMedico',
      type: 'text',
      visible: true
    },
    {
      label: 'Fecha ingreso',
      property: 'fechaIngreso',
      type: 'date',
      visible: true
    },
    {
      label: 'Diagnostico de ingreso',
      property: 'diagnosticoIngreso',
      type: 'text',
      visible: true,
      cssClasses: ['text-wrap']
    },
    {
      label: 'Evolucion, procedimiento y complicaciones',
      property: 'evolucionProcedimientoComplicaciones',
      type: 'text',
      visible: false,
      cssClasses: ['text-wrap']
    },
    {
      label: 'Diagnostico de egreso',
      property: 'diagnosticoEgreso',
      type: 'text',
      visible: false,
      cssClasses: ['text-wrap']
    },
    {
      label: 'Tiempo de estadia (D)',
      property: 'tiempoEstadiaDias',
      type: 'text',
      visible: true
    },
    {
      label: 'Dias de incapacidad',
      property: 'diasIncapacidad',
      type: 'text',
      visible: true
    },
    {
      label: 'Fecha de alta',
      property: 'fechaAlta',
      type: 'date',
      visible: false
    }, {
      label: 'Indicaciones al alta',
      property: 'indicacionesAlta',
      type: 'text',
      visible: false
    }, {
      label: 'Tipo de egreso',
      property: 'tipoEgreso',
      type: 'text',
      visible: false
    }, {
      label: 'Medico Tratante',
      property: 'medicoTratante',
      type: 'text',
      visible: false
    },
    {label: 'Actions', property: 'actions', type: 'button', visible: true}
  ];

  messages = USER_MESSAGES;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<Hospitalizacion> | null;
  selection = new SelectionModel<Hospitalizacion>(true, []);
  searchCtrl = new FormControl();

  icDownload = icDownload;
  icPrint = icPrint;
  icHospital = icHospital;
  icEdit = icEdit;
  icSearch = icSearch;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icMoreHoriz = icMoreHoriz;

  pacienteActual: Paciente;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private router: Router,
    private hospitalizacionService: HospitalizacionService,
    private pacienteGlobalService: PacienteGlobalService,
  ) {
  }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getHospitalizaciones();
        }
      });
    this.searchCtrl.valueChanges.pipe(
      untilDestroyed(this)
    ).subscribe(value => this.onFilterChange(value));
  }

  getHospitalizaciones() {
    this.hospitalizacionService.getHospitalizaciones(this.pacienteActual.id)
      .subscribe((data) => {
        this.hospitalizaciones = data;
        this.dataSource.data = this.hospitalizaciones;
      });
  }

  createHospitalizacion() {
    this.dialog.open(HospitalizacionCreateComponent, {
      width: '750px',
      maxWidth: '100%'
    }).afterClosed()
      .subscribe((hosp: Hospitalizacion) => {
        if (hosp) {
          this.showNotification(this.messages.createdSuccessMessage, 'OK');
          this.getHospitalizaciones();
        }
      });
  }

  deleteHospitalizacion(hospitalizacionEliminar: Hospitalizacion) {
    this.hospitalizacionService.deleteHospitalizacion(hospitalizacionEliminar.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification(this.messages.deletedSuccessMessage, 'CERRAR');
          this.hospitalizaciones.splice(
            this.hospitalizaciones.findIndex((existing) =>
              existing.id === hospitalizacionEliminar.id), 1
          );
          this.dataSource.connect().next(this.hospitalizaciones);
        } else {
          this.showNotification(this.messages.deleteFailMessage, 'CERRAR');
        }
      });
  }

  updateHospitalizacion(hospitalizacion: Hospitalizacion) {
    this.dialog.open(HospitalizacionCreateComponent, {
      data: hospitalizacion,
      width: '750px',
      maxWidth: '100%'
    }).afterClosed().subscribe(updatedHospitalizacion => {
      if (updatedHospitalizacion) {
        this.showNotification(this.messages.updatedSuccessMessage, 'OK');
        this.getHospitalizaciones();
      }
    });
  }

  getFormattedDate(date: Date) {
    return getShortDate(date);
  }

  openDeleteDialog(hospitalizacion: Hospitalizacion) {
    this.dialog.open(DeleteModalComponent, {
      data: this.messages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') { this.deleteHospitalizacion(hospitalizacion); }
    });
  }

  onFilterChange(value: string) {
    if (!this.dataSource) {
      return;
    }
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  print() {
  }

  openSearchModal() {
  }
}
