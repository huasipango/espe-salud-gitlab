export class Hospitalizacion {
  id: number;
  fechaIngreso: string;
  casaSalud: string;
  diagnosticoIngreso: string;
  evolucionProcedimientoComplicaciones: string;
  diagnosticoEgreso: string;
  tiempoEstadiaDias: number;
  fechaAlta: string;
  diasIncapacidad: number;
  indicacionesAlta: string;
  tipoEgreso: string;
  medicoTratante: string;
  especialidadMedico: string;
  idPaciente: number;
}
