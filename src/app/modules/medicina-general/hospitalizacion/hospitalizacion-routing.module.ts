import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from 'src/@vex/interfaces/vex-route.interface';
import { HospitalizacionCreateComponent } from './components/hospitalizacion-create/hospitalizacion-create.component';
import { HospitalizacionListComponent } from './pages/hospitalizacion-list/hospitalizacion-list.component';


const routes: VexRoutes = [
  {
    path: '',
    component: HospitalizacionListComponent,
    data: {
      toolbarShadowEnabled: true,
      containerEnabled: true
    }
  },
  {
    path: 'form',
    component: HospitalizacionCreateComponent,
    data: {
      toolbarShadowEnabled: true,
      containerEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HospitalizacionRoutingModule { }
