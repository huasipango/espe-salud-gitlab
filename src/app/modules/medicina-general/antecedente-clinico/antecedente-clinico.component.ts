import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Link} from 'src/@vex/interfaces/link.interface';
import {AntecedentePersonal} from './models/antecedente-personal.model';
import {AntecedentePersonalSharedService} from './services/antecedente-personal-shared.service';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {AntecedentePersonalService} from './services/antecedente-personal.service';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {LoadingService} from 'src/app/core/services/loading.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {UntilDestroy} from '@ngneat/until-destroy';
import {Subscription} from 'rxjs';

@UntilDestroy()
@Component({
  selector: 'vex-antecedente-clinico',
  templateUrl: './antecedente-clinico.component.html',
  styleUrls: ['./antecedente-clinico.component.scss']
})
export class AntecedenteClinicoComponent implements OnInit, OnDestroy{
  layoutCtrl = new FormControl('full');
  links: Link[] = [
    {
      label: 'GENERAL',
      route: './',
      routerLinkActiveOptions: {exact: true}
    },
    {
      label: 'ESTILO DE VIDA',
      route: './estilo-vida',
    },
    {
      label: 'ANTECEDENTES PERSONALES',
      route: './antecedentes'
    },
    {
      label: 'SEXUALIDAD',
      route: './sexualidad',
    }
  ];

  pacienteActual: Paciente;
  antecedentePersonal: AntecedentePersonal;
  pacienteSubscription: Subscription;

  constructor(
    private pacienteGlobalService: PacienteGlobalService,
    private antecedenteService: AntecedentePersonalService,
    private antecedenteSharedService: AntecedentePersonalSharedService,
    public loadingService: LoadingService,
    private snackBar: MatSnackBar
  ) {
  }

  ngOnInit(): void {
    this.pacienteSubscription = this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getAntecedentePersonal();
        }
      });

  }

  getAntecedentePersonal(): void {
    this.antecedenteSharedService.antecedentePersonal$
      .subscribe((antecedente: AntecedentePersonal) => {
        this.antecedentePersonal = antecedente;
      });
    this.loadingService.loadingOn();
    this.antecedenteService.getAntecedentePersonal(this.pacienteActual.id)
      .subscribe((antecedente) => {
        if (antecedente) {
          this.loadingService.loadingOff();
          this.antecedenteSharedService.setAntecedentePersonal(antecedente);
        }
      }, error => {
        this.loadingService.loadingOff();
        this.snackBar.open('El paciente aún no ha registrado sus antecedentes', 'CERRAR', {
          duration: 5000
        });
        this.antecedenteSharedService.setAntecedentePersonal(null);
      });
  }

  ngOnDestroy(): void {
    this.pacienteSubscription.unsubscribe();
  }
}
