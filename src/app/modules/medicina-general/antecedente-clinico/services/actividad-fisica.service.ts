import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import { ActividadFisica } from '../models/actividad-fisica.model';
import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ActividadFisicaService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getActividadesFisicas(idPaciente: number): Observable<ActividadFisica[]>{
    return this.http.get<ActividadFisica[]>(this.baseUrl + 'actividades-fisicas?idPaciente=' + idPaciente);
  }

  createActividadFisica(data: ActividadFisica): Observable<ActividadFisica> {
    return this.http.post<ActividadFisica>(this.baseUrl + 'actividades-fisicas/', JSON.stringify(data));
  }

  updateActividadFisica(id: number, data: ActividadFisica): Observable<ActividadFisica> {
    return this.http.put<ActividadFisica>(this.baseUrl + 'actividades-fisicas/' + id, JSON.stringify(data));
  }

  deleteActividadFisica(id: number): Observable<boolean> {
    return this.http.delete<boolean>(this.baseUrl + 'actividades-fisicas/' + id);
  }
}
