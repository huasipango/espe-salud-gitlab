import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AntecedentePatologicoPersonal} from '../models/antecedente-patologico-personal.model';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PatologicoPersonalService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getAntecedentesPatologicoPersonales(idPaciente: number): Observable<AntecedentePatologicoPersonal[]>{
    return this.http.get<AntecedentePatologicoPersonal[]>(
      this.baseUrl + 'antecedentes-patologicos-personales?idPaciente=' + idPaciente);
  }

  createAntecedentePatologicoPersonal(data: AntecedentePatologicoPersonal): Observable<AntecedentePatologicoPersonal> {
    return this.http.post<AntecedentePatologicoPersonal>(this.baseUrl + 'antecedentes-patologicos-personales/', JSON.stringify(data));
  }

  updateAntecedentePatologicoPersonal(id: number, data: AntecedentePatologicoPersonal): Observable<AntecedentePatologicoPersonal> {
    return this.http.put<AntecedentePatologicoPersonal>(this.baseUrl + 'antecedentes-patologicos-personales/' + id, JSON.stringify(data));
  }

  deleteAntecedentePatologicoPersonal(id: number): Observable<AntecedentePatologicoPersonal> {
    return this.http.delete<AntecedentePatologicoPersonal>(this.baseUrl + 'antecedentes-patologicos-personales/' + id);
  }
}
