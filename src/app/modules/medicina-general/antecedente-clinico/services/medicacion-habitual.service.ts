import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {MedicacionHabitual} from '../models/medicacion-habitual.model';

@Injectable({
  providedIn: 'root'
})
export class MedicacionHabitualService {
  private baseUrl: string = environment.baseUrl;
  private commonUrl = 'medicaciones-habituales';

  constructor(private http: HttpClient) {
  }

  getMedicacionesHabituales(idPaciente: number): Observable<MedicacionHabitual[]>{
    return this.http.get<MedicacionHabitual[]>(
      this.baseUrl + this.commonUrl + '?idPaciente=' + idPaciente);
  }

  createMedicacionHabitual(data: MedicacionHabitual): Observable<MedicacionHabitual> {
    return this.http.post<MedicacionHabitual>(
      this.baseUrl + this.commonUrl + '/', JSON.stringify(data));
  }

  updateMedicacionHabitual(id: number, data: MedicacionHabitual): Observable<MedicacionHabitual> {
    return this.http.put<MedicacionHabitual>(
      this.baseUrl + this.commonUrl + '/' + id, JSON.stringify(data));
  }

  deleteMedicacionHabitual(id: number): Observable<MedicacionHabitual> {
    return this.http.delete<MedicacionHabitual>(
      this.baseUrl + this.commonUrl + '/' + id);
  }
}
