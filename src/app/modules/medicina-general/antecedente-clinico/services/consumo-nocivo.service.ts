import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConsumoNocivo } from '../models/consumo-nocivo.model';
import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class ConsumoNocivoService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getConsumosNocivos(idPaciente: number): Observable<ConsumoNocivo[]>  {
    return this.http.get<ConsumoNocivo[]>(this.baseUrl + 'consumos-nocivos?idPaciente=' + idPaciente);
  }

  createConsumoNocivo(data: ConsumoNocivo): Observable<ConsumoNocivo> {
    return this.http.post<ConsumoNocivo>(this.baseUrl + 'consumos-nocivos/', JSON.stringify(data));
  }

  updateConsumoNocivo(id: number, data: ConsumoNocivo): Observable<ConsumoNocivo> {
    return this.http.put<ConsumoNocivo>(this.baseUrl + 'consumos-nocivos/' + id, JSON.stringify(data));
  }

  deleteConsumoNocivo(id: number): Observable<boolean> {
    return this.http.delete<boolean>(this.baseUrl + 'consumos-nocivos/' + id);
  }
}


