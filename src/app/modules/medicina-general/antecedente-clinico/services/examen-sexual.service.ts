import {Injectable} from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ExamenSexual} from '../models/examen-sexual.model';

@Injectable({
  providedIn: 'root'
})
export class ExamenSexualService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getExamenesSexuales(idPaciente: number): Observable<ExamenSexual[]> {
    return this.http.get<ExamenSexual[]>(this.baseUrl + 'examenes-sexuales?idPaciente=' + idPaciente);
  }

  createExamenSexual(data: ExamenSexual): Observable<ExamenSexual> {
    return this.http.post<ExamenSexual>(this.baseUrl + 'examenes-sexuales/', JSON.stringify(data));
  }

  updateExamenSexual(id: number, data: ExamenSexual): Observable<ExamenSexual> {
    return this.http.put<ExamenSexual>(this.baseUrl + 'examenes-sexuales/' + id, JSON.stringify(data));
  }

  deleteExamenSexual(id: number): Observable<boolean> {
    return this.http.delete<boolean>(this.baseUrl + 'examenes-sexuales/' + id);
  }
}
