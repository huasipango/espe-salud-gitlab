import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Discapacidad} from '../models/discapacidad.model';

@Injectable({
  providedIn: 'root'
})
export class DiscapacidadService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getDiscapacidades(idPaciente): Observable<Discapacidad[]>{
    return this.http.get<Discapacidad[]>(this.baseUrl + 'discapacidades?idPaciente=' + idPaciente);
  }

  createDiscapacidad(data: Discapacidad): Observable<Discapacidad> {
    return this.http.post<Discapacidad>(this.baseUrl  + 'discapacidades/', JSON.stringify(data));
  }

  updateDiscapacidad(id: number, data: Discapacidad): Observable<Discapacidad> {
    return this.http.put<Discapacidad>(this.baseUrl + 'discapacidades/' + id, JSON.stringify(data))
  }

  deleteDiscapacidad(id: number): Observable<Discapacidad> {
    return this.http.delete<Discapacidad>(this.baseUrl + 'discapacidades/' + id);
  }
}
