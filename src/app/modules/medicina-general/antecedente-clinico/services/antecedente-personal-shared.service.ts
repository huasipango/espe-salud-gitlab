import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {AntecedentePersonal} from '../models/antecedente-personal.model';

@Injectable({
  providedIn: 'root'
})
export class AntecedentePersonalSharedService {
  constructor() { }

  private subject: BehaviorSubject<AntecedentePersonal> =
    new BehaviorSubject<AntecedentePersonal>(null);
  public antecedentePersonal$: Observable<AntecedentePersonal> = this.subject.asObservable();

  public setAntecedentePersonal(antecedente: AntecedentePersonal): void{
    this.subject.next(antecedente);
  }

}
