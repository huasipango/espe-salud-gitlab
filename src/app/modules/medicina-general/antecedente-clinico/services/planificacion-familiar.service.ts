import {Injectable} from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PlanificacionFamiliar} from '../models/planificacion-familiar.model';

@Injectable({
  providedIn: 'root'
})
export class PlanificacionFamiliarService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getPlanificacionesFamiliares(idPaciente: number): Observable<PlanificacionFamiliar[]> {
    return this.http.get<PlanificacionFamiliar[]>(
      this.baseUrl + 'planificaciones-familiares?idPaciente=' + idPaciente);
  }

  createPlanificacionFamiliar(data: PlanificacionFamiliar): Observable<PlanificacionFamiliar> {
    return this.http.post<PlanificacionFamiliar>(
      this.baseUrl + 'planificaciones-familiares/', JSON.stringify(data));
  }

  updatePlanificacionFamiliar(id: number, data: PlanificacionFamiliar): Observable<PlanificacionFamiliar> {
    return this.http.put<PlanificacionFamiliar>(
      this.baseUrl + 'planificaciones-familiares/' + id, JSON.stringify(data));
  }

  deletePlanificacionFamiliar(id: number): Observable<boolean> {
    return this.http.delete<boolean>(this.baseUrl + 'planificaciones-familiares/' + id);
  }

}
