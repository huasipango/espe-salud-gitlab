import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {AntecedentePatologicoFamiliar} from 'src/app/modules/medicina-general/antecedente-clinico/models/patologico-familiar.model';

@Injectable({
  providedIn: 'root'
})
export class PatologicoFamiliarService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) { }

  getAntecedentesPatologicosFamiliares(idPaciente: number): Observable<AntecedentePatologicoFamiliar[]>{
    return this.http.get<AntecedentePatologicoFamiliar[]>(
      this.baseUrl + 'antecedentes-patologicos-familiares?idPaciente=' + idPaciente);
  }

  createAntecedentePatologicoFamiliar(antecedente: AntecedentePatologicoFamiliar): Observable<AntecedentePatologicoFamiliar> {
    return this.http.post<AntecedentePatologicoFamiliar>(
      this.baseUrl + 'antecedentes-patologicos-familiares/', JSON.stringify(antecedente));
  }

  updateAntecedentePatologicoFamiliar(id: number, antecedente: AntecedentePatologicoFamiliar): Observable<AntecedentePatologicoFamiliar> {
    return this.http.put<AntecedentePatologicoFamiliar>(
      this.baseUrl + 'antecedentes-patologicos-familiares/' + id, JSON.stringify(antecedente));
  }

  deleteAntecedentePatologicoFamiliar(id: number): Observable<AntecedentePatologicoFamiliar> {
    return this.http.delete<AntecedentePatologicoFamiliar>(this.baseUrl + 'antecedentes-patologicos-familiares/' + id);
  }
}
