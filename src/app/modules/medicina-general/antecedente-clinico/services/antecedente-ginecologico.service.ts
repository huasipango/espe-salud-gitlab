import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AntecedenteGinecologico} from '../models/antecedente-ginecologico.model';

@Injectable({
  providedIn: 'root'
})
export class AntecedenteGinecologicoService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getAntecedentesGinecologicos(idPaciente: number): Observable<AntecedenteGinecologico[]>{
    return this.http.get<AntecedenteGinecologico[]>(
      this.baseUrl + 'antecedentes-ginecologicos?idPaciente=' + idPaciente);
  }

  createAntecedenteGinecologico(data: AntecedenteGinecologico): Observable<AntecedenteGinecologico> {
    return this.http.post<AntecedenteGinecologico>(
      this.baseUrl + 'antecedentes-ginecologicos/', JSON.stringify(data));
  }

  updateAntecedenteGinecologico(id: number, data: AntecedenteGinecologico): Observable<AntecedenteGinecologico> {
    return this.http.put<AntecedenteGinecologico>(
      this.baseUrl + 'antecedentes-ginecologicos/' + id, JSON.stringify(data));
  }

  deleteAntecedenteGinecologico(id: number): Observable<boolean> {
    return this.http.delete<boolean>(this.baseUrl + 'antecedentes-ginecologicos/' + id);
  }
}
