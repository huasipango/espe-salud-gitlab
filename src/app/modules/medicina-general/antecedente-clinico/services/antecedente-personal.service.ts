import {Injectable} from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AntecedentePersonal} from '../models/antecedente-personal.model';
import {PeriodoGestacion} from 'src/app/modules/medicina-general/antecedente-clinico/models/periodo-gestacion-model';
import {PeriodoLactancia} from 'src/app/modules/medicina-general/antecedente-clinico/models/periodo-lactancia-model';

@Injectable({
  providedIn: 'root'
})
export class AntecedentePersonalService {
  private baseUrl: string = environment.baseUrl;
  private commonName = 'antecedentes-personales';

  constructor(private http: HttpClient) {}

  getAntecedentePersonal(idPaciente: number): Observable<AntecedentePersonal> {
    return this.http.get<AntecedentePersonal>(this.baseUrl + this.commonName + '?idPaciente=' + idPaciente);
  }

  createAntecedentePersonal(antecedentePersonal): Observable<AntecedentePersonal> {
    return this.http.post<AntecedentePersonal>(this.baseUrl + this.commonName + '/', JSON.stringify(antecedentePersonal));
  }

  updateAntecedentePersonal(id, data: AntecedentePersonal): Observable<AntecedentePersonal> {
    return this.http.put<AntecedentePersonal>(
      this.baseUrl + this.commonName + '/' + id, JSON.stringify(data));
  }

  initializePeriodoGestacion(id: number, data: PeriodoGestacion): Observable<AntecedentePersonal> {
    return this.http.post<AntecedentePersonal>(
      this.baseUrl + this.commonName + '/inicializar-gestacion/' + id, JSON.stringify(data));
  }

  finalizePeriodoGestacion(id: number, data: PeriodoLactancia): Observable<AntecedentePersonal> {
    return this.http.post<AntecedentePersonal>(
      this.baseUrl + this.commonName + '/finalizar-gestacion/' + id, JSON.stringify(data));
  }

  finalizePeriodoLactancia(id: number): Observable<AntecedentePersonal> {
    return this.http.post<AntecedentePersonal>(
      this.baseUrl + this.commonName + '/finalizar-lactancia/' + id, null);
  }
}
