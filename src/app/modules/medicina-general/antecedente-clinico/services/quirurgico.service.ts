import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AntecedenteQuirurgico} from '../models/antecedente-quirurgico.model';

@Injectable({
  providedIn: 'root'
})
export class QuirurgicoService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getAntecedentesQuirurgicos(idPaciente: number): Observable<AntecedenteQuirurgico[]>{
    return this.http.get<AntecedenteQuirurgico[]>(this.baseUrl  + 'antecedentes-quirurgicos?idPaciente=' + idPaciente);
  }

  createAntecedenteQuirurgico(data: AntecedenteQuirurgico): Observable<AntecedenteQuirurgico> {
    return this.http.post<AntecedenteQuirurgico>(this.baseUrl + 'antecedentes-quirurgicos/', JSON.stringify(data));
  }

  updateAntecedenteQuirurgico(id: number, data: AntecedenteQuirurgico): Observable<AntecedenteQuirurgico> {
    return this.http.put<AntecedenteQuirurgico>(this.baseUrl + 'antecedentes-quirurgicos/' + id, JSON.stringify(data));
  }

  deleteAntecedenteQuirurgico(id: number): Observable<AntecedenteQuirurgico> {
    return this.http.delete<AntecedenteQuirurgico>(this.baseUrl + 'antecedentes-quirurgicos/' + id);
  }
}
