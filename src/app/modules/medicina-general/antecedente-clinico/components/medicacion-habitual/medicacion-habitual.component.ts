import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {MedicacionHabitual} from '../../models/medicacion-habitual.model';
import {MatTableDataSource} from '@angular/material/table';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import {MedicacionHabitualService} from '../../services/medicacion-habitual.service';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {MedicacionHabitualModalComponent} from './medicacion-habitual-modal/medicacion-habitual-modal.component';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {FrecuenciaMedicacionHabitual} from 'src/app/core/models/catalogo/frecuencia-medicacion-habitual.model';

@Component({
  selector: 'vex-medicacion-habitual',
  templateUrl: './medicacion-habitual.component.html',
  styleUrls: ['./medicacion-habitual.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms,
    scaleFadeIn400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class MedicacionHabitualComponent implements OnInit, AfterViewInit, OnDestroy {

  medicaciones: MedicacionHabitual[];

  @Input()
  columns: TableColumn<MedicacionHabitual>[] = [
    {label: 'Descripción Medicamento', property: 'descripcionMedicamento', type: 'text', visible: true},
    {label: 'Frecuencia', property: 'frecuencia', type: 'object', visible: true},
    {label: 'Cantidad', property: 'cantidad', type: 'text', visible: true},
    {label: 'Observaciones', property: 'observacion', type: 'text', visible: true, cssClasses: ['text-wrap']},
    {label: 'Acciones', property: 'menu', type: 'button', visible: true}
  ];

  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<MedicacionHabitual> | null;

  icEdit = icEdit;
  icSearch = icSearch;
  icMoreHoriz = icMoreHoriz;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  messages = USER_MESSAGES;

  pacienteActual: Paciente;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    protected medicacionHabitualService: MedicacionHabitualService,
    private pacienteGlobalService: PacienteGlobalService
  ) {
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getMedicacionesHabituales();
        }
      });
  }

  getMedicacionesHabituales(): void {
    this.medicacionHabitualService
      .getMedicacionesHabituales(this.pacienteActual.id)
      .subscribe(
        (data: MedicacionHabitual[]) => {
          this.medicaciones = data;
          this.dataSource.data = this.medicaciones;
        }
      );
  }

  openDeleteDialog(medicacion: MedicacionHabitual) {
    this.dialog.open(DeleteModalComponent, {
      data: this.messages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') {
        this.deleteMedicacionHabitual(medicacion);
      }
    });
  }

  deleteMedicacionHabitual(medicacion: MedicacionHabitual) {
    this.medicacionHabitualService.deleteMedicacionHabitual(medicacion.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification(this.messages.deletedSuccessMessage, 'CERRAR');
          this.medicaciones.splice(
            this.medicaciones.findIndex((existing) =>
              existing.id === medicacion.id), 1
          );
          this.dataSource.connect().next(this.medicaciones);
        } else {
          this.showNotification(this.messages.deleteFailMessage, 'CERRAR');
        }
      });
  }

  createMedicacionHabitual() {
    this.dialog.open(MedicacionHabitualModalComponent, {
      width: '550px',
      maxWidth: '100%'
    }).afterClosed()
      .subscribe((medicacion: MedicacionHabitual) => {
        if (medicacion) {
          this.showNotification(this.messages.createdSuccessMessage, 'OK');
          this.getMedicacionesHabituales();
        }
      });
  }

  updateMedicacionHabitual(medicacion: MedicacionHabitual) {
    this.dialog
      .open(MedicacionHabitualModalComponent, {
        data: medicacion,
        width: '550px',
        maxWidth: '100%'
      }).afterClosed()
      .subscribe(updatedMedicacion => {
        if (updatedMedicacion) {
          this.showNotification(this.messages.updatedSuccessMessage, 'OK');
          this.getMedicacionesHabituales();
        }
      });
  }

  get visibleColumns() {
    return this.columns
      .filter(column => column.visible)
      .map(column => column.property);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(column: TableColumn<T>) {
    return column.property;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  getFrecuencia(frecuencia: FrecuenciaMedicacionHabitual) {
    return frecuencia ? frecuencia.nombre : '';
  }

  ngOnDestroy() {
  }
}
