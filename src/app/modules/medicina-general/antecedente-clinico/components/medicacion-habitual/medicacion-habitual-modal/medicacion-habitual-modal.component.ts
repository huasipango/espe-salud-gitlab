import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MedicacionHabitual} from '../../../models/medicacion-habitual.model';
import icClose from '@iconify/icons-ic/twotone-close';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPerson from '@iconify/icons-ic/twotone-person';
import icPills from '@iconify/icons-fa-solid/pills';
import icHourGlass from '@iconify/icons-fa-solid/hourglass-start';
import icSortNumeric from '@iconify/icons-fa-solid/sort-numeric-up-alt';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {Observable} from 'rxjs';
import {FrecuenciaMedicacionHabitual} from 'src/app/core/models/catalogo/frecuencia-medicacion-habitual.model';
import {AntecedentePersonalSharedService} from 'src/app/modules/medicina-general/antecedente-clinico/services/antecedente-personal-shared.service';
import {MedicacionHabitualService} from 'src/app/modules/medicina-general/antecedente-clinico/services/medicacion-habitual.service';
import {LoadingService} from 'src/app/core/services/loading.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
@Component({
  selector: 'vex-medicacion-habitual-modal',
  templateUrl: './medicacion-habitual-modal.component.html',
  styleUrls: ['./medicacion-habitual-modal.component.scss']
})
export class MedicacionHabitualModalComponent implements OnInit {

  form: FormGroup;
  mode: 'create' | 'update' = 'create';

  icClose = icClose;
  icPills = icPills;
  icSortNumeric = icSortNumeric;
  icDelete = icDelete;

  icPerson = icPerson;
  icHourGlass = icHourGlass;

  frecuenciaMedicacionHabitual: Observable<FrecuenciaMedicacionHabitual[]>;
  messages = USER_MESSAGES;
  errorMessages = FORM_ERROR_MESSAGES;
  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: MedicacionHabitual,
    private dialogRef: MatDialogRef<MedicacionHabitualModalComponent>,
    private fb: FormBuilder,
    private catalogoService: CatalogoService,
    private antecedenteSharedService: AntecedentePersonalSharedService,
    private medicacionService: MedicacionHabitualService,
    private loadingService: LoadingService,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit(): void {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as MedicacionHabitual;
    }
    this.frecuenciaMedicacionHabitual = this.catalogoService.getFrecuenciasMedicacionHabitual();
    this.form = this.fb.group({
      id: this.defaults.id || null,
      descripcionMedicamento: [this.defaults.descripcionMedicamento || '', Validators.required],
      idFrecuencia: [this.defaults.idFrecuencia || '', Validators.required],
      cantidad: [this.defaults.cantidad || 1, Validators.required],
      observacion: this.defaults.observacion || '',
      idAntecedentePersonal: [null, Validators.required]
    });

    this.antecedenteSharedService.antecedentePersonal$
      .subscribe((antecedente) => {
        if (antecedente) {
          this.form.patchValue({idAntecedentePersonal: antecedente.id});
        }
      });
  }

  save() {
    const medicacionHabitual = this.form.value;
    if (this.mode === 'create') {
      this.create(medicacionHabitual);
    } else if (this.mode === 'update') {
      this.update(medicacionHabitual);
    }
  }

  create(medicacion: MedicacionHabitual) {
    this.loadingService.showLoaderUntilCompleted(
      this.medicacionService.createMedicacionHabitual(medicacion)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.createdFailedMessage, 'CERRAR');
    });
  }

  update(medicacion: MedicacionHabitual) {
    this.loadingService.showLoaderUntilCompleted(
      this.medicacionService.updateMedicacionHabitual(medicacion.id, medicacion)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.updatedFailedMessage, 'CERRAR');
    });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }
}
