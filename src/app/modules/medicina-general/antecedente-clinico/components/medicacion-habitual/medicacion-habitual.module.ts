import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MedicacionHabitualComponent } from './medicacion-habitual.component';
import { MedicacionHabitualModalComponent } from './medicacion-habitual-modal/medicacion-habitual-modal.component';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {IconModule} from '@visurel/iconify-angular';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ExtendedModule, FlexLayoutModule, FlexModule} from '@angular/flex-layout';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {PageLayoutModule} from 'src/@vex/components/page-layout/page-layout.module';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatRippleModule} from '@angular/material/core';
import {MatInputModule} from '@angular/material/input';
import {MatSliderModule} from '@angular/material/slider';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';



@NgModule({
  declarations: [MedicacionHabitualComponent, MedicacionHabitualModalComponent],
  imports: [
    CommonModule,
    PageLayoutModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatCheckboxModule,
    MatIconModule,
    FlexLayoutModule,
    MatButtonModule,
    MatMenuModule,
    IconModule,
    FormsModule,
    MatTooltipModule,
    ReactiveFormsModule,
    ContainerModule,
    MatSelectModule,
    MatDialogModule,
    MatSidenavModule,
    MatButtonToggleModule,
    MatRippleModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSliderModule,
    LoadingModule,
  ],
  exports: [
    MedicacionHabitualComponent
  ]
})
export class MedicacionHabitualModule { }
