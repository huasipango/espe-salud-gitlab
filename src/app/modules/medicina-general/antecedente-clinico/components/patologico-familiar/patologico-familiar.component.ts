import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {AntecedentePatologicoFamiliar} from 'src/app/modules/medicina-general/antecedente-clinico/models/patologico-familiar.model';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import icEdit from '@iconify/icons-fa-solid/pen';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icClose from '@iconify/icons-ic/twotone-close';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {PatologicoFamiliarService} from 'src/app/modules/medicina-general/antecedente-clinico/services/patologico-familiar.service';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {PatologicoFamiliarModalComponent} from 'src/app/modules/medicina-general/antecedente-clinico/components/patologico-familiar/patologico-familiar-modal/patologico-familiar-modal.component';
import {TipoEnfermedadCIE10} from 'src/app/core/models/catalogo/tipo-enfermedad-cie10.model';
import {TipoEnfermedadFamiliar} from 'src/app/core/models/catalogo/tipo-enfermedad-familiar.model';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {Parentesco} from 'src/app/core/models/catalogo/parentesco.model';

@Component({
  selector: 'vex-patologico-familiar',
  templateUrl: './patologico-familiar.component.html',
  styleUrls: ['./patologico-familiar.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms,
    scaleFadeIn400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class PatologicoFamiliarComponent implements OnInit, AfterViewInit {
  patologicosFamiliares: AntecedentePatologicoFamiliar[];

  @Input()
  columns: TableColumn<AntecedentePatologicoFamiliar>[] = [
    {label: 'Tipo de enfermedad', property: 'tipoEnfermedadFamiliar', type: 'object', visible: true},
    {label: 'Parentesco', property: 'parentesco', type: 'object', visible: true},
    {label: 'Diagnóstico', property: 'diagnostico', type: 'text', visible: true},
    {label: 'Observaciones', property: 'observacion', type: 'text', visible: true, cssClasses: ['text-wrap']},
    {label: 'Acciones', property: 'menu', type: 'button', visible: true}
  ];

  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<AntecedentePatologicoFamiliar> | null;

  icEdit = icEdit;
  icSearch = icSearch;
  icMoreHoriz = icMoreHoriz;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icClose = icClose;

  pacienteActual: Paciente;
  messages = USER_MESSAGES;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    protected patologicoFamiliarService: PatologicoFamiliarService,
    private pacienteGlobalService: PacienteGlobalService
  ) {
  }


  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getAntecedentes();
        }
      });
  }

  getAntecedentes(): void {
    this.patologicoFamiliarService
      .getAntecedentesPatologicosFamiliares(this.pacienteActual.id)
      .subscribe(
        (data: AntecedentePatologicoFamiliar[]) => {
          this.patologicosFamiliares = data;
          this.dataSource.data = this.patologicosFamiliares;
        }
      );
  }

  get visibleColumns() {
    return this.columns
      .filter(column => column.visible)
      .map(column => column.property);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(column: TableColumn<T>) {
    return column.property;
  }

  createPatologicoFamiliar() {
    this.dialog.open(PatologicoFamiliarModalComponent, {
      width: '600px',
      maxWidth: '100%'
    }).afterClosed()
      .subscribe((antecedente: AntecedentePatologicoFamiliar) => {
        if (antecedente) {
          this.showNotification(this.messages.createdSuccessMessage, 'OK');
          this.getAntecedentes();
        }
      });
  }

  updatePatologicoFamiliar(antecedente: AntecedentePatologicoFamiliar): void {
    this.dialog.open(PatologicoFamiliarModalComponent, {
      data: antecedente,
      width: '600px',
      maxWidth: '100%'
    }).afterClosed()
      .subscribe(updatedAntecedente => {
        if (updatedAntecedente) {
          this.showNotification(this.messages.updatedSuccessMessage, 'OK');
          this.getAntecedentes();
        }
      });
  }

  openDeleteDialog(antecedente: AntecedentePatologicoFamiliar) {
    this.dialog.open(DeleteModalComponent, {
      data: this.messages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') {
        this.deletePatologicoFamiliar(antecedente);
      }
    });
  }

  deletePatologicoFamiliar(antecedente: AntecedentePatologicoFamiliar) {
    this.patologicoFamiliarService.deleteAntecedentePatologicoFamiliar(antecedente.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification(this.messages.deletedSuccessMessage, 'CERRAR');
          this.patologicosFamiliares.splice(
            this.patologicosFamiliares.findIndex((existing) =>
              existing.id === antecedente.id), 1
          );
          this.dataSource.connect().next(this.patologicosFamiliares);
        } else {
          this.showNotification(this.messages.deleteFailMessage, 'CERRAR');
        }
      });
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  getTipoEnfermedadFamiliar(tipoEnfermedad: TipoEnfermedadFamiliar): string {
    return tipoEnfermedad ? tipoEnfermedad.nombre : '';
  }
  getParentesco(parentesco: Parentesco): string {
    return parentesco ? parentesco.nombre : '';
  }

}
