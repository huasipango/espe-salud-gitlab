import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatologicoFamiliarComponent } from './patologico-familiar.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {IconModule} from '@visurel/iconify-angular';
import {MatMenuModule} from '@angular/material/menu';
import {MatPaginatorModule} from '@angular/material/paginator';
import { PatologicoFamiliarModalComponent } from './patologico-familiar-modal/patologico-familiar-modal.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';



@NgModule({
  declarations: [PatologicoFamiliarComponent, PatologicoFamiliarModalComponent],
  exports: [
    PatologicoFamiliarComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatButtonModule,
    MatTooltipModule,
    MatIconModule,
    MatTableModule,
    MatSortModule,
    IconModule,
    MatMenuModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatInputModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatMomentDateModule,
    LoadingModule
  ]
})
export class PatologicoFamiliarModule { }
