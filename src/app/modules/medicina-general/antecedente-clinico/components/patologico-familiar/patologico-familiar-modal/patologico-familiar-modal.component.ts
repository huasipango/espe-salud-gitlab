import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import icClose from '@iconify/icons-ic/twotone-close';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPerson from '@iconify/icons-ic/twotone-person';
import icParentesco from '@iconify/icons-fa-solid/recycle';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';

import icThermometerEmpty from '@iconify/icons-fa-solid/thermometer-empty';
import icNotesMedical from '@iconify/icons-fa-solid/notes-medical';
import {TipoEnfermedadFamiliar} from 'src/app/core/models/catalogo/tipo-enfermedad-familiar.model';
import {Observable} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AntecedentePatologicoFamiliar} from 'src/app/modules/medicina-general/antecedente-clinico/models/patologico-familiar.model';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {AntecedentePersonalSharedService} from 'src/app/modules/medicina-general/antecedente-clinico/services/antecedente-personal-shared.service';
import {LoadingService} from 'src/app/core/services/loading.service';
import {PatologicoFamiliarService} from 'src/app/modules/medicina-general/antecedente-clinico/services/patologico-familiar.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TipoEnfermedadCIE10} from 'src/app/core/models/catalogo/tipo-enfermedad-cie10.model';
import {UntilDestroy} from '@ngneat/until-destroy';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {Parentesco} from 'src/app/core/models/catalogo/parentesco.model';

@UntilDestroy()
@Component({
  selector: 'vex-patologico-familiar-modal',
  templateUrl: './patologico-familiar-modal.component.html',
  styleUrls: ['./patologico-familiar-modal.component.scss']
})
export class PatologicoFamiliarModalComponent implements OnInit {
  form: FormGroup;
  mode: 'create' | 'update' = 'create';

  icClose = icClose;
  icDelete = icDelete;
  icPerson = icPerson;
  icThermometerEmpty = icThermometerEmpty;
  icParentesco = icParentesco;
  icNotesMedical = icNotesMedical;
  icArrowDropDown = icArrowDropDown;

  tiposEnfermedadFamiliar: Observable<TipoEnfermedadFamiliar[]>;
  perentescos: Observable<Parentesco[]>;

  filteredCodigosCie: Observable<TipoEnfermedadCIE10[]>;
  messages = USER_MESSAGES;
  errorMessages = FORM_ERROR_MESSAGES;

  constructor(
    private dialogRef: MatDialogRef<PatologicoFamiliarModalComponent>,
    @Inject(MAT_DIALOG_DATA) public defaults: AntecedentePatologicoFamiliar,
    private fb: FormBuilder,
    protected catalogoService: CatalogoService,
    private antecedenteSharedService: AntecedentePersonalSharedService,
    private loadingService: LoadingService,
    private snackbar: MatSnackBar,
    private patologicoService: PatologicoFamiliarService
  ) { }

  ngOnInit(): void {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as AntecedentePatologicoFamiliar;
    }
    this.tiposEnfermedadFamiliar = this.catalogoService.getTiposEnfermedadFamiliar();
    this.perentescos = this.catalogoService.getParentesco();
    this.form = this.fb.group({
      id: this.defaults.id || null,
      idParentesco: [this.defaults.idParentesco || '', Validators.required],
      observacion: [this.defaults.observacion || ''],
      diagnostico: [this.defaults.diagnostico || '', Validators.required],
      idTipoEnfermedadFamiliar: [this.defaults.idTipoEnfermedadFamiliar || '', Validators.required],
      idAntecedentePersonal: [null, Validators.required]
    });

    this.antecedenteSharedService.antecedentePersonal$
      .subscribe((antecedente) => {
        if (antecedente) {
          this.form.patchValue({idAntecedentePersonal: antecedente.id});
        }
      });
  }

  save() {
    const antecedente = this.form.value;
    if (this.mode === 'create') {
      this.createAntecedente(antecedente);
    } else if (this.mode === 'update') {
      this.updateAntecedente(antecedente);
    }
  }

  createAntecedente(antecedente: AntecedentePatologicoFamiliar): void {
    this.loadingService.showLoaderUntilCompleted(
      this.patologicoService.createAntecedentePatologicoFamiliar(antecedente)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.createdFailedMessage, 'CERRAR');
    });
  }

  updateAntecedente(antecedente: AntecedentePatologicoFamiliar): void {
    this.loadingService.showLoaderUntilCompleted(
      this.patologicoService.updateAntecedentePatologicoFamiliar(antecedente.id, antecedente)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.updatedFailedMessage, 'CERRAR');
    });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  searchCie(value: any) {
    if (value !== '' && typeof value === 'string' && value.trim().length > 1) {
      this.filteredCodigosCie = this.catalogoService.getCodigosCIE10ByQuery(value);
    }
  }

  displayFn(cie: TipoEnfermedadCIE10): string {
    return cie ? `${cie.codigo} -- ${cie.nombre}` : '';
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }
}
