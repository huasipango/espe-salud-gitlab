import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {AntecedenteQuirurgico} from '../../models/antecedente-quirurgico.model';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {QuirurgicoService} from '../../services/quirurgico.service';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {DateUtil} from 'src/app/core/utils/date-utils';
import {QuirurgicoModalComponent} from './quirurgico-modal/quirurgico-modal.component';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import {UntilDestroy} from '@ngneat/until-destroy';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';

@UntilDestroy()
@Component({
  selector: 'vex-quirurgico',
  templateUrl: './quirurgico.component.html',
  styleUrls: ['./quirurgico.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms,
    scaleFadeIn400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class QuirurgicoComponent implements OnInit, AfterViewInit, OnDestroy {

  quirurgicos: AntecedenteQuirurgico[];

  @Input()
  columns: TableColumn<AntecedenteQuirurgico>[] = [
    {label: 'Diagnóstico prequirúrgico', property: 'idDiagnosticoPreQuirurgico', type: 'text', visible: true},
    {label: 'Procedimiento quirurgico', property: 'procedimientoQuirurgico', type: 'text', visible: true},
    {label: 'Fecha procedimiento', property: 'fechaProcedimiento', type: 'date', visible: true},
    {label: 'Secuelas', property: 'secuelas', type: 'text', visible: true},
    {label: 'Acciones', property: 'menu', type: 'button', visible: true}
  ];

  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<AntecedenteQuirurgico> | null;

  icEdit = icEdit;
  icSearch = icSearch;
  icMoreHoriz = icMoreHoriz;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;

  pacienteActual: Paciente;
  messages = USER_MESSAGES;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    protected quirurgicoService: QuirurgicoService,
    private pacienteGlobalService: PacienteGlobalService
  ) {
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getAntecedentesQuirurgicos();
        }
      });
  }

  getAntecedentesQuirurgicos(): void {
    this.quirurgicoService.getAntecedentesQuirurgicos(this.pacienteActual.id)
      .subscribe(
        (data: AntecedenteQuirurgico[]) => {
          this.quirurgicos = data;
          this.dataSource.data = this.quirurgicos;
        }
      );
  }

  openDeleteDialog(antecedente: AntecedenteQuirurgico) {
    this.dialog.open(DeleteModalComponent, {
      data: this.messages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') { this.deleteQuirurgico(antecedente); }
    });
  }

  deleteQuirurgico(antecedente: AntecedenteQuirurgico) {
    this.quirurgicoService.deleteAntecedenteQuirurgico(antecedente.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification(this.messages.deletedSuccessMessage, 'CERRAR');
          this.quirurgicos.splice(
            this.quirurgicos.findIndex((existing) =>
              existing.id === antecedente.id), 1
          );
          this.dataSource.connect().next(this.quirurgicos);
        } else {
          this.showNotification(this.messages.deleteFailMessage, 'CERRAR');
        }
      });
  }

  createQuirurgico() {
    this.dialog.open(QuirurgicoModalComponent, {
        width: '600px',
        maxWidth: '100%'
      }).afterClosed()
      .subscribe((antecedente: AntecedenteQuirurgico) => {
        if (antecedente) {
          this.showNotification(this.messages.createdSuccessMessage, 'OK');
          this.getAntecedentesQuirurgicos();
        }
      });
  }

  updateQuirurgico(antecedente: AntecedenteQuirurgico) {
    this.dialog.open(QuirurgicoModalComponent, {
        data: antecedente,
        width: '600px',
        maxWidth: '100%'
      }).afterClosed()
      .subscribe(updatedAntecedente => {
        if (updatedAntecedente) {
          this.showNotification(this.messages.updatedSuccessMessage, 'OK');
          this.getAntecedentesQuirurgicos();
        }
      });
  }

  get visibleColumns() {
    return this.columns
      .filter(column => column.visible)
      .map(column => column.property);
  }

  getFormattedDate(date: string) {
    return DateUtil.showDateFormat(date);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(column: TableColumn<T>) {
    return column.property;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  ngOnDestroy() {
  }

}
