import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AntecedenteQuirurgico} from '../../../models/antecedente-quirurgico.model';
import icClose from '@iconify/icons-ic/twotone-close';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPerson from '@iconify/icons-ic/twotone-person';
import icNotesMedical from '@iconify/icons-fa-solid/notes-medical';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';
import icAssignment from '@iconify/icons-ic/assignment';
import icHealing from '@iconify/icons-ic/healing';
import {Observable} from 'rxjs';
import {TipoEnfermedadCIE10} from 'src/app/core/models/catalogo/tipo-enfermedad-cie10.model';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {debounceTime, distinctUntilChanged, startWith} from 'rxjs/operators';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {LoadingService} from 'src/app/core/services/loading.service';
import {QuirurgicoService} from 'src/app/modules/medicina-general/antecedente-clinico/services/quirurgico.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {AntecedentePersonalSharedService} from 'src/app/modules/medicina-general/antecedente-clinico/services/antecedente-personal-shared.service';
import {forbiddenObjectValidator} from 'src/app/shared/validators/forbbiden-object-validator';

@UntilDestroy()
@Component({
  selector: 'vex-quirurgico-modal',
  templateUrl: './quirurgico-modal.component.html',
  styleUrls: ['./quirurgico-modal.component.scss']
})
export class QuirurgicoModalComponent implements OnInit {
  form: FormGroup;
  mode: 'create' | 'update' = 'create';

  icClose = icClose;
  icDelete = icDelete;

  icPerson = icPerson;
  icAssignment = icAssignment;
  icHealing = icHealing;
  icNotesMedical = icNotesMedical;
  icArrowDropDown = icArrowDropDown;
  maxDate = new Date();

  diagnosticoCtrl: FormControl;
  filteredCodigosCie: Observable<TipoEnfermedadCIE10[]>;
  messages = USER_MESSAGES;
  errorMessages = FORM_ERROR_MESSAGES;

  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: AntecedenteQuirurgico,
    private dialogRef: MatDialogRef<QuirurgicoModalComponent>,
    private fb: FormBuilder,
    private catalogoService: CatalogoService,
    private loadingService: LoadingService,
    private quirurgicoService: QuirurgicoService,
    private antecedenteSharedService: AntecedentePersonalSharedService,
    private snackbar: MatSnackBar
  ) {
  }

  ngOnInit(): void {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as AntecedenteQuirurgico;
    }

    this.diagnosticoCtrl = new FormControl(
      this.defaults.idDiagnosticoPreQuirurgico, [Validators.required, forbiddenObjectValidator]
    );

    if (this.defaults.idDiagnosticoPreQuirurgico) {
      this.catalogoService.getCodigoCIE10(this.defaults.idDiagnosticoPreQuirurgico)
        .subscribe((codigo) => {
          if (codigo) {
            this.diagnosticoCtrl.patchValue(codigo);
          }
        });
    }

    this.diagnosticoCtrl.valueChanges
      .pipe(
        startWith(''),
        debounceTime(400),
        distinctUntilChanged(),
        untilDestroyed(this)
      ).subscribe(value => this.searchCie(value || ''));

    this.form = this.fb.group({
      id: this.defaults.id || null,
      idDiagnosticoPreQuirurgico: this.diagnosticoCtrl,
      fechaProcedimiento: [this.defaults.fechaProcedimiento || null, Validators.required],
      procedimientoQuirurgico: [this.defaults.procedimientoQuirurgico || '', Validators.required],
      secuelas: [this.defaults.secuelas || ''],
      idAntecedentePersonal: [null, Validators.required]
    });

    this.antecedenteSharedService.antecedentePersonal$
      .subscribe((antecedente) => {
        if (antecedente) {
          this.form.patchValue({idAntecedentePersonal: antecedente.id});
        }
      });
  }

  save() {
    const antecedente = this.form.value;
    const cie10 = antecedente.idDiagnosticoPreQuirurgico;
    antecedente.idDiagnosticoPreQuirurgico = cie10.codigo;
    if (this.mode === 'create') {
      this.createAntecedente(antecedente);
    } else if (this.mode === 'update') {
      this.updateAntecedente(antecedente);
    }
  }

  createAntecedente(antecedente: AntecedenteQuirurgico): void {
    this.loadingService.showLoaderUntilCompleted(
      this.quirurgicoService.createAntecedenteQuirurgico(antecedente)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.createdFailedMessage, 'CERRAR');
    });
  }

  updateAntecedente(antecedente: AntecedenteQuirurgico): void {
    this.loadingService.showLoaderUntilCompleted(
      this.quirurgicoService.updateAntecedenteQuirurgico(antecedente.id, antecedente)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.updatedFailedMessage, 'CERRAR');
    });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  searchCie(value: any) {
    if (value !== '' && typeof value === 'string' && value.trim().length > 1) {
      this.filteredCodigosCie = this.catalogoService.getCodigosCIE10ByQuery(value);
    }
  }

  displayFn(cie: TipoEnfermedadCIE10): string {
    return cie ? `${cie.codigo} -- ${cie.nombre}` : '';
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }
}
