import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatMenuModule} from '@angular/material/menu';
import {IconModule} from '@visurel/iconify-angular';
import {MatIconModule} from '@angular/material/icon';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatTableModule} from '@angular/material/table';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatPaginatorModule} from '@angular/material/paginator';
import {PageLayoutModule} from 'src/@vex/components/page-layout/page-layout.module';
import {HttpClientModule} from '@angular/common/http';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {MatSortModule} from '@angular/material/sort';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatTooltipModule} from '@angular/material/tooltip';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDividerModule} from '@angular/material/divider';
import {MatNativeDateModule} from '@angular/material/core';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {ExamenSexualComponent} from './examen-sexual.component';
import {ExamenSexualModalComponent} from './examen-sexual-modal/examen-sexual-modal.component';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';

@NgModule({
  declarations: [ExamenSexualComponent, ExamenSexualModalComponent],
  exports: [
    ExamenSexualComponent
  ],
  imports: [
    CommonModule,
    MatMenuModule,
    IconModule,
    MatIconModule,
    MatCheckboxModule,
    FormsModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    PageLayoutModule,
    HttpClientModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    MatTableModule,
    MatSortModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatTooltipModule,
    ContainerModule,
    MatSelectModule,
    MatButtonToggleModule,
    MatDialogModule,
    MatSnackBarModule,
    MatAutocompleteModule,
    MatDividerModule,
    MatNativeDateModule,
    MatDatepickerModule,
    LoadingModule
  ]
})
export class ExamenSexualModule { }
