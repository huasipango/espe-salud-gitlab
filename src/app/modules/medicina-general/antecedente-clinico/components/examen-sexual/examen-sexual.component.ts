import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {ExamenSexual} from '../../models/examen-sexual.model';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import {MatDialog} from '@angular/material/dialog';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icFolder from '@iconify/icons-ic/twotone-folder';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatSnackBar} from '@angular/material/snack-bar';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {ExamenSexualService} from '../../services/examen-sexual.service';
import {ExamenSexualModalComponent} from './examen-sexual-modal/examen-sexual-modal.component';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {DateUtil} from 'src/app/core/utils/date-utils';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {TipoExamenSexual} from 'src/app/core/models/catalogo/tipo-examen-sexual.model';

@Component({
  selector: 'vex-examen-sexual',
  templateUrl: './examen-sexual.component.html',
  styleUrls: ['./examen-sexual.component.scss'],
  animations: [fadeInUp400ms, stagger40ms, scaleFadeIn400ms],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class ExamenSexualComponent implements OnInit, AfterViewInit, OnDestroy {
  examenesSexuales: ExamenSexual[];
  pacienteActual: Paciente;

  @Input()
  columns: TableColumn<ExamenSexual>[] = [
    {
      label: 'Estudio realizado',
      property: 'nombreExamen',
      type: 'object',
      visible: true
    },
    {
      label: 'Fecha de examen realizado',
      property: 'fechaExamen',
      type: 'text',
      visible: true
    },
    {
      label: 'Tiempo(AÑOS)',
      property: 'tiempoAnios',
      type: 'text',
      visible: true
    },
    {
      label: 'Resultados',
      property: 'resultado',
      type: 'text',
      visible: true
    },
    {label: 'Actions', property: 'menu', type: 'button', visible: true}
  ];
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<ExamenSexual> | null;

  icEdit = icEdit;
  icSearch = icSearch;
  icMoreHoriz = icMoreHoriz;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icFolder = icFolder;
  messages = USER_MESSAGES;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    protected examenSexualService: ExamenSexualService,
    private pacienteGlobalService: PacienteGlobalService
  ) {
  }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getExamenes();
        }
      });
  }

  getExamenes(): void {
    this.examenSexualService.getExamenesSexuales(this.pacienteActual.id)
      .subscribe(
        (data: ExamenSexual[]) => {
          this.examenesSexuales = data;
          this.dataSource.data = this.examenesSexuales;
        }
      );
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  createExamenSexual() {
    this.dialog.open(ExamenSexualModalComponent, {
      width: '500px',
      maxWidth: '100%'
    }).afterClosed().subscribe((examenSexual: ExamenSexual) => {
      if (examenSexual) {
        this.showNotification(this.messages.createdSuccessMessage, 'OK');
        this.getExamenes();
      }
    });
  }

  updateExamenSexual(examenSexual: ExamenSexual) {
    this.dialog.open(ExamenSexualModalComponent, {
      data: examenSexual,
      width: '500px',
      maxWidth: '100%'
    }).afterClosed().subscribe(updatedExamen => {
      if (updatedExamen) {
        this.showNotification(this.messages.updatedSuccessMessage, 'OK');
        this.getExamenes();
      }
    });
  }

  openDeleteDialog(examenSexual: ExamenSexual) {
    this.dialog.open(DeleteModalComponent, {
      data: this.messages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') {
        this.deleteExamenSexual(examenSexual);
      }
    });
  }

  deleteExamenSexual(examen: ExamenSexual) {
    this.examenSexualService.deleteExamenSexual(examen.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification(this.messages.deletedSuccessMessage, 'CERRAR');
          this.examenesSexuales.splice(
            this.examenesSexuales.findIndex((existing) =>
              existing.id === examen.id), 1
          );
          this.dataSource.connect().next(this.examenesSexuales);
        } else {
          this.showNotification(this.messages.deleteFailMessage, 'CERRAR');
        }
      });
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(column: TableColumn<T>) {
    return column.property;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  getFormattedDate(date: string){
    return DateUtil.showDateFormat(date);
  }

  getTipoExamen(examen: TipoExamenSexual): string {
    return examen ? examen.nombre : '';
  }

  ngOnDestroy() {
  }


}
