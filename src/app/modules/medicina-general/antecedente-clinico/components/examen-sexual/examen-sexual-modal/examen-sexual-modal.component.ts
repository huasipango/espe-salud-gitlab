import {Component, Inject, OnInit} from '@angular/core';
import icClose from '@iconify/icons-ic/twotone-close';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icPerson from '@iconify/icons-ic/twotone-person';
import icNotesMedical from '@iconify/icons-fa-solid/notes-medical';
import icClock from '@iconify/icons-fa-solid/clock';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';

import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {TipoExamenSexual} from 'src/app/core/models/catalogo/tipo-examen-sexual.model';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {ExamenSexual} from '../../../models/examen-sexual.model';
import {SexoEnum} from 'src/app/core/enums/sexo.enum';
import {Observable} from 'rxjs';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {AntecedentePersonalSharedService} from 'src/app/modules/medicina-general/antecedente-clinico/services/antecedente-personal-shared.service';
import {LoadingService} from 'src/app/core/services/loading.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ExamenSexualService} from 'src/app/modules/medicina-general/antecedente-clinico/services/examen-sexual.service';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';

@Component({
  selector: 'vex-examen-sexual-modal',
  templateUrl: './examen-sexual-modal.component.html',
  styleUrls: ['./examen-sexual-modal.component.scss']
})
export class ExamenSexualModalComponent implements OnInit {

  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  icClose = icClose;
  icDelete = icDelete;
  icNotesMedical = icNotesMedical;
  icClock = icClock;
  icPerson = icPerson;
  icArrowDropDown = icArrowDropDown;
  messages = USER_MESSAGES;
  maxDate = new Date();
  icPhone = icPhone;
  tiposExamen: Observable<TipoExamenSexual[]>;
  pacienteActual: Paciente;
  errorMessages = FORM_ERROR_MESSAGES;

  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: ExamenSexual,
    private dialogRef: MatDialogRef<ExamenSexualModalComponent>,
    private fb: FormBuilder,
    private catalogoService: CatalogoService,
    private pacienteGlobalService: PacienteGlobalService,
    private antecedenteSharedService: AntecedentePersonalSharedService,
    private loadingService: LoadingService,
    private snackbar: MatSnackBar,
    private examenSexualService: ExamenSexualService
  ) {
  }

  ngOnInit() {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as ExamenSexual;
    }
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getTiposdeExamen();
        }
      });
  }

  getTiposdeExamen(): void {
    if (this.pacienteActual.sexo === SexoEnum.MUJER) {
      this.tiposExamen = this.catalogoService.getTipoExamenesSexualesMujeres();
    } else {
      this.tiposExamen = this.catalogoService.getTiposExamenesSexualesHombres();
    }
    this.initializeForm();
  }

  initializeForm(): void {
    this.form = this.fb.group({
      id: this.defaults.id || null,
      idNombreExamen: [this.defaults.idNombreExamen || null, Validators.required],
      fechaExamen: [this.defaults.fechaExamen, null],
      resultado: [this.defaults.resultado || null, [Validators.required]],
      tiempoAnios: [this.defaults.tiempoAnios || null, Validators.required],
      idAntecedentePersonal: [null, Validators.required]
    });
    this.antecedenteSharedService.antecedentePersonal$
      .subscribe((antecedente) => {
        if (antecedente) {
          this.form.patchValue({idAntecedentePersonal: antecedente.id});
        }
      });
  }

  save() {
    const examenSexual: ExamenSexual = this.form.value;
    if (this.mode === 'create') {
      this.createExamenSexual(examenSexual);
    } else if (this.mode === 'update') {
      this.updateExamenSexual(examenSexual);
    }
  }

  displayFn(examen: TipoExamenSexual): string {
    return examen && examen.nombre ? examen.nombre : '';
  }

  createExamenSexual(examen: ExamenSexual): void {
    this.loadingService.showLoaderUntilCompleted(
      this.examenSexualService.createExamenSexual(examen)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.createdFailedMessage, 'CERRAR');
    });
  }

  updateExamenSexual(examen: ExamenSexual): void {
    this.loadingService.showLoaderUntilCompleted(
      this.examenSexualService.updateExamenSexual(examen.id, examen)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.updatedFailedMessage, 'CERRAR');
    });
  }


  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }
}
