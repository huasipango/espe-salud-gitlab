import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icFolder from '@iconify/icons-ic/twotone-folder';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {PlanificacionFamiliar} from '../../models/planificacion-familiar.model';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {PlanificacionFamiliarService} from '../../services/planificacion-familiar.service';
import {PlanificacionFamiliarModalComponent} from './planificacion-familiar-modal/planificacion-familiar-modal.component';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {UntilDestroy} from '@ngneat/until-destroy';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {TipoPlanificacionFamiliar} from 'src/app/core/models/catalogo/tipo-planificacion-familial.model';

@UntilDestroy()
@Component({
  selector: 'vex-planificacion-familiar',
  templateUrl: './planificacion-familiar.component.html',
  styleUrls: ['./planificacion-familiar.component.scss'],
  animations: [fadeInUp400ms, stagger40ms, scaleFadeIn400ms],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class PlanificacionFamiliarComponent implements OnInit, AfterViewInit, OnDestroy {

  planificaciones: PlanificacionFamiliar[];

  @Input()
  columns: TableColumn<PlanificacionFamiliar>[] = [
    {label: 'Fecha registro', property: 'fecha', type: 'text', visible: true},
    {
      label: 'Vida sexual activa',
      property: 'vidaSexualActiva',
      type: 'text',
      visible: true
    },
    {
      label: 'Método planificacion',
      property: 'metodoPlanificacion',
      type: 'text',
      visible: true,
    },
    {
      label: 'Frecuencia',
      property: 'frecuenciaPlanificacionFamiliar',
      type: 'text',
      visible: true
    },
    {
      label: 'Edad inicio',
      property: 'edadInicioPlanificacionFamiliar',
      type: 'text',
      visible: true
    },
    {
      label: 'Tipo de planificación',
      property: 'tipoPlanificacionFamiliar',
      type: 'object',
      visible: true
    },
    {
      label: 'Observación',
      property: 'observacion',
      type: 'text',
      visible: false
    },
    {
      label: 'Hijos muertos',
      property: 'hijosMuertos',
      type: 'text',
      visible: false
    },
    {
      label: 'Hijos vivos',
      property: 'hijosVivos',
      type: 'text',
      visible: false
    },
    {label: 'Acciones', property: 'menu', type: 'button', visible: true}
  ];
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<PlanificacionFamiliar> | null;

  icEdit = icEdit;
  icSearch = icSearch;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icMoreHoriz = icMoreHoriz;
  icFolder = icFolder;
  pacienteActual: Paciente;
  messages = USER_MESSAGES;


  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    private planificacionFamiliarService: PlanificacionFamiliarService,
    private pacienteGlobalService: PacienteGlobalService
  ) {
  }

  get visibleColumns() {
    return this.columns
      .filter(column => column.visible)
      .map(column => column.property);
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getPlanificacionesFamiliares();
        }
      });
  }

  getPlanificacionesFamiliares(): void {
    this.planificacionFamiliarService.getPlanificacionesFamiliares(this.pacienteActual.id)
      .subscribe(
        (data: PlanificacionFamiliar[]) => {
          this.planificaciones = data;
          this.dataSource.data = this.planificaciones;
        }
      );
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  createPlanificacionFamiliar() {
    this.dialog.open(PlanificacionFamiliarModalComponent, {
      width: '650px',
      maxWidth: '100%'
    }).afterClosed()
      .subscribe((planificacion: PlanificacionFamiliar) => {
        if (planificacion) {
          this.showNotification(this.messages.createdSuccessMessage, 'OK');
          this.getPlanificacionesFamiliares();
        }
      });
  }

  updatePlanificacionFamiliar(planificacion: PlanificacionFamiliar) {
    this.dialog.open(PlanificacionFamiliarModalComponent, {
      data: planificacion,
      width: '650px',
      maxWidth: '100%'
    })
      .afterClosed()
      .subscribe(updatedPlanificacion => {
        if (updatedPlanificacion) {
          this.showNotification(this.messages.updatedSuccessMessage, 'OK');
          this.getPlanificacionesFamiliares();
        }
      });
  }

  openDeleteDialog(planificacion: PlanificacionFamiliar) {
    this.dialog.open(DeleteModalComponent, {
      data: this.messages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') {
        this.deletePlanificacionFamiliar(planificacion);
      }
    });
  }

  deletePlanificacionFamiliar(planificacion: PlanificacionFamiliar) {
    this.planificacionFamiliarService.deletePlanificacionFamiliar(planificacion.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification(this.messages.deletedSuccessMessage, 'CERRAR');
          this.planificaciones.splice(
            this.planificaciones.findIndex((existing) =>
              existing.id === planificacion.id), 1
          );
          this.dataSource.connect().next(this.planificaciones);
        } else {
          this.showNotification(this.messages.deleteFailMessage, 'CERRAR');
        }
      });
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  trackByProperty<T>(column: TableColumn<T>) {
    return column.property;
  }

  ngOnDestroy() {
  }

  getTipoPlanificacion(tipoPlanificacion: TipoPlanificacionFamiliar): string{
    return tipoPlanificacion ? tipoPlanificacion.nombre : '';
  }
}
