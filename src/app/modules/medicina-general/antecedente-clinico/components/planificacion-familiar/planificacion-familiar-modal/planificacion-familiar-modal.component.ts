import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icClose from '@iconify/icons-ic/twotone-close';
import icPrint from '@iconify/icons-ic/twotone-print';
import icDownload from '@iconify/icons-ic/twotone-cloud-download';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icPerson from '@iconify/icons-ic/twotone-person';
import icCheck from '@iconify/icons-ic/check';
import icMale from '@iconify/icons-fa-solid/male';
import icLocationCity from '@iconify/icons-ic/twotone-location-city';
import user from '@iconify/icons-fa-solid/user-check';
import chalkboard from '@iconify/icons-fa-solid/chalkboard-teacher';
import calendar from '@iconify/icons-fa-solid/calendar-day';
import clipboard from '@iconify/icons-fa-solid/clipboard-list';
import baby from '@iconify/icons-fa-solid/baby';
import {PlanificacionFamiliar} from '../../../models/planificacion-familiar.model';
import {TipoPlanificacionFamiliar} from 'src/app/core/models/catalogo/tipo-planificacion-familial.model';
import {SimpleCatalogUtil} from 'src/app/core/utils/simple-catalog-util';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {Observable} from 'rxjs';
import {SexoEnum} from 'src/app/core/enums/sexo.enum';
import {AntecedentePersonalSharedService} from 'src/app/modules/medicina-general/antecedente-clinico/services/antecedente-personal-shared.service';
import {LoadingService} from 'src/app/core/services/loading.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {PlanificacionFamiliarService} from 'src/app/modules/medicina-general/antecedente-clinico/services/planificacion-familiar.service';

@Component({
  selector: 'vex-planificacion-familiar-modal',
  templateUrl: './planificacion-familiar-modal.component.html',
  styleUrls: ['./planificacion-familiar-modal.component.scss']
})
export class PlanificacionFamiliarModalComponent implements OnInit {

  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  icMale = icMale;
  icMoreVert = icMoreVert;
  icClose = icClose;
  prevencionList = SimpleCatalogUtil.tresOpcionesCatalogo;

  icPrint = icPrint;
  icDownload = icDownload;
  icDelete = icDelete;
  icPerson = icPerson;
  icCheck = icCheck;
  user = user;
  chalkboard = chalkboard;
  calendar = calendar;
  baby = baby;
  clipboard = clipboard;
  icLocationCity = icLocationCity;
  icPhone = icPhone;
  minDate = new Date(1900, 1, 1);
  maxDate = new Date();

  listChoise = SimpleCatalogUtil.tresOpcionesCatalogo;
  listPlanificacion: Observable<TipoPlanificacionFamiliar[]>;
  listFrecuencia = SimpleCatalogUtil.frecuenciaPlanificacion;

  esMujer = false;
  pacienteActual: Paciente;
  messages = USER_MESSAGES;
  errorMessages = FORM_ERROR_MESSAGES;

  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: PlanificacionFamiliar,
    private dialogRef: MatDialogRef<PlanificacionFamiliarModalComponent>,
    private fb: FormBuilder,
    private catalogoService: CatalogoService,
    private pacienteGlobalService: PacienteGlobalService,
    private antecedenteSharedService: AntecedentePersonalSharedService,
    private loadingService: LoadingService,
    private snackbar: MatSnackBar,
    private planificacionService: PlanificacionFamiliarService
  ) {
  }

  ngOnInit() {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as PlanificacionFamiliar;
    }
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getTipoPlanificacion();
        }
      });
  }

  getTipoPlanificacion() {
    if (this.pacienteActual.sexo === SexoEnum.MUJER) {
      this.listPlanificacion = this.catalogoService.getTiposPlanificacionMujeres();
      this.esMujer = true;
    } else {
      this.listPlanificacion = this.catalogoService.getTiposPlanificacionHombres();
      this.esMujer = false;
    }
    this.initializeForm();
  }

  initializeForm(): void {
    this.form = this.fb.group({
      id: this.defaults.id || null,
      fecha: this.defaults.fecha || this.maxDate,
      vidaSexualActiva: [this.defaults.vidaSexualActiva || '', Validators.required],
      metodoPlanificacion: [this.defaults.metodoPlanificacion || this.prevencionList[0]],
      edadInicioPlanificacionFamiliar: [
        this.defaults.edadInicioPlanificacionFamiliar || ''
      ],
      frecuenciaPlanificacionFamiliar: [
        this.defaults.frecuenciaPlanificacionFamiliar || '', Validators.required
      ],
      idTipoPlanificacionFamiliar: [
        this.defaults.idTipoPlanificacionFamiliar || '',
        Validators.required
      ],
      hijosVivos: [this.defaults.hijosVivos || null],
      hijosMuertos: [this.defaults.hijosMuertos || null],
      observacion: [this.defaults.observacion || ''],
      idAntecedentePersonal: [null, Validators.required]
    });
    this.antecedenteSharedService.antecedentePersonal$
      .subscribe((antecedente) => {
        if (antecedente) {
          this.form.patchValue({idAntecedentePersonal: antecedente.id});
        }
      });
  }

  save() {
    const planificacion = this.form.value;
    if (this.mode === 'create') {
      this.createPlanificacion(planificacion);
    } else if (this.mode === 'update') {
      this.updatePlanificacion(planificacion);
    }
  }

  createPlanificacion(planificacion: PlanificacionFamiliar): void {
    this.loadingService.showLoaderUntilCompleted(
      this.planificacionService.createPlanificacionFamiliar(planificacion)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.createdFailedMessage, 'CERRAR');
    });
  }

  updatePlanificacion(planificacion: PlanificacionFamiliar): void {
    this.loadingService.showLoaderUntilCompleted(
      this.planificacionService.updatePlanificacionFamiliar(planificacion.id, planificacion)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.updatedFailedMessage, 'CERRAR');
    });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }
}
