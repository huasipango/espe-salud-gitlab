import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {Discapacidad} from '../../../models/discapacidad.model';
import {Observable} from 'rxjs';
import icClose from '@iconify/icons-ic/twotone-close';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPerson from '@iconify/icons-ic/twotone-person';
import icAssignment from '@iconify/icons-ic/assignment';
import icAccessible_forward from '@iconify/icons-ic/accessibility';
import icNotesMedical from '@iconify/icons-fa-solid/notes-medical';
import icBlind from '@iconify/icons-fa-solid/blind';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';

import {SimpleCatalogUtil} from 'src/app/core/utils/simple-catalog-util';
import {CatalogoBannerService} from 'src/app/core/services/catalogo-banner.service';
import {DiscapacidadService} from 'src/app/modules/medicina-general/antecedente-clinico/services/discapacidad.service';
import {LoadingService} from 'src/app/core/services/loading.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {AntecedentePersonalSharedService} from 'src/app/modules/medicina-general/antecedente-clinico/services/antecedente-personal-shared.service';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {TipoEnfermedadCIE10} from 'src/app/core/models/catalogo/tipo-enfermedad-cie10.model';
import {UntilDestroy} from '@ngneat/until-destroy';
import {enumSelector} from 'src/app/core/utils/enum-to-string-util';
import {TipoColaboradorDiscapacidadEnum} from 'src/app/core/enums/tipo-colaborador-discapacidad.model';
import {TipoDiscapacidad} from 'src/app/core/models/catalogo/tipo-discapacidad.model';

@UntilDestroy()
@Component({
  selector: 'vex-discapacidad-modal',
  templateUrl: './discapacidad-modal.component.html',
  styleUrls: ['./discapacidad-modal.component.scss']
})
export class DiscapacidadModalComponent implements OnInit {

  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  tiposDiscapacidad: Observable<TipoDiscapacidad[]>;
  porcentajesDiscapacidad = SimpleCatalogUtil.rangoPorcentajeDiscapacidad;
  tiposColaboradorDiscapacidad = enumSelector(TipoColaboradorDiscapacidadEnum);
  filteredCodigosCie: Observable<TipoEnfermedadCIE10[]>;
  icClose = icClose;
  icDelete = icDelete;
  icNotesMedical = icNotesMedical;
  icPerson = icPerson;
  icBlind = icBlind;
  icAssignment = icAssignment;
  icArrowDropDown = icArrowDropDown;
  icAccessible_forward = icAccessible_forward;
  messages = USER_MESSAGES;
  errorMessages = FORM_ERROR_MESSAGES;

  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: Discapacidad,
    private dialogRef: MatDialogRef<DiscapacidadModalComponent>,
    private fb: FormBuilder,
    private catalogoBannerService: CatalogoBannerService,
    private catalogoService: CatalogoService,
    private discapacidadService: DiscapacidadService,
    private loadingService: LoadingService,
    private snackbar: MatSnackBar,
    private antecedenteSharedService: AntecedentePersonalSharedService,
  ) { }

  ngOnInit(): void {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as Discapacidad;
    }

    this.tiposDiscapacidad = this.catalogoService.getTiposDiscapacidad();
    this.form = this.fb.group({
      id: this.defaults.id || null,
      tipoColaborador: [this.defaults.tipoColaborador || '', Validators.required],
      idTipoDiscapacidad: [this.defaults.idTipoDiscapacidad || '', Validators.required],
      porcentajeDiscapacidad: [this.defaults.porcentajeDiscapacidad || '', Validators.required],
      diagnostico: [this.defaults.diagnostico || '', Validators.required],
      gradoDiscapacidad: [{value: this.defaults.gradoDiscapacidad || '', disabled: true}, Validators.required],
      idAntecedentePersonal: [null, Validators.required]
    });

    this.antecedenteSharedService.antecedentePersonal$
      .subscribe((antecedente) => {
        if (antecedente) {
          this.form.patchValue({idAntecedentePersonal: antecedente.id});
        }
      });
  }

  save() {
    const discapacidad = this.form.getRawValue();
    if (this.mode === 'create') {
      this.createDiscapacidad(discapacidad);
    } else if (this.mode === 'update') {
      this.updateDiscapacidad(discapacidad);
    }
  }

  createDiscapacidad(discapacidad: Discapacidad): void {
    this.loadingService.showLoaderUntilCompleted(
      this.discapacidadService.createDiscapacidad(discapacidad)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.createdFailedMessage, 'CERRAR');
    });
  }

  updateDiscapacidad(discapacidad: Discapacidad): void {
    this.loadingService.showLoaderUntilCompleted(
      this.discapacidadService.updateDiscapacidad(discapacidad.id, discapacidad)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.updatedFailedMessage, 'CERRAR');
    });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  setGradoDiscapacidad(porcentaje: any){
    const porcentajeDiscapacidad = this.porcentajesDiscapacidad
      .find(x => x.porcentaje === porcentaje.value);
    this.form.get('gradoDiscapacidad').setValue(porcentajeDiscapacidad.grado);
  }

  searchCie(value: any) {
    if (value !== '' && typeof value === 'string' && value.trim().length > 1) {
      this.filteredCodigosCie = this.catalogoService.getCodigosCIE10ByQuery(value);
    }
  }

  displayFn(cie: TipoEnfermedadCIE10): string {
    return cie ? `${cie.codigo} -- ${cie.nombre}` : '';
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }
}
