import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DiscapacidadComponent } from './discapacidad.component';
import {PageLayoutModule} from 'src/@vex/components/page-layout/page-layout.module';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatIconModule} from '@angular/material/icon';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {IconModule} from '@visurel/iconify-angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatTooltipModule} from '@angular/material/tooltip';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatRippleModule} from '@angular/material/core';
import {MatInputModule} from '@angular/material/input';
import { DiscapacidadModalComponent } from './discapacidad-modal/discapacidad-modal.component';
import {MatSliderModule} from '@angular/material/slider';
import {MatRadioModule} from '@angular/material/radio';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';


@NgModule({
  declarations: [DiscapacidadComponent, DiscapacidadModalComponent],
  imports: [
    CommonModule,
    PageLayoutModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatCheckboxModule,
    MatIconModule,
    FlexLayoutModule,
    MatButtonModule,
    MatMenuModule,
    IconModule,
    FormsModule,
    MatTooltipModule,
    ReactiveFormsModule,
    ContainerModule,
    MatSelectModule,
    MatDialogModule,
    MatSidenavModule,
    MatButtonToggleModule,
    MatRippleModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSliderModule,
    MatRadioModule,
    MatAutocompleteModule,
    LoadingModule,
  ],
  exports: [
    DiscapacidadComponent
  ],
})
export class DiscapacidadModule { }
