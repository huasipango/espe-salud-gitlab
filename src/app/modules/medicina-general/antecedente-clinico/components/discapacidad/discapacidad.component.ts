import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {Discapacidad} from '../../models/discapacidad.model';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {DiscapacidadService} from '../../services/discapacidad.service';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {DiscapacidadModalComponent} from './discapacidad-modal/discapacidad-modal.component';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {enumSelector} from 'src/app/core/utils/enum-to-string-util';
import {TipoColaboradorDiscapacidadEnum} from 'src/app/core/enums/tipo-colaborador-discapacidad.model';
import {TipoDiscapacidad} from 'src/app/core/models/catalogo/tipo-discapacidad.model';

@Component({
  selector: 'vex-discapacidad',
  templateUrl: './discapacidad.component.html',
  styleUrls: ['./discapacidad.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms,
    scaleFadeIn400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class DiscapacidadComponent implements OnInit, AfterViewInit, OnDestroy {

  discapacidades: Discapacidad[];

  @Input()
  columns: TableColumn<Discapacidad>[] = [
    {label: 'Tipo de colaborador', property: 'tipoColaborador', type: 'object', visible: true},
    {label: 'Tipo de discapacidad', property: 'tipoDiscapacidad', type: 'object', visible: true},
    {label: 'Porcentaje', property: 'porcentajeDiscapacidad', type: 'text', visible: true},
    {label: 'Grado', property: 'gradoDiscapacidad', type: 'text', visible: true},
    {label: 'Diagnóstico', property: 'diagnostico', type: 'text', visible: true},
    {label: 'Acciones', property: 'menu', type: 'button', visible: true}
  ];

  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<Discapacidad> | null;

  icEdit = icEdit;
  icSearch = icSearch;
  icMoreHoriz = icMoreHoriz;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;

  pacienteActual: Paciente;
  messages = USER_MESSAGES;
  tiposColaboradorDiscapacidad = enumSelector(TipoColaboradorDiscapacidadEnum);


  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    protected discapacidadService: DiscapacidadService,
    private pacienteGlobalService: PacienteGlobalService
  ) {
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getDiscapacidades();
        }
      });
  }

  getDiscapacidades(): void {
    this.discapacidadService
      .getDiscapacidades(this.pacienteActual.id)
      .subscribe(
        (data: Discapacidad[]) => {
          this.discapacidades = data;
          this.dataSource.data = this.discapacidades;
        }
      );
  }

  openDeleteDialog(discapacidades: Discapacidad) {
    this.dialog.open(DeleteModalComponent, {
      data: this.messages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') { this.deleteDiscapacidad(discapacidades); }
    });
  }

  deleteDiscapacidad(discapacidad: Discapacidad) {
    this.discapacidadService.deleteDiscapacidad(discapacidad.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification(this.messages.deletedSuccessMessage, 'CERRAR');
          this.discapacidades.splice(
            this.discapacidades.findIndex((existing) =>
              existing.id === discapacidad.id), 1
          );
          this.dataSource.connect().next(this.discapacidades);
        } else {
          this.showNotification(this.messages.deleteFailMessage, 'CERRAR');
        }
      });
  }

  createDiscapacidad() {
    this.dialog.open(DiscapacidadModalComponent, {
        width: '550px',
        maxWidth: '100%'
      }).afterClosed()
      .subscribe((discapacidad: Discapacidad) => {
        if (discapacidad) {
          this.showNotification(this.messages.createdSuccessMessage, 'OK');
          this.getDiscapacidades();
        }
      });
  }

  updateDiscapacidad(discapacidad: Discapacidad) {
    this.dialog.open(DiscapacidadModalComponent, {
        data: discapacidad,
        width: '550px',
        maxWidth: '100%'
      }).afterClosed()
      .subscribe(updatedDiscapacidad => {
        if (updatedDiscapacidad) {
          this.showNotification(this.messages.updatedSuccessMessage, 'OK');
          this.getDiscapacidades();
        }
      });
  }

  get visibleColumns() {
    return this.columns
      .filter(column => column.visible)
      .map(column => column.property);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(column: TableColumn<T>) {
    return column.property;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  getTipoColaborador(tipoColaborador: string): string {
    return tipoColaborador ? this.tiposColaboradorDiscapacidad.find(t => t.title === tipoColaborador).value : '';
  }


  getTipoDiscapacidad(tipoDiscapacidad: TipoDiscapacidad): string {
    return tipoDiscapacidad ? tipoDiscapacidad.nombre : '';
  }

  ngOnDestroy() {
  }
}
