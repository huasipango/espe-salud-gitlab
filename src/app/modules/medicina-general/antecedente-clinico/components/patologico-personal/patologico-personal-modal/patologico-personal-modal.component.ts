import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import icClose from '@iconify/icons-ic/twotone-close';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPerson from '@iconify/icons-ic/twotone-person';
import icCheck from '@iconify/icons-ic/check';
import icNotesMedical from '@iconify/icons-fa-solid/notes-medical';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {AntecedentePatologicoPersonal} from '../../../models/antecedente-patologico-personal.model';
import {SimpleCatalogUtil} from 'src/app/core/utils/simple-catalog-util';
import {Observable} from 'rxjs';
import {TipoEnfermedadPersonal} from 'src/app/core/models/catalogo/tipo-enfermedad-personal.model';
import {TipoEnfermedadCIE10} from 'src/app/core/models/catalogo/tipo-enfermedad-cie10.model';
import {debounceTime, distinctUntilChanged, startWith} from 'rxjs/operators';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {LoadingService} from 'src/app/core/services/loading.service';
import {PatologicoPersonalService} from 'src/app/modules/medicina-general/antecedente-clinico/services/patologico-personal.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {AntecedentePersonalSharedService} from 'src/app/modules/medicina-general/antecedente-clinico/services/antecedente-personal-shared.service';
import {ThemePalette} from '@angular/material/core';
import {forbiddenObjectValidator} from 'src/app/shared/validators/forbbiden-object-validator';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';

@UntilDestroy()
@Component({
  selector: 'vex-patologico-personal-modal',
  templateUrl: './patologico-personal-modal.component.html',
  styleUrls: ['./patologico-personal-modal.component.scss']
})
export class PatologicoPersonalModalComponent implements OnInit {
  public color: ThemePalette = 'primary';
  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  frecuenciaMedicacion = SimpleCatalogUtil.frecuenciaMedicacionCatalogo;
  tiposEnfermedadPersonal: Observable<TipoEnfermedadPersonal[]>;

  diagnosticoCtrl: FormControl;
  filteredCodigosCie: Observable<TipoEnfermedadCIE10[]>;

  icClose = icClose;
  icDelete = icDelete;
  icCheck = icCheck;
  icPerson = icPerson;
  icNotesMedical = icNotesMedical;
  icArrowDropDown = icArrowDropDown;
  maxDate = new Date();
  errorMessages = FORM_ERROR_MESSAGES;
  constructor(
    private dialogRef: MatDialogRef<PatologicoPersonalModalComponent>,
    @Inject(MAT_DIALOG_DATA) public defaults: AntecedentePatologicoPersonal,
    private fb: FormBuilder,
    private catalogoService: CatalogoService,
    private loadingService: LoadingService,
    private patologicoPersonalService: PatologicoPersonalService,
    private snackbar: MatSnackBar,
    private antecedenteSharedService: AntecedentePersonalSharedService
  ) {
  }

  ngOnInit(): void {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as AntecedentePatologicoPersonal;
    }

    this.diagnosticoCtrl = new FormControl(
      this.defaults.idDiagnostico, [Validators.required, forbiddenObjectValidator]
    );

    if (this.defaults.idDiagnostico) {
      this.catalogoService.getCodigoCIE10(this.defaults.idDiagnostico)
        .subscribe((codigo) => {
          if (codigo) {
            this.diagnosticoCtrl.patchValue(codigo);
          }
        });
    }

    this.diagnosticoCtrl.valueChanges
      .pipe(
        startWith(''),
        debounceTime(400),
        distinctUntilChanged(),
        untilDestroyed(this)
      ).subscribe(value => this.searchCie(value || ''));

    this.tiposEnfermedadPersonal = this.catalogoService.getTiposEnfermedadPersonal();
    this.form = this.fb.group({
      id: this.defaults.id || null,
      idTipoEnfermedadPersonal: [this.defaults.idTipoEnfermedadPersonal || '', Validators.required],
      fechaDiagnostico: [this.defaults.fechaDiagnostico || '', Validators.required],
      frecuenciaMedicacion: [this.defaults.frecuenciaMedicacion || '', Validators.required],
      idDiagnostico: this.diagnosticoCtrl,
      observaciones: [this.defaults.observaciones || ''],
      esEnfermedadCatastrofica: [this.defaults.esEnfermedadCatastrofica || null],
      idAntecedentePersonal: [null, Validators.required]
    });
    this.antecedenteSharedService.antecedentePersonal$
      .subscribe((antecedente) => {
        if (antecedente) {
          this.form.patchValue({idAntecedentePersonal: antecedente.id});
        }
      });
  }

  save() {
    const antecedente = this.form.value;
    const cie10 = antecedente.idDiagnostico;
    antecedente.idDiagnostico = cie10.codigo;
    if (this.mode === 'create') {
      this.createAntecedente(antecedente);
    } else if (this.mode === 'update') {
      this.updateAntecedente(antecedente);
    }
  }

  createAntecedente(antecedente: AntecedentePatologicoPersonal): void {
    this.loadingService.showLoaderUntilCompleted(
      this.patologicoPersonalService.createAntecedentePatologicoPersonal(antecedente)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification('No se pudo crear el registro', 'CERRAR');
    });
  }

  updateAntecedente(antecedente: AntecedentePatologicoPersonal): void {
    this.loadingService.showLoaderUntilCompleted(
      this.patologicoPersonalService.updateAntecedentePatologicoPersonal(antecedente.id, antecedente)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification('No se pudo actualizar el registro', 'CERRAR');
    });
  }

  searchCie(value: any) {
    if (value !== '' && typeof value === 'string' && value.trim().length > 1) {
      this.filteredCodigosCie = this.catalogoService.getCodigosCIE10ByQuery(value);
    }
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  displayFn(cie: TipoEnfermedadCIE10): string {
    return cie ? `${cie.codigo} -- ${cie.nombre}` : '';
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }
}
