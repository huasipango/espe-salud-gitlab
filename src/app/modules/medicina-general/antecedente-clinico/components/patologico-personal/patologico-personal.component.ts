import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {AntecedentePatologicoPersonal} from '../../models/antecedente-patologico-personal.model';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {PatologicoPersonalService} from '../../services/patologico-personal.service';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {PatologicoPersonalModalComponent} from './patologico-personal-modal/patologico-personal-modal.component';
import {DateUtil} from 'src/app/core/utils/date-utils';
import {TipoEnfermedadCIE10} from 'src/app/core/models/catalogo/tipo-enfermedad-cie10.model';
import {TipoEnfermedadPersonal} from 'src/app/core/models/catalogo/tipo-enfermedad-personal.model';

@Component({
  selector: 'vex-patologico-personal',
  templateUrl: './patologico-personal.component.html',
  styleUrls: ['./patologico-personal.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms,
    scaleFadeIn400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class PatologicoPersonalComponent implements OnInit, AfterViewInit {

  patologicosPersonales: AntecedentePatologicoPersonal[];

  @Input()
  columns: TableColumn<AntecedentePatologicoPersonal>[] = [
    { label: 'Tipo de enfermedad personal', property: 'tipoEnfermedadPersonal', type: 'object', visible: true },
    { label: 'Fecha de diagnóstico', property: 'fechaDiagnostico', type: 'date', visible: true },
    { label: 'Toma Medicación', property: 'frecuenciaMedicacion', type: 'text', visible: true },
    { label: 'Diagnóstico', property: 'diagnostico', type: 'object', visible: true },
    { label: 'Observaciones', property: 'observaciones', type: 'text', visible: true, cssClasses: ['text-wrap'] },
    { label: 'Enf. catastrófica', property: 'esEnfermedadCatastrofica', type: 'checkbox', visible: true, cssClasses: ['text-wrap'] },
    { label: 'Acciones', property: 'menu', type: 'button', visible: true }
  ];

  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<AntecedentePatologicoPersonal> | null;

  icEdit = icEdit;
  icSearch = icSearch;
  icMoreHoriz = icMoreHoriz;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;

  pacienteActual: Paciente;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    protected patologicoPersonalService: PatologicoPersonalService,
    private pacienteGlobalService: PacienteGlobalService
  ) { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente){
          this.pacienteActual = paciente;
          this.getAntecedentes();
        }
      });
  }

  getAntecedentes(): void{
    this.patologicoPersonalService
      .getAntecedentesPatologicoPersonales(this.pacienteActual.id)
      .subscribe(
        (data: AntecedentePatologicoPersonal[]) => {
          if (data) {
            this.patologicosPersonales = data;
            this.dataSource.data = this.patologicosPersonales;
          }
        }
      );
  }

  createPatologicoPersonal(){
    this.dialog.open(PatologicoPersonalModalComponent, {
        width: '600px',
        maxWidth: '100%'
      }).afterClosed()
      .subscribe((antecedente: AntecedentePatologicoPersonal) => {
        if (antecedente) {
          this.showNotification('Registro creado EXITOSAMENTE', 'OK');
          this.getAntecedentes();
        }
      });
  }

  updatePatologicoPersonal(antecedente: AntecedentePatologicoPersonal) {
    this.dialog.open(PatologicoPersonalModalComponent, {
        data: antecedente,
        width: '600px',
        maxWidth: '100%'
      }).afterClosed()
      .subscribe(updatedAntecedente => {
        if (updatedAntecedente) {
          this.showNotification('Registro actualizado EXITOSAMENTE', 'OK');
          this.getAntecedentes();
        }
      });
  }

  openDeleteDialog(antecedente: AntecedentePatologicoPersonal) {
    this.dialog.open(DeleteModalComponent, {
      data: '¿Estas seguro de eliminar este registro?',
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') { this.deletePatologicoPersonal(antecedente); }
    });
  }

  deletePatologicoPersonal(antecedente: AntecedentePatologicoPersonal) {
    this.patologicoPersonalService.deleteAntecedentePatologicoPersonal(antecedente.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification('Registro eliminado EXITOSAMENTE', 'CERRAR');
          this.patologicosPersonales.splice(
            this.patologicosPersonales.findIndex((existing) =>
              existing.id === antecedente.id), 1
          );
          this.dataSource.connect().next(this.patologicosPersonales);
        } else {
          this.showNotification('No se pudo eliminar el registro', 'CERRAR');
        }
      });
  }

  get visibleColumns() {
    return this.columns
      .filter(column => column.visible)
      .map(column => column.property);
  }

  getFormattedDate(date: string){
    return DateUtil.showDateFormat(date);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(column: TableColumn<T>) {
    return column.property;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  getDiagnostico(diagnostico: TipoEnfermedadCIE10): string {
    return diagnostico ? diagnostico.codigo : '';
  }

  getTipoEnfermedadPersonal(tipoEnfermedad: TipoEnfermedadPersonal): string {
    return tipoEnfermedad ? tipoEnfermedad.nombre : '';
  }
}
