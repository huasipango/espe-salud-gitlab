import {Component, Inject, OnInit} from '@angular/core';
import icClose from '@iconify/icons-ic/twotone-close';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icPerson from '@iconify/icons-ic/twotone-person';
import icCheck from '@iconify/icons-ic/check';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AntecedenteGinecologico} from '../../../models/antecedente-ginecologico.model';
import circlenotch from '@iconify/icons-fa-solid/circle-notch';
import heart from '@iconify/icons-fa-solid/heart';
import baby from '@iconify/icons-fa-solid/baby';
import heartbroken from '@iconify/icons-fa-solid/heart-broken';
import bookmedical from '@iconify/icons-fa-solid/book-medical';
import clinicmedical from '@iconify/icons-fa-solid/clinic-medical';
import calendar from '@iconify/icons-fa-solid/calendar-day';
import {SimpleCatalogUtil} from 'src/app/core/utils/simple-catalog-util';
import icMale from '@iconify/icons-fa-solid/male';
import {AntecedentePersonalSharedService} from 'src/app/modules/medicina-general/antecedente-clinico/services/antecedente-personal-shared.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {LoadingService} from 'src/app/core/services/loading.service';
import {AntecedenteGinecologicoService} from 'src/app/modules/medicina-general/antecedente-clinico/services/antecedente-ginecologico.service';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';

@Component({
  selector: 'vex-antecedente-ginecologico-modal',
  templateUrl: './antecedente-ginecologico-modal.component.html',
  styleUrls: ['./antecedente-ginecologico-modal.component.scss']
})
export class AntecedenteGinecologicoModalComponent implements OnInit {

  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  icClose = icClose;
  icDelete = icDelete;
  icPerson = icPerson;
  icCheck = icCheck;
  icMale = icMale;
  circlenotch = circlenotch;
  heartbroken = heartbroken;
  bookmedical = bookmedical;
  clinicmedical = clinicmedical;
  baby = baby;
  calendar = calendar;
  icPhone = icPhone;
  heart = heart;
  maxDate = new Date();

  ciclos = SimpleCatalogUtil.ciclosMenstruacion;
  messages = USER_MESSAGES;
  errorMessages = FORM_ERROR_MESSAGES;

  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: AntecedenteGinecologico,
    private dialogRef: MatDialogRef<AntecedenteGinecologicoModalComponent>,
    private fb: FormBuilder,
    private snackbar: MatSnackBar,
    private antecedenteSharedService: AntecedentePersonalSharedService,
    private loadingService: LoadingService,
    private antecedenteGinecologicoService: AntecedenteGinecologicoService
  ) {
  }

  ngOnInit() {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as AntecedenteGinecologico;
    }

    this.form = this.fb.group({
      id: this.defaults.id || '',
      cicloMenstruacion: [this.defaults.cicloMenstruacion || '', Validators.required],
      edadMenarquia: [this.defaults.edadMenarquia || null, [
        Validators.min(5), Validators.max(25),
        Validators.pattern('[0-9]*\\.?[0-9]{1}')
      ]],
      fechaUltimaMenstruacion: [this.defaults.fechaUltimaMenstruacion, Validators.required],
      gestas: [this.defaults.gestas || 0],
      fecha: [this.defaults.fecha || new Date()],
      numeroAbortos: [this.defaults.numeroAbortos || 0],
      numeroCesareas: [this.defaults.numeroCesareas || 0],
      numeroPartosVaginales: [this.defaults.numeroPartosVaginales || 0],
      hijosVivos: [this.defaults.hijosVivos || 0],
      hijosMuertos: [this.defaults.hijosMuertos || 0],
      idAntecedentePersonal: [null, Validators.required]
    });

    this.antecedenteSharedService.antecedentePersonal$
      .subscribe((antecedente) => {
        if (antecedente) {
          this.form.patchValue({idAntecedentePersonal: antecedente.id});
        }
      });
  }

  save() {
    const antecedente = this.form.value;
    if (this.mode === 'create') {
      this.createAntecedente(antecedente);
    } else if (this.mode === 'update') {
      this.updateAntecedente(antecedente);
    }
  }

  createAntecedente(antecedente: AntecedenteGinecologico): void {
    this.loadingService.showLoaderUntilCompleted(
      this.antecedenteGinecologicoService.createAntecedenteGinecologico(antecedente)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.createdFailedMessage, 'CERRAR');
    });
  }

  updateAntecedente(antecedente: AntecedenteGinecologico): void {
    this.loadingService.showLoaderUntilCompleted(
      this.antecedenteGinecologicoService.updateAntecedenteGinecologico(antecedente.id, antecedente)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.updatedFailedMessage, 'CERRAR');
    });
  }


  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }
}
