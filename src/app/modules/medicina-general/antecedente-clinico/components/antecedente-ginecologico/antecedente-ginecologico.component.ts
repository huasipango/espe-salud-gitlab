import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AntecedenteGinecologico} from '../../models/antecedente-ginecologico.model';
import {AntecedenteGinecologicoService} from '../../services/antecedente-ginecologico.service';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icFolder from '@iconify/icons-ic/twotone-folder';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {AntecedenteGinecologicoModalComponent} from './antecedente-ginecologico-modal/antecedente-ginecologico-modal.component';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';

@Component({
  selector: 'vex-antecedente-ginecologico',
  templateUrl: './antecedente-ginecologico.component.html',
  styleUrls: ['./antecedente-ginecologico.component.scss'],
  animations: [fadeInUp400ms, stagger40ms, scaleFadeIn400ms],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class AntecedenteGinecologicoComponent implements OnInit, AfterViewInit, OnDestroy {

  antecedentes: AntecedenteGinecologico[];
  @Input()
  columns: TableColumn<AntecedenteGinecologico>[] = [
    {
      label: 'Fecha registro',
      property: 'fecha',
      type: 'text',
      visible: true
    },
    {
      label: 'Edad de Menarquia',
      property: 'edadMenarquia',
      type: 'text',
      visible: true
    },
    {
      label: 'Ciclos',
      property: 'cicloMenstruacion',
      type: 'text',
      visible: true
    },
    {
      label: 'Fecha última menstruación',
      property: 'fechaUltimaMenstruacion',
      type: 'text',
      visible: true
    },
    {
      label: 'Gestas',
      property: 'gestas',
      type: 'text',
      visible: true
    },
    {
      label: 'Partos vaginales',
      property: 'numeroPartosVaginales',
      type: 'text',
      visible: true
    },
    {
      label: 'Cesáreas',
      property: 'numeroCesareas',
      type: 'text',
      visible: true
    },
    {
      label: 'Abortos',
      property: 'numeroAbortos',
      type: 'text',
      visible: false
    },
    {
      label: 'Hijos vivos',
      property: 'hijosVivos',
      type: 'text',
      visible: false
    },
    {
      label: 'Hijos muertos',
      property: 'hijosMuertos',
      type: 'text',
      visible: false
    },
    {
      label: 'Acciones',
      property: 'menu',
      type: 'button',
      visible: true
    },
  ];
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<AntecedenteGinecologico> | null;

  icEdit = icEdit;
  icSearch = icSearch;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icMoreHoriz = icMoreHoriz;
  icFolder = icFolder;
  pacienteActual: Paciente;
  messages = USER_MESSAGES;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    protected antecedenteGinecologicoService: AntecedenteGinecologicoService,
    private pacienteGlobalService: PacienteGlobalService
  ) {
  }

  get visibleColumns() {
    return this.columns
      .filter(column => column.visible)
      .map(column => column.property);
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getAntecedentes();
        }
      });
  }

  getAntecedentes(): void {
    this.antecedenteGinecologicoService.getAntecedentesGinecologicos(this.pacienteActual.id)
      .subscribe(
        (data: AntecedenteGinecologico[]) => {
          this.antecedentes = data;
          this.dataSource.data = data;
        }
      );
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  createAntecedenteGinecologico() {
    this.dialog.open(AntecedenteGinecologicoModalComponent, {
        width: '750px',
        maxWidth: '100%'
      }).afterClosed()
      .subscribe((antecedente: AntecedenteGinecologico) => {
        if (antecedente) {
          this.showNotification(this.messages.createdSuccessMessage, 'OK');
          this.getAntecedentes();
        }
      });
  }

  updateAntecedenteGinecologico(antecedente: AntecedenteGinecologico) {
    this.dialog.open(AntecedenteGinecologicoModalComponent, {
        data: antecedente,
        width: '750px',
        maxWidth: '100%'
      })
      .afterClosed()
      .subscribe(updatedAntecedente => {
        if (updatedAntecedente) {
          this.showNotification(this.messages.updatedSuccessMessage, 'OK');
          this.getAntecedentes();
        }
      });
  }

  openDeleteDialog(antecedente: AntecedenteGinecologico) {
    this.dialog.open(DeleteModalComponent, {
      data: this.messages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') {
        this.deleteAntecedenteGinecologico(antecedente);
      }
    });
  }

  deleteAntecedenteGinecologico(antecedente: AntecedenteGinecologico) {
    this.antecedenteGinecologicoService.deleteAntecedenteGinecologico(antecedente.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification(this.messages.deletedSuccessMessage, 'CERRAR');
          this.antecedentes.splice(
            this.antecedentes.findIndex((existing) =>
              existing.id === antecedente.id), 1
          );
          this.dataSource.connect().next(this.antecedentes);
        } else {
          this.showNotification(this.messages.deleteFailMessage, 'CERRAR');
        }
      });
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  trackByProperty<T>(column: TableColumn<T>) {
    return column.property;
  }

  ngOnDestroy() {
  }

}
