import {Component, OnInit} from '@angular/core';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-fa-solid/plus';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {ActivatedRoute, Router} from '@angular/router';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';

@Component({
  selector: 'vex-antecedente-empty',
  templateUrl: './antecedente-empty.component.html',
  styleUrls: ['./antecedente-empty.component.scss'],
  animations: [
    scaleFadeIn400ms
  ]
})
export class AntecedenteEmptyComponent implements OnInit {
  icSearch = icSearch;
  icAdd = icAdd;

  pacienteActual: Paciente;

  constructor(
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {

  }

  createAntecedente(): void {
    this.router.navigate(['./registro-antecedente'], {relativeTo: this.route})
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

}
