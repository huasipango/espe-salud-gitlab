import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AntecedenteEmptyComponent } from './antecedente-empty.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {IconModule} from '@visurel/iconify-angular';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';

@NgModule({
  declarations: [AntecedenteEmptyComponent],
  exports: [
    AntecedenteEmptyComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatButtonModule,
    IconModule,
    MatIconModule
  ]
})
export class AntecedenteEmptyModule { }
