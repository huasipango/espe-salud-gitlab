import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import icClose from '@iconify/icons-ic/twotone-close';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icPerson from '@iconify/icons-ic/twotone-person';
import icCheck from '@iconify/icons-ic/check';
import running from '@iconify/icons-fa-solid/running';
import calendar from '@iconify/icons-fa-solid/calendar-day';
import stopwatch from '@iconify/icons-fa-solid/stopwatch';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { ActividadFisica} from '../../../models/actividad-fisica.model';
import {SimpleCatalogUtil} from 'src/app/core/utils/simple-catalog-util';
import {AntecedentePersonalSharedService} from 'src/app/modules/medicina-general/antecedente-clinico/services/antecedente-personal-shared.service';
import {LoadingService} from 'src/app/core/services/loading.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActividadFisicaService} from 'src/app/modules/medicina-general/antecedente-clinico/services/actividad-fisica.service';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';

@Component({
  selector: 'vex-actividad-fisica-modal',
  templateUrl: './actividad-fisica-modal.component.html',
  styleUrls: ['./actividad-fisica-modal.component.scss']
})
export class ActividadFisicaModalComponent implements OnInit {

  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  listChoise = SimpleCatalogUtil.frecuenciaTiempo;

  icClose = icClose;
  icDelete = icDelete;
  icPerson = icPerson;
  icCheck = icCheck;
  running = running;
  calendar = calendar;
  stopwatch = stopwatch;
  icPhone = icPhone;
  errorMessages = FORM_ERROR_MESSAGES;
  messages = USER_MESSAGES;
  constructor(
    private dialogRef: MatDialogRef<ActividadFisicaModalComponent>,
    @Inject(MAT_DIALOG_DATA) public defaults: ActividadFisica,
    private fb: FormBuilder,
    private antecedenteSharedService: AntecedentePersonalSharedService,
    private loadingService: LoadingService,
    private snackbar: MatSnackBar,
    private actividadFisicaService: ActividadFisicaService
  ) {
  }

  ngOnInit(): void {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as ActividadFisica;
    }
    this.form = this.fb.group({
      id: this.defaults.id || null,
      nombreActividad: [this.defaults.nombreActividad || '', Validators.required],
      frecuencia: [this.defaults.frecuencia || '', Validators.required],
      tiempoHoras: [this.defaults.tiempoHoras || '', Validators.required],
      observacion: [this.defaults.observacion || ''],
      idAntecedentePersonal: [null, Validators.required]
    });
    this.antecedenteSharedService.antecedentePersonal$
      .subscribe((antecedente) => {
        if (antecedente) {
          this.form.patchValue({idAntecedentePersonal: antecedente.id});
        }
      });
  }
  save() {
    const actividad = this.form.value;
    if (this.mode === 'create') {
      this.createActividad(actividad);
    } else if (this.mode === 'update') {
      this.updateActividad(actividad);
    }
  }

  createActividad(actividad: ActividadFisica) {
    this.loadingService.showLoaderUntilCompleted(
      this.actividadFisicaService.createActividadFisica(actividad)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(actividad);
      }
    }, () => {
      this.showNotification(this.messages.createdFailedMessage, 'CERRAR');
    });
  }

  updateActividad(actividad: ActividadFisica) {
    this.loadingService.showLoaderUntilCompleted(
      this.actividadFisicaService.updateActividadFisica(actividad.id, actividad)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.updatedFailedMessage, 'CERRAR');
    });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

}
