import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatTableDataSource} from '@angular/material/table';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {ActividadFisica} from '../../models/actividad-fisica.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {ActividadFisicaService} from '../../services/actividad-fisica.service';
import {ActividadFisicaModalComponent} from './actividad-fisica-modal/actividad-fisica-modal.component';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icFolder from '@iconify/icons-ic/twotone-folder';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {UntilDestroy} from '@ngneat/until-destroy';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';

@UntilDestroy()
@Component({
  selector: 'vex-actividad-fisica',
  templateUrl: './actividad-fisica.component.html',
  styleUrls: ['./actividad-fisica.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms,
    scaleFadeIn400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class ActividadFisicaComponent implements OnInit, AfterViewInit, OnDestroy {

  actividades: ActividadFisica[];

  columns: TableColumn<ActividadFisica>[] = [
    {label: 'Nombre de actividad', property: 'nombreActividad', type: 'text', visible: true},
    {label: 'Frecuencia', property: 'frecuencia', type: 'text', visible: true},
    {label: 'Horas', property: 'tiempoHoras', type: 'text', visible: true},
    {label: 'Observación', property: 'observacion', type: 'text', visible: true, cssClasses: ['text-wrap']},
    {label: 'Acciones', property: 'menu', type: 'button', visible: true}
  ];

  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<ActividadFisica> | null;

  icEdit = icEdit;
  icSearch = icSearch;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icMoreHoriz = icMoreHoriz;
  icFolder = icFolder;

  pacienteActual: Paciente;
  messages = USER_MESSAGES;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    protected actividadFisicaService: ActividadFisicaService,
    private pacienteGlobalService: PacienteGlobalService
  ) {
  }

  get visibleColumns() {
    return this.columns
      .filter(column => column.visible)
      .map(column => column.property);
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getActividadesFisicas();
        }
      });
  }

  getActividadesFisicas(): void {
    this.actividadFisicaService.getActividadesFisicas(this.pacienteActual.id)
      .subscribe(
        (data: ActividadFisica[]) => {
          this.actividades = data;
          this.dataSource.data = data;
        }
      );
  }

  createActividadFisica() {
    this.dialog.open(ActividadFisicaModalComponent, {
        width: '550px',
        maxWidth: '100%'
      }).afterClosed()
      .subscribe((actividad: ActividadFisica) => {
        if (actividad) {
          this.showNotification(this.messages.createdSuccessMessage, 'OK');
          this.getActividadesFisicas();
        }
      });
  }

  updateActividadFisica(actividadFisica: ActividadFisica) {
    this.dialog.open(ActividadFisicaModalComponent, {
        data: actividadFisica,
        width: '550px',
        maxWidth: '100%'
      }).afterClosed()
      .subscribe(updatedActividad => {
        if (updatedActividad) {
          this.showNotification(this.messages.updatedSuccessMessage, 'OK');
          this.getActividadesFisicas();
        }
      });
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  trackByProperty<T>(column: TableColumn<T>) {
    return column.property;
  }

  openDeleteDialog(actividad: ActividadFisica) {
    this.dialog.open(DeleteModalComponent, {
      data: this.messages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') { this.deleteActividadFisica(actividad); }
    });
  }

  deleteActividadFisica(actividad: ActividadFisica) {
    this.actividadFisicaService.deleteActividadFisica(actividad.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification(this.messages.deletedSuccessMessage, 'CERRAR');
          this.actividades.splice(
            this.actividades.findIndex((existing) =>
              existing.id === actividad.id), 1
          );
          this.dataSource.connect().next(this.actividades);
        } else {
          this.showNotification(this.messages.deleteFailMessage, 'CERRAR');
        }
      });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy() {
  }
}

