import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {LoadingService} from 'src/app/core/services/loading.service';
import {AntecedentePersonalService} from 'src/app/modules/medicina-general/antecedente-clinico/services/antecedente-personal.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import icClose from '@iconify/icons-ic/twotone-close';
import {PeriodoLactancia} from 'src/app/modules/medicina-general/antecedente-clinico/models/periodo-lactancia-model';

@Component({
  selector: 'vex-inicio-lactancia-modal',
  templateUrl: './inicio-lactancia-modal.component.html',
  styleUrls: ['./inicio-lactancia-modal.component.scss']
})
export class InicioLactanciaModalComponent implements OnInit {

  maxDate = new Date();
  form: FormGroup;
  icClose = icClose;
  messages = USER_MESSAGES;
  errorMessages = FORM_ERROR_MESSAGES;

  constructor(
    @Inject(MAT_DIALOG_DATA) public idAntecedente: number,
    private dialogRef: MatDialogRef<InicioLactanciaModalComponent>,
    private fb: FormBuilder,
    private loadingService: LoadingService,
    private antecedentePersonalService: AntecedentePersonalService,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      fechaInicioLactancia: [new Date(), Validators.required],
      fechaFinLactancia: [null],
      observacion: [null]
    });
  }

  save() {
    const data: PeriodoLactancia = this.form.getRawValue();
    this.loadingService.showLoaderUntilCompleted(
      this.antecedentePersonalService.finalizePeriodoGestacion(this.idAntecedente, data)
    ).subscribe((response) => {
      if (response) {
        this.showNotification('La paciente ha finalizado su período de gestación');
        this.dialogRef.close(response);
      }
    }, error => {
      this.showNotification('No se pudo finalizar el período de gestación de la paciente');
    });
  }

  showNotification(message: string) {
    this.snackbar.open(message, 'CERRAR', {
      duration: 5000
    });
  }

}
