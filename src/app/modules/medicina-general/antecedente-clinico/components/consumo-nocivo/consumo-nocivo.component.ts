import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatSort} from '@angular/material/sort';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import {MatDialog} from '@angular/material/dialog';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import {SelectionModel} from '@angular/cdk/collections';
import icFolder from '@iconify/icons-ic/twotone-folder';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {ConsumoNocivoService} from '../../services/consumo-nocivo.service';
import {ConsumoNocivo} from '../../models/consumo-nocivo.model';
import {ConsumoNocivoModalComponent} from './consumo-nocivo-modal/consumo-nocivo-modal.component';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {UntilDestroy} from '@ngneat/until-destroy';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {TipoConsumoNocivo} from 'src/app/core/models/catalogo/tipo-consumo-nocivo.model';

@UntilDestroy()
@Component({
  selector: 'vex-consumo-nocivo',
  templateUrl: './consumo-nocivo.component.html',
  styleUrls: ['./consumo-nocivo.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms,
    scaleFadeIn400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class ConsumoNocivoComponent implements OnInit, AfterViewInit, OnDestroy {

  consumosNocivos: ConsumoNocivo[];

  @Input()
  columns: TableColumn<ConsumoNocivo>[] = [
    {label: 'Tipo consumidor', property: 'tipoConsumidor', type: 'text', visible: true},
    {label: 'Consumo nocivo', property: 'nombreConsumoNocivo', type: 'object', visible: true},
    // {label: 'Fecha inicio', property: 'fechaTentativaInicio', type: 'text', visible: true},
    {label: 'T. consumo (MESES)', property: 'tiempoConsumoMes', type: 'text', visible: true},
    {label: 'T. abstinencia (MESES)', property: 'tiempoAbstinenciaMes', type: 'text', visible: true},
    {label: 'Frecuencia', property: 'frecuenciaConsumo', type: 'text', visible: true},
    {label: 'Cantidad', property: 'cantidadConsumo', type: 'text', visible: true},
    {label: 'Observación', property: 'observacion', type: 'text', visible: false},
    {label: 'Actions', property: 'menu', type: 'button', visible: true}
  ];
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<ConsumoNocivo> | null;
  selection = new SelectionModel<ConsumoNocivo>(true, []);

  icEdit = icEdit;
  icSearch = icSearch;
  icMoreHoriz = icMoreHoriz;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icFolder = icFolder;
  pacienteActual: Paciente;
  messages = USER_MESSAGES;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    protected consumoNocivoService: ConsumoNocivoService,
    private pacienteGlobalService: PacienteGlobalService
  ) {
  }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente){
          this.pacienteActual = paciente;
          this.getConsumosNocivos();
        }
      });
  }

  getConsumosNocivos(): void{
    this.consumoNocivoService.getConsumosNocivos(this.pacienteActual.id)
      .subscribe(
        (data: ConsumoNocivo[]) => {
          this.consumosNocivos = data;
          this.dataSource.data = this.consumosNocivos;
        }
      );
  }

  openDeleteDialog(consumoNocivo: ConsumoNocivo) {
    this.dialog.open(DeleteModalComponent, {
      data: this.messages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') {
        this.deleteConsumoNocivo(consumoNocivo);
      }
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  deleteConsumoNocivo(consumo: ConsumoNocivo) {
    this.consumoNocivoService.deleteConsumoNocivo(consumo.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification(this.messages.deletedSuccessMessage, 'CERRAR');
          this.consumosNocivos.splice(
            this.consumosNocivos.findIndex((existing) =>
              existing.id === consumo.id), 1
          );
          this.dataSource.connect().next(this.consumosNocivos);
        } else {
          this.showNotification(this.messages.deleteFailMessage, 'CERRAR');
        }
      });
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(column: TableColumn<T>) {
    return column.property;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  ngOnDestroy() {
  }

  createConsumoNocivo() {
    this.dialog.open(ConsumoNocivoModalComponent, {
      width: '900px',
      maxWidth: '100%'
    }).afterClosed()
      .subscribe((consumo: ConsumoNocivo) => {
        if (consumo) {
          this.showNotification(this.messages.createdSuccessMessage, 'OK');
          this.getConsumosNocivos();
        }
      });
  }

  updateConsumoNocivo(consumo: ConsumoNocivo) {
    this.dialog.open(ConsumoNocivoModalComponent, {
      data: consumo,
      width: '900px',
      maxWidth: '100%'
    }).afterClosed()
      .subscribe(updated => {
        if (updated) {
          this.showNotification(this.messages.updatedSuccessMessage, 'OK');
          this.getConsumosNocivos();
        }
      });
  }

  getNombreConsumoNocivo(consumo: TipoConsumoNocivo): string{
    return consumo ? consumo.nombre : '';
  }
}


