import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConsumoNocivoComponent} from './consumo-nocivo.component';
import {ConsumoNocivoModalComponent} from './consumo-nocivo-modal/consumo-nocivo-modal.component';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {IconModule} from '@visurel/iconify-angular';
import {FlexLayoutModule} from '@angular/flex-layout';
import {PageLayoutModule} from 'src/@vex/components/page-layout/page-layout.module';
import {HttpClientModule} from '@angular/common/http';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {MatSortModule} from '@angular/material/sort';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatTooltipModule} from '@angular/material/tooltip';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDividerModule} from '@angular/material/divider';
import {MatNativeDateModule} from '@angular/material/core';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatRadioModule} from '@angular/material/radio';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import {MatMomentDateModule} from '@angular/material-moment-adapter';


@NgModule({
  declarations: [ConsumoNocivoComponent, ConsumoNocivoModalComponent],
  exports: [
    ConsumoNocivoComponent
  ],
  imports: [
    CommonModule,
    MatMenuModule,
    IconModule,
    MatIconModule,
    MatCheckboxModule,
    FormsModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    PageLayoutModule,
    HttpClientModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    MatTableModule,
    MatSortModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatTooltipModule,
    ContainerModule,
    MatSelectModule,
    MatButtonToggleModule,
    MatDialogModule,
    MatSnackBarModule,
    MatAutocompleteModule,
    MatDividerModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatRadioModule,
    LoadingModule,
    MatMomentDateModule
  ]
})
export class ConsumoNocivoModule {
}
