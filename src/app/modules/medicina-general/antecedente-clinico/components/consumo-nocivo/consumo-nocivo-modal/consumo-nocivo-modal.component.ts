import {ChangeDetectorRef, Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icClose from '@iconify/icons-ic/twotone-close';
import icPrint from '@iconify/icons-ic/twotone-print';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icPerson from '@iconify/icons-ic/twotone-person';
import icLocationCity from '@iconify/icons-ic/twotone-location-city';
import icFormatListNumbered from '@iconify/icons-ic/format-list-numbered';
import icCheck from '@iconify/icons-ic/check';
import icAccessTime from '@iconify/icons-ic/access-time';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ConsumoNocivo} from '../../../models/consumo-nocivo.model';
import {SimpleCatalogUtil} from 'src/app/core/utils/simple-catalog-util';
import {dropdownAnimation} from 'src/@vex/animations/dropdown.animation';
import {Observable} from 'rxjs';
import {TipoConsumoNocivo} from 'src/app/core/models/catalogo/tipo-consumo-nocivo.model';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {debounceTime, distinctUntilChanged, map, startWith, switchMap} from 'rxjs/operators';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';
import icCannabis from '@iconify/icons-fa-solid/cannabis';
import icChart from '@iconify/icons-fa-solid/chart-bar';
import {enumSelector} from 'src/app/core/utils/enum-to-string-util';
import {TiposConsumidorEnum} from 'src/app/core/enums/tipo-consumidor.enum';
import {MatSnackBar} from '@angular/material/snack-bar';
import {LoadingService} from 'src/app/core/services/loading.service';
import {calculateMonthsDiff} from 'src/app/core/utils/operations.util';
import {ConsumoNocivoService} from 'src/app/modules/medicina-general/antecedente-clinico/services/consumo-nocivo.service';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {AntecedentePersonalSharedService} from 'src/app/modules/medicina-general/antecedente-clinico/services/antecedente-personal-shared.service';

@Component({
  selector: 'vex-consumo-nocivo-modal',
  templateUrl: './consumo-nocivo-modal.component.html',
  styleUrls: ['./consumo-nocivo-modal.component.scss'],
  animations: [
    dropdownAnimation
  ],
  encapsulation: ViewEncapsulation.None
})
export class ConsumoNocivoModalComponent implements OnInit {

  form: FormGroup;
  mode: 'create' | 'update' = 'create';

  icMoreVert = icMoreVert;
  icClose = icClose;
  icCannabis = icCannabis;
  icPrint = icPrint;
  icDelete = icDelete;
  icChart = icChart;
  icPerson = icPerson;
  icLocationCity = icLocationCity;
  icPhone = icPhone;
  icArrowDropDown = icArrowDropDown;
  icFormatListNumbered = icFormatListNumbered;
  icCheck = icCheck;
  icAccessTime = icAccessTime;

  tiposConsumidor = enumSelector(TiposConsumidorEnum);
  listFrecuency = SimpleCatalogUtil.frecuenciaTiempo;

  descipcionB: boolean;
  consumidor = false;
  exConsumidor = false;

  filteredConsumosNocivos$: Observable<TipoConsumoNocivo[]>;
  nombreConsumoNocivo = new FormControl(
    (this.defaults ? (this.defaults.nombreConsumoNocivo ? this.defaults.nombreConsumoNocivo : null) : null),
    [Validators.required]
  );

  today = new Date();
  messages = USER_MESSAGES;
  errorMessages = FORM_ERROR_MESSAGES;

  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: any,
    private cd: ChangeDetectorRef,
    private dialogRef: MatDialogRef<ConsumoNocivoModalComponent>,
    private fb: FormBuilder,
    private catalogoService: CatalogoService,
    private snackbar: MatSnackBar,
    private loadingService: LoadingService,
    private consumoNocivoService: ConsumoNocivoService,
    private antecedenteSharedService: AntecedentePersonalSharedService,
  ) {
  }

  ngOnInit() {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as ConsumoNocivo;
    }

    this.filteredConsumosNocivos$ = this.catalogoService.getTiposConsumoNocivo();
    this.filteredConsumosNocivos$ = this.nombreConsumoNocivo.valueChanges
      .pipe(
        startWith(''),
        debounceTime(600),
        distinctUntilChanged(),
        switchMap(value => {
          return this.filterConsumoNocivo(value || '');
        })
      );

    this.form = this.fb.group({
      id: [this.defaults.id || null],
      tipoConsumidor: [this.defaults.tipoConsumidor || '', Validators.required],
      fechaTentativaInicio: [this.defaults.fechaTentativaInicio || null],
      fechaFinConsumo: [this.defaults.fechaFinConsumo || null],
      nombreConsumoNocivo: this.nombreConsumoNocivo,
      tiempoConsumoMes: {value: this.defaults.tiempoConsumoMes || null, disabled: true},
      frecuenciaConsumo: [this.defaults.frecuenciaConsumo || '', Validators.required],
      cantidadConsumo: [this.defaults.cantidadConsumo || '', Validators.required],
      tiempoAbstinenciaMes:
        {
          value: this.defaults.tiempoAbstinenciaMes || null,
          disabled: true
        },
      observacion: [this.defaults.observacion || ''],
      idAntecedentePersonal: [null, Validators.required]
    });


    this.antecedenteSharedService.antecedentePersonal$
      .subscribe((antecedente) => {
        if (antecedente) {
          this.form.patchValue({idAntecedentePersonal: antecedente.id});
        }
      });
  }

  filterConsumoNocivo(value: any): Observable<TipoConsumoNocivo[]> {
    value = typeof value === 'string' ? value : value.nombre;
    return this.catalogoService.getTiposConsumoNocivo()
      .pipe(
        map(response => response.filter(option => {
          return option.nombre.toLowerCase().indexOf(value.toLowerCase()) === 0;
        }))
      );
  }

  save() {
    const consumo = this.form.getRawValue();
    if (typeof consumo.nombreConsumoNocivo === 'string') {
      consumo.nombreConsumoNocivo = {
        id: null,
        nombre: consumo.nombreConsumoNocivo,
      };
    }
    if (this.mode === 'create') {
      this.createConsumoNocivo(consumo);
    } else if (this.mode === 'update') {
      this.updateConsumoNocivo(consumo);
    }
  }

  createConsumoNocivo(consumo: ConsumoNocivo) {
    this.loadingService.showLoaderUntilCompleted(
      this.consumoNocivoService.createConsumoNocivo(consumo)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.createdFailedMessage, 'CERRAR');
    });
  }

  updateConsumoNocivo(consumo: ConsumoNocivo) {
    this.loadingService.showLoaderUntilCompleted(
      this.consumoNocivoService.updateConsumoNocivo(consumo.id, consumo)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.updatedFailedMessage, 'CERRAR');
    });
  }

  isConsumidor(): boolean{
    if (this.consumidor){
      this.descipcionB = true;
    }
    else{
      this.descipcionB = false;
    }
    return this.descipcionB;
  }

  calcularTiempoConsumo() {
    const dateInit = this.form.get('fechaTentativaInicio').value;
    if ((!this.exConsumidor && !this.consumidor) || !dateInit) {
      return ;
    }
    if (this.consumidor) {
      this.form.get('tiempoConsumoMes').setValue(Math.round(calculateMonthsDiff(dateInit)));
      this.form.get('tiempoAbstinenciaMes').setValue(null);
    }
  }

  calcularTiempoExConsumidor(){
    const dateInit = this.form.get('fechaFinConsumo').value;
    if ((!this.exConsumidor && !this.consumidor) || !dateInit) {
      return ;
    }
    if (this.exConsumidor) {
      this.form.get('tiempoAbstinenciaMes').setValue(Math.round(calculateMonthsDiff(dateInit)));
      this.form.get('tiempoConsumoMes').setValue(null);
    }
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  toggleDropdown(event: any): void {
    if (TiposConsumidorEnum.CONSUMIDOR === event.value) {
      this.consumidor = true;
      this.exConsumidor = false;
    } else {
      this.exConsumidor = true;
      this.consumidor = false;
    }
    this.calcularTiempoConsumo();
  }

  displayFn(tipo: TipoConsumoNocivo): string {
    return tipo && tipo.nombre ? tipo.nombre : '';
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

}
