import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioGestacionModalComponent } from './inicio-gestacion-modal.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';



@NgModule({
  declarations: [InicioGestacionModalComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatDialogModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    IconModule,
    MatInputModule,
    MatDatepickerModule,
    LoadingModule
  ]
})
export class InicioGestacionModalModule { }
