import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import icClose from '@iconify/icons-ic/twotone-close';
import icDoneAll from '@iconify/icons-ic/twotone-done-all';
import icBabyCarriage from '@iconify/icons-fa-solid/baby-carriage';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {calculateDaysDiff, calculateWeeksDiff} from 'src/app/core/utils/operations.util';
import {LoadingService} from 'src/app/core/services/loading.service';
import {AntecedentePersonalService} from 'src/app/modules/medicina-general/antecedente-clinico/services/antecedente-personal.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'vex-inicio-gestacion-modal',
  templateUrl: './inicio-gestacion-modal.component.html',
  styleUrls: ['./inicio-gestacion-modal.component.scss']
})
export class InicioGestacionModalComponent implements OnInit {
  maxDate = new Date();
  form: FormGroup;
  icClose = icClose;
  icBabyCarriage = icBabyCarriage;
  icDoneAll = icDoneAll;
  messages = USER_MESSAGES;
  errorMessages = FORM_ERROR_MESSAGES;

  constructor(
    @Inject(MAT_DIALOG_DATA) public idAntecedente: number,
    private dialogRef: MatDialogRef<InicioGestacionModalComponent>,
    private fb: FormBuilder,
    private loadingService: LoadingService,
    private antecedentePersonalService: AntecedentePersonalService,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      fechaUltimaMenstruacion: [null, Validators.required],
      semanasEmbarazo: [{value: null, disabled: true}, Validators.required],
      diasFaltantesEmbarazo: [{value: null, disabled: true}],
      fechaProbableParto: [null],
      observacion: [null]
    });
  }

  save() {
    const data = this.form.getRawValue();
    this.loadingService.showLoaderUntilCompleted(
      this.antecedentePersonalService.initializePeriodoGestacion(this.idAntecedente, data)
    ).subscribe((response) => {
      if (response) {
        this.showNotification('La paciente ha iniciado su período de gestación');
        this.dialogRef.close(response);
      }
    }, error => {
      this.showNotification('No se pudo iniciar el período de gestación de la paciente');
    });
  }

  showNotification(message: string) {
    this.snackbar.open(message, 'CERRAR', {
      duration: 5000
    });
  }

  calculateSemanas() {
    const fechaIngreso = this.form.get('fechaUltimaMenstruacion').value;
    if (!fechaIngreso) { return; }
    let diff = Math.round(calculateWeeksDiff(fechaIngreso));
    if (diff < 1) { diff = 0; }
    this.form.get('semanasEmbarazo').setValue(diff);
  }

  calculateDiasFaltantes() {
    const fechaProbableParto: Date = this.form.get('fechaProbableParto').value;
    if (!fechaProbableParto) { return; }
    let diff = Math.round(calculateDaysDiff(new Date(), fechaProbableParto));
    if (diff < 1) { diff = 0; }
    this.form.get('diasFaltantesEmbarazo').setValue(diff);
  }
}
