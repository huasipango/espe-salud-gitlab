import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from 'src/@vex/interfaces/vex-route.interface';
import {AntecedenteClinicoComponent} from './antecedente-clinico.component';


const routes: VexRoutes = [
  {
    path: '',
    component: AntecedenteClinicoComponent,
    data: {
      toolbarShadowEnabled: true,
      containerEnabled: true
    },
    children: [
      {
        path: '',
        loadChildren: () => import('./pages/informacion-general/informacion-general.module').then(m => m.InformacionGeneralModule)
      },
      {
        path: 'antecedentes',
        loadChildren: () => import('./pages/antecedentes/antecedentes.module').then(m => m.AntecedentesModule)
      },
      {
        path: 'estilo-vida',
        loadChildren: () => import('./pages/estilo-vida/estilo-vida.module').then(m => m.EstiloVidaModule)
      },
      {
        path: 'sexualidad',
        loadChildren: () => import('./pages/sexualidad/sexualidad.module').then(m => m.SexualidadModule)
      }
    ]
  },
  {
    path: 'registro-antecedente',
    loadChildren: () => import('./pages/antecedente-personal-create/antecedente-personal-create.module')
      .then(m => m.AntecedentePersonalCreateModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AntecedenteClinicoRoutingModule {
}
