import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InformacionGeneralComponent } from './informacion-general.component';
import {InformacionGeneralRoutingModule} from './informacion-general-routing.module';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatRippleModule} from '@angular/material/core';
import {AngularSvgIconModule} from 'angular-svg-icon';



@NgModule({
  declarations: [InformacionGeneralComponent],
  imports: [
    CommonModule,
    InformacionGeneralRoutingModule,
    MatIconModule,
    IconModule,
    FlexLayoutModule,
    MatButtonModule,
    MatRippleModule,
    AngularSvgIconModule
  ]
})
export class InformacionGeneralModule { }
