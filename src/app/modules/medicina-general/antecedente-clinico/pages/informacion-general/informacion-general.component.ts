import {Component, OnInit} from '@angular/core';
import {AntecedentePersonalSharedService} from '../../services/antecedente-personal-shared.service';
import {AntecedentePersonal} from '../../models/antecedente-personal.model';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import icSearch from '@iconify/icons-ic/twotone-search';
import icEdit from '@iconify/icons-fa-solid/edit';
import icAllergies from '@iconify/icons-fa-solid/allergies';
import icTransgenderAlt from '@iconify/icons-fa-solid/transgender-alt';
import icGenderless from '@iconify/icons-fa-solid/genderless';
import icClock from '@iconify/icons-fa-solid/clock';
import icClipBoardList from '@iconify/icons-fa-solid/clipboard-list';
import icClose from '@iconify/icons-ic/close';
import icPen from '@iconify/icons-fa-solid/pen';
import icBaby from '@iconify/icons-fa-solid/baby';

import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {ActivatedRoute, Router} from '@angular/router';
import {SexoEnum} from 'src/app/core/enums/sexo.enum';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {MatDialog} from '@angular/material/dialog';
import {InicioGestacionModalComponent} from 'src/app/modules/medicina-general/antecedente-clinico/components/inicio-gestacion-modal/inicio-gestacion-modal.component';
import {InicioLactanciaModalComponent} from 'src/app/modules/medicina-general/antecedente-clinico/components/inicio-lactancia-modal/inicio-lactancia-modal.component';
import {AntecedentePersonalService} from 'src/app/modules/medicina-general/antecedente-clinico/services/antecedente-personal.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {LoadingService} from 'src/app/core/services/loading.service';

@Component({
  selector: 'vex-informacion-general',
  templateUrl: './informacion-general.component.html',
  styleUrls: ['./informacion-general.component.scss'],
  animations: [
    fadeInUp400ms,
    fadeInRight400ms,
    scaleIn400ms,
    stagger40ms,
    scaleFadeIn400ms
  ]
})
export class InformacionGeneralComponent implements OnInit {

  icSearch = icSearch;
  icEdit = icEdit;
  icClose = icClose;
  icAllergies = icAllergies;
  icTransgenderAlt = icTransgenderAlt;
  icGenderless = icGenderless;
  icClock = icClock;
  icClipBoardList = icClipBoardList;
  icPen = icPen;
  icBaby = icBaby;
  antecedentePersonal: AntecedentePersonal;
  pacienteActual: Paciente;
  constructor(
    private router: Router,
    private antecedenteSharedService: AntecedentePersonalSharedService,
    private route: ActivatedRoute,
    private pacienteGlobalService: PacienteGlobalService,
    private antecedentePersonalService: AntecedentePersonalService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private loadingService: LoadingService
  ) {
  }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
        }
      });
    this.antecedenteSharedService.antecedentePersonal$
      .subscribe((antecedente) => {
        this.antecedentePersonal = antecedente;
      });
  }

  updateAntecedentePersonal() {
    this.router.navigate(['./registro-antecedente'],
      {state: this.antecedentePersonal, relativeTo: this.route});
  }

  isFemenino(): boolean {
    return this.pacienteActual.sexo === SexoEnum.MUJER;
  }

  initializePeriodoGestacion() {
    this.dialog.open(InicioGestacionModalComponent, {
      data: this.antecedentePersonal.id,
      width: '650px',
      maxWidth: '100%',
      disableClose: true
    }).afterClosed()
      .subscribe(value => {
        if (value) {
          this.antecedentePersonal = value;
        }
      });
  }

  finalizePeriodoGestacion() {
    this.dialog.open(InicioLactanciaModalComponent, {
      data: this.antecedentePersonal.id,
      width: '650px',
      maxWidth: '100%',
      disableClose: true
    }).afterClosed()
      .subscribe(value => {
        if (value) {
          this.antecedentePersonal = value;
        }
      });
  }

  finalizePeriodoLactancia() {
    this.loadingService.showLoaderUntilCompleted(
      this.antecedentePersonalService.finalizePeriodoLactancia(this.antecedentePersonal.id)
    ).subscribe((antedente) => {
        if (antedente) {
          this.antecedentePersonal = antedente;
          this.showNotification('La paciente ha finalizado su periodo de lactancia');
        }
      }, error => {
        this.showNotification('La paciente no pudo finalizar su periodo de lactancia');
      });
  }

  showNotification(message: string) {
    this.snackbar.open(message, 'CERRAR', {
      duration: 5000
    });
  }

}
