import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {InformacionGeneralComponent} from './informacion-general.component';


const routes: Routes = [
  {
    path: '',
    component: InformacionGeneralComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InformacionGeneralRoutingModule {
}
