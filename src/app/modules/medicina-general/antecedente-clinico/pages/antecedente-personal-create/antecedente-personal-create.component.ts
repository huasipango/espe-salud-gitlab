import {Component, OnInit} from '@angular/core';
import icDescription from '@iconify/icons-ic/twotone-description';
import icDoneAll from '@iconify/icons-ic/twotone-done-all';
import icClose from '@iconify/icons-ic/twotone-close';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPerson from '@iconify/icons-ic/twotone-person';
import icRestaurantMenu from '@iconify/icons-ic/restaurant';
import icBed from '@iconify/icons-fa-solid/bed';
import icCloudSun from '@iconify/icons-fa-solid/cloud-sun';
import icAllergies from '@iconify/icons-fa-solid/allergies';
import icTransgenderAlt from '@iconify/icons-fa-solid/transgender-alt';
import icGenderless from '@iconify/icons-fa-solid/genderless';
import icCalendarCheck from '@iconify/icons-fa-solid/calendar-check';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {SimpleCatalogUtil} from 'src/app/core/utils/simple-catalog-util';
import {AntecedentePersonal} from 'src/app/modules/medicina-general/antecedente-clinico/models/antecedente-personal.model';
import {ActivatedRoute, Router} from '@angular/router';
import {stagger80ms} from 'src/@vex/animations/stagger.animation';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {AntecedentePersonalService} from 'src/app/modules/medicina-general/antecedente-clinico/services/antecedente-personal.service';
import {AntecedentePersonalSharedService} from 'src/app/modules/medicina-general/antecedente-clinico/services/antecedente-personal-shared.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {LoadingService} from 'src/app/core/services/loading.service';
import {OrientacionSexual} from 'src/app/core/models/catalogo/orientacion-sexual.model';
import {Observable} from 'rxjs';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {FrecuenciaAlimentacion} from 'src/app/core/models/catalogo/frecuencia-alimentacion.model';
import {IdentidadGenero} from 'src/app/core/models/catalogo/identidad-genero.model';

@Component({
  selector: 'vex-antecedente-personal-create',
  templateUrl: './antecedente-personal-create.component.html',
  styleUrls: ['./antecedente-personal-create.component.scss'],
  animations: [
    stagger80ms,
    fadeInUp400ms,
    scaleIn400ms,
    fadeInRight400ms
  ],
})
export class AntecedentePersonalCreateComponent implements OnInit {

  mode: 'create' | 'update' = 'create';

  icDescription = icDescription;
  icDoneAll = icDoneAll;
  icRestaurantMenu = icRestaurantMenu;
  icAllergies = icAllergies;
  icBed = icBed;
  icCloudSun = icCloudSun;
  icTransgenderAlt = icTransgenderAlt;
  icGenderless = icGenderless;
  icCalendarCheck = icCalendarCheck;
  icClose = icClose;
  icPerson = icPerson;
  icDelete = icDelete;

  orientacionList: Observable<OrientacionSexual[]>;
  identidadGeneroList: Observable<IdentidadGenero[]>;
  threeOptionsList = SimpleCatalogUtil.tresOpcionesCatalogo;
  alimentacionList: Observable<FrecuenciaAlimentacion[]>;

  paciente: Paciente;
  antecedente: AntecedentePersonal;

  generalFormGroup: FormGroup;
  alimentacionFormGroup: FormGroup;
  suenioFormGroup: FormGroup;

  constructor(
    private fb: FormBuilder,
    private pacienteGlobalService: PacienteGlobalService,
    private router: Router,
    private snackbar: MatSnackBar,
    private route: ActivatedRoute,
    private antecedenteService: AntecedentePersonalService,
    private antecedentePersonalSharedService: AntecedentePersonalSharedService,
    private loadingService: LoadingService,
    private catalogoService: CatalogoService,
  ) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.mode = 'update';
      this.antecedente = this.router.getCurrentNavigation().extras.state as AntecedentePersonal;
    } else {
      this.antecedente = {} as AntecedentePersonal;
    }
  }

  ngOnInit(): void {
    this.orientacionList = this.catalogoService.getOrientacionesSexuales();
    this.identidadGeneroList = this.catalogoService.getIdentidadesGenero();
    this.alimentacionList = this.catalogoService.getFrecuenciasAlimentacion();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.paciente = paciente;
          this.initializeForms();
          this.verifyExistAntecedente();
        }
      });
  }

  verifyExistAntecedente(): void {
    this.antecedentePersonalSharedService.antecedentePersonal$
      .subscribe((antecedente: AntecedentePersonal) => {
        if (antecedente && this.isCreateMode()) {
          this.navigateBack();
        } else {
          this.verifyExistAntecedenteBackend();
        }
      });
  }

  verifyExistAntecedenteBackend(): void {
    this.antecedenteService.getAntecedentePersonal(this.paciente.id)
      .subscribe((data: AntecedentePersonal) => {
        if (data && this.isCreateMode()) {
          this.navigateBack();
        }
      });
  }

  initializeForms(): void {
    this.generalFormGroup = this.fb.group({
      id: this.antecedente.id || null,
      idOrientacionSexual: [this.antecedente.idOrientacionSexual || '', Validators.required],
      idIdentidadGenero: [this.antecedente.idIdentidadGenero || '', Validators.required],
      poseeAlergia: [this.antecedente.poseeAlergia || '', Validators.required],
      descripcionAlergia: [this.antecedente.descripcionAlergia || '', Validators.maxLength(255)],
      idPaciente: this.antecedente.idPaciente || this.paciente.id
    });

    this.alimentacionFormGroup = this.fb.group({
      idFrecuenciaAlimentacion: [this.antecedente.idFrecuenciaAlimentacion || '', Validators.required],
      predominioAlimentario: [this.antecedente.predominioAlimentario || '', Validators.required],
      observacionAlimentacion: this.antecedente.observacionAlimentacion || '',
    });

    this.suenioFormGroup = this.fb.group({
      horaSuenio: [this.antecedente.horaSuenio || '', Validators.required],
      horaDespertar: [this.antecedente.horaDespertar || '', Validators.required],
      observacionHabitoSuenio: this.antecedente.observacionHabitoSuenio || '',
    });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  poseeAlergia(): boolean {
    return this.generalFormGroup.get('poseeAlergia').value === 'SI';
  }

  save() {
    this.antecedente = this.generalFormGroup.value;
    this.antecedente.idFrecuenciaAlimentacion = this.alimentacionFormGroup.get('idFrecuenciaAlimentacion').value;
    this.antecedente.predominioAlimentario = this.alimentacionFormGroup.get('predominioAlimentario').value;
    this.antecedente.observacionAlimentacion = this.alimentacionFormGroup.get('observacionAlimentacion').value;

    this.antecedente.horaSuenio = this.suenioFormGroup.get('horaSuenio').value;
    this.antecedente.horaDespertar = this.suenioFormGroup.get('horaDespertar').value;
    this.antecedente.observacionHabitoSuenio = this.suenioFormGroup.get('observacionHabitoSuenio').value;

    if (!this.poseeAlergia()) {
      this.antecedente.descripcionAlergia = '';
    }
    if (this.isCreateMode()) {
      this.createNewAntecedente();
    } else {
      this.updateAntecedente();
    }
  }

  createNewAntecedente(): void {
    this.loadingService.showLoaderUntilCompleted(
      this.antecedenteService.createAntecedentePersonal(this.antecedente)
    ).subscribe((data: AntecedentePersonal) => {
        if (data) {
          this.antecedentePersonalSharedService.setAntecedentePersonal(data);
          this.showNotification('Registro guardado EXITOSAMENTE', 'OK');
          this.navigateBack();
        }
      },
      (error) => {
        this.showNotification('ERROR al guardar el registro', 'CERRAR');
      }
    );
  }

  updateAntecedente(): void {
    this.loadingService.showLoaderUntilCompleted(
      this.antecedenteService.updateAntecedentePersonal(this.antecedente.id, this.antecedente)
    ).subscribe((data: AntecedentePersonal) => {
        if (data) {
          this.antecedentePersonalSharedService.setAntecedentePersonal(data);
          this.showNotification('Registro actualizado EXITOSAMENTE', 'OK');
          this.navigateBack();
        }
      },
      (error) => {
        this.showNotification('ERROR al actualizar el registro', 'CERRAR');
        console.log(error);
      });
  }

  navigateBack(): void {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }
}
