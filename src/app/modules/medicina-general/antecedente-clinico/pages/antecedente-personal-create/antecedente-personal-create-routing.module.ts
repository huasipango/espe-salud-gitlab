import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AntecedentePersonalCreateComponent} from 'src/app/modules/medicina-general/antecedente-clinico/pages/antecedente-personal-create/antecedente-personal-create.component';


const routes: Routes = [
  {
    path: '',
    component: AntecedentePersonalCreateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AntecedentePersonalCreateRoutingModule {
}
