import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AntecedentePersonalCreateComponent } from './antecedente-personal-create.component';
import {AntecedentePersonalCreateRoutingModule} from 'src/app/modules/medicina-general/antecedente-clinico/pages/antecedente-personal-create/antecedente-personal-create-routing.module';
import {SecondaryToolbarModule} from 'src/@vex/components/secondary-toolbar/secondary-toolbar.module';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {FlexModule} from '@angular/flex-layout';
import {IconModule} from '@visurel/iconify-angular';
import {MatStepperModule} from '@angular/material/stepper';
import {MatIconModule} from '@angular/material/icon';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {EmptyPacienteModule} from 'src/app/core/components/empty-paciente/empty-paciente.module';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import {UppercaseModule} from 'src/app/shared/directives/uppercase/uppercase.module';



@NgModule({
  declarations: [AntecedentePersonalCreateComponent],
  imports: [
    CommonModule,
    AntecedentePersonalCreateRoutingModule,
    SecondaryToolbarModule,
    BreadcrumbsModule,
    FlexModule,
    IconModule,
    MatStepperModule,
    MatIconModule,
    ContainerModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    EmptyPacienteModule,
    LoadingModule,
    UppercaseModule
  ]
})
export class AntecedentePersonalCreateModule { }
