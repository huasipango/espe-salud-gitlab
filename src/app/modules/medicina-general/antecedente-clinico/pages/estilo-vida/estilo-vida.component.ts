import {Component, OnInit} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import icSearch from '@iconify/icons-ic/twotone-search';
import icEdit from '@iconify/icons-fa-solid/edit';
import icRestaurantMenu from '@iconify/icons-ic/restaurant';
import icBed from '@iconify/icons-fa-solid/bed';
import icCloudSun from '@iconify/icons-fa-solid/cloud-sun';
import icCalendarCheck from '@iconify/icons-fa-solid/calendar-check';
import icClock from '@iconify/icons-fa-solid/clock';
import icClipBoardList from '@iconify/icons-fa-solid/clipboard-list';
import icClose from '@iconify/icons-ic/close';
import {AntecedentePersonal} from 'src/app/modules/medicina-general/antecedente-clinico/models/antecedente-personal.model';
import {AntecedentePersonalSharedService} from 'src/app/modules/medicina-general/antecedente-clinico/services/antecedente-personal-shared.service';

@Component({
  selector: 'vex-estilo-vida',
  templateUrl: './estilo-vida.component.html',
  styleUrls: ['./estilo-vida.component.scss'],
  animations: [
    fadeInUp400ms,
    fadeInRight400ms,
    scaleIn400ms,
    stagger40ms
  ]
})
export class EstiloVidaComponent implements OnInit {

  icSearch = icSearch;
  icEdit = icEdit;
  icClose = icClose;
  icRestaurantMenu = icRestaurantMenu;
  icBed = icBed;
  icCloudSun = icCloudSun;
  icCalendarCheck = icCalendarCheck;
  icClock = icClock;
  icClipBoardList = icClipBoardList;

  twentyFour: Date = new Date();
  sleepTime: Date = new Date();
  getUpTime: Date = new Date();
  totalHorasSuenio = '';

  antecedentePersonal: AntecedentePersonal;


  constructor(
    private antecedenteSharedService: AntecedentePersonalSharedService,
  ) {
  }

  ngOnInit(): void {
    this.antecedenteSharedService.antecedentePersonal$
      .subscribe((antecedente) => {
        this.antecedentePersonal = antecedente;
      });
  }

  calcularTotalHorasSuenio(): string {
    this.getUpTime.setDate(this.getUpTime.getDate() + 1);
    this.twentyFour.setHours(24, 0, 0, 0);
    const horaDespertar = this.antecedentePersonal.horaDespertar.split(':');
    const horaSuenio = this.antecedentePersonal.horaSuenio.split(':');
    this.sleepTime.setHours(Number(horaSuenio[0]), Number(horaSuenio[1]), 0, 0);
    this.getUpTime.setHours(Number(horaDespertar[0]), Number(horaDespertar[1]), 0, 0);
    const milliseconds = (Number(this.getUpTime) - Number(this.sleepTime)); // milliseconds
    const horas = Math.floor((milliseconds % 86400000) / 3600000); // hours
    const minutes = Math.round(((milliseconds % 86400000) % 3600000) / 60000); // minutes
    this.totalHorasSuenio = horas + ' h ' + minutes + ' min';
    return this.totalHorasSuenio;
  }

}
