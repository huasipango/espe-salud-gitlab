import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EstiloVidaComponent } from './estilo-vida.component';
import {EstiloVidaRoutingModule} from './estilo-vida-routing.module';
import {ActividadFisicaModule} from '../../components/actividad-fisica/actividad-fisica.module';
import {AntecedenteClinicoModule} from '../../antecedente-clinico.module';
import {ConsumoNocivoModule} from '../../components/consumo-nocivo/consumo-nocivo.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {PlanificacionFamiliarModule} from '../../components/planificacion-familiar/planificacion-familiar.module';
import {IconModule} from '@visurel/iconify-angular';
import {ContainerModule} from 'src/@vex/directives/container/container.module';


@NgModule({
  declarations: [EstiloVidaComponent],
  imports: [
    CommonModule,
    EstiloVidaRoutingModule,
    AntecedenteClinicoModule,
    FlexLayoutModule,
    ActividadFisicaModule,
    ConsumoNocivoModule,
    IconModule,
    ContainerModule,
  ]
})
export class EstiloVidaModule { }
