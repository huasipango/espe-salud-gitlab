import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {EstiloVidaComponent} from './estilo-vida.component';


const routes: Routes = [
  {
    path: '',
    component: EstiloVidaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstiloVidaRoutingModule {
}
