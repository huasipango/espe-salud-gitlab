import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SexualidadComponent} from './sexualidad.component';

const routes: Routes = [
  {
    path: '',
    component: SexualidadComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SexualidadRoutingModule {
}
