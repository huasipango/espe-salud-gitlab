import { Component, OnInit } from '@angular/core';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {SexoEnum} from 'src/app/core/enums/sexo.enum';

@Component({
  selector: 'vex-sexualidad',
  templateUrl: './sexualidad.component.html',
  styleUrls: ['./sexualidad.component.scss']
})
export class SexualidadComponent implements OnInit {

  pacienteActual: Paciente;
  constructor(
    private pacienteGlobalService: PacienteGlobalService,
  ) { }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        this.pacienteActual = paciente;
        this.isFemenino();
      });
  }

  isFemenino(): boolean {
    return this.pacienteActual.sexo === SexoEnum.MUJER;
  }

}
