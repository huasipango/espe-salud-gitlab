import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SexualidadComponent } from './sexualidad.component';
import {SexualidadRoutingModule} from './sexualidad-routing.module';
import {AntecedenteClinicoModule} from '../../antecedente-clinico.module';
import {PlanificacionFamiliarModule} from '../../components/planificacion-familiar/planificacion-familiar.module';
import {AntecedenteGinecologicoModule} from '../../components/antecedente-ginecologico/antecedente-ginecologico.module';
import {ExamenSexualModule} from '../../components/examen-sexual/examen-sexual.module';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {GridModule} from '@angular/flex-layout';



@NgModule({
  declarations: [SexualidadComponent],
  imports: [
    CommonModule,
    SexualidadRoutingModule,
    AntecedenteClinicoModule,
    PlanificacionFamiliarModule,
    ExamenSexualModule,
    AntecedenteGinecologicoModule,
    ContainerModule,
    GridModule
  ]
})
export class SexualidadModule { }
