import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AntecedentesComponent } from './antecedentes.component';
import {AntecedentesRoutingModule} from './antecedentes-routing.module';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {PatologicoPersonalModule} from '../../components/patologico-personal/patologico-personal.module';
import {MedicacionHabitualModule} from '../../components/medicacion-habitual/medicacion-habitual.module';
import {DiscapacidadModule} from '../../components/discapacidad/discapacidad.module';
import {QuirurgicoModule} from '../../components/quirurgico/quirurgico.module';
import {PatologicoFamiliarModule} from 'src/app/modules/medicina-general/antecedente-clinico/components/patologico-familiar/patologico-familiar.module';



@NgModule({
  declarations: [AntecedentesComponent],
  imports: [
    CommonModule,
    AntecedentesRoutingModule,
    FlexLayoutModule,
    ContainerModule,
    PatologicoPersonalModule,
    MedicacionHabitualModule,
    DiscapacidadModule,
    QuirurgicoModule,
    PatologicoFamiliarModule
  ]
})
export class AntecedentesModule { }
