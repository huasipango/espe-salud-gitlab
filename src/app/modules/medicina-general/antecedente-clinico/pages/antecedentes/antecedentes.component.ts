import { Component, OnInit } from '@angular/core';
import {AntecedentePersonal} from '../../models/antecedente-personal.model';
import {AntecedentePersonalSharedService} from '../../services/antecedente-personal-shared.service';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {AntecedentePersonalService} from '../../services/antecedente-personal.service';

@Component({
  selector: 'vex-antecedentes',
  templateUrl: './antecedentes.component.html',
  styleUrls: ['./antecedentes.component.scss'],
  animations: [
    fadeInUp400ms,
    fadeInRight400ms,
    scaleIn400ms,
    stagger40ms
  ]
})
export class AntecedentesComponent implements OnInit {

  antecedentePersonal: AntecedentePersonal;
  pacienteActual: Paciente;

  constructor(
    private pacienteGlobalService: PacienteGlobalService,
    private antecedenteService: AntecedentePersonalService,
    private antecedenteSharedService: AntecedentePersonalSharedService
  ) { }

  ngOnInit(): void {
    this.antecedenteSharedService.antecedentePersonal$
      .subscribe((antecedente)=> {
        this.antecedentePersonal = antecedente;
      });
  }

}
