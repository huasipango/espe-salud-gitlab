import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AntecedenteClinicoComponent } from './antecedente-clinico.component';
import {AntecedenteClinicoRoutingModule} from './antecedente-clinico-routing.module';
import {PageLayoutModule} from 'src/@vex/components/page-layout/page-layout.module';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatTabsModule} from '@angular/material/tabs';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatPaginatorModule} from '@angular/material/paginator';
import {EmptyPacienteModule} from 'src/app/core/components/empty-paciente/empty-paciente.module';
import {FlexModule} from '@angular/flex-layout';
import {AntecedenteEmptyModule} from './components/antecedente-empty/antecedente-empty.module';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';

@NgModule({
  declarations: [AntecedenteClinicoComponent],
  imports: [
    CommonModule,
    AntecedenteClinicoRoutingModule,
    PageLayoutModule,
    BreadcrumbsModule,
    MatButtonToggleModule,
    ReactiveFormsModule,
    MatTabsModule,
    MatIconModule,
    IconModule,
    MatMenuModule,
    MatTableModule,
    MatCheckboxModule,
    MatPaginatorModule,
    FormsModule,
    EmptyPacienteModule,
    FlexModule,
    AntecedenteEmptyModule,
    LoadingModule
  ]
})
export class AntecedenteClinicoModule { }
