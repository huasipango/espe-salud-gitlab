export class PeriodoGestacion{
  fechaUltimaMenstruacion: Date;
  semanasEmbarazo: number;
  diasFaltantesEmbarazo: number;
  fechaProbableParto: Date;
}
