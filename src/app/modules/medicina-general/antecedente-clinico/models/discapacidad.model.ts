import {TipoDiscapacidad} from 'src/app/core/models/catalogo/tipo-discapacidad.model';

export class Discapacidad {
  id: number;
  tipoColaborador: string;
  porcentajeDiscapacidad: string;
  gradoDiscapacidad: string;
  diagnostico: string;
  idAntecedentePersonal: number;
  idTipoDiscapacidad: number;
  tipoDiscapacidad: TipoDiscapacidad;
}
