import {TipoConsumoNocivo} from 'src/app/core/models/catalogo/tipo-consumo-nocivo.model';

export class ConsumoNocivo{
  id: number;
  tipoConsumidor: string;
  fechaRegistro: string;
  fechaTentativaInicio: string;
  fechaFinConsumo: Date;
  nombreConsumoNocivo: TipoConsumoNocivo;
  tiempoConsumoMes: number;
  frecuenciaConsumo: string;
  cantidadConsumo: string;
  tiempoAbstinenciaMes: number;
  observacion: string;
}
