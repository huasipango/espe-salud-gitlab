import {TipoPlanificacionFamiliar} from 'src/app/core/models/catalogo/tipo-planificacion-familial.model';

export class PlanificacionFamiliar {
  id: number;
  fecha: Date;
  vidaSexualActiva: string;
  metodoPlanificacion: string;
  frecuenciaPlanificacionFamiliar: string;
  edadInicioPlanificacionFamiliar: number;
  idTipoPlanificacionFamiliar: number;
  tipoPlanificacionFamiliar: TipoPlanificacionFamiliar;
  hijosVivos: number;
  hijosMuertos: number;
  observacion: string;
  idAntecedentePersonal: number;
}
