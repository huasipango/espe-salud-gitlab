import {TipoEnfermedadCIE10} from 'src/app/core/models/catalogo/tipo-enfermedad-cie10.model';

export class AntecedenteQuirurgico {
  id: number;
  idDiagnosticoPreQuirurgico: string;
  diagnosticoPreQuirurgico: TipoEnfermedadCIE10;
  procedimientoQuirurgico: string;
  fechaProcedimiento: Date;
  secuelas: string;
  idAntecedentePersonal: number;
}
