import {Parentesco} from 'src/app/core/models/catalogo/parentesco.model';

export class AntecedentePatologicoFamiliar {
  id: number;
  idParentesco: number;
  observacion: string;
  diagnostico: string;
  idTipoEnfermedadFamiliar: number;
  idAntecedentePersonal: number;
  parentesco: Parentesco;
}
