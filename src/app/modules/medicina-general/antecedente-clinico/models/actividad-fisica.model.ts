export class ActividadFisica {
  id: number;
  frecuencia: string;
  tiempoHoras: number;
  nombreActividad: string;
  observacion: string;
  idAntecedentePersonal: number;
}
