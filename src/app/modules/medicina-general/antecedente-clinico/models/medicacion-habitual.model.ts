export class MedicacionHabitual {
  id: number;
  descripcionMedicamento: string;
  idFrecuencia: number;
  cantidad: number;
  observacion: string;
  idAntecedentePersonal: number;
}
