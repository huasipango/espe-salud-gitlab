export class AntecedenteGinecologico {
  id: number;
  fecha: Date;
  edadMenarquia: number;
  cicloMenstruacion: string;
  fechaUltimaMenstruacion: string;
  gestas: number;
  numeroPartosVaginales: number;
  numeroCesareas: number;
  numeroAbortos: number;
  hijosMuertos: number;
  hijosVivos: number;
  idAntecedentePersonal: number;
}
