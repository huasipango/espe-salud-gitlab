import {TipoExamenSexual} from 'src/app/core/models/catalogo/tipo-examen-sexual.model';

export class ExamenSexual {
  id: number;
  fecha: string;
  fechaExamen: Date;
  idNombreExamen: number;
  nombreExamen: TipoExamenSexual;
  resultado: string;
  tiempoAnios: number;
}
