export class AntecedentePatologicoPersonal {
  id: number;
  idTipoEnfermedadPersonal: string;
  fechaDiagnostico: Date;
  frecuenciaMedicacion: string;
  idDiagnostico: string;
  observaciones: string;
  esEnfermedadCatastrofica: boolean;
  idAntecedente: number;
}
