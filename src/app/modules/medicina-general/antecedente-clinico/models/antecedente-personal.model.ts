import {OrientacionSexual} from 'src/app/core/models/catalogo/orientacion-sexual.model';
import {IdentidadGenero} from 'src/app/core/models/catalogo/identidad-genero.model';
import {FrecuenciaAlimentacion} from 'src/app/core/models/catalogo/frecuencia-alimentacion.model';

export class AntecedentePersonal {
  id: number;
  idOrientacionSexual: number;
  orientacionSexual: OrientacionSexual;
  idIdentidadGenero: number;
  identidadGenero: IdentidadGenero;
  poseeAlergia: string;
  descripcionAlergia: string;
  idFrecuenciaAlimentacion: number;
  frecuenciaAlimentacion: FrecuenciaAlimentacion;
  predominioAlimentario: string;
  observacionAlimentacion: string;
  periodoGestacion: boolean;
  periodoLactancia: boolean;
  semanasGestacion: number;
  horaSuenio: string;
  horaDespertar: string;
  observacionHabitoSuenio: string;
  idPaciente: number;
}
