import {UsuarioSimple} from 'src/app/core/models/usuario/usuario-simple.model';

export class ValidacionCertificado {
  id: number;
  fechaDesde: Date;
  fechaHasta: Date;
  fechaRegistro: Date;
  institucionEmite: string;
  diasReposo: number;
  equivalenteHorasLaborables: number;
  idDiagnostico: string;
  responsablePidm: string;
  idPaciente: number;
  usuario: UsuarioSimple;
}
