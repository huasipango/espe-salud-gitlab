import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icClose from '@iconify/icons-ic/twotone-close';
import icPrint from '@iconify/icons-ic/twotone-print';
import icDownload from '@iconify/icons-ic/twotone-cloud-download';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icPerson from '@iconify/icons-ic/twotone-person';
import icLocationCity from '@iconify/icons-ic/twotone-location-city';
import icRestore from '@iconify/icons-ic/restore';
import icWork from '@iconify/icons-ic/work';
import icAssigment from '@iconify/icons-ic/baseline-assignment';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';

import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {ValidacionCertificado} from 'src/app/modules/medicina-general/certificado/models/validacion-certificado.model';
import {DAYS_PLUS_TO_VAL_CERT, FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {Observable} from 'rxjs';
import {TipoEnfermedadCIE10} from 'src/app/core/models/catalogo/tipo-enfermedad-cie10.model';
import {debounceTime, distinctUntilChanged, startWith} from 'rxjs/operators';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {ValidacionCertificadoService} from 'src/app/modules/medicina-general/certificado/services/validacion-certificado.service';
import {LoadingService} from 'src/app/core/services/loading.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {calculateDaysDiff} from 'src/app/core/utils/operations.util';
import {ActividadEnfermeria} from 'src/app/modules/enfermeria/actividad-enfermeria/models/actividad.model';
import {AuthService} from 'src/app/core/auth/auth.service';
import {forbiddenObjectValidator} from 'src/app/shared/validators/forbbiden-object-validator';

@UntilDestroy()
@Component({
  selector: 'vex-validacion-certificado-modal',
  templateUrl: './validacion-certificado-modal.component.html',
  styleUrls: ['./validacion-certificado-modal.component.scss']
})
export class ValidacionCertificadoModalComponent implements OnInit {

  maxDate = new Date(3000, 1, 1);

  date = new Date();
  paciente: Paciente;
  form: FormGroup;
  mode: 'create' | 'update' = 'create';

  icMoreVert = icMoreVert;
  icClose = icClose;

  icPrint = icPrint;
  icDownload = icDownload;
  icDelete = icDelete;

  icPerson = icPerson;
  icLocationCity = icLocationCity;
  icPhone = icPhone;
  icRestore = icRestore;
  icWork = icWork;
  icArrowDropDown = icArrowDropDown;
  icAssigment = icAssigment;
  errorMessages = FORM_ERROR_MESSAGES;
  messages = USER_MESSAGES;

  diagnosticoCtrl: FormControl;
  filteredCodigosCie: Observable<TipoEnfermedadCIE10[]>;

  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: ValidacionCertificado,
    private dialogRef: MatDialogRef<ValidacionCertificadoModalComponent>,
    private fb: FormBuilder,
    private pacienteGlobalService: PacienteGlobalService,
    private catalogoService: CatalogoService,
    private loadingService: LoadingService,
    private snackbar: MatSnackBar,
    private authService: AuthService,
    private validacionCertificadoService: ValidacionCertificadoService
  ) {
  }

  ngOnInit() {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.date.setDate(this.date.getDate());
      this.defaults = {} as ValidacionCertificado;
    }

    this.diagnosticoCtrl = new FormControl(
      this.defaults.idDiagnostico, [Validators.required, forbiddenObjectValidator]
    );

    if (this.defaults.idDiagnostico) {
      this.catalogoService.getCodigoCIE10(this.defaults.idDiagnostico)
        .subscribe((codigo) => {
          if (codigo) {
            this.diagnosticoCtrl.patchValue(codigo);
          }
        });
    }

    this.diagnosticoCtrl.valueChanges
      .pipe(
        startWith(''),
        debounceTime(400),
        distinctUntilChanged(),
        untilDestroyed(this)
      ).subscribe(value => this.searchCie(value || ''));

    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.paciente = paciente;
          this.initializeForm();
        }
      });
  }

  initializeForm(): void {
    this.form = this.fb.group({
      id: this.defaults.id || null,
      fechaDesde: [this.defaults.fechaDesde || null, Validators.required],
      fechaHasta: [this.defaults.fechaHasta || null, Validators.required],
      institucionEmite: [this.defaults.institucionEmite || '', Validators.required],
      diasReposo: [{value: this.defaults.diasReposo || 0, disabled: true}, Validators.required],
      equivalenteHorasLaborables: [this.defaults.equivalenteHorasLaborables || null, Validators.required],
      idDiagnostico: this.diagnosticoCtrl,
      responsablePidm: this.defaults.responsablePidm || null,
      idPaciente: this.defaults.idPaciente || this.paciente.id,
    });

    this.authService.saludUserData$
      .subscribe((saludUser) => {
        if (saludUser) {
          this.form.patchValue({responsablePidm: saludUser.pidm});
        }
      });
  }

  save() {
    const certificado = this.form.getRawValue();
    const cie10 = certificado.idDiagnostico;
    certificado.idDiagnostico = cie10.codigo;
    if (this.mode === 'create') {
      this.createCertificado(certificado);
    } else if (this.mode === 'update') {
      this.updateCertificado(certificado);
    }
  }

  createCertificado(certificado: ValidacionCertificado) {
    this.loadingService.showLoaderUntilCompleted(
      this.validacionCertificadoService.createValidacionCertificado(certificado)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.createdFailedMessage, 'CERRAR');
    });
  }

  updateCertificado(certificado: ValidacionCertificado) {
    this.loadingService.showLoaderUntilCompleted(
      this.validacionCertificadoService.updateValidacionCertificado(certificado.id, certificado)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.updatedFailedMessage, 'CERRAR');
    });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  searchCie(value: any) {
    if (value !== '' && typeof value === 'string' && value.trim().length > 1) {
      this.filteredCodigosCie = this.catalogoService.getCodigosCIE10ByQuery(value);
    }
  }

  displayFn(cie: TipoEnfermedadCIE10): string {
    return cie ? `${cie.codigo} -- ${cie.nombre}` : '';
  }

  calculateDiasReposo(): void {
    const fechaDesde = this.form.get('fechaDesde').value;
    const fechaHasta = this.form.get('fechaHasta').value;

    if (!fechaDesde || !fechaHasta) { return; }

    let daysDiff = calculateDaysDiff(fechaDesde, fechaHasta);
    daysDiff = daysDiff + DAYS_PLUS_TO_VAL_CERT;

    if (daysDiff < 0) { daysDiff = 0; }
    this.form.get('diasReposo').setValue(Math.round(daysDiff));
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

}
