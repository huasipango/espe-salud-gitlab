import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ValidacionCertificado} from 'src/app/modules/medicina-general/certificado/models/validacion-certificado.model';

@Injectable({
  providedIn: 'root'
})
export class ValidacionCertificadoService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getValidacionCertificadosByPaciente(idPaciente: number): Observable<ValidacionCertificado[]> {
    return this.http.get<ValidacionCertificado[]>(
      this.baseUrl + 'validacion-certificados/paciente?idPaciente=' + idPaciente);
  }

  createValidacionCertificado(data: ValidacionCertificado): Observable<ValidacionCertificado> {
    return this.http.post<ValidacionCertificado>(this.baseUrl + 'validacion-certificados/', JSON.stringify(data));
  }

  updateValidacionCertificado(id: number, data: ValidacionCertificado): Observable<ValidacionCertificado> {
    return this.http.put<ValidacionCertificado>(this.baseUrl + 'validacion-certificados/' + id, JSON.stringify(data));
  }

  deleteValidacionCertificado(id: number): Observable<boolean> {
    return this.http.delete<boolean>(this.baseUrl + 'validacion-certificados/' + id);
  }
}
