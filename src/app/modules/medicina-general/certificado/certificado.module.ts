import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CertificadoRoutingModule} from 'src/app/modules/medicina-general/certificado/certificado-routing.module';
import { ValidacionCertificadoListComponent } from './pages/validacion-certificado-list/validacion-certificado-list.component';
import {EmptyPacienteModule} from 'src/app/core/components/empty-paciente/empty-paciente.module';
import {IconModule} from '@visurel/iconify-angular';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {ExtendedModule, FlexModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { ValidacionCertificadoModalComponent } from './components/validacion-certificado-modal/validacion-certificado-modal.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {UppercaseModule} from 'src/app/shared/directives/uppercase/uppercase.module';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';

@NgModule({
  declarations: [ValidacionCertificadoListComponent, ValidacionCertificadoModalComponent],
  imports: [
    CommonModule,
    CertificadoRoutingModule,
    EmptyPacienteModule,
    IconModule,
    BreadcrumbsModule,
    ContainerModule,
    FlexModule,
    ExtendedModule,
    MatButtonModule,
    MatTooltipModule,
    ReactiveFormsModule,
    MatIconModule,
    MatMenuModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatCheckboxModule,
    FormsModule,
    MatDialogModule,
    MatInputModule,
    MatDatepickerModule,
    MatAutocompleteModule,
    UppercaseModule,
    MatMomentDateModule,
    LoadingModule
  ]
})
export class CertificadoModule { }
