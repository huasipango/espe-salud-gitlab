import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import {FormControl} from '@angular/forms';
import {ValidacionCertificado} from 'src/app/modules/medicina-general/certificado/models/validacion-certificado.model';

import icReceipt from '@iconify/icons-fa-solid/receipt';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ValidacionCertificadoModalComponent} from 'src/app/modules/medicina-general/certificado/components/validacion-certificado-modal/validacion-certificado-modal.component';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {ValidacionCertificadoService} from 'src/app/modules/medicina-general/certificado/services/validacion-certificado.service';
import {getShortDate, getShortDateTime} from 'src/app/core/utils/date-utils';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {ImagenUsuarioService} from 'src/app/core/services/imagen-usuario.service';

@UntilDestroy()
@Component({
  selector: 'vex-validacion-certificado-list',
  templateUrl: './validacion-certificado-list.component.html',
  styleUrls: ['./validacion-certificado-list.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms,
    scaleIn400ms,
    fadeInRight400ms,
    scaleFadeIn400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class ValidacionCertificadoListComponent implements OnInit, AfterViewInit {
  validacionCertificados: ValidacionCertificado[];
  pacienteActual: Paciente;
  columns: TableColumn<ValidacionCertificado>[] = [
    {
      label: 'Fecha registro',
      property: 'fechaRegistro',
      type: 'datetime',
      visible: true,
      cssClasses: ['text-secondary', 'font-medium']
    },
    {label: 'Fecha desde', property: 'fechaDesde', type: 'date', visible: true, cssClasses: ['font-medium']},
    {label: 'Fecha hasta', property: 'fechaHasta', type: 'date', visible: true},
    {label: 'Institución emisora', property: 'institucionEmite', type: 'text', visible: true},
    {
      label: 'Días de reposo',
      property: 'diasReposo',
      type: 'text',
      visible: true,
      cssClasses: ['text-secondary', 'font-medium']
    },
    {
      label: 'Eq. horas laborales',
      property: 'equivalenteHorasLaborables',
      type: 'text',
      visible: true,
      cssClasses: ['text-secondary', 'font-medium']
    },
    {
      label: 'CIE10',
      property: 'idDiagnostico',
      type: 'text',
      visible: true,
      cssClasses: ['text-secondary', 'font-medium']
    },
    {
      label: 'Responsable',
      property: 'usuario',
      type: 'object',
      visible: true
    },
    {label: 'Acciones', property: 'actions', type: 'button', visible: true}
  ];
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<ValidacionCertificado> | null;
  searchCtrl = new FormControl();

  icReceipt = icReceipt;
  icSearch = icSearch;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icMoreHoriz = icMoreHoriz;
  icEdit = icEdit;
  icDelete = icDelete;
  messages = USER_MESSAGES;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private pacienteGlobalService: PacienteGlobalService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private validacionService: ValidacionCertificadoService,
    private imagenUsuarioService: ImagenUsuarioService
  ) {
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getValidaciones();
        }
      });
    this.searchCtrl.valueChanges.pipe(
      untilDestroyed(this)
    ).subscribe(value => this.onFilterChange(value));
  }

  getValidaciones(): void {
    this.validacionService.getValidacionCertificadosByPaciente(this.pacienteActual.id)
      .subscribe((data) => {
        this.validacionCertificados = data;
        this.dataSource.data = this.validacionCertificados;
      });
  }

  createValidacion() {
    this.dialog.open(ValidacionCertificadoModalComponent, {
      width: '700px',
      maxWidth: '100%'
    }).afterClosed()
      .subscribe((created) => {
        if (created) {
          this.showNotification(this.messages.createdSuccessMessage, 'CERRAR');
          this.getValidaciones();
        }
      });
  }

  updateValidacion(validacion: ValidacionCertificado) {
    this.dialog.open(ValidacionCertificadoModalComponent, {
      data: validacion,
      width: '700px',
      maxWidth: '100%'
    }).afterClosed()
      .subscribe((updated) => {
        if (updated) {
          this.showNotification(this.messages.updatedSuccessMessage, 'CERRAR');
          this.getValidaciones();
        }
      });
  }

  startDeleteModal(validacion: ValidacionCertificado) {
    this.dialog.open(DeleteModalComponent, {
      data: this.messages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') {
        this.deletePatologicoPersonal(validacion);
      }
    });
  }

  deletePatologicoPersonal(validacion: ValidacionCertificado) {
    this.validacionService.deleteValidacionCertificado(validacion.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification(this.messages.deletedSuccessMessage, 'CERRAR');
          this.validacionCertificados.splice(
            this.validacionCertificados.findIndex((existing) =>
              existing.id === validacion.id), 1
          );
          this.dataSource.connect().next(this.validacionCertificados);
        } else {
          this.showNotification(this.messages.deletedSuccessMessage, 'CERRAR');
        }
      });
  }


  openSearchModal() {
  }

  trackByProperty<T>(column: TableColumn<T>) {
    return column.property;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  onFilterChange(value: string) {
    if (!this.dataSource) {
      return;
    }
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  get visibleColumns() {
    return this.columns
      .filter(column => column.visible)
      .map(column => column.property);
  }

  getDate(date: any): string {
    return getShortDate(date);
  }

  getDateTime(dateTime: any): string {
    return getShortDateTime(dateTime);
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  getUserImage(id: string) {
    return this.imagenUsuarioService.getUserImage(id);
  }
}
