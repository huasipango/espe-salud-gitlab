import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PageLayoutModule} from 'src/@vex/components/page-layout/page-layout.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {HttpClientModule} from '@angular/common/http';
import {IconModule} from '@visurel/iconify-angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDividerModule} from '@angular/material/divider';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MAT_DATE_LOCALE} from '@angular/material/core';
import {EstudioComplementarioRoutingModule} from './estudio-complementario-routing.module';
import {EstudioComplementarioCreateComponent} from './components/estudio-complementario-create/estudio-complementario-create.component';
import {EstudioComplementarioListComponent} from './pages/estudio-complementario-list/estudio-complementario-list.component';
import {EmptyPacienteModule} from 'src/app/core/components/empty-paciente/empty-paciente.module';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';


@NgModule({
  declarations: [
    EstudioComplementarioCreateComponent,
    EstudioComplementarioListComponent,
  ],
  imports: [
    CommonModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    EstudioComplementarioRoutingModule,
    HttpClientModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatCheckboxModule,
    MatIconModule,
    MatButtonModule,
    MatSnackBarModule,
    MatMenuModule,
    MatDialogModule,
    IconModule,
    FormsModule,
    MatTooltipModule,
    ReactiveFormsModule,
    ContainerModule,
    MatSelectModule,
    MatButtonToggleModule,
    MatAutocompleteModule,
    MatDividerModule,
    MatInputModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatMomentDateModule,
    EmptyPacienteModule,
    LoadingModule
  ],
  entryComponents: [EstudioComplementarioCreateComponent],
  exports: [EstudioComplementarioCreateComponent]
})
export class EstudioComplementarioModule {
}
