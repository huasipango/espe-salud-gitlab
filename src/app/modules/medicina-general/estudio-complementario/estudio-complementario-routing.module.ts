import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from 'src/@vex/interfaces/vex-route.interface';
import { EstudioComplementarioCreateComponent } from './components/estudio-complementario-create/estudio-complementario-create.component';
import { EstudioComplementarioListComponent } from './pages/estudio-complementario-list/estudio-complementario-list.component';


const routes: VexRoutes = [
  {
    path: '',
    component: EstudioComplementarioListComponent,
    data: {
      toolbarShadowEnabled: true,
      containerEnabled: true
    }
  },
  {
    path: 'form',
    component: EstudioComplementarioCreateComponent,
    data: {
      toolbarShadowEnabled: true,
      containerEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstudioComplementarioRoutingModule { }
