import {TipoEstudioComplementario} from 'src/app/core/models/catalogo/tipo-estudio-complementario.model';

export class EstudioComplementario {
    id: number;
    fechaEstudio: Date;
    descripcionEstudio: string;
    nombreEstudio: TipoEstudioComplementario;
    idPaciente: number;
}
