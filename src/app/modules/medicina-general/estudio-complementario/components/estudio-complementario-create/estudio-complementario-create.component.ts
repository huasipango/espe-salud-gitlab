import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import icClose from '@iconify/icons-ic/twotone-close';
import icMicroscope from '@iconify/icons-fa-solid/microscope';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {EstudioComplementario} from 'src/app/modules/medicina-general/estudio-complementario/models/estudio-complementario.model';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, startWith, switchMap} from 'rxjs/operators';
import {LoadingService} from 'src/app/core/services/loading.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TipoEstudioComplementario} from 'src/app/core/models/catalogo/tipo-estudio-complementario.model';
import {
  EstudioComplementarioService
} from 'src/app/modules/medicina-general/estudio-complementario/services/estudio-complementario.service';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';

@Component({
  selector: 'vex-estudio-complementario-create',
  templateUrl: './estudio-complementario-create.component.html',
  styleUrls: ['./estudio-complementario-create.component.scss'],
})
export class EstudioComplementarioCreateComponent implements OnInit {

  maxDate = new Date();

  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  filteredEstudiosCatalogo$: Observable<TipoEstudioComplementario[]>;
  tipoEstudiosCatalogo = new FormControl(
    (this.defaults ? (this.defaults.nombreEstudio ? this.defaults.nombreEstudio : null) : null),
    [Validators.required]
  );

  paciente: Paciente;
  icClose = icClose;
  icMicroscope = icMicroscope;
  icArrowDropDown = icArrowDropDown;
  errorMessages = FORM_ERROR_MESSAGES;
  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: any,
    private dialogRef: MatDialogRef<EstudioComplementarioCreateComponent>,
    private pacienteGlobalService: PacienteGlobalService,
    private estudioComplementarioService: EstudioComplementarioService,
    private loadingService: LoadingService,
    private catalogoService: CatalogoService,
    private fb: FormBuilder,
    private snackbar: MatSnackBar,
  ) {
  }

  ngOnInit(): void {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as EstudioComplementario;
    }
    this.filteredEstudiosCatalogo$ = this.catalogoService.getEstudiosComplementario();
    this.filteredEstudiosCatalogo$ = this.tipoEstudiosCatalogo.valueChanges
      .pipe(
        startWith(''),
        debounceTime(600),
        distinctUntilChanged(),
        switchMap(value => {
          return this.filterEstudioComplementario(value || '');
        })
      );
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.paciente = paciente;
          this.initializeForm();
        }
      });

  }

  initializeForm(): void {
    this.form = this.fb.group({
      id: this.defaults.id || null,
      nombreEstudio: this.tipoEstudiosCatalogo || null,
      fechaEstudio: [this.defaults.fechaEstudio || this.maxDate, Validators.required],
      descripcionEstudio: [this.defaults.descripcionEstudio || '', Validators.required],
      idPaciente: [this.defaults.idPaciente || this.paciente.id, Validators.required],
    });
  }

  filterEstudioComplementario(value: any): Observable<TipoEstudioComplementario[]> {
    value = typeof value === 'string' ? value : value.nombre;
    return this.catalogoService.getEstudiosComplementario()
      .pipe(
        map(response => response.filter(option => {
          return option.nombre.toLowerCase().indexOf(value.toLowerCase()) === 0;
        }))
      );
  }

  save() {
    const estudioComplementario: EstudioComplementario = this.form.value;
    if (typeof estudioComplementario.nombreEstudio === 'string') {
      estudioComplementario.nombreEstudio = {
        id: null,
        nombre: estudioComplementario.nombreEstudio,
      };
    }
    if (this.mode === 'create') {
      this.createEstudioComplementario(estudioComplementario);
    } else if (this.mode === 'update') {
      this.updateEstudioComplementario(estudioComplementario);
    }
  }

  createEstudioComplementario(estudioComplementario: EstudioComplementario) {
    this.loadingService.showLoaderUntilCompleted(
      this.estudioComplementarioService.createEstudioComplementario(estudioComplementario)
    ).subscribe((estudio) => {
      if (estudio) {
        this.dialogRef.close(estudio);
      }
    }, () => {
      this.showNotification('No se pudo crear el registro', 'CERRAR');
    });
  }

  updateEstudioComplementario(estudio: EstudioComplementario) {
    this.loadingService.showLoaderUntilCompleted(
      this.estudioComplementarioService.updateEstudioComplementario(estudio.id, estudio)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification('No se pudo actualizar el registro', 'CERRAR');
    });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  displayFn(estudio: TipoEstudioComplementario): string {
    return estudio && estudio.nombre ? estudio.nombre : '';
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }
}

