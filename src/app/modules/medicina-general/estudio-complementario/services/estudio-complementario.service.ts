import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import {Observable} from 'rxjs';
import {EstudioComplementario} from 'src/app/modules/medicina-general/estudio-complementario/models/estudio-complementario.model';

@Injectable({
  providedIn: 'root'
})
export class EstudioComplementarioService {
  private baseUrl: string = environment.baseUrl;
  constructor(private http: HttpClient) {
  }

  getEstudiosComplementarios(id: number): Observable<EstudioComplementario[]>{
    return this.http.get<EstudioComplementario[]>(this.baseUrl + 'estudios-complementarios' + '?idPaciente=' + id);
  }

  createEstudioComplementario(data: EstudioComplementario): Observable<EstudioComplementario>{
    return this.http.post<EstudioComplementario>(this.baseUrl + 'estudios-complementarios/', JSON.stringify(data));
  }

  updateEstudioComplementario(id: number, data: EstudioComplementario): Observable<EstudioComplementario>{
    return this.http.put<EstudioComplementario>(this.baseUrl + 'estudios-complementarios/' + id, JSON.stringify(data));
  }

  deleteEstudioComplementario(id: number): Observable<boolean>{
    return this.http.delete<boolean>(this.baseUrl + 'estudios-complementarios/' + id);
  }
}
