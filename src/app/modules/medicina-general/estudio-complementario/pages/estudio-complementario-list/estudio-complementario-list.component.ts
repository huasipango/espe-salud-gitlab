import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {FormControl} from '@angular/forms';
import {EstudioComplementario} from '../../models/estudio-complementario.model';
import {MatTableDataSource} from '@angular/material/table';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {DateUtil, getLongDate} from 'src/app/core/utils/date-utils';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {EstudioComplementarioService} from '../../services/estudio-complementario.service';
import {EstudioComplementarioCreateComponent} from '../../components/estudio-complementario-create/estudio-complementario-create.component';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';

import icNotesMedical from '@iconify/icons-fa-solid/notes-medical';
import icDownload from '@iconify/icons-ic/cloud-download';
import icPrint from '@iconify/icons-ic/print';
import icCheck from '@iconify/icons-fa-solid/clipboard-check';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {TipoEstudioComplementario} from 'src/app/core/models/catalogo/tipo-estudio-complementario.model';


@UntilDestroy()
@Component({
  selector: 'vex-estudio-complementario-list',
  templateUrl: './estudio-complementario-list.component.html',
  styleUrls: ['./estudio-complementario-list.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms,
    scaleFadeIn400ms,
    scaleIn400ms,
    fadeInRight400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})

export class EstudioComplementarioListComponent implements OnInit, AfterViewInit {

  estudioComplementarios: EstudioComplementario[] = [];

  @Input()
  columns: TableColumn<EstudioComplementario>[] = [
    {
      label: 'Nombre estudio',
      property: 'nombreEstudio',
      type: 'object',
      visible: true
    },
    {
      label: 'Fecha',
      property: 'fechaEstudio',
      type: 'date',
      visible: true
    },
    {
      label: 'Tipo de estudios, hallazgos y conclusiones',
      property: 'descripcionEstudio',
      type: 'text',
      visible: true,
      cssClasses: ['text-wrap']
    },
    {label: 'Actions', property: 'actions', type: 'button', visible: true}
  ];

  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<EstudioComplementario> | null;
  searchCtrl = new FormControl();

  icEdit = icEdit;
  icSearch = icSearch;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icMoreHoriz = icMoreHoriz;
  icNotesMedical = icNotesMedical;
  icDownload = icDownload;
  icPrint = icPrint;
  icCheck = icCheck;

  pacienteActual: Paciente;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private catalogoService: CatalogoService,
    private estudioComplementarioService: EstudioComplementarioService,
    private pacienteGlobalService: PacienteGlobalService,
  ) {
  }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getEstudiosComplementario();
        }
      });
    this.searchCtrl.valueChanges.pipe(
      untilDestroyed(this)
    ).subscribe(value => this.onFilterChange(value));
  }

  getEstudiosComplementario(): void {
    this.estudioComplementarioService
      .getEstudiosComplementarios(this.pacienteActual.id)
      .subscribe((data: EstudioComplementario[]) => {
          this.estudioComplementarios = data;
          this.dataSource.data = this.estudioComplementarios;
        });
  }

  createEstudioComplementario() {
    this.dialog.open(EstudioComplementarioCreateComponent, {
      width: '600px',
      maxWidth: '100%',
      disableClose: true
    })
      .afterClosed()
      .subscribe((estudio: EstudioComplementario) => {
        if (estudio) {
          this.showNotification('Registro creado EXITOSAMENTE', 'OK');
          this.getEstudiosComplementario();
        }
      });
  }

  updateEstudioComplementario(estudioComplementario: EstudioComplementario) {
    this.dialog.open(EstudioComplementarioCreateComponent, {
      data: estudioComplementario,
      maxWidth: '100%',
      width: '600px',
      disableClose: true
    }).afterClosed().subscribe(updatedEstudioComplementario => {
      if (updatedEstudioComplementario) {
        this.showNotification('Registro actualizado EXITOSAMENTE', 'OK');
        this.getEstudiosComplementario();
      }
    });
  }

  deleteEstudioComplementario(estudio: EstudioComplementario) {
    this.estudioComplementarioService.deleteEstudioComplementario(estudio.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification('Registro eliminado EXITOSAMENTE', 'CERRAR');
          this.estudioComplementarios.splice(
            this.estudioComplementarios.findIndex((existing) =>
              existing.id === estudio.id), 1
          );
          this.dataSource.connect().next(this.estudioComplementarios);
        } else {
          this.showNotification('No se pudo eliminar el registro', 'CERRAR');
        }
      });
  }

  openDeleteDialog(estudio: EstudioComplementario) {
    this.dialog.open(DeleteModalComponent, {
      data: '¿Estas seguro de eliminar este registro?',
      disableClose: false,
      width: '400px',
    }).afterClosed().subscribe(result => {
      if (result === 'si') { this.deleteEstudioComplementario(estudio); }
    });
  }

  getFormattedDate(date: string) {
    return DateUtil.showDateFormat(date);
  }

  onFilterChange(value: string) {
    if (!this.dataSource) {
      return;
    }
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  print() {
  }

  openSearchModal(){
  }

  getDate(date: Date): string {
    return getLongDate(date);
  }

  getNombreEstudio(estudio: TipoEstudioComplementario): string {
    return estudio.nombre;
  }
}
