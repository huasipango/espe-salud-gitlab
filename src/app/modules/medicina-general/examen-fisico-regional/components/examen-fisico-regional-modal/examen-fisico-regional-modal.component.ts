import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Region} from 'src/app/modules/medicina-general/evolucion/models/region.model';
import {Area} from 'src/app/modules/medicina-general/evolucion/models/area.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {ExamenExterno} from 'src/app/modules/medicina-general/examen-fisico-regional/models/examen-externo.model';
import icCheck from '@iconify/icons-ic/check';
import icClose from '@iconify/icons-ic/twotone-close';
import {Observable} from 'rxjs';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {LoadingService} from 'src/app/core/services/loading.service';
import {ExamenInternoService} from 'src/app/modules/medicina-general/organos-sistemas/services/examen-interno.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ExamenExternoService} from 'src/app/modules/medicina-general/examen-fisico-regional/services/examen-externo.service';
import {ExamenInterno} from 'src/app/modules/medicina-general/organos-sistemas/models/examen-interno.models';

@Component({
  selector: 'vex-examen-fisico-regional-modal',
  templateUrl: './examen-fisico-regional-modal.component.html',
  styleUrls: ['./examen-fisico-regional-modal.component.scss']
})
export class ExamenFisicoRegionalModalComponent implements OnInit {

  form: FormGroup;
  mode: 'create' | 'update' = 'create';

  icClose = icClose;
  icCheck = icCheck;
  regiones: Observable<Region[]>;
  areas: Observable<Area[]>;
  pacienteActual: Paciente;

  constructor(
    private dialogRef: MatDialogRef<ExamenFisicoRegionalModalComponent>,
    @Inject(MAT_DIALOG_DATA) public defaults: ExamenExterno,
    private fb: FormBuilder,
    protected catalogoService: CatalogoService,
    private pacienteGlobalService: PacienteGlobalService,
    private loadingService: LoadingService,
    private examenExternoService: ExamenExternoService,
    private snackBar: MatSnackBar
  ) {
  }

  ngOnInit(): void {
    if (this.defaults) {
      this.mode = 'update';
      this.areas = this.catalogoService.getAreasByRegion(this.defaults.idRegion);
    } else {
      this.defaults = {} as ExamenExterno;
    }
    this.regiones = this.catalogoService.getRegiones();
    this.form = this.fb.group({
      id: this.defaults.id || null,
      descripcion: [this.defaults.descripcion || '', Validators.required],
      idRegion: [this.defaults.idRegion || null, Validators.required],
      idArea: [this.defaults.idArea || null, Validators.required],
      idPaciente: [this.defaults.idPaciente || null, Validators.required]
    });
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.form.patchValue({idPaciente: paciente.id});
        }
      });
  }

  selectRegion() {
    const idRegion = this.form.get('idRegion').value;
    this.areas = this.catalogoService.getAreasByRegion(idRegion);
  }

  save() {
    const examen: ExamenExterno = this.form.value;
    if (this.mode === 'create') {
      this.createExamen(examen);
    } else if (this.mode === 'update') {
      this.updateExamen(examen);
    }
  }

  createExamen(examen: ExamenExterno): void {
    this.loadingService.showLoaderUntilCompleted(
      this.examenExternoService.createExamenExterno(examen)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification('No se pudo crear el registro', 'CERRAR');
    });
  }

  updateExamen(examen: ExamenExterno): void {
    this.loadingService.showLoaderUntilCompleted(
      this.examenExternoService.updateExamenExterno(examen.id, examen)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification('No se pudo actualizar el registro', 'CERRAR');
    });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  showNotification(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000
    });
  }
}
