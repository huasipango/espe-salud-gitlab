import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {ExamenExterno} from 'src/app/modules/medicina-general/examen-fisico-regional/models/examen-externo.model';
import {MatTableDataSource} from '@angular/material/table';
import {FormControl} from '@angular/forms';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icDownload from '@iconify/icons-ic/cloud-download';
import icBody from '@iconify/icons-fa-solid/child';
import icPrint from '@iconify/icons-ic/twotone-print';

import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {DateUtil} from 'src/app/core/utils/date-utils';
import {ExamenExternoService} from 'src/app/modules/medicina-general/examen-fisico-regional/services/examen-externo.service';
import {ExamenFisicoRegionalModalComponent} from 'src/app/modules/medicina-general/examen-fisico-regional/components/examen-fisico-regional-modal/examen-fisico-regional-modal.component';
import {Region} from 'src/app/modules/medicina-general/evolucion/models/region.model';
import {Area} from 'src/app/modules/medicina-general/evolucion/models/area.model';

@UntilDestroy()
@Component({
  selector: 'vex-examen-fisico-regional',
  templateUrl: './examen-fisico-regional.component.html',
  styleUrls: ['./examen-fisico-regional.component.scss'],
  animations: [
    scaleIn400ms,
    scaleFadeIn400ms,
    stagger40ms,
    fadeInRight400ms,
    fadeInUp400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class ExamenFisicoRegionalComponent implements OnInit, AfterViewInit {
  examenesExternos: ExamenExterno[];

  columns: TableColumn<ExamenExterno>[] = [
    {label: 'Región', property: 'region', type: 'object', visible: true},
    {label: 'Área', property: 'area', type: 'object', visible: true},
    {label: 'Fecha registro', property: 'fechaRegistro', type: 'datetime', visible: true},
    {label: 'Descripción', property: 'descripcion', type: 'text', visible: true, cssClasses: ['text-wrap']},
    {label: 'Acciones', property: 'actions', type: 'button', visible: true}
  ];
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<ExamenExterno> | null;
  searchCtrl = new FormControl();

  icEdit = icEdit;
  icSearch = icSearch;
  icMoreHoriz = icMoreHoriz;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icDownload = icDownload;
  pacienteActual: Paciente;
  icBody = icBody;
  icPrint = icPrint;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private catalogoService: CatalogoService,
    private examenExternoService: ExamenExternoService,
    private pacienteGlobalService: PacienteGlobalService,
  ) {
  }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getExamenesExternos();
        }
      });
    this.searchCtrl.valueChanges.pipe(
      untilDestroyed(this)
    ).subscribe(value => this.onFilterChange(value));
  }

  getExamenesExternos(): void {
    this.examenExternoService.getExamenesExternos(this.pacienteActual.id)
      .subscribe((data: ExamenExterno[]) => {
        this.examenesExternos = data;
        this.dataSource.data = this.examenesExternos;
      });
  }

  createExamenExterno(): void {
    this.dialog.open(ExamenFisicoRegionalModalComponent, {
      width: '600px',
      maxWidth: '100%',
      disableClose: true
    }).afterClosed()
      .subscribe((examen: ExamenExterno) => {
        if (examen) {
          this.showNotification('Registro creado EXITOSAMENTE', 'OK');
          this.examenesExternos.push(examen);
          this.dataSource.connect().next(this.examenesExternos);
        }
      });
  }

  updateExamenExterno(examen: ExamenExterno): void {
    this.dialog.open(ExamenFisicoRegionalModalComponent, {
      data: examen,
      maxWidth: '100%',
      width: '600px',
      disableClose: true
    }).afterClosed()
      .subscribe((updated) => {
        if (updated) {
          this.showNotification('Registro actualizado EXITOSAMENTE', 'OK');
          const id = examen.id;
          const index = this.examenesExternos.findIndex(
            (existing) => existing.id === id
          );
          this.examenesExternos[index] = updated;
          this.dataSource.connect().next(this.examenesExternos);
        }
      });
  }

  deleteExamenExterno(examen: ExamenExterno): void {
    this.examenExternoService.deleteExamenExterno(examen.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification('Registro eliminado EXITOSAMENTE', 'CERRAR');
          this.examenesExternos.splice(
            this.examenesExternos.findIndex((existing) =>
              existing.id === examen.id), 1
          );
          this.dataSource.connect().next(this.examenesExternos);
        } else {
          this.showNotification('No se pudo eliminar el registro', 'CERRAR');
        }
      });
  }

  openDeleteDialog(examen: ExamenExterno): void {
    this.dialog.open(DeleteModalComponent, {
      data: '¿Estas seguro de eliminar este registro?',
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') {
        this.deleteExamenExterno(examen);
      }
    });
  }

  getFormattedDateTime(date: string) {
    return DateUtil.showDateTimeFormat(date);
  }

  onFilterChange(value: string) {
    if (!this.dataSource) {
      return;
    }
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  print() {
  }

  openSearchModal() {
  }

  getRegion(region: Region): string {
    return region.nombre;
  }

  getArea(area: Area): string {
    return area.nombre;
  }
}
