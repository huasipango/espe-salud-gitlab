import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from 'src/@vex/interfaces/vex-route.interface';
import {ExamenFisicoRegionalComponent} from 'src/app/modules/medicina-general/examen-fisico-regional/examen-fisico-regional.component';

const routes: VexRoutes = [
  {
    path: '',
    component: ExamenFisicoRegionalComponent,
    data: {
      toolbarShadowEnabled: true,
      containerEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExamenFisicoRegionalRoutingModule { }
