import {Region} from 'src/app/modules/medicina-general/evolucion/models/region.model';
import {Area} from 'src/app/modules/medicina-general/evolucion/models/area.model';

export class ExamenExterno {
  id: number;
  descripcion: string;
  idRegion: number;
  idArea: number;
  region: Region;
  area: Area;
  idPaciente: number;
}
