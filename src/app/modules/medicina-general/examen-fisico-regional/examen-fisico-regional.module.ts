import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExamenFisicoRegionalComponent } from './examen-fisico-regional.component';
import {ExamenFisicoRegionalRoutingModule} from 'src/app/modules/medicina-general/examen-fisico-regional/examen-fisico-regional-routing.module';
import { ExamenFisicoRegionalModalComponent } from './components/examen-fisico-regional-modal/examen-fisico-regional-modal.component';
import {EmptyPacienteModule} from 'src/app/core/components/empty-paciente/empty-paciente.module';
import {IconModule} from '@visurel/iconify-angular';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {ExtendedModule, FlexModule} from '@angular/flex-layout';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';

@NgModule({
  declarations: [ExamenFisicoRegionalComponent, ExamenFisicoRegionalModalComponent],
  imports: [
    CommonModule,
    ExamenFisicoRegionalRoutingModule,
    EmptyPacienteModule,
    IconModule,
    ContainerModule,
    FlexModule,
    BreadcrumbsModule,
    ExtendedModule,
    MatButtonModule,
    MatTooltipModule,
    MatIconModule,
    ReactiveFormsModule,
    MatMenuModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatCheckboxModule,
    FormsModule,
    MatDialogModule,
    MatInputModule,
    MatSelectModule,
    LoadingModule
  ]
})
export class ExamenFisicoRegionalModule { }
