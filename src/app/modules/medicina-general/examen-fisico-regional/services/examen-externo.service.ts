import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ExamenExterno} from 'src/app/modules/medicina-general/examen-fisico-regional/models/examen-externo.model';

@Injectable({
  providedIn: 'root'
})
export class ExamenExternoService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getExamenesExternos(idPaciente: number): Observable<ExamenExterno[]>{
    return this.http.get<ExamenExterno[]>(this.baseUrl + 'examenes-externos?idPaciente=' + idPaciente);
  }

  createExamenExterno(data: ExamenExterno): Observable<ExamenExterno> {
    return this.http.post<ExamenExterno>(this.baseUrl + 'examenes-externos/', JSON.stringify(data));
  }

  updateExamenExterno(id: number, data: ExamenExterno): Observable<ExamenExterno> {
    return this.http.put<ExamenExterno>(this.baseUrl + 'examenes-externos/' + id, JSON.stringify(data));
  }

  deleteExamenExterno(id: number): Observable<boolean> {
    return this.http.delete<boolean>(this.baseUrl + 'examenes-externos/' + id);
  }
}
