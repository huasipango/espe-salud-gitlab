import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {Component, HostListener, Input, OnInit} from '@angular/core';
import {MatIconRegistry} from "@angular/material/icon";
import {DomSanitizer} from "@angular/platform-browser";
import {FormControl} from '@angular/forms';

import {PiezaPermanente} from '../../graphic-models/pieza-permanente';
import {PiezaDecidua} from '../..//graphic-models/pieza-decidua';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';

import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';

import icSave from '@iconify/icons-ic/save';
import {OdontogramaService} from '../../services/odontograma.service';
import {AdaptadorGraficoModelos} from './adaptador-grafico-modelos';
import {ProtesisTotal} from '../../graphic-models/protesis-total';
import {Protesis} from '../../graphic-models/protesis';
import {ProtesisFija} from '../../graphic-models/protesis-fija';
import {ProtesisCorona} from '../../graphic-models/protesis-corona';
import {ProtesisRemovible} from '../../graphic-models/protesis-removible';

declare const SVG: any;

@Component({
  selector: 'vex-odontograma',
  templateUrl: './odontograma.component.html',
  styleUrls: ['./odontograma.component.scss'],
  animations: [
    fadeInUp400ms,
    fadeInRight400ms,
    scaleIn400ms,
    stagger40ms,
    scaleFadeIn400ms
  ]
})


export class OdontogramaComponent implements OnInit {

  layoutCtrl = new FormControl('full');

  public cursor = '';
  public lienzo;
  /*Para acceder a una pieza se usa piezasPermanentes[numero de pieza], de igual forma para piezasDeciduas*/
  public piezasPermanentes: PiezaPermanente[] = [];
  public piezasDeciduas: PiezaDecidua[] = [];

  public protesis: Protesis[] = [];//detalles graficos compuestos aún no implementado

  /*Para acceder a los índices de movilidad o recesión se usa indicesMovilidad[numero de pieza]*/
  @Input() public indicesMovilidad: number[] = [];
  @Input() public indicesRecesion: number[] = [];

  /**Para gestionar cada sección de piezas como si fuera un solo gráfico**/
  public referencia_grafica_permanentes_superiores: any;
  public referencia_grafica_permanentes_inferiores: any;

  public referencia_grafica_deciduas_superiores: any;
  public referencia_grafica_deciduas_inferiores: any;

  /*Mensaje Finalizar Cancelar Protesis*/
  public mensaje_finalizar_cancelar: any;

  /**Auxiliar para crear protesis fija**/
  public auxiliar_protesis_fija: ProtesisFija = null;

  /**Auxiliar para crear protesis removible**/
  public auxiliar_protesis_removible: ProtesisRemovible = null;
  /**Auxiliar para crear protesis corona**/
  public auxiliar_protesis_corona: ProtesisCorona = null;


  public static detalle_a_aplicar = '';

  icon_data: any[] = [
    {name: 'Sellante', description: 'Aplicado', icon: 'sellante_aplicado'},
    {name: 'Sellante', description: 'Necesario', icon: 'sellante_necesario'},
    {name: 'Extraccion', description: 'Necesaria', icon: 'extraccion_necesaria'},
    {name: 'Perdida', description: 'Por caries', icon: 'perdida_caries'},
    {name: 'Perdida', description: 'Otra causa', icon: 'perdida_otra_causa'},
    {name: 'Endodoncia', description: 'Aplicada', icon: 'endodoncia_aplicada'},
    {name: 'Endodoncia', description: 'Necesaria', icon: 'endodoncia_necesaria'},
    {name: 'Protesis Fija', description: 'Aplicada', icon: 'protesis_fija_aplicada'},
    {name: 'Protesis Fija', description: 'Necesaria', icon: 'protesis_fija_necesaria'},
    {name: 'Protesis Removible', description: 'Aplicado', icon: 'protesis_removible_aplicada'},
    {name: 'Protesis Removible', description: 'Necesaria', icon: 'protesis_removible_necesaria'},
    {name: 'Protesis Total', description: 'Aplicado', icon: 'protesis_total_aplicada'},
    {name: 'Protesis Total', description: 'Necesaria', icon: 'protesis_total_necesaria'},
    {name: 'Corona', description: 'Aplicada', icon: 'protesis_corona_aplicada'},
    {name: 'Corona', description: 'Necesaria', icon: 'protesis_corona_necesaria'},
    {name: 'Caries', description: '', icon: 'caries'},
    {name: 'Obturacion', description: '', icon: 'obturacion'},
    {name: 'Borrar', description: '', icon: 'borrar'}
  ];

  icons = ['caries', 'protesis_corona_aplicada', 'protesis_corona_necesaria', 'endodoncia_aplicada', 'endodoncia_necesaria', 'extraccion_necesaria', 'obturacion', 'perdida_caries', 'perdida_otra_causa', 'protesis_fija_aplicada', 'protesis_fija_necesaria', 'protesis_removible_aplicada', 'protesis_removible_necesaria', 'protesis_total_aplicada', 'protesis_total_necesaria', 'sellante_aplicado', 'sellante_necesario', 'borrar'];
  icSave = icSave;
  pacienteActual: Paciente;
  nombreCompleto: string;

  // Indices
  C: number = 0;
  P: number = 0;
  O: number = 0;
  c: number = 0;
  e: number = 0;
  o: number = 0;

  constructor(private matIconRegistry: MatIconRegistry,
              private domSanitizer: DomSanitizer,
              private pacienteGlobalService: PacienteGlobalService, private servicioOdontograma: OdontogramaService) {
    this.icons.forEach(icon => {
      this.pacienteGlobalService.pacienteGlobal$
        .subscribe((paciente: Paciente) => {
          if (paciente && (this.pacienteActual == null || paciente.id !== this.pacienteActual.id)) {
            this.pacienteActual = paciente;
            this.nombreCompleto = this.nombreCompleto = this.pacienteActual.apellidoPaterno.toUpperCase() + ' ' + this.pacienteActual.primerNombre.toUpperCase();
            this.obtenerOdontograma();
          }
        });

      this.matIconRegistry.addSvgIcon(
        icon,
        this.domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/svg-icon/' + icon + '.svg')
      );
    });
  }

  ngOnInit(): void {

    this.lienzo = SVG('lienzo').size(940, 375);//se pasa el id del lienzo
    this.agruparPiezasPermanentes();
    let aux_posicion_x = 6;
    let aux_posicion_y = 0;

    //Permanentes 18-11
    for (let index = 18; index >= 11; index--) {
      this.indicesMovilidad[index] = 0
      this.indicesRecesion[index] = 0

      let pieza = new PiezaPermanente(this.lienzo, index, this);
      pieza.posicion_x = aux_posicion_x
      pieza.posicion_y = aux_posicion_y
      this.piezasPermanentes[index] = pieza;
      aux_posicion_x += 57
      this.referencia_grafica_permanentes_superiores.add(pieza.referencia_grafico_svg);


    }
    aux_posicion_x += 30
    //Permanentes 21-28
    for (let index = 21; index <= 28; index++) {
      this.indicesMovilidad[index] = 0
      this.indicesRecesion[index] = 0

      let pieza = new PiezaPermanente(this.lienzo, index, this);
      pieza.posicion_x = aux_posicion_x
      pieza.posicion_y = aux_posicion_y
      this.piezasPermanentes[index] = pieza;
      aux_posicion_x += 57
      this.referencia_grafica_permanentes_superiores.add(pieza.referencia_grafico_svg);

    }


    aux_posicion_y += 100
    aux_posicion_x = 178

    //Deciduas 55-51
    for (let index = 55; index >= 51; index--) {
      this.indicesMovilidad[index] = 0
      this.indicesRecesion[index] = 0

      let pieza = new PiezaDecidua(this.lienzo, index, this);
      pieza.posicion_x = aux_posicion_x
      pieza.posicion_y = aux_posicion_y
      this.piezasDeciduas[index] = pieza;
      aux_posicion_x += 57
      this.referencia_grafica_deciduas_superiores.add(pieza.referencia_grafico_svg);
    }
    aux_posicion_x += 31

    //Deciduas 61-65
    for (let index = 61; index <= 65; index++) {
      this.indicesMovilidad[index] = 0
      this.indicesRecesion[index] = 0

      let pieza = new PiezaDecidua(this.lienzo, index, this);
      pieza.posicion_x = aux_posicion_x
      pieza.posicion_y = aux_posicion_y
      this.piezasDeciduas[index] = pieza;
      aux_posicion_x += 57
      this.referencia_grafica_deciduas_superiores.add(pieza.referencia_grafico_svg);

    }

    aux_posicion_y += 100
    aux_posicion_x = 178

    //Deciduas 85-81
    for (let index = 85; index >= 81; index--) {
      this.indicesMovilidad[index] = 0
      this.indicesRecesion[index] = 0

      let pieza = new PiezaDecidua(this.lienzo, index, this);
      pieza.posicion_x = aux_posicion_x
      pieza.posicion_y = aux_posicion_y
      this.piezasDeciduas[index] = pieza;
      aux_posicion_x += 57
      this.referencia_grafica_deciduas_inferiores.add(pieza.referencia_grafico_svg);
    }
    aux_posicion_x += 31
    //Deciduas 71-75
    for (let index = 71; index <= 75; index++) {
      this.indicesMovilidad[index] = 0
      this.indicesRecesion[index] = 0

      let pieza = new PiezaDecidua(this.lienzo, index, this);
      pieza.posicion_x = aux_posicion_x
      pieza.posicion_y = aux_posicion_y
      this.piezasDeciduas[index] = pieza;
      aux_posicion_x += 57
      this.referencia_grafica_deciduas_inferiores.add(pieza.referencia_grafico_svg);

    }

    aux_posicion_y += 100
    aux_posicion_x = 6

    //Permanentes 48-41
    for (let index = 48; index >= 41; index--) {
      this.indicesMovilidad[index] = 0
      this.indicesRecesion[index] = 0

      let pieza = new PiezaPermanente(this.lienzo, index, this);
      pieza.posicion_x = aux_posicion_x
      pieza.posicion_y = aux_posicion_y
      this.piezasPermanentes[index] = pieza;
      aux_posicion_x += 57
      this.referencia_grafica_permanentes_inferiores.add(pieza.referencia_grafico_svg);

    }
    aux_posicion_x += 30
    //Permanentes 31-38
    for (let index = 31; index <= 38; index++) {
      this.indicesMovilidad[index] = 0
      this.indicesRecesion[index] = 0

      let pieza = new PiezaPermanente(this.lienzo, index, this);
      pieza.posicion_x = aux_posicion_x
      pieza.posicion_y = aux_posicion_y
      this.piezasPermanentes[index] = pieza;
      aux_posicion_x += 57
      this.referencia_grafica_permanentes_inferiores.add(pieza.referencia_grafico_svg);

    }

    this.mensaje_finalizar_cancelar = this.lienzo.image('../../../../assets/svg-icon/opciones.png')
    this.mensaje_finalizar_cancelar.scale(0.3)
    this.mensaje_finalizar_cancelar.translate(800, 110)
    this.mensaje_finalizar_cancelar.opacity(0)

  }

  private agruparPiezasPermanentes() {//se agrupan las piezas permanentes para facilitar la colocación de protesis total
    //AGRUPANDO SECCIONS GRAFICAS PARA EFECTOS VISUALES DE PRÓTESIS TOTAL
    this.referencia_grafica_permanentes_superiores = this.lienzo.group();
    this.referencia_grafica_deciduas_superiores = this.lienzo.group();
    this.referencia_grafica_deciduas_inferiores = this.lienzo.group();
    this.referencia_grafica_permanentes_inferiores = this.lienzo.group();


    this.referencia_grafica_permanentes_superiores.on('mouseenter', (evento) => {
      if (OdontogramaComponent.detalle_a_aplicar == 'protesis_total_necesaria' || OdontogramaComponent.detalle_a_aplicar == 'protesis_total_aplicada') {
        this.piezasPermanentes[17].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[16].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[15].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[14].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[13].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[12].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[11].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[21].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[22].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[23].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[24].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[25].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[26].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[27].referencia_grafico_svg.fire('mouseenter');
      }

    });
    this.referencia_grafica_permanentes_superiores.on('mouseleave', (evento) => {
      if (OdontogramaComponent.detalle_a_aplicar == 'protesis_total_necesaria' || OdontogramaComponent.detalle_a_aplicar == 'protesis_total_aplicada') {
        this.piezasPermanentes[17].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[16].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[15].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[14].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[13].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[12].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[11].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[21].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[22].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[23].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[24].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[25].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[26].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[27].referencia_grafico_svg.fire('mouseleave');
      }
    });
    this.referencia_grafica_permanentes_inferiores.on('mouseenter', (evento) => {
      if (OdontogramaComponent.detalle_a_aplicar == 'protesis_total_necesaria' || OdontogramaComponent.detalle_a_aplicar == 'protesis_total_aplicada') {
        this.piezasPermanentes[47].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[46].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[45].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[44].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[43].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[42].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[41].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[31].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[32].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[33].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[34].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[35].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[36].referencia_grafico_svg.fire('mouseenter');
        this.piezasPermanentes[37].referencia_grafico_svg.fire('mouseenter');
      }
    });
    this.referencia_grafica_permanentes_inferiores.on('mouseleave', (evento) => {
      if (OdontogramaComponent.detalle_a_aplicar == 'protesis_total_necesaria' || OdontogramaComponent.detalle_a_aplicar == 'protesis_total_aplicada') {
        this.piezasPermanentes[47].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[46].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[45].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[44].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[43].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[42].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[41].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[31].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[32].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[33].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[34].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[35].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[36].referencia_grafico_svg.fire('mouseleave');
        this.piezasPermanentes[37].referencia_grafico_svg.fire('mouseleave');
      }

    });
    this.referencia_grafica_permanentes_inferiores.on('click', (evento) => {
      if (OdontogramaComponent.detalle_a_aplicar == "protesis_total_necesaria") {
        if (this.piezasPermanentes[47].protesis_total_necesaria) {//solo es necesario verificar el primer elemento ya que de contenerlo, contendrá a los demás
          return;
        } else if (this.piezasPermanentes[47].protesis_total_aplicada) {//solo es necesario verificar el primer elemento ya que de contenerlo, contendrá a los demás
          this.protesis.forEach((protesis, index) => {
            if (protesis instanceof ProtesisTotal) {
              if (protesis.piezas.includes(this.piezasPermanentes[47])) {//solo es necesario verificar el primer elemento ya que de contenerlo, contendrá a los demás
                this.protesis.splice(index, 1);//quitar
              }
            }
          })
        }
        let protesis_total = new ProtesisTotal(this.lienzo, this);
        protesis_total.aplicada = false;
        protesis_total.AgregarPieza(this.piezasPermanentes[47]);
        protesis_total.AgregarPieza(this.piezasPermanentes[46]);
        protesis_total.AgregarPieza(this.piezasPermanentes[45]);
        protesis_total.AgregarPieza(this.piezasPermanentes[44]);
        protesis_total.AgregarPieza(this.piezasPermanentes[43]);
        protesis_total.AgregarPieza(this.piezasPermanentes[42]);
        protesis_total.AgregarPieza(this.piezasPermanentes[41]);
        protesis_total.AgregarPieza(this.piezasPermanentes[31]);
        protesis_total.AgregarPieza(this.piezasPermanentes[32]);
        protesis_total.AgregarPieza(this.piezasPermanentes[33]);
        protesis_total.AgregarPieza(this.piezasPermanentes[34]);
        protesis_total.AgregarPieza(this.piezasPermanentes[35]);
        protesis_total.AgregarPieza(this.piezasPermanentes[36]);
        protesis_total.AgregarPieza(this.piezasPermanentes[37]);
        this.protesis.push(protesis_total);
        this.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this))


      } else if (OdontogramaComponent.detalle_a_aplicar == "protesis_total_aplicada") {
        if (this.piezasPermanentes[47].protesis_total_aplicada) {//solo es necesario verificar el primer elemento ya que de contenerlo, contendrá a los demás
          return;
        } else if (this.piezasPermanentes[47].protesis_total_necesaria) {//solo es necesario verificar el primer elemento ya que de contenerlo, contendrá a los demás
          this.protesis.forEach((protesis, index) => {
            if (protesis instanceof ProtesisTotal) {
              if (protesis.piezas.includes(this.piezasPermanentes[47])) {//solo es necesario verificar el primer elemento ya que de contenerlo, contendrá a los demás
                this.protesis.splice(index, 1);//quitar
              }
            }
          })
        }
        let protesis_total = new ProtesisTotal(this.lienzo, this);
        protesis_total.aplicada = true;
        protesis_total.AgregarPieza(this.piezasPermanentes[47]);
        protesis_total.AgregarPieza(this.piezasPermanentes[46]);
        protesis_total.AgregarPieza(this.piezasPermanentes[45]);
        protesis_total.AgregarPieza(this.piezasPermanentes[44]);
        protesis_total.AgregarPieza(this.piezasPermanentes[43]);
        protesis_total.AgregarPieza(this.piezasPermanentes[42]);
        protesis_total.AgregarPieza(this.piezasPermanentes[41]);
        protesis_total.AgregarPieza(this.piezasPermanentes[31]);
        protesis_total.AgregarPieza(this.piezasPermanentes[32]);
        protesis_total.AgregarPieza(this.piezasPermanentes[33]);
        protesis_total.AgregarPieza(this.piezasPermanentes[34]);
        protesis_total.AgregarPieza(this.piezasPermanentes[35]);
        protesis_total.AgregarPieza(this.piezasPermanentes[36]);
        protesis_total.AgregarPieza(this.piezasPermanentes[37]);
        this.protesis.push(protesis_total);
        this.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this))

      }
    });


    this.referencia_grafica_permanentes_superiores.on('click', (evento) => {
      if (OdontogramaComponent.detalle_a_aplicar == "protesis_total_necesaria") {
        if (this.piezasPermanentes[17].protesis_total_necesaria) {//solo es necesario verificar el primer elemento ya que de contenerlo, contendrá a los demás
          return;
        } else if (this.piezasPermanentes[17].protesis_total_aplicada) {//solo es necesario verificar el primer elemento ya que de contenerlo, contendrá a los demás
          this.protesis.forEach((protesis, index) => {
            if (protesis instanceof ProtesisTotal) {
              if (protesis.piezas.includes(this.piezasPermanentes[17])) {//solo es necesario verificar el primer elemento ya que de contenerlo, contendrá a los demás
                this.protesis.splice(index, 1);//quitar
              }
            }
          })
        }
        let protesis_total = new ProtesisTotal(this.lienzo, this);
        protesis_total.aplicada = false;
        protesis_total.AgregarPieza(this.piezasPermanentes[17]);
        protesis_total.AgregarPieza(this.piezasPermanentes[16]);
        protesis_total.AgregarPieza(this.piezasPermanentes[15]);
        protesis_total.AgregarPieza(this.piezasPermanentes[14]);
        protesis_total.AgregarPieza(this.piezasPermanentes[13]);
        protesis_total.AgregarPieza(this.piezasPermanentes[12]);
        protesis_total.AgregarPieza(this.piezasPermanentes[11]);
        protesis_total.AgregarPieza(this.piezasPermanentes[21]);
        protesis_total.AgregarPieza(this.piezasPermanentes[22]);
        protesis_total.AgregarPieza(this.piezasPermanentes[23]);
        protesis_total.AgregarPieza(this.piezasPermanentes[24]);
        protesis_total.AgregarPieza(this.piezasPermanentes[25]);
        protesis_total.AgregarPieza(this.piezasPermanentes[26]);
        protesis_total.AgregarPieza(this.piezasPermanentes[27]);
        this.protesis.push(protesis_total);
        this.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this))


      } else if (OdontogramaComponent.detalle_a_aplicar == "protesis_total_aplicada") {
        let protesis_total = new ProtesisTotal(this.lienzo, this);
        if (this.piezasPermanentes[17].protesis_total_aplicada) {//solo es necesario verificar el primer elemento ya que de contenerlo, contendrá a los demás
          return;
        } else if (this.piezasPermanentes[17].protesis_total_necesaria) {//solo es necesario verificar el primer elemento ya que de contenerlo, contendrá a los demás
          this.protesis.forEach((protesis, index) => {
            if (protesis instanceof ProtesisTotal) {
              if (protesis.piezas.includes(this.piezasPermanentes[17])) {//solo es necesario verificar el primer elemento ya que de contenerlo, contendrá a los demás
                this.protesis.splice(index, 1);//quitar
              }
            }
          })
        }
        protesis_total.aplicada = true;
        protesis_total.AgregarPieza(this.piezasPermanentes[17]);
        protesis_total.AgregarPieza(this.piezasPermanentes[16]);
        protesis_total.AgregarPieza(this.piezasPermanentes[15]);
        protesis_total.AgregarPieza(this.piezasPermanentes[14]);
        protesis_total.AgregarPieza(this.piezasPermanentes[13]);
        protesis_total.AgregarPieza(this.piezasPermanentes[12]);
        protesis_total.AgregarPieza(this.piezasPermanentes[11]);
        protesis_total.AgregarPieza(this.piezasPermanentes[21]);
        protesis_total.AgregarPieza(this.piezasPermanentes[22]);
        protesis_total.AgregarPieza(this.piezasPermanentes[23]);
        protesis_total.AgregarPieza(this.piezasPermanentes[24]);
        protesis_total.AgregarPieza(this.piezasPermanentes[25]);
        protesis_total.AgregarPieza(this.piezasPermanentes[26]);
        protesis_total.AgregarPieza(this.piezasPermanentes[27]);
        this.protesis.push(protesis_total);
        this.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this))

      }
    });

  }

  seleccionarDetalle(detalle_seleccionado): void {
    /*Quitar detalles temporales (protesis q no se terminaron de armar)*/
    if (this.auxiliar_protesis_fija) {
      this.auxiliar_protesis_fija.referencia_grafico_svg.fire('click');
    }
    if (this.auxiliar_protesis_removible) {
      this.auxiliar_protesis_removible.referencia_grafico_svg.fire('click');
    }


    if (OdontogramaComponent.detalle_a_aplicar == detalle_seleccionado) {
      OdontogramaComponent.detalle_a_aplicar = ''
      this.cursor = '';
    } else {
      OdontogramaComponent.detalle_a_aplicar = detalle_seleccionado;
      this.cursor = 'cursor: url("../../../../assets/svg-cursor/' + detalle_seleccionado + '.svg"),auto;';
    }
  }

  limpiarOdontograma() {
    this.piezasDeciduas.forEach((pieza) => {
      pieza.QuitarTodosDetalles();
    });
    this.piezasPermanentes.forEach((pieza) => {
      pieza.QuitarTodosDetalles();
    });

    while (this.protesis.length != 0 && this.protesis != null) {
      this.protesis.pop().referencia_grafico_svg.fire('click');
    }
    if (this.auxiliar_protesis_corona != null) {
      this.auxiliar_protesis_corona.referencia_grafico_svg.fire('çlick');
    }
    if (this.auxiliar_protesis_fija != null) {
      this.auxiliar_protesis_fija.referencia_grafico_svg.fire('çlick');
    }
    if (this.auxiliar_protesis_removible != null) {
      this.auxiliar_protesis_removible.referencia_grafico_svg.fire('çlick');
    }


    this.indicesMovilidad.forEach((valor, indice) => {
      this.indicesMovilidad[indice] = 0
    });
    this.indicesRecesion.forEach((valor, indice) => {
      this.indicesRecesion[indice] = 0
    });
  }

  guardarOdontograma() {
    let detalles = AdaptadorGraficoModelos.convertirGraficoDetalles(this);
    this.calcularIndices(detalles);
    this.servicioOdontograma.createDetallesOdontograma(detalles, this.pacienteActual.id).subscribe(
      data => {
        console.log(data)
      }
      , err => {
        console.log(err)
      }
    );
  }

  obtenerOdontograma() {
    let detalles = this.servicioOdontograma.getDetallesOdontograma(this.pacienteActual.id).subscribe(
      detalles => {
        this.limpiarOdontograma();
        AdaptadorGraficoModelos.convertirDetallesGrafico(this, detalles);
        this.calcularIndices(detalles);
      },
      err => {
        console.log(err);
      }
    );
  }

  calcularIndices(detalles: any[]) {
    let cariadas: number[] = [];
    let obturadas: number[] = [];
    let perdidas: number[] = [];
    detalles.map(detalle => {
      switch (detalle.tipo) {
        case "Caries":
          if (detalle.realizado) {
            if (!cariadas.includes(detalle.pieza) && !obturadas.includes(detalle.pieza)) obturadas.push(detalle.pieza);
          } else {
            if (!cariadas.includes(detalle.pieza)) cariadas.push(detalle.pieza);
            if (obturadas.includes(detalle.pieza)) obturadas = this.eliminarElemento(obturadas, detalle.pieza);
          }
          break

        case "ProtesisFija":
          if (detalle.realizado) {
            var extremos: number[] = AdaptadorGraficoModelos.identificarExtremos(detalle.piezas);
            var lastIndex: number = extremos.length - 1;
            extremos.map(n => {
              if (extremos.indexOf(n) === 0 || extremos.indexOf(n) === lastIndex) obturadas.push(n);
              else perdidas.push(n);
            });
          } else {
            detalle.piezas.map(n => {
              cariadas.push(n);
            });
          }
          break

        case "ProtesisRemovible":
          if (detalle.realizado) {
            detalle.piezas.map(n => {
              perdidas.push(n);
            });
          } else {
            detalle.piezas.map(n => {
              cariadas.push(n);
            });
          }
          break

        case "ProtesisTotal":
          if (detalle.realizado) {
            detalle.piezas.map(n => {
              perdidas.push(n);
            });
          } else {
            detalle.piezas.map(n => {
              cariadas.push(n);
            });
          }
          break

        case "Corona":
          if (detalle.realizado) {
            detalle.piezas.map(n => {
              obturadas.push(n);
            });
          } else {
            detalle.piezas.map(n => {
              cariadas.push(n);
            });
          }
          break

        case "Sellante":
          if (detalle.realizado) {
            if (!cariadas.includes(detalle.pieza) && !obturadas.includes(detalle.pieza)) obturadas.push(detalle.pieza);
          } else {
            if (!cariadas.includes(detalle.pieza)) cariadas.push(detalle.pieza);
            if (obturadas.includes(detalle.pieza)) obturadas = this.eliminarElemento(obturadas, detalle.pieza);
          }
          break

        case "Endodoncia":
          if (detalle.realizado) {
            if (!cariadas.includes(detalle.pieza) && !obturadas.includes(detalle.pieza)) obturadas.push(detalle.pieza);
          } else {
            if (!cariadas.includes(detalle.pieza)) cariadas.push(detalle.pieza);
            if (obturadas.includes(detalle.pieza)) obturadas = this.eliminarElemento(obturadas, detalle.pieza);
          }
          break

        case "ExtraccionIndicada":
          if (!cariadas.includes(detalle.pieza)) cariadas.push(detalle.pieza);
          break

        case "ExtraccionCaries":
          if (!perdidas.includes(detalle.pieza)) perdidas.push(detalle.pieza);
          break

        case "ExtraccionOtro":
          if (!perdidas.includes(detalle.pieza)) perdidas.push(detalle.pieza);
          break
      }
    });

    this.C = cariadas.filter(this.filtrarPermanentes).length;
    this.P = perdidas.filter(this.filtrarPermanentes).length;
    this.O = obturadas.filter(this.filtrarPermanentes).length;

    this.c = cariadas.filter(this.filtrarDeciduas).length;
    this.e = perdidas.filter(this.filtrarDeciduas).length;
    this.o = obturadas.filter(this.filtrarDeciduas).length;
  }

  filtrarPermanentes(element: number) {
    return element < 50;
  }

  filtrarDeciduas(element: number) {
    return element > 50;
  }

  eliminarElemento(lista: number[], eliminar: number): number[] {
    var i = lista.indexOf(eliminar);
    if (i !== -1) {
      lista.splice(i, 1);
    }
    return lista;
  }

  @HostListener("window:keydown", ['$event'])
  onKeyDown(event: KeyboardEvent) {

    if (event.key == 'Enter') {
      //console.log('se ha presionado enter');
      if ((OdontogramaComponent.detalle_a_aplicar == 'protesis_fija_necesaria' || OdontogramaComponent.detalle_a_aplicar == 'protesis_fija_aplicada') &&
        this.auxiliar_protesis_fija != null) {
        //guardar protesis
        this.protesis.push(this.auxiliar_protesis_fija);
        this.auxiliar_protesis_fija.referencia_grafico_svg.opacity(1);

        if (this.auxiliar_protesis_fija.aplicada) {
          this.auxiliar_protesis_fija.piezas.forEach((pieza) => {
            pieza.QuitarTodosDetalles();
          });
        }

        this.auxiliar_protesis_fija = null;
      }
      if ((OdontogramaComponent.detalle_a_aplicar == 'protesis_removible_necesaria' || OdontogramaComponent.detalle_a_aplicar == 'protesis_removible_aplicada') &&
        this.auxiliar_protesis_removible != null) {
        //guardar protesis
        this.protesis.push(this.auxiliar_protesis_removible);
        this.auxiliar_protesis_removible.referencia_grafico_svg.opacity(1);

        if (this.auxiliar_protesis_removible.aplicada) {
          this.auxiliar_protesis_removible.piezas.forEach((pieza) => {
            pieza.QuitarTodosDetalles();
          });
        }

        this.auxiliar_protesis_removible = null;
      }
      if ((OdontogramaComponent.detalle_a_aplicar == 'protesis_corona_necesaria' || OdontogramaComponent.detalle_a_aplicar == 'protesis_corona_aplicada') &&
        this.auxiliar_protesis_corona != null) {
        //guardar protesis
        this.protesis.push(this.auxiliar_protesis_corona);
        this.auxiliar_protesis_corona.referencia_grafico_svg.opacity(1);

        if (this.auxiliar_protesis_corona.aplicada) {
          this.auxiliar_protesis_corona.piezas.forEach((pieza) => {
            pieza.QuitarTodosDetalles();
          });
        }

        this.auxiliar_protesis_corona = null;
      }
      this.mensaje_finalizar_cancelar.opacity(0)
      this.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this))
    } else if (event.key == 'Escape') {
      //console.log('se ha presionado escape');
      if (OdontogramaComponent.detalle_a_aplicar == 'protesis_fija_necesaria' || OdontogramaComponent.detalle_a_aplicar == 'protesis_fija_aplicada') {
        //quitar protesis
        this.auxiliar_protesis_fija.referencia_grafico_svg.fire('click');
      }
      if (OdontogramaComponent.detalle_a_aplicar == 'protesis_removible_necesaria' || OdontogramaComponent.detalle_a_aplicar == 'protesis_removible_aplicada') {
        //quitar protesis
        this.auxiliar_protesis_removible.referencia_grafico_svg.fire('click');
      }
      if (OdontogramaComponent.detalle_a_aplicar == 'protesis_corona_necesaria' || OdontogramaComponent.detalle_a_aplicar == 'protesis_corona_aplicada') {
        //quitar protesis
        this.auxiliar_protesis_corona.referencia_grafico_svg.fire('click');
      }
      this.mensaje_finalizar_cancelar.opacity(0)
    }
  }

  filtrarRangoMovilidad(event): void {

    if (event.srcElement.value.length == 1) {//un solo caracter
      if (event.srcElement.value == '-') {
        event.srcElement.value = 0
      } else if (event.srcElement.value == '+') {
        event.srcElement.value = 0
      }
      try {
        var numero: number = +event.srcElement.value;
        if (numero > 3) {
          event.srcElement.value = 3
        }
      } catch (ex) {
        event.srcElement.value = 0
      }

    } else if (event.srcElement.value.length == 2) {//un caracter
      var caracter = event.srcElement.value[1]
      if (caracter == '-') {
        event.srcElement.value = event.srcElement.value[0]
      } else if (caracter == '+') {
        event.srcElement.value = event.srcElement.value[0]
      }
      try {
        var numero: number = +caracter;
        if (numero <= 3) {
          event.srcElement.value = numero
        } else {
          event.srcElement.value = 3
        }
      } catch (ex) {
        event.srcElement.value = 0
      }
    } else {
      event.srcElement.value = 0
    }
  }

  filtrarRangoRecesion(event): void {

    if (event.srcElement.value.length == 1) {//un solo caracter
      if (event.srcElement.value == '-') {
        event.srcElement.value = 0
      } else if (event.srcElement.value == '+') {
        event.srcElement.value = 0
      }
      try {
        var numero: number = +event.srcElement.value;
        if (numero > 4) {
          event.srcElement.value = 4
        }
      } catch (ex) {
        event.srcElement.value = 0
      }

    } else if (event.srcElement.value.length == 2) {//un caracter
      var caracter = event.srcElement.value[1]
      if (caracter == '-') {
        event.srcElement.value = event.srcElement.value[0]
      } else if (caracter == '+') {
        event.srcElement.value = event.srcElement.value[0]
      }
      try {
        var numero: number = +caracter;
        if (numero <= 4) {
          event.srcElement.value = numero
        } else {
          event.srcElement.value = 4
        }
      } catch (ex) {
        event.srcElement.value = 0
      }
    } else {
      event.srcElement.value = 0
    }
  }

}
