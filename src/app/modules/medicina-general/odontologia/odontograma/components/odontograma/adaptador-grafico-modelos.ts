import { Pieza } from '../../graphic-models/pieza';
import { DetalleOdontogramaSimple } from '../../models/detalle-odontograma-simple';
import { DetalleOdontograma } from '../../models/detalle-odontograma';
import { DetalleOdontogramaCompuesto } from '../../models/detalle-odontograma-compuesto';
import { DetalleOdontogramaCuantitativo } from '../../models/detalle-odontograma-cuantitativo';
import { ProtesisTotal } from '../../graphic-models/protesis-total';
import { OdontogramaComponent } from './odontograma.component';
import { Protesis } from '../../graphic-models/protesis';
import { ProtesisRemovible } from '../../graphic-models/protesis-removible';
import { ProtesisFija } from '../../graphic-models/protesis-fija';
import { ProtesisCorona } from '../../graphic-models/protesis-corona';

export class AdaptadorGraficoModelos{

    public static convertirGraficoDetalles(componente_odontograma: OdontogramaComponent): DetalleOdontograma[]{
        const piezas_permanentes: Pieza[] = componente_odontograma.piezasPermanentes;
        const piezas_deciduas: Pieza[] = componente_odontograma.piezasDeciduas;
        const detalles_compuestos_graficos: Protesis[] =  componente_odontograma.protesis;
        const indices_movilidad: number[] = componente_odontograma.indicesMovilidad;
        const indices_recesion: number[] = componente_odontograma.indicesRecesion;

        const lista_piezas: Pieza[] = [];
        piezas_deciduas.forEach((pieza) => {
            if (pieza){
                lista_piezas[pieza.numero_pieza] = pieza;
            }
        });
        piezas_permanentes.forEach((pieza) => {
            if (pieza){
                lista_piezas[pieza.numero_pieza] = pieza;
            }
        });

        const detalles: DetalleOdontograma[] = [];
        // BUSCANDO DETALLES SIMPLES   /Por alguna razon no funciona concat en este caso

        // Caries y Obturaciones
        this.buscarDetallesCariesObturaciones(lista_piezas).forEach((detalle) => {
            detalles.push(detalle);
        });
        // Endodoncias
        this.buscarDetallesEndodoncias(lista_piezas).forEach((detalle) => {
            detalles.push(detalle);
        });
        // Extracciones
        this.buscarDetallesExtracciones(lista_piezas).forEach((detalle) => {
            detalles.push(detalle);
        });
        // Sellantes
        this.buscarDetallesSellantes(lista_piezas).forEach((detalle) => {
            detalles.push(detalle);
        });

        // BUSCANDO DETALLES COMPUESTOS
        this.buscarProtesisTotales(detalles_compuestos_graficos).forEach((detalle) => {
            detalles.push(detalle);
        });

        this.buscarProtesisRemovibles(detalles_compuestos_graficos).forEach((detalle) => {
            detalles.push(detalle);
        });

        this.buscarProtesisFijas(detalles_compuestos_graficos).forEach((detalle) => {
            detalles.push(detalle);
        });
        this.buscarCoronas(detalles_compuestos_graficos).forEach((detalle) => {
            detalles.push(detalle);
        });

        // BUSCANDO DETALLES CUANTITATIVOS
        this.buscarMovilidad(indices_movilidad).forEach((detalle) => {
            detalles.push(detalle);
        });
        this.buscarRecesion(indices_recesion).forEach((detalle) => {
            detalles.push(detalle);
        });

        return detalles;
    }

    public static convertirDetallesGrafico(componente_odontograma: OdontogramaComponent, detalles: any[]): void{
        const lienzo: any = componente_odontograma.lienzo;
        const piezas_permanentes: Pieza[] = componente_odontograma.piezasPermanentes;
        const piezas_deciduas: Pieza[] = componente_odontograma.piezasDeciduas;
        const detalles_compuestos_graficos: Protesis[] = componente_odontograma.protesis;
        const indices_movilidad: number[] = componente_odontograma.indicesMovilidad;
        const indices_recesion: number[] = componente_odontograma.indicesRecesion;
       /* console.log('Convirtiendo ')
        console.log(detalles)*/
        const lista_piezas: Pieza[] = [];
        piezas_deciduas.forEach((pieza) => {
            if (pieza){
                lista_piezas[pieza.numero_pieza] = pieza;
            }
        });
        piezas_permanentes.forEach((pieza) => {
            if (pieza){
                lista_piezas[pieza.numero_pieza] = pieza;
            }
        });
        detalles.forEach((detalle) => {
            // Verificar si es detalle simple
            if (detalle.cara ){
                // console.log('detalle simple')
                switch (detalle.tipo){
                    case 'Caries':
                        if (detalle.realizado){
                            switch (detalle.cara){
                                case 'Centro':
                                    lista_piezas[detalle.pieza].cara_centro.obturado = true;
                                    break;
                                case 'Arriba':
                                    lista_piezas[detalle.pieza].cara_arriba.obturado = true;
                                    break;
                                case 'Abajo':
                                    lista_piezas[detalle.pieza].cara_abajo.obturado = true;
                                    break;
                                case 'Izquierda':
                                    lista_piezas[detalle.pieza].cara_izquierda.obturado = true;
                                    break;
                                case 'Derecha':
                                    lista_piezas[detalle.pieza].cara_derecha.obturado = true;
                                    break;
                            }
                        }
                        else{
                            switch (detalle.cara){
                                case 'Centro':
                                    lista_piezas[detalle.pieza].cara_centro.caries = true;
                                    break;
                                case 'Arriba':
                                    lista_piezas[detalle.pieza].cara_arriba.caries = true;
                                    break;
                                case 'Abajo':
                                    lista_piezas[detalle.pieza].cara_abajo.caries = true;
                                    break;
                                case 'Izquierda':
                                    lista_piezas[detalle.pieza].cara_izquierda.caries = true;
                                    break;
                                case 'Derecha':
                                    lista_piezas[detalle.pieza].cara_derecha.caries = true;
                                    break;
                            }
                        }
                        break;
                    case 'Sellante':
                        if (detalle.realizado){
                            lista_piezas[detalle.pieza].sellante_aplicado = true;
                        }
                        else{
                            lista_piezas[detalle.pieza].sellante_necesario = true;
                        }
                        break;
                    case 'Endodoncia':
                        if (detalle.realizado){
                            lista_piezas[detalle.pieza].endodoncia_aplicada = true;
                        }
                        else{
                            lista_piezas[detalle.pieza].endodoncia_necesaria = true;
                        }
                        break;
                    case 'ExtraccionIndicada':
                        lista_piezas[detalle.pieza].extraccion_necesaria = true;
                        break;
                    case 'ExtraccionCaries':
                        lista_piezas[detalle.pieza].extraccion_aplicada_caries = true;
                        break;
                    case 'ExtraccionOtro':
                        lista_piezas[detalle.pieza].extraccion_aplicada_otro = true;
                        break;
                }

            }
            // Verificar si es detalle compuesto
            else if (detalle.piezas){
                // console.log('detalle compuesto')
                if (detalle.tipo === 'ProtesisTotal'){
                    const protesis_total = new ProtesisTotal(lienzo, componente_odontograma);
                    protesis_total.aplicada = detalle.realizado;
                    detalle.piezas.forEach((numero_pieza) => {
                        protesis_total.AgregarPieza(numero_pieza <= 48 ? piezas_permanentes[numero_pieza] : piezas_deciduas[numero_pieza]);
                    });
                    detalles_compuestos_graficos.push(protesis_total);
                }
                else if (detalle.tipo === 'ProtesisRemovible'){
                    const protesis_removible = new ProtesisRemovible(lienzo, componente_odontograma);
                    console.log(detalle.piezas);
                    protesis_removible.aplicada = detalle.realizado;
                    detalle.piezas.forEach((numero_pieza) => {
                        protesis_removible
                          .AgregarPieza(numero_pieza <= 48 ? piezas_permanentes[numero_pieza] : piezas_deciduas[numero_pieza]);
                    });
                    protesis_removible.referencia_grafico_svg.opacity(1);
                    detalles_compuestos_graficos.push(protesis_removible);
                }
                else if (detalle.tipo === 'ProtesisFija'){
                    const protesis_fija = new ProtesisFija(lienzo, componente_odontograma);
                    protesis_fija.aplicada = detalle.realizado;
                    detalle.piezas.forEach((numero_pieza) => {
                        protesis_fija.AgregarPieza(numero_pieza <= 48 ? piezas_permanentes[numero_pieza] : piezas_deciduas[numero_pieza]);
                    });
                    protesis_fija.referencia_grafico_svg.opacity(1);
                    detalles_compuestos_graficos.push(protesis_fija);
                }
                else if (detalle.tipo === 'Corona'){
                    const protesis_corona = new ProtesisCorona(lienzo, componente_odontograma);
                    protesis_corona.aplicada = detalle.realizado;
                    detalle.piezas.forEach((numero_pieza) => {
                        protesis_corona.AgregarPieza(numero_pieza <= 48 ? piezas_permanentes[numero_pieza] : piezas_deciduas[numero_pieza]);
                    });
                    protesis_corona.referencia_grafico_svg.opacity(1);
                    detalles_compuestos_graficos.push(protesis_corona);
                }

            }
            // Verificar si es detalle cuantitativo
            else if (detalle.nivel){
                // console.log('detalle cuantitativo')
                if (detalle.tipo == 'Movilidad'){
                    indices_movilidad[detalle.pieza] = detalle.nivel;
                }
                else{// Recesion
                    indices_recesion[detalle.pieza] = detalle.nivel;
                }

            }
        });
    }

    private static buscarDetallesExtracciones(piezasGraficas: Pieza[]): DetalleOdontogramaSimple[]{
        const detalles: DetalleOdontogramaSimple[] = [];
        piezasGraficas.forEach((pieza) => {
            if (pieza.extraccion_necesaria){
                const nuevo_detalle = new DetalleOdontogramaSimple();
                nuevo_detalle.tipo = 'ExtraccionIndicada';
                nuevo_detalle.realizado = false;
                nuevo_detalle.pieza = pieza.numero_pieza;
                nuevo_detalle.cara = 'Centro'; // Por defecto
                detalles.push(nuevo_detalle);
            }
            if (pieza.extraccion_aplicada_caries){
                const nuevo_detalle = new DetalleOdontogramaSimple();
                nuevo_detalle.tipo = 'ExtraccionCaries';
                nuevo_detalle.realizado = true;
                nuevo_detalle.pieza = pieza.numero_pieza;
                nuevo_detalle.cara = 'Centro'; // Por defecto
                detalles.push(nuevo_detalle);
            }
            if (pieza.extraccion_aplicada_otro){
                const nuevo_detalle = new DetalleOdontogramaSimple();
                nuevo_detalle.tipo = 'ExtraccionOtro';
                nuevo_detalle.realizado = true;
                nuevo_detalle.pieza = pieza.numero_pieza;
                nuevo_detalle.cara = 'Centro'; // Por defecto
                detalles.push(nuevo_detalle);
            }
        });
        return detalles;
    }
    private static buscarDetallesSellantes(piezasGraficas: Pieza[]): DetalleOdontogramaSimple[]{
        const detalles: DetalleOdontogramaSimple[] = [];
        piezasGraficas.forEach((pieza) => {
            if (pieza.sellante_aplicado){
                const nuevo_detalle = new DetalleOdontogramaSimple();
                nuevo_detalle.tipo = 'Sellante';
                nuevo_detalle.realizado = true;
                nuevo_detalle.pieza = pieza.numero_pieza;
                nuevo_detalle.cara = 'Centro'; // Por defecto
                detalles.push(nuevo_detalle);
            }
            if (pieza.sellante_necesario){
                const nuevo_detalle = new DetalleOdontogramaSimple();
                nuevo_detalle.tipo = 'Sellante';
                nuevo_detalle.realizado = false;
                nuevo_detalle.pieza = pieza.numero_pieza;
                nuevo_detalle.cara = 'Centro'; // Por defecto
                detalles.push(nuevo_detalle);
            }
        });
        return detalles;
    }
    private static buscarDetallesEndodoncias(piezasGraficas: Pieza[]): DetalleOdontogramaSimple[]{
        const detalles: DetalleOdontogramaSimple[] = [];
        piezasGraficas.forEach((pieza) => {
            if (pieza.endodoncia_aplicada){
                const nuevo_detalle = new DetalleOdontogramaSimple();
                nuevo_detalle.tipo = 'Endodoncia';
                nuevo_detalle.realizado = true;
                nuevo_detalle.pieza = pieza.numero_pieza;
                nuevo_detalle.cara = 'Centro'; // Por defecto
                detalles.push(nuevo_detalle);
            }
            if (pieza.endodoncia_necesaria){
                const nuevo_detalle = new DetalleOdontogramaSimple();
                nuevo_detalle.tipo = 'Endodoncia';
                nuevo_detalle.realizado = false;
                nuevo_detalle.pieza = pieza.numero_pieza;
                nuevo_detalle.cara = 'Centro'; // Por defecto
                detalles.push(nuevo_detalle);
            }
        });
        return detalles;
    }

    private static buscarDetallesCariesObturaciones(piezasGraficas: Pieza[]): DetalleOdontogramaSimple[]{
        const detalles: DetalleOdontogramaSimple[] = [];
        piezasGraficas.forEach((pieza) => {
            // buscandoCaries
            if (pieza.cara_abajo.caries){
                const nuevo_detalle = new DetalleOdontogramaSimple();
                nuevo_detalle.tipo  = 'Caries';
                nuevo_detalle.realizado =  false;
                nuevo_detalle.pieza = pieza.numero_pieza;
                nuevo_detalle.cara = 'Abajo';
                detalles.push(nuevo_detalle);
            }
            if (pieza.cara_arriba.caries){
                const nuevo_detalle = new DetalleOdontogramaSimple();
                nuevo_detalle.tipo  = 'Caries';
                nuevo_detalle.realizado =  false;
                nuevo_detalle.pieza = pieza.numero_pieza;
                nuevo_detalle.cara = 'Arriba';
                detalles.push(nuevo_detalle);
            }
            if (pieza.cara_izquierda.caries){
                const nuevo_detalle = new DetalleOdontogramaSimple();
                nuevo_detalle.tipo  = 'Caries';
                nuevo_detalle.realizado =  false;
                nuevo_detalle.pieza = pieza.numero_pieza;
                nuevo_detalle.cara = 'Izquierda';
                detalles.push(nuevo_detalle);
            }
            if (pieza.cara_derecha.caries){
                const nuevo_detalle = new DetalleOdontogramaSimple();
                nuevo_detalle.tipo  = 'Caries';
                nuevo_detalle.realizado =  false;
                nuevo_detalle.pieza = pieza.numero_pieza;
                nuevo_detalle.cara = 'Derecha';
                detalles.push(nuevo_detalle);
            }
            if (pieza.cara_centro.caries){
                const nuevo_detalle = new DetalleOdontogramaSimple();
                nuevo_detalle.tipo  = 'Caries';
                nuevo_detalle.realizado =  false;
                nuevo_detalle.pieza = pieza.numero_pieza;
                nuevo_detalle.cara = 'Centro';
                detalles.push(nuevo_detalle);
            }
            // buscandoObturaciones
            if (pieza.cara_abajo.obturado){
                const nuevo_detalle = new DetalleOdontogramaSimple();
                nuevo_detalle.tipo  = 'Caries';
                nuevo_detalle.realizado =  true;
                nuevo_detalle.pieza = pieza.numero_pieza;
                nuevo_detalle.cara = 'Abajo';
                detalles.push(nuevo_detalle);
            }
            if (pieza.cara_arriba.obturado){
                const nuevo_detalle = new DetalleOdontogramaSimple();
                nuevo_detalle.tipo  = 'Caries';
                nuevo_detalle.realizado =  true;
                nuevo_detalle.pieza = pieza.numero_pieza;
                nuevo_detalle.cara = 'Arriba';
                detalles.push(nuevo_detalle);
            }
            if (pieza.cara_izquierda.obturado){
                const nuevo_detalle = new DetalleOdontogramaSimple();
                nuevo_detalle.tipo  = 'Caries';
                nuevo_detalle.realizado =  true;
                nuevo_detalle.pieza = pieza.numero_pieza;
                nuevo_detalle.cara = 'Izquierda';
                detalles.push(nuevo_detalle);
            }
            if (pieza.cara_derecha.obturado){
                const nuevo_detalle = new DetalleOdontogramaSimple();
                nuevo_detalle.tipo  = 'Caries';
                nuevo_detalle.realizado =  true;
                nuevo_detalle.pieza = pieza.numero_pieza;
                nuevo_detalle.cara = 'Derecha';
                detalles.push(nuevo_detalle);
            }
            if (pieza.cara_centro.obturado){
                const nuevo_detalle = new DetalleOdontogramaSimple();
                nuevo_detalle.tipo  = 'Caries';
                nuevo_detalle.realizado =  true;
                nuevo_detalle.pieza = pieza.numero_pieza;
                nuevo_detalle.cara = 'Centro';
                detalles.push(nuevo_detalle);
            }
        });
        return detalles;
    }
    private static buscarMovilidad(indices_movilidad: number[]): DetalleOdontogramaCuantitativo[]{
        const detalles: DetalleOdontogramaCuantitativo[] = [];
        indices_movilidad.forEach((numero, indice) => {
            if (numero !== 0){
                const nuevo_detalle = new DetalleOdontogramaCuantitativo();
                nuevo_detalle.tipo = 'Movilidad';
                nuevo_detalle.pieza = indice;
                nuevo_detalle.nivel = numero;
                detalles.push(nuevo_detalle);
            }
        });
        return detalles;
    }
    private static buscarRecesion(indices_recesion: number[]): DetalleOdontogramaCuantitativo[]{
        const detalles: DetalleOdontogramaCuantitativo[] = [];
        indices_recesion.forEach((numero, indice) => {
            if (numero !== 0){
                const nuevo_detalle = new DetalleOdontogramaCuantitativo();
                nuevo_detalle.tipo = 'Recesion';
                nuevo_detalle.pieza = indice;
                nuevo_detalle.nivel = numero;
                detalles.push(nuevo_detalle);

            }
        });
        return detalles;
    }

    private static buscarProtesisTotales(protesis: Protesis[]): DetalleOdontogramaCompuesto[]{
        const detalles: DetalleOdontogramaCompuesto[] = [];
        protesis.forEach((protesis) => {
            if (protesis instanceof ProtesisTotal){
                const nuevo_detalle = new DetalleOdontogramaCompuesto();
                nuevo_detalle.tipo  = 'ProtesisTotal';
                nuevo_detalle.realizado = protesis.aplicada;
                nuevo_detalle.piezas = [];
                protesis.piezas.forEach((pieza) => {
                    nuevo_detalle.piezas.push(pieza.numero_pieza);
                });
                detalles.push(nuevo_detalle);
            }
        });
        return detalles;
    }

    private static buscarProtesisFijas(protesis: Protesis[]): DetalleOdontogramaCompuesto[]{
        const detalles: DetalleOdontogramaCompuesto[] = [];
        protesis.forEach((protesis) => {
            if (protesis instanceof ProtesisFija){
                const nuevo_detalle = new DetalleOdontogramaCompuesto();
                nuevo_detalle.tipo  = 'ProtesisFija';
                nuevo_detalle.realizado = protesis.aplicada;
                nuevo_detalle.piezas = [];
                protesis.piezas.forEach((pieza) => {
                    nuevo_detalle.piezas.push(pieza.numero_pieza);
                });
                detalles.push(nuevo_detalle);
            }
        });
        return detalles;
    }

    private static buscarProtesisRemovibles(protesis: Protesis[]): DetalleOdontogramaCompuesto[]{
        const detalles: DetalleOdontogramaCompuesto[] = [];
        protesis.forEach((protesis) => {
            if (protesis instanceof ProtesisRemovible){
                const nuevo_detalle = new DetalleOdontogramaCompuesto();
                nuevo_detalle.tipo  = 'ProtesisRemovible';
                nuevo_detalle.realizado = protesis.aplicada;
                nuevo_detalle.piezas = [];
                protesis.piezas.forEach((pieza) => {
                    nuevo_detalle.piezas.push(pieza.numero_pieza);
                });
                detalles.push(nuevo_detalle);
            }
        });
        return detalles;
    }
    private static buscarCoronas(protesis: Protesis[]): DetalleOdontogramaCompuesto[]{
        const detalles: DetalleOdontogramaCompuesto[] = [];
        protesis.forEach((protesis) => {
            if (protesis instanceof ProtesisCorona){
                const nuevo_detalle = new DetalleOdontogramaCompuesto();
                nuevo_detalle.tipo  = 'Corona';
                nuevo_detalle.realizado = protesis.aplicada;
                nuevo_detalle.piezas = [];
                protesis.piezas.forEach((pieza) => {
                    nuevo_detalle.piezas.push(pieza.numero_pieza);
                });
                detalles.push(nuevo_detalle);
            }
        });
        return detalles;
    }


    public static identificarExtremos(piezas: number[]): number[] {
        const grupos: number[] = [Math.trunc(piezas.sort()[0] / 10)];
        if (grupos[0] % 2 === 0) { grupos[1] = grupos[0] - 1; }
        else { grupos[1] = grupos[0] + 1; }
        const indicadorPrimerGrupo = grupos[0];
        let grupo1, grupo2, result, indexToSplice = 0;
        piezas.forEach(p => {
            const grupoPiezaActual = Math.trunc(p / 10);
            if (!grupos.includes(grupoPiezaActual)) {
                throw new Error('Las piezas no pertenecen al mismo grupo');
            }
            if (grupoPiezaActual !== indicadorPrimerGrupo && indexToSplice === 0) {
                indexToSplice = piezas.indexOf(p);
            }
        });
        grupo1 = piezas.slice(0, indexToSplice);
        grupo2 = piezas.slice(indexToSplice, piezas.length);
        if (grupo1.length > 0) {
            if (grupos.includes(2) || grupos.includes(6)) {
                grupo1.reverse();
                result = grupo1.concat(grupo2);
            } else {
                grupo2.reverse();
                result = grupo2.concat(grupo1);
            }
        } else {
            const toRevert: number[] = [1, 4, 5 , 8];
            const grupoPiezaActual = Math.trunc(grupo2[0] / 10);
            if (toRevert.includes(grupoPiezaActual)) {
                result = grupo2.reverse();
            } else {
                result = grupo2;
            }
        }
        return result;
    }

    public static generarGrupos(inicio: number, fin: number): number[] {
        const grupos: number[] = [Math.trunc(+inicio / 10)];
        if (grupos[0] % 2 === 0) { grupos[1] = grupos[0] - 1; }
        else { grupos[1] = grupos[0] + 1; }
        let result: number[] = [];
        if (!grupos.includes(Math.trunc(+fin / 10))){
            throw new Error('Las piezas no pertenecen al mismo grupo');
        }
        if (Math.trunc(+inicio / 10) !== Math.trunc(+fin / 10)) {
            const toRevert: number[] = [1, 4, 5 , 8];
            if (toRevert.includes(grupos[0])) {
                result = result.concat(this.llenarGrupo(inicio, 1));
                result = result.concat(this.llenarGrupo(fin, 1));
            }
            else {
                result = result.concat(this.llenarGrupo(fin, 1 ));
                result = result.concat(this.llenarGrupo(inicio, 1));
            }
        } else {
            if (inicio < fin) {
                const aux = inicio;
                inicio = fin;
                fin = aux;
            }
            // console.log('Mismo grupo: ', this.llenarGrupo(inicio, fin));
            result = result.concat(this.llenarGrupo(inicio, fin));
        }


        return result;
    }

    private static llenarGrupo(pieza: number, limit: number): number[] {
        const toRevert: number[] = [1, 4, 5 , 8];
        const grupoPiezaActual = Math.trunc(+pieza / 10);
        const result: number[] = [];
        if (limit !== 1) { limit = +limit - (grupoPiezaActual * 10); }
        if (toRevert.includes(grupoPiezaActual)) {
            // console.log(+pieza, +limit + (grupoPiezaActual*10));
            for (let index = +pieza; index >= +limit + (grupoPiezaActual * 10); index--) {
                result.push(index);
            }
        } else {
            // console.log(+limit + (grupoPiezaActual*10), +pieza);
            for (let index = +limit + (grupoPiezaActual * 10); index <= +pieza; index++) {
                result.push(index);
            }
        }
        return result;
    }
}
