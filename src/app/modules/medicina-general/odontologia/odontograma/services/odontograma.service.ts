import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { retry } from 'rxjs/operators';
import { DetalleOdontograma } from '../models/detalle-odontograma';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OdontogramaService {
  public url: string;
  public identity;
  public token;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Allow: 'GET, POST, HEAD'
    })
  };

  constructor(private http: HttpClient) {
    this.url = 'http://localhost:8080/api/v1.0/'; // cambiar cuando este subido al servidor
  }


  // GET
  getDetallesOdontograma(idPaciente: number): Observable<DetalleOdontograma[]>  {
    return this.http.get<DetalleOdontograma[]>(this.url + "odontograma",{params:{
      idPaciente: idPaciente.toString(),
    }})
    .pipe(
      retry(1)
    );
  }

  // POST
  createDetallesOdontograma(detalles: DetalleOdontograma[], idPaciente): Observable<DetalleOdontograma[]>  {
    return this.http.post<DetalleOdontograma[]>(
      this.url + 'odontograma', JSON.stringify(detalles), {
        params:{
          idPaciente: idPaciente.toString(),
        },
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Accept: 'application/json',
          Allow: 'GET, POST, HEAD'
        })
      })
    .pipe(
      retry(1)
    );
  }


}
