import { GraficoSVG } from './grafico-svg';
import { Pieza } from './pieza';
import { OdontogramaComponent } from '../components/odontograma/odontograma.component';


export abstract class Protesis extends GraficoSVG{
    public piezas:Pieza[] = [];
    
    protected _aplicada: boolean;
    public get aplicada(): boolean {return this._aplicada;}
    public set aplicada(value: boolean) {this._aplicada = value;this.ActualizarColorLinea();}


    constructor(lienzo:any,odontograma_component:OdontogramaComponent){
        super(lienzo);
        this.componente_odontograma = odontograma_component;
        this.referencia_grafico_svg = this.lienzo.group();
        this.aplicada = false;
    }
    protected GenerarGrafico(): void {
        console.log('No hay grafico que generar ya que la clase Protesis es solo una agrupación de elementos')
    }
    protected ActualizarEscala(): void {
        console.log('No hay como agrandar la escala de detalle compuesto ya que esta la clase Protesis solo representa una grupación de piezas');
    }
    protected ActualizarColorRelleno(): void {
        console.log('No hay relleno ya que la clase Protesis es solo una agrupación de piezas');
    }
    protected ActualizarFormaFinLinea(): void {
        console.log('No se puede modificar la forma de fin de linea ya que la clase Protesis es solo una agrupación de piezas');
    }
    protected ActualizarPosicion(): void {
        console.log('No se puede cambiar la posicion de la linea ya que la clase Protesis es solo una agrupación de piezas')
    }
    
    public abstract AgregarPieza(pieza:Pieza);
    public abstract QuitarPieza(pieza:Pieza);

    public ContienePieza(pieza:Pieza){
        let existe = false
        this.piezas.forEach( (item) => {
            
            if(item.numero_pieza+'' == pieza.numero_pieza+''){ 
                existe = true
            }
          });
        return existe;
    }
}