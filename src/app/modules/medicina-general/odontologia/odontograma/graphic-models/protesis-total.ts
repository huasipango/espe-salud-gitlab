import { Protesis } from './protesis';
import { Pieza } from './pieza';
import { OdontogramaComponent } from '../components/odontograma/odontograma.component';
import { AdaptadorGraficoModelos } from '../components/odontograma/adaptador-grafico-modelos';

export class ProtesisTotal extends Protesis{

    constructor(lienzo:any,odontograma_component:OdontogramaComponent){
        super(lienzo,odontograma_component);
        this.GenerarEventos();
    }
    protected GenerarEventos(): void {
        this.referencia_grafico_svg.on('mouseenter', ()=>{
            if(OdontogramaComponent.detalle_a_aplicar == "borrar"||OdontogramaComponent.detalle_a_aplicar == "protesis_total_necesaria"
            ||OdontogramaComponent.detalle_a_aplicar == "protesis_total_aplicada"){
                this.grosor_linea = 5
            }
        })
        this.referencia_grafico_svg.on('mouseleave', ()=>{
            if(OdontogramaComponent.detalle_a_aplicar == "borrar"||OdontogramaComponent.detalle_a_aplicar == "protesis_total_necesaria"
            ||OdontogramaComponent.detalle_a_aplicar == "protesis_total_aplicada"){
                
                this.grosor_linea = 4
            }
        })
        this.referencia_grafico_svg.on('click', (event)=>{
            
            if(OdontogramaComponent.detalle_a_aplicar == "borrar" || !(event instanceof MouseEvent)){
                //quitando el detalle de las piezas
                this.piezas.forEach((pieza)=>{
                    pieza.protesis_total_aplicada = false
                    pieza.protesis_total_necesaria = false                    
                });
                //quitando el detalle de la lista 
                this.componente_odontograma.protesis.forEach( (item, index) => {
                    if(item === this){ 
                        this.componente_odontograma.protesis.splice(index,1);
                    }
                });
                this.referencia_grafico_svg.remove();
                this.piezas = null;
                this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
                this.componente_odontograma = null;
            }
            else if(OdontogramaComponent.detalle_a_aplicar == "protesis_total_aplicada"){
                this.piezas.forEach((pieza)=>{
                    this.aplicada = true
                });
                this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
            }
            else if(OdontogramaComponent.detalle_a_aplicar == "protesis_total_necesaria"){
                this.piezas.forEach((pieza)=>{
                    this.aplicada = false
                });
                this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
            }
        })
    }
    
    
    public AgregarPieza(pieza: Pieza) {
        let agregar = true
        this.piezas.forEach( (item) => {
            if(item === pieza) {agregar = false}
          });
        if(agregar){
            this.piezas.push(pieza)
            if(this.aplicada){
                pieza.QuitarTodosDetalles();
            }
        }
        if(this.aplicada){
            pieza.protesis_total_aplicada = true;
        }
        else{
            pieza.protesis_total_necesaria = true;
        }
        this.referencia_grafico_svg.add(pieza.referencia_grafico_protesis_total);
    }
    public QuitarPieza(pieza: Pieza) {
        this.piezas.forEach( (item, index) => {
            if(item === pieza){ 
                this.piezas.splice(index,1);
                pieza.protesis_total_aplicada = false
                pieza.protesis_total_necesaria = false
            }
          });
    }
    protected ActualizarColorLinea(): void {
        try{
            this.referencia_grafico_svg.remove();
        }catch(error){};
        this.referencia_grafico_svg= null;
        this.referencia_grafico_svg = this.lienzo.group();
        this.GenerarEventos();

        if(this.aplicada){
            this.piezas.forEach( (item) => {
                item.protesis_total_aplicada = true;
                this.referencia_grafico_svg.add(item.referencia_grafico_protesis_total);
                item.QuitarTodosDetalles();
              });

        }else{
            this.piezas.forEach( (item) => {
                item.protesis_total_necesaria = true;
                this.referencia_grafico_svg.add(item.referencia_grafico_protesis_total);
              });
        }
    }
    protected ActualizarGrosorLinea(): void {
        this.piezas.forEach( (pieza) => {
            pieza.referencia_grafico_protesis_total.stroke({width: this.grosor_linea})
          });
    }
}