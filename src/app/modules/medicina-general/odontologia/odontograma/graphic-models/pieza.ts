import { Cara } from './cara';
import { GraficoSVG } from './grafico-svg';
import { OdontogramaComponent } from '../components/odontograma/odontograma.component';
import { ThrowStmt } from '@angular/compiler';
import { ProtesisFija } from './protesis-fija';
import { ProtesisRemovible } from './protesis-removible';
import { ProtesisCorona } from './protesis-corona';
import { AdaptadorGraficoModelos } from '../components/odontograma/adaptador-grafico-modelos';

export abstract class Pieza extends GraficoSVG {

    // Revisar error por duplicación en GraficSVG
    // private _componente_odontograma: OdontogramaComponent;
    // protected get componente_odontograma(): OdontogramaComponent {return this._componente_odontograma;}
    // protected set componente_odontograma(value: OdontogramaComponent) {this._componente_odontograma = value; }

    protected _numero_pieza: number;
    public get numero_pieza(): number {return this._numero_pieza; }
    public set numero_pieza(value: number) {this._numero_pieza = value;this.referencia_grafico_numero.text(''+this.numero_pieza)}


    protected _endodoncia_necesaria: boolean;
    public get endodoncia_necesaria(): boolean {return this._endodoncia_necesaria;}
    public set endodoncia_necesaria(value: boolean) {this._endodoncia_necesaria = value;if(this._endodoncia_necesaria){this.MarcarEndodonciaNecesaria();if(this._endodoncia_aplicada){this._endodoncia_aplicada = false;}}else{if(!this.endodoncia_aplicada){this.DesmarcarEndodonciaNecesaria();}}}
    protected _endodoncia_aplicada: boolean;
    public get endodoncia_aplicada(): boolean {return this._endodoncia_aplicada;}
    public set endodoncia_aplicada(value: boolean) {this._endodoncia_aplicada = value;if(this._endodoncia_aplicada){this.MarcarEndodonciaAplicada();if(this._endodoncia_necesaria){this._endodoncia_necesaria = false;}}else{if(!this.endodoncia_necesaria){this.DesmarcarEndodonciaAplicada();}}}
    protected _extraccion_necesaria: boolean;
    public get extraccion_necesaria(): boolean {return this._extraccion_necesaria;}
    public set extraccion_necesaria(value: boolean) {this._extraccion_necesaria = value;if(this._extraccion_necesaria){this.MarcarExtraccionNecesaria();if(this._extraccion_aplicada_caries ||this._extraccion_aplicada_otro){this._extraccion_aplicada_caries = false;this._extraccion_aplicada_otro = false;}}else{if(!this.extraccion_aplicada_caries && !this.extraccion_aplicada_otro){this.DesmarcarExtraccionNecesaria();}}}
    protected _extraccion_aplicada_caries: boolean;
    public get extraccion_aplicada_caries(): boolean {return this._extraccion_aplicada_caries;}
    public set extraccion_aplicada_caries(value: boolean) {this._extraccion_aplicada_caries = value;if(this._extraccion_aplicada_caries){this.MarcarExtraccionAplicadaCaries();if(this._extraccion_necesaria||this._extraccion_aplicada_otro){this._extraccion_necesaria = false;this._extraccion_aplicada_otro = false;}}else{if(!this.extraccion_necesaria && !this.extraccion_aplicada_otro){this.DesmarcarExtraccionAplicadaCaries();}}}
    protected _extraccion_aplicada_otro: boolean;
    public get extraccion_aplicada_otro(): boolean {return this._extraccion_aplicada_otro;}
    public set extraccion_aplicada_otro(value: boolean) {this._extraccion_aplicada_otro = value;if(this._extraccion_aplicada_otro){this.MarcarExtraccionAplicadaOtro();if(this._extraccion_necesaria||this._extraccion_aplicada_caries){this._extraccion_necesaria = false;this._extraccion_aplicada_caries = false;}}else{if(!this.extraccion_necesaria && !this.extraccion_aplicada_caries){this.DesmarcarExtraccionAplicadaOtro();}}}
    protected _sellante_necesario: boolean;
    public get sellante_necesario(): boolean {return this._sellante_necesario;}
    public set sellante_necesario(value: boolean) {this._sellante_necesario = value;if(this._sellante_necesario){this.MarcarSellanteNecesario();if(this._sellante_aplicado){this._sellante_aplicado = false;}}else{if(!this.sellante_aplicado){this.DesmarcarSellanteNecesario();}}}
    protected _sellante_aplicado: boolean;
    public get sellante_aplicado(): boolean {return this._sellante_aplicado;}
    public set sellante_aplicado(value: boolean) {this._sellante_aplicado = value;if(this._sellante_aplicado){this.MarcarSellanteAplicado();if(this._sellante_necesario){this._sellante_necesario = false;}}else{if(!this._sellante_necesario){this.DesmarcarSellanteAplicado();}}}
    protected _protesis_total_necesaria: boolean;
    public get protesis_total_necesaria(): boolean {return this._protesis_total_necesaria;}
    public set protesis_total_necesaria(value: boolean) {this._protesis_total_necesaria = value;this.MarcarProtesisTotalNecesaria();if(this._protesis_total_necesaria){if(this._protesis_total_aplicada){this._protesis_total_aplicada = false;}}else{if(!this._protesis_total_aplicada){this.DesmarcarProtesisTotalNecesaria();}}}
    protected _protesis_total_aplicada: boolean;
    public get protesis_total_aplicada(): boolean {return this._protesis_total_aplicada;}
    public set protesis_total_aplicada(value: boolean) {this._protesis_total_aplicada = value;if(this._protesis_total_aplicada){this.MarcarProtesisTotalAplicada();if(this._protesis_total_necesaria){this._protesis_total_necesaria = false;}}else{if(!this._protesis_total_necesaria){this.DesmarcarProtesisTotalAplicada();}}}


    protected _protesis_fija_necesaria: boolean;
    public get protesis_fija_necesaria(): boolean {return this._protesis_fija_necesaria;}
    public set protesis_fija_necesaria(value: boolean) {this._protesis_fija_necesaria = value;this.MarcarProtesisFijaNecesaria();if(this._protesis_fija_necesaria){if(this._protesis_fija_aplicada){this._protesis_fija_aplicada = false;}}else{if(!this._protesis_fija_aplicada){this.DesmarcarProtesisFijaNecesaria();}}}
    protected _protesis_fija_aplicada: boolean;
    public get protesis_fija_aplicada(): boolean {return this._protesis_fija_aplicada;}
    public set protesis_fija_aplicada(value: boolean) {this._protesis_fija_aplicada = value;if(this._protesis_fija_aplicada){this.MarcarProtesisFijaAplicada();if(this._protesis_fija_necesaria){this._protesis_fija_necesaria = false;}}else{if(!this._protesis_fija_necesaria){this.DesmarcarProtesisFijaAplicada();}}}
    protected _posicion_en_protesis_fija: string = 'centro';
    public get posicion_en_protesis_fija(): string {return this._posicion_en_protesis_fija;}
    public set posicion_en_protesis_fija(value: string) {this._posicion_en_protesis_fija = value;}//inicio centro final o inicio_final

    protected _protesis_removible_necesaria: boolean;
    public get protesis_removible_necesaria(): boolean {return this._protesis_removible_necesaria;}
    public set protesis_removible_necesaria(value: boolean) {this._protesis_removible_necesaria = value;this.MarcarProtesisRemovibleNecesaria();if(this._protesis_removible_necesaria){if(this._protesis_removible_aplicada){this._protesis_removible_aplicada = false;}}else{if(!this._protesis_removible_aplicada){this.DesmarcarProtesisRemovibleNecesaria();}}}
    protected _protesis_removible_aplicada: boolean;
    public get protesis_removible_aplicada(): boolean {return this._protesis_removible_aplicada;}
    public set protesis_removible_aplicada(value: boolean) {this._protesis_removible_aplicada = value;if(this._protesis_removible_aplicada){this.MarcarProtesisRemovibleAplicada();if(this._protesis_removible_necesaria){this._protesis_removible_necesaria = false;}}else{if(!this._protesis_removible_necesaria){this.DesmarcarProtesisRemovibleAplicada();}}}
    protected _posicion_en_protesis_removible: string = 'centro';
    public get posicion_en_protesis_removible(): string {return this._posicion_en_protesis_removible;}
    public set posicion_en_protesis_removible(value: string) {this._posicion_en_protesis_removible = value;}//inicio centro final o inicio_final

    protected _protesis_corona_necesaria: boolean;
    public get protesis_corona_necesaria(): boolean {return this._protesis_corona_necesaria;}
    public set protesis_corona_necesaria(value: boolean) {this._protesis_corona_necesaria = value;this.MarcarProtesisCoronaNecesaria();if(this._protesis_corona_necesaria){if(this._protesis_corona_aplicada){this._protesis_corona_aplicada = false;}}else{if(!this._protesis_corona_aplicada){this.DesmarcarProtesisCoronaNecesaria();}}}
    protected _protesis_corona_aplicada: boolean;
    public get protesis_corona_aplicada(): boolean {return this._protesis_corona_aplicada;}
    public set protesis_corona_aplicada(value: boolean) {this._protesis_corona_aplicada = value;if(this._protesis_corona_aplicada){this.MarcarProtesisCoronaAplicada();if(this._protesis_corona_necesaria){this._protesis_corona_necesaria = false;}}else{if(!this._protesis_corona_necesaria){this.DesmarcarProtesisCoronaAplicada();}}}
    protected _posicion_en_protesis_corona: string = 'centro';
    public get posicion_en_protesis_corona(): string {return this._posicion_en_protesis_corona;}
    public set posicion_en_protesis_corona(value: string) {this._posicion_en_protesis_corona = value;}//inicio centro final o inicio_final

    public cara_izquierda:Cara
    public cara_arriba:Cara
    public cara_derecha:Cara
    public cara_abajo:Cara
    public cara_centro:Cara

    public referencia_grafico_numero: any
    protected referencia_grafico_endodoncia: any
    protected referencia_grafico_extraccion: any
    protected referencia_grafico_sellante: any

    public referencia_grafico_protesis_total: any
    public referencia_grafico_protesis_fija: any
    public referencia_grafico_protesis_removible: any
    public referencia_grafico_protesis_corona: any

    constructor(lienzo, numero_pieza,odontograma_component:OdontogramaComponent){
        super(lienzo)
        this.componente_odontograma = odontograma_component;
        this.referencia_grafico_svg = this.lienzo.group()
        this.referencia_grafico_svg.addClass('pieza')
        this.GenerarGrafico()
        this.GenerarEventos()
        this.referencia_grafico_numero = this.lienzo.text(""+this.numero_pieza).fill('#1D96CF')
        this.numero_pieza = numero_pieza
        this.referencia_grafico_numero.translate(
            this.posicion_x+this.grosor_linea+40*this.escala/2-this.referencia_grafico_numero.length()/2,
            this.posicion_y+this.grosor_linea+40*this.escala+5)
       // this.referencia_grafico_numero.font({weight :  'bold'})
    }

    protected GenerarEventos(): void{
        //EFECTO AL PASAR EL MOUSE SOBRE UNA PIEZA
        this.referencia_grafico_svg.on('mouseenter', (evento)=>{
            if(!(evento instanceof MouseEvent) ||OdontogramaComponent.detalle_a_aplicar == ""||OdontogramaComponent.detalle_a_aplicar == "borrar"||OdontogramaComponent.detalle_a_aplicar == "obturacion"||OdontogramaComponent.detalle_a_aplicar == "caries"||
            OdontogramaComponent.detalle_a_aplicar == "sellante_aplicado"||OdontogramaComponent.detalle_a_aplicar == "sellante_necesario" ||
            OdontogramaComponent.detalle_a_aplicar == "extraccion_necesaria"||OdontogramaComponent.detalle_a_aplicar == "perdida_caries"||
            OdontogramaComponent.detalle_a_aplicar == "perdida_otra_causa" ||OdontogramaComponent.detalle_a_aplicar == "endodoncia_aplicada"||
            OdontogramaComponent.detalle_a_aplicar == "endodoncia_necesaria"||OdontogramaComponent.detalle_a_aplicar == "protesis_corona_necesaria"||
            OdontogramaComponent.detalle_a_aplicar == "protesis_corona_aplicada"||OdontogramaComponent.detalle_a_aplicar == "protesis_removible_necesaria"||
            OdontogramaComponent.detalle_a_aplicar == "protesis_removible_aplicada"||OdontogramaComponent.detalle_a_aplicar == "protesis_fija_necesaria"||
            OdontogramaComponent.detalle_a_aplicar == "protesis_fija_aplicada"){
                this.grosor_linea = this.grosor_linea+1

                if(this.protesis_corona_aplicada || this.protesis_corona_necesaria){
                    console.log("agrandando protesis")
                    this.referencia_grafico_protesis_corona.translate(this.posicion_x+this.grosor_linea+(this.escala*10) - 9 +1 ,this.posicion_y+this.grosor_linea+(this.escala*10)-9 +1)
                }

                if(OdontogramaComponent.detalle_a_aplicar == ""||OdontogramaComponent.detalle_a_aplicar == "borrar"||OdontogramaComponent.detalle_a_aplicar == "obturacion"||OdontogramaComponent.detalle_a_aplicar == "caries"){
                    return;
                }

                this.cara_abajo.referencia_grafico_svg.fire('mouseenter');
                this.cara_arriba.referencia_grafico_svg.fire('mouseenter');
                this.cara_izquierda.referencia_grafico_svg.fire('mouseenter');
                this.cara_derecha.referencia_grafico_svg.fire('mouseenter');
                this.cara_centro.referencia_grafico_svg.fire('mouseenter');

                
            }

        })
        this.referencia_grafico_svg.on('mouseleave', (evento)=>{
            if(!(evento instanceof MouseEvent) ||OdontogramaComponent.detalle_a_aplicar == ""||OdontogramaComponent.detalle_a_aplicar == "borrar"||OdontogramaComponent.detalle_a_aplicar == "caries"||OdontogramaComponent.detalle_a_aplicar == "obturacion"||
            OdontogramaComponent.detalle_a_aplicar == "sellante_aplicado"||OdontogramaComponent.detalle_a_aplicar == "sellante_necesario" ||
            OdontogramaComponent.detalle_a_aplicar == "extraccion_necesaria"||OdontogramaComponent.detalle_a_aplicar == "perdida_caries"||
            OdontogramaComponent.detalle_a_aplicar == "perdida_otra_causa" ||OdontogramaComponent.detalle_a_aplicar == "endodoncia_aplicada"||
            OdontogramaComponent.detalle_a_aplicar == "endodoncia_necesaria"||OdontogramaComponent.detalle_a_aplicar == "protesis_corona_necesaria"||
            OdontogramaComponent.detalle_a_aplicar == "protesis_corona_aplicada"||OdontogramaComponent.detalle_a_aplicar == "protesis_removible_necesaria"||
            OdontogramaComponent.detalle_a_aplicar == "protesis_removible_aplicada"||OdontogramaComponent.detalle_a_aplicar == "protesis_fija_necesaria"||
            OdontogramaComponent.detalle_a_aplicar == "protesis_fija_aplicada"){
                this.grosor_linea = this.grosor_linea-1
                if(this.protesis_corona_aplicada || this.protesis_corona_necesaria){
                    console.log("agrandando protesis")
                    this.referencia_grafico_protesis_corona.translate(this.posicion_x+this.grosor_linea+(this.escala*10) - 9 +1 ,this.posicion_y+this.grosor_linea+(this.escala*10)-9 +1)
                }
                if(OdontogramaComponent.detalle_a_aplicar == ""||OdontogramaComponent.detalle_a_aplicar == "borrar"||OdontogramaComponent.detalle_a_aplicar == "obturacion"||OdontogramaComponent.detalle_a_aplicar == "caries"){
                    return;
                }

                this.cara_abajo.referencia_grafico_svg.fire('mouseleave');
                this.cara_arriba.referencia_grafico_svg.fire('mouseleave');
                this.cara_izquierda.referencia_grafico_svg.fire('mouseleave');
                this.cara_derecha.referencia_grafico_svg.fire('mouseleave');
                this.cara_centro.referencia_grafico_svg.fire('mouseleave');
            }

        })

        this.referencia_grafico_svg.on('click', ()=>{
            //EVENTOS DE DETALLES SIMPLES
            if(OdontogramaComponent.detalle_a_aplicar == "sellante_aplicado" && !this.sellante_aplicado){
                this.sellante_aplicado = true
                this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
            }
            else if(OdontogramaComponent.detalle_a_aplicar == "sellante_necesario" && !this.sellante_necesario){
                this.sellante_necesario = true
                this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
            }
            else if(OdontogramaComponent.detalle_a_aplicar == "extraccion_necesaria" && !this.extraccion_necesaria){
                this.extraccion_necesaria = true
                this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
            }
            else if(OdontogramaComponent.detalle_a_aplicar == "perdida_caries" && !this.extraccion_aplicada_caries){
                this.extraccion_aplicada_caries = true
                this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
            }
            else if(OdontogramaComponent.detalle_a_aplicar == "perdida_otra_causa" && !this.extraccion_aplicada_otro){
                this.extraccion_aplicada_otro = true
                this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
            }
            else if(OdontogramaComponent.detalle_a_aplicar == "endodoncia_aplicada" && !this.endodoncia_aplicada){
                this.endodoncia_aplicada = true
                this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
            }
            else if(OdontogramaComponent.detalle_a_aplicar == "endodoncia_necesaria" && !this.endodoncia_necesaria){
                this.endodoncia_necesaria = true
                this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
            }
            //EVENTOS DE DETALLES COMPUESTOS
            else if(OdontogramaComponent.detalle_a_aplicar == "protesis_fija_necesaria"){
                if(this.componente_odontograma.auxiliar_protesis_fija == null){
                    this.componente_odontograma.auxiliar_protesis_fija = new ProtesisFija(this.lienzo, this.componente_odontograma);
                    this.componente_odontograma.auxiliar_protesis_fija.aplicada = false;
                    this.componente_odontograma.mensaje_finalizar_cancelar.opacity(0.55)
                }
                else if(this.componente_odontograma.auxiliar_protesis_fija.ContienePieza(this)){
                    this.componente_odontograma.protesis.push(this.componente_odontograma.auxiliar_protesis_fija);
                    this.componente_odontograma.auxiliar_protesis_fija.referencia_grafico_svg.opacity(1);
                    this.componente_odontograma.mensaje_finalizar_cancelar.opacity(0)
                    this.componente_odontograma.auxiliar_protesis_fija = null;
                    this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
                    return;
                }
                this.componente_odontograma.auxiliar_protesis_fija.AgregarPieza(this);
            }
            else if(OdontogramaComponent.detalle_a_aplicar == "protesis_fija_aplicada"){
                if(this.componente_odontograma.auxiliar_protesis_fija == null){
                    this.componente_odontograma.auxiliar_protesis_fija = new ProtesisFija(this.lienzo, this.componente_odontograma);
                    this.componente_odontograma.auxiliar_protesis_fija.aplicada = true;
                    this.componente_odontograma.mensaje_finalizar_cancelar.opacity(0.55)
                }
                else if(this.componente_odontograma.auxiliar_protesis_fija.ContienePieza(this)){
                    this.componente_odontograma.protesis.push(this.componente_odontograma.auxiliar_protesis_fija);
                    this.componente_odontograma.auxiliar_protesis_fija.referencia_grafico_svg.opacity(1);
                    this.componente_odontograma.mensaje_finalizar_cancelar.opacity(0)
                    if(this.componente_odontograma.auxiliar_protesis_fija.aplicada){
                        this.componente_odontograma.auxiliar_protesis_fija.piezas.forEach((pieza)=>{
                          pieza.QuitarTodosDetalles();
                        });
                    }
                    this.componente_odontograma.auxiliar_protesis_fija = null;
                    this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
                    return;
                }
                this.componente_odontograma.auxiliar_protesis_fija.AgregarPieza(this);
            }

            else if(OdontogramaComponent.detalle_a_aplicar == "protesis_removible_necesaria"){
                if(this.componente_odontograma.auxiliar_protesis_removible == null){
                    this.componente_odontograma.auxiliar_protesis_removible = new ProtesisRemovible(this.lienzo, this.componente_odontograma);
                    this.componente_odontograma.auxiliar_protesis_removible.aplicada = false;
                    this.componente_odontograma.mensaje_finalizar_cancelar.opacity(0.55)
                }
                else if(this.componente_odontograma.auxiliar_protesis_removible.ContienePieza(this)){
                    this.componente_odontograma.protesis.push(this.componente_odontograma.auxiliar_protesis_removible);
                    this.componente_odontograma.auxiliar_protesis_removible.referencia_grafico_svg.opacity(1);
                    this.componente_odontograma.mensaje_finalizar_cancelar.opacity(0)
                    this.componente_odontograma.auxiliar_protesis_removible = null;
                    this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
                    return;
                }
                this.componente_odontograma.auxiliar_protesis_removible.AgregarPieza(this);
            }
            else if(OdontogramaComponent.detalle_a_aplicar == "protesis_removible_aplicada"){
                if(this.componente_odontograma.auxiliar_protesis_removible == null){
                    this.componente_odontograma.auxiliar_protesis_removible = new ProtesisRemovible(this.lienzo, this.componente_odontograma);
                    this.componente_odontograma.auxiliar_protesis_removible.aplicada = true;
                    this.componente_odontograma.mensaje_finalizar_cancelar.opacity(0.55)
                }
                else if(this.componente_odontograma.auxiliar_protesis_removible.ContienePieza(this)){
                    this.componente_odontograma.protesis.push(this.componente_odontograma.auxiliar_protesis_removible);
                    this.componente_odontograma.auxiliar_protesis_removible.referencia_grafico_svg.opacity(1);
                    this.componente_odontograma.mensaje_finalizar_cancelar.opacity(0)
                    if(this.componente_odontograma.auxiliar_protesis_removible.aplicada){
                        this.componente_odontograma.auxiliar_protesis_removible.piezas.forEach((pieza)=>{
                          pieza.QuitarTodosDetalles();
                        });
                    }
                    this.componente_odontograma.auxiliar_protesis_removible = null;
                    this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
                    return;
                }
                this.componente_odontograma.auxiliar_protesis_removible.AgregarPieza(this);
            }
            else if(OdontogramaComponent.detalle_a_aplicar == "protesis_corona_necesaria"){
                if(this.componente_odontograma.auxiliar_protesis_corona == null){
                    this.componente_odontograma.auxiliar_protesis_corona = new ProtesisCorona(this.lienzo, this.componente_odontograma);
                    this.componente_odontograma.auxiliar_protesis_corona.aplicada = false;
                    this.componente_odontograma.mensaje_finalizar_cancelar.opacity(0.55)
                    //BORRAR LAS SIGUIENTES 6 LINEAS EN CASO DE QUE UNA CORONA PUEDA CONTENER VARIAS PIEZAS
                    this.componente_odontograma.auxiliar_protesis_corona.AgregarPieza(this);
                    this.componente_odontograma.protesis.push(this.componente_odontograma.auxiliar_protesis_corona);
                    this.componente_odontograma.auxiliar_protesis_corona.referencia_grafico_svg.opacity(1);
                    this.componente_odontograma.mensaje_finalizar_cancelar.opacity(0)
                    this.componente_odontograma.auxiliar_protesis_corona = null;
                    this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
                    return;
                }
                else if(this.componente_odontograma.auxiliar_protesis_corona.ContienePieza(this)){
                    this.componente_odontograma.protesis.push(this.componente_odontograma.auxiliar_protesis_corona);
                    this.componente_odontograma.auxiliar_protesis_corona.referencia_grafico_svg.opacity(1);
                    this.componente_odontograma.mensaje_finalizar_cancelar.opacity(0)
                    this.componente_odontograma.auxiliar_protesis_corona = null;
                    this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
                    return;
                }
                this.componente_odontograma.auxiliar_protesis_corona.AgregarPieza(this);
            }
            else if(OdontogramaComponent.detalle_a_aplicar == "protesis_corona_aplicada"){
                if(this.componente_odontograma.auxiliar_protesis_corona == null){
                    this.componente_odontograma.auxiliar_protesis_corona = new ProtesisCorona(this.lienzo, this.componente_odontograma);
                    this.componente_odontograma.auxiliar_protesis_corona.aplicada = true;
                    this.componente_odontograma.mensaje_finalizar_cancelar.opacity(0.55)
                    //BORRAR LAS SIGUIENTES 6 LINEAS EN CASO DE QUE UNA CORONA PUEDA CONTENER VARIAS PIEZAS
                    this.componente_odontograma.auxiliar_protesis_corona.AgregarPieza(this);
                    this.componente_odontograma.protesis.push(this.componente_odontograma.auxiliar_protesis_corona);
                    this.componente_odontograma.auxiliar_protesis_corona.referencia_grafico_svg.opacity(1);
                    this.componente_odontograma.mensaje_finalizar_cancelar.opacity(0)
                    if(this.componente_odontograma.auxiliar_protesis_corona.aplicada){
                        this.componente_odontograma.auxiliar_protesis_corona.piezas.forEach((pieza)=>{
                          pieza.QuitarTodosDetalles();
                        });
                    }
                    this.componente_odontograma.auxiliar_protesis_corona = null;
                    this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
                    return;

                }
                else if(this.componente_odontograma.auxiliar_protesis_corona.ContienePieza(this)){
                    this.componente_odontograma.protesis.push(this.componente_odontograma.auxiliar_protesis_corona);
                    this.componente_odontograma.auxiliar_protesis_corona.referencia_grafico_svg.opacity(1);
                    this.componente_odontograma.mensaje_finalizar_cancelar.opacity(0)
                    this.componente_odontograma.auxiliar_protesis_corona = null;
                    this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
                    return;
                }
                this.componente_odontograma.auxiliar_protesis_corona.AgregarPieza(this);
            }

        })




    }

    //SOBRESCRITURA DE MÉTODOS GRÁFICOS DE GraficoSVG
    protected ActualizarEscala(): void {
        this.cara_izquierda.escala = this.escala
        this.cara_arriba.escala = this.escala
        this.cara_derecha.escala = this.escala
        this.cara_abajo.escala = this.escala
        this.cara_centro.escala = this.escala
        this.referencia_grafico_numero.translate(
            this.posicion_x+this.grosor_linea+40*this.escala/2-this.referencia_grafico_numero.length()/2
           ,this.posicion_y+this.grosor_linea+40*this.escala+5)
        //Actualizando Escala De Endodoncia
        try{
            this.referencia_grafico_endodoncia.plot('M '+(this.escala*10)+' 0 L 0 '+(this.escala*20)+' L '+(this.escala*20)+' '+(this.escala*20)+' L '+(this.escala*10)+' 0');
        }catch(error){}
        //Actualizando Escala De Extracción
        if(!this._extraccion_aplicada_otro){
            try{
                this.referencia_grafico_extraccion.plot('M '+(this.escala*10)+' '+(this.escala*10)+' L 0 '+(this.escala*20)+' L '+(this.escala*20)+' 0 L '+(this.escala*10)+' '+(this.escala*10)+' L 0 0 L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*20)+' '+(this.escala*20)+'');
            }catch(error){}
        }
        else{
            try{
                this.referencia_grafico_extraccion.plot('M '+(this.escala*3)+' '+(this.escala*17)+' A 1 1 0 0 0 '+(this.escala*17)+' '+(this.escala*3)+' A 1 1 0 0 0 '+(this.escala*3)+' '+(this.escala*17)+' L '+(this.escala*17)+' '+(this.escala*3)+' L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*3)+' '+(this.escala*3)+' L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*17)+' '+(this.escala*17)+' L '+(this.escala*10)+' '+(this.escala*10)+'');
            }catch(error){}
        }
        //Actualizando Escala De Sellante
        try{
            this.referencia_grafico_sellante.plot('M '+(this.escala*2.5)+' '+(this.escala*2.5)+' L '+(this.escala*17.5)+' '+(this.escala*17.5)+' L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*17.5)+' '+(this.escala*2.5)+' L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*2.5)+' '+(this.escala*17.5)+' L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*10)+' 0 L '+(this.escala*10)+' '+(this.escala*20)+' L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*10)+' '+(this.escala*10)+' L 0 '+(this.escala*10)+' L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*20)+' '+(this.escala*10)+' L '+(this.escala*10)+' '+(this.escala*10)+'');
        }catch(error){}
        this.ActualizarPosicion();
    }
    protected ActualizarGrosorLinea(): void {
        this.cara_izquierda.grosor_linea = this.grosor_linea
        this.cara_arriba.grosor_linea = this.grosor_linea
        this.cara_derecha.grosor_linea = this.grosor_linea
        this.cara_abajo.grosor_linea = this.grosor_linea
        this.cara_centro.grosor_linea = this.grosor_linea

        //Actualizando Grosor De Linea De Endodoncia
        try{
            this.referencia_grafico_endodoncia.stroke({width: this.grosor_linea+3})
        }catch(error){}
        //Actualizando Grosor De Linea De Extracción
        try{
            this.referencia_grafico_extraccion.stroke({width: this.grosor_linea+3})
        }catch(error){}
        //Actualizando Grosor De Linea De Sellante
        try{
            this.referencia_grafico_sellante.stroke({width: this.grosor_linea+3})
        }catch(error){}

        this.ActualizarPosicion();
    }
    protected ActualizarColorLinea(): void {
        this.cara_izquierda.color_linea = this.color_linea
        this.cara_arriba.color_linea = this.color_linea
        this.cara_derecha.color_linea = this.color_linea
        this.cara_abajo.color_linea = this.color_linea
        this.cara_centro.color_linea = this.color_linea
    }
    protected ActualizarColorRelleno(): void {
        console.log('No hay relleno');
    }
    protected ActualizarFormaFinLinea(): void {
        this.cara_izquierda.forma_fin_linea = this.forma_fin_linea
        this.cara_arriba.forma_fin_linea = this.forma_fin_linea
        this.cara_derecha.forma_fin_linea = this.forma_fin_linea
        this.cara_abajo.forma_fin_linea = this.forma_fin_linea
        this.cara_centro.forma_fin_linea = this.forma_fin_linea
    }
    protected ActualizarPosicion(): void {
        this.cara_izquierda.posicion_x = this.posicion_x
        this.cara_arriba.posicion_x = this.posicion_x
        this.cara_derecha.posicion_x = this.posicion_x
        this.cara_abajo.posicion_x = this.posicion_x
        this.cara_centro.posicion_x = this.posicion_x

        this.cara_izquierda.posicion_y = this.posicion_y
        this.cara_arriba.posicion_y = this.posicion_y
        this.cara_derecha.posicion_y = this.posicion_y
        this.cara_abajo.posicion_y = this.posicion_y
        this.cara_centro.posicion_y = this.posicion_y
        this.referencia_grafico_numero.translate(
            this.posicion_x+this.grosor_linea+40*this.escala/2-this.referencia_grafico_numero.length()/2
           ,this.posicion_y+this.grosor_linea+40*this.escala+5)

        //Actualizando Posicion Endodoncia
        try{
            this.referencia_grafico_endodoncia.translate(this.posicion_x+this.grosor_linea+(this.escala*10),this.posicion_y+this.grosor_linea+(this.escala*10))
        }catch(error){}//dará error si no hay endodoncia en la pieza
        //Actualizando Posicion Extracción
        try{
            this.referencia_grafico_extraccion.translate(this.posicion_x+this.grosor_linea+(this.escala*10),this.posicion_y+this.grosor_linea+(this.escala*10))
        }catch(error){}//dará error si no hay extraccion en la pieza
        //Actualizando Posicion Sellante
        try{
            this.referencia_grafico_sellante.translate(this.posicion_x+this.grosor_linea+(this.escala*10),this.posicion_y+this.grosor_linea+(this.escala*10))
        }catch(error){}//dará error si no hay sellante en la pieza
        /*//Actualizando Posicion Protesis Total
        try{
            this.referencia_grafico_protesis_total.translate(this.posicion_x+this.grosor_linea+(this.escala*10) -20 ,this.posicion_y+this.grosor_linea+(this.escala*10))
        }catch(error){}//dará error si no hay sellante en la pieza*/

    }




    //******************************************DETALLES ODONTOLÓGICOS*******************************************
    //Endodoncias
    protected MarcarEndodonciaNecesaria():void{//M 20 0 L 0 40 L 40 40 L 20 0
        try{
            this.referencia_grafico_endodoncia.remove();
            this.referencia_grafico_endodoncia = null
        }catch(error){}
        this.referencia_grafico_endodoncia = this.lienzo.path('M '+(this.escala*10)+' 0 L 0 '+(this.escala*20)+' L '+(this.escala*20)+' '+(this.escala*20)+' L '+(this.escala*10)+' 0');
        this.referencia_grafico_endodoncia.stroke({width: this.grosor_linea+3})
        this.referencia_grafico_endodoncia.stroke({color: '#E90F07'})
        this.referencia_grafico_endodoncia.stroke({linecap: this.forma_fin_linea , linejoin: this.forma_fin_linea })
        this.referencia_grafico_endodoncia.translate(this.posicion_x+this.grosor_linea+(this.escala*10),this.posicion_y+this.grosor_linea+(this.escala*10))
        this.referencia_grafico_endodoncia.fill({opacity: 0.0});//relleno transparente
        this.referencia_grafico_svg.add(this.referencia_grafico_endodoncia)
        this.referencia_grafico_endodoncia.on('click', ()=>{
            if(OdontogramaComponent.detalle_a_aplicar == "borrar"){
                this.endodoncia_necesaria = false;
                this.grosor_linea-=1
            }
            else if(OdontogramaComponent.detalle_a_aplicar == "endodoncia_aplicada"){
                this.grosor_linea-=1;
            }
        });
    }
    protected DesmarcarEndodonciaNecesaria():void{
        try{
            this.referencia_grafico_endodoncia.remove();
        }catch(error){}
        this.referencia_grafico_endodoncia = null
    }
    protected MarcarEndodonciaAplicada():void{
        try{
            this.referencia_grafico_endodoncia.remove();
            this.referencia_grafico_endodoncia = null
        }catch(error){}
        this.referencia_grafico_endodoncia = this.lienzo.path('M '+(this.escala*10)+' 0 L 0 '+(this.escala*20)+' L '+(this.escala*20)+' '+(this.escala*20)+' L '+(this.escala*10)+' 0');
        this.referencia_grafico_endodoncia.stroke({width: this.grosor_linea+3})
        this.referencia_grafico_endodoncia.stroke({color: '#0765E9'})
        this.referencia_grafico_endodoncia.stroke({linecap: this.forma_fin_linea , linejoin: this.forma_fin_linea })
        this.referencia_grafico_endodoncia.translate(this.posicion_x+this.grosor_linea+(this.escala*10),this.posicion_y+this.grosor_linea+(this.escala*10))
        this.referencia_grafico_endodoncia.fill({opacity: 0.0});//relleno transparente
        this.referencia_grafico_svg.add(this.referencia_grafico_endodoncia)
        this.referencia_grafico_endodoncia.on('click', ()=>{
            if(OdontogramaComponent.detalle_a_aplicar == "borrar"){
                this.endodoncia_aplicada = false;
                this.grosor_linea-=1
            }
            else if(OdontogramaComponent.detalle_a_aplicar == "endodoncia_necesaria"){
                this.grosor_linea-=1;
            }
        });

    }
    protected DesmarcarEndodonciaAplicada():void{
        try{
            this.referencia_grafico_endodoncia.remove();
        }catch(error){}
        this.referencia_grafico_endodoncia = null
    }

    //Extracciones
    protected MarcarExtraccionNecesaria():void{
        try{
            this.referencia_grafico_extraccion.remove();
            this.referencia_grafico_extraccion = null
        }catch(error){}
        this.referencia_grafico_extraccion = this.lienzo.path('M '+(this.escala*10)+' '+(this.escala*10)+' L 0 '+(this.escala*20)+' L '+(this.escala*20)+' 0 L '+(this.escala*10)+' '+(this.escala*10)+' L 0 0 L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*20)+' '+(this.escala*20)+'');
        this.referencia_grafico_extraccion.stroke({width: this.grosor_linea+3})
        this.referencia_grafico_extraccion.stroke({color: '#E90F07'})
        this.referencia_grafico_extraccion.stroke({linecap: this.forma_fin_linea , linejoin: this.forma_fin_linea })
        this.referencia_grafico_extraccion.translate(this.posicion_x+this.grosor_linea+(this.escala*10),this.posicion_y+this.grosor_linea+(this.escala*10))
        this.referencia_grafico_extraccion.fill({opacity: 0.0});
        this.referencia_grafico_svg.add(this.referencia_grafico_extraccion)
        this.referencia_grafico_extraccion.on('click', ()=>{
            if(OdontogramaComponent.detalle_a_aplicar == "borrar"){
                this.extraccion_necesaria = false;
                this.grosor_linea-=1
            }
            else if(OdontogramaComponent.detalle_a_aplicar == "perdida_caries" || OdontogramaComponent.detalle_a_aplicar == "perdida_otra_causa"){
                this.grosor_linea-=1;
            }
        });

    }
    protected DesmarcarExtraccionNecesaria():void{
        try{
            this.referencia_grafico_extraccion.remove();
        }catch(error){}

        this.referencia_grafico_extraccion = null
    }

    protected MarcarExtraccionAplicadaCaries():void{
        try{
            this.referencia_grafico_extraccion.remove();
            this.referencia_grafico_extraccion = null
        }catch(error){}
        this.QuitarTodosDetalles();
        this._extraccion_aplicada_caries = true;//porque quitar detalles marca en false esta propiedad
        this.referencia_grafico_extraccion = this.lienzo.path('M '+(this.escala*10)+' '+(this.escala*10)+' L 0 '+(this.escala*20)+' L '+(this.escala*20)+' 0 L '+(this.escala*10)+' '+(this.escala*10)+' L 0 0 L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*20)+' '+(this.escala*20)+'');
        this.referencia_grafico_extraccion.stroke({width: this.grosor_linea+3})
        this.referencia_grafico_extraccion.stroke({color: '#0765E9'})
        this.referencia_grafico_extraccion.stroke({linecap: this.forma_fin_linea , linejoin: this.forma_fin_linea })
        this.referencia_grafico_extraccion.translate(this.posicion_x+this.grosor_linea+(this.escala*10),this.posicion_y+this.grosor_linea+(this.escala*10))
        this.referencia_grafico_extraccion.fill({opacity: 0.0});
        this.referencia_grafico_svg.add(this.referencia_grafico_extraccion)
        this.referencia_grafico_extraccion.on('click', ()=>{
            if(OdontogramaComponent.detalle_a_aplicar == "borrar"){
                this.extraccion_aplicada_caries = false;
                this.grosor_linea-=1
            }
            else if(OdontogramaComponent.detalle_a_aplicar == "extraccion_necesaria" || OdontogramaComponent.detalle_a_aplicar == "perdida_otra_causa"){
                this.grosor_linea-=1;
            }
        });

    }
    protected DesmarcarExtraccionAplicadaCaries():void{
        try{
            this.referencia_grafico_extraccion.remove();
        }catch(error){}
        this.referencia_grafico_extraccion = null
    }

    protected MarcarExtraccionAplicadaOtro():void{
        try{
            this.referencia_grafico_extraccion.remove();
            this.referencia_grafico_extraccion = null
        }catch(error){}
        this.QuitarTodosDetalles();
        this._extraccion_aplicada_otro = true;//porque quitar detalles marca en false esta propiedad
        this.referencia_grafico_extraccion = this.lienzo.path('M '+(this.escala*3)+' '+(this.escala*17)+' A 1 1 0 0 0 '+(this.escala*17)+' '+(this.escala*3)+' A 1 1 0 0 0 '+(this.escala*3)+' '+(this.escala*17)+' L '+(this.escala*17)+' '+(this.escala*3)+' L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*3)+' '+(this.escala*3)+' L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*17)+' '+(this.escala*17)+' L '+(this.escala*10)+' '+(this.escala*10)+'');
        this.referencia_grafico_extraccion.stroke({width: this.grosor_linea+3})
        this.referencia_grafico_extraccion.stroke({color: '#0765E9'})
        this.referencia_grafico_extraccion.stroke({linecap: this.forma_fin_linea , linejoin: this.forma_fin_linea })
        this.referencia_grafico_extraccion.translate(this.posicion_x+this.grosor_linea+(this.escala*10),this.posicion_y+this.grosor_linea+(this.escala*10))
        this.referencia_grafico_extraccion.fill({opacity: 0.0});
        this.referencia_grafico_svg.add(this.referencia_grafico_extraccion)
        this.referencia_grafico_extraccion.on('click', ()=>{
            if(OdontogramaComponent.detalle_a_aplicar == "borrar"){
                this.extraccion_aplicada_otro = false;
                this.grosor_linea-=1
            }
            else if(OdontogramaComponent.detalle_a_aplicar == "perdida_caries" || OdontogramaComponent.detalle_a_aplicar == "extraccion_necesaria"){
                this.grosor_linea-=1;
            }
        });

    }
    protected DesmarcarExtraccionAplicadaOtro():void{
        try{
            this.referencia_grafico_extraccion.remove();
        }catch(error){}
        this.referencia_grafico_extraccion = null
    }

    //Sellantes
    protected MarcarSellanteNecesario():void{
        try{
            this.referencia_grafico_sellante.remove();

        }catch(error){}
        this.referencia_grafico_sellante = null
        this.referencia_grafico_sellante = this.lienzo.path('M '+(this.escala*2.5)+' '+(this.escala*2.5)+' L '+(this.escala*17.5)+' '+(this.escala*17.5)+' L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*17.5)+' '+(this.escala*2.5)+' L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*2.5)+' '+(this.escala*17.5)+' L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*10)+' 0 L '+(this.escala*10)+' '+(this.escala*20)+' L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*10)+' '+(this.escala*10)+' L 0 '+(this.escala*10)+' L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*20)+' '+(this.escala*10)+' L '+(this.escala*10)+' '+(this.escala*10)+'');
        this.referencia_grafico_sellante.stroke({width: this.grosor_linea+3})
        this.referencia_grafico_sellante.stroke({color: '#E90F07'})
        this.referencia_grafico_sellante.stroke({linecap: this.forma_fin_linea , linejoin: this.forma_fin_linea })
        this.referencia_grafico_sellante.translate(this.posicion_x+this.grosor_linea+(this.escala*10),this.posicion_y+this.grosor_linea+(this.escala*10))
        this.referencia_grafico_sellante.fill({opacity: 0.0});
        this.referencia_grafico_svg.add(this.referencia_grafico_sellante)
        this.referencia_grafico_sellante.on('click', ()=>{
            if(OdontogramaComponent.detalle_a_aplicar == "borrar"){
                this.sellante_necesario = false;
                this.grosor_linea-=1
            }
            else if(OdontogramaComponent.detalle_a_aplicar == "sellante_aplicado"){
                this.grosor_linea-=1;
            }
        });

    }
    protected DesmarcarSellanteNecesario():void{
        try{
            this.referencia_grafico_sellante.remove();
        }catch(error){}
        this.referencia_grafico_sellante = null
    }
    protected MarcarSellanteAplicado():void{
        try{
            this.referencia_grafico_sellante.remove();

        }catch(error){}
        this.referencia_grafico_sellante = null
        this.referencia_grafico_sellante = this.lienzo.path('M '+(this.escala*2.5)+' '+(this.escala*2.5)+' L '+(this.escala*17.5)+' '+(this.escala*17.5)+' L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*17.5)+' '+(this.escala*2.5)+' L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*2.5)+' '+(this.escala*17.5)+' L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*10)+' 0 L '+(this.escala*10)+' '+(this.escala*20)+' L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*10)+' '+(this.escala*10)+' L 0 '+(this.escala*10)+' L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*20)+' '+(this.escala*10)+' L '+(this.escala*10)+' '+(this.escala*10)+'');
        this.referencia_grafico_sellante.stroke({width: this.grosor_linea+3})
        this.referencia_grafico_sellante.stroke({color: '#0765E9'})
        this.referencia_grafico_sellante.stroke({linecap: this.forma_fin_linea , linejoin: this.forma_fin_linea })
        this.referencia_grafico_sellante.translate(this.posicion_x+this.grosor_linea+(this.escala*10),this.posicion_y+this.grosor_linea+(this.escala*10))
        this.referencia_grafico_sellante.fill({opacity: 0.0});
        this.referencia_grafico_svg.add(this.referencia_grafico_sellante)
        this.referencia_grafico_sellante.on('click', ()=>{
            if(OdontogramaComponent.detalle_a_aplicar == "borrar"){
                this.sellante_aplicado = false;
                this.grosor_linea-=1
            }
            else if(OdontogramaComponent.detalle_a_aplicar == "sellante_necesario"){
                this.grosor_linea-=1;
            }
        });

    }
    protected DesmarcarSellanteAplicado():void{
        try{
            this.referencia_grafico_sellante.remove();
        }catch(error){}
        this.referencia_grafico_sellante = null
    }

    //PROTESIS TOTAL
    protected MarcarProtesisTotalNecesaria():void{
        try{
            this.referencia_grafico_protesis_total.remove();

        }catch(error){}
        this.referencia_grafico_protesis_total = null

        if(this.numero_pieza == 41 ||this.numero_pieza ==  11){
            this.referencia_grafico_protesis_total = this.lienzo.path('M 0 0 L '+(this.escala*80)+' 0 M 0 '+(this.escala*20)+' L '+(this.escala*80)+' '+(this.escala*20)+'');
        }
        else if(this.numero_pieza == 21 ||this.numero_pieza ==  31){
            this.referencia_grafico_protesis_total = this.lienzo.path('M '+(this.escala*-20)+' 0 L '+(this.escala*60)+' 0 M '+(this.escala*-20)+' '+(this.escala*20)+' L '+(this.escala*60)+' '+(this.escala*20)+'');
        }
        else{
            this.referencia_grafico_protesis_total = this.lienzo.path('M 0 0 L '+(this.escala*60)+' 0 M 0 '+(this.escala*20)+' L '+(this.escala*60)+' '+(this.escala*20)+'');
        }

        this.referencia_grafico_protesis_total.stroke({width: 4})
        this.referencia_grafico_protesis_total.stroke({color: '#E90F07'})
        this.referencia_grafico_protesis_total.stroke({linecap: this.forma_fin_linea , linejoin: this.forma_fin_linea })
        this.referencia_grafico_protesis_total.translate(this.posicion_x+this.grosor_linea+(this.escala*10) -20 ,this.posicion_y+this.grosor_linea+(this.escala*10))
        this.referencia_grafico_protesis_total.fill({opacity: 0.0});
        this.referencia_grafico_svg.add(this.referencia_grafico_protesis_total)
        //evento borrar está programado en la prótesis

    }
    protected DesmarcarProtesisTotalNecesaria():void{
        try{
            this.referencia_grafico_protesis_total.remove();
        }catch(error){}
        this.referencia_grafico_protesis_total = null
    }
    protected MarcarProtesisTotalAplicada():void{
        try{
            this.referencia_grafico_protesis_total.remove();

        }catch(error){}
        this.referencia_grafico_protesis_total = null

        if(this.numero_pieza == 41 ||this.numero_pieza ==  11){
            this.referencia_grafico_protesis_total = this.lienzo.path('M 0 0 L '+(this.escala*80)+' 0 M 0 '+(this.escala*20)+' L '+(this.escala*80)+' '+(this.escala*20)+'');
        }
        else if(this.numero_pieza == 21 ||this.numero_pieza ==  31){
            this.referencia_grafico_protesis_total = this.lienzo.path('M '+(this.escala*-20)+' 0 L '+(this.escala*60)+' 0 M '+(this.escala*-20)+' '+(this.escala*20)+' L '+(this.escala*60)+' '+(this.escala*20)+'');
        }
        else{
            this.referencia_grafico_protesis_total = this.lienzo.path('M 0 0 L '+(this.escala*60)+' 0 M 0 '+(this.escala*20)+' L '+(this.escala*60)+' '+(this.escala*20)+'');
        }

        this.referencia_grafico_protesis_total.stroke({width: 4})
        this.referencia_grafico_protesis_total.stroke({color: '#0765E9'})
        this.referencia_grafico_protesis_total.stroke({linecap: this.forma_fin_linea , linejoin: this.forma_fin_linea })
        this.referencia_grafico_protesis_total.translate(this.posicion_x+this.grosor_linea+(this.escala*10) - 20,this.posicion_y+this.grosor_linea+(this.escala*10))
        this.referencia_grafico_protesis_total.fill({opacity: 0.0});
        this.referencia_grafico_svg.add(this.referencia_grafico_protesis_total)
        //evento borrar está programado en la prótesis

    }
    protected DesmarcarProtesisTotalAplicada():void{
        try{
            this.referencia_grafico_protesis_total.remove();
        }catch(error){}
        this.referencia_grafico_protesis_total = null

    }
    //PROTESIS FIJA
    protected MarcarProtesisFijaNecesaria():void{
        try{
            this.referencia_grafico_protesis_fija.remove();

        }catch(error){}
        this.referencia_grafico_protesis_fija = null
        if(this.posicion_en_protesis_fija == 'centro'){
            if(this.numero_pieza == 41 ||this.numero_pieza ==  11||this.numero_pieza ==  51||this.numero_pieza ==  81){
                this.referencia_grafico_protesis_fija = this.lienzo.path('M '+(8)+' '+(this.escala*10)+' L '+(this.escala*65)+' '+(this.escala*10)+'');
            }
            else if(this.numero_pieza == 21 ||this.numero_pieza ==  31||this.numero_pieza ==  61||this.numero_pieza ==  71){
                this.referencia_grafico_protesis_fija = this.lienzo.path('M '+(-13)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+'');
            }
            else{
                this.referencia_grafico_protesis_fija = this.lienzo.path('M '+(8)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+'');
            }
        }
        else if(this.posicion_en_protesis_fija == 'inicio'){
            if(this.numero_pieza == 41 ||this.numero_pieza ==  11||this.numero_pieza ==  51||this.numero_pieza ==  81){
                this.referencia_grafico_protesis_fija = this.lienzo.path('M '+(this.escala*11)+' '+(this.escala*10)+' L '+(this.escala*60)+' '+(this.escala*10)+' L '+(this.escala*11)+' '+(this.escala*10)+' L '+(this.escala*11)+' '+(this.escala*13)+' L '+(this.escala*5)+' '+(this.escala*13)+' L '+(this.escala*5)+' '+(this.escala*7)+' L '+(this.escala*11)+' '+(this.escala*7)+' L '+(this.escala*11)+' '+(this.escala*10)+'');
            }
            else{
                this.referencia_grafico_protesis_fija = this.lienzo.path('M '+(this.escala*11)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*11)+' '+(this.escala*10)+' L '+(this.escala*11)+' '+(this.escala*13)+' L '+(this.escala*5)+' '+(this.escala*13)+' L '+(this.escala*5)+' '+(this.escala*7)+' L '+(this.escala*11)+' '+(this.escala*7)+' L '+(this.escala*11)+' '+(this.escala*10)+'');
            }

        }
        else if(this.posicion_en_protesis_fija == 'final'){//final
            if(this.numero_pieza == 21 ||this.numero_pieza ==  31||this.numero_pieza ==  61||this.numero_pieza ==  71){
                this.referencia_grafico_protesis_fija = this.lienzo.path('M '+(this.escala*-1)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*7)+' L '+(this.escala*55)+' '+(this.escala*7)+' L '+(this.escala*55)+' '+(this.escala*13)+' L '+(this.escala*50)+' '+(this.escala*13)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*-1)+' '+(this.escala*10)+'');
            }
            else{
                this.referencia_grafico_protesis_fija = this.lienzo.path('M '+(this.escala*8)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*7)+' L '+(this.escala*55)+' '+(this.escala*7)+' L '+(this.escala*55)+' '+(this.escala*13)+' L '+(this.escala*50)+' '+(this.escala*13)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*8)+' '+(this.escala*10)+'');
            }
        }
        else{//inicio final
            this.referencia_grafico_protesis_fija = this.lienzo.path('M '+(this.escala*8)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*7)+' L '+(this.escala*55)+' '+(this.escala*7)+' L '+(this.escala*55)+' '+(this.escala*13)+' L '+(this.escala*50)+' '+(this.escala*13)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*8)+' '+(this.escala*10)+' L '+(this.escala*8)+' '+(this.escala*13)+' L '+(this.escala*3)+' '+(this.escala*13)+' L '+(this.escala*3)+' '+(this.escala*7)+' L '+(this.escala*8)+' '+(this.escala*7)+' L '+(this.escala*8)+' '+(this.escala*10)+'');
        }


        this.referencia_grafico_protesis_fija.stroke({width: 4})
        this.referencia_grafico_protesis_fija.stroke({color: '#E90F07'})
        this.referencia_grafico_protesis_fija.stroke({linecap: this.forma_fin_linea , linejoin: this.forma_fin_linea })
        this.referencia_grafico_protesis_fija.translate(this.posicion_x+this.grosor_linea+(this.escala*10) -20 ,this.posicion_y+this.grosor_linea+(this.escala*10))
        this.referencia_grafico_protesis_fija.fill({color: '#E90F07'});
        this.referencia_grafico_svg.add(this.referencia_grafico_protesis_fija)
        //evento borrar está programado en la prótesis

    }
    protected DesmarcarProtesisFijaNecesaria():void{
        try{
            this.referencia_grafico_protesis_fija.remove();
        }catch(error){}
        this.referencia_grafico_protesis_fija = null
    }
    protected MarcarProtesisFijaAplicada():void{
        try{
            this.referencia_grafico_protesis_fija.remove();

        }catch(error){}
        this.referencia_grafico_protesis_fija = null

        if(this.posicion_en_protesis_fija == 'centro'){
            if(this.numero_pieza == 41 ||this.numero_pieza ==  11||this.numero_pieza ==  51||this.numero_pieza ==  81){
                this.referencia_grafico_protesis_fija = this.lienzo.path('M '+(8)+' '+(this.escala*10)+' L '+(this.escala*65)+' '+(this.escala*10)+'');
            }
            else if(this.numero_pieza == 21 ||this.numero_pieza ==  31||this.numero_pieza ==  61||this.numero_pieza ==  71){
                this.referencia_grafico_protesis_fija = this.lienzo.path('M '+(-13)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+'');
            }
            else{
                this.referencia_grafico_protesis_fija = this.lienzo.path('M '+(8)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+'');
            }
        }
        else if(this.posicion_en_protesis_fija == 'inicio'){
            if(this.numero_pieza == 41 ||this.numero_pieza ==  11||this.numero_pieza ==  51||this.numero_pieza ==  81){
                this.referencia_grafico_protesis_fija = this.lienzo.path('M '+(this.escala*11)+' '+(this.escala*10)+' L '+(this.escala*60)+' '+(this.escala*10)+' L '+(this.escala*11)+' '+(this.escala*10)+' L '+(this.escala*11)+' '+(this.escala*13)+' L '+(this.escala*5)+' '+(this.escala*13)+' L '+(this.escala*5)+' '+(this.escala*7)+' L '+(this.escala*11)+' '+(this.escala*7)+' L '+(this.escala*11)+' '+(this.escala*10)+'');
            }
            else{
                this.referencia_grafico_protesis_fija = this.lienzo.path('M '+(this.escala*11)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*11)+' '+(this.escala*10)+' L '+(this.escala*11)+' '+(this.escala*13)+' L '+(this.escala*5)+' '+(this.escala*13)+' L '+(this.escala*5)+' '+(this.escala*7)+' L '+(this.escala*11)+' '+(this.escala*7)+' L '+(this.escala*11)+' '+(this.escala*10)+'');
            }

        }
        else if(this.posicion_en_protesis_fija == 'final'){//final
            if(this.numero_pieza == 21 ||this.numero_pieza ==  31||this.numero_pieza ==  61||this.numero_pieza ==  71){
                this.referencia_grafico_protesis_fija = this.lienzo.path('M '+(this.escala*-1)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*7)+' L '+(this.escala*55)+' '+(this.escala*7)+' L '+(this.escala*55)+' '+(this.escala*13)+' L '+(this.escala*50)+' '+(this.escala*13)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*-1)+' '+(this.escala*10)+'');
            }
            else{
                this.referencia_grafico_protesis_fija = this.lienzo.path('M '+(this.escala*8)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*7)+' L '+(this.escala*55)+' '+(this.escala*7)+' L '+(this.escala*55)+' '+(this.escala*13)+' L '+(this.escala*50)+' '+(this.escala*13)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*8)+' '+(this.escala*10)+'');
            }
        }
        else{//inicio final
            this.referencia_grafico_protesis_fija = this.lienzo.path('M '+(this.escala*8)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*7)+' L '+(this.escala*55)+' '+(this.escala*7)+' L '+(this.escala*55)+' '+(this.escala*13)+' L '+(this.escala*50)+' '+(this.escala*13)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*8)+' '+(this.escala*10)+' L '+(this.escala*8)+' '+(this.escala*13)+' L '+(this.escala*3)+' '+(this.escala*13)+' L '+(this.escala*3)+' '+(this.escala*7)+' L '+(this.escala*8)+' '+(this.escala*7)+' L '+(this.escala*8)+' '+(this.escala*10)+'');
        }

        this.referencia_grafico_protesis_fija.stroke({width: 4})
        this.referencia_grafico_protesis_fija.stroke({color: '#0765E9'})
        this.referencia_grafico_protesis_fija.stroke({linecap: this.forma_fin_linea , linejoin: this.forma_fin_linea })
        this.referencia_grafico_protesis_fija.translate(this.posicion_x+this.grosor_linea+(this.escala*10) - 20,this.posicion_y+this.grosor_linea+(this.escala*10))
        this.referencia_grafico_protesis_fija.fill({color: '#0765E9'});
        this.referencia_grafico_svg.add(this.referencia_grafico_protesis_fija)
        //evento borrar está programado en la prótesis

    }
    protected DesmarcarProtesisFijaAplicada():void{
        try{
            this.referencia_grafico_protesis_fija.remove();
        }catch(error){}
        this.referencia_grafico_protesis_fija = null

    }

    //PROTESIS REMOVIBLE
    protected MarcarProtesisRemovibleNecesaria():void{
        try{
            this.referencia_grafico_protesis_removible.remove();

        }catch(error){}
        this.referencia_grafico_protesis_removible = null
        if(this.posicion_en_protesis_removible == 'centro'){
            if(this.numero_pieza == 41 ||this.numero_pieza ==  11||this.numero_pieza ==  51||this.numero_pieza ==  81){
                this.referencia_grafico_protesis_removible = this.lienzo.path('M '+(8)+' '+(this.escala*10)+' L '+(this.escala*65)+' '+(this.escala*10)+'');
            }
            else if(this.numero_pieza == 21 ||this.numero_pieza ==  31||this.numero_pieza ==  61||this.numero_pieza ==  71){
                this.referencia_grafico_protesis_removible = this.lienzo.path('M '+(-13)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+'');
            }
            else{
                this.referencia_grafico_protesis_removible = this.lienzo.path('M '+(8)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+'');
            }
        }
        else if(this.posicion_en_protesis_removible == 'inicio'){
            if(this.numero_pieza == 41 ||this.numero_pieza ==  11||this.numero_pieza ==  51||this.numero_pieza ==  81){
                this.referencia_grafico_protesis_removible = this.lienzo.path('M '+(this.escala*9)+' '+(this.escala*22)+' L '+(this.escala*9)+' '+(this.escala*22)+' C '+(this.escala*7)+' '+(this.escala*13)+' '+(this.escala*6)+' '+(this.escala*8)+' '+(this.escala*9)+' '+(this.escala*-2)+' M '+(this.escala*7)+' '+(this.escala*10)+' L '+(this.escala*60)+' '+(this.escala*10)+'');
            }
            else{
                this.referencia_grafico_protesis_removible = this.lienzo.path('M '+(this.escala*9)+' '+(this.escala*22)+' L '+(this.escala*9)+' '+(this.escala*22)+' C '+(this.escala*7)+' '+(this.escala*13)+' '+(this.escala*6)+' '+(this.escala*8)+' '+(this.escala*9)+' '+(this.escala*-2)+' M '+(this.escala*7)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+'');
            }
        }
        else if(this.posicion_en_protesis_removible == 'final'){//final
            if(this.numero_pieza == 21 ||this.numero_pieza ==  31||this.numero_pieza ==  61||this.numero_pieza ==  71){
                this.referencia_grafico_protesis_removible = this.lienzo.path('M '+(this.escala*50)+' '+(this.escala*22)+' L '+(this.escala*50)+' '+(this.escala*22)+' C '+(this.escala*53)+' '+(this.escala*13)+' '+(this.escala*53)+' '+(this.escala*8)+' '+(this.escala*50)+' '+(this.escala*-2)+' M '+(this.escala*-1)+' '+(this.escala*10)+' L '+(this.escala*52)+' '+(this.escala*10)+' L '+(this.escala*52)+' '+(this.escala*10)+' L '+(this.escala*-1)+' '+(this.escala*10)+'');
            }
            else{
                this.referencia_grafico_protesis_removible = this.lienzo.path('M '+(this.escala*50)+' '+(this.escala*22)+' L '+(this.escala*50)+' '+(this.escala*22)+' C '+(this.escala*53)+' '+(this.escala*13)+' '+(this.escala*53)+' '+(this.escala*8)+' '+(this.escala*50)+' '+(this.escala*-2)+' M '+(this.escala*8)+' '+(this.escala*10)+' L '+(this.escala*52)+' '+(this.escala*10)+' L '+(this.escala*52)+' '+(this.escala*10)+' L '+(this.escala*8)+' '+(this.escala*10)+'');
            }
        }
        else{//inicio final
            this.referencia_grafico_protesis_removible = this.lienzo.path('M '+(this.escala*9)+' '+(this.escala*22)+' L '+(this.escala*9)+' '+(this.escala*22)+' C '+(this.escala*6)+' '+(this.escala*13)+' '+(this.escala*6)+' '+(this.escala*8)+' '+(this.escala*9)+' '+(this.escala*-2)+' M '+(this.escala*7)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+' M '+(this.escala*48)+' '+(this.escala*22)+' L '+(this.escala*48)+' '+(this.escala*22)+' C '+(this.escala*51)+' '+(this.escala*13)+' '+(this.escala*51)+' '+(this.escala*8)+' '+(this.escala*48)+' '+(this.escala*-2)+'');
        }


        this.referencia_grafico_protesis_removible.stroke({width: 4})
        this.referencia_grafico_protesis_removible.stroke({color: '#E90F07'})
        this.referencia_grafico_protesis_removible.stroke({linecap: this.forma_fin_linea , linejoin: this.forma_fin_linea })
        this.referencia_grafico_protesis_removible.translate(this.posicion_x+this.grosor_linea+(this.escala*10) -20 ,this.posicion_y+this.grosor_linea+(this.escala*10))
        this.referencia_grafico_protesis_removible.fill({opacity: 0.0});
        this.referencia_grafico_svg.add(this.referencia_grafico_protesis_removible)
        //evento borrar está programado en la prótesis

    }
    protected DesmarcarProtesisRemovibleNecesaria():void{
        try{
            this.referencia_grafico_protesis_removible.remove();
        }catch(error){}
        this.referencia_grafico_protesis_removible = null
    }
    protected MarcarProtesisRemovibleAplicada():void{
        try{
            this.referencia_grafico_protesis_removible.remove();

        }catch(error){}
        this.referencia_grafico_protesis_removible = null
        if(this.posicion_en_protesis_removible == 'centro'){
            if(this.numero_pieza == 41 ||this.numero_pieza ==  11||this.numero_pieza ==  51||this.numero_pieza ==  81){
                this.referencia_grafico_protesis_removible = this.lienzo.path('M '+(8)+' '+(this.escala*10)+' L '+(this.escala*65)+' '+(this.escala*10)+'');
            }
            else if(this.numero_pieza == 21 ||this.numero_pieza ==  31||this.numero_pieza ==  61||this.numero_pieza ==  71){
                this.referencia_grafico_protesis_removible = this.lienzo.path('M '+(-13)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+'');
            }
            else{
                this.referencia_grafico_protesis_removible = this.lienzo.path('M '+(8)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+'');
            }
        }
        else if(this.posicion_en_protesis_removible == 'inicio'){
            if(this.numero_pieza == 41 ||this.numero_pieza ==  11||this.numero_pieza ==  51||this.numero_pieza ==  81){
                this.referencia_grafico_protesis_removible = this.lienzo.path('M '+(this.escala*9)+' '+(this.escala*22)+' L '+(this.escala*9)+' '+(this.escala*22)+' C '+(this.escala*7)+' '+(this.escala*13)+' '+(this.escala*6)+' '+(this.escala*8)+' '+(this.escala*9)+' '+(this.escala*-2)+' M '+(this.escala*7)+' '+(this.escala*10)+' L '+(this.escala*60)+' '+(this.escala*10)+'');
            }
            else{
                this.referencia_grafico_protesis_removible = this.lienzo.path('M '+(this.escala*9)+' '+(this.escala*22)+' L '+(this.escala*9)+' '+(this.escala*22)+' C '+(this.escala*7)+' '+(this.escala*13)+' '+(this.escala*6)+' '+(this.escala*8)+' '+(this.escala*9)+' '+(this.escala*-2)+' M '+(this.escala*7)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+'');
            }
        }
        else if(this.posicion_en_protesis_removible == 'final'){//final
            if(this.numero_pieza == 21 ||this.numero_pieza ==  31||this.numero_pieza ==  61||this.numero_pieza ==  71){
                this.referencia_grafico_protesis_removible = this.lienzo.path('M '+(this.escala*50)+' '+(this.escala*22)+' L '+(this.escala*50)+' '+(this.escala*22)+' C '+(this.escala*53)+' '+(this.escala*13)+' '+(this.escala*53)+' '+(this.escala*8)+' '+(this.escala*50)+' '+(this.escala*-2)+' M '+(this.escala*-1)+' '+(this.escala*10)+' L '+(this.escala*52)+' '+(this.escala*10)+' L '+(this.escala*52)+' '+(this.escala*10)+' L '+(this.escala*-1)+' '+(this.escala*10)+'');
            }
            else{
                this.referencia_grafico_protesis_removible = this.lienzo.path('M '+(this.escala*50)+' '+(this.escala*22)+' L '+(this.escala*50)+' '+(this.escala*22)+' C '+(this.escala*53)+' '+(this.escala*13)+' '+(this.escala*53)+' '+(this.escala*8)+' '+(this.escala*50)+' '+(this.escala*-2)+' M '+(this.escala*8)+' '+(this.escala*10)+' L '+(this.escala*52)+' '+(this.escala*10)+' L '+(this.escala*52)+' '+(this.escala*10)+' L '+(this.escala*8)+' '+(this.escala*10)+'');
            }
        }
        else{//inicio final
            this.referencia_grafico_protesis_removible = this.lienzo.path('M '+(this.escala*9)+' '+(this.escala*22)+' L '+(this.escala*9)+' '+(this.escala*22)+' C '+(this.escala*6)+' '+(this.escala*13)+' '+(this.escala*6)+' '+(this.escala*8)+' '+(this.escala*9)+' '+(this.escala*-2)+' M '+(this.escala*7)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+' M '+(this.escala*48)+' '+(this.escala*22)+' L '+(this.escala*48)+' '+(this.escala*22)+' C '+(this.escala*51)+' '+(this.escala*13)+' '+(this.escala*51)+' '+(this.escala*8)+' '+(this.escala*48)+' '+(this.escala*-2)+'');
        }

        this.referencia_grafico_protesis_removible.stroke({width: 4})
        this.referencia_grafico_protesis_removible.stroke({color: '#0765E9'})
        this.referencia_grafico_protesis_removible.stroke({linecap: this.forma_fin_linea , linejoin: this.forma_fin_linea })
        this.referencia_grafico_protesis_removible.translate(this.posicion_x+this.grosor_linea+(this.escala*10) - 20,this.posicion_y+this.grosor_linea+(this.escala*10))
        this.referencia_grafico_protesis_removible.fill({opacity: 0.0});
        this.referencia_grafico_svg.add(this.referencia_grafico_protesis_removible)
        //evento borrar está programado en la prótesis

    }
    protected DesmarcarProtesisRemovibleAplicada():void{
        try{
            this.referencia_grafico_protesis_removible.remove();
        }catch(error){}
        this.referencia_grafico_protesis_removible = null

    }

    //PROTESIS CORONA
    protected MarcarProtesisCoronaNecesaria():void{
        try{
            this.referencia_grafico_protesis_corona.remove();

        }catch(error){}
        this.referencia_grafico_protesis_corona = null
        if(this.posicion_en_protesis_corona == 'centro'){
            if(this.numero_pieza == 41 ||this.numero_pieza ==  11||this.numero_pieza ==  51||this.numero_pieza ==  81){
                this.referencia_grafico_protesis_corona = this.lienzo.path('M '+(8)+' '+(this.escala*10)+' L '+(this.escala*65)+' '+(this.escala*10)+'');
            }
            else if(this.numero_pieza == 21 ||this.numero_pieza ==  31||this.numero_pieza ==  61||this.numero_pieza ==  71){
                this.referencia_grafico_protesis_corona = this.lienzo.path('M '+(-13)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+'');
            }
            else{
                this.referencia_grafico_protesis_corona = this.lienzo.path('M '+(8)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+'');
            }
        }
        else if(this.posicion_en_protesis_corona == 'inicio'){
            if(this.numero_pieza == 41 ||this.numero_pieza ==  11||this.numero_pieza ==  51||this.numero_pieza ==  81){
                this.referencia_grafico_protesis_corona = this.lienzo.path('M '+(this.escala*11)+' '+(this.escala*10)+' L '+(this.escala*60)+' '+(this.escala*10)+' L '+(this.escala*11)+' '+(this.escala*10)+' L '+(this.escala*11)+' '+(this.escala*13)+' L '+(this.escala*5)+' '+(this.escala*13)+' L '+(this.escala*5)+' '+(this.escala*7)+' L '+(this.escala*11)+' '+(this.escala*7)+' L '+(this.escala*11)+' '+(this.escala*10)+'');
            }
            else{
                this.referencia_grafico_protesis_corona = this.lienzo.path('M '+(this.escala*11)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*11)+' '+(this.escala*10)+' L '+(this.escala*11)+' '+(this.escala*13)+' L '+(this.escala*5)+' '+(this.escala*13)+' L '+(this.escala*5)+' '+(this.escala*7)+' L '+(this.escala*11)+' '+(this.escala*7)+' L '+(this.escala*11)+' '+(this.escala*10)+'');
            }

        }
        else if(this.posicion_en_protesis_corona == 'final'){//final
            if(this.numero_pieza == 21 ||this.numero_pieza ==  31||this.numero_pieza ==  61||this.numero_pieza ==  71){
                this.referencia_grafico_protesis_corona = this.lienzo.path('M '+(this.escala*-1)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*7)+' L '+(this.escala*55)+' '+(this.escala*7)+' L '+(this.escala*55)+' '+(this.escala*13)+' L '+(this.escala*50)+' '+(this.escala*13)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*-1)+' '+(this.escala*10)+'');
            }
            else{
                this.referencia_grafico_protesis_corona = this.lienzo.path('M '+(this.escala*8)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*7)+' L '+(this.escala*55)+' '+(this.escala*7)+' L '+(this.escala*55)+' '+(this.escala*13)+' L '+(this.escala*50)+' '+(this.escala*13)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*8)+' '+(this.escala*10)+'');
            }
        }
        else{//inicio final
            this.referencia_grafico_protesis_corona = this.lienzo.path('M '+(this.escala*0)+' '+(this.escala*0)+' L '+(this.escala*36)+' 0 L '+(this.escala*36)+' '+(this.escala*36)+' L '+(this.escala*0)+' '+(this.escala*36)+' L '+(this.escala*0)+' '+(this.escala*0)+' M '+(this.escala*4)+' '+(this.escala*4)+' L '+(this.escala*4)+' '+(this.escala*32)+' L '+(this.escala*32)+' '+(this.escala*32)+' L '+(this.escala*32)+' '+(this.escala*4)+' L '+(this.escala*4)+' '+(this.escala*4)+' M '+(this.escala*8)+' '+(this.escala*8)+' L '+(this.escala*8)+' '+(this.escala*28)+' L '+(this.escala*28)+' '+(this.escala*28)+' L '+(this.escala*28)+' '+(this.escala*8)+' L '+(this.escala*8)+' '+(this.escala*8)+' M '+(this.escala*12)+' '+(this.escala*12)+' L '+(this.escala*24)+' '+(this.escala*12)+' L '+(this.escala*24)+' '+(this.escala*24)+' L '+(this.escala*12)+' '+(this.escala*24)+' L '+(this.escala*12)+' '+(this.escala*12)+' M '+(this.escala*16)+' '+(this.escala*16)+' L '+(this.escala*20)+' '+(this.escala*16)+' L '+(this.escala*20)+' '+(this.escala*20)+' L '+(this.escala*16)+' '+(this.escala*20)+' L '+(this.escala*16)+' '+(this.escala*16)+'');
        }


        this.referencia_grafico_protesis_corona.stroke({width: 1})
        this.referencia_grafico_protesis_corona.stroke({color: '#E90F07'})
        this.referencia_grafico_protesis_corona.stroke({linecap: this.forma_fin_linea , linejoin: this.forma_fin_linea })
        this.referencia_grafico_protesis_corona.translate(this.posicion_x+this.grosor_linea+(this.escala*10) - 9 +1,this.posicion_y+this.grosor_linea+(this.escala*10)-9 +1)
        this.referencia_grafico_protesis_corona.fill({color: '#E90F07'});
        this.referencia_grafico_svg.add(this.referencia_grafico_protesis_corona)
        //evento borrar está programado en la prótesis

    }
    protected DesmarcarProtesisCoronaNecesaria():void{
        try{
            this.referencia_grafico_protesis_corona.remove();
        }catch(error){}
        this.referencia_grafico_protesis_corona = null
    }
    protected MarcarProtesisCoronaAplicada():void{
        try{
            this.referencia_grafico_protesis_corona.remove();

        }catch(error){}
        this.referencia_grafico_protesis_corona = null

        if(this.posicion_en_protesis_corona == 'centro'){
            if(this.numero_pieza == 41 ||this.numero_pieza ==  11||this.numero_pieza ==  51||this.numero_pieza ==  81){
                this.referencia_grafico_protesis_corona = this.lienzo.path('M '+(8)+' '+(this.escala*10)+' L '+(this.escala*65)+' '+(this.escala*10)+'');
            }
            else if(this.numero_pieza == 21 ||this.numero_pieza ==  31||this.numero_pieza ==  61||this.numero_pieza ==  71){
                this.referencia_grafico_protesis_corona = this.lienzo.path('M '+(-13)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+'');
            }
            else{
                this.referencia_grafico_protesis_corona = this.lienzo.path('M '+(8)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+'');
            }
        }
        else if(this.posicion_en_protesis_corona == 'inicio'){
            if(this.numero_pieza == 41 ||this.numero_pieza ==  11||this.numero_pieza ==  51||this.numero_pieza ==  81){
                this.referencia_grafico_protesis_corona = this.lienzo.path('M '+(this.escala*11)+' '+(this.escala*10)+' L '+(this.escala*60)+' '+(this.escala*10)+' L '+(this.escala*11)+' '+(this.escala*10)+' L '+(this.escala*11)+' '+(this.escala*13)+' L '+(this.escala*5)+' '+(this.escala*13)+' L '+(this.escala*5)+' '+(this.escala*7)+' L '+(this.escala*11)+' '+(this.escala*7)+' L '+(this.escala*11)+' '+(this.escala*10)+'');
            }
            else{
                this.referencia_grafico_protesis_corona = this.lienzo.path('M '+(this.escala*11)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*11)+' '+(this.escala*10)+' L '+(this.escala*11)+' '+(this.escala*13)+' L '+(this.escala*5)+' '+(this.escala*13)+' L '+(this.escala*5)+' '+(this.escala*7)+' L '+(this.escala*11)+' '+(this.escala*7)+' L '+(this.escala*11)+' '+(this.escala*10)+'');
            }

        }
        else if(this.posicion_en_protesis_corona == 'final'){//final
            if(this.numero_pieza == 21 ||this.numero_pieza ==  31||this.numero_pieza ==  61||this.numero_pieza ==  71){
                this.referencia_grafico_protesis_corona = this.lienzo.path('M '+(this.escala*-1)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*7)+' L '+(this.escala*55)+' '+(this.escala*7)+' L '+(this.escala*55)+' '+(this.escala*13)+' L '+(this.escala*50)+' '+(this.escala*13)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*-1)+' '+(this.escala*10)+'');
            }
            else{
                this.referencia_grafico_protesis_corona = this.lienzo.path('M '+(this.escala*8)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*50)+' '+(this.escala*7)+' L '+(this.escala*55)+' '+(this.escala*7)+' L '+(this.escala*55)+' '+(this.escala*13)+' L '+(this.escala*50)+' '+(this.escala*13)+' L '+(this.escala*50)+' '+(this.escala*10)+' L '+(this.escala*8)+' '+(this.escala*10)+'');
            }
        }
        else{//inicio final
            this.referencia_grafico_protesis_corona = this.lienzo.path('M '+(this.escala*0)+' '+(this.escala*0)+' L '+(this.escala*36)+' 0 L '+(this.escala*36)+' '+(this.escala*36)+' L '+(this.escala*0)+' '+(this.escala*36)+' L '+(this.escala*0)+' '+(this.escala*0)+' M '+(this.escala*4)+' '+(this.escala*4)+' L '+(this.escala*4)+' '+(this.escala*32)+' L '+(this.escala*32)+' '+(this.escala*32)+' L '+(this.escala*32)+' '+(this.escala*4)+' L '+(this.escala*4)+' '+(this.escala*4)+' M '+(this.escala*8)+' '+(this.escala*8)+' L '+(this.escala*8)+' '+(this.escala*28)+' L '+(this.escala*28)+' '+(this.escala*28)+' L '+(this.escala*28)+' '+(this.escala*8)+' L '+(this.escala*8)+' '+(this.escala*8)+' M '+(this.escala*12)+' '+(this.escala*12)+' L '+(this.escala*24)+' '+(this.escala*12)+' L '+(this.escala*24)+' '+(this.escala*24)+' L '+(this.escala*12)+' '+(this.escala*24)+' L '+(this.escala*12)+' '+(this.escala*12)+' M '+(this.escala*16)+' '+(this.escala*16)+' L '+(this.escala*20)+' '+(this.escala*16)+' L '+(this.escala*20)+' '+(this.escala*20)+' L '+(this.escala*16)+' '+(this.escala*20)+' L '+(this.escala*16)+' '+(this.escala*16)+'');
        }

        this.referencia_grafico_protesis_corona.stroke({width: 1})
        this.referencia_grafico_protesis_corona.stroke({color: '#0765E9'})
        this.referencia_grafico_protesis_corona.stroke({linecap: this.forma_fin_linea , linejoin: this.forma_fin_linea })
        this.referencia_grafico_protesis_corona.translate(this.posicion_x+this.grosor_linea+(this.escala*10) - 9 +1 ,this.posicion_y+this.grosor_linea+(this.escala*10)-9 +1)
        this.referencia_grafico_protesis_corona.fill({color: '#0765E9'});
        this.referencia_grafico_svg.add(this.referencia_grafico_protesis_corona)
        //evento borrar está programado en la prótesis

    }
    protected DesmarcarProtesisCoronaAplicada():void{
        try{
            this.referencia_grafico_protesis_corona.remove();
        }catch(error){}
        this.referencia_grafico_protesis_corona = null

    }





    public QuitarTodosDetalles():void{
        this.sellante_necesario = false;
        this.sellante_aplicado = false;
        this.extraccion_aplicada_caries = false;
        this.extraccion_necesaria = false;
        this.extraccion_aplicada_otro = false;
        this.endodoncia_aplicada = false;
        this.endodoncia_necesaria = false;
        this.cara_abajo.caries = false;
        this.cara_abajo.obturado = false;
        this.cara_arriba.caries = false;
        this.cara_arriba.obturado = false;
        this.cara_izquierda.caries = false;
        this.cara_izquierda.obturado = false;
        this.cara_derecha.caries = false;
        this.cara_derecha.obturado = false;
        this.cara_centro.caries = false;
        this.cara_centro.obturado = false;
    }
}
