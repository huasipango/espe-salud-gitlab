import { GraficoSVG } from './grafico-svg';
import { OdontogramaComponent } from '../components/odontograma/odontograma.component';
import { AdaptadorGraficoModelos } from '../components/odontograma/adaptador-grafico-modelos';

export abstract class Cara extends GraficoSVG{

    public ubicacion: string; // ubicacion de la cara (centro, izquierda, derecha, arriba, abajo)

    protected _caries: boolean;
    public get caries(): boolean {return this._caries; }
    public set caries(value: boolean) {
      this._caries = value;
      if (this.caries){
        this._obturado = false; this.MarcarCaries();
      } else {
        if (!this.obturado) {
          this.DesmarcarCaries();
        }
      }
    }

    protected _obturado: boolean;
    public get obturado(): boolean {return this._obturado; }
    public set obturado(value: boolean) {
      this._obturado = value;
      if (this.obturado) {
        this._caries = false;
        this.MarcarObturado();
      } else {
        if (!this.caries) {
          this.DesmarcarObturado();
        }
      }
    }


    constructor(lienzo, ubicacion, odontograma_component: OdontogramaComponent){
        super(lienzo);
        this.componente_odontograma = odontograma_component;
        this.ubicacion = ubicacion;
        this.GenerarGrafico();
        this.GenerarEventos();
    }

    protected GenerarEventos(): void{
      // FECTO AL PASAR EL MOUSE SOBRE UNA CARA
      this.referencia_grafico_svg.on('mouseenter', (evento) => {
          if (!(evento instanceof MouseEvent) ||
            OdontogramaComponent.detalle_a_aplicar === 'caries' ||
            OdontogramaComponent.detalle_a_aplicar === 'obturacion' ||
            OdontogramaComponent.detalle_a_aplicar === 'borrar' ||
            OdontogramaComponent.detalle_a_aplicar === ''){
              // Generando color de relleno al pasar el mouse
              let color = this.color_relleno === '#FFFFFF' ? '#34992D' : this.color_relleno;
              const grado_aclaracion = 75;
              let usePound = false;
              if ( color[0] === '#' ) {color = color.slice(1); usePound = true; }
              const num = parseInt(color, 16);
              let r = (num >> 16) + grado_aclaracion;
              if ( r > 255 ) { r = 255; }
              else if  (r < 0) { r = 0; }
              let b = ((num >> 8) & 0x00FF) + grado_aclaracion;
              if ( b > 255 ) { b = 255; }
              else if  (b < 0) { b = 0; }
              let g = (num & 0x0000FF) + grado_aclaracion;
              if ( g > 255 ) { g = 255; }
              else if  ( g < 0 ) { g = 0; }
              color = (usePound ? '#' : '') + (g | (b << 8) | (r << 16)).toString(16);

              this.referencia_grafico_svg.fill(color);
          }
      });

      this.referencia_grafico_svg.on('mouseleave', (evento) => {
          if (!(evento instanceof MouseEvent) ||
            OdontogramaComponent.detalle_a_aplicar === 'caries' ||
            OdontogramaComponent.detalle_a_aplicar === 'obturacion' ||
            OdontogramaComponent.detalle_a_aplicar === 'borrar' ||
            OdontogramaComponent.detalle_a_aplicar === ''){
              this.referencia_grafico_svg.fill(this.color_relleno);
          }
      });

      this.referencia_grafico_svg.on('click', () => {
          if (OdontogramaComponent.detalle_a_aplicar === 'caries' && !this.caries){
              this.caries = true;
          }
          else if (OdontogramaComponent.detalle_a_aplicar === 'obturacion' && !this.obturado){
              this.obturado = true;
          }
          else if (OdontogramaComponent.detalle_a_aplicar === 'borrar' && this.caries){
              this.caries = false;
          }
          else if (OdontogramaComponent.detalle_a_aplicar === 'borrar' && this.obturado){
              this.obturado = false;
          }
          this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma));
      });

    }

    protected ActualizarGrosorLinea(){
        this.referencia_grafico_svg.stroke({width: this.grosor_linea});
        this.ActualizarPosicion();
    }
    protected ActualizarColorLinea(){
        this.referencia_grafico_svg.stroke({color: this.color_linea});
    }
    protected ActualizarColorRelleno(){
        this.referencia_grafico_svg.fill(this.color_relleno);
    }
    protected ActualizarFormaFinLinea(){
        this.referencia_grafico_svg.stroke({linecap: this.forma_fin_linea , linejoin: this.forma_fin_linea });
    }

    // DETALLES ODONTOLÓGICOS
    protected MarcarCaries(): void{
        this.color_relleno = '#FF3838';
    }
    protected DesmarcarCaries(): void{
        this.color_relleno = '#FFFFFF';
    }
    protected MarcarObturado(): void{
        this.color_relleno = '#3890FF';
    }
    protected DesmarcarObturado(): void{
        this.color_relleno = '#FFFFFF';
    }


}
