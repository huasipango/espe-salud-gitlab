import { Cara } from './cara';
import { OdontogramaComponent } from '../components/odontograma/odontograma.component';

export class CaraDecidua extends Cara{

    constructor(lienzo, ubicacion,odontograma_component:OdontogramaComponent){
        super(lienzo,ubicacion,odontograma_component)
    }

    //SOBRESCRITURA DE MÉTODOS GRÁFICOS DE GraficoSVG
    protected GenerarGrafico(){
        switch(this.ubicacion){
            case "izquierda":
                // M 5.455 5.455 C -1.818 12.727 -1.818 27.273 5.455 34.545 L 12.727 27.273 C 9.091 23.636 9.091 16.364 12.727 12.727 L 5.455 5.455 C 12.727 -1.818 27.273 -1.818 34.545 5.455 C 41.818 12.727 41.818 27.273 34.545 34.545 C 27.273 41.818 12.727 41.818 5.455 34.545
                this.referencia_grafico_svg = this.lienzo.path('M '+(this.escala*5.455)+' '+(this.escala*5.455)+' C '+(this.escala*-1.818)+' '+(this.escala*12.727)+' '+(this.escala*-1.818)+' '+(this.escala*27.273)+' '+(this.escala*5.455)+' '+(this.escala*34.545)+' L '+(this.escala*12.727)+' '+(this.escala*27.273)+' C '+(this.escala*9.091)+' '+(this.escala*23.636)+' '+(this.escala*9.091)+' '+(this.escala*16.364)+' '+(this.escala*12.727)+' '+(this.escala*12.727)+' L '+(this.escala*5.455)+' '+(this.escala*5.455));
                this.referencia_grafico_svg.translate(this.posicion_x + this.grosor_linea, this.posicion_y + this.grosor_linea)
                break;
            case "arriba":
                this.referencia_grafico_svg = this.lienzo.path('M '+(this.escala*5.455)+' '+(this.escala*5.455)+' C '+(this.escala*-1.818)+' '+(this.escala*12.727)+' '+(this.escala*-1.818)+' '+(this.escala*27.273)+' '+(this.escala*5.455)+' '+(this.escala*34.545)+' L '+(this.escala*12.727)+' '+(this.escala*27.273)+' C '+(this.escala*9.091)+' '+(this.escala*23.636)+' '+(this.escala*9.091)+' '+(this.escala*16.364)+' '+(this.escala*12.727)+' '+(this.escala*12.727)+' L '+(this.escala*5.455)+' '+(this.escala*5.455));
                this.referencia_grafico_svg.rotate(90)
                this.referencia_grafico_svg.translate(this.posicion_x + 40.00333*this.escala + this.grosor_linea,this.posicion_y + 0 + this.grosor_linea)
                break;
            case "derecha":
                this.referencia_grafico_svg = this.lienzo.path('M '+(this.escala*5.455)+' '+(this.escala*5.455)+' C '+(this.escala*-1.818)+' '+(this.escala*12.727)+' '+(this.escala*-1.818)+' '+(this.escala*27.273)+' '+(this.escala*5.455)+' '+(this.escala*34.545)+' L '+(this.escala*12.727)+' '+(this.escala*27.273)+' C '+(this.escala*9.091)+' '+(this.escala*23.636)+' '+(this.escala*9.091)+' '+(this.escala*16.364)+' '+(this.escala*12.727)+' '+(this.escala*12.727)+' L '+(this.escala*5.455)+' '+(this.escala*5.455));
                this.referencia_grafico_svg.rotate(180)
                this.referencia_grafico_svg.translate(this.posicion_x + 40.00333*this.escala + this.grosor_linea,this.posicion_y + 40.00333*this.escala + this.grosor_linea)
                break;
            case "abajo":
                this.referencia_grafico_svg = this.lienzo.path('M '+(this.escala*5.455)+' '+(this.escala*5.455)+' C '+(this.escala*-1.818)+' '+(this.escala*12.727)+' '+(this.escala*-1.818)+' '+(this.escala*27.273)+' '+(this.escala*5.455)+' '+(this.escala*34.545)+' L '+(this.escala*12.727)+' '+(this.escala*27.273)+' C '+(this.escala*9.091)+' '+(this.escala*23.636)+' '+(this.escala*9.091)+' '+(this.escala*16.364)+' '+(this.escala*12.727)+' '+(this.escala*12.727)+' L '+(this.escala*5.455)+' '+(this.escala*5.455));
                this.referencia_grafico_svg.rotate(270)
                this.referencia_grafico_svg.translate(this.posicion_x + 0 + this.grosor_linea,this.posicion_y + 40.00333*this.escala + this.grosor_linea)
                break;
            case "centro":
                this.referencia_grafico_svg = this.lienzo.path('M '+(this.escala*12.727)+' '+(this.escala*27.273)+' C '+(this.escala*9.091)+' '+(this.escala*23.636)+' '+(this.escala*9.091)+' '+(this.escala*16.364)+' '+(this.escala*12.727)+' '+(this.escala*12.727)+' C '+(this.escala*16.364)+' '+(this.escala*9.091)+' '+(this.escala*23.636)+' '+(this.escala*9.091)+' '+(this.escala*27.273)+' '+(this.escala*12.727)+' C '+(this.escala*30.909)+' '+(this.escala*16.364)+' '+(this.escala*30.909)+' '+(this.escala*23.636)+' '+(this.escala*27.273)+' '+(this.escala*27.273)+' C '+(this.escala*23.636)+' '+(this.escala*30.909)+' '+(this.escala*16.364)+' '+(this.escala*30.909)+' '+(this.escala*12.727)+' '+(this.escala*27.273));
                this.referencia_grafico_svg.translate(this.posicion_x + 10.0008325*(this.escala-1) + this.grosor_linea  ,this.posicion_y + 10.0008325*(this.escala-1) + this.grosor_linea);
                break;
        }
        
        this.referencia_grafico_svg.fill(this.color_relleno);
        this.referencia_grafico_svg.stroke({ color: this.color_linea, width: this.grosor_linea, linecap: this.forma_fin_linea , linejoin: this.forma_fin_linea })
        //PARA FACILITAR LA CREACIÓN DE LNEAS EN SVG https://yqnn.github.io/svg-path-editor/
    }
    protected ActualizarEscala(){
        switch(this.ubicacion){
            case "izquierda":
                this.referencia_grafico_svg.plot('M '+(this.escala*5.455)+' '+(this.escala*5.455)+' C '+(this.escala*-1.818)+' '+(this.escala*12.727)+' '+(this.escala*-1.818)+' '+(this.escala*27.273)+' '+(this.escala*5.455)+' '+(this.escala*34.545)+' L '+(this.escala*12.727)+' '+(this.escala*27.273)+' C '+(this.escala*9.091)+' '+(this.escala*23.636)+' '+(this.escala*9.091)+' '+(this.escala*16.364)+' '+(this.escala*12.727)+' '+(this.escala*12.727)+' L '+(this.escala*5.455)+' '+(this.escala*5.455));
                this.referencia_grafico_svg.translate(this.posicion_x + this.grosor_linea, this.posicion_y + this.grosor_linea)
                break;
            case "arriba":
                this.referencia_grafico_svg.plot('M '+(this.escala*5.455)+' '+(this.escala*5.455)+' C '+(this.escala*-1.818)+' '+(this.escala*12.727)+' '+(this.escala*-1.818)+' '+(this.escala*27.273)+' '+(this.escala*5.455)+' '+(this.escala*34.545)+' L '+(this.escala*12.727)+' '+(this.escala*27.273)+' C '+(this.escala*9.091)+' '+(this.escala*23.636)+' '+(this.escala*9.091)+' '+(this.escala*16.364)+' '+(this.escala*12.727)+' '+(this.escala*12.727)+' L '+(this.escala*5.455)+' '+(this.escala*5.455));
                this.referencia_grafico_svg.rotate(90)
                this.referencia_grafico_svg.translate(this.posicion_x + 40.00333*this.escala + this.grosor_linea,this.posicion_y + 0 + this.grosor_linea)
                break;
            case "derecha":
                this.referencia_grafico_svg.plot('M '+(this.escala*5.455)+' '+(this.escala*5.455)+' C '+(this.escala*-1.818)+' '+(this.escala*12.727)+' '+(this.escala*-1.818)+' '+(this.escala*27.273)+' '+(this.escala*5.455)+' '+(this.escala*34.545)+' L '+(this.escala*12.727)+' '+(this.escala*27.273)+' C '+(this.escala*9.091)+' '+(this.escala*23.636)+' '+(this.escala*9.091)+' '+(this.escala*16.364)+' '+(this.escala*12.727)+' '+(this.escala*12.727)+' L '+(this.escala*5.455)+' '+(this.escala*5.455));
                this.referencia_grafico_svg.rotate(180)
                this.referencia_grafico_svg.translate(this.posicion_x + 40.00333*this.escala + this.grosor_linea,this.posicion_y + 40.00333*this.escala + this.grosor_linea)
                break;
            case "abajo":
                this.referencia_grafico_svg.plot('M '+(this.escala*5.455)+' '+(this.escala*5.455)+' C '+(this.escala*-1.818)+' '+(this.escala*12.727)+' '+(this.escala*-1.818)+' '+(this.escala*27.273)+' '+(this.escala*5.455)+' '+(this.escala*34.545)+' L '+(this.escala*12.727)+' '+(this.escala*27.273)+' C '+(this.escala*9.091)+' '+(this.escala*23.636)+' '+(this.escala*9.091)+' '+(this.escala*16.364)+' '+(this.escala*12.727)+' '+(this.escala*12.727)+' L '+(this.escala*5.455)+' '+(this.escala*5.455));
                this.referencia_grafico_svg.rotate(270)
                this.referencia_grafico_svg.translate(this.posicion_x + 0 + this.grosor_linea,this.posicion_y + 40.00333*this.escala + this.grosor_linea)
                break;
            case "centro":
                this.referencia_grafico_svg.height(20.001665*this.escala)
                this.referencia_grafico_svg.width(20.001665*this.escala)
                this.referencia_grafico_svg.translate(this.posicion_x + 10.0008325*(this.escala-1) + this.grosor_linea  ,this.posicion_y + 10.0008325*(this.escala-1) + this.grosor_linea)
                break;
        }
    }

    protected ActualizarPosicion(){
        switch(this.ubicacion){
            case "izquierda":
                this.referencia_grafico_svg.translate(this.posicion_x + this.grosor_linea, this.posicion_y + this.grosor_linea)
                break;
            case "arriba":
                this.referencia_grafico_svg.translate(this.posicion_x + 40.00333*this.escala + this.grosor_linea,this.posicion_y + 0 + this.grosor_linea)
                break;
            case "derecha":
                this.referencia_grafico_svg.translate(this.posicion_x + 40.00333*this.escala + this.grosor_linea,this.posicion_y + 40.00333*this.escala + this.grosor_linea)
                break;
            case "abajo":
                this.referencia_grafico_svg.translate(this.posicion_x + 0 + this.grosor_linea,this.posicion_y + 40.00333*this.escala + this.grosor_linea)
                break;
            case "centro":
                this.referencia_grafico_svg.translate(this.posicion_x + 10.0008325*(this.escala-1) + this.grosor_linea  ,this.posicion_y + 10.0008325*(this.escala-1) + this.grosor_linea)
                break;
        }
    }

}