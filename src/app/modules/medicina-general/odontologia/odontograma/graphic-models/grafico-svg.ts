import { OdontogramaComponent } from '../components/odontograma/odontograma.component';

export abstract class GraficoSVG {

    private _componente_odontograma: OdontogramaComponent;
    public get componente_odontograma(): OdontogramaComponent {return this._componente_odontograma;}
    public set componente_odontograma(value: OdontogramaComponent) {this._componente_odontograma = value; }

    public referencia_grafico_svg:any

    protected lienzo:any;

    protected _escala:number = 1
    protected _grosor_linea:number = 1
    protected _color_linea:string = '#1FABED'
    protected _color_relleno:string = '#FFFFFF'
    protected _forma_fin_linea:string = 'round'
    protected _posicion_x:number = 0
    protected _posicion_y:number = 0

    public get escala() {return this._escala;}
    public set escala(value) {this._escala = value;this.ActualizarEscala();}

    public get grosor_linea() {return this._grosor_linea;}
    public set grosor_linea(value) {this._grosor_linea = value;this.ActualizarGrosorLinea();}

    public get color_linea() {return this._color_linea;}
    public set color_linea(value) {this._color_linea = value;this.ActualizarColorLinea();}

    public get color_relleno() {return this._color_relleno;}
    public set color_relleno(value) {this._color_relleno = value;this.ActualizarColorRelleno()}

    public get forma_fin_linea() {return this._forma_fin_linea;}
    public set forma_fin_linea(value) {this._forma_fin_linea = value;this.ActualizarFormaFinLinea();}

    public get posicion_x() { return this._posicion_x;}
    public set posicion_x(value) {this._posicion_x = value;this.ActualizarPosicion()}

    public get posicion_y() {return this._posicion_y;}
    public set posicion_y(value) {this._posicion_y = value;this.ActualizarPosicion()}

    protected constructor(lienzo){
        this.lienzo = lienzo;
    }

    // MÉTODO QUE DIBUJA EL GRÁFICO INICIAL
    protected abstract GenerarGrafico(): void;
    // MÉTODO QUE GENERA LOS EVENTOS
    protected abstract GenerarEventos(): void;

    // MÉTODOS QUE SE DEBEN SOBREESCRIBIR YA QUE CADA TIPO DE GRÁFICO LO HACE DIFERENTE (LINEAS, RECTÁNGULOS,ETC)
    // SERÁN LLAMADOS CADA QUE SE MODIFIQUE ALGUNO DE LOS ATRIBUTOS CORRESPONDIENTES
    protected abstract ActualizarEscala(): void;
    protected abstract ActualizarGrosorLinea(): void;
    protected abstract ActualizarColorLinea(): void;
    protected abstract ActualizarColorRelleno(): void;
    protected abstract ActualizarFormaFinLinea(): void;
    protected abstract ActualizarPosicion(): void;
}
