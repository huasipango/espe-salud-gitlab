import { Cara } from './cara';
import { OdontogramaComponent } from '../components/odontograma/odontograma.component';

export class CaraPermanente extends Cara{

    constructor(lienzo, ubicacion,odontograma_component:OdontogramaComponent){
        super(lienzo,ubicacion,odontograma_component)
    }

    //SOBRESCRITURA DE MÉTODOS GRÁFICOS DE GraficoSVG
    protected GenerarGrafico(){
        switch(this.ubicacion){
            case "izquierda":
                this.referencia_grafico_svg = this.lienzo.path('M 0 0 L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*10)+' '+(this.escala*30)+' L 0 '+(this.escala*40)+' L 0 0');
                this.referencia_grafico_svg.translate(this.posicion_x + this.grosor_linea, this.posicion_y + this.grosor_linea)
                break;
            case "arriba":
                this.referencia_grafico_svg = this.lienzo.path('M 0 0 L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*10)+' '+(this.escala*30)+' L 0 '+(this.escala*40)+' L 0 0');
                this.referencia_grafico_svg.rotate(90)
                this.referencia_grafico_svg.translate(this.posicion_x + 40*this.escala + this.grosor_linea,this.posicion_y + 0 + this.grosor_linea)
                break;
            case "derecha":
                this.referencia_grafico_svg = this.lienzo.path('M 0 0 L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*10)+' '+(this.escala*30)+' L 0 '+(this.escala*40)+' L 0 0');
                this.referencia_grafico_svg.rotate(180)
                this.referencia_grafico_svg.translate(this.posicion_x + 40*this.escala + this.grosor_linea,this.posicion_y + 40*this.escala + this.grosor_linea)
                break;
            case "abajo":
                this.referencia_grafico_svg = this.lienzo.path('M 0 0 L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*10)+' '+(this.escala*30)+' L 0 '+(this.escala*40)+' L 0 0');
                this.referencia_grafico_svg.rotate(270)
                this.referencia_grafico_svg.translate(this.posicion_x + 0 + this.grosor_linea,this.posicion_y + 40*this.escala + this.grosor_linea)
                break;
            case "centro":
                this.referencia_grafico_svg = this.lienzo.rect(20*this.escala,20*this.escala);
                this.referencia_grafico_svg.translate(this.posicion_x + 10*this.escala + this.grosor_linea  ,this.posicion_y + 10*this.escala + this.grosor_linea)
                break;
        }
        
        this.referencia_grafico_svg.fill(this.color_relleno);
        this.referencia_grafico_svg.stroke({ color: this.color_linea, width: this.grosor_linea, linecap: this.forma_fin_linea , linejoin: this.forma_fin_linea })
        //PARA FACILITAR LA CREACIÓN DE LNEAS EN SVG https://yqnn.github.io/svg-path-editor/
    }
    protected ActualizarEscala(){
        switch(this.ubicacion){
            case "izquierda":
                this.referencia_grafico_svg.plot('M 0 0 L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*10)+' '+(this.escala*30)+' L 0 '+(this.escala*40)+' L 0 0');
                this.referencia_grafico_svg.translate(this.posicion_x + this.grosor_linea, this.posicion_y + this.grosor_linea)
                break;
            case "arriba":
                this.referencia_grafico_svg.plot('M 0 0 L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*10)+' '+(this.escala*30)+' L 0 '+(this.escala*40)+' L 0 0');
                this.referencia_grafico_svg.rotate(90)
                this.referencia_grafico_svg.translate(this.posicion_x + 40*this.escala + this.grosor_linea,this.posicion_y + 0 + this.grosor_linea)
                break;
            case "derecha":
                this.referencia_grafico_svg.plot('M 0 0 L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*10)+' '+(this.escala*30)+' L 0 '+(this.escala*40)+' L 0 0');
                this.referencia_grafico_svg.rotate(180)
                this.referencia_grafico_svg.translate(this.posicion_x + 40*this.escala + this.grosor_linea,this.posicion_y + 40*this.escala + this.grosor_linea)
                break;
            case "abajo":
                this.referencia_grafico_svg.plot('M 0 0 L '+(this.escala*10)+' '+(this.escala*10)+' L '+(this.escala*10)+' '+(this.escala*30)+' L 0 '+(this.escala*40)+' L 0 0');
                this.referencia_grafico_svg.rotate(270)
                this.referencia_grafico_svg.translate(this.posicion_x + 0 + this.grosor_linea,this.posicion_y + 40*this.escala + this.grosor_linea)
                break;
            case "centro":
                this.referencia_grafico_svg.height(20*this.escala)
                this.referencia_grafico_svg.width(20*this.escala)
                this.referencia_grafico_svg.translate(this.posicion_x + 10*this.escala + this.grosor_linea  ,this.posicion_y + 10*this.escala + this.grosor_linea)
                break;
        }
    }

    protected ActualizarPosicion(){
        switch(this.ubicacion){
            case "izquierda":
                this.referencia_grafico_svg.translate(this.posicion_x + this.grosor_linea, this.posicion_y + this.grosor_linea)
                break;
            case "arriba":
                this.referencia_grafico_svg.translate(this.posicion_x + 40*this.escala + this.grosor_linea,this.posicion_y + 0 + this.grosor_linea)
                break;
            case "derecha":
                this.referencia_grafico_svg.translate(this.posicion_x + 40*this.escala + this.grosor_linea,this.posicion_y + 40*this.escala + this.grosor_linea)
                break;
            case "abajo":
                this.referencia_grafico_svg.translate(this.posicion_x + 0 + this.grosor_linea,this.posicion_y + 40*this.escala + this.grosor_linea)
                break;
            case "centro":
                this.referencia_grafico_svg.translate(this.posicion_x + 10*this.escala + this.grosor_linea  ,this.posicion_y + 10*this.escala + this.grosor_linea)
                break;
        }
    }

}