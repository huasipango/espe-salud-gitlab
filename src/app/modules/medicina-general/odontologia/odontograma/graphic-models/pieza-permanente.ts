import { CaraPermanente } from './cara-permanente';
import { Pieza } from './pieza';
import { OdontogramaComponent } from '../components/odontograma/odontograma.component';

export class PiezaPermanente extends Pieza{


    //TANTO MOVILIDAD COMO RECESIÓN DEBEN SER GESTIONADOS CON INPUTS HTML
    protected _movilidad: number;
    public get movilidad(): number {return this._movilidad;}
    public set movilidad(value: number) {
        this._movilidad = value;
    }

    protected _recesion: number;
    public get recesion(): number {return this._recesion;}
    public set recesion(value: number) {
        this._recesion = value;
    }
    
    constructor(lienzo, numero_pieza,odontograma_component:OdontogramaComponent){
        super(lienzo, numero_pieza,odontograma_component)
    }

    protected GenerarGrafico(): void{
        this.cara_izquierda = new CaraPermanente(this.lienzo,"izquierda",this.componente_odontograma);
        this.cara_arriba = new CaraPermanente(this.lienzo,"arriba",this.componente_odontograma);
        this.cara_derecha = new CaraPermanente(this.lienzo,"derecha",this.componente_odontograma);
        this.cara_abajo = new CaraPermanente(this.lienzo,"abajo",this.componente_odontograma);
        this.cara_centro = new CaraPermanente(this.lienzo,"centro",this.componente_odontograma);

        this.referencia_grafico_svg.add(this.cara_izquierda.referencia_grafico_svg)
        this.referencia_grafico_svg.add(this.cara_arriba.referencia_grafico_svg)
        this.referencia_grafico_svg.add(this.cara_derecha.referencia_grafico_svg)
        this.referencia_grafico_svg.add(this.cara_abajo.referencia_grafico_svg)
        this.referencia_grafico_svg.add(this.cara_centro.referencia_grafico_svg)
    }

}