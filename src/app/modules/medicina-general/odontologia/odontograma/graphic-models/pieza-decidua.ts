import { CaraDecidua } from './cara-decidua';
import { Pieza } from './pieza';
import { OdontogramaComponent } from '../components/odontograma/odontograma.component';

export class PiezaDecidua extends Pieza{

    constructor(lienzo, numero_pieza, odontograma_component:OdontogramaComponent){
        super(lienzo, numero_pieza, odontograma_component)
    }

    protected GenerarGrafico(): void{
        this.cara_izquierda = new CaraDecidua(this.lienzo,"izquierda",this.componente_odontograma);
        this.cara_arriba = new CaraDecidua(this.lienzo,"arriba",this.componente_odontograma);
        this.cara_derecha = new CaraDecidua(this.lienzo,"derecha",this.componente_odontograma);
        this.cara_abajo = new CaraDecidua(this.lienzo,"abajo",this.componente_odontograma);
        this.cara_centro = new CaraDecidua(this.lienzo,"centro",this.componente_odontograma);

        
        //Agrupando Caras
        this.referencia_grafico_svg.add(this.cara_izquierda.referencia_grafico_svg)
        this.referencia_grafico_svg.add(this.cara_arriba.referencia_grafico_svg)
        this.referencia_grafico_svg.add(this.cara_derecha.referencia_grafico_svg)
        this.referencia_grafico_svg.add(this.cara_abajo.referencia_grafico_svg)
        this.referencia_grafico_svg.add(this.cara_centro.referencia_grafico_svg)
        
    }

}