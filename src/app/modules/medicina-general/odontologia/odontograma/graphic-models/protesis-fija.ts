import { Protesis } from './protesis';
import { Pieza } from './pieza';
import { OdontogramaComponent } from '../components/odontograma/odontograma.component';
import { AdaptadorGraficoModelos } from '../components/odontograma/adaptador-grafico-modelos';

export class ProtesisFija extends Protesis{
    constructor(lienzo:any,odontograma_component:OdontogramaComponent){
        super(lienzo,odontograma_component);
        this.GenerarEventos();
    }
    
    protected GenerarEventos(): void {
        this.referencia_grafico_svg.on('mouseenter', ()=>{
            if(OdontogramaComponent.detalle_a_aplicar == "borrar"||OdontogramaComponent.detalle_a_aplicar == "protesis_fija_necesaria"
            ||OdontogramaComponent.detalle_a_aplicar == "protesis_fija_aplicada"){
                this.grosor_linea = 5
            }
        })
        this.referencia_grafico_svg.on('mouseleave', ()=>{
            if(OdontogramaComponent.detalle_a_aplicar == "borrar"||OdontogramaComponent.detalle_a_aplicar == "protesis_fija_necesaria"
            ||OdontogramaComponent.detalle_a_aplicar == "protesis_fija_aplicada"){
                
                this.grosor_linea = 4
            }
        })
        this.referencia_grafico_svg.on('click', (event)=>{
            this.componente_odontograma.mensaje_finalizar_cancelar.opacity(0)
            if(OdontogramaComponent.detalle_a_aplicar == "borrar" || !(event instanceof MouseEvent)){
                //quitando el detalle de las piezas
                this.piezas.forEach((pieza)=>{
                    pieza.protesis_fija_aplicada = false
                    pieza.protesis_fija_necesaria = false                    
                });
                //quitando el detalle de la lista 
                this.componente_odontograma.protesis.forEach( (item, index) => {
                    if(item === this){ 
                        this.componente_odontograma.protesis.splice(index,1);
                    }
                });
                this.referencia_grafico_svg.remove();
                this.piezas = null;
                this.componente_odontograma.auxiliar_protesis_fija = null;
                this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
                this.componente_odontograma = null;
            }
            else if(OdontogramaComponent.detalle_a_aplicar == "protesis_fija_aplicada"){
                if(this.componente_odontograma.auxiliar_protesis_fija == null){//si no se está creando una protesis
                    if(!this.aplicada){
                        this.aplicada = true
                        this.piezas.forEach((aux_piezas)=>{
                            aux_piezas.QuitarTodosDetalles();
                        });
                    }
                    this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
                }
                else{//si se está creando una prótesis
                    if(this == this.componente_odontograma.auxiliar_protesis_fija){//si se clickeo la misma protesis q se está creando(la pieza pertenece a la protesis q se está creando)
                        this.componente_odontograma.protesis.push(this.componente_odontograma.auxiliar_protesis_fija);
                        this.componente_odontograma.auxiliar_protesis_fija.referencia_grafico_svg.opacity(1);
                        this.componente_odontograma.auxiliar_protesis_fija = null;
                        if(this.aplicada){
                            this.piezas.forEach((pieza)=>{
                              pieza.QuitarTodosDetalles();
                            });
                        }
                        this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
                    }
                    else{//si se clickeo una proteis distinta (si la pieza pertenece a otra prótesis)
                        this.componente_odontograma.auxiliar_protesis_fija.referencia_grafico_svg.fire('click');//borrar la protesis q se estaba creando
                    }
                }         
            }
            else if(OdontogramaComponent.detalle_a_aplicar == "protesis_fija_necesaria"){
                if(this.componente_odontograma.auxiliar_protesis_fija == null){//si no se está creando una protesis
                    if(this.aplicada){
                        this.aplicada = false
                    }
                    this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
                }
                else{//si se está creando una prótesis
                    if(this == this.componente_odontograma.auxiliar_protesis_fija){//si se clickeo la misma protesis q se está creando(la pieza pertenece a la protesis q se está creando)
                        this.componente_odontograma.protesis.push(this.componente_odontograma.auxiliar_protesis_fija);
                        this.componente_odontograma.auxiliar_protesis_fija.referencia_grafico_svg.opacity(1);
                        this.componente_odontograma.auxiliar_protesis_fija = null;
                        this.componente_odontograma.calcularIndices(AdaptadorGraficoModelos.convertirGraficoDetalles(this.componente_odontograma))
                    }
                    else{//si se clickeo una proteis distinta (si la pieza pertenece a otra prótesis)
                        this.componente_odontograma.auxiliar_protesis_fija.referencia_grafico_svg.fire('click');//borrar la protesis q se estaba creando
                    }
                }     
            }
        })
    }




    public AgregarPieza(pieza: Pieza) {
        
        /*VERIFICANDO SI LA PIEZA YA ESTÁ AGREGADA EN LA LISTA DE ESTA PROTESIS*/
        let agregar = true
        this.piezas.forEach( (item) => {
            if(item === pieza) {agregar = false}
          });
        if(agregar){
            if(pieza.protesis_fija_aplicada || pieza.protesis_fija_necesaria||//SI LA PIEZA PERTENECE A OTRA PROTESIS
                pieza.protesis_total_aplicada || pieza.protesis_total_necesaria||
                pieza.protesis_removible_aplicada || pieza.protesis_removible_necesaria||
                pieza.protesis_corona_aplicada || pieza.protesis_corona_necesaria
                ){
                    this.referencia_grafico_svg.fire('click');
                    return;
            }
            else{
                this.piezas.push(pieza)
            }
        }
        /*BORRANDO LA AGRUPACIÓN GRÁFICA ANTERIOR Y CREANDO NUEVAMENTE*/
         try{
            this.referencia_grafico_svg.remove();
        }catch(error){};
        this.referencia_grafico_svg= null;
        this.referencia_grafico_svg = this.lienzo.group();
        /*GENERANDO EVENTOS NUEVAMENTE*/
        this.GenerarEventos();
        /*OBTENIENDO LA NUMERACIÓN DE LAS PIEZAS*/
        let numeracion = [];
        this.piezas.forEach((pieza)=>{
            numeracion.push(pieza.numero_pieza);
        });
        /*ORDENANDO LAS PIEZAS DE IZQUIERDA A DERECHA*/
        let extremos = [];
        try{
            extremos = AdaptadorGraficoModelos.identificarExtremos(numeracion);
        }catch(error){
            console.log('error pieza no coincide');
            this.referencia_grafico_svg.fire('click');
            return;
        }
       
        /*DETERMINANDO EL DIBUJO DE LA PROTESIS EN CADA PIEZA*/
        this.piezas.forEach((pieza)=>{
            if(extremos[0] == extremos[extremos.length-1]){
                pieza.posicion_en_protesis_fija = 'inicio_final'
            }
            else if(pieza.numero_pieza == extremos[0]){
                pieza.posicion_en_protesis_fija = 'inicio'
            }
            else if(pieza.numero_pieza == extremos[extremos.length-1]){
                pieza.posicion_en_protesis_fija = 'final'
            }
            else{
                pieza.posicion_en_protesis_fija = 'centro'
            }
            if(this.aplicada){
                pieza.protesis_fija_aplicada = true;
            }
            else{
                pieza.protesis_fija_necesaria = true;
            }
            this.referencia_grafico_svg.add(pieza.referencia_grafico_protesis_fija);
        });
        this.referencia_grafico_svg.opacity(0.25);

        //Obteniendo todas las piezas intermedias para el autollenado
        let piezas_intermedias = AdaptadorGraficoModelos.generarGrupos(extremos[0],extremos[extremos.length-1]);        
        if(this.piezas[0].numero_pieza<=48){//si es una protesis de piezas permanentes
            //Obteniendo las piezas intermedias faltantes
            let piezas_intermedias_faltantes = [];
            piezas_intermedias.forEach((numero_pieza)=>{
                if(!this.ContienePieza(this.componente_odontograma.piezasPermanentes[numero_pieza])){
                    piezas_intermedias_faltantes.push(numero_pieza);
                }
            });
            piezas_intermedias_faltantes.forEach((numero_pieza)=>{
                this.AgregarPieza(this.componente_odontograma.piezasPermanentes[numero_pieza]);
            });
        }
        else{//si es una protesis de piezas deciduas
            //Obteniendo las piezas intermedias faltantes
            let piezas_intermedias_faltantes = [];
            piezas_intermedias.forEach((numero_pieza)=>{
                if(!this.ContienePieza(this.componente_odontograma.piezasDeciduas[numero_pieza])){
                    piezas_intermedias_faltantes.push(numero_pieza);
                }
            });
            piezas_intermedias_faltantes.forEach((numero_pieza)=>{
                this.AgregarPieza(this.componente_odontograma.piezasDeciduas[numero_pieza]);
            });
        }
    }
    public QuitarPieza(pieza: Pieza) {
        /*QUITANDO LA PIEZA DE LA LISTA*/
        this.piezas.forEach( (item, index) => {
            if(item === pieza){ 
                this.piezas.splice(index,1);
                pieza.protesis_fija_aplicada = false
                pieza.protesis_fija_necesaria = false
            }
          });
        /*BORRANDO LA AGRUPACIÓN GRÁFICA ANTERIOR Y CREANDO NUEVAMENTE*/
        try{
            this.referencia_grafico_svg.remove();
        }catch(error){};
        this.referencia_grafico_svg= null;
        this.referencia_grafico_svg = this.lienzo.group();
        /*GENERANDO EVENTOS NUEVAMENTE*/
        this.GenerarEventos();
        /*OBTENIENDO LA NUMERACIÓN DE LAS PIEZAS*/
        let numeracion = [];
        this.piezas.forEach((pieza)=>{
            numeracion.push(pieza.numero_pieza);
        });
        /*ORDENANDO LAS PIEZAS DE IZQUIERDA A DERECHA*/
        let extremos = AdaptadorGraficoModelos.identificarExtremos(numeracion);
        /*DETERMINANDO EL DIBUJO DE LA PROTESIS EN CADA PIEZA*/
        this.piezas.forEach((pieza)=>{
            if(extremos[0] == extremos[extremos.length-1]){
                pieza.posicion_en_protesis_fija = 'inicio_final'
            }
            else if(pieza.numero_pieza == extremos[0]){
                pieza.posicion_en_protesis_fija = 'inicio'
            }
            else if(pieza.numero_pieza == extremos[extremos.length-1]){
                pieza.posicion_en_protesis_fija = 'final'
            }
            else{
                pieza.posicion_en_protesis_fija = 'centro'
            }
            if(this.aplicada){
                pieza.protesis_fija_aplicada = true;
            }
            else{
                pieza.protesis_fija_necesaria = true;
            }
            this.referencia_grafico_svg.add(pieza.referencia_grafico_protesis_fija);
        });
        this.referencia_grafico_svg.opacity(0.25);
    }


    protected ActualizarColorLinea(): void {
        try{
            this.referencia_grafico_svg.remove();
        }catch(error){};
        this.referencia_grafico_svg= null;
        this.referencia_grafico_svg = this.lienzo.group();
        this.GenerarEventos();

        if(this.aplicada){
            this.piezas.forEach( (item) => {
                item.protesis_fija_aplicada = true;
                this.referencia_grafico_svg.add(item.referencia_grafico_protesis_fija);
              });
        }else{
            this.piezas.forEach( (item) => {
                item.protesis_fija_necesaria = true;
                this.referencia_grafico_svg.add(item.referencia_grafico_protesis_fija);
              });
        }
    }
    protected ActualizarGrosorLinea(): void {
        this.piezas.forEach( (pieza) => {
            pieza.referencia_grafico_protesis_fija.stroke({width: this.grosor_linea})
          });
    }
}