import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from 'src/@vex/interfaces/vex-route.interface';
import { RoleGuard } from 'src/app/core/auth/role.guard';
import { RolEnum } from 'src/app/core/enums/rol-enum';
import { OdontogramaComponent } from './components/odontograma/odontograma.component';

const routes: VexRoutes = [
  {
    path: '',
    component: OdontogramaComponent,
    canActivate: [RoleGuard],
    data: {
      toolbarShadowEnabled: true,
      roles: [RolEnum.ROLE_ODONTOLOGO]
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OdontogramaRoutingModule { }
