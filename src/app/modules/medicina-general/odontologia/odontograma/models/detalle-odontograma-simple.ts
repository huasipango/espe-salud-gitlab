import { DetalleOdontogramaGrafico } from './detalle-odontograma-grafico'

export class DetalleOdontogramaSimple extends DetalleOdontogramaGrafico{
    pieza:number
    cara:string
    constructor(data?){
        super()
        if(data){
            this.tipo = data.tipo
            this.realizado = data.realizado
            this.pieza = data.pieza
            this.cara = data.cara
        }
    }
} 