import { DetalleOdontogramaGrafico } from './detalle-odontograma-grafico'

export class DetalleOdontogramaCuantitativo extends DetalleOdontogramaGrafico{
    nivel:number
    pieza:number
    constructor(data?){
        super()
        if(data){
            this.tipo = data.tipo;
            this.pieza = data.pieza;
            this.nivel = data.nivel
        }
    }
} 