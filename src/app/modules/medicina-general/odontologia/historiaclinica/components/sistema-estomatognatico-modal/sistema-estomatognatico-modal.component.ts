import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Paciente} from '../../../../../../core/models/paciente/paciente.model';
import {Observable} from 'rxjs';
import icCheck from '@iconify/icons-ic/check';
import icClose from '@iconify/icons-ic/twotone-close';
import {ExamenSistemaEstomatognatico} from '../../models/examen-sistema-estomatognatico';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {PacienteGlobalService} from '../../../../../../core/services/paciente-global.service';
import {LoadingService} from '../../../../../../core/services/loading.service';
import {SistemaEstomatognaticoService} from '../../services/sistema-estomatognatico.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ExamenInterno} from "../../../../organos-sistemas/models/examen-interno.models";

@Component({
  selector: 'vex-sistema-estomatognatico-modal',
  templateUrl: './sistema-estomatognatico-modal.component.html',
  styleUrls: ['./sistema-estomatognatico-modal.component.scss']
})
export class SistemaEstomatognaticoModalComponent implements OnInit {

  idPaciente: number;
  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  pacienteActual: Paciente;
  unidades = ['Labios', 'Mejillas', 'Maxila superior', 'Maxilar inferior', 'Lengua', 'Paladar', 'Piso', 'Carrillos', 'Glándulas salivales', 'Orofaringe', 'A. T. M.', 'Ganglios'];
  icClose = icClose;
  icCheck = icCheck;

  constructor(
    private dialogRef: MatDialogRef<SistemaEstomatognaticoModalComponent>,
    @Inject(MAT_DIALOG_DATA) public defaults: ExamenSistemaEstomatognatico,
    private fb: FormBuilder,
    private pacienteGlobalService: PacienteGlobalService,
    private loadingService: LoadingService,
    private sistemaEstomatognaticoService: SistemaEstomatognaticoService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as ExamenSistemaEstomatognatico;
    }
    this.form = this.fb.group({
      id: [this.defaults.id || null],
      unidadEstomatognatica: [this.defaults.unidadEstomatognatica || null, Validators.required],
      codigoCIEAsociado: [this.defaults.codigoCIEAsociado || '', Validators.required],
      idHsitoria: [this.defaults.idHistoria || null],
    });
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.idPaciente = this.pacienteActual.id;
          this.form.patchValue({idPaciente: paciente.id});
        }
      });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  guardar() {
    const examen: ExamenSistemaEstomatognatico = this.form.value;
    if (this.mode === 'create') {
      this.crearExamen(examen);
    } else if (this.mode === 'update') {
      this.actualizarExamen(examen);
    }
  }

  crearExamen(examen: ExamenSistemaEstomatognatico): void {
    this.loadingService.showLoaderUntilCompleted(
      this.sistemaEstomatognaticoService.createExamenSistemaEstomatognatico(examen, this.idPaciente)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      } else {
        this.showNotification('No se pudo crear el registro', 'CERRAR');
      }
    });
  }

  actualizarExamen(examen: ExamenSistemaEstomatognatico): void {
    this.loadingService.showLoaderUntilCompleted(
      this.sistemaEstomatognaticoService.updateExamenSistemaEstomatognatico(examen, this.idPaciente)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      } else{
        this.showNotification('No se pudo actualizar el registro', 'CERRAR');
      }
    });
  }

  showNotification(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000
    });
  }
}
