import {Component, OnInit} from '@angular/core';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {ExamenSistemaEstomatognatico} from '../../models/examen-sistema-estomatognatico';
import {MatTableDataSource} from '@angular/material/table';
import {FormControl} from '@angular/forms';
import {ReplaySubject} from 'rxjs';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icFolder from '@iconify/icons-ic/twotone-folder';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icPrint from '@iconify/icons-ic/print';
import {ExamenLaboratorio} from '../../../../examen-laboratorio/models/examen-laboratorio.model';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {SistemaEstomatognaticoService} from '../../services/sistema-estomatognatico.service';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {SistemaEstomatognaticoModalComponent} from '../sistema-estomatognatico-modal/sistema-estomatognatico-modal.component';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'vex-sistema-estomatognatico',
  templateUrl: './sistema-estomatognatico.component.html',
  styleUrls: ['./sistema-estomatognatico.component.scss'],
  animations: [fadeInUp400ms, stagger40ms, scaleFadeIn400ms],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class SistemaEstomatognaticoComponent implements OnInit {

  subject$: ReplaySubject<ExamenSistemaEstomatognatico[]> = new ReplaySubject<ExamenSistemaEstomatognatico[]>(1);

  detalleSistemaEstomatognatico: ExamenSistemaEstomatognatico[];

  columns: TableColumn<ExamenSistemaEstomatognatico>[] = [
    {label: 'Unidad', property: 'unidadEstomatognatica', type: 'text', visible: true},
    {label: 'CIE', property: 'codigoCIEAsociado', type: 'text', visible: true},
    {label: 'Acciones', property: 'actions', type: 'button', visible: true}
  ];

  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<ExamenSistemaEstomatognatico> | null;

  pacienteActual: Paciente;
  nombreCompleto: string;
  searchCtrl = new FormControl();

  icEdit = icEdit;
  icSearch = icSearch;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icMoreHoriz = icMoreHoriz;
  icFolder = icFolder;
  icPrint = icPrint;

  constructor(
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private sistemaEstomatognaticoService: SistemaEstomatognaticoService,
    private pacienteGlobalService: PacienteGlobalService
  ) {
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.obtenerDetalles();
        }
      });
    this.searchCtrl.valueChanges.pipe(
      untilDestroyed(this)
    ).subscribe(value => this.onFilterChange(value));
  }

  onFilterChange(value: string) {
    if (!this.dataSource) {
      return;
    }
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  obtenerDetalles() {
    this.sistemaEstomatognaticoService.getExamenesSistemaEstomatognatico(this.pacienteActual.id)
      .subscribe(detalles => {
        console.log(detalles);
        this.detalleSistemaEstomatognatico = detalles;
        this.dataSource.data = this.detalleSistemaEstomatognatico;
        // this.subject$.next(detalles);
      });
  }

  iniciarEliminacionDetalle(
    listExamenes?: ExamenLaboratorio[],
    examen?: ExamenLaboratorio) {
    let message = '¿Esta seguro de eliminar este registro?';
    if (examen) {
      listExamenes = new Array<ExamenLaboratorio>();
      listExamenes.push(examen);
    } else if (listExamenes.length > 1) {
      message = '¿Estas seguro de eliminar ' + listExamenes.length + ' registros?';
    }
    this.openDeleteDialog(message, listExamenes);
  }

  openDeleteDialog(message: string, listExamanesLaboratorio: ExamenLaboratorio[]) {
    this.dialog.open(DeleteModalComponent, {
      data: message,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') {
        this.eliminarDetalle(listExamanesLaboratorio);
      }
    });
  }

  openCreateDialog() {
    this.dialog.open(SistemaEstomatognaticoModalComponent, {
      width: '600px',
      maxWidth: '100%',
      disableClose: true
    }).afterClosed()
      .subscribe((detalle: ExamenSistemaEstomatognatico) => {
        if (detalle) {
          this.showNotification('Registro creado EXITOSAMENTE', 'OK');
          this.detalleSistemaEstomatognatico.push(detalle);
          this.dataSource.connect().next(this.detalleSistemaEstomatognatico);
        }
      });
  }

  eliminarDetalle(examenes: ExamenLaboratorio[]) {
    const size = examenes.length;
    examenes.forEach(examen => {
      this.detalleSistemaEstomatognatico.splice(
        this.detalleSistemaEstomatognatico.findIndex(
          existingExamen =>
            existingExamen.id === examen.id
        ),
        1
      );
    });
    if (size > 1) {
      this.showNotification('Registros eliminados EXITOSAMENTE', 'OK');
    } else {
      this.showNotification('Registro eliminado EXITOSAMENTE', 'OK');
    }
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }
}
