import {Component, OnInit} from '@angular/core';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {HistoriaClinicaOdontologica} from '../../models/historia-clinica-odontologica.model';
import {HistoriaClinicaService} from '../../services/historia-clinica.service';
import {DatePipe} from '@angular/common';

import icFecha from '@iconify/icons-ic/date-range';
import icHora from '@iconify/icons-ic/hourglass-empty';
import icPerson from '@iconify/icons-ic/person';
import icNotesMedical from '@iconify/icons-fa-solid/notes-medical';

@Component({
  selector: 'vex-historia-clinica',
  templateUrl: './historia-clinica.component.html',
  styleUrls: ['./historia-clinica.component.scss'],
  animations: [
    scaleIn400ms,
    fadeInRight400ms
  ]
})
export class HistoriaClinicaComponent implements OnInit {
  pacienteActual: Paciente;
  nombreCompleto: string;
  historiaClinica = null;

  icFecha = icFecha;
  icHora = icHora;
  icPerson = icPerson;
  icNotesMedical = icNotesMedical;

  constructor(
    private pacienteGlobalService: PacienteGlobalService,
    private historiaClinicaService: HistoriaClinicaService,
    private datePipe: DatePipe
  ) {
  }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.nombreCompleto = this.pacienteActual.apellidoPaterno.toUpperCase() + ' ' + this.pacienteActual.primerNombre.toUpperCase();
          this.verifyExistHistoriaClinica();
        }
      });
  }

  verifyExistHistoriaClinica(): void {
    this.historiaClinicaService.getHistoriaClinica(this.pacienteActual.id)
      .subscribe(
        data => {
          if (data) {
            this.historiaClinica = new HistoriaClinicaOdontologica(data);
          } else {
            this.historiaClinica = null;
          }
        }
        , err => {
          this.historiaClinica = null;
        }
      );
  }

  abrirHistoriaClinica() {
    const nuevaHistoriaClinica = new HistoriaClinicaOdontologica();
    nuevaHistoriaClinica.id = 0;
    nuevaHistoriaClinica.profesional = 'Daniel Samaniego'; // TODO CAMBIAR POR EL NOMBRE DEL PROFESIONAL LOGUEADO
    nuevaHistoriaClinica.codigoProfesional = 'KM7389'; // TODO CAMBIAR POR EL CÓDIO DEL PROFESIONAL LOGUEADO
    nuevaHistoriaClinica.fechaApertura = this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm');
    nuevaHistoriaClinica.fechaControl = this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm');
    this.historiaClinicaService.createHistoriaClinica(nuevaHistoriaClinica, this.pacienteActual.id).subscribe(
      data => {
        if (data) {
          this.historiaClinica = new HistoriaClinicaOdontologica(data);
        } else {
          this.historiaClinica = null;
        }
      }
      , err => {
        this.historiaClinica = null;
      }
    );
  }

  actualizarHistoriaClinica() {
    this.historiaClinica.fechaControl = this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm');
    this.historiaClinicaService.updateHistoriaClinica(this.historiaClinica, this.pacienteActual.id).subscribe(
      data => {
        if (data) {
          this.historiaClinica = new HistoriaClinicaOdontologica(data);
        } else {
          this.historiaClinica = null;
        }
      }, err => {
        this.historiaClinica = null;
      }
    );
  }
}

