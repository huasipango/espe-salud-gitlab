export class HistoriaClinicaOdontologica {
    id: number;
    fechaApertura: string;;
    fechaControl: string;
    profesional: string;
    codigoProfesional: string;

    constructor(data?){
        if (data){
            this.id = data.id;
            this.fechaApertura = data.fechaApertura;
            this.fechaControl = data.fechaControl;
            this.profesional = data.profesional;
            this.codigoProfesional = data.codigoProfesional;
        }
    }
}
