export class ExamenSistemaEstomatognatico {
  id: number;
  unidadEstomatognatica: string;
  codigoCIEAsociado: string;
  idHistoria: string;
}
