import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HistoriaClinicaComponent} from './components/historia-clinica/historia-clinica.component';

import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconModule } from '@angular/material/icon';
import { IconModule } from '@visurel/iconify-angular';
import { BreadcrumbsModule } from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import { MatButtonModule } from '@angular/material/button';
import { PageLayoutModule } from 'src/@vex/components/page-layout/page-layout.module';
import { ContainerModule } from 'src/@vex/directives/container/container.module';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { EmptyPacienteModule } from '../../../../core/components/empty-paciente/empty-paciente.module';
import { HistoriaClinicaRoutingModule } from './historia-clinica-routing.module';
import { Routes } from '@angular/router';
import { HistoriaClinicaService } from './services/historia-clinica.service';
import { SistemaEstomatognaticoComponent } from './components/sistema-estomatognatico/sistema-estomatognatico.component';
import { EnfermedadProblemaActualComponent } from './components/enfermedad-problema-actual/enfermedad-problema-actual.component';
import { EnfermedadProblemaActualDialogComponent } from './components/enfermedad-problema-actual-dialog/enfermedad-problema-actual-dialog.component';
import { SistemaEstomatognaticoModalComponent } from './components/sistema-estomatognatico-modal/sistema-estomatognatico-modal.component';
import {LoadingModule} from "../../../../shared/components/loading/loading.module";
import {MatInputModule} from "@angular/material/input";



const routes: Routes = [
  {
    path: '',
    component: HistoriaClinicaComponent,
    data:{
      toolbarShadowEnabled:true
    }
  }
];

@NgModule({
  declarations: [HistoriaClinicaComponent, SistemaEstomatognaticoComponent, EnfermedadProblemaActualComponent, EnfermedadProblemaActualDialogComponent, SistemaEstomatognaticoModalComponent],
  imports: [
    CommonModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    HistoriaClinicaRoutingModule,
    EmptyPacienteModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatCheckboxModule,
    MatIconModule,
    MatButtonModule,
    MatSnackBarModule,
    MatMenuModule,
    MatDialogModule,
    IconModule,
    FormsModule,
    MatTooltipModule,
    ReactiveFormsModule,
    ContainerModule,
    MatSelectModule,
    MatButtonToggleModule,
    LoadingModule,
    MatInputModule
  ],
  // exports: [AccidenteTrabajoComponent],
  entryComponents: [],
  providers: [HistoriaClinicaService]
})

export class HistoriaClinicaModule { }
