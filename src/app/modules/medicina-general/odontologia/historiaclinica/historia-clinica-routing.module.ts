import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RoleGuard } from 'src/app/core/auth/role.guard';
import { RolEnum } from 'src/app/core/enums/rol-enum';
import { VexRoutes } from '../../../../../@vex/interfaces/vex-route.interface';
import { HistoriaClinicaComponent } from './components/historia-clinica/historia-clinica.component';

const routes: VexRoutes = [
  {
    path: '',
    component: HistoriaClinicaComponent,
    canActivate: [RoleGuard],
    data: {
      toolbarShadowEnabled: true,
      roles: [RolEnum.ROLE_ODONTOLOGO]
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HistoriaClinicaRoutingModule { }
