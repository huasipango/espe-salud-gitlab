import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {retry} from 'rxjs/operators';
import {ExamenSistemaEstomatognatico} from '../models/examen-sistema-estomatognatico';
import {environment} from "../../../../../../environments/environment";
import {HistoriaClinicaOdontologica} from "../models/historia-clinica-odontologica.model";

@Injectable({
  providedIn: 'root'
})
export class SistemaEstomatognaticoService {

  private baseUrl: string = 'http://localhost:8080/api/v1.0/';
  public identity;
  public token;
  public servicioURI: string;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Allow: 'GET, POST, HEAD'
    })
  };

  constructor(private http: HttpClient) {
    this.servicioURI = this.baseUrl + 'examen-sistema-estomatognatico';
  }

  // GET
  getExamenesSistemaEstomatognatico(idPaciente: number): Observable<ExamenSistemaEstomatognatico[]>  {
    return this.http.get<ExamenSistemaEstomatognatico[]>(
      this.servicioURI + '?idPaciente=' + idPaciente)
      .pipe(
        retry(1)
      );
  }

  // POST
  createExamenSistemaEstomatognatico(detalle: ExamenSistemaEstomatognatico,
                                     idPaciente): Observable<ExamenSistemaEstomatognatico>  {
    console.log(JSON.stringify(detalle));
    return this.http.post<ExamenSistemaEstomatognatico>(
      this.servicioURI + '?idPaciente=' + idPaciente,
      JSON.stringify(detalle), this.httpOptions)
      .pipe(
        retry(1)
      );
  }

  // PUT
  updateExamenSistemaEstomatognatico(detalle: ExamenSistemaEstomatognatico,
                                     idPaciente): Observable<ExamenSistemaEstomatognatico> {
    console.log(JSON.stringify(detalle));
    return this.http.put<ExamenSistemaEstomatognatico>(
      this.servicioURI + '/' + idPaciente,
      JSON.stringify(detalle), this.httpOptions)
      .pipe(
        retry(1)
      );
  }

  // DELETE
  deleteExamenSistemaEstomatognatico(idExamen): Observable<ExamenSistemaEstomatognatico> {
    return this.http.delete<ExamenSistemaEstomatognatico>(
      this.servicioURI + '/' + idExamen)
      .pipe(
        retry(1)
      );
  }
}
