import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HistoriaClinicaOdontologica } from '../models/historia-clinica-odontologica.model';


@Injectable({
  providedIn: 'root'
})
export class HistoriaClinicaService {

  private baseUrl: string = 'http://localhost:8080/api/v1.0/';
  public identity;
  public token;
  public servicioURI: string;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Allow: 'GET, POST, HEAD'
    })
  };

  constructor(private http: HttpClient) {
    this.servicioURI = this.baseUrl + 'historia-clinica-odontologica';
  }

  // GET
  getHistoriaClinica(idPaciente: number): Observable<HistoriaClinicaOdontologica[]>  {
    return this.http.get<HistoriaClinicaOdontologica[]>(
      this.servicioURI + '?idPaciente=' + idPaciente)
    .pipe(
      retry(1)
    );
  }

  // POST
  createHistoriaClinica(historiaClinicaOdontologica, idPaciente): Observable<HistoriaClinicaOdontologica>  {
    console.log(historiaClinicaOdontologica)
    return this.http.post<HistoriaClinicaOdontologica>(
      this.servicioURI + '?idPaciente=' + idPaciente,
      JSON.stringify(historiaClinicaOdontologica), this.httpOptions)
    .pipe(
      retry(1)
    );
  }

  // PUT
  updateHistoriaClinica(historiaClinicaOdontologica, idPaciente): Observable<HistoriaClinicaOdontologica>  {
    return this.http.put<HistoriaClinicaOdontologica>(
      this.servicioURI + '/' + idPaciente,
      JSON.stringify(historiaClinicaOdontologica), this.httpOptions)
    .pipe(
      retry(1)
    );
  }

  // DELETE
  deleteHistoriaClinica(idPaciente): Observable<HistoriaClinicaOdontologica>  {
    return this.http.delete<HistoriaClinicaOdontologica>(
      this.servicioURI + '/' + idPaciente)
    .pipe(
      retry(1)
    );
  }
}
