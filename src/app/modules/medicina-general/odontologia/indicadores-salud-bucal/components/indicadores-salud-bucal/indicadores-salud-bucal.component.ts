import { IndicadorSaludBucal } from './../../models/indicador-salud-bucal.model';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Paciente } from 'src/app/core/models/paciente/paciente.model';
import { IndicadoresSaludBucalService } from '../../services/indicadores-salud-bucal.service';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { PacienteGlobalService } from 'src/app/core/services/paciente-global.service';
import { DetalleHigieneOral } from '../../models/detalle-higiene-oral.model';
import icSave from '@iconify/icons-ic/save';

@Component({
  selector: 'vex-indicadores-salud-bucal',
  templateUrl: './indicadores-salud-bucal.component.html',
  styleUrls: ['./indicadores-salud-bucal.component.scss']
})

export class IndicadoresSaludBucalComponent implements OnInit {

  layoutCtrl = new FormControl('full');
  pacienteActual: Paciente;
  nombreCompleto: string;

  icSave = icSave;

  // Grupo 1
  pieza_1 = 0;
  nivel_placa_1 = 0;
  nivel_calculo_1 = 0;
  nivel_gingivitis_1 = 0;

  // Grupo 2
  pieza_2 = 0;
  nivel_placa_2 = 0;
  nivel_calculo_2 = 0;
  nivel_gingivitis_2 = 0;

  // Grupo 3
  pieza_3 = 0;
  nivel_placa_3 = 0;
  nivel_calculo_3 = 0;
  nivel_gingivitis_3 = 0;

  // Grupo 4
  pieza_4 = 0;
  nivel_placa_4 = 0;
  nivel_calculo_4 = 0;
  nivel_gingivitis_4 = 0;

  // Grupo 5
  pieza_5 = 0;
  nivel_placa_5 = 0;
  nivel_calculo_5 = 0;
  nivel_gingivitis_5 = 0;

  // Grupo 6
  pieza_6 = 0;
  nivel_placa_6 = 0;
  nivel_calculo_6 = 0;
  nivel_gingivitis_6 = 0;

  // Generales
  fluorosis = 0;
  mal_oclusion = 0;
  enfermedad_periodontal = 0;


  promedio_placa = 0;
  promedio_calculo = 0;
  promedio_gingivitis = 0;

  constructor(private pacienteGlobalService: PacienteGlobalService, private servicioIndicadoresSaludBucal: IndicadoresSaludBucalService) {}

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente){
          this.pacienteActual = paciente;
          this.nombreCompleto = this.nombreCompleto = this.pacienteActual.apellidoPaterno.toUpperCase()
            + ' ' + this.pacienteActual.primerNombre.toUpperCase();
          this.cargarIndicadores();
        }
      });
  }

  cargarIndicadores(){
    this.servicioIndicadoresSaludBucal.getIndicadoresSaludBucal(this.pacienteActual.id)
      .subscribe(res => {
        const indicadores: IndicadorSaludBucal = new IndicadorSaludBucal(res);
        this.fluorosis = indicadores.nivelFluorosis;
        this.mal_oclusion = indicadores.nivelMalOclusion;
        this.enfermedad_periodontal = indicadores.nivelEnfermedadPeriodontal;

        indicadores.detallesHigieneOral.forEach((detalle) => {
          if (detalle.seccion === 1){
            this.pieza_1 = detalle.pieza;
            this.nivel_calculo_1 = detalle.nivelCalculo;
            this.nivel_gingivitis_1 = detalle.nivelGingivitis;
            this.nivel_placa_1 = detalle.nivelPlaca;
          }
          else if (detalle.seccion === 2){
            this.pieza_2 = detalle.pieza;
            this.nivel_calculo_2 = detalle.nivelCalculo;
            this.nivel_gingivitis_2 = detalle.nivelGingivitis;
            this.nivel_placa_2 = detalle.nivelPlaca;
          }
          else if (detalle.seccion === 3){
            this.pieza_3 = detalle.pieza;
            this.nivel_calculo_3 = detalle.nivelCalculo;
            this.nivel_gingivitis_3 = detalle.nivelGingivitis;
            this.nivel_placa_3 = detalle.nivelPlaca;
          }
          else if (detalle.seccion === 4){
            this.pieza_4 = detalle.pieza;
            this.nivel_calculo_4 = detalle.nivelCalculo;
            this.nivel_gingivitis_4 = detalle.nivelGingivitis;
            this.nivel_placa_4 = detalle.nivelPlaca;
          }
          else if (detalle.seccion === 5){
            this.pieza_5 = detalle.pieza;
            this.nivel_calculo_5 = detalle.nivelCalculo;
            this.nivel_gingivitis_5 = detalle.nivelGingivitis;
            this.nivel_placa_5 = detalle.nivelPlaca;
          }
          else if (detalle.seccion === 6){
            this.pieza_6 = detalle.pieza;
            this.nivel_calculo_6 = detalle.nivelCalculo;
            this.nivel_gingivitis_6 = detalle.nivelGingivitis;
            this.nivel_placa_6 = detalle.nivelPlaca;
          }
        });
        this.calcularPromedios();
    });

  }

  guardarIndicadores(){
    const indicadores: IndicadorSaludBucal = new IndicadorSaludBucal({});
    indicadores.detallesHigieneOral = [];
    indicadores.nivelFluorosis = this.fluorosis;
    indicadores.nivelMalOclusion = this.mal_oclusion;
    indicadores.nivelEnfermedadPeriodontal  = this.enfermedad_periodontal;

    const detalle1: DetalleHigieneOral  = new DetalleHigieneOral();
    detalle1.seccion = 1;
    detalle1.pieza = this.pieza_1;
    detalle1.nivelCalculo = this.nivel_calculo_1;
    detalle1.nivelGingivitis = this.nivel_gingivitis_1;
    detalle1.nivelPlaca = this.nivel_placa_1;
    indicadores.detallesHigieneOral.push(detalle1);

    const detalle2: DetalleHigieneOral = new DetalleHigieneOral();
    detalle2.seccion = 2;
    detalle2.pieza = this.pieza_2;
    detalle2.nivelCalculo = this.nivel_calculo_2;
    detalle2.nivelGingivitis = this.nivel_gingivitis_2;
    detalle2.nivelPlaca = this.nivel_placa_2;
    indicadores.detallesHigieneOral.push(detalle2);

    const detalle3: DetalleHigieneOral = new DetalleHigieneOral();
    detalle3.seccion = 3;
    detalle3.pieza = this.pieza_3;
    detalle3.nivelCalculo = this.nivel_calculo_3;
    detalle3.nivelGingivitis = this.nivel_gingivitis_3;
    detalle3.nivelPlaca = this.nivel_placa_3;
    indicadores.detallesHigieneOral.push(detalle3);

    const detalle4: DetalleHigieneOral = new DetalleHigieneOral();
    detalle4.seccion = 4;
    detalle4.pieza = this.pieza_4;
    detalle4.nivelCalculo = this.nivel_calculo_4;
    detalle4.nivelGingivitis = this.nivel_gingivitis_4;
    detalle4.nivelPlaca = this.nivel_placa_4;
    indicadores.detallesHigieneOral.push(detalle4);

    const detalle5: DetalleHigieneOral = new DetalleHigieneOral();
    detalle5.seccion = 5;
    detalle5.pieza = this.pieza_5;
    detalle5.nivelCalculo = this.nivel_calculo_5;
    detalle5.nivelGingivitis = this.nivel_gingivitis_5;
    detalle5.nivelPlaca = this.nivel_placa_5;
    indicadores.detallesHigieneOral.push(detalle5);

    const detalle6: DetalleHigieneOral = new DetalleHigieneOral();
    detalle6.seccion = 6;
    detalle6.pieza = this.pieza_6;
    detalle6.nivelCalculo = this.nivel_calculo_6;
    detalle6.nivelGingivitis = this.nivel_gingivitis_6;
    detalle6.nivelPlaca = this.nivel_placa_6;
    indicadores.detallesHigieneOral.push(detalle6);

    console.log("--------INDICADORES DE SALUD BUCAL--------")
    console.log(indicadores)
    this.servicioIndicadoresSaludBucal.createIndicadoresSaludBucal(indicadores, this.pacienteActual.id)
      .subscribe(res => {
        console.log(res);
      });
  }

  calcularPromedios(){
    console.log('ASDASD: ' + this.nivel_placa_2);
    let suma_placa = 0;
    let conteo_placa = 0;

    if (this.nivel_placa_1 !== 0){suma_placa += this.nivel_placa_1; conteo_placa++; }
    if (this.nivel_placa_2 !== 0){suma_placa += this.nivel_placa_2; conteo_placa++; }
    if (this.nivel_placa_3 !== 0){suma_placa += this.nivel_placa_3; conteo_placa++; }
    if (this.nivel_placa_4 !== 0){suma_placa += this.nivel_placa_4; conteo_placa++; }
    if (this.nivel_placa_5 !== 0){suma_placa += this.nivel_placa_5; conteo_placa++; }
    if (this.nivel_placa_6 !== 0){suma_placa += this.nivel_placa_6; conteo_placa++; }


    this.promedio_placa = conteo_placa === 0 ? 0 : Math.round(suma_placa / conteo_placa);

    let suma_calculo = 0;
    let conteo_calculo = 0;

    if (this.nivel_calculo_1 !== 0){suma_calculo += this.nivel_calculo_1; conteo_calculo++; }
    if (this.nivel_calculo_2 !== 0){suma_calculo += this.nivel_calculo_2; conteo_calculo++; }
    if (this.nivel_calculo_3 !== 0){suma_calculo += this.nivel_calculo_3; conteo_calculo++; }
    if (this.nivel_calculo_4 !== 0){suma_calculo += this.nivel_calculo_4; conteo_calculo++; }
    if (this.nivel_calculo_5 !== 0){suma_calculo += this.nivel_calculo_5; conteo_calculo++; }
    if (this.nivel_calculo_6 !== 0){suma_calculo += this.nivel_calculo_6; conteo_calculo++; }

    this.promedio_calculo = conteo_calculo === 0 ? 0 : Math.round(suma_calculo / conteo_calculo);

    let suma_gingivitis = 0;
    let conteo_gingivitis = 0;

    if (this.nivel_gingivitis_1 !== 0){suma_gingivitis += this.nivel_gingivitis_1; conteo_gingivitis++; }
    if (this.nivel_gingivitis_2 !== 0){suma_gingivitis += this.nivel_gingivitis_2; conteo_gingivitis++; }
    if (this.nivel_gingivitis_3 !== 0){suma_gingivitis += this.nivel_gingivitis_3; conteo_gingivitis++; }
    if (this.nivel_gingivitis_4 !== 0){suma_gingivitis += this.nivel_gingivitis_4; conteo_gingivitis++; }
    if (this.nivel_gingivitis_5 !== 0){suma_gingivitis += this.nivel_gingivitis_5; conteo_gingivitis++; }
    if (this.nivel_gingivitis_6 !== 0){suma_gingivitis += this.nivel_gingivitis_6; conteo_gingivitis++; }

    this.promedio_gingivitis = conteo_gingivitis === 0 ? 0 : Math.round(suma_gingivitis / conteo_gingivitis);
  }
}
