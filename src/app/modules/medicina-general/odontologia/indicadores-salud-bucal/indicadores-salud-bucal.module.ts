import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IndicadoresSaludBucalRoutingModule } from './indicadores-salud-bucal-routing.module';
import { IndicadoresSaludBucalComponent } from './components/indicadores-salud-bucal/indicadores-salud-bucal.component';
import { IndicadoresSaludBucalService } from './services/indicadores-salud-bucal.service';
import { HttpClientModule } from "@angular/common/http";
import { MatIconModule } from "@angular/material/icon";
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';

import { FlexLayoutModule } from '@angular/flex-layout';
import { IconModule } from '@visurel/iconify-angular';
import { BreadcrumbsModule } from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import { PageLayoutModule } from 'src/@vex/components/page-layout/page-layout.module';
import { ContainerModule } from 'src/@vex/directives/container/container.module';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { EmptyPacienteModule } from 'src/app/core/components/empty-paciente/empty-paciente.module';
import {MatRadioModule} from '@angular/material/radio';

@NgModule({
  declarations: [
    IndicadoresSaludBucalComponent
  ],
  imports: [
    CommonModule,
    IndicadoresSaludBucalRoutingModule,
    HttpClientModule,
    MatIconModule,
    MatListModule,
    MatButtonModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    EmptyPacienteModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatMenuModule,
    MatDialogModule,
    IconModule,
    FormsModule,
    MatTooltipModule,
    ReactiveFormsModule,
    ContainerModule,
    MatSelectModule,
    MatButtonToggleModule,
    MatRadioModule
    // BrowserAnimationsModule
  ],
  providers: [ IndicadoresSaludBucalService ],
  exports: [
    IndicadoresSaludBucalComponent
  ]
})
export class IndicadoresSaludBucalModule { }
