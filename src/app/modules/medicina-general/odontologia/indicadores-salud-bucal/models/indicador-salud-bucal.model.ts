import { DetalleHigieneOral } from './detalle-higiene-oral.model';

export class IndicadorSaludBucal{
    public nivelEnfermedadPeriodontal: number;
    public nivelMalOclusion: number;
    public nivelFluorosis: number;
    public detallesHigieneOral: DetalleHigieneOral[];

    constructor(data){
        this.nivelEnfermedadPeriodontal = data.nivelEnfermedadPeriodontal;
        this.nivelMalOclusion = data.nivelMalOclusion;
        this.nivelFluorosis = data.nivelFluorosis;
        this.detallesHigieneOral = data.detallesHigieneOral;
    }
}
