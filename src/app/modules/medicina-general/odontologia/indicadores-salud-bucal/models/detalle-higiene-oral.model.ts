export class DetalleHigieneOral {
    public seccion: number;
    public pieza: number;
    public nivelPlaca: number;
    public nivelCalculo: number;
    public nivelGingivitis: number;
}
