import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { retry } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { IndicadorSaludBucal } from '../models/indicador-salud-bucal.model';

@Injectable({
  providedIn: 'root'
})
export class IndicadoresSaludBucalService {

  public url: string;
  public identity;
  public token;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Allow: 'GET, POST, HEAD'
    })
  };
  constructor(private http: HttpClient) {
    this.url = 'http://localhost:8080/api/v1.0/';
  }


  // GET
  getIndicadoresSaludBucal(idPaciente: number): Observable<IndicadorSaludBucal>  {
    return this.http.get<IndicadorSaludBucal>(this.url + "indicador-salud-bucal",{params:{
      idPaciente: idPaciente.toString(),
    }})
    .pipe(
      retry(1)
    );
  }

  // POST
  createIndicadoresSaludBucal(indicador: IndicadorSaludBucal, idPaciente): Observable<IndicadorSaludBucal>  {
    return this.http.post<IndicadorSaludBucal>(
      this.url + 'indicador-salud-bucal', JSON.stringify(indicador), {
        params:{
          idPaciente: idPaciente.toString(),
        },
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Accept: 'application/json',
          Allow: 'GET, POST, HEAD'
        })
      })
    .pipe(
      retry(1)
    );
  }
}
