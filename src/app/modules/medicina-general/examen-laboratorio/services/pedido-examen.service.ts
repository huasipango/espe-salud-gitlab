import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {Observable} from 'rxjs';
import {PedidoExamen} from '../models/pedido-examen.model';
import {ExamenLaboratorio} from 'src/app/modules/medicina-general/examen-laboratorio/models/examen-laboratorio.model';

@Injectable({
  providedIn: 'root'
})
export class PedidoExamenService {
  private baseUrl: string = environment.baseUrl;
  private readonly commonUrl: string;

  constructor(private http: HttpClient) {
    this.commonUrl = this.baseUrl + 'pedidos-examen';
  }

  getPedidosExamen(idPaciente: number): Observable<PedidoExamen[]> {
    return this.http.get<PedidoExamen[]>(this.commonUrl + '?idPaciente=' + idPaciente);
  }

  getPedidoExamen(idPedido: number): Observable<PedidoExamen> {
    return this.http.get<PedidoExamen>(this.commonUrl + '/' + idPedido);
  }

  createPedidoExamen(data: PedidoExamen): Observable<PedidoExamen> {
    return this.http.post<PedidoExamen>(this.commonUrl + '/', JSON.stringify(data));
  }

  reportResults(idPedido: number, data: ExamenLaboratorio[]): Observable<PedidoExamen> {
    return this.http.post<PedidoExamen>(this.commonUrl + '/' + idPedido, JSON.stringify(data));
  }

  deletePedidoExamen(id: number): Observable<boolean> {
    const url = `${this.commonUrl}/${id}`;
    return this.http.delete<boolean>(url);
  }

  updatePedidoExamen(id: number, data: PedidoExamen): Observable<PedidoExamen> {
    const url = `${this.commonUrl}/${id}`;
    return this.http.put<PedidoExamen>(url, JSON.stringify(data));
  }
}
