import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {Observable} from 'rxjs';
import {ExamenLaboratorio} from '../models/examen-laboratorio.model';

@Injectable({
  providedIn: 'root'
})
export class ExamenLaboratorioService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getExamenesLaboratorio(idPedidoExamen: number): Observable<ExamenLaboratorio[]> {
    const url = `${this.baseUrl}pedidos-examen/${idPedidoExamen}/examenes-laboratorio`;
    return this.http.get<ExamenLaboratorio[]>(url);
  }

  getExamenLaboratorio(idPedidoExamen: number, id: number): Observable<ExamenLaboratorio> {
    const url = `${this.baseUrl}pedidos-examen/${idPedidoExamen}/examenes-laboratorio/${id}`;
    return this.http.get<ExamenLaboratorio>(url);
  }

  createExamenLaboratorio(idPedidoExamen: number, data: ExamenLaboratorio): Observable<ExamenLaboratorio> {
    const url = `${this.baseUrl}pedidos-examen/${idPedidoExamen}/examenes-laboratorio/`;
    return this.http.post<ExamenLaboratorio>(url, JSON.stringify(data));
  }

  updateExamenLaboratorio(id: number,
                          data: ExamenLaboratorio,
                          idPedidoExamen: number): Observable<ExamenLaboratorio> {
    const url = `${this.baseUrl}pedidos-examen/${idPedidoExamen}/examenes-laboratorio/${id}`;
    return this.http.put<ExamenLaboratorio>(url, JSON.stringify(data));
  }

  deleteExamenLaboratorio(id: number, idPedidoExamen: number): Observable<ExamenLaboratorio> {
    const url = `${this.baseUrl}pedidos-examen/${idPedidoExamen}/examenes-laboratorio/${id}`;
    return this.http.delete<ExamenLaboratorio>(url);
  }
}
