import {Component, OnDestroy, OnInit} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {PedidoExamen} from '../../models/pedido-examen.model';
import {Observable} from 'rxjs';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icFolder from '@iconify/icons-ic/twotone-folder';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icPrint from '@iconify/icons-ic/print';
import {FormControl} from '@angular/forms';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {PedidoExamenService} from '../../services/pedido-examen.service';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {DateUtil} from 'src/app/core/utils/date-utils';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {trackById} from 'src/@vex/utils/track-by';
import {LoadingService} from 'src/app/core/services/loading.service';
import {PedidoExamenModalComponent} from 'src/app/modules/medicina-general/examen-laboratorio/components/pedido-examen-modal/pedido-examen-modal.component';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {ImagenUsuarioService} from 'src/app/core/services/imagen-usuario.service';
import {ColorUtil} from 'src/app/core/utils/color-utils';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import icMicroscope from '@iconify/icons-fa-solid/microscope';


@UntilDestroy()
@Component({
  selector: 'vex-pedido-examen',
  templateUrl: './pedido-examen.component.html',
  styleUrls: ['./pedido-examen.component.scss'],
  animations: [fadeInUp400ms, stagger40ms, scaleFadeIn400ms, fadeInRight400ms, scaleIn400ms],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})

export class PedidoExamenComponent implements OnInit, OnDestroy {

  pedidosExamen$: Observable<PedidoExamen[]>;
  searchCtrl = new FormControl();
  trackById = trackById;

  icEdit = icEdit;
  icSearch = icSearch;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icMoreHoriz = icMoreHoriz;
  icFolder = icFolder;
  icPrint = icPrint;
  icMicroscope = icMicroscope;
  pacienteActual: Paciente;
  userMessages = USER_MESSAGES;
  labelClassInfo = ColorUtil.labStateColors();
  colorClassInfo = ColorUtil.labelColors();

  constructor(
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
    private pedidoExamenService: PedidoExamenService,
    private pacienteGlobalService: PacienteGlobalService,
    private loadingService: LoadingService,
    private imagenUsuarioService: ImagenUsuarioService
  ) {
  }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getPedidosExamen();
        }
      });
    this.searchCtrl.valueChanges.pipe(
      untilDestroyed(this)
    ).subscribe(value => this.onFilterChange(value));
  }

  getPedidosExamen(): void {
    this.pedidosExamen$ = this.loadingService.showLoaderUntilCompleted(
      this.pedidoExamenService.getPedidosExamen(this.pacienteActual.id)
    );
  }

  getFormattedDate(date: string) {
    return DateUtil.showDateFormat(date);
  }

  createPedidoExamen() {
    this.dialog.open(PedidoExamenModalComponent, {
      width: '650px',
      disableClose: true,
      maxWidth: '100%'
    }).afterClosed()
      .subscribe((pedido: PedidoExamen) => {
        if (pedido) {
          this.loadingService.showLoaderUntilCompleted(
            this.pedidoExamenService.createPedidoExamen(pedido)
          ).subscribe(response => {
            if (response) {
              this.getPedidosExamen();
              this.showNotification(this.userMessages.createdSuccessMessage);
            }
          }, error => {
            this.showNotification(this.userMessages.createdFailedMessage);
          });
        }
      });
  }

  deletePedidoExamen(pedido: PedidoExamen) {
    this.pedidoExamenService.deletePedidoExamen(pedido.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification(this.userMessages.deletedSuccessMessage);
          this.getPedidosExamen();
        } else {
          this.showNotification(this.userMessages.deleteFailMessage);
        }
      });
  }

  startDeletePedidoExamen(pedido: PedidoExamen) {
    this.dialog.open(DeleteModalComponent, {
      data: this.userMessages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') {
        this.deletePedidoExamen(pedido);
      }
    });
  }

  updatePedidoExamen(pedidoExamen: PedidoExamen) {
    const id = pedidoExamen.id;
    this.router.navigate(['./', id], {relativeTo: this.route});
  }

  openDeleteDialog(pedidoExamen: PedidoExamen) {
    this.dialog.open(DeleteModalComponent, {
      data: this.userMessages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') {
        this.deletePedidoExamen(pedidoExamen);
      }
    });
  }

  onFilterChange(value: string) {
    // if (!this.dataSource) {
    //   return;
    // }
    // value = value.trim();
    // value = value.toLowerCase();
    // this.dataSource.filter = value;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  showNotification(message: string) {
    this.snackbar.open(message, 'CERRAR', {
      duration: 5000
    });
  }

  ngOnDestroy() {
  }

  getUserImage(id: string) {
    return this.imagenUsuarioService.getUserImage(id);
  }

  getLabStateColor(state: string) {
    const foundState = this.labelClassInfo.find(s => s.state === state);
    if (foundState) {
      return [foundState.color.bgClass, foundState.color.textClass];
    }
    return [];
  }
}
