import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PedidoExamenComponent } from './pedido-examen.component';


const routes: Routes = [
  {
    path: '',
    component: PedidoExamenComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PedidoExamenRoutingModule { }
