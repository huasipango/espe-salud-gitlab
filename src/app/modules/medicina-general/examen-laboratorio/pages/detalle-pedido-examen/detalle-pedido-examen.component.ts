import { Component, OnInit } from '@angular/core';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {Link} from 'src/@vex/interfaces/link.interface';
import {ActivatedRoute, Router} from '@angular/router';
import icAdd from '@iconify/icons-ic/twotone-add';
import icBack from '@iconify/icons-ic/arrow-back';
import icCloudDownload from '@iconify/icons-ic/twotone-cloud-download';

@Component({
  selector: 'vex-detalle-pedido-examen',
  templateUrl: './detalle-pedido-examen.component.html',
  styleUrls: ['./detalle-pedido-examen.component.scss'],
  animations: [
    scaleIn400ms,
    fadeInRight400ms,
    stagger40ms,
    fadeInUp400ms,
    scaleFadeIn400ms
  ]
})
export class DetallePedidoExamenComponent implements OnInit {
  icAdd = icAdd;
  icBack = icBack;
  icCloudDownload = icCloudDownload;

  pedidoExamenId;
  links: Link[] = [
    {
      label: 'PEDIDO EXAMEN',
      route: './',
      routerLinkActiveOptions: {exact: true}
    },
    {
      label: 'EXAMENES REALIZADOS',
      route: './examenes-laboratorio',
    }
  ];

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(( params) => {
      const id = params.get('id');
      this.pedidoExamenId = id;
    });
  }

  createNuevoPedido() {
    // this.router.navigate(['../create'], {relativeTo: this.route});
  }

  navigateBack() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }
}
