import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetallePedidoExamenRoutingModule } from './detalle-pedido-examen-routing.module';
import { DetallePedidoExamenComponent } from './detalle-pedido-examen.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {MatButtonModule} from '@angular/material/button';
import {IconModule} from '@visurel/iconify-angular';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatIconModule} from '@angular/material/icon';


@NgModule({
  declarations: [DetallePedidoExamenComponent],
  imports: [
    CommonModule,
    DetallePedidoExamenRoutingModule,
    FlexLayoutModule,
    ContainerModule,
    MatButtonModule,
    IconModule,
    MatTabsModule,
    MatTooltipModule,
    MatIconModule
  ]
})
export class DetallePedidoExamenModule { }
