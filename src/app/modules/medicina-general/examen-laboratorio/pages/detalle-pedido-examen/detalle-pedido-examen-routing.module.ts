import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {VexRoutes} from 'src/@vex/interfaces/vex-route.interface';
import {DetallePedidoExamenComponent} from '../detalle-pedido-examen/detalle-pedido-examen.component';

const routes: VexRoutes = [
  {
    path: '',
    component: DetallePedidoExamenComponent,
    data: {
      toolbarShadowEnabled: false,
      scrollDisabled: true
    },
    children: [
      {
        path: '',
        loadChildren: () => import('../informacion-pedido-examen/informacion-pedido-examen.module')
          .then(m => m.InformacionPedidoExamenModule)
      },
      {
        path: 'examenes-laboratorio',
        loadChildren: () => import('../examenes-laboratorio/examenes-laboratorio.module')
          .then(m => m.ExamenesLaboratorioModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetallePedidoExamenRoutingModule {
}
