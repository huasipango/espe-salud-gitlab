import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {InformacionPedidoExamenComponent} from 'src/app/modules/medicina-general/examen-laboratorio/pages/informacion-pedido-examen/informacion-pedido-examen.component';

const routes: Routes = [
  {
    path: '',
    component: InformacionPedidoExamenComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InformacionPedidoExamenRoutingModule { }
