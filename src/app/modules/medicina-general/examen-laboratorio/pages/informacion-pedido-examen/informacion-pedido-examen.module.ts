import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InformacionPedidoExamenRoutingModule } from './informacion-pedido-examen-routing.module';
import { InformacionPedidoExamenComponent } from './informacion-pedido-examen.component';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {IconModule} from '@visurel/iconify-angular';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatMomentDateModule} from '@angular/material-moment-adapter';


@NgModule({
  declarations: [InformacionPedidoExamenComponent],
  imports: [
    CommonModule,
    InformacionPedidoExamenRoutingModule,
    ContainerModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    IconModule,
    MatButtonModule,
    MatIconModule,
    LoadingModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatTooltipModule,
    MatMomentDateModule
  ]
})
export class InformacionPedidoExamenModule { }
