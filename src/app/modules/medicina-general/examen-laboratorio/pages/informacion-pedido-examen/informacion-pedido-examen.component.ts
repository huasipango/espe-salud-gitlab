import { Component, OnInit } from '@angular/core';
import {switchMap} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PedidoExamenService} from 'src/app/modules/medicina-general/examen-laboratorio/services/pedido-examen.service';
import {PedidoExamen} from 'src/app/modules/medicina-general/examen-laboratorio/models/pedido-examen.model';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {Observable} from 'rxjs';
import {TipoExamenLaboratorio} from 'src/app/core/models/catalogo/tipo-examen-laboratorio.model';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import icMicroscope from '@iconify/icons-fa-solid/microscope';
import icSave from '@iconify/icons-fa-solid/save';

import {SaludUserService} from 'src/app/core/services/salud-user.service';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {enumSelector} from 'src/app/core/utils/enum-to-string-util';
import {EstadoPedidoExamenEnum} from 'src/app/core/enums/estado-pedido-examen.enum';
import {OrigenPedidoExamenEnum} from 'src/app/core/enums/origen-pedido-examen.enum';
import {LoadingService} from 'src/app/core/services/loading.service';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';

@Component({
  selector: 'vex-informacion-pedido-examen',
  templateUrl: './informacion-pedido-examen.component.html',
  styleUrls: ['./informacion-pedido-examen.component.scss'],
  animations: [
    scaleIn400ms,
    fadeInRight400ms
  ]
})
export class InformacionPedidoExamenComponent implements OnInit {
  pedidoExamen: PedidoExamen;
  pedidoForm: FormGroup;
  maxDate = new Date();
  errorMessages = FORM_ERROR_MESSAGES;
  tipoExamenesLaboratorio: Observable<TipoExamenLaboratorio[]>;
  usuarios: Observable<SaludUser[]>;
  icMicroscope = icMicroscope;
  icSave = icSave;
  estados = enumSelector(EstadoPedidoExamenEnum);
  origenes = enumSelector(OrigenPedidoExamenEnum);
  userMessages = USER_MESSAGES;

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    private pedidoExamenService: PedidoExamenService,
    private snackbar: MatSnackBar,
    private catalogoService: CatalogoService,
    private usuarioSaludService: SaludUserService,
    private loadingService: LoadingService
  ) { }

  ngOnInit(): void {
    this.usuarios = this.usuarioSaludService.getAllUsers();
    this.tipoExamenesLaboratorio = this.catalogoService.getTiposExamenLaboratorio();
    this.route.paramMap.pipe(
      switchMap(params => {
        const id = params.get('id');
        return this.pedidoExamenService.getPedidoExamen(Number(id));
      })
    ).subscribe((pedido) => {
      if (pedido) {
        this.pedidoExamen = pedido;
        this.cargarFormulario();
      }
    });
  }

  cargarFormulario(): void {
    this.pedidoForm = this.fb.group({
      id: this.pedidoExamen.id,
      fecha: [{value: this.pedidoExamen.fecha, disabled: false}, Validators.required],
      responsablePidm: [{value: this.pedidoExamen.responsablePidm, disabled: true}, Validators.required],
      origen: [{value: this.pedidoExamen.origen, disabled: true}, Validators.required],
      estado: [{value: this.pedidoExamen.estado, disabled: true}, Validators.required],
      idPaciente: this.pedidoExamen.idPaciente,
    });
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000,
    });
  }

  update() {
    const pedidoExamen: PedidoExamen = this.pedidoForm.getRawValue();
    this.loadingService.showLoaderUntilCompleted(
      this.pedidoExamenService.updatePedidoExamen(pedidoExamen.id, pedidoExamen)
    ).subscribe((response) => {
      if (response) {
        this.pedidoExamen = response;
        this.cargarFormulario();
        this.showNotification(this.userMessages.updatedSuccessMessage, 'CERRAR');
      }
    }, error => {
      this.showNotification(this.userMessages.updatedFailedMessage, 'CERRAR');
    });
  }
}
