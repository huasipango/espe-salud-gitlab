import { Component, OnInit } from '@angular/core';
import {SelectionModel} from '@angular/cdk/collections';
import {ExamenLaboratorio} from 'src/app/modules/medicina-general/examen-laboratorio/models/examen-laboratorio.model';
import {Observable} from 'rxjs';
import {ExamenLaboratorioService} from 'src/app/modules/medicina-general/examen-laboratorio/services/examen-laboratorio.service';
import {switchMap} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {trackById} from 'src/@vex/utils/track-by';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {LayoutService} from 'src/@vex/services/layout.service';
import icSettings from '@iconify/icons-ic/twotone-settings';

@Component({
  selector: 'vex-examenes-laboratorio',
  templateUrl: './examenes-laboratorio.component.html',
  styleUrls: ['./examenes-laboratorio.component.scss'],
  animations: [
    stagger40ms,
    fadeInUp400ms
  ]
})
export class ExamenesLaboratorioComponent implements OnInit {
  selection = new SelectionModel<ExamenLaboratorio['id']>(true, []);
  examenesLaboratorio$: Observable<ExamenLaboratorio[]>;
  pedidoExamenId: number;
  trackById = trackById;
  isDesktop$ = this.layoutService.isDesktop$;
  gtSm$ = this.layoutService.gtSm$;
  icSettings = icSettings;

  constructor(
    private route: ActivatedRoute,
    private layoutService: LayoutService,
    private examenLaboratorioService: ExamenLaboratorioService
  ) { }

  ngOnInit(): void {
    this.examenesLaboratorio$ = this.route.paramMap.pipe(
      switchMap(params => {
        const id = params.get('id');
        this.pedidoExamenId = Number(id);
        return this.examenLaboratorioService.getExamenesLaboratorio(this.pedidoExamenId);
      })
    );
  }

}
