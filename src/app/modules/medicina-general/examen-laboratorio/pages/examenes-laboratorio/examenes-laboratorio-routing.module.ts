import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {ExamenesLaboratorioComponent} from 'src/app/modules/medicina-general/examen-laboratorio/pages/examenes-laboratorio/examenes-laboratorio.component';
import {ExamenViewComponentComponent} from 'src/app/modules/medicina-general/examen-laboratorio/components/examen-view-component/examen-view-component.component';
import {ExamenViewEmptyComponentComponent} from 'src/app/modules/medicina-general/examen-laboratorio/components/examen-view-empty-component/examen-view-empty-component.component';
import {VexRoutes} from 'src/@vex/interfaces/vex-route.interface';

const routes: VexRoutes = [
  {
    path: '',
    component: ExamenesLaboratorioComponent,
    children: [
      {
        path: '',
        component: ExamenViewEmptyComponentComponent
      },
      {
        path: ':examenId',
        component: ExamenViewComponentComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExamenesLaboratorioRoutingModule { }
