import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExamenesLaboratorioRoutingModule } from './examenes-laboratorio-routing.module';
import { ExamenesLaboratorioComponent } from './examenes-laboratorio.component';
import {ScrollbarModule} from 'src/@vex/components/scrollbar/scrollbar.module';
import {ExamenListEntryModule} from 'src/app/modules/medicina-general/examen-laboratorio/components/examen-list-entry/examen-list-entry.module';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {IconModule} from '@visurel/iconify-angular';
import {MatTooltipModule} from '@angular/material/tooltip';


@NgModule({
  declarations: [ExamenesLaboratorioComponent],
  imports: [
    CommonModule,
    ExamenesLaboratorioRoutingModule,
    ScrollbarModule,
    ExamenListEntryModule,
    MatIconModule,
    MatButtonModule,
    IconModule,
    MatTooltipModule
  ]
})
export class ExamenesLaboratorioModule { }
