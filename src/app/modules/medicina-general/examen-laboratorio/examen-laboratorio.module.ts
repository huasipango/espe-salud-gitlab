import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ExamenLaboratorioRoutingModule} from './examen-laboratorio-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ExamenLaboratorioRoutingModule
  ]
})
export class ExamenLaboratorioModule {
}
