import {ExamenLaboratorio} from './examen-laboratorio.model';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';

export class PedidoExamen {
  id: number;
  fecha: string;
  examenes: ExamenLaboratorio[];
  idPaciente: number;
  responsablePidm: number;
  estado = 'SOLICITADO' || 'FINALIZADO' || 'REPORTADO';
  origen = 'LABORATORIO' || 'CONSULTA';
  usuario: SaludUser;
}
