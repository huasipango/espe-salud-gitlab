import { DetalleExamenLaboratorio } from './detalle-examen-laboratorio.model';
import {TipoExamenLaboratorio} from 'src/app/core/models/catalogo/tipo-examen-laboratorio.model';

export class ExamenLaboratorio {
  id: number;
  completado: boolean;
  examen: TipoExamenLaboratorio;
  detalles: DetalleExamenLaboratorio[];
}
