import {TipoDetalleExamenLaboratorio} from 'src/app/core/models/catalogo/tipo-detalle-examen-laboratorio.model';

export class DetalleExamenLaboratorio {
  id: number;
  detalle: TipoDetalleExamenLaboratorio;
  valor: string;
  observacion: string;
}
