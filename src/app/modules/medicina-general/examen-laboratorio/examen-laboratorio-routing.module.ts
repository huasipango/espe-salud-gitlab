import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {VexRoutes} from 'src/@vex/interfaces/vex-route.interface';
import {RolEnum} from 'src/app/core/enums/rol.enum';
import {RoleGuard} from 'src/app/core/auth/role.guard';

const routes: VexRoutes = [
  {
    path: '',
    canActivate: [RoleGuard],
    data: {
      roles: [RolEnum.ROLE_LABORATORISTA]
    },
    children: [
      {
        path: '',
        loadChildren: () => import('./pages/pedido-examen/pedido-examen.module').then(m => m.PedidoExamenModule)
      },
      {
        path: ':id',
        loadChildren: () => import('./pages/detalle-pedido-examen/detalle-pedido-examen.module').then(m => m.DetallePedidoExamenModule)
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExamenLaboratorioRoutingModule {
}
