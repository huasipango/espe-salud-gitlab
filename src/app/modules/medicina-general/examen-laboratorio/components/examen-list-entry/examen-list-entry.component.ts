import {Component, Input, OnInit, Output, EventEmitter, ChangeDetectorRef} from '@angular/core';
import {ExamenLaboratorio} from 'src/app/modules/medicina-general/examen-laboratorio/models/examen-laboratorio.model';
import {MatCheckboxChange} from '@angular/material/checkbox';

@Component({
  selector: 'vex-examen-list-entry',
  templateUrl: './examen-list-entry.component.html',
  styleUrls: ['./examen-list-entry.component.scss']
})
export class ExamenListEntryComponent implements OnInit {
  @Input() examen: ExamenLaboratorio;
  @Input() selected: boolean;
  @Output() selectedChange = new EventEmitter<boolean>();
  hovered: boolean;

  constructor(
    private cd: ChangeDetectorRef,
  ) { }

  ngOnInit(): void {
  }

  onMouseEnter() {
    this.hovered = true;
    this.cd.markForCheck();
  }

  onMouseLeave() {
    this.hovered = false;
    this.cd.markForCheck();
  }

  onCheckboxChange(event: MatCheckboxChange) {
    this.selectedChange.emit(event.checked);
  }
}
