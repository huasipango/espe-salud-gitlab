import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExamenListEntryComponent } from './examen-list-entry.component';
import {RouterModule} from '@angular/router';
import {MatCheckboxModule} from '@angular/material/checkbox';



@NgModule({
  declarations: [ExamenListEntryComponent],
  exports: [
    ExamenListEntryComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatCheckboxModule
  ]
})
export class ExamenListEntryModule { }
