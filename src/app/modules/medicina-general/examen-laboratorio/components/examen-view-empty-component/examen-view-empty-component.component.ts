import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'vex-examen-view-empty-component',
  templateUrl: './examen-view-empty-component.component.html',
  styleUrls: ['./examen-view-empty-component.component.scss']
})
export class ExamenViewEmptyComponentComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
