import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExamenViewEmptyComponentComponent } from './examen-view-empty-component.component';



@NgModule({
  declarations: [ExamenViewEmptyComponentComponent],
  imports: [
    CommonModule
  ]
})
export class ExamenViewEmptyComponentModule { }
