import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PedidoExamenModalComponent } from './pedido-examen-modal.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSelectModule} from '@angular/material/select';
import {MatMomentDateModule} from '@angular/material-moment-adapter';



@NgModule({
  declarations: [PedidoExamenModalComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatDialogModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    IconModule,
    MatInputModule,
    MatDatepickerModule,
    MatSelectModule,
    MatMomentDateModule
  ]
})
export class PedidoExamenModalModule { }
