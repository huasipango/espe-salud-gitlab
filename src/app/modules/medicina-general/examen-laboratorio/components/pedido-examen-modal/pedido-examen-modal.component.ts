import {Component, Inject, OnInit} from '@angular/core';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {TipoExamenLaboratorio} from 'src/app/core/models/catalogo/tipo-examen-laboratorio.model';
import {PedidoExamen} from 'src/app/modules/medicina-general/examen-laboratorio/models/pedido-examen.model';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import icMicroscope from '@iconify/icons-fa-solid/microscope';
import icClose from '@iconify/icons-ic/twotone-close';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {ExamenLaboratorio} from 'src/app/modules/medicina-general/examen-laboratorio/models/examen-laboratorio.model';
import {AuthService} from 'src/app/core/auth/auth.service';
import {enumSelector} from 'src/app/core/utils/enum-to-string-util';
import {LateralidadEnum} from 'src/app/core/enums/lateralidad.enum';
import {EstadoPedidoExamenEnum} from 'src/app/core/enums/estado-pedido-examen.enum';
import {OrigenPedidoExamenEnum} from 'src/app/core/enums/origen-pedido-examen.enum';

@Component({
  selector: 'vex-pedido-examen-modal',
  templateUrl: './pedido-examen-modal.component.html',
  styleUrls: ['./pedido-examen-modal.component.scss']
})
export class PedidoExamenModalComponent implements OnInit {
  form: FormGroup;
  unidadExamen = '';
  pacienteActual: Paciente;
  mode: 'create' | 'update' = 'create';

  tipoExamenesLaboratorio: Observable<TipoExamenLaboratorio[]>;
  maxDate = new Date();
  icClose = icClose;
  icMicroscope = icMicroscope;
  icArrowDropDown = icArrowDropDown;
  errorMessages = FORM_ERROR_MESSAGES;
  estados = enumSelector(EstadoPedidoExamenEnum);
  origenes = enumSelector(OrigenPedidoExamenEnum);

  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: PedidoExamen,
    private catalogoService: CatalogoService,
    private pacienteGlobalService: PacienteGlobalService,
    private dialogRef: MatDialogRef<PedidoExamenModalComponent>,
    private fb: FormBuilder,
    private authService: AuthService,
  ) {
  }

  ngOnInit(): void {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as PedidoExamen;
    }
    this.tipoExamenesLaboratorio = this.catalogoService.getTiposExamenLaboratorio();
    this.form = this.fb.group({
      id: this.defaults.id || null,
      fecha: [this.defaults.fecha || this.maxDate, Validators.required],
      examenes: [this.defaults.examenes || null, Validators.min(1)],
      origen: [{value: this.defaults.origen || this.origenes[0].title, disabled: true}, Validators.required],
      responsablePidm: [this.defaults.responsablePidm || null, Validators.required],
      idPaciente: [this.defaults.idPaciente || null, Validators.required]
    });
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.form.patchValue({idPaciente: paciente.id});
        }
      });
    this.authService.saludUserData$
      .subscribe((saludUser) => {
        if (saludUser) {
          this.form.patchValue({responsablePidm: saludUser.pidm});
        }
      });
  }

  save() {
    const pedidoExamen = this.form.getRawValue();
    const detalles: TipoExamenLaboratorio[] = pedidoExamen.examenes;
    const examenes: ExamenLaboratorio[] = [];
    for (const detalle of detalles) {
      const examen = new ExamenLaboratorio();
      examen.examen = detalle;
      examenes.push(examen);
    }
    pedidoExamen.examenes = examenes;
    this.dialogRef.close(pedidoExamen);
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }
}
