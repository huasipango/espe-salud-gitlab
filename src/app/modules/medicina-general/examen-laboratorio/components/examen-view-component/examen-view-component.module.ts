import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExamenViewComponentComponent } from './examen-view-component.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatInputModule} from '@angular/material/input';
import {ScrollbarModule} from 'src/@vex/components/scrollbar/scrollbar.module';
import {RouterModule} from '@angular/router';
import {LabelModule} from 'src/app/shared/components/label/label.module';



@NgModule({
  declarations: [ExamenViewComponentComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatIconModule,
    IconModule,
    MatDialogModule,
    MatButtonModule,
    FlexLayoutModule,
    MatInputModule,
    ScrollbarModule,
    RouterModule,
    LabelModule
  ]
})
export class ExamenViewComponentModule { }
