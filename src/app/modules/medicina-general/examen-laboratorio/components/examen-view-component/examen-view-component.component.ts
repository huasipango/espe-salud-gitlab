import { Component, OnInit } from '@angular/core';
import {switchMap} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {ExamenLaboratorioService} from 'src/app/modules/medicina-general/examen-laboratorio/services/examen-laboratorio.service';
import {ExamenLaboratorio} from 'src/app/modules/medicina-general/examen-laboratorio/models/examen-laboratorio.model';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {LayoutService} from 'src/@vex/services/layout.service';
import icArrowBack from '@iconify/icons-ic/twotone-arrow-back';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';

@Component({
  selector: 'vex-examen-view-component',
  templateUrl: './examen-view-component.component.html',
  styleUrls: ['./examen-view-component.component.scss'],
  animations: [
    fadeInUp400ms
  ]
})
export class ExamenViewComponentComponent implements OnInit {
  examenLaboratorio: ExamenLaboratorio;
  examenLaboratorioId: number;
  pedidoExamenId: number;
  form: FormGroup;
  gtSm$ = this.layoutService.gtSm$;
  icArrowBack = icArrowBack;

  constructor(
    private route: ActivatedRoute,
    private layoutService: LayoutService,
    private examenLaboratorioService: ExamenLaboratorioService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap(params => {
        const idExamen = params.get('examenId');
        const idPedido = params.get('id');
        this.examenLaboratorioId = Number(idExamen);
        this.pedidoExamenId = Number(idPedido);
        return this.examenLaboratorioService.getExamenLaboratorio(
          this.pedidoExamenId,
          this.examenLaboratorioId
        );
      })
    ).subscribe((response) => {
      if (response) {
        this.examenLaboratorio = response;
        this.initForm();
      }
    });
  }

  initForm(): void {
    this.form = this.fb.group({
      id: this.examenLaboratorio.id,
      examen: this.examenLaboratorio.examen,
      detalles: this.fb.array(this.examenLaboratorio.detalles.map(detalle => this.fb.group({
        detalle: [detalle.detalle],
        valor: [detalle.valor],
        observacion: [detalle.observacion]
      }))),
    });
  }

  get detallesArray(){
    return (this.form.get('detalles') as FormArray).controls;
  }

  save() {
  }
}
