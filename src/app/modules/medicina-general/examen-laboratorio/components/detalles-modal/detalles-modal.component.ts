import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Paciente } from 'src/app/core/models/paciente/paciente.model';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import icClose from '@iconify/icons-ic/twotone-close';
import { PacienteGlobalService } from 'src/app/core/services/paciente-global.service';
import { ExamenLaboratorio } from '../../models/examen-laboratorio.model';
import { DateUtil } from 'src/app/core/utils/date-utils';

@Component({
  selector: 'vex-detalles-modal',
  templateUrl: './detalles-modal.component.html',
  styleUrls: ['./detalles-modal.component.scss']
})
export class DetallesModalComponent implements OnInit {

  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  pacienteActual: Paciente;
  maxDate = new Date();

  icClose = icClose;

  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: any,
    private pacienteGlobalService: PacienteGlobalService,
    private dialogRef: MatDialogRef<DetallesModalComponent>,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    console.log(this.defaults.detalles);
    
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as ExamenLaboratorio;
    }
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente)=>{
        this.pacienteActual = paciente;
      });
    this.form = this.fb.group({
      id: this.defaults.id,
      fecha: ({value: this.defaults.fecha, disabled: true}),
      nombre: ({value: this.defaults.nombre, disabled: true}),
      detalles: this.fb.array(this.defaults.detalles.map(detalle => this.fb.group({
        nombreParametro: [detalle.nombreParametro],
        unidadParametro: [detalle.unidadParametro],
        valorParametro: [detalle.valorParametro],
        observacion: [detalle.observacion],
        examenLaboratorio: this.defaults.id
      }))),
      idPaciente: this.pacienteActual.id || null
    });
  }

  get detallesArray(){
    return (<FormArray>this.form.get('detalles')).controls;
  }

  getControlLabel(type: string){
    return this.form.controls[type].value;
  }

  getArrayGroupControlByIndex(index) {
    return (<FormArray>this.form.get('detalles'))
      .at(index);
  }
  
  save() {
    var examenLaboratorio = this.form.value;
    console.log(examenLaboratorio.id);
    
    this.dialogRef.close(examenLaboratorio);
    console.log(examenLaboratorio);
    
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  getFormattedDate(date: string){
    return DateUtil.showDateFormat(date);
  }
}
