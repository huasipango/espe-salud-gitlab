import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {VexRoutes} from 'src/@vex/interfaces/vex-route.interface';
import {PerfilPacienteComponent} from './perfil-paciente.component';


const routes: VexRoutes = [
  {
    path: '',
    component: PerfilPacienteComponent,
    data: {
      toolbarShadowEnabled: true,
      containerEnabled: true
    },
    children: [
      {
        path: '',
        loadChildren: () => import('./pages/perfil-informacion-personal/perfil-informacion-personal.module')
          .then(m => m.PerfilInformacionPersonalModule)
      },
      {
        path: 'estudiante',
        loadChildren: () => import('./pages/perfil-estudiante/perfil-estudiante.module')
          .then(m => m.PerfilEstudianteModule)
      },
      {
        path: 'empleado',
        loadChildren: () => import('./pages/perfil-empleado/perfil-empleado.module')
          .then(m => m.PerfilEmpleadoModule)
      }
    ],
  },
  {
    path: 'actualizarPacienteExterno',
    loadChildren: () => import('./pages/actualizar-paciente-externo/actualizar-paciente.module')
      .then(m => m.ActualizarPacienteModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PerfilPacienteRoutingModule {
}
