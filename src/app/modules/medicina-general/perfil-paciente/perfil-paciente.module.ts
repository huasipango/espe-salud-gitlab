import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PerfilPacienteRoutingModule} from './perfil-paciente-routing.module';
import {PerfilPacienteComponent} from './perfil-paciente.component';
import {PageLayoutModule} from 'src/@vex/components/page-layout/page-layout.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatTabsModule} from '@angular/material/tabs';
import {IconModule} from '@visurel/iconify-angular';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatRippleModule} from '@angular/material/core';
import {EmptyPacienteModule} from 'src/app/core/components/empty-paciente/empty-paciente.module';

@NgModule({
  declarations: [PerfilPacienteComponent],
  exports: [
  ],
  imports: [
    CommonModule,
    PerfilPacienteRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    MatTabsModule,
    IconModule,
    MatButtonModule,
    MatIconModule,
    MatRippleModule,
    EmptyPacienteModule
  ]
})
export class PerfilPacienteModule {
}
