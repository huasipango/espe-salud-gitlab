import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActualizarPacienteComponent } from './actualizar-paciente.component';
import {ActualizarPacienteRotingModule} from './actualizar-paciente-roting.module';
import {SecondaryToolbarModule} from '../../../../../../@vex/components/secondary-toolbar/secondary-toolbar.module';
import {BreadcrumbsModule} from '../../../../../../@vex/components/breadcrumbs/breadcrumbs.module';
import {FlexModule} from '@angular/flex-layout';
import {IconModule} from '@visurel/iconify-angular';
import {MatStepperModule} from '@angular/material/stepper';
import {MatIconModule} from '@angular/material/icon';
import {ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {UppercaseModule} from '../../../../../shared/directives/uppercase/uppercase.module';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatButtonModule} from '@angular/material/button';
import {ContainerModule} from '../../../../../../@vex/directives/container/container.module';
import {ContactosEmergenciaListModule} from '../../../registro-paciente/components/contactos-emergencia-list/contactos-emergencia-list.module';
import {EmptyPacienteModule} from '../../../../../core/components/empty-paciente/empty-paciente.module';
import {LoadingModule} from '../../../../../shared/components/loading/loading.module';
import {OnlyNumbersModule} from 'src/app/shared/directives/only-numbers/only-numbers.module';
import {MatMomentDateModule} from '@angular/material-moment-adapter';



@NgModule({
  declarations: [ActualizarPacienteComponent],
  imports: [
    CommonModule,
    ActualizarPacienteRotingModule,
    SecondaryToolbarModule,
    BreadcrumbsModule,
    FlexModule,
    IconModule,
    MatStepperModule,
    MatIconModule,
    ReactiveFormsModule,
    MatInputModule,
    UppercaseModule,
    MatSelectModule,
    MatDatepickerModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatButtonModule,
    ContainerModule,
    ContactosEmergenciaListModule,
    EmptyPacienteModule,
    LoadingModule,
    OnlyNumbersModule,
    MatMomentDateModule
  ]
})
export class ActualizarPacienteModule { }
