import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {SimpleCatalogUtil} from 'src/app/core/utils/simple-catalog-util';
import icDoneAll from '@iconify/icons-ic/twotone-done-all';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';
import icClose from '@iconify/icons-ic/twotone-close';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icAdd from '@iconify/icons-ic/twotone-add';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icDescription from '@iconify/icons-ic/twotone-description';
import icBack from '@iconify/icons-ic/arrow-back';

import {stagger80ms} from 'src/@vex/animations/stagger.animation';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {Observable} from 'rxjs';
import {ContactoEmergencia} from 'src/app/core/models/paciente/contacto-emergencia.model';
import {PacienteService} from 'src/app/core/services/paciente/paciente.service';
import {LoadingService} from 'src/app/core/services/loading.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {enumSelector} from 'src/app/core/utils/enum-to-string-util';
import {SexoEnum} from 'src/app/core/enums/sexo.enum';
import {LateralidadEnum} from 'src/app/core/enums/lateralidad.enum';
import {NivelInstruccion} from 'src/app/core/models/catalogo/nivel-instruccion.model';
import {EstadoCivil} from 'src/app/core/models/catalogo-banner/estado-civil.model';
import {Religion} from 'src/app/core/models/catalogo-banner/religion.model';
import {SeguroSalud} from 'src/app/core/models/catalogo/seguro-salud.model';
import {Asociacion} from 'src/app/core/models/catalogo/asociacion.model';
import {RelacionEspe} from 'src/app/core/models/catalogo/relacion-espe.model';
import {Pais} from 'src/app/core/models/catalogo-banner/pais.model';
import {Provincia} from 'src/app/core/models/catalogo-banner/provincia,model';
import {Canton} from 'src/app/core/models/catalogo-banner/canton.model';
import {Nacionalidad} from 'src/app/core/models/catalogo-banner/nacionalidad.model';
import {Etnia} from 'src/app/core/models/catalogo-banner/etnia.model';
import {Dispensario} from 'src/app/core/models/catalogo/dispensario.model';
import {CatalogoBannerService} from 'src/app/core/services/catalogo-banner.service';
import {Plurinacionalidad} from 'src/app/core/models/catalogo/plurinacionalidad.model';
import {TipoSangre} from 'src/app/core/models/catalogo-banner/tipo-sangre.model';
import {Parroquia} from 'src/app/core/models/catalogo-banner/parroquia.model';
import {PacienteExterno} from 'src/app/core/models/paciente/paciente-externo.model';
import {TipoIdentificacion} from 'src/app/core/models/catalogo/tipo-identificacion.model';
import {UniqueNumeroArchivoValidator} from 'src/app/shared/directives/unique-cedula/unique-cedula.directive';
import {
  ECUADOR_COUNTRY_NAME,
  ECUADOR_NACIONALIDAD_NAME,
  FORM_ERROR_MESSAGES,
  MAX_AGE_TO_CI_REPRESENANTE
} from 'src/app/core/constants/constants';
import {calculateAge, getFullAge} from 'src/app/core/utils/operations.util';
import {ParroquiaSalud} from 'src/app/core/models/catalogo/parroquia-salud.model';
import {Pueblo} from 'src/app/core/models/catalogo/pueblo.model';

@Component({
  selector: 'vex-actualizar-paciente',
  templateUrl: './actualizar-paciente.component.html',
  styleUrls: ['./actualizar-paciente.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    stagger80ms,
    fadeInUp400ms,
    scaleIn400ms,
    fadeInRight400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class ActualizarPacienteComponent implements OnInit {

  paciente: Paciente;
  idPaciente: number;
  contactosEmergencia: ContactoEmergencia[];

  sexoList = enumSelector(SexoEnum);
  lateralidades = enumSelector(LateralidadEnum);
  estadosCiviles: Observable<EstadoCivil[]>;
  gruposSanguineos: Observable<TipoSangre[]>;
  religiones: Observable<Religion[]>;
  segurosSalud: Observable<SeguroSalud[]>;
  asosiaciones: Observable<Asociacion[]>;
  nivelesInstrucccion: Observable<NivelInstruccion[]>;
  relacionesEspe: Observable<RelacionEspe[]>;
  phonePrefixOptions = SimpleCatalogUtil.codigosRegion;
  today = new Date();

  paises: Observable<Pais[]>;
  provincias: Provincia[] = [];
  cantones: Canton[] = [];
  cantonesContacto: Canton[];
  parroquias: ParroquiaSalud[];
  nacionalidades: Observable<Nacionalidad[]>;
  gruposCulturales: Observable<Etnia[]>;
  nacionalidadesPueblos: Observable<Plurinacionalidad[]>;
  pueblos: Observable<Pueblo[]>;
  zonas = SimpleCatalogUtil.zonas;
  dispensarios: Dispensario[] = [];
  dispensarioSeleccionado: Dispensario;

  icDoneAll = icDoneAll;
  icDescription = icDescription;
  icMoreVert = icMoreVert;
  icArrowDropDown = icArrowDropDown;
  icClose = icClose;
  icDelete = icDelete;
  icAdd = icAdd;
  icMoreHoriz = icMoreHoriz;
  icBack = icBack;
  tiposIdentificacion: TipoIdentificacion[];
  informacionPersonalFormGroup: FormGroup;
  informacionContactoFormGroup: FormGroup;
  informacionAdicionalFormGroup: FormGroup;
  numeroIdentifiacionIsRequired = false;
  numeroIdentificacionRepresentanteIsRequired = false;
  errorMessages = FORM_ERROR_MESSAGES;
  fullAge = '';
  cantonNacimientoIsRequired = true;
  provinciaNacimientoIsRequired = true;

  constructor(
    private fb: FormBuilder,
    private pacienteGlobalService: PacienteGlobalService,
    private catalogoService: CatalogoService,
    private pacienteService: PacienteService,
    private loadingService: LoadingService,
    private snackbar: MatSnackBar,
    private router: Router,
    private numeroArchivoValidator: UniqueNumeroArchivoValidator,
    private route: ActivatedRoute,
    private catalogoBannerService: CatalogoBannerService
  ) {
  }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.paciente = paciente;
          this.contactosEmergencia = this.paciente.contactosEmergencia;
          this.idPaciente = this.paciente.id;
        }
      });
    this.estadosCiviles = this.catalogoBannerService.getEstadosCiviles();
    this.religiones = this.catalogoBannerService.getReligiones();
    this.segurosSalud = this.catalogoService.getSegurosSalud();
    this.asosiaciones = this.catalogoService.getAsociaciones();
    this.nivelesInstrucccion = this.catalogoService.getNivelesInstruccion();
    this.paises = this.catalogoBannerService.getPaises();
    this.nacionalidades = this.catalogoBannerService.getNacionalidades();
    this.gruposCulturales = this.catalogoService.getEtnias();
    this.relacionesEspe = this.catalogoService.getRelacionesEspe();
    this.gruposSanguineos = this.catalogoBannerService.getTiposSangre();
    this.catalogoBannerService.getProvincias()
      .subscribe((data) => {
        this.provincias = data;
      });
    this.catalogoBannerService.getCantonesByProvincia(this.paciente.idProvinciaNacimiento)
      .subscribe((data) => {
        this.cantones = data;
      });
    this.catalogoBannerService.getCantonesByProvincia(this.paciente.contacto.idProvinciaResidencia)
      .subscribe((data) => {
        this.cantonesContacto = data;
      });
    this.catalogoService.getParroquiasByCanton(this.paciente.contacto.idCantonResidencia)
      .subscribe((data) => {
        this.parroquias = data;
      });
    this.cargarInformacionPersonalFormGroup();
    this.cargarInformacionContactoFormGroup();
    this.cargarInformacionAdicionalFormGroup();
    this.catalogoService.getTiposIdentificacion()
      .subscribe((data) => this.tiposIdentificacion = data);
    this.catalogoService.getDispensarios()
      .subscribe(
        (data) => {
          this.dispensarios = data;
        }
      );
    this.catalogoService.getDispensarios()
      .subscribe(
        (data) => {
          this.dispensarios = data;
          this.cargarReadOnlyInputs(this.paciente.idDispensario);
        }
      );
    this.validatePais(this.paciente.paisNacimiento);
  }

  cargarInformacionPersonalFormGroup() {
    this.informacionPersonalFormGroup = this.fb.group({
      id: this.paciente.id,
      primerNombre: [this.paciente.primerNombre, Validators.required],
      segundoNombre: [this.paciente.segundoNombre, Validators.required],
      apellidoPaterno: [this.paciente.apellidoPaterno, Validators.required],
      apellidoMaterno: [this.paciente.apellidoMaterno, Validators.required],
      numeroIdentificacion: [this.paciente.numeroIdentificacion, Validators.required],
      fechaNacimiento: [this.paciente.fechaNacimiento, Validators.required],
      sexo: [this.paciente.sexo, Validators.required],
      estadoCivil: [this.paciente.estadoCivil, Validators.required],
      grupoSanguineo: [this.paciente.grupoSanguineo, Validators.required],
      idEtnia: [this.paciente.idEtnia, Validators.required],
      paisNacimiento: [this.paciente.paisNacimiento, Validators.required],
      provinciaNacimiento: [this.paciente.idProvinciaNacimiento, Validators.required],
      cantonNacimiento: [this.paciente.idCantonNacimiento, Validators.required],
      nacionalidad: [this.paciente.nacionalidad || ''],
      nacionalidad2: [this.paciente.nacionalidad2 || ''],
      religion: [this.paciente.religion, Validators.required],
      aceptaTransfucion: [this.paciente.aceptaTransfucion],
      idPueblo: [this.paciente.idPueblo],
      idNacionalidadEtnica: [this.paciente.idNacionalidadEtnica],
      lateralidad: [this.paciente.lateralidad, Validators.required],
      idVinculadoEspe: [this.paciente.idVinculadoEspe, Validators.required],
      profesion: [this.paciente.profesion],
      idInstruccion: [this.paciente.idInstruccion, Validators.required],
      idSeguroSalud: [this.paciente.idSeguroSalud, Validators.required],
      idAsociacionAfiliada: [this.paciente.idAsociacionAfiliada, Validators.required],
      idTipoIdentificacion: [this.paciente.idTipoIdentificacion, Validators.required],
      consentimientoInformadoAceptado: [this.paciente.consentimientoInformadoAceptado]
    });
    this.onChanges();
    if (this.paciente.idNacionalidadEtnica) {
      this.searchNacionalidades(this.paciente.idEtnia);
      this.informacionPersonalFormGroup.patchValue({idNacionalidadEtnica: this.paciente.idNacionalidadEtnica});
    }
    if (this.paciente.idPueblo) {
      this.seachPueblos(this.paciente.idNacionalidadEtnica);
      this.informacionPersonalFormGroup.patchValue({idPueblo: this.paciente.idPueblo});
    }
  }

  onChanges(): void {
    this.informacionPersonalFormGroup.get('fechaNacimiento')
      .valueChanges
      .subscribe(val => {
        if (val) {
          this.setIdentificacionRepresentanteRequired(val);
        }
      });
  }

  setIdentificacionRepresentanteRequired(fechaNacimiento: any) {
    if (!this.informacionAdicionalFormGroup) { return ; }
    this.fullAge = getFullAge(fechaNacimiento);
    if (calculateAge(fechaNacimiento) <= MAX_AGE_TO_CI_REPRESENANTE) {
      this.numeroIdentificacionRepresentanteIsRequired = true;
      this.informacionAdicionalFormGroup.get('identificacionRepresentante').setValidators(
        [Validators.required]
      );
      this.informacionAdicionalFormGroup.get('identificacionRepresentante').updateValueAndValidity();
    } else {
      this.numeroIdentificacionRepresentanteIsRequired = false;
      this.informacionAdicionalFormGroup.get('identificacionRepresentante').clearValidators();
      this.informacionAdicionalFormGroup.get('identificacionRepresentante').updateValueAndValidity();
    }
  }

  cargarInformacionContactoFormGroup() {
    this.informacionContactoFormGroup = this.fb.group({
      callePrincipal: [this.paciente.contacto.callePrincipal, Validators.required],
      calleSecundaria: [this.paciente.contacto.calleSecundaria || ''],
      direccionReferencia: [this.paciente.contacto.direccionReferencia || ''],
      parroquiaResidencia: [this.paciente.contacto.idParroquiaResidencia || ''],
      barrio: [this.paciente.contacto.barrio || ''],
      zonaGeografica: [this.paciente.contacto.zonaGeografica, Validators.required],
      numeroCelular: [this.paciente.contacto.numeroCelular, Validators.required],
      numeroConvencional: [this.paciente.contacto.numeroConvencional || ''],
      extension: [this.paciente.contacto.extension || this.phonePrefixOptions[0]],
      correoPersonal: [this.paciente.contacto.correoPersonal, [Validators.email]],
      correoInstitucional: [this.paciente.contacto.correoInstitucional, Validators.email],
      provinciaResidencia: [this.paciente.contacto.idProvinciaResidencia, Validators.required],
      cantonResidencia: [this.paciente.contacto.idCantonResidencia, Validators.required],
      contactosEmergencia: [this.paciente.contactosEmergencia]
    });
  }

  cargarInformacionAdicionalFormGroup() {
    this.informacionAdicionalFormGroup = this.fb.group({
      identificacionRepresentante: [this.paciente.identificacionRepresentante],
      idDispensario: [this.paciente.idDispensario, Validators.required]
    });
    this.setIdentificacionRepresentanteRequired(this.paciente.fechaNacimiento);
  }

  actualizarPaciente(): void {
    const paciente: PacienteExterno = this.informacionPersonalFormGroup.getRawValue();
    paciente.contacto = this.informacionContactoFormGroup.value;
    paciente.contactosEmergencia = this.paciente.contactosEmergencia;
    paciente.idDispensario = this.informacionAdicionalFormGroup.get('idDispensario').value;
    paciente.identificacionRepresentante = this.informacionAdicionalFormGroup.get('identificacionRepresentante').value;
    paciente.idProvinciaNacimiento = this.informacionPersonalFormGroup.get('provinciaNacimiento')?.value;
    paciente.provinciaNacimiento = this.provincias.find(p => p.idProvincia === paciente.idProvinciaNacimiento)?.descripcion;
    paciente.idCantonNacimiento = this.informacionPersonalFormGroup.get('cantonNacimiento')?.value;
    paciente.cantonNacimiento = this.cantones.find(c => c.idCanton === paciente.idCantonNacimiento)?.descripcion;

    paciente.contacto.idProvinciaResidencia = this.informacionContactoFormGroup.get('provinciaResidencia').value;
    paciente.contacto.provinciaResidencia = this.provincias.find(
      p => p.idProvincia === paciente.contacto.idProvinciaResidencia)?.descripcion;
    paciente.contacto.idCantonResidencia = this.informacionContactoFormGroup.get('cantonResidencia').value;
    paciente.contacto.cantonResidencia = this.cantonesContacto.find(c => c.idCanton === paciente.contacto.idCantonResidencia)?.descripcion;
    paciente.contacto.idParroquiaResidencia = this.informacionContactoFormGroup.get('parroquiaResidencia').value;
    paciente.contacto.parroquiaResidencia = this.parroquias.find(p => p.id === paciente.contacto.idParroquiaResidencia)?.nombre;

    this.loadingService.showLoaderUntilCompleted(
      this.pacienteService.updatePaciente(paciente, this.idPaciente)
    ).subscribe((response: Paciente) => {
      if (response) {
        this.pacienteGlobalService.setPacienteGlobal(response);
        this.showNotification('Paciente actualizado EXITOSAMENTE', 'CERRAR');
        this.router.navigate(['../'], {relativeTo: this.route});
      }
    });
  }

  cargarReadOnlyInputs(idDispensario) {
    this.dispensarioSeleccionado = this.dispensarios.find(x => x.id === idDispensario);
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  searchCantones(event: any) {
    const idProvincia: number = event.value;
    this.catalogoBannerService.getCantonesByProvincia(idProvincia)
      .subscribe((data) => {
        this.cantones = data;
      });
  }

  searchCantonesContacto(event: any) {
    const idProvincia: number = event.value;
    this.catalogoBannerService.getCantonesByProvincia(idProvincia)
      .subscribe((data) => {
        this.cantonesContacto = data;
      });
  }

  searchParroquias(event: any) {
    const idCanton: string = event.value;
    this.catalogoService.getParroquiasByCanton(idCanton)
      .subscribe((data) => {
        this.parroquias = data;
      });
  }

  navigateBack() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  setRequiredParamToNumeroIdentificacion(idTipoIdentificacion: number) {
    const tipoIdentificacion = this.tiposIdentificacion.find(x => x.id === idTipoIdentificacion);
    this.numeroIdentifiacionIsRequired = tipoIdentificacion ? tipoIdentificacion.esRequerido : false;
    if (tipoIdentificacion && tipoIdentificacion.esRequerido) {
      this.informacionPersonalFormGroup.get('numeroIdentificacion').setValidators(
        [Validators.required]
      );
      this.informacionPersonalFormGroup.get('numeroIdentificacion').updateValueAndValidity();
    } else {
      this.informacionPersonalFormGroup.get('numeroIdentificacion').clearValidators();
      this.informacionPersonalFormGroup.get('numeroIdentificacion').updateValueAndValidity();
    }
  }

  searchNacionalidades(idEtnia: number) {
    this.informacionPersonalFormGroup.get('idNacionalidadEtnica').reset();
    this.nacionalidadesPueblos = this.catalogoService.getNacionalidadesByEtnia(idEtnia);
  }

  validatePais(nombrePais: string) {
    if (nombrePais === ECUADOR_COUNTRY_NAME) {
      this.informacionPersonalFormGroup.get('provinciaNacimiento').setValidators([Validators.required]);
      this.informacionPersonalFormGroup.get('provinciaNacimiento').updateValueAndValidity();
      this.provinciaNacimientoIsRequired = true;

      this.informacionPersonalFormGroup.get('cantonNacimiento').setValidators([Validators.required]);
      this.informacionPersonalFormGroup.get('cantonNacimiento').updateValueAndValidity();
      this.cantonNacimientoIsRequired = true;

      this.informacionPersonalFormGroup.patchValue({nacionalidad: ECUADOR_NACIONALIDAD_NAME});
      this.informacionPersonalFormGroup.get('nacionalidad').disable({ onlySelf: true });
    } else {
      this.informacionPersonalFormGroup.get('provinciaNacimiento').reset();
      this.informacionPersonalFormGroup.get('provinciaNacimiento').clearValidators();
      this.informacionPersonalFormGroup.get('provinciaNacimiento').updateValueAndValidity();
      this.provinciaNacimientoIsRequired = false;

      this.informacionPersonalFormGroup.get('cantonNacimiento').reset();
      this.informacionPersonalFormGroup.get('cantonNacimiento').clearValidators();
      this.informacionPersonalFormGroup.get('cantonNacimiento').updateValueAndValidity();
      this.cantonNacimientoIsRequired = false;

      this.informacionPersonalFormGroup.get('nacionalidad').reset();
      this.informacionPersonalFormGroup.get('nacionalidad').enable({onlySelf: true });
    }
  }

  seachPueblos(idNacionalidad: number) {
    this.informacionPersonalFormGroup.get('idPueblo').reset();
    this.pueblos = this.catalogoService.getPueblosByNacionalidad(idNacionalidad);
  }

}
