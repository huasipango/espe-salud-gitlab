import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ActualizarPacienteComponent} from './actualizar-paciente.component';

const routes: Routes = [
  {
    path: '',
    component: ActualizarPacienteComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActualizarPacienteRotingModule {
}
