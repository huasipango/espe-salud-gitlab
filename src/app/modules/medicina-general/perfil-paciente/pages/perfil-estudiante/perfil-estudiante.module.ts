import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfilEstudianteComponent } from './perfil-estudiante.component';
import {PerfilEstudianteRoutingModule} from './perfil-estudiante-routing.module';
import {MatButtonModule} from '@angular/material/button';
import {MatStepperModule} from '@angular/material/stepper';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {FlexLayoutModule, FlexModule} from '@angular/flex-layout';
import {IconModule} from '@visurel/iconify-angular';
import {InfoEstudianteComponent} from '../../components/info-estudiante/info-estudiante.component';
import {MatRippleModule} from '@angular/material/core';
import {MatIconModule} from '@angular/material/icon';

@NgModule({
  declarations: [PerfilEstudianteComponent, InfoEstudianteComponent],
  imports: [
    CommonModule,
    PerfilEstudianteRoutingModule,
    MatButtonModule,
    MatStepperModule,
    MatProgressSpinnerModule,
    FlexModule,
    IconModule,
    FlexLayoutModule,
    MatRippleModule,
    MatIconModule
  ]
})
export class PerfilEstudianteModule { }
