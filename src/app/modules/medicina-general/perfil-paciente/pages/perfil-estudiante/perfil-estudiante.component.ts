import {Component, OnInit} from '@angular/core';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import icUserCheck from '@iconify/icons-fa-solid/user-check';
import {PacienteService} from 'src/app/core/services/paciente/paciente.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {filter} from 'rxjs/operators';
import {EstudianteModalComponent} from '../../components/estudiante-modal/estudiante-modal.component';
import {MatDialog} from '@angular/material/dialog';
import {EstudianteService} from 'src/app/core/services/paciente/estudiante.service';
import {Estudiante} from 'src/app/core/models/estudiante/estudiante.model';

@Component({
  selector: 'vex-perfil-estudiante',
  templateUrl: './perfil-estudiante.component.html',
  styleUrls: ['./perfil-estudiante.component.scss'],
  animations: [
    scaleFadeIn400ms
  ]
})
export class PerfilEstudianteComponent implements OnInit {

  paciente: Paciente;
  icUserCheck = icUserCheck;

  constructor(
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private pacienteGlobalService: PacienteGlobalService,
    private pacienteService: PacienteService,
    private estudianteService: EstudianteService
  ) {
  }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.paciente = paciente;
        }
      });
  }

  registrarEstudiante(): void {
    this.dialog.open(EstudianteModalComponent, {
      width: '900px',
      maxWidth: '100%',
      disableClose: true
    }).beforeClosed().pipe(
      filter<Estudiante>(Boolean)
    ).subscribe(value => {
      this.estudianteService
        .createEstudiante(value, this.paciente.id)
        .subscribe((estudiante) => {
          if (estudiante) {
            this.paciente.estudiante = estudiante;
            this.pacienteGlobalService.setPacienteGlobal(this.paciente);
            this.showNotification('Registro creado EXITOSAMENTE', 'OK');
          }
        }, error => {
          this.showNotification('ERROR al crear el registro', 'CERRAR');
        });
    });
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }
}
