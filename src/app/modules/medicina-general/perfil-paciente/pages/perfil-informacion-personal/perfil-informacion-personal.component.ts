import {Component, OnInit} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import icEdit from '@iconify/icons-fa-solid/pen';
import icActivate from '@iconify/icons-ic/check-box';
import icRevoke from '@iconify/icons-ic/cancel';
import {ActivatedRoute, Router} from '@angular/router';
import {TipoPaciente} from 'src/app/core/enums/tipo-paciente.enum';
import {FormularioPacienteInternoComponent} from 'src/app/modules/medicina-general/registro-paciente/components/formulario-paciente-interno/formulario-paciente-interno.component';
import {PacienteInternoForm} from 'src/app/modules/medicina-general/registro-paciente/pages/paciente-interno-form/paciente-interno-form.component';
import {PersonaBanner} from 'src/app/core/models/banner/persona-banner-model';
import {MatDialog} from '@angular/material/dialog';
import {LoadingService} from 'src/app/core/services/loading.service';
import {BannerService} from 'src/app/core/services/banner.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ConsentimientoModalComponent} from 'src/app/modules/medicina-general/registro-paciente/components/consentimiento-modal/consentimiento-modal.component';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {PacienteService} from 'src/app/core/services/paciente/paciente.service';
import {AuthService} from 'src/app/core/auth/auth.service';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';


@Component({
  selector: 'vex-perfil-informacion-personal',
  templateUrl: './perfil-informacion-personal.component.html',
  styleUrls: ['./perfil-informacion-personal.component.scss'],
  animations: [
    fadeInUp400ms,
    fadeInRight400ms,
    scaleIn400ms,
    stagger40ms,
    scaleFadeIn400ms
  ]
})
export class PerfilInformacionPersonalComponent implements OnInit {

  paciente: Paciente;
  esEmpleado: boolean;
  esEstudiante: boolean;
  tipoPaciente = TipoPaciente;
  icEdit = icEdit;
  icActivate = icActivate;
  icRevoke = icRevoke;
  personaBanner = {} as PersonaBanner;
  userMessages = USER_MESSAGES;
  user: SaludUser;

  constructor(
    private pacienteGlobalService: PacienteGlobalService,
    private router: Router,
    private route: ActivatedRoute,
    private loadingService: LoadingService,
    private bannerService: BannerService,
    private matSnackBar: MatSnackBar,
    private dialog: MatDialog,
    private pacienteService: PacienteService,
    private authService: AuthService,
    private snackbar: MatSnackBar
  ) {
  }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente){
          this.paciente = paciente;
          this.infoEstudianteEmpleado();
          this.personaBanner.cedula = this.paciente.numeroIdentificacion;
          this.personaBanner.id_banner = this.paciente.idBanner;
          this.personaBanner.nombres = this.paciente.primerNombre + ' ' + this.paciente.segundoNombre;
          this.personaBanner.apellidos = this.paciente.apellidoPaterno + ' ' + this.paciente.apellidoMaterno;
          this.personaBanner.pidm = this.paciente.pidm;
        }
      });
    this.authService.saludUserData$.subscribe(value => this.user = value);
  }

  infoEstudianteEmpleado() {
    if (this.paciente.estudiante === null) {
      this.esEmpleado = true;
    } else {
      this.esEstudiante = true;
    }
  }

  actualizarPacienteExterno(): void{
    this.router.navigate(['actualizarPacienteExterno'], {relativeTo: this.route});
  }

  actualizarPacienteInterno(): void{
    const formData: PacienteInternoForm = {
      personaBanner: this.personaBanner,
      paciente: this.paciente,
      consentimientoInformadoAceptado: this.paciente.consentimientoInformadoAceptado
    };
    this.dialog.open(FormularioPacienteInternoComponent, {
      data: formData,
      width: '700px',
      maxWidth: '100%',
      disableClose: true
    }).afterClosed();
  }

  aceptarConsentimientoInformado(): void {
    this.dialog.open(DeleteModalComponent, {
      data: this.userMessages.acceptConInfAfterRegistered,
      width: '600px',
      maxWidth: '100%',
      disableClose: true
    }).afterClosed()
      .subscribe(value => {
        if (value) {
          if (value === 'si') {
            this.loadingService.showLoaderUntilCompleted(
              this.pacienteService.acceptConsentimientoInformado(
                this.paciente.id,
                this.user.pidm
              )
            ).subscribe(() => {
              this.paciente.consentimientoInformadoAceptado = true;
              this.pacienteGlobalService.setPacienteGlobal(this.paciente);
              this.showMessage('El paciente ha aceptado el consentimiento informado');
            }, error => {
              this.showMessage('No se pudo aceptar el consentimiento informado del paciente');
            })
          }
        }
      })
  }

  revokeConsentimientoInformado(): void {
    this.dialog.open(DeleteModalComponent, {
      data: this.userMessages.revokeConInf,
      width: '600px',
      maxWidth: '100%',
      disableClose: true
    }).afterClosed()
      .subscribe(value => {
        if (value) {
          if (value === 'si') {
            this.loadingService.showLoaderUntilCompleted(
              this.pacienteService.revokeConsentimientoInformado(
                this.paciente.id,
                this.user.pidm
              )
            ).subscribe(() => {
              this.paciente.consentimientoInformadoAceptado = false;
              this.pacienteGlobalService.setPacienteGlobal(this.paciente);
              this.showMessage('Se ha revocado el consentimiento inforado del paciente');
            }, error => {
              this.showMessage('No se pudo revocar el consentimiento informado del paciente');
            })
          }
        }
      })
  }

  showMessage(message: string) {
    this.snackbar.open(message, 'CERRAR', {
      duration: 5000,
      horizontalPosition: 'center'
    });
  }
}
