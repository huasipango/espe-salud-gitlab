import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PerfilInformacionPersonalComponent} from './perfil-informacion-personal.component';


const routes: Routes = [
  {
    path: '',
    component: PerfilInformacionPersonalComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PerfilInformacionPersonalRoutingModule {
}
