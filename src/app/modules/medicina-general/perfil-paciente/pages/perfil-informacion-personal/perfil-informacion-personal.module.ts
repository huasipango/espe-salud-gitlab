import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfilInformacionPersonalComponent } from './perfil-informacion-personal.component';
import {PerfilInformacionPersonalRoutingModule} from './perfil-informacion-personal-routing.module';
import {IconModule} from '@visurel/iconify-angular';
import {MatIconModule} from '@angular/material/icon';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {InfoPersonalComponent} from '../../components/info-personal/info-personal.component';
import { InfoContactoComponent } from '../../components/info-contacto/info-contacto.component';
import { InfoContactoEmergenciaComponent } from '../../components/info-contacto-emergencia/info-contacto-emergencia.component';
import {MatRippleModule} from '@angular/material/core';
import {InfoGeneralComponent} from 'src/app/modules/medicina-general/perfil-paciente/components/info-general/info-general.component';
import {MatSelectModule} from '@angular/material/select';
import {InfoGruposPrioritariosComponent} from 'src/app/modules/medicina-general/perfil-paciente/components/info-grupos-prioritarios/info-grupos-prioritarios.component';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';

@NgModule({
  declarations: [
    PerfilInformacionPersonalComponent,
    InfoPersonalComponent,
    InfoContactoComponent,
    InfoGeneralComponent,
    InfoContactoEmergenciaComponent,
    InfoGruposPrioritariosComponent
  ],
  imports: [
    CommonModule,
    PerfilInformacionPersonalRoutingModule,
    MatIconModule,
    IconModule,
    FlexLayoutModule,
    MatButtonModule,
    MatRippleModule,
    MatSelectModule,
    LoadingModule
  ]
})
export class PerfilInformacionPersonalModule { }
