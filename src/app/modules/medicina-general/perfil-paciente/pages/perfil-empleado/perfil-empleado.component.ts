import { Component, OnInit } from '@angular/core';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import icUserCheck from '@iconify/icons-fa-solid/user-check';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {PacienteService} from 'src/app/core/services/paciente/paciente.service';
import {filter} from 'rxjs/operators';
import {EmpleadoModalComponent} from 'src/app/modules/medicina-general/perfil-paciente/components/empleado-modal/empleado-modal.component';
import {Empleado} from 'src/app/core/models/empleado/empleado.model';
import {EmpleadoService} from 'src/app/core/services/paciente/empleado.service';

@Component({
  selector: 'vex-perfil-empleado',
  templateUrl: './perfil-empleado.component.html',
  styleUrls: ['./perfil-empleado.component.scss'],
  animations: [
    scaleFadeIn400ms
  ]
})
export class PerfilEmpleadoComponent implements OnInit {

  paciente: Paciente;
  icUserCheck = icUserCheck;

  constructor(
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private pacienteGlobalService: PacienteGlobalService,
    private pacienteService: PacienteService,
    private empleadoService: EmpleadoService
  ) { }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.paciente = paciente;
        }
      });
  }

  registrarEmpleado(): void {
    this.dialog.open(EmpleadoModalComponent, {
      width: '1200px',
      maxWidth: '100%',
      disableClose: true
    }).beforeClosed().pipe(
      filter<Empleado>(Boolean)
    ).subscribe(value => {
      this.empleadoService
        .createEmpleado(value, this.paciente.id)
        .subscribe((empleado) => {
          if (empleado) {
            this.paciente.empleado = empleado;
            this.pacienteGlobalService.setPacienteGlobal(this.paciente);
            this.showNotification('Registro creado EXITOSAMENTE', 'OK');
          }
        }, error => {
          this.showNotification('ERROR al crear el registro', 'CERRAR');
        });
    });
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }
}
