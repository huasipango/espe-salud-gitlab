import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfilEmpleadoComponent } from './perfil-empleado.component';
import {PerfilEmpleadoRoutingModule} from './perfil-empleado-routing.module';
import {IconModule} from '@visurel/iconify-angular';
import {FlexLayoutModule, FlexModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {InfoEmpleadoComponent} from 'src/app/modules/medicina-general/perfil-paciente/components/info-empleado/info-empleado.component';
import {MatRippleModule} from '@angular/material/core';
import {MatIconModule} from '@angular/material/icon';


@NgModule({
  declarations: [PerfilEmpleadoComponent, InfoEmpleadoComponent],
  imports: [
    CommonModule,
    PerfilEmpleadoRoutingModule,
    IconModule,
    FlexModule,
    MatButtonModule,
    MatRippleModule,
    MatIconModule,
    FlexLayoutModule,
  ]
})
export class PerfilEmpleadoModule { }
