import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PerfilEmpleadoComponent} from './perfil-empleado.component';

const routes: Routes = [
  {
    path: '',
    component: PerfilEmpleadoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PerfilEmpleadoRoutingModule {
}
