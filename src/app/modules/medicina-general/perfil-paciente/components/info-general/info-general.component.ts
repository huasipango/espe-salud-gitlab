import { Component, OnInit } from '@angular/core';
import icConsultorio from '@iconify/icons-fa-solid/clinic-medical';
import icActivo from '@iconify/icons-fa-solid/user-check';
import icFileMedical from '@iconify/icons-fa-solid/file-medical';

import {Dispensario} from 'src/app/core/models/catalogo/dispensario.model';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {TipoPaciente} from 'src/app/core/enums/tipo-paciente.enum';

@Component({
  selector: 'vex-info-general',
  templateUrl: './info-general.component.html',
  styleUrls: ['./info-general.component.scss'],
  animations: [
    fadeInRight400ms,
    stagger40ms,
    scaleFadeIn400ms,
    scaleIn400ms
  ]
})
export class InfoGeneralComponent implements OnInit {
  icConsultorio = icConsultorio;
  icActivo = icActivo;
  icFileMedical = icFileMedical;
  dispensario: Dispensario;
  paciente: Paciente;
  tipoPaciente = TipoPaciente;

  constructor(
    private catalogoService: CatalogoService,
    private pacienteGlobalService: PacienteGlobalService
  ) { }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        this.paciente = paciente;
        this.getDispensario();
      });
  }

  getDispensario() {
    this.catalogoService.getDispensario(this.paciente.idDispensario).subscribe((data: Dispensario) => {
      this.dispensario = data;
    });
  }
}
