import {Component, OnInit} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';

import icEdit from '@iconify/icons-fa-solid/edit';
import icAdd from '@iconify/icons-ic/twotone-add';

import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {ContactoEmergencia} from 'src/app/core/models/paciente/contacto-emergencia.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {MatDialog} from '@angular/material/dialog';
import {ContactoEmergenciaModalComponent} from '../../../registro-paciente/components/contactos-emergencia-list/contacto-emergencia-modal/contacto-emergencia-modal.component';
import {filter} from 'rxjs/operators';
import {ContactoEmergenciaService} from 'src/app/core/services/paciente/contacto-emergencia.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {TipoPaciente} from 'src/app/core/enums/tipo-paciente.enum';

@Component({
  selector: 'vex-info-contacto-emergencia',
  templateUrl: './info-contacto-emergencia.component.html',
  styleUrls: ['./info-contacto-emergencia.component.scss'],
  animations: [
    fadeInUp400ms,
    fadeInRight400ms,
    scaleIn400ms,
    stagger40ms,
    scaleFadeIn400ms
  ]
})
export class InfoContactoEmergenciaComponent implements OnInit {
  paciente: Paciente;

  icEdit = icEdit;
  icAdd = icAdd;
  tipoPaciente = TipoPaciente;
  contactosEmergencia: ContactoEmergencia[];

  constructor(
    private pacienteGlobalService: PacienteGlobalService,
    private dialog: MatDialog,
    private contactoEmergenciaService: ContactoEmergenciaService,
    private snackbar: MatSnackBar,
  ) {
  }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente) => {
        this.paciente = paciente;
        this.contactosEmergencia = this.paciente.contactosEmergencia;
      });
  }

  modificarContactoEmergencia(contactoEmergencia: ContactoEmergencia): void {
    this.dialog.open(ContactoEmergenciaModalComponent, {
      data: contactoEmergencia,
      width: '500px',
      maxWidth: '100%',
      disableClose: true
    }).beforeClosed().pipe(
      filter<ContactoEmergencia>(Boolean)
    ).subscribe(value => {
      value.idPaciente = this.paciente.id;
      this.contactoEmergenciaService
        .updateContactoEmergencia(value, value.id, this.paciente.id)
        .subscribe((contacto: ContactoEmergencia) => {
          if (contacto) {
            const index = this.contactosEmergencia.findIndex(c => c.id === contacto.id);
            if (index > -1) {
              this.contactosEmergencia[index] = value;
            }
            this.showNotification('Registro actualizado EXITOSAMENTE', 'OK');
          }
        }, error => {
          this.showNotification('ERROR al actualizar el registro', 'CERRAR');
        });
    });
  }

  agregarContactoEmergencia(): void {
    this.dialog.open(ContactoEmergenciaModalComponent, {
      width: '500px',
      maxWidth: '100%',
      disableClose: true
    }).beforeClosed().pipe(
      filter<ContactoEmergencia>(Boolean)
    ).subscribe(value => {
      this.contactoEmergenciaService
        .createContactoEmergencia(value, this.paciente.id)
        .subscribe((contacto) => {
          if (contacto) {
            this.contactosEmergencia.push(contacto);
            this.showNotification('Registro creado EXITOSAMENTE', 'OK');
          }
        }, error => {
          this.showNotification('ERROR al crear el registro', 'CERRAR');
        });
    });
  }

  eliminarContactoEmergencia(contactoEmergencia: ContactoEmergencia): void {
    this.dialog.open(DeleteModalComponent, {
      data: '¿Esta seguro de eliminar este registro?',
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') {
        this.contactoEmergenciaService
          .deleteContactoEmergencia(contactoEmergencia.id)
          .subscribe((response) => {
            if (response) {
              const index = this.contactosEmergencia.findIndex(c => c.id === contactoEmergencia.id);
              this.contactosEmergencia.splice(index, 1);
              this.showNotification('Registro eliminado EXITOSAMENTE', 'OK');
            }
          }, error => {
            this.showNotification('ERROR al eliminar el registro', 'CERRAR');
          });
      }
    });
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }
}
