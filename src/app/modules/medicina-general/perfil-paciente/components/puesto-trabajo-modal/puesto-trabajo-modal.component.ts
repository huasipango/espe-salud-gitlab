import {Component, Inject, OnInit} from '@angular/core';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {CodigoCiuo} from 'src/app/core/models/catalogo/codigo-ciuo.model';
import {SimpleCatalogUtil} from 'src/app/core/utils/simple-catalog-util';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {debounceTime, distinctUntilChanged, startWith} from 'rxjs/operators';
import {PuestoTrabajo} from 'src/app/core/models/empleado/puesto-trabajo.model';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import icClose from '@iconify/icons-ic/twotone-close';
import icClock from '@iconify/icons-fa-solid/user-clock';
import icActivity from '@iconify/icons-fa-solid/book-open';
import icCheck from '@iconify/icons-fa-solid/check';
import icCode from '@iconify/icons-fa-solid/user-tie';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';

@UntilDestroy()
@Component({
  selector: 'vex-puesto-trabajo-modal',
  templateUrl: './puesto-trabajo-modal.component.html',
  styleUrls: ['./puesto-trabajo-modal.component.scss']
})
export class PuestoTrabajoModalComponent implements OnInit {
  form: FormGroup;
  mode: 'create' | 'update' = 'create';

  ciuoCtrl: FormControl;
  filteredCodigosCiuo: Observable<CodigoCiuo[]>;
  tresOpcionesCatalogo = SimpleCatalogUtil.tresOpcionesCatalogo;
  tiposJornadaLaboral = SimpleCatalogUtil.jornadasLaborales;
  errorMessages = FORM_ERROR_MESSAGES;
  userMessages = USER_MESSAGES;

  icArrowDropDown = icArrowDropDown;
  icClose = icClose;
  icCode = icCode;
  icClock = icClock;
  icCheck = icCheck;
  icActivity = icActivity;

  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: PuestoTrabajo,
    private dialogRef: MatDialogRef<PuestoTrabajoModalComponent>,
    private fb: FormBuilder,
    private catalogoService: CatalogoService
  ) {
  }

  ngOnInit(): void {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as PuestoTrabajo;
    }
    this.ciuoCtrl = new FormControl(
      this.defaults.ciuo || '', Validators.required
    );
    this.ciuoCtrl.valueChanges
      .pipe(
        startWith(''),
        debounceTime(400),
        distinctUntilChanged(),
        untilDestroyed(this)
      ).subscribe(value => this.searchCiuo(value || ''));

    this.form = this.fb.group({
      id: [this.defaults.id || null],
      observacionesHorario: this.defaults.observacionesHorario || '',
      ciuo: this.ciuoCtrl,
      actividadesRelevantes: this.defaults.actividadesRelevantes || '',
      horarioLaboralActual: [this.defaults.horarioLaboralActual || '', Validators.required],
      horasTrabajoDia: [this.defaults.horasTrabajoDia || null, Validators.required],
      horasTrabajoSemana: [this.defaults.horasTrabajoSemana || null, Validators.required],
      horasTrabajoMes: [this.defaults.horasTrabajoMes || null, Validators.required],
      cumpleComisionServicio: [this.defaults.cumpleComisionServicio || '', Validators.required],
      idPaciente: [this.defaults.idPaciente || null, Validators.required],
    });
  }

  searchCiuo(value: string) {
    if (value !== '') {
      this.filteredCodigosCiuo = this.catalogoService.getCodigosCiuoByQuery(value);
    }
  }

  save() {
    const puestoTrabajo = this.form.value;
    this.dialogRef.close(puestoTrabajo);
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }
}
