import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PuestoTrabajoModalComponent } from './puesto-trabajo-modal.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatDialogModule} from '@angular/material/dialog';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {IconModule} from '@visurel/iconify-angular';
import {MatInputModule} from '@angular/material/input';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSelectModule} from '@angular/material/select';



@NgModule({
  declarations: [PuestoTrabajoModalComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatDialogModule,
    MatIconModule,
    MatButtonModule,
    IconModule,
    MatInputModule,
    MatAutocompleteModule,
    MatSelectModule
  ]
})
export class PuestoTrabajoModalModule { }
