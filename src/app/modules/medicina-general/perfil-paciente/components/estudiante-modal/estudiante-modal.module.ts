import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EstudianteModalComponent } from './estudiante-modal.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {IconModule} from '@visurel/iconify-angular';
import {FlexModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSelectModule} from '@angular/material/select';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {UppercaseModule} from 'src/app/shared/directives/uppercase/uppercase.module';



@NgModule({
  declarations: [EstudianteModalComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        MatMenuModule,
        MatIconModule,
        MatListModule,
        MatDialogModule,
        MatInputModule,
        IconModule,
        FlexModule,
        MatButtonModule,
        MatDatepickerModule,
        MatSelectModule,
        MatAutocompleteModule,
        UppercaseModule
    ]
})
export class EstudianteModalModule { }
