import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import icClose from '@iconify/icons-ic/twotone-close';
import icCollege from '@iconify/icons-fa-solid/school';
import icFlag from '@iconify/icons-fa-solid/landmark';
import icCap from '@iconify/icons-fa-solid/graduation-cap';
import icBuilding from '@iconify/icons-fa-solid/building';
import icCode from '@iconify/icons-fa-solid/user-tie';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Estudiante} from 'src/app/core/models/estudiante/estudiante.model';
import {DateUtil} from 'src/app/core/utils/date-utils';
import {Observable} from 'rxjs';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'vex-estudiante-modal',
  templateUrl: './estudiante-modal.component.html',
  styleUrls: ['./estudiante-modal.component.scss']
})
export class EstudianteModalComponent implements OnInit {

  maxDate = new Date();

  form: FormGroup;
  mode: 'create' | 'update' = 'create';

  icMoreVert = icMoreVert;
  icClose = icClose;
  icCollege = icCollege;
  icFlag = icFlag;
  icCap = icCap;
  icCode = icCode;
  icArrowDropDown = icArrowDropDown;
  icBuilding = icBuilding;

  unidadDepartamentoCtrl: FormControl;

  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: any,
    private dialogRef: MatDialogRef<EstudianteModalComponent>,
    private fb: FormBuilder,
    private catalogoService: CatalogoService
  ) {
  }

  ngOnInit(): void {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as Estudiante;
    }
    this.unidadDepartamentoCtrl = new FormControl(
      this.defaults.departamento || '', Validators.required
    );
    this.form = this.fb.group({
      id: [this.defaults.id || null],
      idSede: [this.defaults.idSede || null, Validators.required],
      departamento: this.unidadDepartamentoCtrl,
      fechaIngresoEspe: [DateUtil.fechaReal(this.defaults.fechaIngresoEspe) || ''],
      carrera: [this.defaults.carrera || '', Validators.required],
      escuela: this.defaults.escuela || '',
      ciuo: this.defaults.ciuo || '',
      actividadesRelevantes: this.defaults.actividadesRelevantes || ''
    });
  }

  save() {
    const estudiante = this.form.value;
    const fechaIngresoEspe = DateUtil.customFormatDate(this.form.get('fechaIngresoEspe').value);
    estudiante.fechaIngresoEspe = fechaIngresoEspe;
    this.dialogRef.close(estudiante);
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }
}
