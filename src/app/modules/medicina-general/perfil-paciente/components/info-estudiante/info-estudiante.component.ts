import {Component, OnInit} from '@angular/core';

import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';

import icActividad from '@iconify/icons-ic/baseline-local-activity';
import icSede from '@iconify/icons-ic/account-balance';
import icDireccion from '@iconify/icons-ic/directions';
import icFecha from '@iconify/icons-ic/date-range';
import icCanton from '@iconify/icons-fa-solid/image';
import icFlag from '@iconify/icons-fa-solid/landmark';
import icCap from '@iconify/icons-fa-solid/graduation-cap';
import icBuilding from '@iconify/icons-fa-solid/building';
import icCode from '@iconify/icons-fa-solid/user-tie';
import icEdit from '@iconify/icons-fa-solid/pen';

import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {filter} from 'rxjs/operators';
import {EstudianteService} from 'src/app/core/services/paciente/estudiante.service';
import {EstudianteModalComponent} from 'src/app/modules/medicina-general/perfil-paciente/components/estudiante-modal/estudiante-modal.component';
import {Estudiante} from 'src/app/core/models/estudiante/estudiante.model';
import {TipoPaciente} from 'src/app/core/enums/tipo-paciente.enum';

@Component({
  selector: 'vex-info-estudiante',
  templateUrl: './info-estudiante.component.html',
  styleUrls: ['./info-estudiante.component.scss'],
  animations: [
    fadeInUp400ms,
    fadeInRight400ms,
    scaleIn400ms,
    stagger40ms
  ]
})
export class InfoEstudianteComponent implements OnInit {

  icActividad = icActividad;
  icSede = icSede;
  icDireccion = icDireccion;
  icFecha = icFecha;
  icCanton = icCanton;
  icBuilding = icBuilding;
  icCap = icCap;
  icFlag = icFlag;
  icCode = icCode;
  icEdit = icEdit;

  paciente: Paciente;
  tipoPaciente = TipoPaciente;
  constructor(
    private catalogoService: CatalogoService,
    private pacienteGlobalService: PacienteGlobalService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private estudianteService: EstudianteService
  ) {
  }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.paciente = paciente;
        }
      });
  }

  actualizarEstudiante(): void{
    this.dialog.open(EstudianteModalComponent, {
      data: this.paciente.estudiante,
      width: '900px',
      maxWidth: '100%',
      disableClose: true
    }).beforeClosed().pipe(
      filter<Estudiante>(Boolean)
    ).subscribe(value => {
      this.estudianteService
        .updateEstudiante(value, this.paciente.estudiante.id)
        .subscribe((estudiante: Estudiante) => {
          if (estudiante){
            this.paciente.estudiante = estudiante;
            this.pacienteGlobalService.setPacienteGlobal(this.paciente);
            this.showNotification('Registro actualizado EXITOSAMENTE', 'OK');
          }
        }, error => {
          this.showNotification('ERROR al actualizar el registro', 'CERRAR');
        });
    });
  }
  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }
}
