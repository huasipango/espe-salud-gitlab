import {Component, Inject, OnInit} from '@angular/core';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {GrupoPrioritario} from 'src/app/core/models/catalogo/grupo-prioritario.model';
import {switchMap} from 'rxjs/operators';
import {PacienteService} from 'src/app/core/services/paciente/paciente.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {GrupoPrioritarioPaciente} from 'src/app/core/models/paciente/grupo-prioritario-paciente.model';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import icClose from '@iconify/icons-ic/twotone-close';
import {MatSnackBar} from '@angular/material/snack-bar';
import {LoadingService} from 'src/app/core/services/loading.service';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';

@Component({
  selector: 'vex-grupo-prioritario-modal',
  templateUrl: './grupo-prioritario-modal.component.html',
  styleUrls: ['./grupo-prioritario-modal.component.scss']
})
export class GrupoPrioritarioModalComponent implements OnInit {
  paciente: Paciente;
  gruposPrioritarios: GrupoPrioritario[] = [];
  groups: GrupoPrioritario[] = [];
  icClose = icClose;
  messages = USER_MESSAGES;

  compareFunction = (o1: any, o2: any) => o1.id === o2.id;

  constructor(
    private dialogRef: MatDialogRef<GrupoPrioritarioModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: GrupoPrioritarioPaciente[],
    private catalogoService: CatalogoService,
    private pacienteService: PacienteService,
    private snackbar: MatSnackBar,
    private loadingService: LoadingService,
    private pacienteGlobalService: PacienteGlobalService) { }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$.pipe(
      switchMap((paciente) => {
        this.paciente = paciente;
        return this.catalogoService.getGruposPrioritarios();
      })
    ).subscribe((data) => {
      this.gruposPrioritarios = data;
    });
    this.data.forEach(g => this.groups.push(g.grupoPrioritario));
  }

  save() {
    const created: GrupoPrioritarioPaciente[] = [];
    this.groups.forEach(group => {
      const similarGroup = this.data.find(gpc => gpc.grupoPrioritario.id === group.id);
      if (!similarGroup) {
        const gpc = new GrupoPrioritarioPaciente();
        gpc.id = null;
        gpc.grupoPrioritario = group;
        created.push(gpc);
      } else {
        const gpc = new GrupoPrioritarioPaciente();
        gpc.id = similarGroup.id;
        gpc.grupoPrioritario = group;
        created.push(gpc);
      }
    });
    this.pacienteService.createGrupoPrioritario(this.paciente.id, created)
      .subscribe((response) => {
        if (response) {
          this.showNotification(this.messages.updatedSuccessMessage);
          this.dialogRef.close(response);
        }
      }, error => {
        this.showNotification('No se pudo actualizar los grupos prioritarios');
      });
  }

  showNotification(message: string) {
    this.snackbar.open(message, 'CERRAR', {
      duration: 5000
    });
  }
}
