import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GrupoPrioritarioModalComponent } from './grupo-prioritario-modal.component';
import {MatDialogModule} from '@angular/material/dialog';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {IconModule} from '@visurel/iconify-angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';



@NgModule({
  declarations: [GrupoPrioritarioModalComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    FlexLayoutModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    IconModule,
    ReactiveFormsModule,
    FormsModule,
    LoadingModule
  ]
})
export class GrupoPrioritarioModalModule { }
