import { Component, OnInit } from '@angular/core';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {PacienteService} from 'src/app/core/services/paciente/paciente.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {switchMap} from 'rxjs/operators';
import {GrupoPrioritarioPaciente} from 'src/app/core/models/paciente/grupo-prioritario-paciente.model';
import icAdd from '@iconify/icons-ic/twotone-add';
import {MatDialog} from '@angular/material/dialog';
import {GrupoPrioritarioModalComponent} from 'src/app/modules/medicina-general/perfil-paciente/components/grupo-prioritario-modal/grupo-prioritario-modal.component';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';

@Component({
  selector: 'vex-info-grupos-prioritarios',
  templateUrl: './info-grupos-prioritarios.component.html',
  styleUrls: ['./info-grupos-prioritarios.component.scss'],
  animations: [
    fadeInUp400ms,
    fadeInRight400ms,
    scaleIn400ms,
    stagger40ms,
    scaleFadeIn400ms
  ]
})
export class InfoGruposPrioritariosComponent implements OnInit {
  gruposPrioritarios: GrupoPrioritarioPaciente[] = [];
  icAdd = icAdd;

  constructor(
    private pacienteGlobalService: PacienteGlobalService,
    private pacienteService: PacienteService,
    private snackbar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$.pipe(
      switchMap((paciente) => {
        const idPaciente = paciente.id;
        return this.pacienteService.getGruposPrioritarios(idPaciente);
      })
    ).subscribe((gruposPrioritarios) => {
      this.gruposPrioritarios = gruposPrioritarios;
    });
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  manageGruposPrioritario() {
    this.dialog.open(GrupoPrioritarioModalComponent, {
      data: this.gruposPrioritarios,
      width: '600px',
      maxWidth: '100%'
    }).afterClosed()
      .subscribe((value: GrupoPrioritarioPaciente[]) => {
        if (value) {
          this.gruposPrioritarios = value;
        }
      });
  }
}
