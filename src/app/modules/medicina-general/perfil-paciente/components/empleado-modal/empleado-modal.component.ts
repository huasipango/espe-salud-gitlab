import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {DateUtil} from 'src/app/core/utils/date-utils';
import {Observable} from 'rxjs';

import icClose from '@iconify/icons-ic/twotone-close';
import icCollege from '@iconify/icons-fa-solid/school';
import icClock from '@iconify/icons-fa-solid/user-clock';
import icActivity from '@iconify/icons-fa-solid/book-open';
import icBadge from '@iconify/icons-fa-solid/id-badge';
import icArea from '@iconify/icons-fa-solid/street-view';
import icInfo from '@iconify/icons-fa-solid/info-circle';
import icBuilding from '@iconify/icons-fa-solid/building';
import icCheck from '@iconify/icons-fa-solid/check';
import icCode from '@iconify/icons-fa-solid/user-tie';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';

import {CodigoCiuo} from 'src/app/core/models/catalogo/codigo-ciuo.model';
import {debounceTime, distinctUntilChanged, map, startWith} from 'rxjs/operators';
import {Empleado} from 'src/app/core/models/empleado/empleado.model';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {SimpleCatalogUtil} from 'src/app/core/utils/simple-catalog-util';


@UntilDestroy()
@Component({
  selector: 'vex-empleado-modal',
  templateUrl: './empleado-modal.component.html',
  styleUrls: ['./empleado-modal.component.scss']
})
export class EmpleadoModalComponent implements OnInit {

  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  maxDate = new Date();

  unidadDepartamentoCtrl: FormControl;

  ciuoCtrl: FormControl;
  filteredCodigosCiuo: Observable<CodigoCiuo[]>;

  icMoreVert = icMoreVert;
  icClose = icClose;
  icCollege = icCollege;
  icBuilding = icBuilding;
  icArrowDropDown = icArrowDropDown;
  icCode = icCode;
  icBadge = icBadge;
  icArea = icArea;
  icClock = icClock;
  icActivity = icActivity;
  icCheck = icCheck;
  icInfo = icInfo;

  tiposJornadaLaboral = SimpleCatalogUtil.jornadasLaborales;
  tresOpcionesCatalogo = SimpleCatalogUtil.tresOpcionesCatalogo;
  situacionesAdministrativas = SimpleCatalogUtil.situacionesAdministrativas;

  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: any,
    private dialogRef: MatDialogRef<EmpleadoModalComponent>,
    private fb: FormBuilder,
    private catalogoService: CatalogoService
  ) {
  }

  ngOnInit(): void {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as Empleado;
    }
    this.unidadDepartamentoCtrl = new FormControl(
      this.defaults.unidadDepartamento || '', Validators.required
    );
    this.ciuoCtrl = new FormControl(
      this.defaults.ciuo || '', Validators.required
    );
    this.ciuoCtrl.valueChanges
      .pipe(
        startWith(''),
        debounceTime(400),
        distinctUntilChanged(),
        untilDestroyed(this)
      ).subscribe(value => this.searchCiuo(value || ''));

    this.form = this.fb.group({
      id: [this.defaults.id || null],
      fechaIngresoLaboral: [DateUtil.fechaReal(this.defaults.fechaIngresoLaboral) || ''],
      unidadDepartamento: this.unidadDepartamentoCtrl,
      observacionHorario: this.defaults.observacionHorario || '',
      areaTrabajo: [this.defaults.areaTrabajo || '', Validators.required],
      cargoTrabajoActual: [this.defaults.cargoTrabajoActual || '', Validators.required],
      ciuo: this.ciuoCtrl,
      actividadesRelevantes: this.defaults.actividadesRelevantes || '',
      tipoJornadaLaboral: [this.defaults.tipoJornadaLaboral || '', Validators.required],
      horasTrabajoDia: [this.defaults.horasTrabajoDia || null, Validators.required],
      horasTrabajoSemana: [this.defaults.horasTrabajoSemana || null, Validators.required],
      horasTrabajoMes: [this.defaults.horasTrabajoMes || null, Validators.required],
      cumpleMisionServicio: [this.defaults.cumpleMisionServicio || '', Validators.required],
      situacionAdministrativa: [this.defaults.situacionAdministrativa || '', Validators.required],
      idSede: [this.defaults.idSede || null, Validators.required],
    });
  }

  searchCiuo(value: string) {
    if (value !== '') {
      this.filteredCodigosCiuo = this.catalogoService.getCodigosCiuoByQuery(value);
    }
  }

  save() {
    const empleado = this.form.value;
    const fechaIngresoLaboral = DateUtil.customFormatDate(this.form.get('fechaIngresoLaboral').value);
    empleado.fechaIngresoLaboral = fechaIngresoLaboral;
    this.dialogRef.close(empleado);
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }
}
