import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmpleadoModalComponent } from './empleado-modal.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {FlexModule} from '@angular/flex-layout';
import {MatSelectModule} from '@angular/material/select';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {UppercaseModule} from 'src/app/shared/directives/uppercase/uppercase.module';
import {MatButtonModule} from '@angular/material/button';



@NgModule({
  declarations: [EmpleadoModalComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatIconModule,
    IconModule,
    MatDialogModule,
    MatInputModule,
    MatDatepickerModule,
    FlexModule,
    MatSelectModule,
    MatAutocompleteModule,
    UppercaseModule,
    MatButtonModule
  ]
})
export class EmpleadoModalModule { }
