import {Component, OnInit} from '@angular/core';

import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';

import icActividad from '@iconify/icons-ic/baseline-local-activity';
import icMisionServicio from '@iconify/icons-ic/baseline-thumb-up-alt';
import icDireccion from '@iconify/icons-ic/directions';
import icFecha from '@iconify/icons-ic/date-range';
import icHora from '@iconify/icons-ic/hourglass-empty';
import icCanton from '@iconify/icons-fa-solid/image';
import icSede from '@iconify/icons-ic/account-balance';
import icObservacion from '@iconify/icons-ic/cloud-done';
import icProfesion from '@iconify/icons-fa-solid/chalkboard-teacher';
import icEdit from '@iconify/icons-fa-solid/pen';

import icClose from '@iconify/icons-ic/twotone-close';
import icClock from '@iconify/icons-fa-solid/user-clock';
import icBadge from '@iconify/icons-fa-solid/id-badge';
import icArea from '@iconify/icons-fa-solid/street-view';
import icBuilding from '@iconify/icons-fa-solid/building';
import icCheck from '@iconify/icons-fa-solid/check';
import icCode from '@iconify/icons-fa-solid/user-tie';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';

import {Paciente} from 'src/app/core/models/paciente/paciente.model';

import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {filter} from 'rxjs/operators';
import {EmpleadoModalComponent} from 'src/app/modules/medicina-general/perfil-paciente/components/empleado-modal/empleado-modal.component';
import {Empleado} from 'src/app/core/models/empleado/empleado.model';
import {EmpleadoService} from 'src/app/core/services/paciente/empleado.service';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TipoPaciente} from 'src/app/core/enums/tipo-paciente.enum';
import {PuestoTrabajoModalComponent} from 'src/app/modules/medicina-general/perfil-paciente/components/puesto-trabajo-modal/puesto-trabajo-modal.component';
import {PuestoTrabajo} from 'src/app/core/models/empleado/puesto-trabajo.model';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';

@Component({
  selector: 'vex-info-empleado',
  templateUrl: './info-empleado.component.html',
  styleUrls: ['./info-empleado.component.scss'],
  animations: [
    fadeInUp400ms,
    fadeInRight400ms,
    scaleIn400ms,
    stagger40ms,
    scaleFadeIn400ms
  ]
})
export class InfoEmpleadoComponent implements OnInit {

  paciente: Paciente;

  icActividad = icActividad;
  icMisionServicio = icMisionServicio;
  icDireccion = icDireccion;
  icFecha = icFecha;
  icHora = icHora;
  icCanton = icCanton;
  icSede = icSede;
  icObservacion = icObservacion;
  icProfesion = icProfesion;
  icEdit = icEdit;

  icMoreVert = icMoreVert;
  icClose = icClose;
  icBuilding = icBuilding;
  icArrowDropDown = icArrowDropDown;
  icCode = icCode;
  icBadge = icBadge;
  icArea = icArea;
  icClock = icClock;
  icCheck = icCheck;

  tipoPaciente = TipoPaciente;

  constructor(
    private catalogoService: CatalogoService,
    private empleadoService: EmpleadoService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private pacienteGlobalService: PacienteGlobalService) {
  }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.paciente = paciente;
        }
      });
  }

  actualizarEmpleado(): void {
    this.dialog.open(EmpleadoModalComponent, {
      data: this.paciente.empleado,
      width: '900px',
      maxWidth: '100%',
      disableClose: true
    }).beforeClosed().pipe(
      filter<Empleado>(Boolean)
    ).subscribe(value => {
      this.empleadoService
        .updateEmpleado(value, this.paciente.empleado.id)
        .subscribe((empleado: Empleado) => {
          if (empleado) {
            this.paciente.empleado = empleado;
            this.pacienteGlobalService.setPacienteGlobal(this.paciente);
            this.showNotification('Registro actualizado EXITOSAMENTE', 'OK');
          }
        }, error => {
          this.showNotification('ERROR al actualizar el registro', 'CERRAR');
        });
    });
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  registerPuestoTrabajo() {
    this.dialog.open(PuestoTrabajoModalComponent, {
      maxWidth: '100%',
      width: '700px',
      disableClose: true
    }).beforeClosed().pipe(
      filter<PuestoTrabajo>(Boolean)
    ).subscribe(value => {
      console.log(value);
    });
  }
}
