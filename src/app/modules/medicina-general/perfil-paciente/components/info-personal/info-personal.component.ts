import {Component, OnInit} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import icAdd from '@iconify/icons-ic/twotone-add';
import icWhatshot from '@iconify/icons-ic/twotone-whatshot';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icCheck from '@iconify/icons-ic/twotone-check';
import icPerson from '@iconify/icons-ic/person';
import icCedula from '@iconify/icons-fa-solid/address-card';
import icNacimiento from '@iconify/icons-fa-solid/birthday-cake';
import icSexo from '@iconify/icons-fa-solid/transgender-alt';
import icCivil from '@iconify/icons-fa-solid/heart';
import icSangre from '@iconify/icons-fa-solid/syringe';
import icLateralidad from '@iconify/icons-fa-solid/arrows-alt-h';
import icTransfucion from '@iconify/icons-fa-solid/heartbeat';
import icInstruccion from '@iconify/icons-fa-solid/poll';
import icProfesion from '@iconify/icons-fa-solid/chalkboard-teacher';
import icAsosiacion from '@iconify/icons-fa-solid/share-alt';
import icSalud from '@iconify/icons-fa-solid/ambulance';
import icVinculado from '@iconify/icons-fa-solid/check';
import icNacionalidad from '@iconify/icons-fa-solid/flag';
import icPueblo from '@iconify/icons-fa-solid/city';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icChurch from '@iconify/icons-fa-solid/church';
import icCanton from '@iconify/icons-fa-solid/image';

import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {enumSelector} from 'src/app/core/utils/enum-to-string-util';
import {SexoEnum} from 'src/app/core/enums/sexo.enum';

@Component({
  selector: 'vex-info-personal',
  templateUrl: './info-personal.component.html',
  styleUrls: ['./info-personal.component.scss'],
  animations: [
    fadeInUp400ms,
    fadeInRight400ms,
    scaleIn400ms,
    stagger40ms,
    scaleFadeIn400ms
  ]
})
export class InfoPersonalComponent implements OnInit {
  paciente: Paciente;
  icCheck = icCheck;
  icAdd = icAdd;
  icWhatshot = icWhatshot;
  icPerson = icPerson;
  icCedula = icCedula;
  icNacimiento = icNacimiento;
  icSexo = icSexo;
  icSangre = icSangre;
  icLateralidad = icLateralidad;
  icTransfucion = icTransfucion;
  icInstruccion = icInstruccion;
  icProfesion = icProfesion;
  icAsosiacion = icAsosiacion;
  icSalud = icSalud;
  icEdit = icEdit;
  icVinculado = icVinculado;
  icCanton = icCanton;
  icCivil = icCivil;

  icNacionalidad = icNacionalidad;
  icPueblo = icPueblo;
  icPhone = icPhone;
  icChurch = icChurch;

  msg1 = '';
  sexoList = enumSelector(SexoEnum);

  constructor(
    private catalogoService: CatalogoService,
    private pacienteGlobalService: PacienteGlobalService
  ) {
  }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        this.paciente = paciente;
        this.infoPersonal();
      });
  }

  infoPersonal() {
    if (this.paciente.aceptaTransfucion) {
      this.msg1 = 'Si';
    } else {
      this.msg1 = 'No';
    }
  }

  getSexoLongName(sexo: string): string {
    const founded = this.sexoList.find(s => s.value === sexo)
    return founded ? founded.title : '';
  }

}
