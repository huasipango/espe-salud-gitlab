import {Component, OnInit} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import icInfo from '@iconify/icons-fa-solid/info';
import icCorreo from '@iconify/icons-ic/baseline-alternate-email';
import icCanton from '@iconify/icons-fa-solid/image';
import icExtension from '@iconify/icons-ic/extension';
import icConvencional from '@iconify/icons-ic/phone';
import icCelular from '@iconify/icons-ic/smartphone';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';


@Component({
  selector: 'vex-info-contacto',
  templateUrl: './info-contacto.component.html',
  styleUrls: ['./info-contacto.component.scss'],
  animations: [
    fadeInUp400ms,
    fadeInRight400ms,
    scaleIn400ms,
    stagger40ms
  ]
})
export class InfoContactoComponent implements OnInit {

  paciente: Paciente;

  icInfo = icInfo;
  icCorreo = icCorreo;
  icExtension = icExtension;
  icCanton = icCanton;
  icCelular = icCelular;
  icConvencional = icConvencional;

  constructor(
    private catalogoService: CatalogoService,
    private pacienteGlobalService: PacienteGlobalService
  ) {
  }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        this.paciente = paciente;
      });
  }
}
