import {Component, OnInit} from '@angular/core';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {Link} from 'src/@vex/interfaces/link.interface';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {Observable, of} from 'rxjs';
import {TipoPaciente} from 'src/app/core/enums/tipo-paciente.enum';
import {ImagenUsuarioService} from 'src/app/core/services/imagen-usuario.service';
import {PacienteService} from 'src/app/core/services/paciente/paciente.service';

@Component({
  selector: 'vex-perfil-paciente',
  templateUrl: './perfil-paciente.component.html',
  styleUrls: ['./perfil-paciente.component.scss'],
  animations: [
    scaleIn400ms,
    fadeInRight400ms
  ]
})
export class PerfilPacienteComponent implements OnInit {
  imagePath: any;
  paciente: Paciente;
  dispensario: number;
  nombreCompleto: string;
  tipoPaciente = TipoPaciente;

  links: Observable<Link[]>;
  defaultLinks: Link[] = [
    {
      label: 'DATOS PERSONALES',
      route: './',
      routerLinkActiveOptions: {exact: true}
    }
  ];

  defaultEstudianteLinks: Link[] = [
    {
      label: 'DATOS PERSONALES',
      route: './',
      routerLinkActiveOptions: {exact: true}
    },
    {
      label: 'ESTUDIANTE',
      route: './estudiante'
    },
  ];

  defaultEmpleadoLinks: Link[] = [
    {
      label: 'DATOS PERSONALES',
      route: './',
      routerLinkActiveOptions: {exact: true}
    },
    {
      label: 'EMPLEADO',
      route: './empleado'
    }
  ];


  constructor(
    private pacienteGlobalService: PacienteGlobalService,
    private catalogoService: CatalogoService,
    private imagenUsuarioService: ImagenUsuarioService
  ) {
  }

  ngOnInit(): void {
    this.links = of(this.defaultLinks);
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.paciente = paciente;
          this.dispensario = paciente.idDispensario;
          this.selectDispensario();
          if (this.paciente.esEmpleado) {
            this.links = of(this.defaultEmpleadoLinks);
          } else if (this.paciente.esEstudiante) {
            this.links = of(this.defaultEstudianteLinks);
          }
        }
      });
  }

  selectDispensario() {
    this.catalogoService.getDispensario(this.dispensario)
      .subscribe((response) => {
        if (response) {
          this.imagePath = this.catalogoService.getDispensarioImagen(response.nombreImagen);
        }
      });
  }

  getUserImage(idBanner: string, tipoPaciente: string): string {
    if (idBanner && tipoPaciente === TipoPaciente.INTERNO) {
      return this.imagenUsuarioService.getUserImage(idBanner);
    } else {
      return 'assets/img/demo/hombre.svg';
    }
  }
}

