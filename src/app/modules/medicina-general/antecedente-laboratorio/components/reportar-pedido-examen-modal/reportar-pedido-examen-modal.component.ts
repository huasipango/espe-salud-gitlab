import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {PedidoExamen} from 'src/app/modules/medicina-general/examen-laboratorio/models/pedido-examen.model';
import icClose from '@iconify/icons-ic/twotone-close';
import icAssignment from '@iconify/icons-ic/twotone-assignment';
import icMicroscope from '@iconify/icons-fa-solid/microscope';

import {FormBuilder, FormGroup} from '@angular/forms';
import {ImagenUsuarioService} from 'src/app/core/services/imagen-usuario.service';
import {ColorUtil} from 'src/app/core/utils/color-utils';
import {EstadoPedidoExamenEnum} from 'src/app/core/enums/estado-pedido-examen.enum';

@Component({
  selector: 'vex-reportar-pedido-examen-modal',
  templateUrl: './reportar-pedido-examen-modal.component.html',
  styleUrls: ['./reportar-pedido-examen-modal.component.scss']
})
export class ReportarPedidoExamenModalComponent implements OnInit {
  form: FormGroup;
  icClose = icClose;
  icAssignment = icAssignment;
  icMicroscope = icMicroscope;
  labelClassInfo = ColorUtil.labStateColors();
  estadoPedidoExamenEnum = EstadoPedidoExamenEnum;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: PedidoExamen,
    private dialogRef: MatDialogRef<ReportarPedidoExamenModalComponent>,
    private fb: FormBuilder,
    private imagenUsuarioService: ImagenUsuarioService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      examenes: this.fb.array(this.data.examenes.map(examen => this.fb.group({
        id: examen.id,
        examen: examen.examen,
        detalles: this.fb.array(examen.detalles.map(detalle => this.fb.group({
          detalle: [detalle.detalle],
          valor: [detalle.valor],
          observacion: [detalle.observacion]
        }))),
      })))
    })
  }

  getExamenesArray(form){
    return form.controls.examenes.controls;
  }

  getDetallesArray(form){
    return form.controls.detalles.controls;
  }

  save() {
    const data: PedidoExamen = this.form.value;
    this.dialogRef.close(data.examenes);
  }

  getUserImage(id: string) {
    return this.imagenUsuarioService.getUserImage(id);
  }

  getLabStateColor(state: string) {
    const foundState = this.labelClassInfo.find(s => s.state === state);
    if (foundState) {
      return [foundState.color.bgClass, foundState.color.textClass];
    }
    return [];
  }
}
