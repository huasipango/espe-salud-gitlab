import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportarPedidoExamenModalComponent } from './reportar-pedido-examen-modal.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {FlexLayoutModule} from '@angular/flex-layout';
import {IconModule} from '@visurel/iconify-angular';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {MatInputModule} from '@angular/material/input';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatListModule} from '@angular/material/list';
import {MatTooltipModule} from '@angular/material/tooltip';



@NgModule({
  declarations: [
    ReportarPedidoExamenModalComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatDialogModule,
    FlexLayoutModule,
    IconModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatInputModule,
    MatExpansionModule,
    MatListModule,
    MatTooltipModule
  ],
  exports: [
    ReportarPedidoExamenModalComponent
  ]
})
export class ReportarPedidoExamenModalModule { }
