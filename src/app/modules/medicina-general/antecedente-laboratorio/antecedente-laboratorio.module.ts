import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AntecedenteLaboratorioRoutingModule } from 'src/app/modules/medicina-general/antecedente-laboratorio/antecedente-laboratorio-routing.module';
import {AntecedenteLaboratorioComponent} from 'src/app/modules/medicina-general/antecedente-laboratorio/antecedente-laboratorio.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {MatButtonModule} from '@angular/material/button';
import {IconModule} from '@visurel/iconify-angular';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatRippleModule} from '@angular/material/core';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {SecondaryToolbarModule} from 'src/@vex/components/secondary-toolbar/secondary-toolbar.module';
import {PageLayoutModule} from 'src/@vex/components/page-layout/page-layout.module';
import {ScrollbarModule} from 'src/@vex/components/scrollbar/scrollbar.module';


@NgModule({
  declarations: [AntecedenteLaboratorioComponent],
  imports: [
    CommonModule,
    AntecedenteLaboratorioRoutingModule,
    FlexLayoutModule,
    ContainerModule,
    MatButtonModule,
    IconModule,
    MatTabsModule,
    MatTooltipModule,
    MatIconModule,
    MatListModule,
    MatRippleModule,
    BreadcrumbsModule,
    SecondaryToolbarModule,
    PageLayoutModule,
    ScrollbarModule,
  ]
})
export class AntecedenteLaboratorioModule { }
