import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {VexRoutes} from 'src/@vex/interfaces/vex-route.interface';
import {AntecedenteLaboratorioComponent} from 'src/app/modules/medicina-general/antecedente-laboratorio/antecedente-laboratorio.component';

const routes: VexRoutes = [
  {
    path: '',
    redirectTo: 'todos'
  },
  {
    path: ':filter',
    component: AntecedenteLaboratorioComponent,
    data: {
      scrollDisabled: false,
      toolbarShadowEnabled: false,
      containerEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AntecedenteLaboratorioRoutingModule {
}
