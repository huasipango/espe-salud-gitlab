import { Component, OnInit } from '@angular/core';
import {Link} from 'src/@vex/interfaces/link.interface';
import icCloudDownload from '@iconify/icons-ic/twotone-cloud-download';
import icMicroscope from '@iconify/icons-fa-solid/microscope';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icAdd from '@iconify/icons-ic/twotone-add';
import {stagger80ms} from 'src/@vex/animations/stagger.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {Observable} from 'rxjs';
import {trackById} from 'src/@vex/utils/track-by';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {PedidoExamenService} from 'src/app/modules/medicina-general/examen-laboratorio/services/pedido-examen.service';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {PedidoExamen} from 'src/app/modules/medicina-general/examen-laboratorio/models/pedido-examen.model';
import {ExamenLaboratorio} from 'src/app/modules/medicina-general/examen-laboratorio/models/examen-laboratorio.model';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {ColorUtil} from 'src/app/core/utils/color-utils';
import {MatDialog} from '@angular/material/dialog';
import {ReportarPedidoExamenModalComponent} from 'src/app/modules/medicina-general/antecedente-laboratorio/components/reportar-pedido-examen-modal/reportar-pedido-examen-modal.component';
import {LoadingService} from 'src/app/core/services/loading.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';

@Component({
  selector: 'vex-antecedente-laboratorio',
  templateUrl: './antecedente-laboratorio.component.html',
  styleUrls: ['./antecedente-laboratorio.component.scss'],
  animations: [
    stagger80ms,
    fadeInRight400ms,
    fadeInUp400ms,
    scaleFadeIn400ms
  ]
})
export class AntecedenteLaboratorioComponent implements OnInit {
  menuWidth = '250px';
  options = { autoHide: false, scrollbarMinSize: 100 };

  links: Link[] = [
    {
      label: 'TODOS',
      route: '../todos'
    },
  ];
  icCloudDownload = icCloudDownload;
  icMicroscope = icMicroscope;
  icAdd = icAdd;
  icEdit = icEdit;
  paciente: Paciente;
  pedidosExamen: Observable<PedidoExamen[]>;
  trackById = trackById;
  labelClassInfo = ColorUtil.labStateColors();
  userMessages = USER_MESSAGES;

  constructor(
    private catalogoService: CatalogoService,
    private pedidoExamenService: PedidoExamenService,
    private pacienteGlobalService: PacienteGlobalService,
    private dialog: MatDialog,
    private loadingService: LoadingService,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe(paciente => {
        if (paciente) {
          this.paciente = paciente;
          this.pedidosExamen = this.pedidoExamenService.getPedidosExamen(this.paciente.id);
        }
      });
  }

  crearNuevoExamen() {
  }

  openExamen(examen: ExamenLaboratorio) {
  }

  getLabStateColor(state: string) {
    const foundState = this.labelClassInfo.find(s => s.state === state);
    if (foundState) {
      return [foundState.color.bgClass, foundState.color.textClass];
    }
    return [];
  }

  reportarResultados(pedidoExamen: PedidoExamen) {
    this.dialog.open(ReportarPedidoExamenModalComponent, {
      data: pedidoExamen,
      width: '800px',
      maxWidth: '100%',
      disableClose: true
    }).afterClosed()
      .subscribe((data) => {
        this.loadingService.showLoaderUntilCompleted(
          this.pedidoExamenService.reportResults(pedidoExamen.id, data)
        ).subscribe((response) => {
          if (response) {
            console.log(response);
            this.showNotification(this.userMessages.updatedSuccessMessage);
          }
        }, error => {
          this.showNotification(this.userMessages.updatedFailedMessage);
        })
      });
  }

  showNotification(message: string) {
    this.snackbar.open(message, 'CERRAR', {
      duration: 5000
    });
  }

}
