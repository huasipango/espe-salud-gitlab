import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from 'src/@vex/interfaces/vex-route.interface';
import {PacienteExternoFormComponent} from 'src/app/modules/medicina-general/registro-paciente/pages/paciente-externo-form/paciente-externo-form.component';
import {PacienteInternoFormComponent} from 'src/app/modules/medicina-general/registro-paciente/pages/paciente-interno-form/paciente-interno-form.component';

const routes: VexRoutes = [
  {
    path: 'externo',
    component: PacienteExternoFormComponent,
    data: {
      toolbarShadowEnabled: true,
      containerEnabled: true
    }
  },
  {
    path: 'interno',
    component: PacienteInternoFormComponent,
    data: {
      toolbarShadowEnabled: true,
      containerEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistroPacienteRoutingModule { }
