import { Component, OnInit } from '@angular/core';
import icSearch from '@iconify/icons-ic/twotone-search';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {BannerService} from 'src/app/core/services/banner.service';
import {FormControl} from '@angular/forms';
import {LoadingService} from 'src/app/core/services/loading.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {PersonaBanner} from 'src/app/core/models/banner/persona-banner-model';
import {MatDialog} from '@angular/material/dialog';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {ConsentimientoModalComponent} from 'src/app/modules/medicina-general/registro-paciente/components/consentimiento-modal/consentimiento-modal.component';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import {ReportDashboardService} from 'src/app/core/services/report-dashboard.service';
import {AuthService} from 'src/app/core/auth/auth.service';
import {FormularioPacienteInternoComponent} from 'src/app/modules/medicina-general/registro-paciente/components/formulario-paciente-interno/formulario-paciente-interno.component';

export interface PacienteInternoForm {
  personaBanner?: PersonaBanner;
  paciente?: Paciente;
  consentimientoInformadoAceptado: boolean;
}

@Component({
  selector: 'vex-paciente-interno-form',
  templateUrl: './paciente-interno-form.component.html',
  styleUrls: ['./paciente-interno-form.component.scss'],
  animations: [
    stagger40ms,
    fadeInUp400ms
  ]
})
export class PacienteInternoFormComponent implements OnInit {
  icSearch = icSearch;
  busquedaCtrl = new FormControl('');
  data: PersonaBanner;
  userMessages = USER_MESSAGES;
  user: SaludUser;
  constructor(
    private bannerService: BannerService,
    private loadingService: LoadingService,
    private matSnackBar: MatSnackBar,
    private dialog: MatDialog,
    private authService: AuthService,
    private reportDashboardService: ReportDashboardService) { }

  ngOnInit(): void {
    this.authService.saludUserData$.subscribe(user => this.user = user);
  }

  buscarPacienteInterno(): void {
    if (this.busquedaCtrl.value === ''){ return; }
    this.loadingService.showLoaderUntilCompleted(
      this.bannerService.getPersonaBannerInfo(this.busquedaCtrl.value)
    ).subscribe((data) => {
        if (data.length > 0) {
          this.data = data[0];
        } else {
          this.matSnackBar.open('No se encontraron resultados', 'CERRAR');
          this.data = null;
        }
      }, (error) => {
        this.matSnackBar.open('Ha ocurrido un error en la consulta', 'CERRAR');
    });
  }

  abrirFormPacienteInterno(): void {
    this.data.cedula = this.busquedaCtrl.value;
    const formData: PacienteInternoForm = {
      personaBanner: this.data,
      paciente: null,
      consentimientoInformadoAceptado: true
    };
    this.dialog.open(ConsentimientoModalComponent, {
      data: this.userMessages.acceptConInf,
      width: '500px',
      maxWidth: '100%',
      disableClose: true
    }).afterClosed()
      .subscribe(value => {
        if (value) {
          if (value === 'si') {
            formData.consentimientoInformadoAceptado = true;
            this.loadingService.showLoaderUntilCompleted(
              this.reportDashboardService.getConsentimientoInformadoAceptado(
                formData.personaBanner.nombres,
                formData.personaBanner.apellidos,
                formData.personaBanner.cedula,
                this.user.pidm
              )
            ).subscribe(() => {
              this.dialog.open(FormularioPacienteInternoComponent, {
                data: formData,
                width: '700px',
                maxWidth: '100%',
                disableClose: true
              }).afterClosed();
            }, error => {
              this.matSnackBar.open(this.userMessages.getConInfDocFail, 'CERRAR', {
                duration: 5000
              });
            })
          } else {
            formData.consentimientoInformadoAceptado = false;
            this.loadingService.showLoaderUntilCompleted(
              this.reportDashboardService.getConsentimientoInformadoNegado(this.user.pidm)
            ).subscribe(() => {
              this.matSnackBar.open(this.userMessages.rejectedConInf, 'CERRAR', {
                duration: 5000
              });
            }, error => {
              this.matSnackBar.open(this.userMessages.getConInfDocFail, 'CERRAR', {
                duration: 5000
              });
            })
          }
        }
      });
  }
}
