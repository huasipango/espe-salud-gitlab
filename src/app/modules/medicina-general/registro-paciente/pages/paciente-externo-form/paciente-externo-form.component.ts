import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import icDescription from '@iconify/icons-ic/twotone-description';
import icDoneAll from '@iconify/icons-ic/twotone-done-all';
import {stagger80ms} from 'src/@vex/animations/stagger.animation';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {PacienteService} from 'src/app/core/services/paciente/paciente.service';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {UniqueNumeroArchivoValidator} from 'src/app/shared/directives/unique-cedula/unique-cedula.directive';
import {SimpleCatalogUtil} from 'src/app/core/utils/simple-catalog-util';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {ContactoEmergencia} from 'src/app/core/models/paciente/contacto-emergencia.model';
import {MatTableDataSource} from '@angular/material/table';
import {enumSelector} from 'src/app/core/utils/enum-to-string-util';
import {SexoEnum} from 'src/app/core/enums/sexo.enum';
import {LateralidadEnum} from 'src/app/core/enums/lateralidad.enum';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {CatalogoBannerService} from 'src/app/core/services/catalogo-banner.service';
import {Observable} from 'rxjs';
import {EstadoCivil} from 'src/app/core/models/catalogo-banner/estado-civil.model';
import {Religion} from 'src/app/core/models/catalogo-banner/religion.model';
import {SeguroSalud} from 'src/app/core/models/catalogo/seguro-salud.model';
import {Asociacion} from 'src/app/core/models/catalogo/asociacion.model';
import {NivelInstruccion} from 'src/app/core/models/catalogo/nivel-instruccion.model';
import {Pais} from 'src/app/core/models/catalogo-banner/pais.model';
import {Provincia} from 'src/app/core/models/catalogo-banner/provincia,model';
import {Canton} from 'src/app/core/models/catalogo-banner/canton.model';
import {Nacionalidad} from 'src/app/core/models/catalogo-banner/nacionalidad.model';
import {Etnia} from 'src/app/core/models/catalogo-banner/etnia.model';
import {Dispensario} from 'src/app/core/models/catalogo/dispensario.model';
import {PacienteExterno} from 'src/app/core/models/paciente/paciente-externo.model';
import {LoadingService} from 'src/app/core/services/loading.service';
import {RelacionEspe} from 'src/app/core/models/catalogo/relacion-espe.model';
import {Plurinacionalidad} from 'src/app/core/models/catalogo/plurinacionalidad.model';
import {TipoSangre} from 'src/app/core/models/catalogo-banner/tipo-sangre.model';
import {TipoIdentificacion} from 'src/app/core/models/catalogo/tipo-identificacion.model';
import {
  ECUADOR_COUNTRY_NAME,
  ECUADOR_NACIONALIDAD_NAME,
  FORM_ERROR_MESSAGES,
  MAX_AGE_TO_CI_REPRESENANTE
} from 'src/app/core/constants/constants';
import {GrupoPrioritario} from 'src/app/core/models/catalogo/grupo-prioritario.model';
import {calculateAge, getFullAge} from 'src/app/core/utils/operations.util';
import {Pueblo} from 'src/app/core/models/catalogo/pueblo.model';
import {ParroquiaSalud} from 'src/app/core/models/catalogo/parroquia-salud.model';
import {ConsentimientoModalComponent} from 'src/app/modules/medicina-general/registro-paciente/components/consentimiento-modal/consentimiento-modal.component';
import {FormularioPacienteInternoComponent} from 'src/app/modules/medicina-general/registro-paciente/components/formulario-paciente-interno/formulario-paciente-interno.component';
import {ReportDashboardService} from 'src/app/core/services/report-dashboard.service';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import {AuthService} from 'src/app/core/auth/auth.service';

@Component({
  selector: 'vex-paciente-externo-form',
  templateUrl: './paciente-externo-form.component.html',
  styleUrls: ['./paciente-externo-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    stagger80ms,
    fadeInUp400ms,
    scaleIn400ms,
    fadeInRight400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class PacienteExternoFormComponent implements OnInit {
  columns: TableColumn<ContactoEmergencia>[] = [
    {label: 'Nombres', property: 'nombreContacto', type: 'text', visible: true},
    {label: 'Nombres', property: 'apellidoContacto', type: 'text', visible: true},
    {label: 'Parentesco', property: 'idParentesco', type: 'text', visible: true},
    {label: 'Dirección', property: 'direccion', type: 'text', visible: true},
    {label: 'Teléfono Celular', property: 'telefonoCelular', type: 'text', visible: true},
    {label: 'Teléfono Convencional', property: 'telefonoConvencional', type: 'text', visible: true},
    {label: 'Acciones', property: 'actions', type: 'button', visible: true}
  ];
  dataSource: MatTableDataSource<ContactoEmergencia> | null;

  informacionPersonalFormGroup: FormGroup;
  informacionContactoFormGroup: FormGroup;
  informacionAdicionalFormGroup: FormGroup;

  icDescription = icDescription;
  icDoneAll = icDoneAll;

  sexoList = enumSelector(SexoEnum);
  lateralidades = enumSelector(LateralidadEnum);
  estadosCiviles: Observable<EstadoCivil[]>;
  gruposSanguineos: Observable<TipoSangre[]>;
  religiones: Observable<Religion[]>;
  segurosSalud: Observable<SeguroSalud[]>;
  asosiaciones: Observable<Asociacion[]>;
  nivelesInstrucccion: Observable<NivelInstruccion[]>;
  relacionesEspe: Observable<RelacionEspe[]>;
  phonePrefixOptions = SimpleCatalogUtil.codigosRegion;
  contactosEmergencia: ContactoEmergencia[] = [];
  today = new Date();

  paises: Observable<Pais[]>;
  provincias: Observable<Provincia[]>;
  cantones: Observable<Canton[]>;
  cantonesContacto: Observable<Canton[]>;
  parroquias: Observable<ParroquiaSalud[]>;
  nacionalidades: Observable<Nacionalidad[]>;
  gruposCulturales: Observable<Etnia[]>;
  nacionalidadesPueblos: Observable<Plurinacionalidad[]>;
  gruposPrioritarios: Observable<GrupoPrioritario[]>;
  tiposIdentificacion: TipoIdentificacion[];
  pueblos: Observable<Pueblo[]>;
  zonas = SimpleCatalogUtil.zonas;
  dispensarios: Dispensario[] = [];
  dispensarioSeleccionado: Dispensario;
  numeroIdentifiacionIsRequired = false;
  numeroIdentificacionRepresentanteIsRequired = false;
  errorMessages = FORM_ERROR_MESSAGES;
  fullAge = '';
  cantonNacimientoIsRequired = true;
  provinciaNacimientoIsRequired = true;
  userMessages = USER_MESSAGES;
  user: SaludUser;

  constructor(
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private snackbar: MatSnackBar,
    private router: Router,
    private dialog: MatDialog,
    private pacienteService: PacienteService,
    private pacienteGlobalService: PacienteGlobalService,
    private numeroArchivoValidator: UniqueNumeroArchivoValidator,
    private catalogoService: CatalogoService,
    private catalogoBannerService: CatalogoBannerService,
    private loadingService: LoadingService,
    private reportDashboardService: ReportDashboardService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.authService.saludUserData$.subscribe(value => this.user = value);
    this.estadosCiviles = this.catalogoBannerService.getEstadosCiviles();
    this.religiones = this.catalogoBannerService.getReligiones();
    this.segurosSalud = this.catalogoService.getSegurosSalud();
    this.asosiaciones = this.catalogoService.getAsociaciones();
    this.nivelesInstrucccion = this.catalogoService.getNivelesInstruccion();
    this.paises = this.catalogoBannerService.getPaises();
    this.provincias = this.catalogoBannerService.getProvincias();
    this.nacionalidades = this.catalogoBannerService.getNacionalidades();
    this.gruposCulturales = this.catalogoService.getEtnias();
    this.relacionesEspe = this.catalogoService.getRelacionesEspe();
    this.gruposSanguineos = this.catalogoBannerService.getTiposSangre();
    this.gruposPrioritarios = this.catalogoService.getGruposPrioritarios();
    this.catalogoService.getTiposIdentificacion()
      .subscribe((data) => this.tiposIdentificacion = data);
    this.catalogoService.getDispensarios()
    .subscribe(
      (data) => {
        this.dispensarios = data;
      }
    );
    this.cargarInformacionPersonalFormGroup();
    this.cargarInformacionContactoFormGroup();
    this.cargarInformacionAdicionalFormGroup();
  }

  cargarInformacionPersonalFormGroup(): void {
    this.informacionPersonalFormGroup = this.fb.group({
      primerNombre: [null, Validators.required],
      segundoNombre: [null, Validators.required],
      apellidoPaterno: [null, Validators.required],
      apellidoMaterno: [null, Validators.required],
      numeroIdentificacion: [
        null,
      ],
      fechaNacimiento: [null, Validators.required],
      sexo: ['', Validators.required],
      estadoCivil: ['', Validators.required],
      grupoSanguineo: ['', Validators.required],
      idEtnia: ['', Validators.required],
      paisNacimiento: [null, Validators.required],
      provinciaNacimiento: [null, Validators.required],
      cantonNacimiento: [null, Validators.required],
      nacionalidad: ['', Validators.required],
      nacionalidad2: [''],
      religion: ['', Validators.required],
      aceptaTransfucion: [false],
      idPueblo: [null],
      idNacionalidadEtnica: [null],
      lateralidad: ['', Validators.required],
      idVinculadoEspe: ['', Validators.required],
      profesion: [''],
      idInstruccion: ['', Validators.required],
      idSeguroSalud: [null, Validators.required],
      idAsociacionAfiliada: [null, Validators.required],
      idTipoIdentificacion: [null, Validators.required],
      consentimientoInformadoAceptado: [null, Validators.required]
    });
    this.onChanges();
  }

  cargarInformacionContactoFormGroup() {
    this.informacionContactoFormGroup = this.fb.group({
      callePrincipal: ['', Validators.required],
      calleSecundaria: [''],
      direccionReferencia: [''],
      parroquiaResidencia: [''],
      barrio: [''],
      zonaGeografica: ['', Validators.required],
      numeroCelular: ['', Validators.required],
      numeroConvencional: [''],
      extension: [''],
      correoPersonal: ['', Validators.email],
      correoInstitucional: ['', Validators.email],
      provinciaResidencia: [null, Validators.required],
      cantonResidencia: [null, Validators.required],
      contactosEmergencia: [0, Validators.min(0)]
    });
  }

  cargarInformacionAdicionalFormGroup() {
    this.informacionAdicionalFormGroup = this.fb.group({
      idDispensario: [null, Validators.required],
      identificacionRepresentante: [null],
      gruposPrioritarios: [null]
    });
  }

  onChanges(): void {
    this.informacionPersonalFormGroup.get('fechaNacimiento')
      .valueChanges
      .subscribe(val => {
        if (val) {
          this.fullAge = getFullAge(val);
          if (calculateAge(val) <= MAX_AGE_TO_CI_REPRESENANTE) {
            this.numeroIdentificacionRepresentanteIsRequired = true;
            this.informacionAdicionalFormGroup.get('identificacionRepresentante').setValidators(
              [Validators.required]
            );
            this.informacionAdicionalFormGroup.get('identificacionRepresentante').updateValueAndValidity();
          } else {
            this.numeroIdentificacionRepresentanteIsRequired = false;
            this.informacionAdicionalFormGroup.get('identificacionRepresentante').clearValidators();
            this.informacionAdicionalFormGroup.get('identificacionRepresentante').updateValueAndValidity();
          }
        }
    });
  }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  createPaciente() {
    const paciente: PacienteExterno = this.informacionPersonalFormGroup.getRawValue();
    paciente.contacto = this.informacionContactoFormGroup.value;
    paciente.contactosEmergencia = this.contactosEmergencia;
    paciente.idDispensario = this.informacionAdicionalFormGroup.get('idDispensario').value;
    paciente.gruposPrioritarios = this.informacionAdicionalFormGroup.get('gruposPrioritarios').value;
    paciente.identificacionRepresentante = this.informacionAdicionalFormGroup.get('identificacionRepresentante').value;
    paciente.provinciaNacimiento = this.informacionPersonalFormGroup.get('provinciaNacimiento').value?.descripcion;
    paciente.idProvinciaNacimiento = this.informacionPersonalFormGroup.get('provinciaNacimiento').value?.idProvincia;
    paciente.cantonNacimiento = this.informacionPersonalFormGroup.get('cantonNacimiento').value?.descripcion;
    paciente.idCantonNacimiento = this.informacionPersonalFormGroup.get('cantonNacimiento').value?.idCanton;

    paciente.contacto.provinciaResidencia = this.informacionContactoFormGroup.get('provinciaResidencia').value.descripcion;
    paciente.contacto.idProvinciaResidencia = this.informacionContactoFormGroup.get('provinciaResidencia').value.idProvincia;
    paciente.contacto.cantonResidencia = this.informacionContactoFormGroup.get('cantonResidencia').value.descripcion;
    paciente.contacto.idCantonResidencia = this.informacionContactoFormGroup.get('cantonResidencia').value.idCanton;
    paciente.contacto.idParroquiaResidencia = this.informacionContactoFormGroup.get('parroquiaResidencia').value.id;
    paciente.contacto.parroquiaResidencia = this.informacionContactoFormGroup.get('parroquiaResidencia').value.nombre;

    this.loadingService.showLoaderUntilCompleted(
      this.pacienteService.createPacienteExterno(paciente)
    ).subscribe((response) => {
      if (response){
        this.pacienteService.getPacienteByNumeroArchivo(response.numeroArchivo)
          .subscribe((registered) => {
            this.pacienteGlobalService.setPacienteGlobal(registered);
            this.pacienteGlobalService.setLastPacienteToLocalStorage(registered.numeroArchivo);
          });
        this.showNotification('El paciente ha sido registrado con éxito', 'OK');
        this.router.navigate(['medicina-general/perfil-paciente']);
      }
    }, () => {
      this.showNotification('No se pudo registrar al paciente', 'CERRAR');
    });
  }

  chargeContactosEmegergencia(contactos: ContactoEmergencia[]): void {
    contactos.forEach((value) => {
      value.id = null;
    });
    this.contactosEmergencia = contactos;
    this.informacionContactoFormGroup
      .get('contactosEmergencia')
      .setValue(this.contactosEmergencia.length);
  }

  cargarReadOnlyInputs(idDispensario) {
    this.dispensarioSeleccionado = this.dispensarios.find(x => x.id === idDispensario);
  }

  searchCantones(event: any) {
    const provincia: Provincia = event.value;
    this.cantones = this.catalogoBannerService.getCantonesByProvincia(provincia.idProvincia);
  }

  searchCantonesContacto(event: any) {
    const provincia: Provincia = event.value;
    this.cantonesContacto = this.catalogoBannerService.getCantonesByProvincia(provincia.idProvincia);
  }

  searchParroquias(event: any) {
    const canton: Canton = event.value;
    this.parroquias = this.catalogoService.getParroquiasByCanton(canton.idCanton);
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  setRequiredParamToNumeroIdentificacion(idTipoIdentificacion: number) {
    const tipoIdentificacion = this.tiposIdentificacion.find(x => x.id === idTipoIdentificacion);
    this.numeroIdentifiacionIsRequired = tipoIdentificacion ? tipoIdentificacion.esRequerido : false;
    if (tipoIdentificacion && tipoIdentificacion.esRequerido) {
      this.informacionPersonalFormGroup.get('numeroIdentificacion').setValidators(
        [Validators.required]
      );
      this.informacionPersonalFormGroup.get('numeroIdentificacion').setAsyncValidators(
        [this.numeroArchivoValidator.validate.bind(this.numeroArchivoValidator)]
      );
      this.informacionPersonalFormGroup.get('numeroIdentificacion').updateValueAndValidity();
    } else {
      this.informacionPersonalFormGroup.get('numeroIdentificacion').clearValidators();
      this.informacionPersonalFormGroup.get('numeroIdentificacion').clearAsyncValidators();
      this.informacionPersonalFormGroup.get('numeroIdentificacion').updateValueAndValidity();
    }
  }

  searchNacionalidades(idEtnia: number) {
    this.informacionPersonalFormGroup.get('idNacionalidadEtnica').reset();
    this.nacionalidadesPueblos = this.catalogoService.getNacionalidadesByEtnia(idEtnia);
  }

  validatePais(nombrePais: string) {
    if (nombrePais === ECUADOR_COUNTRY_NAME) {
      this.informacionPersonalFormGroup.get('provinciaNacimiento').setValidators([Validators.required]);
      this.informacionPersonalFormGroup.get('provinciaNacimiento').updateValueAndValidity();
      this.provinciaNacimientoIsRequired = true;

      this.informacionPersonalFormGroup.get('cantonNacimiento').setValidators([Validators.required]);
      this.informacionPersonalFormGroup.get('cantonNacimiento').updateValueAndValidity();
      this.cantonNacimientoIsRequired = true;

      this.informacionPersonalFormGroup.patchValue({nacionalidad: ECUADOR_NACIONALIDAD_NAME});
      this.informacionPersonalFormGroup.get('nacionalidad').disable({ onlySelf: true });
    } else {
      this.informacionPersonalFormGroup.get('provinciaNacimiento').reset();
      this.informacionPersonalFormGroup.get('provinciaNacimiento').clearValidators();
      this.informacionPersonalFormGroup.get('provinciaNacimiento').updateValueAndValidity();
      this.provinciaNacimientoIsRequired = false;

      this.informacionPersonalFormGroup.get('cantonNacimiento').reset();
      this.informacionPersonalFormGroup.get('cantonNacimiento').clearValidators();
      this.informacionPersonalFormGroup.get('cantonNacimiento').updateValueAndValidity();
      this.cantonNacimientoIsRequired = false;

      this.informacionPersonalFormGroup.get('nacionalidad').reset();
      this.informacionPersonalFormGroup.get('nacionalidad').enable({onlySelf: true });
    }
  }

  seachPueblos(idNacionalidad: number) {
    this.informacionPersonalFormGroup.get('idPueblo').reset();
    this.pueblos = this.catalogoService.getPueblosByNacionalidad(idNacionalidad);
  }

  changeConsentimientoinformado(): void {
    const primerNombre = this.informacionPersonalFormGroup.get('primerNombre').value;
    const segundoNombre = this.informacionPersonalFormGroup.get('segundoNombre').value;
    const apellidoPaterno = this.informacionPersonalFormGroup.get('apellidoPaterno').value;
    const apellidoMaterno = this.informacionPersonalFormGroup.get('apellidoMaterno').value;
    const numeroIdentificacion = this.informacionPersonalFormGroup.get('numeroIdentificacion').value;
    const consentimientoInformadoAceptado = this.informacionPersonalFormGroup.get('consentimientoInformadoAceptado').value;
     if (
       (!primerNombre || primerNombre === '') ||
       (!segundoNombre || segundoNombre === '') ||
       (!apellidoPaterno || apellidoPaterno === '') ||
       (!apellidoMaterno || apellidoMaterno === '') ||
       (!numeroIdentificacion || numeroIdentificacion === '') ||
       consentimientoInformadoAceptado === true
     ) { return; }
    const nombres = `${primerNombre} ${segundoNombre}`;
    const apellidos = `${apellidoPaterno} ${apellidoMaterno}`;
    this.dialog.open(ConsentimientoModalComponent, {
      data: this.userMessages.acceptConInf,
      width: '500px',
      maxWidth: '100%',
      disableClose: true
    }).afterClosed()
      .subscribe(value => {
        if (value) {
          if (value === 'si') {
            this.informacionPersonalFormGroup.patchValue({consentimientoInformadoAceptado: true});
            this.reportDashboardService.getConsentimientoInformadoAceptado(
              nombres,
              apellidos,
              numeroIdentificacion,
              this.user.pidm
            ).subscribe(() => {},
                error => {
              this.snackbar.open(this.userMessages.getConInfDocFail, 'CERRAR', {
                duration: 5000
              });
            })
          } else {
            this.informacionPersonalFormGroup.patchValue({consentimientoInformadoAceptado: null});
            this.reportDashboardService.getConsentimientoInformadoNegado(this.user.pidm)
              .subscribe(() => {
                this.snackbar.open(this.userMessages.rejectedConInf, 'CERRAR', {
                  duration: 5000
                });
                this.router.navigate(['/']);
              }, error => {
                this.snackbar.open(this.userMessages.getConInfDocFail, 'CERRAR', {
                  duration: 5000
                });
              });
          }
        }
      });
  }
}
