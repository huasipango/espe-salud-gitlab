import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactosEmergenciaListComponent } from './contactos-emergencia-list.component';
import {MatMenuModule} from '@angular/material/menu';
import {MatPaginatorModule} from '@angular/material/paginator';
import {ExtendedModule, FlexModule} from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {ScrollbarModule} from '../../../../../../@vex/components/scrollbar/scrollbar.module';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatButtonModule} from '@angular/material/button';
import { ContactoEmergenciaModalComponent } from './contacto-emergencia-modal/contacto-emergencia-modal.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';



@NgModule({
  declarations: [ContactosEmergenciaListComponent],
  exports: [
    ContactosEmergenciaListComponent
  ],
  imports: [
    CommonModule,
    MatMenuModule,
    MatInputModule,
    MatPaginatorModule,
    ExtendedModule,
    MatIconModule,
    IconModule,
    ScrollbarModule,
    MatTableModule,
    FlexModule,
    MatSortModule,
    MatButtonModule,
    MatDialogModule
  ]
})
export class ContactosEmergenciaListModule { }
