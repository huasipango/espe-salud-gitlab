import {NgModule} from '@angular/core';
import {ContactoEmergenciaModalComponent} from './contacto-emergencia-modal.component';
import {CommonModule} from '@angular/common';
import {MatInputModule} from '@angular/material/input';
import {ExtendedModule, FlexModule} from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {MatButtonModule} from '@angular/material/button';
import {ReactiveFormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import {UppercaseModule} from 'src/app/shared/directives/uppercase/uppercase.module';
import {OnlyNumbersModule} from 'src/app/shared/directives/only-numbers/only-numbers.module';

@NgModule({
  declarations: [ContactoEmergenciaModalComponent],
  exports: [
    ContactoEmergenciaModalComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatIconModule,
    MatDialogModule,
    MatInputModule,
    IconModule,
    MatSelectModule,
    MatButtonModule,
    FlexModule,
    ExtendedModule,
    UppercaseModule,
    OnlyNumbersModule,
  ]
})
export class ContactoEmergenciaModalModule { }
