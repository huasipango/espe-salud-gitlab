import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import icClose from '@iconify/icons-ic/twotone-close';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPerson from '@iconify/icons-ic/twotone-person';
import icAddress from '@iconify/icons-fa-solid/location-arrow';
import icMobile from '@iconify/icons-fa-solid/mobile-alt';
import icPhone from '@iconify/icons-ic/twotone-phone';

import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Observable} from 'rxjs';
import {Parentesco} from 'src/app/core/models/catalogo-banner/parentesco.model';
import {ContactoEmergencia} from 'src/app/core/models/paciente/contacto-emergencia.model';
import {CatalogoBannerService} from 'src/app/core/services/catalogo-banner.service';

@Component({
  selector: 'vex-contacto-emergencia-modal',
  templateUrl: './contacto-emergencia-modal.component.html',
  styleUrls: ['./contacto-emergencia-modal.component.scss']
})
export class ContactoEmergenciaModalComponent implements OnInit {
  form: FormGroup;
  mode: 'create' | 'update' = 'create';

  icClose = icClose;
  icDelete = icDelete;
  icPerson = icPerson;
  icAddress = icAddress;
  icPhone = icPhone;
  icMobile = icMobile;
  perentescos: Observable<Parentesco[]>;

  constructor(
    private dialogRef: MatDialogRef<ContactoEmergenciaModalComponent>,
    @Inject(MAT_DIALOG_DATA) public defaults: any,
    private fb: FormBuilder,
    protected catalogoBannerService: CatalogoBannerService
  ) { }

  ngOnInit(): void {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as ContactoEmergencia;
    }
    this.perentescos = this.catalogoBannerService.getParentescos();
    this.form = this.fb.group({
      id: this.defaults.id || null,
      parentesco: this.defaults.parentesco || '',
      direccion: this.defaults.direccion || [null, Validators.required],
      nombreContacto: this.defaults.nombreContacto || [null, Validators.required],
      telefonoCelular: this.defaults.telefonoCelular || [null, Validators.required],
      telefonoConvencional: this.defaults.telefonoConvencional || ''
    });
  }

  save(){
    const contactoEmergencia = this.form.value;
    this.dialogRef.close(contactoEmergencia);
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }
}
