import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {ContactoEmergencia} from 'src/app/core/models/paciente/contacto-emergencia.model';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import icSearch from '@iconify/icons-ic/twotone-search';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDeleteForever from '@iconify/icons-ic/twotone-delete-forever';
import icPersonAdd from '@iconify/icons-ic/twotone-person-add';
import {MatDialog} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {stagger20ms} from 'src/@vex/animations/stagger.animation';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {MatSort} from '@angular/material/sort';
import {ContactoEmergenciaModalComponent} from './contacto-emergencia-modal/contacto-emergencia-modal.component';

@Component({
  selector: 'vex-contactos-emergencia-list',
  templateUrl: './contactos-emergencia-list.component.html',
  styleUrls: ['./contactos-emergencia-list.component.scss'],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ],
  animations: [
    stagger20ms,
    fadeInUp400ms,
    scaleFadeIn400ms
  ]
})
export class ContactosEmergenciaListComponent implements OnInit, AfterViewInit, OnDestroy {
  // Return a list with id incremental (1,2,3 ...) Must assign null when return in parent component
  @Input() contactosEmergencia: ContactoEmergencia[];
  @Output() returnContactosEmergencia = new EventEmitter();

  tableColumns: TableColumn<ContactoEmergencia>[] = [
    {
      label: 'NOMBRES',
      property: 'nombreContacto',
      type: 'text',
      cssClasses: ['font-medium'],
      visible: true
    },
    {
      label: 'PARENTESCO',
      property: 'parentesco',
      type: 'text',
      cssClasses: ['text-secondary'],
      visible: true
    },
    {
      label: 'DIRECCIÓN',
      property: 'direccion',
      type: 'text',
      cssClasses: ['text-secondary'],
      visible: true
    },
    {
      label: 'TELÉFONO CELULAR',
      property: 'telefonoCelular',
      type: 'text',
      cssClasses: ['text-secondary'],
      visible: true
    },
    {
      label: 'TELÉFONO CONVENCIONAL',
      property: 'telefonoConvencional',
      type: 'text',
      cssClasses: ['text-secondary'],
      visible: true
    },
    {
      label: '',
      property: 'menu',
      type: 'button',
      cssClasses: ['text-secondary', 'w-10'],
      visible: true
    }
  ];
  dataSource: MatTableDataSource<ContactoEmergencia> | null;

  icSearch = icSearch;
  icPersonAdd = icPersonAdd;
  icDeleteForever = icDeleteForever;
  icEdit = icEdit;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    if (!this.contactosEmergencia){
      this.contactosEmergencia = [];
    }
    this.dataSource = new MatTableDataSource();
    this.dataSource.data = this.contactosEmergencia;
  }

  createContactoEmergencia(){
    this.dialog
      .open(ContactoEmergenciaModalComponent, {width: '500px'})
      .afterClosed()
      .subscribe((contactoEmergencia: ContactoEmergencia) => {
        if (contactoEmergencia){
          this.contactosEmergencia.push(contactoEmergencia);
          this.updateIdsContactoEmergencia();
          this.dataSource.data = this.contactosEmergencia;
          this.returnDataToParentComponent();
        }
      });
  }

  updateIdsContactoEmergencia() {
    let id = 1;
    this.contactosEmergencia.forEach(contactoEmergencia => {
      contactoEmergencia.id = id;
      id++;
    });
  }

  updateContactoEmergencia(contactoEmergencia: ContactoEmergencia) {
    this.dialog.open(ContactoEmergenciaModalComponent, {
      width: '500px',
      data: contactoEmergencia
    }).afterClosed().subscribe(contactoModificado => {
      if (contactoModificado) {
        const index = this.contactosEmergencia.findIndex((contactoEmergenciaExistente) =>
          contactoEmergenciaExistente.id === contactoModificado.id);
        this.contactosEmergencia[index] = contactoModificado;
        this.dataSource.connect().next(this.contactosEmergencia);
        this.returnDataToParentComponent();
      }
    });
  }

  returnDataToParentComponent(){
    this.returnContactosEmergencia.emit(this.contactosEmergencia);
  }

  deleteContactoEmergencia(contactoEmergencia: ContactoEmergencia) {
    this.contactosEmergencia.forEach((item, index) => {
      if (item === contactoEmergencia) {
        this.contactosEmergencia.splice(index, 1);
      }
    });
    this.dataSource.connect().next(this.contactosEmergencia);
    this.returnDataToParentComponent();
  }

  trackByProperty<T>(column: TableColumn<T>) {
    return column.property;
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  get visibleColumns() {
    return this.tableColumns
      .filter(column => column.visible)
      .map(column => column.property);
  }
  ngOnDestroy() {
  }
}
