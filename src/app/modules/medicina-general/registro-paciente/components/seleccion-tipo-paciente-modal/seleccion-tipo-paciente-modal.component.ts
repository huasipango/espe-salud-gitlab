import { Component, OnInit } from '@angular/core';
import icInternal from '@iconify/icons-fa-solid/university';
import icExternal from '@iconify/icons-fa-solid/users';
import {Router} from '@angular/router';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'vex-seleccion-tipo-paciente-modal',
  templateUrl: './seleccion-tipo-paciente-modal.component.html',
  styleUrls: ['./seleccion-tipo-paciente-modal.component.scss']
})
export class SeleccionTipoPacienteModalComponent implements OnInit {

  icInternal = icInternal;
  icExternal = icExternal;

  constructor(
    private router: Router,
    private matDialogRef: MatDialogRef<SeleccionTipoPacienteModalComponent>) { }

  ngOnInit(): void {
  }

  gotToPacienteExternoForm(): void{
    this.router.navigate(['/medicina-general/registro-paciente/externo']);
    this.matDialogRef.close();
  }

  goToPacienteInternoForm(): void{
    this.router.navigate(['/medicina-general/registro-paciente/interno']);
    this.matDialogRef.close();
  }

}
