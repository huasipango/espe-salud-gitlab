import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import icClose from '@iconify/icons-ic/twotone-close';
import {enumSelector} from 'src/app/core/utils/enum-to-string-util';
import {LateralidadEnum} from 'src/app/core/enums/lateralidad.enum';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {Observable} from 'rxjs';
import {AuthService} from 'src/app/core/auth/auth.service';
import {PacienteService} from 'src/app/core/services/paciente/paciente.service';
import {LoadingService} from 'src/app/core/services/loading.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Router} from '@angular/router';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {Dispensario} from 'src/app/core/models/catalogo/dispensario.model';
import {PacienteInterno} from 'src/app/core/models/paciente/paciente-interno.model';
import {RelacionEspe} from 'src/app/core/models/catalogo/relacion-espe.model';
import {Asociacion} from 'src/app/core/models/catalogo/asociacion.model';
import {SeguroSalud} from 'src/app/core/models/catalogo/seguro-salud.model';
import {PacienteInternoForm} from 'src/app/modules/medicina-general/registro-paciente/pages/paciente-interno-form/paciente-interno-form.component';
import {ImagenUsuarioService} from 'src/app/core/services/imagen-usuario.service';
import {NivelInstruccion} from 'src/app/core/models/catalogo/nivel-instruccion.model';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {ApiError} from 'src/app/core/models/error/api-error.model';
import {GrupoPrioritario} from 'src/app/core/models/catalogo/grupo-prioritario.model';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {TipoIdentificacion} from 'src/app/core/models/catalogo/tipo-identificacion.model';

@Component({
  selector: 'vex-formulario-paciente-interno',
  templateUrl: './formulario-paciente-interno.component.html',
  styleUrls: ['./formulario-paciente-interno.component.scss']
})
export class FormularioPacienteInternoComponent implements OnInit {
  form: FormGroup;
  icClose = icClose;
  mode: 'create' | 'update' = 'create';
  lateralidades = enumSelector(LateralidadEnum);
  dispensarios: Observable<Dispensario[]>;
  relacionesEspe: Observable<RelacionEspe[]>;
  asosiaciones: Observable<Asociacion[]>;
  segurosSalud: Observable<SeguroSalud[]>;
  instrucciones: Observable<NivelInstruccion[]>;
  gruposPrioritarios: Observable<GrupoPrioritario[]>;
  tiposIdentifiacion: TipoIdentificacion[];
  errorMessages = FORM_ERROR_MESSAGES;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: PacienteInternoForm,
    private dialogRef: MatDialogRef<FormularioPacienteInternoComponent>,
    private fb: FormBuilder,
    private catalogoService: CatalogoService,
    private authService: AuthService,
    private pacienteService: PacienteService,
    private loadingService: LoadingService,
    private matSnackBar: MatSnackBar,
    private router: Router,
    private pacienteGlobalService: PacienteGlobalService,
    private imagenUsuarioService: ImagenUsuarioService,
  ) { }

  ngOnInit(): void {
    if (this.data.paciente) {
      this.mode = 'update';
    }else {
      this.gruposPrioritarios = this.catalogoService.getGruposPrioritarios();
      this.data.paciente = {} as Paciente;
    }
    this.dispensarios = this.catalogoService.getDispensarios();
    this.asosiaciones = this.catalogoService.getAsociaciones();
    this.segurosSalud = this.catalogoService.getSegurosSalud();
    this.relacionesEspe = this.catalogoService.getRelacionesEspe();
    this.instrucciones = this.catalogoService.getNivelesInstruccion();
    this.form = this.fb.group({
      idTipoIdentificacion: [this.data.paciente.idTipoIdentificacion, Validators.required],
      numeroIdentificacion: [
        {value: this.data.personaBanner.cedula, disabled: true},
      ],
      aceptaTransfucion: [this.data.paciente.aceptaTransfucion || false, Validators.required],
      pidm: [this.data.personaBanner.pidm || null, Validators.required],
      lateralidad: [this.data.paciente.lateralidad || null, Validators.required],
      idSeguroSalud: [this.data.paciente.seguroSalud?.id || null, Validators.required],
      idInstruccion: [this.data.paciente.instruccion?.id || null, Validators.required],
      idAsociacionAfiliada: [this.data.paciente.asociacionAfiliada?.id || null, Validators.required],
      idVinculadoEspe: [this.data.paciente.vinculadoEspe?.id || null, Validators.required],
      idDispensario: [this.data.paciente.idDispensario || null, Validators.required],
      esEmpleado: [this.data.paciente.esEmpleado || false, Validators.required],
      esEstudiante: [this.data.paciente.esEstudiante || false, Validators.required],
      gruposPrioritarios: [this.data.paciente.gruposPrioritarios || null],
      consentimientoInformadoAceptado: [this.data.consentimientoInformadoAceptado, Validators.required]
    });
    this.catalogoService.getTiposIdentificacion()
      .subscribe((tipos) => {
        this.tiposIdentifiacion = tipos;
        if (this.mode === 'create') {
          const type = tipos.find(t => t.porDefecto === true);
          this.form.patchValue({idTipoIdentificacion: type ? type.id : null});
        }
      });
    if (this.mode === 'create') {
      this.authService.saludUserData$
        .subscribe((saludUser) => {
          if (saludUser) {
            this.form.patchValue({idDispensario: saludUser.idDispensario});
          }
        });
    }
  }

  save() {
    if (this.mode === 'create'){
      this.createForm();
    } else if (this.mode === 'update'){
      this.updateForm();
    }
  }

  private createForm(){
    const paciente: PacienteInterno = this.form.getRawValue();
    if (!paciente.esEmpleado && !paciente.esEstudiante) {
      this.matSnackBar.open('Debe seleccionar si el paciente es estudiante o empleado', 'OK', {
        duration: 5000
      });
      return;
    } else if (paciente.esEmpleado && paciente.esEstudiante) {
      this.matSnackBar.open('Seleccione entre estudiante o empleado', 'OK', {
        duration: 5000
      });
      return;
    }
    this.loadingService.showLoaderUntilCompleted(
      this.pacienteService.createPacienteInterno(paciente)
    ).subscribe((response) => {
      if (response) {
        this.matSnackBar.open('El paciente ha sido registrado CORRECTAMENTE', 'OK', {
          duration: 5000
        });
        this.pacienteService.getPacienteByNumeroArchivo(response.numeroArchivo)
          .subscribe((registered) => {
            this.pacienteGlobalService.setPacienteGlobal(registered);
            this.pacienteGlobalService.setLastPacienteToLocalStorage(registered.numeroArchivo);
          });
        this.router.navigate(['medicina-general/perfil-paciente']);
        this.dialogRef.close();
      }
    }, error => {
      const apiError = error.error.apierror as ApiError;
      const message = apiError?.debugMessage || apiError?.message || error.error.error;
      this.matSnackBar.open(message || 'No se pudo registrar al paciente', 'CERRAR', {
        duration: 5000
      });
    });
  }

  private updateForm(){
    const paciente: PacienteInterno = this.form.getRawValue();
    this.loadingService.showLoaderUntilCompleted(
      this.pacienteService.updatePacienteInterno(paciente, this.data.paciente.id)
    ).subscribe((response) => {
      if (response) {
        this.matSnackBar.open('El paciente ha sido actualizado CORRECTAMENTE', 'OK', {
          duration: 5000
        });
        this.pacienteService.getPacienteByNumeroArchivo(response.numeroArchivo)
          .subscribe((registered) => {
            this.pacienteGlobalService.setPacienteGlobal(registered);
            this.pacienteGlobalService.setLastPacienteToLocalStorage(registered.numeroArchivo);
          });
        this.dialogRef.close();
      }
    }, () => {
      this.matSnackBar.open('No se ha podido actualizar al paciente', 'CERRAR', {
        duration: 5000
      });
    });
  }

  getUserImage(idBanner: string) {
    return this.imagenUsuarioService.getUserImage(idBanner);
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }
}
