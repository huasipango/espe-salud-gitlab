import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsentimientoModalComponent } from './consentimiento-modal.component';
import {MatDialogModule} from '@angular/material/dialog';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {MatTooltipModule} from '@angular/material/tooltip';



@NgModule({
  declarations: [
    ConsentimientoModalComponent
  ],
  exports: [
    ConsentimientoModalComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    IconModule,
    MatTooltipModule
  ]
})
export class ConsentimientoModalModule { }
