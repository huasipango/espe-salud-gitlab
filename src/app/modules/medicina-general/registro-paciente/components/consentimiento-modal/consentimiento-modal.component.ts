import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import icClose from '@iconify/icons-ic/twotone-close';
import icHelp from '@iconify/icons-ic/help-outline';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';

@Component({
  selector: 'vex-consentimiento-modal',
  templateUrl: './consentimiento-modal.component.html',
  styleUrls: ['./consentimiento-modal.component.scss']
})
export class ConsentimientoModalComponent implements OnInit {

  icClose = icClose;
  icHelp = icHelp;
  message: string;
  definitionConInf = USER_MESSAGES.definitionConInf;

  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: any,
    private dialogRef: MatDialogRef<ConsentimientoModalComponent>,
  ) { }

  ngOnInit() {
    this.message = this.defaults;
  }

  close(answer: string) {
    this.dialogRef.close(answer);
  }

}
