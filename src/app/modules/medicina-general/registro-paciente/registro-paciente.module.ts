import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RegistroPacienteRoutingModule} from './registro-paciente-routing.module';
import {SecondaryToolbarModule} from 'src/@vex/components/secondary-toolbar/secondary-toolbar.module';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {FlexModule, GridModule} from '@angular/flex-layout';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {MatStepperModule} from '@angular/material/stepper';
import {ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {ContactosEmergenciaListModule} from './components/contactos-emergencia-list/contactos-emergencia-list.module';
import {UppercaseModule} from 'src/app/shared/directives/uppercase/uppercase.module';
import {UniqueCedulaModule} from 'src/app/shared/directives/unique-cedula/unique-cedula.module';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import { SeleccionTipoPacienteModalComponent } from './components/seleccion-tipo-paciente-modal/seleccion-tipo-paciente-modal.component';
import { PacienteInternoFormComponent } from './pages/paciente-interno-form/paciente-interno-form.component';
import { PacienteExternoFormComponent } from './pages/paciente-externo-form/paciente-externo-form.component';
import { FormularioPacienteInternoComponent } from './components/formulario-paciente-interno/formulario-paciente-interno.component';
import {MatDialogModule} from '@angular/material/dialog';
import {OnlyNumbersModule} from 'src/app/shared/directives/only-numbers/only-numbers.module';
import {MatMomentDateModule} from '@angular/material-moment-adapter';

@NgModule({
  declarations: [
    SeleccionTipoPacienteModalComponent,
    PacienteInternoFormComponent,
    PacienteExternoFormComponent,
    FormularioPacienteInternoComponent
  ],
  imports: [
    CommonModule,
    RegistroPacienteRoutingModule,
    SecondaryToolbarModule,
    BreadcrumbsModule,
    MatInputModule,
    MatIconModule,
    IconModule,
    FlexModule,
    ContainerModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatCheckboxModule,
    MatButtonModule,
    MatTableModule,
    MatSortModule,
    ContactosEmergenciaListModule,
    UppercaseModule,
    UniqueCedulaModule,
    LoadingModule,
    GridModule,
    MatDialogModule,
    OnlyNumbersModule
  ]
})
export class RegistroPacienteModule { }
