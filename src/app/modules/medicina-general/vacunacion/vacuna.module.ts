import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {VacunaRoutingModule} from 'src/app/modules/medicina-general/vacunacion/vacuna-routing.module';
import { DosisVacunasBottomSheetComponent } from './components/dosis-vacunas-bottom-sheet/dosis-vacunas-bottom-sheet.component';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import {MatButtonModule} from '@angular/material/button';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';



@NgModule({
  declarations: [DosisVacunasBottomSheetComponent],
  imports: [
    CommonModule,
    VacunaRoutingModule,
    MatListModule,
    MatIconModule,
    IconModule,
    LoadingModule,
    MatButtonModule,
    FlexLayoutModule,
    MatBottomSheetModule
  ]
})
export class VacunaModule { }
