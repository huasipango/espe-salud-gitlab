import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {VexRoutes} from 'src/@vex/interfaces/vex-route.interface';
import {RoleGuard} from 'src/app/core/auth/role.guard';
import {RolEnum} from 'src/app/core/enums/rol.enum';


const routes: VexRoutes = [
  {
    path: '',
    canActivate: [RoleGuard],
    data: {
      roles: [RolEnum.ROLE_MEDICO, RolEnum.ROLE_ENFERMERO]
    },
    children: [
      {
        path: '',
        loadChildren: () => import('./pages/vacuna-list/vacuna-list.module').then(m => m.VacunaListModule)
      },
      {
        path: 'create',
        loadChildren: () => import('./pages/vacuna-create/vacuna-create.module').then(m => m.VacunaCreateModule)
      },
      {
        path: ':codigo',
        loadChildren: () => import('./pages/detalle-vacuna/detalle-vacuna.module').then(m => m.DetalleVacunaModule)
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VacunaRoutingModule {
}
