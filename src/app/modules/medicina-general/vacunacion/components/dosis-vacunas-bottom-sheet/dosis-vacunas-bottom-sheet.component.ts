import {Component, Inject, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {LoadingService} from 'src/app/core/services/loading.service';
import icClose from '@iconify/icons-ic/twotone-close';
import {DosisVacuna} from 'src/app/modules/medicina-general/vacunacion/models/dosis-vacuna.model';
import {DosisVacunaService} from 'src/app/modules/medicina-general/vacunacion/services/dosis-vacuna.service';
import {Vacuna} from 'src/app/modules/medicina-general/vacunacion/models/vacuna.model';

@Component({
  selector: 'vex-dosis-vacunas-bottom-sheet',
  templateUrl: './dosis-vacunas-bottom-sheet.component.html',
  styleUrls: ['./dosis-vacunas-bottom-sheet.component.scss']
})
export class DosisVacunasBottomSheetComponent implements OnInit {
  dosisVacuna: Observable<DosisVacuna[]>;
  icClose = icClose;
  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) private vacuna: Vacuna,
    private _bottomSheetRef: MatBottomSheetRef<DosisVacunasBottomSheetComponent>,
    private loadingService: LoadingService,
    private dosisVacunaService: DosisVacunaService
  ) { }

  ngOnInit(): void {
    this.dosisVacuna = this.loadingService.showLoaderUntilCompleted(
      this.dosisVacunaService.getDosiVacunas(this.vacuna.id)
    );
  }

  close(): void {
    this._bottomSheetRef.dismiss();
  }
}
