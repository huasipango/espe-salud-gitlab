import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {stagger20ms} from 'src/@vex/animations/stagger.animation';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {DosisVacuna} from 'src/app/modules/medicina-general/vacunacion/models/dosis-vacuna.model';
import icSearch from '@iconify/icons-ic/twotone-search';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDeleteForever from '@iconify/icons-ic/twotone-delete-forever';
import icAdd from '@iconify/icons-ic/twotone-add';
import {DosisVacunaModalComponent} from 'src/app/modules/medicina-general/vacunacion/components/dosis-vacuna-modal/dosis-vacuna-modal.component';
import {Dosis} from 'src/app/core/models/catalogo/dosis.model';
import {getShortDate} from 'src/app/core/utils/date-utils';

@Component({
  selector: 'vex-dosis-vacuna-table',
  templateUrl: './dosis-vacuna-table.component.html',
  styleUrls: ['./dosis-vacuna-table.component.scss'],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ],
  animations: [
    stagger20ms,
    fadeInUp400ms,
    scaleFadeIn400ms
  ]
})
export class DosisVacunaTableComponent implements OnInit, AfterViewInit {

  @Input() dosisVacunas: DosisVacuna[];
  @Output() returnDiagnosticos = new EventEmitter();

  tableColumns: TableColumn<DosisVacuna>[] = [
    {label: 'Fecha', property: 'fecha', type: 'date', visible: true},
    {label: 'fechaSiguienteDosis', property: 'fechaSiguienteDosis', type: 'date', visible: true},
    {label: 'establecimiento', property: 'establecimiento', type: 'text', visible: true},
    {label: 'Responsable', property: 'responsable', type: 'text', visible: true},
    {label: 'Dosis', property: 'dosis', type: 'object', visible: true},
    {label: 'Acciones', property: 'menu', type: 'button', visible: true}
  ];

  dataSource: MatTableDataSource<DosisVacuna> | null;
  icSearch = icSearch;
  icAdd = icAdd;
  icDeleteForever = icDeleteForever;
  icEdit = icEdit;

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
    if (!this.dosisVacunas) {
      this.dosisVacunas = [];
    }
    this.dataSource = new MatTableDataSource();
    this.dataSource.data = this.dosisVacunas;
  }

  createDosisVacuna() {
    this.dialog
      .open(DosisVacunaModalComponent, {
        width: '700px',
        maxWidth: '100%'
      })
      .afterClosed()
      .subscribe((dosisVacuna: DosisVacuna) => {
        if (dosisVacuna) {
          this.dosisVacunas.push(dosisVacuna);
          this.updateIdDosisVacuna();
          this.dataSource.data = this.dosisVacunas;
          this.returnDataToParentComponent();
        }
      });
  }

  updateDosisVacuna(dosisVacuna: DosisVacuna) {
    this.dialog.open(DosisVacunaModalComponent, {
      data: dosisVacuna,
      width: '700px',
      maxWidth: '100%'
    }).afterClosed().subscribe(dosisVacunaModificado => {
      if (dosisVacunaModificado) {
        const index = this.dosisVacunas.findIndex((diagnosticoExistente) =>
          diagnosticoExistente.id === dosisVacunaModificado.id);
        this.dosisVacunas[index] = dosisVacunaModificado;
        this.dataSource.connect().next(this.dosisVacunas);
        this.returnDataToParentComponent();
      }
    });
  }

  deleteDosisVacuna(dosisVacuna: DosisVacuna) {
    this.dosisVacunas.forEach((item, index) => {
      if (item === dosisVacuna) {
        this.dosisVacunas.splice(index, 1);
      }
    });
    this.dataSource.connect().next(this.dosisVacunas);
    this.returnDataToParentComponent();
  }

  updateIdDosisVacuna() {
    let id = 1;
    this.dosisVacunas.forEach(dosisVacuna => {
      dosisVacuna.id = id;
      id++;
    });
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  trackByProperty<T>(column: TableColumn<T>) {
    return column.property;
  }

  get visibleColumns() {
    return this.tableColumns
      .filter(column => column.visible)
      .map(column => column.property);
  }

  returnDataToParentComponent() {
    this.returnDiagnosticos.emit(this.dosisVacunas);
  }

  getDosis(dosis: Dosis): string {
    return dosis ? dosis.nombre : '';
  }

  getFormattedDate(date: Date) {
    return getShortDate(date);
  }
}
