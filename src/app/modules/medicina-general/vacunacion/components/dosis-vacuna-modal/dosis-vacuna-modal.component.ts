import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Dosis} from 'src/app/core/models/catalogo/dosis.model';
import icPills from '@iconify/icons-fa-solid/pills';
import icClose from '@iconify/icons-ic/twotone-close';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {DosisVacuna} from 'src/app/modules/medicina-general/vacunacion/models/dosis-vacuna.model';
import {VacunaSharedService} from 'src/app/modules/medicina-general/vacunacion/services/vacuna-shared.service';
import {TipoVacuna} from 'src/app/core/models/catalogo/tipo-vacunas.model';

@Component({
  selector: 'vex-dosis-vacuna-modal',
  templateUrl: './dosis-vacuna-modal.component.html',
  styleUrls: ['./dosis-vacuna-modal.component.scss']
})
export class DosisVacunaModalComponent implements OnInit {

  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  filteredDosis$: Observable<Dosis[]>;
  icClose = icClose;
  tipoVacuna$: Observable<TipoVacuna>;
  tipoVacuna: TipoVacuna;
  icPills = icPills;
  icArrowDropDown = icArrowDropDown;
  errorMessages = FORM_ERROR_MESSAGES;
  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: DosisVacuna,
    private dialogRef: MatDialogRef<DosisVacunaModalComponent>,
    private fb: FormBuilder,
    private catalogoService: CatalogoService,
    private vacunaSharedService: VacunaSharedService
  ) { }

  ngOnInit(): void {
    this.tipoVacuna$ = this.vacunaSharedService.sharingTipoVacunaObservable;
    if (this.defaults) {
      this.mode = 'update';
    }else {
      this.defaults = {} as DosisVacuna;
    }
    this.tipoVacuna$
      .subscribe((tipoVacuna: TipoVacuna) => {
        this.tipoVacuna = tipoVacuna;
      });
    this.filteredDosis$ = this.catalogoService.getDosisByIdTipoVacuna(this.tipoVacuna.id);
    this.inicializerForm();
  }

  inicializerForm(): void{
    this.form = this.fb.group({
      id: this.defaults.id || null,
      fecha: [this.defaults.fecha || null, Validators.required],
      fechaSiguienteDosis: [this.defaults.fechaSiguienteDosis || null, Validators.required],
      responsable: [this.defaults.responsable || null, Validators.required],
      establecimiento: [this.defaults.establecimiento || null, Validators.required],
      idDosis: [this.defaults.idDosis || null, Validators.required],
    });
  }

  save(){
    const dosisVacuna = this.form.getRawValue();
    this.catalogoService.getDosisById(dosisVacuna.idDosis)
      .subscribe((dosis: any) => {
      if (dosis){
        dosisVacuna.dosis = dosis;
      }
    });
    this.dialogRef.close(dosisVacuna);
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

}





