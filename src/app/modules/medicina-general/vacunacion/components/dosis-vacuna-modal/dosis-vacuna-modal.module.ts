import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DosisVacunaModalComponent } from './dosis-vacuna-modal.component';
import {FlexModule} from '@angular/flex-layout';
import {ReactiveFormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {IconModule} from '@visurel/iconify-angular';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';

@NgModule({
  declarations: [DosisVacunaModalComponent],
  imports: [
    CommonModule,
    FlexModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    IconModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule
  ],
  exports: [DosisVacunaModalComponent]
})
export class DosisVacunaModalModule { }
