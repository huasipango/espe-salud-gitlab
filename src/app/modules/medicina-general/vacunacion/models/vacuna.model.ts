import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {TipoVacuna} from 'src/app/core/models/catalogo/tipo-vacunas.model';
import {DosisVacuna} from 'src/app/modules/medicina-general/vacunacion/models/dosis-vacuna.model';

export class Vacuna {
  id: number;
  lote: string;
  observacion: string;
  idTipoVacuna: number;
  tipoVacuna: TipoVacuna;
  idPaciente: number;
  paciente: Paciente;
  dosisVacunas: DosisVacuna[];
}
