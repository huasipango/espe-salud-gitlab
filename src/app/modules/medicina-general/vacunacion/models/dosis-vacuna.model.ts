import {Dosis} from 'src/app/core/models/catalogo/dosis.model';

export class DosisVacuna{
  id: number;
  fecha: Date;
  fechaSiguienteDosis: Date;
  responsable: string;
  establecimiento: string;
  idDosis: number;
  dosis: Dosis;
}
