import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {VexRoutes} from 'src/@vex/interfaces/vex-route.interface';
import {VacunaListComponent} from 'src/app/modules/medicina-general/vacunacion/pages/vacuna-list/vacuna-list.component';

const routes: VexRoutes = [
  {
    path: '',
    component: VacunaListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VacunaListRoutingModule {
}
