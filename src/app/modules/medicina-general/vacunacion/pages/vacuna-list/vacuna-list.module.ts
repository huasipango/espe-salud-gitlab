import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VacunaListComponent } from './vacuna-list.component';
import {VacunaListRoutingModule} from 'src/app/modules/medicina-general/vacunacion/pages/vacuna-list/vacuna-list-routing.module';
import {ExtendedModule, FlexModule} from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatTableModule} from '@angular/material/table';
import {MatMenuModule} from '@angular/material/menu';
import {MatSortModule} from '@angular/material/sort';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatPaginatorModule} from '@angular/material/paginator';
import {PageLayoutModule} from 'src/@vex/components/page-layout/page-layout.module';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {EmptyPacienteModule} from 'src/app/core/components/empty-paciente/empty-paciente.module';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {MatListModule} from '@angular/material/list';
import {StripHtmlModule} from 'src/@vex/pipes/strip-html/strip-html.module';



@NgModule({
  declarations: [VacunaListComponent],
  imports: [
    CommonModule,
    VacunaListRoutingModule,
    FlexModule,
    ExtendedModule,
    MatIconModule,
    IconModule,
    MatButtonModule,
    MatTooltipModule,
    ReactiveFormsModule,
    MatTableModule,
    MatMenuModule,
    MatSortModule,
    MatCheckboxModule,
    MatPaginatorModule,
    FormsModule,
    PageLayoutModule,
    BreadcrumbsModule,
    EmptyPacienteModule,
    ContainerModule,
    MatListModule,
    StripHtmlModule,
  ]
})
export class VacunaListModule { }
