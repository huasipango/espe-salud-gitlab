import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {UntilDestroy} from '@ngneat/until-destroy';
import {Observable} from 'rxjs';
import {Evolucion} from 'src/app/modules/medicina-general/evolucion/models/evolucion.model';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {FormControl} from '@angular/forms';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {TipoConsultaColor} from 'src/app/core/enums/tipo-consulta.enum';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icEye from '@iconify/icons-fa-solid/eye';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icMedKit from '@iconify/icons-fa-solid/medkit';
import icDownload from '@iconify/icons-ic/cloud-download';
import icPrint from '@iconify/icons-ic/print';
import icVaccine from '@iconify/icons-ic/outline-colorize';
import {trackById} from 'src/@vex/utils/track-by';
import {Vacuna} from 'src/app/modules/medicina-general/vacunacion/models/vacuna.model';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {LoadingService} from 'src/app/core/services/loading.service';
import {PopoverService} from 'src/@vex/components/popover/popover.service';
import {ImagenUsuarioService} from 'src/app/core/services/imagen-usuario.service';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {VacunaService} from 'src/app/modules/medicina-general/vacunacion/services/vacuna.service';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {getShortDateTime} from 'src/app/core/utils/date-utils';
import {ReposoModalComponent} from 'src/app/modules/medicina-general/evolucion/components/reposo-modal/reposo-modal.component';
import {DosisVacunasBottomSheetComponent} from 'src/app/modules/medicina-general/vacunacion/components/dosis-vacunas-bottom-sheet/dosis-vacunas-bottom-sheet.component';
import {DosisVacunaService} from 'src/app/modules/medicina-general/vacunacion/services/dosis-vacuna.service';
import {VacunaReportService} from "src/app/modules/medicina-general/vacunacion/services/vacuna-report.service";

@UntilDestroy()
@Component({
  selector: 'vex-vacuna-list',
  templateUrl: './vacuna-list.component.html',
  styleUrls: ['./vacuna-list.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms,
    scaleIn400ms,
    fadeInRight400ms,
    scaleFadeIn400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class VacunaListComponent implements OnInit, AfterViewInit {

  vacunas$: Observable<Vacuna[]>;
  vacunas: Vacuna[] = [];
  countVacuna: number;
  trackById = trackById;
  messages = USER_MESSAGES;

  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  searchCtrl = new FormControl();

  icVaccine = icVaccine;
  icEdit = icEdit;
  icSearch = icSearch;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icMoreHoriz = icMoreHoriz;
  icEye = icEye;
  icMedKit = icMedKit;
  icDownload = icDownload;
  icPrint = icPrint;

  pacienteActual: Paciente;
  tipoConsultaColors = TipoConsultaColor;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
    private vacunaService: VacunaService,
    private dosisVacunaService: DosisVacunaService,
    private vacunaReportService: VacunaReportService,
    private pacienteGlobalService: PacienteGlobalService,
    private loadingService: LoadingService,
    private popoverService: PopoverService,
    private imagenUsuarioService: ImagenUsuarioService,
    private _bottomSheet: MatBottomSheet,
  ) { }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getVacunas();
        }
      });
  }

  getVacunas(): void {
    this.vacunas$ = this.vacunaService.getVacunas(this.pacienteActual.id);
  }

  openDeleteDialog(vacuna: Vacuna) {
    this.dialog.open(DeleteModalComponent, {
      data: this.messages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') {
        this.deleteVacuna(vacuna);
      }
    });
  }

  createVacuna() {
    this.router.navigate(['/medicina-general/vacunacion/create']);
  }

  showDosisVacuna(vacuna: Vacuna): void {
    this._bottomSheet
      .open(DosisVacunasBottomSheetComponent, {
        data: vacuna
      }).afterDismissed()
      .subscribe();
  }

  deleteVacuna(vacuna: Vacuna) {
    this.vacunaService.deleteVacuna(vacuna.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification('Registro eliminado EXITOSAMENTE', 'CERRAR');
          this.vacunas.splice(
            this.vacunas.findIndex((existing) =>
              existing.id === vacuna.id), 1
          );
          this.getVacunas();
        } else {
          this.showNotification('No se pudo eliminar el registro', 'CERRAR');
        }
      });
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  getDatetime(date: Date): string {
    return getShortDateTime(date);
  }

  updateVacuna(vacuna: Vacuna) {
    const codigo = vacuna.id;
    this.router.navigate(['./', codigo], {relativeTo: this.route});
  }

  downloadCertificadoVacuna(vacuna: Vacuna) {
    this.loadingService.showLoaderUntilCompleted(
      this.vacunaReportService.getCertificadoVacuna(vacuna.id)
    ).subscribe();
  }

  getTipoConsultaColors(tipoConsulta: string) {
    const foundType = this.tipoConsultaColors.find(t => t.type === tipoConsulta);
    if (foundType) {
      return foundType.classes;
    }
    return [];
  }

  getUserImage(idBanner: string): string {
    return this.imagenUsuarioService.getUserImage(idBanner);
  }

  ngAfterViewInit(): void {
  }

}
