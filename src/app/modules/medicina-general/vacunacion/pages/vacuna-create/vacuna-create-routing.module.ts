import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {VexRoutes} from 'src/@vex/interfaces/vex-route.interface';
import {VacunaCreateComponent} from 'src/app/modules/medicina-general/vacunacion/pages/vacuna-create/vacuna-create.component';

const routes: VexRoutes = [
  {
    path: '',
    component: VacunaCreateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VacunaCreateRoutingModule {
}
