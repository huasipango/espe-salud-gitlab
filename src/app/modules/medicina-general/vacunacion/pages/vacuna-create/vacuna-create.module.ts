import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VacunaCreateComponent } from './vacuna-create.component';
import {VacunaCreateRoutingModule} from 'src/app/modules/medicina-general/vacunacion/pages/vacuna-create/vacuna-create-routing.module';
import {PageLayoutModule} from 'src/@vex/components/page-layout/page-layout.module';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {FlexModule} from '@angular/flex-layout';
import {MatStepperModule} from '@angular/material/stepper';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {SecondaryToolbarModule} from 'src/@vex/components/secondary-toolbar/secondary-toolbar.module';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {EmptyPacienteModule} from 'src/app/core/components/empty-paciente/empty-paciente.module';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {MatTabsModule} from '@angular/material/tabs';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import {DosisVacunaTableModule} from 'src/app/modules/medicina-general/vacunacion/components/dosis-vacuna-table/dosis-vacuna-table.module';



@NgModule({
  declarations: [VacunaCreateComponent],
  imports: [
    CommonModule,
    VacunaCreateRoutingModule,
    PageLayoutModule,
    BreadcrumbsModule,
    FlexModule,
    MatStepperModule,
    MatIconModule,
    IconModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDatepickerModule,
    MatButtonModule,
    MatTooltipModule,
    SecondaryToolbarModule,
    ContainerModule,
    EmptyPacienteModule,
    MatCheckboxModule,
    MatDividerModule,
    MatListModule,
    MatTabsModule,
    DosisVacunaTableModule,
    LoadingModule
  ]
})
export class VacunaCreateModule { }
