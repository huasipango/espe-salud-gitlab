import { Component, OnInit } from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {Observable} from 'rxjs';
import {DosisVacuna} from 'src/app/modules/medicina-general/vacunacion/models/dosis-vacuna.model';
import {TipoVacuna} from 'src/app/core/models/catalogo/tipo-vacunas.model';
import {FormBuilder, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {LoadingService} from 'src/app/core/services/loading.service';
import {VacunaService} from 'src/app/modules/medicina-general/vacunacion/services/vacuna.service';
import {VacunaSharedService} from 'src/app/modules/medicina-general/vacunacion/services/vacuna-shared.service';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icClose from '@iconify/icons-ic/twotone-close';
import icPrint from '@iconify/icons-ic/twotone-print';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icPerson from '@iconify/icons-ic/twotone-person';
import icLocationCity from '@iconify/icons-ic/twotone-location-city';
import icCheck from '@iconify/icons-ic/check';
import icDescription from '@iconify/icons-ic/twotone-description';
import icDoneAll from '@iconify/icons-ic/twotone-done-all';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';
import icAdd from '@iconify/icons-ic/twotone-add';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icSearch from '@iconify/icons-ic/search';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {Vacuna} from 'src/app/modules/medicina-general/vacunacion/models/vacuna.model';
@Component({
  selector: 'vex-vacuna-create',
  templateUrl: './vacuna-create.component.html',
  styleUrls: ['./vacuna-create.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms,
    scaleFadeIn400ms,
    scaleIn400ms,
    fadeInRight400ms
  ],
})
export class VacunaCreateComponent implements OnInit {

  icMoreVert = icMoreVert;
  icClose = icClose;
  icPrint = icPrint;
  icDelete = icDelete;
  icCheck = icCheck;
  icPerson = icPerson;
  icLocationCity = icLocationCity;
  icPhone = icPhone;
  icDoneAll = icDoneAll;
  icDescription = icDescription;
  icArrowDropDown = icArrowDropDown;
  icAdd = icAdd;
  icMoreHoriz = icMoreHoriz;
  icSearch = icSearch;
  errorMessages = FORM_ERROR_MESSAGES;
  pacienteActual: Paciente;
  dosisVacuna: DosisVacuna[] = [];
  tipo: TipoVacuna;
  tipoVacuna: Observable<TipoVacuna[]>;
  step1FormGroup = this.fb.group({
    idPaciente: [null, Validators.required],
    idTipoVacuna: [null, Validators.required],
    lote: [null],
    observacion: [null],
  });
  step2FormGroup = this.fb.group({
    dosisVacunas: [this.dosisVacuna]
  });
  constructor(
    private  snackbar: MatSnackBar, private fb: FormBuilder,
    private dialog: MatDialog,
    private pacienteGlobalService: PacienteGlobalService,
    private router: Router,
    private route: ActivatedRoute,
    protected catalogoService: CatalogoService,
    private vacunaService: VacunaService,
    private _bottomSheet: MatBottomSheet,
    private loadingService: LoadingService,
    private vacunaSharedService: VacunaSharedService,
  ) { }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.step1FormGroup.patchValue({idPaciente: paciente.id});
        }
      });
    this.tipoVacuna = this.catalogoService.getTipoVacuna();
  }

  sharedTipoVacuna(){
    const tipo = this.step1FormGroup.get('idTipoVacuna').value;
    this.catalogoService.getTipoVacunaById(tipo)
      .subscribe((tipoVacuna: TipoVacuna) => {
        this.vacunaSharedService.setTipoVacuna(tipoVacuna);
        // console.log(this.vacunaSharedService.sharingTipoVacunaObservable);
      });
  }

  createVacuna(): void {
    const vacuna: Vacuna = this.step1FormGroup.value;
    vacuna.dosisVacunas = this.dosisVacuna;
    this.loadingService.showLoaderUntilCompleted(
      this.vacunaService.createVacuna(vacuna)
    ).subscribe((data) => {
      if (data != null) {
        this.vacunaSharedService.setVacuna(data);
        this.showNotification('Registro creado EXITOSAMENTE', 'OK');
        this.router.navigate(['../'], {relativeTo: this.route});
      }
    }, () => {
      this.showNotification('ERROR al crear el registro', 'CERRAR');
    });
  }

  chargeDosisVacunas(dosisVacuna: DosisVacuna[]) {
    dosisVacuna.forEach((value) => {
      value.id = null;
    });
    this.dosisVacuna = dosisVacuna;
    this.step2FormGroup
      .get('dosisVacunas')
      .setValue(this.dosisVacuna);
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

}
