import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import {VexRoutes} from 'src/@vex/interfaces/vex-route.interface';
import {DetalleVacunaComponent} from 'src/app/modules/medicina-general/vacunacion/pages/detalle-vacuna/detalle-vacuna.component';

const routes: VexRoutes = [
  {
    path: '',
    component: DetalleVacunaComponent,
    data: {
      toolbarShadowEnabled: false,
      scrollDisabled: true
    },
    children: [
      {
        path: '',
        loadChildren: () => import('../informacion-vacuna/informacion-vacuna.module').then(m => m.InformacionVacunaModule)
      },
      {
        path: 'dosis-vacuna',
        loadChildren: () => import('../dosis-vacuna/dosis-vacuna.module').then(m => m.DosisVacunaModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetalleVacunaRoutingModule {
}
