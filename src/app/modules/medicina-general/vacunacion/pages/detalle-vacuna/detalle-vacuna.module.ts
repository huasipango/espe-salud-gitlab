import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetalleVacunaComponent } from './detalle-vacuna.component';
import {DetalleVacunaRoutingModule} from 'src/app/modules/medicina-general/vacunacion/pages/detalle-vacuna/detalle-vacuna-routing.module';
import {PageLayoutModule} from 'src/@vex/components/page-layout/page-layout.module';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {MatTabsModule} from '@angular/material/tabs';
import {IconModule} from '@visurel/iconify-angular';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {ExtendedModule, FlexModule} from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';



@NgModule({
  declarations: [DetalleVacunaComponent],
  imports: [
    CommonModule,
    DetalleVacunaRoutingModule,
    PageLayoutModule,
    BreadcrumbsModule,
    MatTabsModule,
    IconModule,
    ContainerModule,
    FlexModule,
    MatIconModule,
    ExtendedModule,
    MatButtonModule,
    MatTooltipModule,
  ],
  exports: [DetalleVacunaComponent]
})
export class DetalleVacunaModule { }
