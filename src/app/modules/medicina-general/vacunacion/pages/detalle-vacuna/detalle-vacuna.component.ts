import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Link} from 'src/@vex/interfaces/link.interface';
import {ActivatedRoute, Router} from '@angular/router';
import icAdd from '@iconify/icons-ic/twotone-add';
import icBack from '@iconify/icons-ic/arrow-back';
import icCloudDownload from '@iconify/icons-ic/twotone-cloud-download';
import {Vacuna} from 'src/app/modules/medicina-general/vacunacion/models/vacuna.model';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';


@Component({
  selector: 'vex-detalle-vacuna',
  templateUrl: './detalle-vacuna.component.html',
  styleUrls: ['./detalle-vacuna.component.scss'],
  animations: [
    scaleIn400ms,
    fadeInRight400ms,
    stagger40ms,
    fadeInUp400ms,
    scaleFadeIn400ms
  ]
})
export class DetalleVacunaComponent implements OnInit {

  icCloudDownload = icCloudDownload;
  icAdd = icAdd;
  icBack = icBack;

  vacuna: Vacuna;
  vacunaCodigo = '';
  layoutCtrl = new FormControl('full');
  links: Link[] = [
    {
      label: 'VACUNA',
      route: './',
      routerLinkActiveOptions: {exact: true}
    },
    {
      label: 'DOSIS APLICADAS',
      route: './dosis-vacuna',
    }
  ];

  constructor(
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(( params) => {
      const codigo = params.get('codigo');
      this.vacunaCodigo = codigo;
    });
  }

  crearNuevaVacuna(): void {
    this.router.navigate(['../create'], {relativeTo: this.route});
  }

  navigateBack() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

}
