import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// tslint:disable-next-line:max-line-length
import {InformacionVacunaComponent} from 'src/app/modules/medicina-general/vacunacion/pages/informacion-vacuna/informacion-vacuna.component';


const routes: Routes = [
  {
    path: '',
    component: InformacionVacunaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InformacionVacunaRoutingModule {
}
