import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icClose from '@iconify/icons-ic/twotone-close';
import icPrint from '@iconify/icons-ic/twotone-print';
import icDownload from '@iconify/icons-ic/twotone-cloud-download';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icPerson from '@iconify/icons-ic/twotone-person';
import icLocationCity from '@iconify/icons-ic/twotone-location-city';
import icCheck from '@iconify/icons-ic/check';
import icSave from '@iconify/icons-fa-solid/save';
import icEdit from '@iconify/icons-fa-solid/edit';
import icMedKit from '@iconify/icons-fa-solid/medkit';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {LoadingService} from 'src/app/core/services/loading.service';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {VacunaService} from 'src/app/modules/medicina-general/vacunacion/services/vacuna.service';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {Observable} from 'rxjs';
import {switchMap} from 'rxjs/operators';
import {TipoVacuna} from 'src/app/core/models/catalogo/tipo-vacunas.model';
import {Vacuna} from 'src/app/modules/medicina-general/vacunacion/models/vacuna.model';
import {VacunaSharedService} from "src/app/modules/medicina-general/vacunacion/services/vacuna-shared.service";
@Component({
  selector: 'vex-informacion-vacuna',
  templateUrl: './informacion-vacuna.component.html',
  styleUrls: ['./informacion-vacuna.component.scss'],
  animations: [
    scaleIn400ms,
    fadeInRight400ms
  ]
})
export class InformacionVacunaComponent implements OnInit {


  icMoreHoriz = icMoreHoriz;
  icMoreVert = icMoreVert;
  icClose = icClose;
  icSave = icSave;
  icPrint = icPrint;
  icDownload = icDownload;
  icDelete = icDelete;
  icPerson = icPerson;
  icLocationCity = icLocationCity;
  icPhone = icPhone;
  icCheck = icCheck;
  icEdit = icEdit;
  icMedKit = icMedKit;
  vacuna: Vacuna;
  pacienteActual: Paciente;
  vacunaForm: FormGroup;
  maxDate = new Date();
  tipoVacuna: Observable<TipoVacuna[]>;
  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    private pacienteGlobalService: PacienteGlobalService,
    private cd: ChangeDetectorRef,
    private snackbar: MatSnackBar,
    private vacunaService: VacunaService,
    private loadingService: LoadingService,
    private catalogoService: CatalogoService,
    private vacunaSharedService: VacunaSharedService
  ) { }
  ngOnInit() {
    this.tipoVacuna = this.catalogoService.getTipoVacuna();
    this.route.paramMap.pipe(
      switchMap(params => {
        const codigo = params.get('codigo');
        return this.vacunaService.getVacuna(Number(codigo));
      })
    ).subscribe((vacuna) => {
      if (vacuna) {
        this.vacuna = vacuna;
        this.vacunaSharedService.setTipoVacuna(vacuna.tipoVacuna);
        this.cargarFormularioVacuna();
      }
    });
  }

  cargarFormularioVacuna() {
    this.vacunaForm = this.fb.group({
      id: this.vacuna.id,
      idTipoVacuna: [this.vacuna.idTipoVacuna, Validators.required],
      lote: [this.vacuna.lote],
      observacion: [this.vacuna.observacion || null],
      idPaciente: this.vacuna.idPaciente,
    });
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
        }
      });
  }

  updateVacuna() {
    const vacuna = this.vacunaForm.getRawValue();
    this.loadingService.showLoaderUntilCompleted(
      this.vacunaService.updateVacuna(vacuna.id, vacuna)
    ).subscribe((data: Vacuna) => {
      console.log(data);
      if (data) {
        this.vacuna = data;
        this.showNotification('Vacuna actualizada EXITOSAMENTE', 'CERRAR');
      }
    }, error => {
      this.showNotification('ERROR al actualizar', 'CERRAR');
    });
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000,
    });
  }

  sharedTipoVacuna(){
    const tipo = this.vacunaForm.get('idTipoVacuna').value;
    this.catalogoService.getTipoVacunaById(tipo)
      .subscribe((tipoVacuna: TipoVacuna) => {
        this.vacunaSharedService.setTipoVacuna(tipoVacuna);
      });
  }
}
