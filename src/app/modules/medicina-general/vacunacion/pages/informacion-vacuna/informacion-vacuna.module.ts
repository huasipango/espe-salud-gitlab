import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InformacionVacunaComponent } from './informacion-vacuna.component';
import {InformacionVacunaRoutingModule} from 'src/app/modules/medicina-general/vacunacion/pages/informacion-vacuna/informacion-vacuna-routing.module';
import {PageLayoutModule} from 'src/@vex/components/page-layout/page-layout.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatTableModule} from '@angular/material/table';
import {MatDividerModule} from '@angular/material/divider';
import {MatSortModule} from '@angular/material/sort';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {WidgetLargeChartModule} from 'src/@vex/components/widgets/widget-large-chart/widget-large-chart.module';
import {WidgetTableModule} from 'src/@vex/components/widgets/widget-table/widget-table.module';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {IconModule} from '@visurel/iconify-angular';
import {MatTooltipModule} from '@angular/material/tooltip';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatExpansionModule} from '@angular/material/expansion';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';



@NgModule({
  declarations: [InformacionVacunaComponent],
  imports: [
    CommonModule,
    InformacionVacunaRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule,
    MatDividerModule,
    MatSortModule,
    MatCheckboxModule,
    MatIconModule,
    ReactiveFormsModule,
    WidgetLargeChartModule,
    FormsModule,
    WidgetTableModule,
    MatInputModule,
    MatButtonModule,
    MatMenuModule,
    IconModule,
    FormsModule,
    MatTooltipModule,
    ReactiveFormsModule,
    ContainerModule,
    MatSelectModule,
    MatButtonToggleModule,
    MatSnackBarModule,
    MatExpansionModule,
    LoadingModule,
  ]
})
export class InformacionVacunaModule { }
