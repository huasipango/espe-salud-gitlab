import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DosisVacunaComponent } from './dosis-vacuna.component';
import {DosisVacunaRoutingModule} from 'src/app/modules/medicina-general/vacunacion/pages/dosis-vacuna/dosis-vacuna-routing.module';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {ExtendedModule, FlexModule} from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatInputModule} from '@angular/material/input';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {MatFormFieldModule} from '@angular/material/form-field';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import {MatMenuModule} from '@angular/material/menu';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonToggleModule} from '@angular/material/button-toggle';



@NgModule({
  declarations: [DosisVacunaComponent],
  imports: [
    CommonModule,
    DosisVacunaRoutingModule,
    FlexModule,
    ExtendedModule,
    MatButtonModule,
    MatTooltipModule,
    MatIconModule,
    IconModule,
    MatMenuModule,
    MatTableModule,
    MatSortModule,
    MatCheckboxModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatInputModule,
    MatDividerModule,
    MatSnackBarModule,
    ContainerModule,
    MatSelectModule,
    MatButtonToggleModule,
    FormsModule,
    LoadingModule,
    MatFormFieldModule
  ]
})
export class DosisVacunaModule { }
