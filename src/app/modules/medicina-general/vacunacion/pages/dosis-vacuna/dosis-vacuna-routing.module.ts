import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DosisVacunaComponent} from 'src/app/modules/medicina-general/vacunacion/pages/dosis-vacuna/dosis-vacuna.component';

const routes: Routes = [
  {
    path: '',
      component: DosisVacunaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DosisVacunaRoutingModule {
}
