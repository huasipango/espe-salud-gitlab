import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {switchMap} from 'rxjs/operators';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import icAdd from '@iconify/icons-ic/twotone-add';
import icDeleteForever from '@iconify/icons-ic/twotone-delete-forever';
import icSave from '@iconify/icons-fa-solid/save';
import {DosisVacuna} from 'src/app/modules/medicina-general/vacunacion/models/dosis-vacuna.model';
import {Vacuna} from 'src/app/modules/medicina-general/vacunacion/models/vacuna.model';
import {DosisVacunaService} from 'src/app/modules/medicina-general/vacunacion/services/dosis-vacuna.service';
import {DosisVacunaModalComponent} from 'src/app/modules/medicina-general/vacunacion/components/dosis-vacuna-modal/dosis-vacuna-modal.component';
import {Dosis} from 'src/app/core/models/catalogo/dosis.model';
import {getShortDate} from 'src/app/core/utils/date-utils';
import {VacunaService} from 'src/app/modules/medicina-general/vacunacion/services/vacuna.service';
import {VacunaSharedService} from 'src/app/modules/medicina-general/vacunacion/services/vacuna-shared.service';

@Component({
  selector: 'vex-dosis-vacuna',
  templateUrl: './dosis-vacuna.component.html',
  styleUrls: ['./dosis-vacuna.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms,
    scaleFadeIn400ms,
    scaleIn400ms,
    fadeInRight400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class DosisVacunaComponent implements OnInit, AfterViewInit {

  dosisVacunas: DosisVacuna[];
  vacunaCodigo: number;
  messages = USER_MESSAGES;
  icAdd = icAdd;
  icDeleteForever = icDeleteForever;
  icSave = icSave;
  vacuna: Vacuna;
  dosisTotales: number;
  dosisSuministradas: number;

  tableColumns: TableColumn<DosisVacuna>[] = [
    {label: 'Fecha', property: 'fecha', type: 'date', visible: true},
    {label: 'Fecha de la siguiente dosis', property: 'fechaSiguienteDosis', type: 'date', visible: true},
    {label: 'establecimiento', property: 'establecimiento', type: 'text', visible: true},
    {label: 'Responsable', property: 'responsable', type: 'text', visible: true},
    {label: 'Dosis', property: 'dosis', type: 'object', visible: true},
    {label: 'Acciones', property: 'menu', type: 'button', visible: true}
  ];
  dataSource: MatTableDataSource<DosisVacuna> | null;

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private snackbar: MatSnackBar,
    private dosisVacunasService: DosisVacunaService,
    private vacunaService: VacunaService,
    private vacunaSharedService: VacunaSharedService
  ) { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.getDosisVacuna();
    this.countDosisVacunas();
  }

  getDosisVacuna(){
    this.route.paramMap.pipe(
      switchMap(params => {
        const codigo = params.get('codigo');
        this.vacunaCodigo = Number(codigo);
        return this.dosisVacunasService.getDosiVacunas(this.vacunaCodigo);
      })
    ).subscribe((dosisVacunas) => {
      if (dosisVacunas.length > 0) {
        this.dosisVacunas = dosisVacunas;
        this.dataSource.data = this.dosisVacunas;
      }
    });
  }

  countDosisVacunas() {
    this.dosisVacunasService.getCountDosiVacunas(this.vacunaCodigo)
      .subscribe((dosis: number) => {
        if (dosis){
          this.dosisSuministradas = dosis;
        }
      });
    this.vacunaService.getVacuna(this.vacunaCodigo)
      .subscribe((vacuna: Vacuna) => {
        if (vacuna){
          this.vacuna = vacuna;
          this.vacunaSharedService.setTipoVacuna(vacuna.tipoVacuna);
        }
      });
  }
  createDosisVacuna() {
    this.vacunaSharedService.setTipoVacuna(this.vacuna.tipoVacuna);
    this.dialog
      .open(DosisVacunaModalComponent, {
        width: '700px',
        maxWidth: '100%'
      }).afterClosed()
      .subscribe((dosisVacunas: DosisVacuna) => {
        if (dosisVacunas) {
          this.dosisVacunasService.createDosisVacuna(dosisVacunas, this.vacunaCodigo)
            .subscribe((response) => {
              if (response) {
                this.dosisVacunas.push(response);
                this.dataSource.data = this.dosisVacunas;
                this.getDosisVacuna();
                this.countDosisVacunas();
                this.showNotification(this.messages.createdSuccessMessage, 'CERRAR');
              }
            }, error => {
              this.showNotification(this.messages.createdFailedMessage, 'CERRAR');
            });
        }
      });
  }

  updateDosisVacunas(dosisVacunas: DosisVacuna) {
    this.dialog.open(DosisVacunaModalComponent, {
      data: dosisVacunas,
      width: '700px',
      maxWidth: '100%'
    }).afterClosed().subscribe(dosisVacunasModificada => {
      if (dosisVacunasModificada) {
        this.dosisVacunasService.updateDosisVacuna(
          dosisVacunas.id,
          dosisVacunasModificada,
          this.vacunaCodigo
        ).subscribe((response: DosisVacuna) => {
          if (response) {
            const index = this.dosisVacunas.findIndex((dosisVacunaExistente) =>
              dosisVacunaExistente.id === dosisVacunasModificada.id);
            this.dosisVacunas[index] = response;
            this.getDosisVacuna();
            this.countDosisVacunas();
            this.dataSource.connect().next(this.dosisVacunas);
            this.showNotification(this.messages.updatedSuccessMessage, 'CERRAR');
          }
        }, error => {
          this.showNotification(this.messages.updatedFailedMessage, 'CERRAR');
        });
      }
    });
  }

  openDeleteDialog(dosisVacuna: DosisVacuna) {
    this.dialog.open(DeleteModalComponent, {
      data: this.messages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') {
        this.deleteDosisVacuna(dosisVacuna);
      }
    });
  }

  deleteDosisVacuna(dosisVacunas: DosisVacuna) {
    this.dosisVacunasService.deleteDosisVacuna(dosisVacunas.id, this.vacunaCodigo)
      .subscribe((response: boolean) => {
        if (response) {
          const index = this.dosisVacunas.findIndex(p => p.id === dosisVacunas.id);
          this.dosisVacunas.splice(index, 1);
          this.dataSource.connect().next(this.dosisVacunas);
          this.getDosisVacuna();
          this.countDosisVacunas();
          this.showNotification(this.messages.deletedSuccessMessage, 'CERRAR');
        } else {
          this.showNotification(this.messages.deleteFailMessage, 'CERRAR');
        }
      });
  }

  getDosis(dosis: Dosis): string {
    return dosis ? dosis.nombre : '';
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  trackByProperty<T>(column: TableColumn<T>) {
    return column.property;
  }

  get visibleColumns() {
    return this.tableColumns
      .filter(column => column.visible)
      .map(column => column.property);
  }

  getFormattedDate(date: Date) {
    return getShortDate(date);
  }

}
