import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Vacuna} from 'src/app/modules/medicina-general/vacunacion/models/vacuna.model';

@Injectable({
  providedIn: 'root'
})
export class VacunaService {

  private baseUrl: string = environment.baseUrl;
  private commonUrl: string;

  constructor(private http: HttpClient) {
    this.commonUrl = this.baseUrl + 'vacunas/';
  }

  getVacuna(id: number): Observable<Vacuna> {
    return this.http.get<Vacuna>(this.commonUrl + id);
  }

  getVacunas(idPaciente: number): Observable<Vacuna[]> {
    return this.http
      .get<Vacuna[]>(this.commonUrl + 'paciente?idPaciente=' + idPaciente);
  }

  createVacuna(data: Vacuna): Observable<Vacuna> {
    return this.http.post<Vacuna>(this.commonUrl, JSON.stringify(data));
  }

  updateVacuna(id: number, data: Vacuna): Observable<Vacuna> {
    return this.http.put<Vacuna>(this.commonUrl + id, JSON.stringify(data));
  }

  deleteVacuna(id: number): Observable<boolean> {
    return this.http.delete<boolean>(this.commonUrl + id);
  }
  patchVacuna(data: any, id: string): Observable<Vacuna> {
    return this.http.patch<Vacuna>(this.commonUrl + id, JSON.stringify(data));
  }
}
