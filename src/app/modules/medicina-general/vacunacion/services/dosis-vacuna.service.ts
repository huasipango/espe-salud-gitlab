import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DosisVacuna} from 'src/app/modules/medicina-general/vacunacion/models/dosis-vacuna.model';

@Injectable({
  providedIn: 'root'
})
export class DosisVacunaService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getDosiVacunas(idVacuna: number): Observable<DosisVacuna[]> {
    const url = `${this.baseUrl}vacunas/${idVacuna}/dosis-vacunas?idVacuna=${idVacuna}`;
    return this.http.get<DosisVacuna[]>(url);
  }

  getCountDosiVacunas(idVacuna: number): Observable<number>{
    const url = `${this.baseUrl}vacunas/${idVacuna}/dosis-vacunas/idVacuna?id=${idVacuna}`;
    return this.http.get<number>(url);
  }

  createDosisVacuna(data: DosisVacuna, idVacuna: number): Observable<DosisVacuna> {
    const url = `${this.baseUrl}vacunas/${idVacuna}/dosis-vacunas/?idVacuna=${idVacuna}`;
    return this.http.post<DosisVacuna>(url, JSON.stringify(data));
  }

  updateDosisVacuna(id: number, data: DosisVacuna, idVacuna: number): Observable<DosisVacuna> {
    const url = `${this.baseUrl}vacunas/${idVacuna}/dosis-vacunas/${id}`;
    return this.http.put<DosisVacuna>(url, JSON.stringify(data));
  }

  deleteDosisVacuna(id: number, idVacuna: number): Observable<boolean> {
    const url = `${this.baseUrl}vacunas/${idVacuna}/dosis-vacunas/${id}`;
    return this.http.delete<boolean>(url);
  }
}
