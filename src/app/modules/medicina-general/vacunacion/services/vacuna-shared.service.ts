import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Vacuna} from 'src/app/modules/medicina-general/vacunacion/models/vacuna.model';
import {TipoVacuna} from 'src/app/core/models/catalogo/tipo-vacunas.model';

@Injectable({
  providedIn: 'root'
})
export class VacunaSharedService {
  constructor() { }
  private subject: BehaviorSubject<Vacuna> = new BehaviorSubject<Vacuna>(null);
  private subjectTipoVacuna: BehaviorSubject<TipoVacuna> = new BehaviorSubject<TipoVacuna>(null);
  public vacuna$: Observable<Vacuna> = this.subject.asObservable();
  public setVacuna(vacuna: Vacuna): void{
    this.subject.next(vacuna);
  }
  public setTipoVacuna(tipoVacuna: TipoVacuna): void{
    this.subjectTipoVacuna.next(tipoVacuna);
  }
  get sharingObservable(){
    return this.subject.asObservable();
  }
  get sharingTipoVacunaObservable(){
     return this.subjectTipoVacuna.asObservable();
  }
}
