import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';


@Injectable({
  providedIn: 'root'
})
export class VacunaReportService {

  private baseUrl: string = environment.baseUrl;
  private readonly commonUrl: string;

  constructor(private http: HttpClient) {
    this.commonUrl = this.baseUrl + 'vacunas/';
  }

  getCertificadoVacuna(idVacuna: number): any{
    const url = this.commonUrl + 'reporte/certificado-vacuna/pdf?idVacuna=' + idVacuna;
    return this.http.get(url, {
      observe: 'response',
      responseType: 'blob'
    }).pipe(
      map((response) => {
        const type = response.body.type;
        const a = document.createElement('a');
        const generatedUrl = URL.createObjectURL(
          new Blob([response.body], { type })
        );
        a.href = generatedUrl;
        a.download = response.headers
          .get('content-disposition')
          .split('filename=')[1];
        document.body.appendChild(a);
        a.click();
        setTimeout(() => {
          document.body.removeChild(a);
          window.URL.revokeObjectURL(generatedUrl);
        }, 0);
        return response;
      })
    );
  }

}
