import {OrganoSistema} from 'src/app/modules/medicina-general/evolucion/models/organo-sistema';

export class ExamenInterno {
  id: number;
  descripcion: string;
  fechaRegistro: Date;
  idPaciente: number;
  idOrganoSistema: number;
  organoSistema: OrganoSistema;
}


