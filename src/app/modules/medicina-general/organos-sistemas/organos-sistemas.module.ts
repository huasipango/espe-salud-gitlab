import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrganosSistemasComponent } from './organos-sistemas.component';
import {OrganosSistemasRoutingModule} from 'src/app/modules/medicina-general/organos-sistemas/organos-sistemas-routing.module';
import {ExtendedModule, FlexModule} from '@angular/flex-layout';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {IconModule} from '@visurel/iconify-angular';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {EmptyPacienteModule} from 'src/app/core/components/empty-paciente/empty-paciente.module';
import { OrganosSistemasModalComponent } from './components/organos-sistemas-modal/organos-sistemas-modal.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';

@NgModule({
  declarations: [OrganosSistemasComponent, OrganosSistemasModalComponent],
  imports: [
    CommonModule,
    OrganosSistemasRoutingModule,
    FlexModule,
    ContainerModule,
    IconModule,
    BreadcrumbsModule,
    ExtendedModule,
    MatButtonModule,
    MatTooltipModule,
    MatIconModule,
    ReactiveFormsModule,
    MatMenuModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatCheckboxModule,
    FormsModule,
    EmptyPacienteModule,
    MatDialogModule,
    MatInputModule,
    MatSelectModule,
    LoadingModule
  ]
})
export class OrganosSistemasModule { }
