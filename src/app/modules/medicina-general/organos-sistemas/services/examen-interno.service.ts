import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ExamenInterno} from 'src/app/modules/medicina-general/organos-sistemas/models/examen-interno.models';

@Injectable({
  providedIn: 'root'
})
export class ExamenInternoService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getExamenesInternos(idPaciente: number): Observable<ExamenInterno[]>{
    return this.http.get<ExamenInterno[]>(this.baseUrl + 'examenes-internos?idPaciente=' + idPaciente);
  }

  createExamenInterno(data: ExamenInterno): Observable<ExamenInterno> {
    return this.http.post<ExamenInterno>(this.baseUrl + 'examenes-internos/', JSON.stringify(data));
  }

  updateExamenInterno(id: number, data: ExamenInterno): Observable<ExamenInterno> {
    return this.http.put<ExamenInterno>(this.baseUrl + 'examenes-internos/' + id, JSON.stringify(data));
  }

  deleteExamenInterno(id: number): Observable<boolean> {
    return this.http.delete<boolean>(this.baseUrl + 'examenes-internos/' + id);
  }
}
