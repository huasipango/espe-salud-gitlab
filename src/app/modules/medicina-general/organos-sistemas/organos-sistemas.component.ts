import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {ExamenInterno} from 'src/app/modules/medicina-general/organos-sistemas/models/examen-interno.models';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import {FormControl} from '@angular/forms';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icHeart from '@iconify/icons-fa-solid/heart';
import icDownload from '@iconify/icons-ic/cloud-download';
import icPrint from '@iconify/icons-ic/print';
import icCheck from '@iconify/icons-fa-solid/clipboard-check';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {ExamenInternoService} from 'src/app/modules/medicina-general/organos-sistemas/services/examen-interno.service';
import {DateUtil, getLongDate} from 'src/app/core/utils/date-utils';
import {OrganoSistema} from 'src/app/modules/medicina-general/evolucion/models/organo-sistema';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {OrganosSistemasModalComponent} from 'src/app/modules/medicina-general/organos-sistemas/components/organos-sistemas-modal/organos-sistemas-modal.component';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';

@UntilDestroy()
@Component({
  selector: 'vex-organos-sistemas',
  templateUrl: './organos-sistemas.component.html',
  styleUrls: ['./organos-sistemas.component.scss'],
  animations: [
    scaleIn400ms,
    scaleFadeIn400ms,
    stagger40ms,
    fadeInRight400ms,
    fadeInUp400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class OrganosSistemasComponent implements OnInit, AfterViewInit {
  examenesInternos: ExamenInterno[] = [];

  @Input()
  columns: TableColumn<ExamenInterno>[] = [
    {label: 'Órganos y sistemas', property: 'organoSistema', type: 'object', visible: true},
    {label: 'Fecha registro', property: 'fechaRegistro', type: 'datetime', visible: true},
    {label: 'Descripción', property: 'descripcion', type: 'text', visible: true, cssClasses: ['text-wrap']},
    {label: 'Acciones', property: 'actions', type: 'button', visible: true}
  ];
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<ExamenInterno> | null;
  searchCtrl = new FormControl();

  icEdit = icEdit;
  icSearch = icSearch;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icMoreHoriz = icMoreHoriz;
  icHeart = icHeart;
  icDownload = icDownload;
  icPrint = icPrint;
  icCheck = icCheck;

  pacienteActual: Paciente;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private catalogoService: CatalogoService,
    private examenInternoService: ExamenInternoService,
    private pacienteGlobalService: PacienteGlobalService,
  ) { }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getExamenesInternos();
        }
      });
    this.searchCtrl.valueChanges.pipe(
      untilDestroyed(this)
    ).subscribe(value => this.onFilterChange(value));
  }

  getExamenesInternos(): void {
    this.examenInternoService.getExamenesInternos(this.pacienteActual.id)
      .subscribe((data: ExamenInterno[]) => {
        this.examenesInternos = data;
        this.dataSource.data = this.examenesInternos;
      });
  }

  createExamenInterno(): void {
    this.dialog.open(OrganosSistemasModalComponent, {
      width: '600px',
      maxWidth: '100%',
      disableClose: true
    }).afterClosed()
      .subscribe((examen: ExamenInterno) => {
        if (examen) {
          this.showNotification('Registro creado EXITOSAMENTE', 'OK');
          this.examenesInternos.push(examen);
          this.dataSource.connect().next(this.examenesInternos);
        }
      });
  }

  updateExamenInterno(examen: ExamenInterno): void {
    this.dialog.open(OrganosSistemasModalComponent, {
      data: examen,
      maxWidth: '100%',
      width: '600px',
      disableClose: true
    }).afterClosed()
      .subscribe((updated) => {
        if (updated) {
          this.showNotification('Registro actualizado EXITOSAMENTE', 'OK');
          const id = examen.id;
          const index = this.examenesInternos.findIndex(
            (existing) => existing.id === id
          );
          this.examenesInternos[index] = updated;
          this.dataSource.connect().next(this.examenesInternos);
        }
      });
  }

  deleteExamenInterno(examen: ExamenInterno): void {
    this.examenInternoService.deleteExamenInterno(examen.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification('Registro eliminado EXITOSAMENTE', 'CERRAR');
          this.examenesInternos.splice(
            this.examenesInternos.findIndex((existing) =>
              existing.id === examen.id), 1
          );
          this.dataSource.connect().next(this.examenesInternos);
        } else {
          this.showNotification('No se pudo eliminar el registro', 'CERRAR');
        }
      });
  }

  openDeleteDialog(examen: ExamenInterno): void {
    this.dialog.open(DeleteModalComponent, {
      data: '¿Estas seguro de eliminar este registro?',
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') { this.deleteExamenInterno(examen); }
    });
  }

  getFormattedDateTime(date: string) {
    return DateUtil.showDateTimeFormat(date);
  }

  onFilterChange(value: string) {
    if (!this.dataSource) {
      return;
    }
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  print() {
  }

  openSearchModal(){
  }

  getOrganoSistema(organoSistema: OrganoSistema): string {
    return organoSistema.nombre;
  }
}
