import {Component, Inject, OnInit} from '@angular/core';
import icCheck from '@iconify/icons-ic/check';
import icClose from '@iconify/icons-ic/twotone-close';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {ExamenInterno} from 'src/app/modules/medicina-general/organos-sistemas/models/examen-interno.models';
import {OrganoSistema} from 'src/app/modules/medicina-general/evolucion/models/organo-sistema';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {Observable} from 'rxjs';
import {LoadingService} from 'src/app/core/services/loading.service';
import {ExamenInternoService} from 'src/app/modules/medicina-general/organos-sistemas/services/examen-interno.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'vex-organos-sistemas-modal',
  templateUrl: './organos-sistemas-modal.component.html',
  styleUrls: ['./organos-sistemas-modal.component.scss']
})
export class OrganosSistemasModalComponent implements OnInit {
  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  pacienteActual: Paciente;
  organosSistemas: Observable<OrganoSistema[]>;
  icClose = icClose;
  icCheck = icCheck;

  constructor(
    private dialogRef: MatDialogRef<OrganosSistemasModalComponent>,
    @Inject(MAT_DIALOG_DATA) public defaults: ExamenInterno,
    private fb: FormBuilder,
    private catalogoService: CatalogoService,
    private pacienteGlobalService: PacienteGlobalService,
    private loadingService: LoadingService,
    private examenInternoService: ExamenInternoService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.organosSistemas = this.catalogoService.getOrganoSistema();
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as ExamenInterno;
    }
    this.form = this.fb.group({
      id: [this.defaults.id || null],
      idOrganoSistema: [this.defaults.idOrganoSistema || null, Validators.required],
      descripcion: [this.defaults.descripcion || '', Validators.required],
      idPaciente: [this.defaults.idPaciente || null, Validators.required],
    });
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.form.patchValue({idPaciente: paciente.id});
        }
      });
  }

  save(){
    const examen: ExamenInterno = this.form.value;
    if (this.mode === 'create') {
      this.createExamen(examen);
    } else if (this.mode === 'update') {
      this.updateExamen(examen);
    }
  }

  createExamen(examen: ExamenInterno): void {
    this.loadingService.showLoaderUntilCompleted(
      this.examenInternoService.createExamenInterno(examen)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      } else {
        this.showNotification('No se pudo crear el registro', 'CERRAR');
      }
    });
  }

  updateExamen(examen: ExamenInterno): void {
    this.loadingService.showLoaderUntilCompleted(
      this.examenInternoService.updateExamenInterno(examen.id, examen)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      } else{
        this.showNotification('No se pudo actualizar el registro', 'CERRAR');
      }
    });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  showNotification(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000
    });
  }
}
