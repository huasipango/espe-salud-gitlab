import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from 'src/@vex/interfaces/vex-route.interface';
import {OrganosSistemasComponent} from 'src/app/modules/medicina-general/organos-sistemas/organos-sistemas.component';

const routes: VexRoutes = [
  {
    path: '',
    component: OrganosSistemasComponent,
    data: {
      toolbarShadowEnabled: true,
      containerEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganosSistemasRoutingModule { }
