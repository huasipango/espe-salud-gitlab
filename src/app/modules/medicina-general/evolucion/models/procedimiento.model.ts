export class Procedimiento {
  id: number;
  numeroActividades: number;
  nota: string;
  responsable: string;
  tipoProcedimiento: string;

constructor(procedimiento){
  this.id = procedimiento.id;
  this.numeroActividades = procedimiento.numeroActividades;
  this.nota = procedimiento.nota;
  this.responsable = procedimiento.responsable;
  this.tipoProcedimiento = procedimiento.tipoProcedimiento;
  }
}
