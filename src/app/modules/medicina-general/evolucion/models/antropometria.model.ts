export class Antropometria {
  id: number;
  peso: number;
  talla: number;
  indiceMasaCorporal: number;
  perimetroAbdominal: number;
}
