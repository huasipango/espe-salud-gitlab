
export class ReporteReposo {
  id: number;
  fechaInicio: Date;
  fechaFin: Date;
  condicionPaciente: string;
  recomendacion: string;
}
