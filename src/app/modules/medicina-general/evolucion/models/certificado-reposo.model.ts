export class CertificadoReposo {
  fechaInicio: Date;
  fechaFin: Date;
  condicionPaciente: string;
  recomendacion: string;
}
