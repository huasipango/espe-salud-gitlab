import { Area } from './area.model';

export class Region {
    id: number;
    nombre: string;
    areas: Area[];
}
