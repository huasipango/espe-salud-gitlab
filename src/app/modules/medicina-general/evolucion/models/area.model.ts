export class Area {
  id: number;
  nombre: string;

  constructor(area) {
    this.id = area.id;
    this.nombre = area.nombre;
  }
}
