import {TipoEnfermedadCIE10} from 'src/app/core/models/catalogo/tipo-enfermedad-cie10.model';

export class Diagnostico {
  id: number;
  codigoCIE: string;
  tipo: string;
  cronologia: string;
  condicionDiagnostico: string;
  cie10: TipoEnfermedadCIE10;
}
