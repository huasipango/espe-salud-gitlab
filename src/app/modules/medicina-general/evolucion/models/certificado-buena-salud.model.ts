export class CertificadoBuenaSalud {
  aptitudMedica: 'APTO' | 'APTOENOBSERVACION' | 'APTOCONLIMITACIONES' | 'NOAPTO';
  observaciones: string;
  recomendacion: string;
}
