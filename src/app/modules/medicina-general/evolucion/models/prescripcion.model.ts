import { RepertorioMedicamento } from 'src/app/core/models/catalogo/repertorio-medicamento.model';
export class Prescripcion {
  id: number;
  medicamento: RepertorioMedicamento;
  cantidad: number;
  presentacion: string;
  indicacion: string;
}
