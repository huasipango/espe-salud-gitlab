import {Dispensario} from 'src/app/core/models/catalogo/dispensario.model';
import {AreaSalud} from 'src/app/core/models/catalogo/area-salud.model';
import {MotivoAtencion} from 'src/app/core/models/catalogo/motivo-atencion.model';
import {NotaEnfermeria} from 'src/app/modules/enfermeria/notas-enfermeria/models/nota-enfermeria.model';
import {LugarAtencion} from 'src/app/core/models/catalogo/lugar-atencion.model';
import {UsuarioSimple} from 'src/app/core/models/usuario/usuario-simple.model';

export class Evolucion {
  id: string;
  fechaInicio: Date;
  estado: string;
  fechaFinalizacion: string;
  tipoConsulta: string;
  motivoConsulta: string;
  idMotivoAtencion: number;
  notaEvolucion: string;
  indicacionNoFarmacologica: string;
  responsablePidm: number;
  esEnfermedadOcupacional: boolean;
  idPaciente: number;
  idDispensario: number;
  idAreaSalud: string;
  idNotaEnfermeria: number;
  dispensario: Dispensario;
  areaSalud: AreaSalud;
  motivoAtencion: MotivoAtencion;
  notaEnfermeria: NotaEnfermeria;
  usuario: UsuarioSimple;
  idTipoHistoriaClinica: number;
  idLugarAtencion: number;
  lugarAtencion: LugarAtencion;
  hasEvolucion: boolean;
}
