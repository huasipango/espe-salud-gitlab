import {Evolucion} from 'src/app/modules/medicina-general/evolucion/models/evolucion.model';

export class DetallePrescripcion {
  id: number;
  dosis: string;
  duracion: string;
  frecuencia: string;
  fecha: Date;
  observacion: string;
}
