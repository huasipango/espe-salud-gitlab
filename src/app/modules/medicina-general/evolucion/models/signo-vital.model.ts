export class SignoVital{
  id: number;
  fechaUltimaMenstruacion: string;
  frecuenciaCardiaca: number;
  frecuenciaRespiratoria: number;
  presionArterial: number;
  diastole: number;
  saturacionOxigeno: number;
  temperatura: number;
}
