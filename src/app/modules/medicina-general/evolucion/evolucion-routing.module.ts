import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {VexRoutes} from 'src/@vex/interfaces/vex-route.interface';
import {RolEnum} from 'src/app/core/enums/rol.enum';
import {RoleGuard} from 'src/app/core/auth/role.guard';


const routes: VexRoutes = [
  {
    path: '',
    canActivate: [RoleGuard],
    data: {
      roles: [
        RolEnum.ROLE_ENFERMERO,
        RolEnum.ROLE_MEDICO,
        RolEnum.ROLE_OCUPACIONAL,
        RolEnum.ROLE_ODONTOLOGO
      ]
    },
    children: [
      {
        path: '',
        loadChildren: () => import('./pages/evolucion-list/evolucion-list.module').then(m => m.EvolucionListModule)
      },
      {
        path: 'create',
        loadChildren: () => import('./pages/evolucion-create/evolucion-create.module').then(m => m.EvolucionCreateModule)
      },
      {
        path: ':codigo',
        loadChildren: () => import('./pages/detalle-evolucion/detalle-evolucion.module').then(m => m.DetalleEvolucionModule)
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvolucionRoutingModule {
}
