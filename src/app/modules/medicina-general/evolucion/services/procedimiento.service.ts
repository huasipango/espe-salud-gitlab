import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Procedimiento } from 'src/app/modules/medicina-general/evolucion/models/procedimiento.model';
import { retry, catchError, tap } from 'rxjs/operators';
import {Observable, of, throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProcedimientoService {
  baseUrl: string = environment.baseUrl;
  commonUrl: string;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Allow: 'GET, POST, HEAD'
    })
  };

  constructor(
    private http: HttpClient
  ) {
    this.commonUrl = this.baseUrl + 'evoluciones/';
  }

  // GET
  getProcedimiento(idEvolucion): Observable<Procedimiento[]> {
    return this.http.get<Procedimiento[]>(
      this.commonUrl + idEvolucion + '/procedimientos/', this.httpOptions)
      .pipe(
        retry(1),
        tap(_=>(this.log('Procedimientos obtenidos'))),
        catchError(this.handleError)
      )
  }

  createProcedimiento(
    data: Procedimiento,
    idEvolucion): Observable<Procedimiento> {
    return this.http.post<Procedimiento>(
      this.commonUrl + idEvolucion + '/procedimientos/', JSON.stringify(data), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  // PUT
  updateProcediminto(
    id,
    data: Procedimiento,
    idEvolucion): Observable<Procedimiento> {
    return this.http.put<Procedimiento>(
      this.commonUrl + idEvolucion + '/procedimientos/' + id, JSON.stringify(data), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // DELETE
  deleteProcedimiento(procedimientosList, idEvolucion): Observable<Procedimiento> {
    const options = {
      headers: this.httpOptions.headers,
      body: procedimientosList
    };
    return this.http.delete<Procedimiento>(
      this.commonUrl + idEvolucion + '/procedimientos/delete-all', options)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  private handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

  private log(message: string) {
    console.log(message);
  }
}
