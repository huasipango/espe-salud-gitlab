import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {CertificadoReposo} from 'src/app/modules/medicina-general/evolucion/models/certificado-reposo.model';
import {DateUtil} from 'src/app/core/utils/date-utils';
import {CertificadoBuenaSalud} from 'src/app/modules/medicina-general/evolucion/models/certificado-buena-salud.model';

@Injectable({
  providedIn: 'root'
})
export class EvolucionReportService {
  private baseUrl: string = environment.baseUrl;
  private readonly commonUrl: string;

  constructor(private http: HttpClient) {
    this.commonUrl = this.baseUrl + 'evoluciones/';
  }

  getRecetaMedica(idEvolucion: string): any{
    const url = this.commonUrl + 'reporte/receta-medica/pdf?idEvolucion=' + idEvolucion;
    return this.http.get(url, {
        observe: 'response',
        responseType: 'blob'
      }).pipe(
        map((response) => {
          const type = response.body.type;
          const a = document.createElement('a');
          const generatedUrl = URL.createObjectURL(
            new Blob([response.body], { type })
          );
          a.href = generatedUrl;
          a.download = response.headers
            .get('content-disposition')
            .split('filename=')[1];
          document.body.appendChild(a);
          a.click();
          setTimeout(() => {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(generatedUrl);
          }, 0);
          return response;
        })
      );
  }

  getCertificadoReposo(data: CertificadoReposo, idEvolucion: string){
    const params = new HttpParams()
      .set('idEvolucion', idEvolucion)
      .set('fechaInicio', DateUtil.customFormatDate(data.fechaInicio.toString()))
      .set('fechaFin', DateUtil.customFormatDate(data.fechaFin.toString()))
      .set('condicionPaciente', data.condicionPaciente)
      .set('recomendacion', data.recomendacion);
    const url = this.commonUrl + 'reporte/certificado-reposo/pdf';
    return this.http.get(url, {observe: 'response', responseType: 'blob', params})
      .pipe(
        map((response) => {
          const type = response.body.type;
          const a = document.createElement('a');
          const generatedUrl = URL.createObjectURL(
            new Blob([response.body], { type })
          );
          a.href = generatedUrl;
          a.download = response.headers
            .get('content-disposition')
            .split('filename=')[1];
          document.body.appendChild(a);
          a.click();
          setTimeout(() => {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(generatedUrl);
          }, 0);
          return response;
        })
      );
  }

  getCertificadoBuenaSalud(data: CertificadoBuenaSalud, idEvolucion: string) {
    const params = new HttpParams()
      .set('idEvolucion', idEvolucion)
      .set('aptitudMedica', data.aptitudMedica)
      .set('observaciones', data.observaciones)
      .set('recomendacion', data.recomendacion);
    const url = this.commonUrl + 'reporte/certificado-buena-salud/pdf';
    return this.http.get(url, {observe: 'response', responseType: 'blob', params})
      .pipe(
        map((response) => {
          const type = response.body.type;
          const a = document.createElement('a');
          const generatedUrl = URL.createObjectURL(
            new Blob([response.body], { type })
          );
          a.href = generatedUrl;
          a.download = response.headers
            .get('content-disposition')
            .split('filename=')[1];
          document.body.appendChild(a);
          a.click();
          setTimeout(() => {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(generatedUrl);
          }, 0);
          return response;
        })
      );
  }

}
