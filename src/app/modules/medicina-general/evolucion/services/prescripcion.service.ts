import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Prescripcion} from 'src/app/modules/medicina-general/evolucion/models/prescripcion.model';

@Injectable({
  providedIn: 'root'
})
export class PrescripcionService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getPrescripciones(idEvolucion: string): Observable<Prescripcion[]> {
    const url = `${this.baseUrl}evoluciones/${idEvolucion}/prescripciones`;
    return this.http.get<Prescripcion[]>(url);
  }

  createPrescripcion(data: Prescripcion, idEvolucion: string): Observable<Prescripcion> {
    const url = `${this.baseUrl}evoluciones/${idEvolucion}/prescripciones/`;
    return this.http.post<Prescripcion>(url, JSON.stringify(data));
  }

  updatePrescripcion(id: number, data: Prescripcion, idEvolucion: string): Observable<Prescripcion> {
    const url = `${this.baseUrl}evoluciones/${idEvolucion}/prescripciones/${id}`;
    return this.http.put<Prescripcion>(url, JSON.stringify(data));
  }

  deletePrescripcion(id: number, idEvolucion: string): Observable<boolean> {
    const url = `${this.baseUrl}evoluciones/${idEvolucion}/prescripciones/${id}`;
    return this.http.delete<boolean>(url);
  }
}
