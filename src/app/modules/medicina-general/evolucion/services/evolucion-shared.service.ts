import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Evolucion} from '../models/evolucion.model';


@Injectable({
  providedIn: 'root'
})
export class EvolucionSharedService {
  constructor() { }
  private subject: BehaviorSubject<Evolucion> = new BehaviorSubject<Evolucion>(null);
  public evolucion$: Observable<Evolucion> = this.subject.asObservable();
  public setEvolucion(evolucion: Evolucion): void{
    this.subject.next(evolucion);
  }
}

