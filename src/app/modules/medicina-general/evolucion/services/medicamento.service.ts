import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {RepertorioMedicamento} from 'src/app/core/models/catalogo/repertorio-medicamento.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MedicamentoService {
  baseUrl: string = environment.baseUrl;
  commonUrl: string;

  constructor(private http: HttpClient) {
    this.commonUrl = this.baseUrl  + 'repertorios-medicamento/';
  }

  getMedicamentos(): Observable<RepertorioMedicamento[]>  {
    return this.http.get<RepertorioMedicamento[]>(this.commonUrl);
  }

  createMedicamento(data): Observable<RepertorioMedicamento>  {
    return this.http.post<RepertorioMedicamento>(this.commonUrl, JSON.stringify(data));
  }

  updateMedicamento(idMedicamento, data): Observable<RepertorioMedicamento>  {
    return this.http.put<RepertorioMedicamento>(this.commonUrl + idMedicamento, JSON.stringify(data));
  }

  deleteMedicamento(idMedicamento): Observable<RepertorioMedicamento>  {
    return this.http.delete<RepertorioMedicamento>(this.commonUrl + idMedicamento);
  }
}
