import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Evolucion} from 'src/app/modules/medicina-general/evolucion/models/evolucion.model';
import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class EvolucionService {
  private baseUrl: string = environment.baseUrl;
  private readonly commonUrl: string;

  constructor(private http: HttpClient) {
    this.commonUrl = this.baseUrl + 'evoluciones/';
  }

  getEvolucion(id: string): Observable<Evolucion> {
    return this.http.get<Evolucion>(this.commonUrl + id);
  }

  getEvoluciones(idPaciente: number): Observable<Evolucion[]> {
    return this.http
      .get<Evolucion[]>(this.commonUrl + 'paciente?idPaciente=' + idPaciente);
  }

  createEvolucion(data): Observable<Evolucion> {
    return this.http.post<Evolucion>(this.commonUrl, JSON.stringify(data));
  }

  createEvolucionPartial(data): Observable<Evolucion> {
    return this.http.post<Evolucion>(this.commonUrl + 'partial', JSON.stringify(data));
  }

  updateEvolucion(id, data): Observable<Evolucion> {
    return this.http.put<Evolucion>(this.commonUrl + id, JSON.stringify(data));
  }

  patchEvolucion(data: any, id: string): Observable<Evolucion> {
    return this.http.patch<Evolucion>(this.commonUrl + id, JSON.stringify(data));
  }

  finalizeNotaEvolucion(id: string): Observable<boolean> {
    return this.http.post<boolean>(this.commonUrl + id + '/finalize', null);
  }
}
