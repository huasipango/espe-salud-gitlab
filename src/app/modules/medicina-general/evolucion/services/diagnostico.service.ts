import {Injectable} from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Diagnostico} from '../models/diagnostico.model';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DiagnosticoService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getDiagnosticos(idEvolucion: string): Observable<Diagnostico[]> {
    const url = `${this.baseUrl}evoluciones/${idEvolucion}/diagnosticos`;
    return this.http.get<Diagnostico[]>(url);
  }

  createDiagnostico(data: Diagnostico, idEvolucion: string): Observable<Diagnostico> {
    const url = `${this.baseUrl}evoluciones/${idEvolucion}/diagnosticos/`;
    return this.http.post<Diagnostico>(url, JSON.stringify(data));
  }

  updateDiagnostico(id: number, data: Diagnostico, idEvolucion: string): Observable<Diagnostico> {
    const url = `${this.baseUrl}evoluciones/${idEvolucion}/diagnosticos/${id}`;
    return this.http.put<Diagnostico>(url, JSON.stringify(data));
  }

  deleteDiagnostico(id: number, idEvolucion: string): Observable<boolean> {
    const url = `${this.baseUrl}evoluciones/${idEvolucion}/diagnosticos/${id}`;
    return this.http.delete<boolean>(url);
  }
}
