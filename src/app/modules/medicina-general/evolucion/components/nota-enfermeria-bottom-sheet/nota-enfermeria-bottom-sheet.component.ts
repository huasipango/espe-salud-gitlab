import {Component, Inject, OnInit} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {LoadingService} from 'src/app/core/services/loading.service';
import {NotaEnfermeriaService} from 'src/app/modules/enfermeria/notas-enfermeria/services/nota-enfermeria.service';
import {Observable} from 'rxjs';
import {NotaEnfermeria} from 'src/app/modules/enfermeria/notas-enfermeria/models/nota-enfermeria.model';
import icClose from '@iconify/icons-ic/twotone-close';

@Component({
  selector: 'vex-nota-enfermeria-bottom-sheet',
  templateUrl: './nota-enfermeria-bottom-sheet.component.html',
  styleUrls: ['./nota-enfermeria-bottom-sheet.component.scss']
})
export class NotaEnfermeriaBottomSheetComponent implements OnInit {
  notaEnfermeria: Observable<NotaEnfermeria>;
  icClose = icClose;

  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) private idNotaEnfermeria: number,
    private _bottomSheetRef: MatBottomSheetRef<NotaEnfermeriaBottomSheetComponent>,
    private loadingService: LoadingService,
    private notaEnfermeriaService: NotaEnfermeriaService
  ) { }

  ngOnInit(): void {
    this.notaEnfermeria = this.loadingService.showLoaderUntilCompleted(
      this.notaEnfermeriaService.getNotaEnfermeria(this.idNotaEnfermeria)
    );
  }

  close(): void {
    this._bottomSheetRef.dismiss();
  }
}
