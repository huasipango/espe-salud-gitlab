import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrescripcionFarmacologicaModalComponent } from './prescripcion-farmacologica-modal.component';
import {FlexModule} from '@angular/flex-layout';
import {ReactiveFormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {IconModule} from '@visurel/iconify-angular';
import {MatInputModule} from '@angular/material/input';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSelectModule} from '@angular/material/select';
import {UppercaseModule} from 'src/app/shared/directives/uppercase/uppercase.module';

@NgModule({
  declarations: [PrescripcionFarmacologicaModalComponent],
  imports: [
    CommonModule,
    FlexModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    IconModule,
    MatInputModule,
    MatAutocompleteModule,
    MatSelectModule,
    UppercaseModule
  ]
})
export class PrescripcionFarmacologicaModalModule { }
