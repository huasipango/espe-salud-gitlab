import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import icPills from '@iconify/icons-fa-solid/pills';
import icClose from '@iconify/icons-ic/twotone-close';
import {RepertorioMedicamento} from 'src/app/core/models/catalogo/repertorio-medicamento.model';
import {MedicamentoService} from 'src/app/modules/medicina-general/evolucion/services/medicamento.service';
import {Observable} from 'rxjs';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';
import {debounceTime, distinctUntilChanged, map, startWith, switchMap} from 'rxjs/operators';
import {Prescripcion} from 'src/app/modules/medicina-general/evolucion/models/prescripcion.model';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {SimpleCatalogUtil} from 'src/app/core/utils/simple-catalog-util';

@Component({
  selector: 'vex-prescripcion-farmacologica-modal',
  templateUrl: './prescripcion-farmacologica-modal.component.html',
  styleUrls: ['./prescripcion-farmacologica-modal.component.scss']
})
export class PrescripcionFarmacologicaModalComponent implements OnInit {

  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  filteredMedicamento$: Observable<RepertorioMedicamento[]>;
  tipoMedicamentoCatalogo = new FormControl(
    (this.defaults ? (this.defaults.medicamento ? this.defaults.medicamento : null) : null),
    [Validators.required]
  );

  icClose = icClose;
  icPills = icPills;
  icArrowDropDown = icArrowDropDown;
  medicamento: RepertorioMedicamento;
  presentaciones = SimpleCatalogUtil.presentacionMedicamentos;
  errorMessages = FORM_ERROR_MESSAGES;
  constructor(
    private dialogRef: MatDialogRef<PrescripcionFarmacologicaModalComponent>,
    @Inject(MAT_DIALOG_DATA) public defaults: Prescripcion,
    private fb: FormBuilder,
    protected medicamentoService: MedicamentoService,
  ) { }

  ngOnInit(): void {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as Prescripcion;
    }
    this.filteredMedicamento$ = this.medicamentoService.getMedicamentos();
    this.filteredMedicamento$ = this.tipoMedicamentoCatalogo.valueChanges
      .pipe(
        startWith(''),
        debounceTime(600),
        distinctUntilChanged(),
        switchMap(value => {
          return this.filterMedicamento(value || '');
        })
      );
    this.inicializerForm();
  }

  inicializerForm(): void{
    this.form = this.fb.group({
      id: this.defaults.id || null,
      cantidad: [this.defaults.cantidad || '', Validators.required],
      presentacion: [this.defaults.presentacion || '', Validators.required],
      indicacion: [this.defaults.indicacion || null, Validators.required],
      medicamento: this.tipoMedicamentoCatalogo
    });
  }


  filterMedicamento(value: any): Observable<RepertorioMedicamento[]> {
    value = typeof value === 'string' ? value : value.nombre;
    return this.medicamentoService.getMedicamentos()
      .pipe(
        map(response => response.filter(option => {
          return option.nombre.toLowerCase().indexOf(value.toLowerCase()) === 0;
        }))
      );
  }
  save(){
    const prescripcion = this.form.value;
    if (typeof prescripcion.medicamento === 'string') {
      prescripcion.medicamento = {
        id: null,
        nombre: prescripcion.medicamento,
      };
    }
    prescripcion.indicacion = prescripcion.indicacion.trim();
    this.dialogRef.close(prescripcion);
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  displayFn(medicacion: RepertorioMedicamento): string {
    return medicacion && medicacion.nombre ? medicacion.nombre : '';
  }
}
