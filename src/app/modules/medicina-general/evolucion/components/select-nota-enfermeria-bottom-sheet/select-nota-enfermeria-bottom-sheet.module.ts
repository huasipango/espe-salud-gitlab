import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectNotaEnfermeriaBottomSheetComponent } from './select-nota-enfermeria-bottom-sheet.component';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatButtonModule} from '@angular/material/button';
import {FlexModule} from '@angular/flex-layout';
import {IconModule} from '@visurel/iconify-angular';
import {MatListModule} from '@angular/material/list';
import {NotaEnfermeriaItemModule} from 'src/app/modules/enfermeria/notas-enfermeria/components/nota-enfermeria-item/nota-enfermeria-item.module';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';



@NgModule({
  declarations: [SelectNotaEnfermeriaBottomSheetComponent],
  imports: [
    CommonModule,
    MatBottomSheetModule,
    MatButtonModule,
    FlexModule,
    IconModule,
    MatListModule,
    NotaEnfermeriaItemModule,
    LoadingModule,
    MatIconModule,
    MatTooltipModule
  ],
  exports: [
    MatBottomSheetModule
  ],
  entryComponents: [SelectNotaEnfermeriaBottomSheetComponent]
})
export class SelectNotaEnfermeriaBottomSheetModule { }
