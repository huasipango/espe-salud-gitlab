import { Component, OnInit } from '@angular/core';
import {MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {NotaEnfermeriaCreateComponent} from 'src/app/modules/enfermeria/notas-enfermeria/components/nota-enfermeria-create/nota-enfermeria-create.component';
import {filter} from 'rxjs/operators';
import {NotaEnfermeria} from 'src/app/modules/enfermeria/notas-enfermeria/models/nota-enfermeria.model';
import {MatDialog} from '@angular/material/dialog';
import icAdd from '@iconify/icons-ic/twotone-add';
import icDelete from '@iconify/icons-ic/twotone-delete';

import {Observable} from 'rxjs';
import {NotaEnfermeriaService} from 'src/app/modules/enfermeria/notas-enfermeria/services/nota-enfermeria.service';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {LoadingService} from 'src/app/core/services/loading.service';

@Component({
  selector: 'vex-select-nota-enfermeria-bottom-sheet',
  templateUrl: './select-nota-enfermeria-bottom-sheet.component.html',
  styleUrls: ['./select-nota-enfermeria-bottom-sheet.component.scss']
})
export class SelectNotaEnfermeriaBottomSheetComponent implements OnInit {
  icAdd = icAdd;
  icDelete = icDelete;
  notasEnfermeria: NotaEnfermeria[];

  constructor(
    private dialog: MatDialog,
    private notaEnfermeriaService: NotaEnfermeriaService,
    private pacienteGlobalService: PacienteGlobalService,
    private loadingService: LoadingService,
    private _bottomSheetRef: MatBottomSheetRef<SelectNotaEnfermeriaBottomSheetComponent>,
  ) { }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente) => {
        if (paciente) {
          this.loadNotaEnfermeriaByPaciente(paciente.id);
        }
      });
  }
  loadNotaEnfermeriaByPaciente(idPaciente: number): void {
    this.loadingService.showLoaderUntilCompleted(
      this.notaEnfermeriaService.getNotasEnfermeriaByPacienteAndEvolucionEmpty(idPaciente)
    ).subscribe((data) => {
        this.notasEnfermeria = data;
      });
  }

  createNotaEnfermeria() {
    this.dialog.open(NotaEnfermeriaCreateComponent, {
      width: '900px',
      maxWidth: '100%',
      disableClose: true
    }).beforeClosed().pipe(
      filter<NotaEnfermeria>(Boolean)
    ).subscribe(value => {
      if (value) {
        this._bottomSheetRef.dismiss(value);
      }
    });
  }

  onNotaEnfermeriaChange(nota: NotaEnfermeria): void {
    this._bottomSheetRef.dismiss(nota);
  }

  deleteFromList(id: number) {
    let index = this.notasEnfermeria.findIndex(d => d.id === id); //find index in your array
    this.notasEnfermeria.splice(index, 1);//remove element from arr
  }
}
