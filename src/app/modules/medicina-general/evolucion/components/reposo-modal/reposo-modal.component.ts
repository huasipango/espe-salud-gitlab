import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ThemePalette} from '@angular/material/core';
import icClose from '@iconify/icons-ic/twotone-close';
import icCheck from '@iconify/icons-ic/check';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {LoadingService} from 'src/app/core/services/loading.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {EvolucionReportService} from 'src/app/modules/medicina-general/evolucion/services/evolucion-report.service';
import {CertificadoReposo} from 'src/app/modules/medicina-general/evolucion/models/certificado-reposo.model';

@Component({
  selector: 'vex-reposo-modal',
  templateUrl: './reposo-modal.component.html',
  styleUrls: ['./reposo-modal.component.scss']
})
export class ReposoModalComponent implements OnInit {

  form: FormGroup;
  public color: ThemePalette = 'primary';

  maxDate = new Date();
  icClose = icClose;
  icCheck = icCheck;
  loading = false;
  errorMessages = FORM_ERROR_MESSAGES;
  constructor(
    @Inject(MAT_DIALOG_DATA) public idEvolucion: string,
    private dialogRef: MatDialogRef<ReposoModalComponent>,
    private fb: FormBuilder,
    private loadingService: LoadingService,
    private snackbar: MatSnackBar,
    private evolucionReportService: EvolucionReportService,
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      fechaInicio: [this.maxDate, Validators.required],
      fechaFin: [null, Validators.required],
      condicionPaciente : ['', Validators.required],
      recomendacion: ['', Validators.required]
    });
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  createPdf() {
    const data: CertificadoReposo = this.form.value;
    this.loadingService.showLoaderUntilCompleted(
      this.evolucionReportService.getCertificadoReposo(data, this.idEvolucion)
    ).subscribe(() => {
      this.dialogRef.close();
    });
  }
}
