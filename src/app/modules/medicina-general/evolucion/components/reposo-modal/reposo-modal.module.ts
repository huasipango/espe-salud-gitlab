import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ExtendedModule, FlexModule} from '@angular/flex-layout';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {IconModule} from '@visurel/iconify-angular';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import {ReposoModalComponent} from 'src/app/modules/medicina-general/evolucion/components/reposo-modal/reposo-modal.component';



@NgModule({
  declarations: [ReposoModalComponent],
  imports: [
    CommonModule,
    FlexModule,
    ContainerModule,
    IconModule,
    BreadcrumbsModule,
    ExtendedModule,
    MatButtonModule,
    MatTooltipModule,
    MatIconModule,
    ReactiveFormsModule,
    MatMenuModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatCheckboxModule,
    FormsModule,
    MatDialogModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    LoadingModule
  ]
})
export class ReposoModalModule { }
