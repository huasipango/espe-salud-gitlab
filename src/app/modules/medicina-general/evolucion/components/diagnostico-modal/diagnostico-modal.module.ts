import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DiagnosticoModalComponent } from './diagnostico-modal.component';
import {FlexModule} from '@angular/flex-layout';
import {ReactiveFormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {IconModule} from '@visurel/iconify-angular';
import {MatInputModule} from '@angular/material/input';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSelectModule} from '@angular/material/select';



@NgModule({
  declarations: [DiagnosticoModalComponent],
  imports: [
    CommonModule,
    FlexModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    IconModule,
    MatInputModule,
    MatAutocompleteModule,
    MatSelectModule
  ]
})
export class DiagnosticoModalModule { }
