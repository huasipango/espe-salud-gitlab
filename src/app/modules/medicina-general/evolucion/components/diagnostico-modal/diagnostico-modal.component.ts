import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Evolucion} from 'src/app/modules/medicina-general/evolucion/models/evolucion.model';
import {Observable} from 'rxjs';
import {TipoEnfermedadCIE10} from 'src/app/core/models/catalogo/tipo-enfermedad-cie10.model';
import {SimpleCatalogUtil} from 'src/app/core/utils/simple-catalog-util';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {EvolucionSharedService} from 'src/app/modules/medicina-general/evolucion/services/evolucion-shared.service';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {Diagnostico} from 'src/app/modules/medicina-general/evolucion/models/diagnostico.model';
import {debounceTime, distinctUntilChanged, startWith} from 'rxjs/operators';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import icClose from '@iconify/icons-ic/twotone-close';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPerson from '@iconify/icons-ic/twotone-person';
import icLaptoMedical from '@iconify/icons-fa-solid/laptop-medical';
import icCode from '@iconify/icons-fa-solid/user-tie';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';
import icDescription from '@iconify/icons-ic/outline-description';
import {Dispensario} from 'src/app/core/models/catalogo/dispensario.model';
import {AuthService} from 'src/app/core/auth/auth.service';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import {LoadingService} from 'src/app/core/services/loading.service';
import {DiagnosticoService} from 'src/app/modules/medicina-general/evolucion/services/diagnostico.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {forbiddenObjectValidator} from 'src/app/shared/validators/forbbiden-object-validator';

@UntilDestroy()
@Component({
  selector: 'vex-diagnostico-modal',
  templateUrl: './diagnostico-modal.component.html',
  styleUrls: ['./diagnostico-modal.component.scss']
})
export class DiagnosticoModalComponent implements OnInit {
  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  evolucionActual: Evolucion;
  cieCtrl: FormControl;
  filteredCodigosCie: Observable<TipoEnfermedadCIE10[]>;
  dispensarios: Dispensario[] = [];
  usuarioSalud: SaludUser;
  tipoList = SimpleCatalogUtil.tipo;
  morbilidadList: any;
  validated: boolean;
  condicionesDiagnosticoList: any;
  codigo: string;

  icClose = icClose;
  icDelete = icDelete;
  icPerson = icPerson;
  icLaptoMedical = icLaptoMedical;
  icDescription = icDescription;
  icCode = icCode;
  icArrowDropDown = icArrowDropDown;

  errorMessages = FORM_ERROR_MESSAGES;

  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: any,
    private dialogRef: MatDialogRef<DiagnosticoModalComponent>,
    private fb: FormBuilder,
    private evolucionSharedService: EvolucionSharedService,
    private catalogoService: CatalogoService,
    private authService: AuthService,
    private loadingService: LoadingService,
    private diagnosticoService: DiagnosticoService,
    private snackbar: MatSnackBar,
  ) {

  }

  ngOnInit() {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as Diagnostico;
    }
    this.authService.saludUserData$
      .subscribe((saludUser) => {
        if (saludUser) {
          this.usuarioSalud = saludUser;
        }
      });
    this.cieCtrl = new FormControl(
      '', [Validators.required, forbiddenObjectValidator]
    );
    if (this.defaults.codigoCIE) {
      this.catalogoService.getCodigoCIE10(this.defaults.codigoCIE)
        .subscribe((codigo) => {
          if (codigo) {
            this.cieCtrl.patchValue(codigo);
            this.cargarCondicionCIE10(null, codigo.codigo);
          }
        });
    }
    this.cieCtrl.valueChanges
      .pipe(
        startWith(''),
        debounceTime(400),
        distinctUntilChanged(),
        untilDestroyed(this)
      ).subscribe(value => this.searchCiuo(value || ''));
    this.initForm();

    this.catalogoService.getDispensarios()
      .subscribe(
        (data) => {
          this.dispensarios = data;
        }
      );
  }

  initForm(): void {
    this.form = this.fb.group({
      id: this.defaults.id || null,
      codigoCIE: this.cieCtrl,
      tipo: [this.defaults.tipo || null, Validators.required],
      cronologia: this.defaults.cronologia || null,
      condicionDiagnostico: this.defaults.condicionDiagnostico || null,
    });
    if (this.defaults.condicionDiagnostico) {
      this.cargarDiagnosticos(this.defaults.cronologia);
    }
    if (this.defaults.tipo){
      this.changeTipo();
    }
  }

  save() {
      const diagnostico = this.form.getRawValue();
      diagnostico.codigoCIE = diagnostico.codigoCIE.codigo;
      this.dialogRef.close(diagnostico);
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  searchCiuo(value: any) {
    if (value !== '' && typeof value === 'string' && value.trim().length > 1) {
      this.filteredCodigosCie = this.catalogoService.getCodigosCIE10ByQuery(value);
    }
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  cargarCondicionCIE10(event: any, codigo?: string) {
    this.codigo = codigo ? codigo : event.option.value.codigo;
    if (this.codigo.substring(0, 1) === 'Z') {
      this.form.get('tipo').setValue('PREVENCION');
      this.form.get('tipo').disable();
      this.form.get('condicionDiagnostico').disable();
      this.form.get('condicionDiagnostico').setValue(null);
      this.changeTipo();
    } else {
      this.form.get('tipo').setValue('MORBILIDAD');
      this.form.get('tipo').disable();
      this.form.get('condicionDiagnostico').enable();
      const cronologia = this.form.get('cronologia').value;
      this.cargarDiagnosticos(cronologia);
    }
  }

  cargarDiagnosticos(value: string) {
    this.changeTipo();
    const morbilidad = this.morbilidadList.find(x => x.nombre === value);
    this.condicionesDiagnosticoList = morbilidad?.condicionesDiagnostico;
  }

  displayFn(cie: TipoEnfermedadCIE10): string {
    return cie ? `${cie.codigo} -- ${cie.nombre}` : '';
  }

  changeTipo(): void {
    if (this.form.get('tipo').value === 'MORBILIDAD'){
      this.validated = true;
      this.morbilidadList = SimpleCatalogUtil.morbilidadCatalogo;
    } else {
      this.validated = false;
      this.morbilidadList = SimpleCatalogUtil.prevencionCatalogo;
    }
  }
}
