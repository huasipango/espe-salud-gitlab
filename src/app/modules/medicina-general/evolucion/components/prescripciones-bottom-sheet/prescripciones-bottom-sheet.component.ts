import {Component, Inject, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Prescripcion} from 'src/app/modules/medicina-general/evolucion/models/prescripcion.model';
import {Evolucion} from 'src/app/modules/medicina-general/evolucion/models/evolucion.model';
import {PopoverRef} from 'src/@vex/components/popover/popover-ref';
import {LoadingService} from 'src/app/core/services/loading.service';
import {PrescripcionService} from 'src/app/modules/medicina-general/evolucion/services/prescripcion.service';
import {EvolucionReportService} from 'src/app/modules/medicina-general/evolucion/services/evolucion-report.service';
import icClose from '@iconify/icons-ic/twotone-close';
import icDownload from '@iconify/icons-ic/cloud-download';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from '@angular/material/bottom-sheet';

@Component({
  selector: 'vex-prescripciones-bottom-sheet',
  templateUrl: './prescripciones-bottom-sheet.component.html',
  styleUrls: ['./prescripciones-bottom-sheet.component.scss']
})
export class PrescripcionesBottomSheetComponent implements OnInit {

  prescripciones: Observable<Prescripcion[]>;
  icClose = icClose;
  icDownload = icDownload;
  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) public evolucion: Evolucion,
    private _bottomSheetRef: MatBottomSheetRef<PrescripcionesBottomSheetComponent>,
    private loadingService: LoadingService,
    private prescripcionService: PrescripcionService,
    private evolucionReportService: EvolucionReportService,
  ) { }

  ngOnInit(): void {
    this.prescripciones = this.loadingService.showLoaderUntilCompleted(
      this.prescripcionService.getPrescripciones(this.evolucion.id)
    );
  }

  close(): void {
    this._bottomSheetRef.dismiss();
  }

  downloadRecetaMedica() {
    this.loadingService.showLoaderUntilCompleted(
      this.evolucionReportService.getRecetaMedica(this.evolucion.id)
    ).subscribe();
  }
}
