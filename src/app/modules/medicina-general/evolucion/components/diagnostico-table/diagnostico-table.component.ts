import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {stagger20ms} from 'src/@vex/animations/stagger.animation';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {Diagnostico} from 'src/app/modules/medicina-general/evolucion/models/diagnostico.model';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import icSearch from '@iconify/icons-ic/twotone-search';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDeleteForever from '@iconify/icons-ic/twotone-delete-forever';
import icAdd from '@iconify/icons-ic/twotone-add';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
// eslint-disable-next-line max-len
import {DiagnosticoModalComponent} from 'src/app/modules/medicina-general/evolucion/components/diagnostico-modal/diagnostico-modal.component';
import {EvolucionSharedService} from 'src/app/modules/medicina-general/evolucion/services/evolucion-shared.service';
import {Evolucion} from 'src/app/modules/medicina-general/evolucion/models/evolucion.model';
import {DiagnosticoService} from 'src/app/modules/medicina-general/evolucion/services/diagnostico.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {switchMap} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'vex-diagnostico-table',
  templateUrl: './diagnostico-table.component.html',
  styleUrls: ['./diagnostico-table.component.scss'],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ],
  animations: [
    stagger20ms,
    fadeInUp400ms,
    scaleFadeIn400ms
  ]
})
export class DiagnosticoTableComponent implements OnInit, AfterViewInit {
  @Input() diagnosticos: Diagnostico[];
  @Output() returnDiagnosticos = new EventEmitter();

  tableColumns: TableColumn<Diagnostico>[] = [
    {label: 'Código CIE', property: 'codigoCIE', type: 'text', visible: true},
    {label: 'Tipo', property: 'tipo', type: 'text', visible: true},
    {label: 'Cronologia', property: 'cronologia', type: 'text', visible: true},
    {label: 'Condición Diagnostico', property: 'condicionDiagnostico', type: 'text', visible: true},
    {label: 'Acciones', property: 'menu', type: 'button', visible: true}
  ];

  dataSource: MatTableDataSource<Diagnostico> | null;
  icSearch = icSearch;
  icAdd = icAdd;
  icDeleteForever = icDeleteForever;
  icEdit = icEdit;
  evolucionCodigo: string;
  messages = USER_MESSAGES;

  // eslint-disable-next-line @typescript-eslint/member-ordering
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private evolucionSharedService: EvolucionSharedService,
    private diagnosticoService: DiagnosticoService,
    private snackbar: MatSnackBar
  ) {
  }

  ngOnInit(): void {
    this.evolucionSharedService.evolucion$.subscribe((data: Evolucion) => {
      if (data){
        this.evolucionCodigo = data.id;
      }
    });
    if (!this.diagnosticos) {
      this.diagnosticos = [];
    }
    this.dataSource = new MatTableDataSource();
    this.dataSource.data = this.diagnosticos;
  }

  getDiagnosticos(): void {
    this.diagnosticoService.getDiagnosticos(this.evolucionCodigo)
      .subscribe((diagnosticos) => {
        this.diagnosticos = diagnosticos;
        this.dataSource.data = this.diagnosticos;
        this.returnDataToParentComponent();
      });
  }

  createDiagnostico() {
    this.dialog
      .open(DiagnosticoModalComponent, {
        width: '700px',
        maxWidth: '100%'
      }).afterClosed()
      .subscribe((diagnostico: Diagnostico) => {
        if (diagnostico) {
          this.diagnosticoService.createDiagnostico(diagnostico, this.evolucionCodigo)
            .subscribe((response) => {
              if (response) {
                this.showNotification(this.messages.createdSuccessMessage, 'CERRAR');
                this.getDiagnosticos();
              }
            }, error => {
              this.showNotification(this.messages.createdFailedMessage, 'CERRAR');
            });
        }
      });
  }

  updateDiagnostico(diagnostico: Diagnostico) {
    this.dialog.open(DiagnosticoModalComponent, {
      data: diagnostico,
      width: '700px',
      maxWidth: '100%'
    }).afterClosed().subscribe(diagnosticoModificado => {
      if (diagnosticoModificado) {
        this.diagnosticoService.updateDiagnostico(
          diagnostico.id,
          diagnosticoModificado,
          this.evolucionCodigo
        ).subscribe((response) => {
          if (response) {
            const index = this.diagnosticos.findIndex((diagnosticoExistente) =>
              diagnosticoExistente.id === diagnosticoModificado.id);
            this.diagnosticos[index] = response;
            this.dataSource.connect().next(this.diagnosticos);
            this.returnDataToParentComponent();
            this.showNotification(this.messages.updatedSuccessMessage, 'CERRAR');
          }
        }, error => {
          this.showNotification(this.messages.updatedFailedMessage, 'CERRAR');
        });
      }
    });
  }

  openDeleteDialog(diagnostico: Diagnostico) {
    this.dialog.open(DeleteModalComponent, {
      data: this.messages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') {
        this.deleteDiagnostico(diagnostico);
      }
    });
  }

  deleteDiagnostico(diagnostico: Diagnostico) {
    this.diagnosticoService.deleteDiagnostico(diagnostico.id, this.evolucionCodigo)
      .subscribe((response) => {
        if (response) {
          const index = this.diagnosticos.findIndex(d => d.id === diagnostico.id);
          this.diagnosticos.splice(index, 1);
          this.dataSource.connect().next(this.diagnosticos);
          this.returnDataToParentComponent();
          this.showNotification(this.messages.deletedSuccessMessage, 'CERRAR');
        } else {
          this.showNotification(this.messages.deleteFailMessage, 'CERRAR');
        }
      });
  }

  updateIdsDiagnosticos() {
    let id = 1;
    this.diagnosticos.forEach(diagnostico => {
      diagnostico.id = id;
      id++;
    });
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  trackByProperty<T>(column: TableColumn<T>) {
    return column.property;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }
  get visibleColumns() {
    return this.tableColumns
      .filter(column => column.visible)
      .map(column => column.property);
  }

  returnDataToParentComponent() {
    this.returnDiagnosticos.emit(this.diagnosticos);
  }

}
