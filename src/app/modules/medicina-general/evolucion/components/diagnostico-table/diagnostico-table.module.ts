import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DiagnosticoTableComponent } from './diagnostico-table.component';
import {FlexModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {IconModule} from '@visurel/iconify-angular';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatIconModule} from '@angular/material/icon';



@NgModule({
  declarations: [DiagnosticoTableComponent],
  exports: [
    DiagnosticoTableComponent
  ],
  imports: [
    CommonModule,
    FlexModule,
    MatButtonModule,
    IconModule,
    MatTableModule,
    MatSortModule,
    MatIconModule
  ]
})
export class DiagnosticoTableModule { }
