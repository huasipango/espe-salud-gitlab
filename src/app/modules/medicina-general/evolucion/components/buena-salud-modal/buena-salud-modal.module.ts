import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuenaSaludModalComponent } from './buena-salud-modal.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {MatInputModule} from '@angular/material/input';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import {MatRadioModule} from '@angular/material/radio';



@NgModule({
  declarations: [BuenaSaludModalComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatDialogModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    IconModule,
    MatInputModule,
    LoadingModule,
    MatRadioModule
  ]
})
export class BuenaSaludModalModule { }
