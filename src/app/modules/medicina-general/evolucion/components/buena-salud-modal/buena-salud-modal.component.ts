import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ThemePalette} from '@angular/material/core';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import icClose from '@iconify/icons-ic/twotone-close';
import icCheck from '@iconify/icons-ic/check';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {LoadingService} from 'src/app/core/services/loading.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {EvolucionReportService} from 'src/app/modules/medicina-general/evolucion/services/evolucion-report.service';
import {CertificadoBuenaSalud} from 'src/app/modules/medicina-general/evolucion/models/certificado-buena-salud.model';

@Component({
  selector: 'vex-buena-salud-modal',
  templateUrl: './buena-salud-modal.component.html',
  styleUrls: ['./buena-salud-modal.component.scss']
})
export class BuenaSaludModalComponent implements OnInit {

  form: FormGroup;
  public color: ThemePalette = 'primary';

  maxDate = new Date();
  icClose = icClose;
  icCheck = icCheck;
  loading = false;
  errorMessages = FORM_ERROR_MESSAGES;

  constructor(
    @Inject(MAT_DIALOG_DATA) public idEvolucion: string,
    private dialogRef: MatDialogRef<BuenaSaludModalComponent>,
    private fb: FormBuilder,
    private loadingService: LoadingService,
    private snackbar: MatSnackBar,
    private evolucionReportService: EvolucionReportService,
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      aptitudMedica: ['APTO', Validators.required],
      observaciones: ['', Validators.required],
      recomendacion: ['', Validators.required]
    });
  }

  createPdf() {
    const data: CertificadoBuenaSalud = this.form.value;
    this.loadingService.showLoaderUntilCompleted(
      this.evolucionReportService.getCertificadoBuenaSalud(data, this.idEvolucion)
    ).subscribe(() => {
      this.dialogRef.close();
    });
  }
}
