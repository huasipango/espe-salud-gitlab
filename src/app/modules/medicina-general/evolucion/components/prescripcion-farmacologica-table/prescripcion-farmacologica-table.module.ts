import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrescripcionFarmacologicaTableComponent } from './prescripcion-farmacologica-table.component';
import {MatDialogModule} from '@angular/material/dialog';
import {FlexModule} from '@angular/flex-layout';
import {MatInputModule} from '@angular/material/input';
import {IconModule} from '@visurel/iconify-angular';
import {MatButtonModule} from '@angular/material/button';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatIconModule} from '@angular/material/icon';



@NgModule({
  declarations: [PrescripcionFarmacologicaTableComponent],
  exports: [
    PrescripcionFarmacologicaTableComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    FlexModule,
    MatInputModule,
    IconModule,
    MatButtonModule,
    MatTableModule,
    MatSortModule,
    MatIconModule
  ]
})
export class PrescripcionFarmacologicaTableModule { }
