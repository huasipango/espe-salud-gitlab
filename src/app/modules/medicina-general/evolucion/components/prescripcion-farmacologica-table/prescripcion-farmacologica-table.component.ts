import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {stagger20ms} from 'src/@vex/animations/stagger.animation';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import icSearch from '@iconify/icons-ic/twotone-search';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDeleteForever from '@iconify/icons-ic/twotone-delete-forever';
import icAdd from '@iconify/icons-ic/twotone-add';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {PrescripcionFarmacologicaModalComponent} from 'src/app/modules/medicina-general/evolucion/components/prescripcion-farmacologica-modal/prescripcion-farmacologica-modal.component';
import {RepertorioMedicamento} from 'src/app/core/models/catalogo/repertorio-medicamento.model';
import {Prescripcion} from 'src/app/modules/medicina-general/evolucion/models/prescripcion.model';
import {EvolucionSharedService} from 'src/app/modules/medicina-general/evolucion/services/evolucion-shared.service';
import {DiagnosticoService} from 'src/app/modules/medicina-general/evolucion/services/diagnostico.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Evolucion} from 'src/app/modules/medicina-general/evolucion/models/evolucion.model';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {PrescripcionService} from 'src/app/modules/medicina-general/evolucion/services/prescripcion.service';
import {EvolucionService} from 'src/app/modules/medicina-general/evolucion/services/evolucion.service';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';

@Component({
  selector: 'vex-prescripcion-farmacologica-table',
  templateUrl: './prescripcion-farmacologica-table.component.html',
  styleUrls: ['./prescripcion-farmacologica-table.component.scss'],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ],
  animations: [
    stagger20ms,
    fadeInUp400ms,
    scaleFadeIn400ms
  ]
})
export class PrescripcionFarmacologicaTableComponent implements OnInit, AfterViewInit {
  @Input() prescripciones: Prescripcion[];
  @Output() returnPrescripciones = new EventEmitter();

  tableColumns: TableColumn<Prescripcion>[] = [
    {label: 'Medicamento', property: 'medicamento', type: 'object', visible: true},
    {label: 'Presentación', property: 'presentacion', type: 'text', visible: true},
    {label: 'Cantidad', property: 'cantidad', type: 'text', visible: true},
    {label: 'Indicaciones', property: 'indicacion', type: 'text', visible: false},
    {label: 'Acciones', property: 'menu', type: 'button', visible: true}
  ];

  dataSource: MatTableDataSource<Prescripcion> | null;

  icSearch = icSearch;
  icAdd = icAdd;
  icDeleteForever = icDeleteForever;
  icEdit = icEdit;
  evolucionCodigo: string;
  messages = USER_MESSAGES;


  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private evolucionService: EvolucionService,
    private evolucionSharedService: EvolucionSharedService,
    private diagnosticoService: DiagnosticoService,
    private snackbar: MatSnackBar,
    private prescripcionService: PrescripcionService
  ) {
  }

  ngOnInit(): void {
    this.evolucionSharedService.evolucion$.subscribe((data: Evolucion) => {
      if (data){
        this.evolucionCodigo = data.id;
      }
    });
    if (!this.prescripciones) {
      this.prescripciones = [];
    }
    this.dataSource = new MatTableDataSource();
    this.dataSource.data = this.prescripciones;
  }

  getPrescripciones(): void {
    this.prescripcionService.getPrescripciones(this.evolucionCodigo)
      .subscribe((prescripciones) => {
        this.prescripciones = prescripciones;
        this.dataSource.data = this.prescripciones;
        this.returnDataToParentComponent();
      });
  }

  createPrescripcion() {
    this.dialog
      .open(PrescripcionFarmacologicaModalComponent, {
        width: '700px',
        maxWidth: '100%'
      }).afterClosed()
      .subscribe((prescripcion: Prescripcion) => {
        if (prescripcion) {
          this.prescripcionService.createPrescripcion(prescripcion, this.evolucionCodigo)
            .subscribe((response) => {
              if (response) {
                this.prescripciones.push(response);
                this.dataSource.data = this.prescripciones;
                this.getPrescripciones();
                this.showNotification(this.messages.createdSuccessMessage, 'CERRAR');
              }
            }, error => {
              this.showNotification(this.messages.createdFailedMessage, 'CERRAR');
            });
        }
      });
  }

  updatePrescripcion(prescripcion: Prescripcion) {
    this.dialog.open(PrescripcionFarmacologicaModalComponent, {
      data: prescripcion,
      width: '700px',
      maxWidth: '100%'
    }).afterClosed().subscribe(prescripcionModificada => {
      if (prescripcionModificada) {
        this.prescripcionService.updatePrescripcion(
          prescripcion.id,
          prescripcionModificada,
          this.evolucionCodigo
        ).subscribe((response: Prescripcion) => {
          if (response) {
            const index = this.prescripciones.findIndex((prescripcionExistente) =>
              prescripcionExistente.id === prescripcionModificada.id);
            this.prescripciones[index] = response;
            this.dataSource.connect().next(this.prescripciones);
            this.showNotification(this.messages.updatedSuccessMessage, 'CERRAR');
          }
        }, error => {
          this.showNotification(this.messages.updatedFailedMessage, 'CERRAR');
        });
      }
    });
  }

  openDeleteDialog(prescripcion: Prescripcion) {
    this.dialog.open(DeleteModalComponent, {
      data: this.messages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') {
        this.deletePrescripcion(prescripcion);
      }
    });
  }

  deletePrescripcion(prescripcion: Prescripcion) {
    this.prescripcionService.deletePrescripcion(prescripcion.id, this.evolucionCodigo)
      .subscribe((response: boolean) => {
        if (response) {
          const index = this.prescripciones.findIndex(p => p.id === prescripcion.id);
          this.prescripciones.splice(index, 1);
          this.dataSource.connect().next(this.prescripciones);
          this.showNotification(this.messages.deletedSuccessMessage, 'CERRAR');
        } else {
          this.showNotification(this.messages.deleteFailMessage, 'CERRAR');
        }
      });
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  returnDataToParentComponent() {
    this.returnPrescripciones.emit(this.prescripciones);
  }

  trackByProperty<T>(column: TableColumn<T>) {
    return column.property;
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  get visibleColumns() {
    return this.tableColumns
      .filter(column => column.visible)
      .map(column => column.property);
  }

  getMedicamento(medicamento: RepertorioMedicamento): string {
    return medicamento ? medicamento.nombre : '';
  }
}
