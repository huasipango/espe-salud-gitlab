import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectTipoExamenLaboratorioComponent } from './select-tipo-examen-laboratorio.component';
import {MatListModule} from '@angular/material/list';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';



@NgModule({
  declarations: [SelectTipoExamenLaboratorioComponent],
  exports: [
    SelectTipoExamenLaboratorioComponent
  ],
  imports: [
    CommonModule,
    MatListModule,
    FlexLayoutModule,
    MatIconModule,
    IconModule,
    MatButtonModule,
    MatTooltipModule
  ]
})
export class SelectTipoExamenLaboratorioModule { }
