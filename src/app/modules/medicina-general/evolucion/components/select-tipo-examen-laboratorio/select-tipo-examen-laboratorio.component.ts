import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {TipoExamenLaboratorio} from 'src/app/core/models/catalogo/tipo-examen-laboratorio.model';
import {Observable} from 'rxjs';
import {MatListOption} from '@angular/material/list';
import {TipoDetalleExamenLaboratorio} from 'src/app/core/models/catalogo/tipo-detalle-examen-laboratorio.model';
import icDelete from '@iconify/icons-ic/twotone-delete';

@Component({
  selector: 'vex-select-tipo-examen-laboratorio',
  templateUrl: './select-tipo-examen-laboratorio.component.html',
  styleUrls: ['./select-tipo-examen-laboratorio.component.scss']
})
export class SelectTipoExamenLaboratorioComponent implements OnInit {
  @Output() returnParametros = new EventEmitter();

  tipoExamenes: Observable<TipoExamenLaboratorio[]>;
  parameters: Observable<TipoDetalleExamenLaboratorio[]>;
  parametrosSolicitados: TipoDetalleExamenLaboratorio[] = [];
  icDelete = icDelete;
  constructor(
    private catalogoService: CatalogoService
  ) { }

  ngOnInit(): void {
    this.tipoExamenes = this.catalogoService.getTiposExamenLaboratorio();
  }

  searchParameters($event: MatListOption) {
    const examen: TipoExamenLaboratorio = $event.value;
    this.parameters = this.catalogoService.getTipoDetalleExameneLaboratorio(examen.id);
  }

  setParameter($event: MatListOption) {
    const parameter: TipoDetalleExamenLaboratorio = $event.value;
    if ($event.selected) {
      this.parametrosSolicitados.push(parameter);
      this.returnDataToParentComponent();
    } else {
      this.deleteFromList(parameter.id);
    }
  }

  deleteFromList(id: number) {
    this.parametrosSolicitados.splice(
      this.parametrosSolicitados.findIndex((existing: TipoDetalleExamenLaboratorio) =>
        existing.id === id), 1);
    this.returnDataToParentComponent();
  }

  remove(id: number) {
    const founded = this.parametrosSolicitados.find(p => p.id === id);
    if (founded) {
      this.deleteFromList(id);
    }
  }

  checkIfSelect(id: number) {
    const founded = this.parametrosSolicitados.find(p => p.id === id);
    return !!founded;
  }

  returnDataToParentComponent() {
    this.returnParametros.emit(this.parametrosSolicitados);
  }
}
