import {Component, Inject, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Diagnostico} from 'src/app/modules/medicina-general/evolucion/models/diagnostico.model';
import {LoadingService} from 'src/app/core/services/loading.service';
import {DiagnosticoService} from 'src/app/modules/medicina-general/evolucion/services/diagnostico.service';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import icClose from '@iconify/icons-ic/twotone-close';

@Component({
  selector: 'vex-diagnosticos-bottom-sheet',
  templateUrl: './diagnosticos-bottom-sheet.component.html',
  styleUrls: ['./diagnosticos-bottom-sheet.component.scss']
})
export class DiagnosticosBottomSheetComponent implements OnInit {

  diagnosticos: Observable<Diagnostico[]>;
  icClose = icClose;
  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) private idEvolucion: string,
    private _bottomSheetRef: MatBottomSheetRef<DiagnosticosBottomSheetComponent>,
    private loadingService: LoadingService,
    private diagnosticoService: DiagnosticoService
  ) { }

  ngOnInit(): void {
    this.diagnosticos = this.loadingService.showLoaderUntilCompleted(
      this.diagnosticoService.getDiagnosticos(this.idEvolucion)
    );
  }

  close(): void {
    this._bottomSheetRef.dismiss();
  }

}
