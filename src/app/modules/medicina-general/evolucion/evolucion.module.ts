import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EvolucionRoutingModule} from './evolucion-routing.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatListModule} from '@angular/material/list';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import {MatTooltipModule} from '@angular/material/tooltip';
import {StripHtmlModule} from 'src/@vex/pipes/strip-html/strip-html.module';
import { DiagnosticosBottomSheetComponent } from './components/diagnosticos-bottom-sheet/diagnosticos-bottom-sheet.component';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import { PrescripcionesBottomSheetComponent } from './components/prescripciones-bottom-sheet/prescripciones-bottom-sheet.component';
import { NotaEnfermeriaBottomSheetComponent } from './components/nota-enfermeria-bottom-sheet/nota-enfermeria-bottom-sheet.component';
import {NotaEnfermeriaItemModule} from 'src/app/modules/enfermeria/notas-enfermeria/components/nota-enfermeria-item/nota-enfermeria-item.module';

@NgModule({
  declarations: [DiagnosticosBottomSheetComponent, PrescripcionesBottomSheetComponent, NotaEnfermeriaBottomSheetComponent],
  imports: [
    CommonModule,
    EvolucionRoutingModule,
    FlexLayoutModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    IconModule,
    LoadingModule,
    MatTooltipModule,
    StripHtmlModule,
    MatBottomSheetModule,
    NotaEnfermeriaItemModule,
  ]
})
export class EvolucionModule {
}
