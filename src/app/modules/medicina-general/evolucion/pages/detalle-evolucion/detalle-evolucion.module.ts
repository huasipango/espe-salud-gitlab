import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetalleEvolucionComponent } from './detalle-evolucion.component';
import {PageLayoutModule} from 'src/@vex/components/page-layout/page-layout.module';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {MatTabsModule} from '@angular/material/tabs';
import {DetalleEvolucionRoutingModule} from './detalle-evolucion-routing.module';
import {IconModule} from '@visurel/iconify-angular';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {ExtendedModule, FlexModule} from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
@NgModule({
  declarations: [DetalleEvolucionComponent],
  imports: [
    CommonModule,
    DetalleEvolucionRoutingModule,
    PageLayoutModule,
    BreadcrumbsModule,
    MatTabsModule,
    IconModule,
    ContainerModule,
    FlexModule,
    MatIconModule,
    ExtendedModule,
    MatButtonModule,
    MatTooltipModule,
  ],
  exports: [DetalleEvolucionComponent]
})
export class DetalleEvolucionModule { }
