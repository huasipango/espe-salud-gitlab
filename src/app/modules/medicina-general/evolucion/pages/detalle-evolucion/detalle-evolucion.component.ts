import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Evolucion} from '../../models/evolucion.model';
import {Link} from 'src/@vex/interfaces/link.interface';
import {ActivatedRoute, Router} from '@angular/router';
import icCloudDownload from '@iconify/icons-ic/twotone-cloud-download';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import icAdd from '@iconify/icons-ic/twotone-add';
import icBack from '@iconify/icons-ic/arrow-back';

@Component({
  selector: 'vex-detalle-evolucion',
  templateUrl: './detalle-evolucion.component.html',
  styleUrls: [
    './detalle-evolucion.component.scss'
  ],
  animations: [
    scaleIn400ms,
    fadeInRight400ms,
    stagger40ms,
    fadeInUp400ms,
    scaleFadeIn400ms
  ]
})
export class DetalleEvolucionComponent implements OnInit {

  icCloudDownload = icCloudDownload;
  icAdd = icAdd;
  icBack = icBack;

  evolucion: Evolucion;
  evolucionCodigo = '';
  layoutCtrl = new FormControl('full');
  links: Link[] = [
    {
      label: 'NOTA EVOLUCIÓN',
      route: './',
      routerLinkActiveOptions: {exact: true}
    },
    {
      label: 'DIAGNÓSTICOS',
      route: './diagnosticos',
    },
    {
      label: 'PRESCRIPCIONES',
      route: './prescripciones',
    }
  ];

  constructor(
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(( params) => {
      const codigo = params.get('codigo');
      this.evolucionCodigo = codigo;
    });
  }

  crearNuevaEvolucion(): void {
    this.router.navigate(['../create'], {relativeTo: this.route});
  }

  navigateBack() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }
}
