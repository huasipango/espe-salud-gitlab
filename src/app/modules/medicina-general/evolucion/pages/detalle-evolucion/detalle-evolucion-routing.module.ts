import { NgModule } from '@angular/core';
import {ExtraOptions, RouterModule} from '@angular/router';
import {DetalleEvolucionComponent} from './detalle-evolucion.component';
import {VexRoutes} from 'src/@vex/interfaces/vex-route.interface';

const routes: VexRoutes = [
  {
    path: '',
    component: DetalleEvolucionComponent,
    data: {
      toolbarShadowEnabled: false,
      scrollDisabled: true
    },
    children: [
      {
        path: '',
        loadChildren: () => import('../informacion-evolucion/informacion-evolucion.module').then(m => m.InformacionEvolucionModule)
      },
      {
        path: 'procedimientos',
        loadChildren: () => import('../procedimientos/procedimientos.module').then(m => m.ProcedimientosModule)
      },
      {
        path: 'diagnosticos',
        loadChildren: () => import('../diagnosticos/diagnosticos.module').then(m => m.DiagnosticosModule)
      },
      {
        path: 'prescripciones',
        loadChildren: () => import('../prescripciones/prescripciones.module').then(m => m.PrescripcionesModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetalleEvolucionRoutingModule {
}
