import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {InformacionEvolucionComponent} from './informacion-evolucion.component';


const routes: Routes = [
  {
    path: '',
    component: InformacionEvolucionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InformacionEvolucionRoutingModule {
}
