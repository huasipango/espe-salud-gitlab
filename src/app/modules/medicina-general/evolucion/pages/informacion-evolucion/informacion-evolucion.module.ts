import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InformacionEvolucionComponent } from './informacion-evolucion.component';
import { InformacionEvolucionRoutingModule } from './informacion-evolucion-routing.module';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatDividerModule } from '@angular/material/divider';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import { PageLayoutModule } from 'src/@vex/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { WidgetLargeChartModule } from 'src/@vex/components/widgets/widget-large-chart/widget-large-chart.module';
import { WidgetTableModule } from 'src/@vex/components/widgets/widget-table/widget-table.module';
import { IconModule } from '@visurel/iconify-angular';
import { ContainerModule } from 'src/@vex/directives/container/container.module';
import {MatExpansionModule} from '@angular/material/expansion';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import {QuillModule} from 'ngx-quill';
import {QuillEvolucionConfiguration} from 'src/app/core/utils/quill-config';

@NgModule({
  declarations: [InformacionEvolucionComponent],
  imports: [
    CommonModule,
    InformacionEvolucionRoutingModule,
    CommonModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule,
    MatDividerModule,
    MatSortModule,
    MatCheckboxModule,
    MatIconModule,
    ReactiveFormsModule,
    WidgetLargeChartModule,
    FormsModule,
    WidgetTableModule,
    MatInputModule,
    MatButtonModule,
    MatMenuModule,
    IconModule,
    FormsModule,
    MatTooltipModule,
    ReactiveFormsModule,
    ContainerModule,
    MatSelectModule,
    MatButtonToggleModule,
    MatSnackBarModule,
    MatExpansionModule,
    LoadingModule,
    QuillModule.forRoot(QuillEvolucionConfiguration),
  ],
})
export class InformacionEvolucionModule {}
