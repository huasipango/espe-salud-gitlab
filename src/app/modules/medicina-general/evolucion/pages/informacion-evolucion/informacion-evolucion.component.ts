import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Evolucion} from '../../models/evolucion.model';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {ActivatedRoute, Router} from '@angular/router';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {EvolucionService} from '../../services/evolucion.service';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icClose from '@iconify/icons-ic/twotone-close';
import icPrint from '@iconify/icons-ic/twotone-print';
import icDownload from '@iconify/icons-ic/twotone-cloud-download';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icPerson from '@iconify/icons-ic/twotone-person';
import icLocationCity from '@iconify/icons-ic/twotone-location-city';
import icCheck from '@iconify/icons-ic/check';
import icSave from '@iconify/icons-fa-solid/save';
import icEdit from '@iconify/icons-fa-solid/edit';
import icMedKit from '@iconify/icons-fa-solid/medkit';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';

import {enumSelector} from 'src/app/core/utils/enum-to-string-util';
import {LoadingService} from 'src/app/core/services/loading.service';
import {switchMap} from 'rxjs/operators';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {Observable} from 'rxjs';
import {Dispensario} from 'src/app/core/models/catalogo/dispensario.model';
import {AreaSalud} from 'src/app/core/models/catalogo/area-salud.model';
import {SaludUserService} from 'src/app/core/services/salud-user.service';
import {TipoConsulta} from 'src/app/core/enums/tipo-consulta.enum';
import {MotivoAtencion} from 'src/app/core/models/catalogo/motivo-atencion.model';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import {LugarAtencion} from 'src/app/core/models/catalogo/lugar-atencion.model';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';

@Component({
  selector: 'vex-informacion-evolucion',
  templateUrl: './informacion-evolucion.component.html',
  styleUrls: [
    './informacion-evolucion.component.scss',
    '../../../../../../@vex/styles/partials/plugins/_quill.scss'
  ],
  animations: [
    scaleIn400ms,
    fadeInRight400ms,
    fadeInUp400ms
  ]
})
export class InformacionEvolucionComponent implements OnInit {

  icMoreHoriz = icMoreHoriz;
  icMoreVert = icMoreVert;
  icClose = icClose;
  icSave = icSave;
  icPrint = icPrint;
  icDownload = icDownload;
  icDelete = icDelete;
  icPerson = icPerson;
  icLocationCity = icLocationCity;
  icPhone = icPhone;
  icCheck = icCheck;
  icEdit = icEdit;
  icMedKit = icMedKit;

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    private pacienteGlobalService: PacienteGlobalService,
    private cd: ChangeDetectorRef,
    private snackbar: MatSnackBar,
    private evolucionService: EvolucionService,
    private loadingService: LoadingService,
    private catalogoService: CatalogoService,
    private usuarioSaludService: SaludUserService
  ) {
  }

  evolucion: Evolucion;
  pacienteActual: Paciente;
  evolucionForm: FormGroup;
  maxDate = new Date();
  dispensarios: Observable<Dispensario[]>;
  areasSalud: Observable<AreaSalud[]>;
  tiposConsultas = enumSelector(TipoConsulta);
  motivosAtencion: Observable<MotivoAtencion[]>;
  usuarios: Observable<SaludUser[]>;
  lugaresAtencion: Observable<LugarAtencion[]>;
  errorMessages = FORM_ERROR_MESSAGES;

  ngOnInit() {
    this.dispensarios = this.catalogoService.getDispensarios();
    this.areasSalud = this.usuarioSaludService.getAreasSalud();
    this.motivosAtencion = this.catalogoService.getMotivosAtencion();
    this.usuarios = this.usuarioSaludService.getAllUsers();
    this.lugaresAtencion = this.catalogoService.getLugaresAtencion();
    this.route.paramMap.pipe(
      switchMap(params => {
        const codigo = params.get('codigo');
        return this.evolucionService.getEvolucion(codigo);
      })
    ).subscribe((evolucion) => {
      if (evolucion) {
        this.evolucion = evolucion;
        this.cargarFormularioEvolucion();
      }
    });
  }

  cargarFormularioEvolucion() {
    this.evolucionForm = this.fb.group({
      id: this.evolucion.id,
      idAreaSalud: [{value: this.evolucion.idAreaSalud, disabled: true}, Validators.required],
      idDispensario: [{value: this.evolucion.idDispensario, disabled: true}, Validators.required],
      idMotivoAtencion: [this.evolucion.idMotivoAtencion, Validators.required],
      idTipoHistoriaClinica: [this.evolucion.idTipoHistoriaClinica],
      motivoConsulta: [this.evolucion.motivoConsulta, Validators.required],
      estado: [this.evolucion.estado],
      notaEvolucion: [this.evolucion.notaEvolucion, Validators.required],
      tipoConsulta: [this.evolucion.tipoConsulta, Validators.required],
      idLugarAtencion: [this.evolucion.idLugarAtencion, Validators.required],
      responsablePidm: [{value: this.evolucion.responsablePidm, disabled: true}, Validators.required],
      idPaciente: this.evolucion.idPaciente,
    });
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
        }
      });
  }

  updateEvolucion() {
    const evolucion = this.evolucionForm.getRawValue();
    this.loadingService.showLoaderUntilCompleted(
      this.evolucionService.updateEvolucion(evolucion.id, evolucion)
    ).subscribe((data: Evolucion) => {
      if (data) {
        this.evolucion = data;
        this.showNotification('Evolución actualizada EXITOSAMENTE', 'CERRAR');
      }
    }, error => {
      this.showNotification('ERROR al actualizar', 'CERRAR');
    });
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000,
    });
  }
}
