import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {Procedimiento} from '../../models/procedimiento.model';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {FormControl} from '@angular/forms';

import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {ProcedimientoService} from '../../services/procedimiento.service';
import {ActivatedRoute} from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {ProcedimientosCreateComponent} from './procedimientos-create/procedimientos-create.component';
import {EvolucionSharedService} from '../../services/evolucion-shared.service';
import {Evolucion} from '../../models/evolucion.model';

@UntilDestroy()
@Component({
  selector: 'vex-procedimientos',
  templateUrl: './procedimientos.component.html',
  styleUrls: ['./procedimientos.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms,
    scaleFadeIn400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class ProcedimientosComponent implements OnInit, AfterViewInit {
  procedimientos: Procedimiento[];
  columns: TableColumn<Procedimiento>[] = [
    {label: 'Procedimiento', property: 'tipoProcedimiento', type: 'text', visible: true},
    {label: 'Numero de Actividades', property: 'numeroActividades', type: 'text', visible: true},
    {label: 'Observaciones', property: 'nota', type: 'text', visible: true},
    {label: 'Responsable', property: 'responsable', type: 'text', visible: true},
    {label: 'Acciones', property: 'menu', type: 'button', visible: true}
  ];
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<Procedimiento> | null;
  selection = new SelectionModel<Procedimiento>(true, []);
  searchCtrl = new FormControl();

  icEdit = icEdit;
  icSearch = icSearch;
  icMoreHoriz = icMoreHoriz;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;

  evolucion: Evolucion;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    private procedimientoService: ProcedimientoService,
    private route: ActivatedRoute,
    private evolucionSharedService: EvolucionSharedService,
  ) {
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.evolucionSharedService.evolucion$
      .subscribe((evolucion: Evolucion) => {
        if (evolucion) {
          this.evolucion = evolucion;
          this.procedimientoService.getProcedimiento(this.evolucion.id)
            .subscribe((data) => {
              this.procedimientos = data;
              this.dataSource.data = this.procedimientos;
            });
        }
      });

    this.searchCtrl.valueChanges
      .pipe(untilDestroyed(this))
      .subscribe(value => this.onFilterChange(value));
  }

  startDeleteProcedimiento(
    listProcedimientos?: Procedimiento[],
    procedimiento?: Procedimiento) {
    let message = '¿Esta seguro de eliminar este registro?';
    if (procedimiento) {
      listProcedimientos = new Array<Procedimiento>();
      listProcedimientos.push(procedimiento);
    } else if (listProcedimientos.length > 1) {
      message = '¿Estas seguro de eliminar ' + listProcedimientos.length + ' registros?';
    }
    this.openDeleteDialog(message, listProcedimientos);
  }

  openDeleteDialog(message: string, procedimientos: Procedimiento[]) {
    this.dialog.open(DeleteModalComponent, {
      data: message,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') { this.deleteProcedimiento(procedimientos); }
    });
  }

  deleteProcedimiento(procedimientos: Procedimiento[]) {
    const size = procedimientos.length;
    const promise = new Promise<void>((resolve, reject) => {
      this.procedimientoService.deleteProcedimiento(procedimientos, this.evolucion.id)
        .subscribe((data) => {
            procedimientos.forEach(procedimiento => {
              this.procedimientos.splice(
                this.procedimientos.findIndex(
                  existingProcedimiento =>
                    existingProcedimiento.id === procedimiento.id
                ),
                1
              );
              this.selection.deselect(data);
            });
            this.dataSource.connect().next(this.procedimientos);
          },
          error => {
            this.showNotification('A ocurrido un ERROR', 'CERRAR');
            console.error(error);
          });
      resolve();
    });
    promise.then(() => {
      this.selection.clear();
      if (size > 1) {
        this.showNotification('Registros eliminados EXITOSAMENTE', 'OK');
      } else {
        this.showNotification('Registro eliminado EXITOSAMENTE', 'OK');
      }
    });
  }

  createProcedimiento() {
    this.dialog
      .open(ProcedimientosCreateComponent, {
        width: '550px',
        maxWidth: '100%'
      }).afterClosed()
      .subscribe((procedimiento: Procedimiento) => {
        if (procedimiento) {
          this.procedimientoService.createProcedimiento(procedimiento, this.evolucion.id)
            .subscribe(
              data => {
                if (data) {
                  this.procedimientos.unshift(
                    new Procedimiento(data)
                  );
                  this.dataSource.connect().next(this.procedimientos);
                  this.showNotification('Registro agregado EXITOSAMENTE', 'OK');
                }
              },
              error => {
                this.showNotification('ERROR al crear el registro', 'CERRAR');
                console.log(error);
              });
        }
      });
  }

  updateProcedimiento(procedimiento: Procedimiento) {
    this.dialog
      .open(ProcedimientosCreateComponent, {
        data: procedimiento,
        width: '550px',
        maxWidth: '100%'
      })
      .afterClosed()
      .subscribe(updatedProcedimiento => {
        if (updatedProcedimiento) {
          const id = updatedProcedimiento.id;
          this.procedimientoService
            .updateProcediminto(id, updatedProcedimiento, this.evolucion.id)
            .subscribe(
              data => {
                // Success
                const index = this.procedimientos.findIndex(
                  existingProcedimiento =>
                    existingProcedimiento.id === id
                );
                this.procedimientos[index] = new Procedimiento(data);
                this.dataSource.connect().next(this.procedimientos);
                this.showNotification('Registro actualizado EXITOSAMENTE', 'OK');
              },
              err => {
                this.showNotification(
                  'ERROR al actualizar el registro',
                  'CERRAR'
                );
                console.error(err);
              }
            );
        }
      });
  }


  get visibleColumns() {
    return this.columns
      .filter(column => column.visible)
      .map(column => column.property);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  onFilterChange(value: string) {
    if (!this.dataSource) {
      return;
    }
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  trackByProperty<T>(column: TableColumn<T>) {
    return column.property;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }
}
