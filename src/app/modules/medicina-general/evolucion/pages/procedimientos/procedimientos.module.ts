import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProcedimientosComponent } from './procedimientos.component';
import { ProcedimientosCreateComponent } from './procedimientos-create/procedimientos-create.component';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import {ExtendedModule, FlexModule} from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatPaginatorModule} from '@angular/material/paginator'
import { ProcedimientosRoutingModule } from './procedimientos-routing.module';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import {MatAutocompleteModule} from '@angular/material/autocomplete';


@NgModule({
  declarations: [ ProcedimientosComponent, ProcedimientosCreateComponent],
    imports: [
        ProcedimientosRoutingModule,
        CommonModule,
        MatButtonModule,
        MatTooltipModule,
        FlexModule,
        ExtendedModule,
        MatIconModule,
        IconModule,
        ReactiveFormsModule,
        MatMenuModule,
        MatTableModule,
        MatSortModule,
        MatCheckboxModule,
        MatPaginatorModule,
        FormsModule,
        MatDialogModule,
        MatInputModule,
        MatSelectModule,
        MatAutocompleteModule,
    ]
})
export class ProcedimientosModule { }
