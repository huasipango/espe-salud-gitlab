import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

import {Procedimiento} from 'src/app/modules/medicina-general/evolucion/models/procedimiento.model';
import icCheck from '@iconify/icons-ic/check';
import icClose from '@iconify/icons-ic/twotone-close';
import {Observable} from 'rxjs';
import {TipoProcedimiento} from 'src/app/core/models/catalogo/tipo-procedimiento.model';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {map, startWith} from 'rxjs/operators';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';

@Component({
  selector: 'vex-procedimientos-create',
  templateUrl: './procedimientos-create.component.html',
  styleUrls: ['./procedimientos-create.component.scss']
})
export class ProcedimientosCreateComponent implements OnInit {
  form: FormGroup;
  mode: 'create' | 'update' = 'create';

  icClose = icClose;
  icCheck = icCheck;
  icArrowDropDown = icArrowDropDown;

  procedimientoCtrl: FormControl;
  procedimientos: TipoProcedimiento[] = [];
  filteredProcedimientos: Observable<TipoProcedimiento[]>;

  constructor(
    private dialogRef: MatDialogRef<ProcedimientosCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public defaults: any,
    private fb: FormBuilder,
    private catalogoService: CatalogoService
  ) {
  }

  ngOnInit(): void {
    this.catalogoService.getTiposProcedimiento()
      .subscribe((procedimientos: TipoProcedimiento[]) => {
        this.procedimientos = procedimientos;
        this.cargarProcedimientoFormCtrl();
      });
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as Procedimiento;
    }
    this.procedimientoCtrl = new FormControl(
      this.defaults.tipoProcedimiento || null, Validators.required
    );
    this.form = this.fb.group({
      id: this.defaults.id || null,
      nota: [this.defaults.nota || '', Validators.required],
      tipoProcedimiento: this.procedimientoCtrl,
      responsable: [this.defaults.responsable || 'David Smith'], // TODO Obtener del usuario logeado
      numeroActividades: [this.defaults.numeroActividades || null, Validators.required]
    });
  }

  cargarProcedimientoFormCtrl() {
    this.filteredProcedimientos = this.procedimientoCtrl.valueChanges
      .pipe(
        startWith(''),
        map(value => value ? this.filterProcedimiento(value) : this.procedimientos.slice())
      );
  }

  filterProcedimiento(value: string) {
    return this.procedimientos.filter(ud =>
      ud.nombre.toLowerCase().indexOf(value.toLowerCase()) === 0);
  }


  save() {
    const procedimiento = this.form.value;
    console.log(procedimiento)
    this.dialogRef.close(procedimiento);
  }


  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }


}
