import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {VexRoutes} from 'src/@vex/interfaces/vex-route.interface';
import {EvolucionCreateComponent} from './evolucion-create.component';

const routes: VexRoutes = [
  {
    path: '',
    component: EvolucionCreateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvolucionCreateRoutingModule {
}
