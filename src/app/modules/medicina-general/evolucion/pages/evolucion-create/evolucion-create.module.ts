import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EvolucionCreateRoutingModule} from './evolucion-create-routing.module';
import {EvolucionCreateComponent} from './evolucion-create.component';
import {PageLayoutModule} from 'src/@vex/components/page-layout/page-layout.module';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {FlexModule} from '@angular/flex-layout';
import {MatStepperModule} from '@angular/material/stepper';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatInputModule} from '@angular/material/input';
import {SecondaryToolbarModule} from 'src/@vex/components/secondary-toolbar/secondary-toolbar.module';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {EmptyPacienteModule} from 'src/app/core/components/empty-paciente/empty-paciente.module';
import {MatSelectModule} from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {SelectNotaEnfermeriaBottomSheetModule} from 'src/app/modules/medicina-general/evolucion/components/select-nota-enfermeria-bottom-sheet/select-nota-enfermeria-bottom-sheet.module';
import {MatTabsModule} from '@angular/material/tabs';
import {DiagnosticoTableModule} from 'src/app/modules/medicina-general/evolucion/components/diagnostico-table/diagnostico-table.module';
import {PrescripcionFarmacologicaTableModule} from 'src/app/modules/medicina-general/evolucion/components/prescripcion-farmacologica-table/prescripcion-farmacologica-table.module';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import {QuillModule} from 'ngx-quill';
import {QuillEvolucionConfiguration} from 'src/app/core/utils/quill-config';
import {SelectTipoExamenLaboratorioModule} from 'src/app/modules/medicina-general/evolucion/components/select-tipo-examen-laboratorio/select-tipo-examen-laboratorio.module';

@NgModule({
  declarations: [EvolucionCreateComponent],
  imports: [
    CommonModule,
    EvolucionCreateRoutingModule,
    PageLayoutModule,
    BreadcrumbsModule,
    FlexModule,
    MatStepperModule,
    MatIconModule,
    IconModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDatepickerModule,
    MatButtonModule,
    MatTooltipModule,
    SecondaryToolbarModule,
    ContainerModule,
    EmptyPacienteModule,
    MatCheckboxModule,
    MatDividerModule,
    MatListModule,
    SelectNotaEnfermeriaBottomSheetModule,
    MatTabsModule,
    DiagnosticoTableModule,
    PrescripcionFarmacologicaTableModule,
    LoadingModule,
    QuillModule.forRoot(QuillEvolucionConfiguration),
    SelectTipoExamenLaboratorioModule
  ]
})
export class EvolucionCreateModule {}
