import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icClose from '@iconify/icons-ic/twotone-close';
import icPrint from '@iconify/icons-ic/twotone-print';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icPerson from '@iconify/icons-ic/twotone-person';
import icLocationCity from '@iconify/icons-ic/twotone-location-city';
import icCheck from '@iconify/icons-ic/check';
import icDescription from '@iconify/icons-ic/twotone-description';
import icDoneAll from '@iconify/icons-ic/twotone-done-all';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';
import icAdd from '@iconify/icons-ic/twotone-add';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icSearch from '@iconify/icons-ic/search';
import icBack from '@iconify/icons-ic/arrow-back';

import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {EvolucionService} from 'src/app/modules/medicina-general/evolucion/services/evolucion.service';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {SexoEnum} from 'src/app/core/enums/sexo.enum';
import {EvolucionSharedService} from '../../services/evolucion-shared.service';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {interval, Observable, Subscription} from 'rxjs';
import {MotivoAtencion} from 'src/app/core/models/catalogo/motivo-atencion.model';
import {NotaEnfermeria} from 'src/app/modules/enfermeria/notas-enfermeria/models/nota-enfermeria.model';
import {AuthService} from 'src/app/core/auth/auth.service';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import {Diagnostico} from 'src/app/modules/medicina-general/evolucion/models/diagnostico.model';
import {AreaSalud} from 'src/app/core/models/catalogo/area-salud.model';
import {SaludUserService} from 'src/app/core/services/salud-user.service';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {SelectNotaEnfermeriaBottomSheetComponent} from 'src/app/modules/medicina-general/evolucion/components/select-nota-enfermeria-bottom-sheet/select-nota-enfermeria-bottom-sheet.component';
import {LoadingService} from 'src/app/core/services/loading.service';
import {Dispensario} from 'src/app/core/models/catalogo/dispensario.model';
import {enumSelector} from 'src/app/core/utils/enum-to-string-util';
import {TipoConsulta} from 'src/app/core/enums/tipo-consulta.enum';
import {Prescripcion} from 'src/app/modules/medicina-general/evolucion/models/prescripcion.model';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {LugarAtencion} from 'src/app/core/models/catalogo/lugar-atencion.model';
import {TipoDetalleExamenLaboratorio} from 'src/app/core/models/catalogo/tipo-detalle-examen-laboratorio.model';
import {setCustomFormatDateTime} from 'src/app/core/utils/date-utils';
import {Evolucion} from 'src/app/modules/medicina-general/evolucion/models/evolucion.model';

@Component({
  selector: 'vex-evolucion-create',
  templateUrl: './evolucion-create.component.html',
  styleUrls: [
    './evolucion-create.component.scss',
    '../../../../../../@vex/styles/partials/plugins/_quill.scss'
  ],
  encapsulation: ViewEncapsulation.None,
  animations: [
    fadeInUp400ms,
    stagger40ms,
    scaleFadeIn400ms,
    scaleIn400ms,
    fadeInRight400ms
  ],
})
export class EvolucionCreateComponent implements OnInit, OnDestroy {
  icMoreVert = icMoreVert;
  icClose = icClose;
  icPrint = icPrint;
  icDelete = icDelete;
  icCheck = icCheck;
  icPerson = icPerson;
  icLocationCity = icLocationCity;
  icPhone = icPhone;
  icDoneAll = icDoneAll;
  icDescription = icDescription;
  icArrowDropDown = icArrowDropDown;
  icAdd = icAdd;
  icMoreHoriz = icMoreHoriz;
  icSearch = icSearch;
  icBack = icBack;
  tiposConsultas = enumSelector(TipoConsulta);
  notaEnfermeria: NotaEnfermeria;
  pacienteActual: Paciente;
  usuarioSalud: SaludUser;
  evolucion: Evolucion;
  diagnosticos: Diagnostico[] = [];
  prescripciones: Prescripcion[] = [];
  parametrosLaboratorio: TipoDetalleExamenLaboratorio[] = [];
  dispensarios: Observable<Dispensario[]>;
  motivosAtencion: Observable<MotivoAtencion[]>;
  areasSalud: Observable<AreaSalud[]>;
  lugaresAtencion: Observable<LugarAtencion[]>;
  fechaInicio: Date;

  step0FormGroup = this.fb.group({
    idNotaEnfermeria: [null]
  });

  step1FormGroup = this.fb.group({
    idAreaSalud: [{value: null, disabled: true}, Validators.required],
    idDispensario: [{value: null, disabled: true}, Validators.required],
    idPaciente: [null, Validators.required],
    idMotivoAtencion: [null, Validators.required],
    motivoConsulta: [null, Validators.required],
    notaEvolucion: [null, Validators.required],
    tipoConsulta: [null, Validators.required],
    idLugarAtencion: [null, Validators.required],
    responsablePidm: [null, Validators.required]
  });
  step2FormGroup = this.fb.group({
    diagnosticos: [0, Validators.min(1)]
  });
  step3FormGroup = this.fb.group({
    indicacionNoFarmacologica: [null],
    prescripciones: [this.prescripciones],
    parametrosLaboratorio: [this.parametrosLaboratorio]
  });
  errorMessages = FORM_ERROR_MESSAGES;
  private subscription: Subscription;
  time: number = 0;
  idEvolucion: string;

  constructor(
    private  snackbar: MatSnackBar, private fb: FormBuilder,
    private dialog: MatDialog,
    private pacienteGlobalService: PacienteGlobalService,
    private router: Router,
    private usuarioService: SaludUserService,
    private route: ActivatedRoute,
    protected catalogoService: CatalogoService,
    private evolucionService: EvolucionService,
    private authService: AuthService,
    private _bottomSheet: MatBottomSheet,
    private loadingService: LoadingService,
    private evolucionSharedService: EvolucionSharedService,
  ) {
    this.notaEnfermeria = this.router.getCurrentNavigation().extras.state as NotaEnfermeria;
  }

  ngOnInit(): void {
    this.subscription = interval(1000)
      .subscribe(x => {
        this.time++;
      });
    this.fechaInicio = new Date();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.step1FormGroup.patchValue({idPaciente: paciente.id});
        }
      });
    this.authService.saludUserData$
      .subscribe((saludUser) => {
        if (saludUser) {
          this.usuarioSalud = saludUser;
          this.step1FormGroup.patchValue({responsablePidm: saludUser.pidm});
          this.step1FormGroup.patchValue({idAreaSalud: saludUser.idAreaSalud});
          this.step1FormGroup.patchValue({idDispensario: saludUser.idDispensario});
        }
      });
    this.motivosAtencion = this.catalogoService.getMotivosAtencion();
    this.dispensarios = this.catalogoService.getDispensarios();
    this.areasSalud = this.usuarioService.getAreasSalud();
    this.lugaresAtencion = this.catalogoService.getLugaresAtencion();
    this.router.events.subscribe(event =>{
      if (event instanceof NavigationEnd){
        this.evolucionSharedService.setEvolucion(null);
      }
    })
  }

  createEvolucion(): void {
    const evolucion = this.getFormData();
    this.loadingService.showLoaderUntilCompleted(
      this.evolucionService.updateEvolucion(this.idEvolucion, evolucion)
    ).subscribe((data) => {
      if (data != null) {
        this.evolucionSharedService.setEvolucion(data);
        this.showNotification('Registro creado EXITOSAMENTE', 'OK');
        this.evolucionService.finalizeNotaEvolucion(this.idEvolucion)
          .subscribe(() => {
            this.router.navigate(['../'], {relativeTo: this.route});
          }, error => {
            this.router.navigate(['../'], {relativeTo: this.route});
          })
      }
    }, (error) => {
      this.showNotification('ERROR al crear el registro', 'CERRAR');
    });
  }

  savePartialData() {
    const evolucion = this.getFormData();
    this.loadingService.showLoaderUntilCompleted(
      this.evolucionService.createEvolucionPartial(evolucion)
    ).subscribe((data) => {
      if (data != null) {
        this.evolucionSharedService.setEvolucion(data);
        this.idEvolucion = data.id;
        this.showNotification('Se ha guardado el registro', 'OK');
      }
    }, (error) => {
      this.showNotification('ERROR al guardar el registro', 'CERRAR');
    });
  }

  getFormData(): any {
    const evolucion = this.step1FormGroup.getRawValue();
    evolucion.indicacionNoFarmacologica = this.step3FormGroup.get('indicacionNoFarmacologica').value;
    evolucion.diagnosticos = this.diagnosticos;
    evolucion.prescripciones = this.prescripciones;
    evolucion.parametrosLaboratorio = this.parametrosLaboratorio;
    evolucion.idNotaEnfermeria = this.notaEnfermeria?.id || null;
    evolucion.fechaInicio = setCustomFormatDateTime(this.fechaInicio);
    // Set tiempo empleado en la consulta
    evolucion.fechaFinalizacion = setCustomFormatDateTime(new Date(this.fechaInicio.getTime() + (this.time * 1000)));
    return evolucion;
  }

  chargeDiagnosticos(diagnosticos: Diagnostico[]) {
    this.diagnosticos = diagnosticos;
    this.step2FormGroup
      .get('diagnosticos')
      .setValue(this.diagnosticos.length);
  }

  chargePrescripciones(prescripciones: Prescripcion[]) {
    this.prescripciones = prescripciones;
    this.step3FormGroup
      .get('prescripciones')
      .setValue(this.prescripciones);
  }

  chargeParametrosLaboratorio(parametros: TipoDetalleExamenLaboratorio[]) {
    this.parametrosLaboratorio = parametros;
    this.step3FormGroup
      .get('parametrosLaboratorio')
      .setValue(this.parametrosLaboratorio);
  }

  selectNotaEnfermeria(): void {
    this._bottomSheet
      .open(SelectNotaEnfermeriaBottomSheetComponent)
      .afterDismissed()
      .subscribe(notaEnfermeria => {
        if (notaEnfermeria) {
          this.notaEnfermeria = notaEnfermeria;
        }
      });
  }

  isFemenino(): boolean {
    return this.pacienteActual.sexo === SexoEnum.MUJER;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  removeNotaEnfermeria() {
    this.notaEnfermeria = null;
  }

  navigateBack(): void {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  saveFirstTwoSteps() {
    if (this.idEvolucion == null) {
      this.savePartialData();
    }
  }
}
