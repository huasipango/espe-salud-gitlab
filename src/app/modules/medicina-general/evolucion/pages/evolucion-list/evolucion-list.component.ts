import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {Observable} from 'rxjs';
import {Evolucion} from '../../models/evolucion.model';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {FormControl} from '@angular/forms';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {EvolucionService} from '../../services/evolucion.service';

import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {UntilDestroy} from '@ngneat/until-destroy';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {getShortDateTime, isToday} from 'src/app/core/utils/date-utils';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icEye from '@iconify/icons-fa-solid/eye';
import icCheck from '@iconify/icons-fa-solid/check';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icMedKit from '@iconify/icons-fa-solid/medkit';
import icDownload from '@iconify/icons-ic/cloud-download';
import icPrint from '@iconify/icons-ic/print';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {trackById} from 'src/@vex/utils/track-by';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {ReposoModalComponent} from 'src/app/modules/medicina-general/evolucion/components/reposo-modal/reposo-modal.component';
import {LoadingService} from 'src/app/core/services/loading.service';
import {EvolucionReportService} from 'src/app/modules/medicina-general/evolucion/services/evolucion-report.service';
import {PopoverService} from 'src/@vex/components/popover/popover.service';
import {TipoConsultaColor} from 'src/app/core/enums/tipo-consulta.enum';
import {ImagenUsuarioService} from 'src/app/core/services/imagen-usuario.service';
import {
  BuenaSaludModalComponent
} from 'src/app/modules/medicina-general/evolucion/components/buena-salud-modal/buena-salud-modal.component';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {DiagnosticosBottomSheetComponent} from 'src/app/modules/medicina-general/evolucion/components/diagnosticos-bottom-sheet/diagnosticos-bottom-sheet.component';
import {PrescripcionesBottomSheetComponent} from 'src/app/modules/medicina-general/evolucion/components/prescripciones-bottom-sheet/prescripciones-bottom-sheet.component';
import {NotaEnfermeriaBottomSheetComponent} from 'src/app/modules/medicina-general/evolucion/components/nota-enfermeria-bottom-sheet/nota-enfermeria-bottom-sheet.component';
import {ColorUtil} from 'src/app/core/utils/color-utils';

@UntilDestroy()
@Component({
  selector: 'vex-evolucion-list',
  templateUrl: './evolucion-list.component.html',
  styleUrls: [
    './evolucion-list.component.scss',
  ],
  animations: [
    fadeInUp400ms,
    stagger40ms,
    scaleIn400ms,
    fadeInRight400ms,
    scaleFadeIn400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class EvolucionListComponent implements OnInit, AfterViewInit {

  evoluciones$: Observable<Evolucion[]>;
  trackById = trackById;
  messages = USER_MESSAGES;

  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  searchCtrl = new FormControl();

  icEdit = icEdit;
  icSearch = icSearch;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icMoreHoriz = icMoreHoriz;
  icEye = icEye;
  icMedKit = icMedKit;
  icDownload = icDownload;
  icPrint = icPrint;
  icCheck = icCheck;

  pacienteActual: Paciente;
  tipoConsultaColors = TipoConsultaColor;
  labelClassInfo = ColorUtil.evolucionStateColors();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
    private evolucionService: EvolucionService,
    private evolucionReportService: EvolucionReportService,
    private pacienteGlobalService: PacienteGlobalService,
    private loadingService: LoadingService,
    private popoverService: PopoverService,
    private imagenUsuarioService: ImagenUsuarioService,
    private _bottomSheet: MatBottomSheet,
  ) {
  }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getEvoluciones();
        }
      });
  }

  getEvoluciones(): void {
    this.evoluciones$ = this.loadingService.showLoaderUntilCompleted(
      this.evolucionService.getEvoluciones(this.pacienteActual.id)
    );
  }

  openDeleteDialog(evolucion: Evolucion) {
    this.dialog.open(DeleteModalComponent, {
      data: this.messages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') {
        this.deleteEvolucion(evolucion);
      }
    });
  }

  createEvolucion() {
    this.router.navigate(['/medicina-general/evolucion/create']);
  }

  showDiagnosticos(idEvolucion: string): void {
    this._bottomSheet.open(DiagnosticosBottomSheetComponent, {
      data: idEvolucion
    }).afterDismissed()
      .subscribe();
  }

  showPrescripciones(evolucion: Evolucion): void {
    this._bottomSheet
      .open(PrescripcionesBottomSheetComponent, {
        data: evolucion
      }).afterDismissed()
      .subscribe();
  }

  showNotaEnfermeria(idNotaEnfermeria: number) {
    this._bottomSheet
      .open(NotaEnfermeriaBottomSheetComponent, {
        data: idNotaEnfermeria
      }).afterDismissed()
      .subscribe();
  }

  deleteEvolucion(evolucion: Evolucion) {

  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  ngAfterViewInit() {
  }

  getDatetime(date: Date): string {
    return getShortDateTime(date);
  }

  updateEvolucion(evolucion: Evolucion) {
    const codigo = evolucion.id;
    this.router.navigate(['./', codigo], {relativeTo: this.route});
  }

  generateCertificadoReposo(evolucion: Evolucion) {
    this.dialog.open(ReposoModalComponent, {
      data: evolucion.id,
      width: '600px',
      maxWidth: '100%'
    }).afterClosed().subscribe();
  }

  generateCertificadoBuenaSalud(evolucion: Evolucion) {
    this.dialog.open(BuenaSaludModalComponent, {
      data: evolucion.id,
      width: '600px',
      maxWidth: '100%'
    }).afterClosed().subscribe();
  }

  getTipoConsultaColors(tipoConsulta: string) {
    const foundType = this.tipoConsultaColors.find(t => t.type === tipoConsulta);
    if (foundType) {
      return foundType.classes;
    }
    return [];
  }

  getUserImage(idBanner: string): string {
    return this.imagenUsuarioService.getUserImage(idBanner);
  }

  availableToModify(fechaInicio: Date): boolean {
    return isToday(fechaInicio);
  }

  getEvolucionStateColor(state: string) {
    const foundState = this.labelClassInfo.find(s => s.state === state);
    if (foundState) {
      return [foundState.color.bgClass, foundState.color.textClass];
    }
    return [];
  }

  finalizeEvolucion(evolucion: Evolucion) {
    this.evolucionService.finalizeNotaEvolucion(evolucion.id)
      .subscribe((response) => {
        if (response) {
          this.showNotification('Se ha finalizado la atención EXITOSAMENTE', 'CERRAR');
          this.getEvoluciones();
        } else {
          this.showNotification('No se ha podido finalizar la atención', 'CERRAR');
        }
      }, error => {
        this.showNotification(error.error.apierror.message, 'CERRAR');
      })
  }
}
