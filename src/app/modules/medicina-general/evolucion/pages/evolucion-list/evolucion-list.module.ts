import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EvolucionListComponent} from './evolucion-list.component';
import {ExtendedModule, FlexModule} from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatTableModule} from '@angular/material/table';
import {MatMenuModule} from '@angular/material/menu';
import {MatSortModule} from '@angular/material/sort';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatPaginatorModule} from '@angular/material/paginator';
import {EvolucionListRoutingModule} from './evolucion-list-routing.module';
import {PageLayoutModule} from 'src/@vex/components/page-layout/page-layout.module';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {EmptyPacienteModule} from 'src/app/core/components/empty-paciente/empty-paciente.module';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {MatListModule} from '@angular/material/list';
import {StripHtmlModule} from 'src/@vex/pipes/strip-html/strip-html.module';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import {QuillModule} from 'ngx-quill';
import {QuillEvolucionConfiguration} from 'src/app/core/utils/quill-config';


@NgModule({
  declarations: [EvolucionListComponent],
  imports: [
    CommonModule,
    EvolucionListRoutingModule,
    FlexModule,
    ExtendedModule,
    MatIconModule,
    IconModule,
    MatButtonModule,
    MatTooltipModule,
    ReactiveFormsModule,
    MatTableModule,
    MatMenuModule,
    MatSortModule,
    MatCheckboxModule,
    MatPaginatorModule,
    FormsModule,
    PageLayoutModule,
    BreadcrumbsModule,
    EmptyPacienteModule,
    ContainerModule,
    MatListModule,
    StripHtmlModule,
    LoadingModule,
    QuillModule.forRoot(QuillEvolucionConfiguration)
  ]
})
export class EvolucionListModule {
}
