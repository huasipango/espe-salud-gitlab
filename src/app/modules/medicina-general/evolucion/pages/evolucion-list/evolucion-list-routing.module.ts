import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {VexRoutes} from 'src/@vex/interfaces/vex-route.interface';
import {EvolucionListComponent} from './evolucion-list.component';

const routes: VexRoutes = [
  {
    path: '',
    component: EvolucionListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvolucionListRoutingModule {
}
