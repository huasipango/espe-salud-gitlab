import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {Prescripcion} from 'src/app/modules/medicina-general/evolucion/models/prescripcion.model';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {PrescripcionService} from 'src/app/modules/medicina-general/evolucion/services/prescripcion.service';
import {switchMap, tap} from 'rxjs/operators';
import {PrescripcionFarmacologicaModalComponent} from 'src/app/modules/medicina-general/evolucion/components/prescripcion-farmacologica-modal/prescripcion-farmacologica-modal.component';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import icAdd from '@iconify/icons-ic/twotone-add';
import icDeleteForever from '@iconify/icons-ic/twotone-delete-forever';
import icSave from '@iconify/icons-fa-solid/save';

import {RepertorioMedicamento} from 'src/app/core/models/catalogo/repertorio-medicamento.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {EvolucionService} from 'src/app/modules/medicina-general/evolucion/services/evolucion.service';
import {Evolucion} from 'src/app/modules/medicina-general/evolucion/models/evolucion.model';
import {LoadingService} from 'src/app/core/services/loading.service';


@Component({
  selector: 'vex-prescripciones',
  templateUrl: './prescripciones.component.html',
  styleUrls: ['./prescripciones.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms,
    scaleFadeIn400ms,
    scaleIn400ms,
    fadeInRight400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class PrescripcionesComponent implements OnInit, AfterViewInit {
  prescripciones: Prescripcion[];
  evolucionCodigo: string;
  messages = USER_MESSAGES;
  icAdd = icAdd;
  icDeleteForever = icDeleteForever;
  icSave = icSave;
  prescripcionNoFarmacologicaForm: FormGroup;
  evolucion: Evolucion;

  tableColumns: TableColumn<Prescripcion>[] = [
    {label: 'Medicamento', property: 'medicamento', type: 'object', visible: true},
    {label: 'Presentación', property: 'presentacion', type: 'text', visible: true},
    {label: 'Cantidad', property: 'cantidad', type: 'text', visible: true},
    {label: 'Indicaciones', property: 'indicacion', type: 'text', visible: false},
    {label: 'Acciones', property: 'menu', type: 'button', visible: true}
  ];
  dataSource: MatTableDataSource<Prescripcion> | null;

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private snackbar: MatSnackBar,
    private prescripcionService: PrescripcionService,
    private fb: FormBuilder,
    private evolucionService: EvolucionService,
    private loadingService: LoadingService
  ) { }

  ngOnInit(): void {
    this.prescripcionNoFarmacologicaForm = this.fb.group({
      indicacionNoFarmacologica: [null],
    });
    this.dataSource = new MatTableDataSource();
    this.route.paramMap.pipe(
      switchMap(params => {
        const codigo = params.get('codigo');
        this.evolucionCodigo = codigo;
        this.getEvolucion();
        return this.prescripcionService.getPrescripciones(codigo);
      })
    ).subscribe((prescripciones) => {
      this.prescripciones = prescripciones;
      this.dataSource.data = this.prescripciones;
    });
  }

  getEvolucion(): void {
    this.evolucionService.getEvolucion(this.evolucionCodigo)
      .subscribe((evolucion) => {
        if (evolucion) {
          this.prescripcionNoFarmacologicaForm.patchValue({
            indicacionNoFarmacologica: evolucion.indicacionNoFarmacologica
          });
        }
      });
  }

  createPrescripcion() {
    this.dialog
      .open(PrescripcionFarmacologicaModalComponent, {
        width: '700px',
        maxWidth: '100%'
      }).afterClosed()
      .subscribe((prescripcion: Prescripcion) => {
        if (prescripcion) {
          this.prescripcionService.createPrescripcion(prescripcion, this.evolucionCodigo)
            .subscribe((response) => {
              if (response) {
                this.prescripcionService.getPrescripciones(this.evolucionCodigo)
                  .subscribe(data => {
                    this.prescripciones = data;
                    this.dataSource.data = this.prescripciones;
                  });
                this.showNotification(this.messages.createdSuccessMessage, 'CERRAR');
              }
            }, error => {
              this.showNotification(this.messages.createdFailedMessage, 'CERRAR');
            });
        }
      });
  }

  updatePrescripcion(prescripcion: Prescripcion) {
    this.dialog.open(PrescripcionFarmacologicaModalComponent, {
      data: prescripcion,
      width: '700px',
      maxWidth: '100%'
    }).afterClosed().subscribe(prescripcionModificada => {
      if (prescripcionModificada) {
        this.prescripcionService.updatePrescripcion(
          prescripcion.id,
          prescripcionModificada,
          this.evolucionCodigo
        ).subscribe((response: Prescripcion) => {
          if (response) {
            const index = this.prescripciones.findIndex((prescripcionExistente) =>
              prescripcionExistente.id === prescripcionModificada.id);
            this.prescripciones[index] = response;
            this.dataSource.connect().next(this.prescripciones);
            this.showNotification(this.messages.updatedSuccessMessage, 'CERRAR');
          }
        }, error => {
          this.showNotification(this.messages.updatedFailedMessage, 'CERRAR');
        });
      }
    });
  }

  openDeleteDialog(prescripcion: Prescripcion) {
    this.dialog.open(DeleteModalComponent, {
      data: this.messages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') {
        this.deletePrescripcion(prescripcion);
      }
    });
  }

  deletePrescripcion(prescripcion: Prescripcion) {
    this.prescripcionService.deletePrescripcion(prescripcion.id, this.evolucionCodigo)
      .subscribe((response: boolean) => {
        if (response) {
          const index = this.prescripciones.findIndex(p => p.id === prescripcion.id);
          this.prescripciones.splice(index, 1);
          this.dataSource.connect().next(this.prescripciones);
          this.showNotification(this.messages.deletedSuccessMessage, 'CERRAR');
        } else {
          this.showNotification(this.messages.deleteFailMessage, 'CERRAR');
        }
      });
  }

  saveIndicacionNoFarmacologica() {
    const data = this.prescripcionNoFarmacologicaForm.value;
    this.loadingService.showLoaderUntilCompleted(
      this.evolucionService.patchEvolucion(data, this.evolucionCodigo)
    ).subscribe((response: Evolucion) => {
      if (response) {
        this.showNotification(this.messages.updatedSuccessMessage, 'CERRAR');
      }
    }, error => {
      this.showNotification(this.messages.updatedFailedMessage, 'CERRAR');
    });
  }

  getMedicamento(medicamento: RepertorioMedicamento): string {
    return medicamento ? medicamento.nombre : '';
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  trackByProperty<T>(column: TableColumn<T>) {
    return column.property;
  }

  get visibleColumns() {
    return this.tableColumns
      .filter(column => column.visible)
      .map(column => column.property);
  }
}
