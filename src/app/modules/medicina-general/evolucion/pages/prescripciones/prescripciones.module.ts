import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PrescripcionesComponent} from './prescripciones.component';
import {PrescripcionesRoutingModule} from 'src/app/modules/medicina-general/evolucion/pages/prescripciones/prescripciones-routing.module';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {IconModule} from '@visurel/iconify-angular';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatIconModule} from '@angular/material/icon';
import {MatTabsModule} from '@angular/material/tabs';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';


@NgModule({
  declarations: [PrescripcionesComponent],
  imports: [
    CommonModule,
    PrescripcionesRoutingModule,
    ContainerModule,
    FlexLayoutModule,
    IconModule,
    MatTableModule,
    MatSortModule,
    MatButtonModule,
    MatTooltipModule,
    MatIconModule,
    MatTabsModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    LoadingModule
  ]
})
export class PrescripcionesModule {
}
