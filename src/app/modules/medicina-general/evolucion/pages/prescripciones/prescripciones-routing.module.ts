import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PrescripcionesComponent} from 'src/app/modules/medicina-general/evolucion/pages/prescripciones/prescripciones.component';

const routes: Routes = [
  {
    path: '',
    component: PrescripcionesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrescripcionesRoutingModule {
}
