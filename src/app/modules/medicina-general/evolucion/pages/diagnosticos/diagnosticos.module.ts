import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DiagnosticosComponent } from './diagnosticos.component';
import { DiagnosticosRoutingModule } from 'src/app/modules/medicina-general/evolucion/pages/diagnosticos/diagnosticos-routing.module';
import { ExtendedModule, FlexModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatIconModule } from '@angular/material/icon';
import { IconModule } from '@visurel/iconify-angular';
import { MatMenuModule } from '@angular/material/menu';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ReactiveFormsModule, FormsModule, } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatDividerModule } from '@angular/material/divider';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ContainerModule } from 'src/@vex/directives/container/container.module';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

@NgModule({
  declarations: [DiagnosticosComponent],
  imports: [
    CommonModule,
    DiagnosticosRoutingModule,
    FlexModule,
    ExtendedModule,
    MatButtonModule,
    MatTooltipModule,
    MatIconModule,
    IconModule,
    MatMenuModule,
    MatTableModule,
    MatSortModule,
    MatCheckboxModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatInputModule,
    MatDividerModule,
    MatSnackBarModule,
    ContainerModule,
    MatSelectModule,
    MatButtonToggleModule,
    FormsModule,
    MatAutocompleteModule
  ]
})
export class DiagnosticosModule {}
