import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Diagnostico} from 'src/app/modules/medicina-general/evolucion/models/diagnostico.model';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DiagnosticoService} from 'src/app/modules/medicina-general/evolucion/services/diagnostico.service';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icEye from '@iconify/icons-fa-solid/eye';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icNotes from '@iconify/icons-fa-solid/notes-medical';
import icDeleteForever from '@iconify/icons-ic/twotone-delete-forever';

import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {switchMap} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {DiagnosticoModalComponent} from 'src/app/modules/medicina-general/evolucion/components/diagnostico-modal/diagnostico-modal.component';

@Component({
  selector: 'vex-diagnosticos',
  templateUrl: './diagnosticos.component.html',
  styleUrls: ['./diagnosticos.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms,
    scaleFadeIn400ms,
    scaleIn400ms,
    fadeInRight400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class DiagnosticosComponent implements OnInit, AfterViewInit, OnDestroy {
  diagnosticos: Diagnostico[];
  evolucionCodigo: string;

  tableColumns: TableColumn<Diagnostico>[] = [
    {label: 'Código CIE', property: 'codigoCIE', type: 'text', visible: true},
    {label: 'Tipo', property: 'tipo', type: 'text', visible: true},
    {label: 'Cronología', property: 'cronologia', type: 'text', visible: true},
    {label: 'Condición Diagnostico', property: 'condicionDiagnostico', type: 'text', visible: true},
    {label: 'Acciones', property: 'menu', type: 'button', visible: true}
  ];
  dataSource: MatTableDataSource<Diagnostico> | null;

  icEdit = icEdit;
  icSearch = icSearch;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icMoreHoriz = icMoreHoriz;
  icEye = icEye;
  icDeleteForever = icDeleteForever;
  icNotes = icNotes;
  messages = USER_MESSAGES;

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private snackbar: MatSnackBar,
    private diagnosticoService: DiagnosticoService,
  ) {
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.route.paramMap.pipe(
      switchMap(params => {
        const codigo = params.get('codigo');
        this.evolucionCodigo = codigo;
        return this.diagnosticoService.getDiagnosticos(codigo);
      })
    ).subscribe((diagnosticos) => {
      if (diagnosticos.length > 0) {
        this.diagnosticos = diagnosticos;
        this.dataSource.data = this.diagnosticos;
      }
    });
  }

  getDiagnosticos(): void {
    this.diagnosticoService.getDiagnosticos(this.evolucionCodigo)
      .subscribe((dianosticos) => {
        this.diagnosticos = dianosticos;
        this.dataSource.data = this.diagnosticos;
      });
  }

  createDiagnostico() {
    this.dialog
      .open(DiagnosticoModalComponent, {
        width: '700px',
        maxWidth: '100%'
      }).afterClosed()
      .subscribe((diagnostico: Diagnostico) => {
        if (diagnostico) {
          this.diagnosticoService.createDiagnostico(diagnostico, this.evolucionCodigo)
            .subscribe((response) => {
              if (response) {
                this.showNotification(this.messages.createdSuccessMessage, 'CERRAR');
                this.getDiagnosticos();
              }
            }, error => {
              this.showNotification(this.messages.createdFailedMessage, 'CERRAR');
            });
        }
      });
  }

  updateDiagnostico(diagnostico: Diagnostico) {
    this.dialog.open(DiagnosticoModalComponent, {
      data: diagnostico,
      width: '700px',
      maxWidth: '100%'
    }).afterClosed().subscribe(diagnosticoModificado => {
      if (diagnosticoModificado) {
        this.diagnosticoService.updateDiagnostico(
          diagnostico.id,
          diagnosticoModificado,
          this.evolucionCodigo
        ).subscribe((response) => {
          if (response) {
            const index = this.diagnosticos.findIndex((diagnosticoExistente) =>
              diagnosticoExistente.id === diagnosticoModificado.id);
            this.diagnosticos[index] = response;
            this.dataSource.connect().next(this.diagnosticos);
            this.showNotification(this.messages.updatedSuccessMessage, 'CERRAR');
          }
        }, error => {
          this.showNotification(this.messages.updatedFailedMessage, 'CERRAR');
        });
      }
    });
  }


  openDeleteDialog(diagnostico: Diagnostico) {
    this.dialog.open(DeleteModalComponent, {
      data: this.messages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') {
        this.deleteDiagnostico(diagnostico);
      }
    });
  }

  deleteDiagnostico(diagnostico: Diagnostico) {
    this.diagnosticoService.deleteDiagnostico(diagnostico.id, this.evolucionCodigo)
      .subscribe((response) => {
        if (response) {
          const index = this.diagnosticos.findIndex(d => d.id === diagnostico.id);
          this.diagnosticos.splice(index, 1);
          this.dataSource.connect().next(this.diagnosticos);
          this.showNotification(this.messages.deletedSuccessMessage, 'CERRAR');
        } else {
          this.showNotification(this.messages.deleteFailMessage, 'CERRAR');
        }
      });
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  ngOnDestroy() {
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  trackByProperty<T>(column: TableColumn<T>) {
    return column.property;
  }

  get visibleColumns() {
    return this.tableColumns
      .filter(column => column.visible)
      .map(column => column.property);
  }
}
