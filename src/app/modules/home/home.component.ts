import { Component, OnInit } from '@angular/core';
import icInfo from '@iconify/icons-fa-solid/info-circle';
import icLeft from '@iconify/icons-fa-solid/align-left';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import icSearch from '@iconify/icons-ic/twotone-search';
import {PacienteSelectMenuComponent} from 'src/app/core/components/paciente-select-menu/paciente-select-menu.component';
import {MatDialog} from '@angular/material/dialog';

import {Observable} from 'rxjs';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {OwlOptions} from 'ngx-owl-carousel-o';

@Component({
  selector: 'vex-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [
    fadeInUp400ms,
    fadeInRight400ms,
    scaleIn400ms,
    stagger40ms
  ]
})
export class HomeComponent implements OnInit {
  customOptions: OwlOptions = {
    items: 1,
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    nav: true,
    navText: ['', ''],
    autoplay: true,
    navSpeed: 700,
    responsive: {
      0: {
        items: 1,
      }
    },
  }
  saludUser: Observable<SaludUser>;
  icInfo = icInfo;
  icLeft = icLeft;
  icSearch = icSearch;
  nameDispensario$: Observable<string>;
  constructor(
    private catalogoService: CatalogoService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.nameDispensario$ = this.catalogoService.getDispensarioNmesImage();
  }

  selectPatient(): void {
    this.dialog.open(PacienteSelectMenuComponent, {
      width: '900px',
      maxWidth: '100%',
    })
    .afterClosed();
  }
  getUserImage(nombreImagen: string): string {
    return this.catalogoService.getDispensarioImage(nombreImagen);
  }
}
