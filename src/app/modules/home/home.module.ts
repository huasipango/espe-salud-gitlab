import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import {HomeRoutingModule} from 'src/app/modules/home/home-routing.module';
import {FlexModule, GridModule} from '@angular/flex-layout';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {MatButtonModule} from '@angular/material/button';
import {CarouselModule} from 'ngx-owl-carousel-o';



@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    GridModule,
    MatListModule,
    MatIconModule,
    IconModule,
    FlexModule,
    MatButtonModule,
    CarouselModule
  ]
})
export class HomeModule { }
