import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {FormControl} from '@angular/forms';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {Empleado} from 'src/app/core/models/empleado/empleado.model';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icTemperature from '@iconify/icons-fa-solid/temperature-high';
import icAdd from '@iconify/icons-ic/twotone-add';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icFolder from '@iconify/icons-ic/twotone-folder';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {ActvidadExtralaboralService} from 'src/app/modules/salud-ocupacional/antecedente-laboral/services/actvidad-extralaboral.service';
import {ActividadExtralaboral} from 'src/app/modules/salud-ocupacional/antecedente-laboral/models/actividad-extralaboral.model';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {ActividadExtralaboralModalComponent} from 'src/app/modules/salud-ocupacional/antecedente-laboral/components/actividad-extralaboral/actividad-extralaboral-modal/actividad-extralaboral-modal.component';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import icPerson from '@iconify/icons-ic/twotone-person';


@UntilDestroy()
@Component({
  selector: 'vex-actividad-extralaboral',
  templateUrl: './actividad-extralaboral.component.html',
  styleUrls: ['./actividad-extralaboral.component.scss'],
  animations: [
    fadeInUp400ms,
    fadeInRight400ms,
    scaleIn400ms,
    stagger40ms,
    scaleFadeIn400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class ActividadExtralaboralComponent implements OnInit, AfterViewInit {
  layoutCtrl = new FormControl('boxed');
  actividadExtralaboral: ActividadExtralaboral[] = [];
  pacienteActual: Paciente;

  @Input()
  columns: TableColumn<ActividadExtralaboral>[] = [
    {label: 'Empresa', property: 'empresa', type: 'text', visible: true},
    {label: 'Puesto de trabajo', property: 'puestoTrabajo', type: 'text', visible: true},
    {label: 'Actividad desempeñada', property: 'actividad', type: 'text', visible: true, cssClasses: ['text-secondary']},
    {label: 'Trabajo al mes (h)', property: 'meses', type: 'text', visible: true, cssClasses: ['text-secondary']},
    {label: 'Riesgos', property: 'riesgo', type: 'text', visible: true, cssClasses: ['text-secondary']},
    // eslint-disable-next-line max-len
    {label: 'Uso de seguridad', property: 'usabanSeguridad', type: 'text', visible: true, cssClasses: ['text-secondary']},
    {label: 'Recibia vigilancia de salud', property: 'vigilanciaSalud', type: 'text', visible: true, cssClasses: ['text-secondary']},
    {label: 'Observación', property: 'observacion', type: 'text', visible: true, cssClasses: ['text-secondary']},
    {label: 'Actions', property: 'actions', type: 'button', visible: true}
  ];

  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<ActividadExtralaboral> | null;
  selection = new SelectionModel<ActividadExtralaboral>(true, []);
  searchCtrl = new FormControl();
  paciente: Paciente;
  correctos: number;
  icEdit = icEdit;
  icSearch = icSearch;
  icDelete = icDelete;
  icPerson = icPerson;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icMoreHoriz = icMoreHoriz;
  icFolder = icFolder;
  icTemperature = icTemperature;

  constructor(
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    protected actividadExtralaboralService: ActvidadExtralaboralService,
    private pacienteGlobalService: PacienteGlobalService) {
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getActividadExtralaboral();
        }
      });
    this.searchCtrl.valueChanges.pipe(
      untilDestroyed(this)
    ).subscribe(value => this.onFilterChange(value));
  }

  getActividadExtralaboral(): void {
    this.actividadExtralaboralService.getActividadExtra(this.pacienteActual.id)
      .subscribe((data: ActividadExtralaboral[]) => {
        this.actividadExtralaboral = data;
        this.dataSource.data = this.actividadExtralaboral;
      });
  }


  createActividad(): void {
    this.dialog.open(ActividadExtralaboralModalComponent, {
      width: '800px',
      maxWidth: '100%',
      disableClose: true
    }).afterClosed()
      .subscribe((actividad: ActividadExtralaboral) => {
        if (actividad) {
          this.showNotification(' Registro creado EXITOSAMENTE', 'OK');
          this.actividadExtralaboral.push(actividad);
          this.dataSource.connect().next(this.actividadExtralaboral);
        }
      });

  }


  updateActividad(actividad: ActividadExtralaboral): void {
    this.dialog.open(ActividadExtralaboralModalComponent, {
      data: actividad,
      maxWidth: '100%',
      width: '800px',
      disableClose: true

    }).afterClosed()
      .subscribe((updatedActividad) => {
        if (updatedActividad) {
          this.showNotification('Registro actualizado EXITOSAMENTE', 'OK');
          const id = updatedActividad.id;
          console.log(id);
          const index = this.actividadExtralaboral.findIndex((existing) => existing.id === id);
          this.actividadExtralaboral[index] = updatedActividad;
          this.dataSource.connect().next(this.actividadExtralaboral);
        }
      });

  }

  deleteActividad(actividad: ActividadExtralaboral): void {
    this.actividadExtralaboralService.deleteActividadExtra(actividad.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification('Registro eliminado EXITOSAMENTE', 'CERRAR');
          this.actividadExtralaboral.splice(
            this.actividadExtralaboral.findIndex((existing) =>
              existing.id === actividad.id), 1
          );
          this.dataSource.connect().next(this.actividadExtralaboral);
        } else {
          this.showNotification('No se pudo eliminar el registro', 'CERRAR');
        }
      });
  }


  openDeleteDialog(actividad: ActividadExtralaboral): void {
    this.dialog.open(DeleteModalComponent, {
      data: '¿Estas seguro de eliminar este registro?',
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') {
        this.deleteActividad(actividad);
      }
    });
  }

  onFilterChange(value: string) {
    if (!this.dataSource) {
      return;
    }
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  openSearchModal() {

  }
}
