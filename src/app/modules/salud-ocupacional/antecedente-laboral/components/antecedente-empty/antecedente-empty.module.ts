import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { IconModule } from '@visurel/iconify-angular';
import { MatIconModule } from '@angular/material/icon';
import { AntecedenteEmptyComponent } from './antecedente-empty.component';

@NgModule({
  declarations: [AntecedenteEmptyComponent],
  exports: [AntecedenteEmptyComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatButtonModule,
    IconModule,
    MatIconModule
  ]
})
export class AntecedenteEmptyModule { }
