import {Component, OnInit} from '@angular/core';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-fa-solid/plus';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {ActivatedRoute, Router} from '@angular/router';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import { AntecedenteLaboral } from '../../models/antecedente-laboral.model';
import { PacienteGlobalService } from 'src/app/core/services/paciente-global.service';
import { AntecedenteLaboralService } from '../../services/antecedente-laboral.service';
import { AntecedenteLaboralSharedService } from '../../services/antecedente-laboral-shared.service';
import { LoadingService } from 'src/app/core/services/loading.service';

@Component({
  selector: 'vex-antecedente-empty',
  templateUrl: './antecedente-empty.component.html',
  styleUrls: ['./antecedente-empty.component.scss'],
  animations: [
    scaleFadeIn400ms
  ]
})
export class AntecedenteEmptyComponent implements OnInit {
  icSearch = icSearch;
  icAdd = icAdd;

  antecedente: AntecedenteLaboral;
  pacienteActual: Paciente;

  constructor(
    private dialog: MatDialog,
    private pacienteGlobalService: PacienteGlobalService,
    private router: Router,
    private snackbar: MatSnackBar,
    private route: ActivatedRoute,
    private antecedenteService: AntecedenteLaboralService,
    private antecedenteLaboralSharedService: AntecedenteLaboralSharedService,
    private loadingService: LoadingService,
  ) {
    this.antecedente = {} as AntecedenteLaboral;
  }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
        }
      });
  }

  createNewAntecedente(): void {
    this.antecedente.idPaciente = this.pacienteActual.id;
    this.antecedente.idUsuario = 369981 //TODO: Use the correct idPassword
    this.loadingService.showLoaderUntilCompleted(
      this.antecedenteService.createAntecedenteLaboral(this.antecedente)
    ).subscribe((data: AntecedenteLaboral) => {
        if (data) {
          this.antecedenteLaboralSharedService.setAntecedenteLaboral(data);
          this.showNotification('Registro guardado EXITOSAMENTE', 'OK');
          this.navigateBack();
        }
      },
      (error) => {
        this.showNotification('ERROR al guardar el registro', 'CERRAR');
      }
    );
  }

  navigateBack(): void {
    this.router.navigate(['./antecedentes'], {relativeTo: this.route});
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

}
