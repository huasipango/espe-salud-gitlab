import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {FormControl} from '@angular/forms';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {EnfermedadProfesional} from 'src/app/modules/salud-ocupacional/antecedente-laboral/models/enfermedad-profesional.model';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icFolder from '@iconify/icons-ic/twotone-folder';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {Router} from '@angular/router';
// eslint-disable-next-line max-len
// tslint:disable-next-line:max-line-length
import {EnfermedadProfesionalService} from 'src/app/modules/salud-ocupacional/antecedente-laboral/services/enfermedad-profesional.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {EnfermedadProfesionalModalComponent} from 'src/app/modules/salud-ocupacional/antecedente-laboral/components/enfermedad-profesional/enfermedad-profesional-modal/enfermedad-profesional-modal.component';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';

@UntilDestroy()
@Component({
  selector: 'vex-enfermedad-profesional',
  templateUrl: './enfermedad-profesional.component.html',
  styleUrls: ['./enfermedad-profesional.component.scss'],

  animations: [
    fadeInUp400ms,
    fadeInRight400ms,
    scaleIn400ms,
    stagger40ms,
    scaleFadeIn400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})



export class EnfermedadProfesionalComponent implements OnInit, AfterViewInit {
  layoutCtrl = new FormControl('boxed');
  enfermedadProfesional: EnfermedadProfesional[] = [];
  pacienteActual: Paciente;

  @Input()
  columns: TableColumn<EnfermedadProfesional>[] = [
    { label: 'Empresa', property: 'empresa', type: 'text', visible: true, cssClasses: ['text-secondary'] },
    { label: 'Fecha', property: 'fecha', type: 'text', visible: true, cssClasses: ['text-secondary'] },
    { label: 'Lugar de Atención', property: 'lugarAtencion', type: 'text', visible: true, cssClasses: ['text-secondary'] },
    { label: 'Recibió Atención', property: 'recibioAtencion', type: 'text', visible: true, cssClasses: ['text-secondary'] },
    // eslint-disable-next-line max-len
    { label: 'Riesgo Implicado', property: 'riesgoImplicado', type: 'text', visible: true, cssClasses: ['text-secondary']},
    { label: 'Secuelas', property: 'secuelas', type: 'text', visible: true, cssClasses: ['text-secondary']},
    { label: 'Horas de Reposo', property: 'horasReposo', type: 'text', visible: true, cssClasses: ['text-secondary'] },
    { label: 'Diagnostico', property: 'idDiagnostico', type: 'text', visible: true, cssClasses: ['text-secondary'] },
    { label: 'Observaciones', property: 'observaciones', type: 'text', visible: true, cssClasses: ['text-secondary'] },
    { label: 'Actions', property: 'actions', type: 'button', visible: true }
  ];

  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<EnfermedadProfesional> | null;
  selection = new SelectionModel<EnfermedadProfesional>(true, []);
  searchCtrl = new FormControl();
  paciente: Paciente;
  correctos: number;
  icEdit = icEdit;
  icSearch = icSearch;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icMoreHoriz = icMoreHoriz;
  icFolder = icFolder;

  constructor(private snackbar: MatSnackBar,
              private dialog: MatDialog,
              protected enfermedadProfesionalService: EnfermedadProfesionalService,
              private pacienteGlobalService: PacienteGlobalService, private router: Router) { }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente){
          this.pacienteActual = paciente;
          this.getEnfermedadProfesional();
        }
      });
    this.searchCtrl.valueChanges.pipe(
      untilDestroyed(this)
    ).subscribe(value => this.onFilterChange(value));

  }

  getEnfermedadProfesional(): void {
    this.enfermedadProfesionalService.getEnfermedadProfesional(this.pacienteActual.id)
      .subscribe((data: EnfermedadProfesional[]) => {
        this.enfermedadProfesional = data;
        this.dataSource.data = this.enfermedadProfesional;
      });
  }

  createActividad(): void {
    this.dialog.open(EnfermedadProfesionalModalComponent, {
      width: '800px',
      maxWidth: '100%',
      disableClose: true
    }).afterClosed()
      .subscribe((actividad: EnfermedadProfesional) => {
        if (actividad) {
          this.showNotification(' Registro creado EXITOSAMENTE', 'OK');
          this.enfermedadProfesional.push(actividad);
          this.dataSource.connect().next(this.enfermedadProfesional);
        }
      });

  }

  updateActividad(actividad: EnfermedadProfesional): void {
    this.dialog.open(EnfermedadProfesionalModalComponent, {
      data: actividad,
      maxWidth: '100%',
      width: '800px',
      disableClose: true

    }).afterClosed()
      .subscribe((updatedActividad) => {
        if (updatedActividad) {
          this.showNotification('Registro actualizado EXITOSAMENTE', 'OK');
          const id = updatedActividad.id;
          console.log(id);
          const index = this.enfermedadProfesional.findIndex((existing) => existing.id === id);
          this.enfermedadProfesional[index] = updatedActividad;
          this.dataSource.connect().next(this.enfermedadProfesional);
        }
      });

  }

  deleteActividad(actividad: EnfermedadProfesional): void {
    this.enfermedadProfesionalService.deleteEnfermedadProfesional(actividad.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification('Registro eliminado EXITOSAMENTE', 'CERRAR');
          this.enfermedadProfesional.splice(
            this.enfermedadProfesional.findIndex((existing) =>
              existing.id === actividad.id), 1
          );
          this.dataSource.connect().next(this.enfermedadProfesional);
        } else {
          this.showNotification('No se pudo eliminar el registro', 'CERRAR');
        }
      });
  }

  openDeleteDialog(actividad: EnfermedadProfesional): void {
    this.dialog.open(DeleteModalComponent, {
      data: '¿Estas seguro de eliminar este registro?',
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') { this.deleteActividad(actividad); }
    });
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  onFilterChange(value: string) {
    if (!this.dataSource) {
      return;
    }
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  openSearchModal() {

  }

}
