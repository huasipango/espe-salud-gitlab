import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EnfermedadProfesionalComponent } from './enfermedad-profesional.component';
import { EnfermedadProfesionalModalComponent } from './enfermedad-profesional-modal/enfermedad-profesional-modal.component';
import {ExtendedModule, FlexModule} from '@angular/flex-layout';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {IconModule} from '@visurel/iconify-angular';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSelectModule} from '@angular/material/select';
import {NgxMatDatetimePickerModule, NgxMatNativeDateModule, NgxMatTimepickerModule} from '@angular-material-components/datetime-picker';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatAutocompleteModule} from "@angular/material/autocomplete";



@NgModule({
  declarations: [EnfermedadProfesionalComponent, EnfermedadProfesionalModalComponent],
  exports: [
    EnfermedadProfesionalComponent
  ],
    imports: [
        CommonModule,
        FlexModule,
        ContainerModule,
        IconModule,
        BreadcrumbsModule,
        ExtendedModule,
        MatButtonModule,
        MatTooltipModule,
        MatIconModule,
        ReactiveFormsModule,
        MatMenuModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatCheckboxModule,
        FormsModule,
        MatDialogModule,
        MatInputModule,
        MatSelectModule,
        NgxMatDatetimePickerModule,
        NgxMatNativeDateModule,
        NgxMatTimepickerModule,
        MatDatepickerModule,
        LoadingModule,
        MatAutocompleteModule
    ]
})
export class EnfermedadProfesionalModule { }
