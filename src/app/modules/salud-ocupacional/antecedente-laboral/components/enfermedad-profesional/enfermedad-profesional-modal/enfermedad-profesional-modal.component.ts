import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ThemePalette} from '@angular/material/core';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {LoadingService} from 'src/app/core/services/loading.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {EnfermedadProfesional} from 'src/app/modules/salud-ocupacional/antecedente-laboral/models/enfermedad-profesional.model';
// eslint-disable-next-line max-len
import {EnfermedadProfesionalService} from 'src/app/modules/salud-ocupacional/antecedente-laboral/services/enfermedad-profesional.service';
import icClose from '@iconify/icons-ic/twotone-close';
import icCalendarCheck from '@iconify/icons-fa-solid/calendar-check';
import icPerson from '@iconify/icons-ic/twotone-person';
import outlineWork from '@iconify/icons-ic/outline-work';
import icInfo from '@iconify/icons-fa-solid/info-circle';
import icBuilding from '@iconify/icons-fa-solid/building';
import icRiesgo from '@iconify/icons-ic/warning';
import icSecuridad from '@iconify/icons-ic/security';
import icHora from '@iconify/icons-ic/hourglass-empty';
import icMedKit from '@iconify/icons-fa-solid/medkit';
import icNotesMedical from '@iconify/icons-fa-solid/notes-medical';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';
import {TipoEnfermedadCIE10} from 'src/app/core/models/catalogo/tipo-enfermedad-cie10.model';
import {Observable} from 'rxjs';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {forbiddenObjectValidator} from 'src/app/shared/validators/forbbiden-object-validator';
import {debounceTime, distinctUntilChanged, startWith} from 'rxjs/operators';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {AntecedenteLaboralSharedService} from 'src/app/modules/salud-ocupacional/antecedente-laboral/services/antecedente-laboral-shared.service';

@UntilDestroy()
@Component({
  selector: 'vex-enfermedad-profesional-modal',
  templateUrl: './enfermedad-profesional-modal.component.html',
  styleUrls: ['./enfermedad-profesional-modal.component.scss']
})
export class EnfermedadProfesionalModalComponent implements OnInit {
  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  icClose = icClose;
  icCalendarCheck = icCalendarCheck;
  icPerson = icPerson;
  outlineWork = outlineWork;
  icNotesMedical = icNotesMedical;
  icArrowDropDown = icArrowDropDown;
  icRiesgo = icRiesgo;
  icBuilding = icBuilding;
  icInfo = icInfo;
  icHora = icHora;
  icMedKit = icMedKit;
  icSecuridad = icSecuridad;

  diagnosticoCtrl: FormControl;
  filteredCodigosCie: Observable<TipoEnfermedadCIE10[]>;
  public touchUi = false;
  public enableMeridian = false;
  public stepSecond = 1;
  public color: ThemePalette = 'primary';
  loading = false;
  errorMessages = FORM_ERROR_MESSAGES;

  constructor(@Inject(MAT_DIALOG_DATA) public defaults: EnfermedadProfesional,
              private dialogRef: MatDialogRef<EnfermedadProfesionalModalComponent>,
              private fb: FormBuilder,
              private catalogoService: CatalogoService,
              private actividadService: EnfermedadProfesionalService,
              private loadingService: LoadingService,
              private snackbar: MatSnackBar,
              private antecedenteSharedService: AntecedenteLaboralSharedService) { }

  ngOnInit(): void {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as EnfermedadProfesional;
    }

    this.diagnosticoCtrl = new FormControl(
      this.defaults.idDiagnostico, [Validators.required, forbiddenObjectValidator]
    );

    if (this.defaults.idDiagnostico) {
      this.catalogoService.getCodigoCIE10(this.defaults.idDiagnostico)
        .subscribe((codigo) => {
          if (codigo) {
            this.diagnosticoCtrl.patchValue(codigo);
          }
        });
    }

    this.diagnosticoCtrl.valueChanges
      .pipe(
        startWith(''),
        debounceTime(400),
        distinctUntilChanged(),
        untilDestroyed(this)
      ).subscribe(value => this.searchCie(value || ''));

    this.form = this.fb.group({
      id: this.defaults.id || null,
      empresa: [this.defaults.empresa || null, Validators.required],
      fecha: [this.defaults.fecha || null , Validators.required],
      lugarAtencion: [this.defaults.lugarAtencion || null, Validators.required],
      observaciones: [this.defaults.observaciones || null],
      recibioAtencion: [this.defaults.recibioAtencion || false],
      riesgoImplicado: [this.defaults.riesgoImplicado || null, Validators.required],
      secuelas: [this.defaults.secuelas || null, Validators.required],
      horasReposo: [this.defaults.horasReposo || null, Validators.required],
      idDiagnostico: this.diagnosticoCtrl,
      idAntecedenteLaboral: [null, Validators.required]
    });

    this.antecedenteSharedService.antecedenteLaboral$
      .subscribe((antecedente) => {
        if (antecedente) {
          this.form.patchValue({idAntecedenteLaboral: antecedente.id});
        }
      });
  }

  save() {
    const actividad = this.form.getRawValue();
    const cie10 = actividad.idDiagnostico;
    actividad.idDiagnostico = cie10.codigo;
    if (this.mode === 'create') {
      this.createActividad(actividad);
    } else if (this.mode === 'update') {
      this.updateActividad(actividad);
    }
  }

  createActividad(actividad: EnfermedadProfesional): void {
    this.loadingService.showLoaderUntilCompleted(
      this.actividadService.createEnfermedadProfesional(actividad)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      } else {
        this.showNotification('No se pudo crear la actividad extralaboral', 'CERRAR');
      }
    });
  }

  updateActividad(actividad: EnfermedadProfesional): void {
    this.loadingService.showLoaderUntilCompleted(
      this.actividadService.updateEnfermedadProfesional(actividad.id, actividad)
    ).subscribe((response) => {
      if (response) {
        console.log(response);
        this.dialogRef.close(response);
      } else {
        this.showNotification('No se pudo crear la actividad extralaboral', 'CERRAR');
      }
    });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  searchCie(value: any) {
    if (value !== '' && typeof value === 'string' && value.trim().length > 1) {
      this.filteredCodigosCie = this.catalogoService.getCodigosCIE10ByQuery(value);
    }
  }

  displayFn(cie: TipoEnfermedadCIE10): string {
    return cie ? `${cie.codigo} -- ${cie.nombre}` : '';
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

}