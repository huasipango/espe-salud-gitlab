import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {FormControl} from '@angular/forms';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icFolder from '@iconify/icons-ic/twotone-folder';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import icPerson from '@iconify/icons-ic/twotone-person';
import {IncidenteTrabajo} from 'src/app/modules/salud-ocupacional/antecedente-laboral/models/incidente-trabajo.model';
import {IncidenteTrabajoService} from 'src/app/modules/salud-ocupacional/antecedente-laboral/services/incidente-trabajo.service';
import {IncidenteTrabajoModalComponent} from 'src/app/modules/salud-ocupacional/antecedente-laboral/components/incidente-trabajo/incidente-trabajo-modal/incidente-trabajo-modal.component';

@UntilDestroy()
@Component({
  selector: 'vex-incidente-trabajo',
  templateUrl: './incidente-trabajo.component.html',
  styleUrls: ['./incidente-trabajo.component.scss'],
  animations: [
    fadeInUp400ms,
    fadeInRight400ms,
    scaleIn400ms,
    stagger40ms,
    scaleFadeIn400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class IncidenteTrabajoComponent implements OnInit, AfterViewInit {
  layoutCtrl = new FormControl('boxed');
  incidentesTrabajo: IncidenteTrabajo[];
  pacienteActual: Paciente;

  @Input()
  columns: TableColumn<IncidenteTrabajo>[] = [
    { label: 'Recibió Atención', property: 'recibioAtencion', type: 'text', visible: true, cssClasses: ['text-secondary'] },
    { label: 'Lugar de Atención', property: 'lugarAtencion', type: 'text', visible: true, cssClasses: ['text-secondary'] },
    { label: 'Fecha', property: 'fecha', type: 'text', visible: true, cssClasses: ['text-secondary'] },
    { label: 'Riesgo Implicado', property: 'riesgoImplicado', type: 'text', visible: true, cssClasses: ['text-secondary']},
    { label: 'Naturaleza del incidente', property: 'naturaleza', type: 'text', visible: true, cssClasses: ['text-secondary'] },
    { label: 'Parte del cuerpo afectada', property: 'parteCuerpoAfectada', type: 'text', visible: true, cssClasses: ['text-secondary'] },
    { label: 'Empresa', property: 'empresa', type: 'text', visible: true, cssClasses: ['text-secondary'] },
    { label: 'Horas de Reposo', property: 'horasReposo', type: 'text', visible: true, cssClasses: ['text-secondary'] },
    { label: 'Secuelas', property: 'secuelas', type: 'text', visible: true, cssClasses: ['text-secondary']},
    { label: 'Observaciones', property: 'observaciones', type: 'text', visible: true, cssClasses: ['text-secondary'] },
    { label: 'Actions', property: 'actions', type: 'button', visible: true }
  ];

  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<IncidenteTrabajo> | null;
  selection = new SelectionModel<IncidenteTrabajo>(true, []);
  searchCtrl = new FormControl();
  paciente: Paciente;
  icEdit = icEdit;
  icSearch = icSearch;
  icMoreHoriz = icMoreHoriz;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icFolder = icFolder;
  icPerson = icPerson;
  constructor(
      private snackbar: MatSnackBar,
      private dialog: MatDialog,
      protected incidenteTrabajoService: IncidenteTrabajoService,
      private pacienteGlobalService: PacienteGlobalService) {
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getIncidente();
        }
      });
  }

  getIncidente() {
    this.incidenteTrabajoService.getIncidentesTrabajo(this.pacienteActual.id)
      .subscribe((data: IncidenteTrabajo[]) => {
          this.incidentesTrabajo = data;
          this.dataSource.data = this.incidentesTrabajo;
        }
      );
  }

  createIncidente(): void {
    this.dialog.open(IncidenteTrabajoModalComponent, {
      width: '800px',
      maxWidth: '100%',
      disableClose: true
    }).afterClosed()
      .subscribe((incidente: IncidenteTrabajo) => {
        if (incidente) {
          this.showNotification(' Registro creado EXITOSAMENTE', 'OK');
          this.incidentesTrabajo.push(incidente);
          this.dataSource.connect().next(this.incidentesTrabajo);
        }
      });

  }


  updateIncidente(incidente: IncidenteTrabajo): void {
    this.dialog.open(IncidenteTrabajoModalComponent, {
      data: incidente,
      maxWidth: '100%',
      width: '800px',
      disableClose: true

    }).afterClosed()
      .subscribe((updatedIncidente) => {
        if (updatedIncidente) {
          this.showNotification('Registro actualizado EXITOSAMENTE', 'OK');
          const id = updatedIncidente.id;
          console.log(id);
          const index = this.incidentesTrabajo.findIndex((existing) => existing.id === id);
          this.incidentesTrabajo[index] = updatedIncidente;
          this.dataSource.connect().next(this.incidentesTrabajo);
        }
      });

  }

  deleteIncidenteTrabajo(incidente: IncidenteTrabajo): void {
    this.incidenteTrabajoService.deleteIncidenteTrabajo(incidente.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification('Registro eliminado EXITOSAMENTE', 'CERRAR');
          this.incidentesTrabajo.splice(
            this.incidentesTrabajo.findIndex((existing) =>
              existing.id === incidente.id), 1
          );
          this.dataSource.connect().next(this.incidentesTrabajo);
        } else {
          this.showNotification('No se pudo eliminar el registro', 'CERRAR');
        }
      });
  }


  openDeleteDialog(incidente: IncidenteTrabajo): void {
    this.dialog.open(DeleteModalComponent, {
      data: '¿Estas seguro de eliminar este registro?',
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') {
        this.deleteIncidenteTrabajo(incidente);
      }
    });
  }

  onFilterChange(value: string) {
    if (!this.dataSource) {
      return;
    }
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  openSearchModal() {

  }

}
