import { Component, Inject, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ThemePalette} from '@angular/material/core';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {LoadingService} from 'src/app/core/services/loading.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {IncidenteTrabajo} from 'src/app/modules/salud-ocupacional/antecedente-laboral/models/incidente-trabajo.model';
// eslint-disable-next-line max-len
import {IncidenteTrabajoService} from 'src/app/modules/salud-ocupacional/antecedente-laboral/services/incidente-trabajo.service';
import icClose from '@iconify/icons-ic/twotone-close';
import icCalendarCheck from '@iconify/icons-fa-solid/calendar-check';
import icPerson from '@iconify/icons-ic/twotone-person';
import outlineWork from '@iconify/icons-ic/outline-work';
import icInfo from '@iconify/icons-fa-solid/info-circle';
import icBuilding from '@iconify/icons-fa-solid/building';
import icRiesgo from '@iconify/icons-ic/warning';
import icSecuridad from '@iconify/icons-ic/security';
import icHora from '@iconify/icons-ic/hourglass-empty';
import icMedKit from '@iconify/icons-fa-solid/medkit';
import {AntecedenteLaboralSharedService} from 'src/app/modules/salud-ocupacional/antecedente-laboral/services/antecedente-laboral-shared.service';

@Component({
  selector: 'vex-incidente-trabajo-modal',
  templateUrl: './incidente-trabajo-modal.component.html',
  styleUrls: ['./incidente-trabajo-modal.component.scss']
})
export class IncidenteTrabajoModalComponent implements OnInit {

  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  icClose = icClose;
  icCalendarCheck = icCalendarCheck;
  icPerson = icPerson;
  outlineWork = outlineWork;
  icRiesgo = icRiesgo;
  icBuilding = icBuilding;
  icInfo = icInfo;
  icHora = icHora;
  icMedKit = icMedKit;
  icSecuridad = icSecuridad;

  public showSpinners = true;
  public touchUi = false;
  public enableMeridian = false;
  public stepSecond = 1;
  public color: ThemePalette = 'primary';
  loading = false;
  errorMessages = FORM_ERROR_MESSAGES;

  constructor(@Inject(MAT_DIALOG_DATA) public defaults: IncidenteTrabajo,
              private dialogRef: MatDialogRef<IncidenteTrabajoModalComponent>,
              private fb: FormBuilder,
              private incidenteService: IncidenteTrabajoService,
              private loadingService: LoadingService,
              private antecedenteSharedService: AntecedenteLaboralSharedService,
              private snackbar: MatSnackBar) { }

  ngOnInit(): void {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as IncidenteTrabajo;
    }

    this.form = this.fb.group({
      id: [this.defaults.id || null],
      recibioAtencion: [this.defaults.recibioAtencion || false],
      lugarAtencion: [this.defaults.lugarAtencion || null, Validators.required],
      fecha: [this.defaults.fecha || null , Validators.required],
      riesgoImplicado: [this.defaults.riesgoImplicado || null, Validators.required],
      naturaleza: [this.defaults.naturaleza || null, Validators.required],
      parteCuerpoAfectada: [this.defaults.parteCuerpoAfectada || null, Validators.required],
      empresa: [this.defaults.empresa || null, Validators.required],
      horasReposo: [this.defaults.horasReposo || null, Validators.required],
      secuelas: [this.defaults.secuelas || null, Validators.required],
      observaciones: [this.defaults.observaciones || null],
      idAntecedenteLaboral: [null, Validators.required]
    });

    this.antecedenteSharedService.antecedenteLaboral$
      .subscribe((antecedente) => {
        if (antecedente) {
          this.form.patchValue({idAntecedenteLaboral: antecedente.id});
        }
      });
  }

  save() {
    const incidente: IncidenteTrabajo = this.form.value;
    if (this.mode === 'create') {
      this.createIncidente(incidente);
    } else if (this.mode === 'update') {
      this.updateIncidente(incidente);
    }
  }

  createIncidente(incidente: IncidenteTrabajo): void {
    this.loadingService.showLoaderUntilCompleted(
      this.incidenteService.createIncidenteTrabajo(incidente)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      } else {
        this.showNotification('No se pudo crear la incidente de trabajo', 'CERRAR');
      }
    });
  }

  updateIncidente(incidente: IncidenteTrabajo): void {
    this.loadingService.showLoaderUntilCompleted(
      this.incidenteService.updateIncidenteTrabajo(incidente.id, incidente)
    ).subscribe((response) => {
      if (response) {
        console.log(response);
        this.dialogRef.close(response);
      } else {
        this.showNotification('No se pudo crear la incidente de trabajo', 'CERRAR');
      }
    });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }


}
