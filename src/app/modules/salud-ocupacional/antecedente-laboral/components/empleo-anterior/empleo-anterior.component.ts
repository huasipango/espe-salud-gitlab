import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {FormControl} from '@angular/forms';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {Empleado} from 'src/app/core/models/empleado/empleado.model';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {EmpleoAnteriorService} from 'src/app/modules/salud-ocupacional/antecedente-laboral/services/empleo-anterior.service';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {EmpleoAnterior} from 'src/app/modules/salud-ocupacional/antecedente-laboral/models/empleo-anterior.model';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icFolder from '@iconify/icons-ic/twotone-folder';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icPerson from '@iconify/icons-ic/twotone-person';
import {EmpleoAnteriorModalComponent} from 'src/app/modules/salud-ocupacional/antecedente-laboral/components/empleo-anterior/empleo-anterior-modal/empleo-anterior-modal.component';
@UntilDestroy()
@Component({
  selector: 'vex-empleo-anterior',
  templateUrl: './empleo-anterior.component.html',
  styleUrls: ['./empleo-anterior.component.scss'],
  animations: [
    fadeInUp400ms,
    fadeInRight400ms,
    scaleIn400ms,
    stagger40ms,
    scaleFadeIn400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class EmpleoAnteriorComponent implements OnInit, AfterViewInit {
  layoutCtrl = new FormControl('boxed');
  empleoAnterior: EmpleoAnterior[] = [];
  pacienteActual: Paciente;
  empleadoActual: Empleado;
  @Input()
  columns: TableColumn<EmpleoAnterior>[] = [
    { label: 'Empresa', property: 'empresa', type: 'text', visible: true, cssClasses: ['text-secondary'] },
    { label: 'Puesto de trabajo', property: 'puestoTrabajo', type: 'text', visible: true, cssClasses: ['text-secondary'] },
    { label: 'Actividad desempeñada', property: 'actividad', type: 'text', visible: true, cssClasses: ['text-secondary'] },
    { label: 'Trabajo al mes (h)', property: 'meses', type: 'text', visible: true, cssClasses: ['text-secondary'] },
    { label: 'Riesgos', property: 'riesgo', type: 'text', visible: true, cssClasses: ['text-secondary'] },
    // eslint-disable-next-line max-len
    { label: 'Uso de seguridad', property: 'usabanSeguridad', type: 'text', visible: true, cssClasses: ['text-secondary']},
    { label: 'Recibia vigilancia de salud', property: 'vigilanciaSalud', type: 'text', visible: true, cssClasses: ['text-secondary']},
    { label: 'Observación', property: 'observacion', type: 'text', visible: true, cssClasses: ['text-secondary'] },
    { label: 'Actions', property: 'actions', type: 'button', visible: true }
  ];

  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<EmpleoAnterior> | null;
  selection = new SelectionModel<EmpleoAnterior>(true, []);
  searchCtrl = new FormControl();
  paciente: Paciente;
  correctos: number;
  icEdit = icEdit;
  icSearch = icSearch;
  icDelete = icDelete;
  icPerson = icPerson;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icMoreHoriz = icMoreHoriz;
  icFolder = icFolder;
  constructor(private snackbar: MatSnackBar,
              private dialog: MatDialog,
              protected empleoAnteriorService: EmpleoAnteriorService,
              private pacienteGlobalService: PacienteGlobalService, private router: Router, ) {
  }
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente){
          this.pacienteActual = paciente;
          this.getEmpleoAnterior();
        }
      });
    this.searchCtrl.valueChanges.pipe(
      untilDestroyed(this)
    ).subscribe(value => this.onFilterChange(value));
  }

  getEmpleoAnterior(): void {
    this.empleoAnteriorService.getEmpleoAnterior(this.pacienteActual.id)
      .subscribe((data: EmpleoAnterior[]) => {
        this.empleoAnterior = data;
        this.dataSource.data = this.empleoAnterior;
      });
  }


  createEmpleo(): void {
    this.dialog.open(EmpleoAnteriorModalComponent, {
      width: '800px',
      maxWidth: '100%',
      disableClose: true
    }).afterClosed()
      .subscribe((empleo: EmpleoAnterior) => {
        if (empleo) {
          this.showNotification(' Registro creado EXITOSAMENTE', 'OK');
          this.empleoAnterior.push(empleo);
          this.dataSource.connect().next(this.empleoAnterior);
        }
      });

  }


  updateEmpleo(empleo: EmpleoAnterior): void {
    this.dialog.open(EmpleoAnteriorModalComponent, {
      data: empleo,
      maxWidth: '100%',
      width: '800px',
      disableClose: true

    }).afterClosed()
      .subscribe((updatedEmpleo) => {
        if (updatedEmpleo) {
          this.showNotification('Registro actualizado EXITOSAMENTE', 'OK');
          const id = updatedEmpleo.id;
          console.log(id);
          const index = this.empleoAnterior.findIndex((existing) => existing.id === id);
          this.empleoAnterior[index] = updatedEmpleo;
          this.dataSource.connect().next(this.empleoAnterior);
        }
      });

  }

  deleteEmpleo(empleo: EmpleoAnterior): void {
    this.empleoAnteriorService.deleteEmpleoAnterior(empleo.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification('Registro eliminado EXITOSAMENTE', 'CERRAR');
          this.empleoAnterior.splice(
            this.empleoAnterior.findIndex((existing) =>
              existing.id === empleo.id), 1
          );
          this.dataSource.connect().next(this.empleoAnterior);
        } else {
          this.showNotification('No se pudo eliminar el registro', 'CERRAR');
        }
      });
  }


  openDeleteDialog(empleo: EmpleoAnterior): void {
    this.dialog.open(DeleteModalComponent, {
      data: '¿Estas seguro de eliminar este registro?',
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') { this.deleteEmpleo(empleo); }
    });
  }

  onFilterChange(value: string) {
    if (!this.dataSource) {
      return;
    }
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  openSearchModal() {

  }
}
