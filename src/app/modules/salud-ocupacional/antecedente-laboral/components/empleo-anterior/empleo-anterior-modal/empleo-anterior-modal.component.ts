import {Component, Inject, OnInit} from '@angular/core';
import {ThemePalette} from '@angular/material/core';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoadingService} from 'src/app/core/services/loading.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import icClose from '@iconify/icons-ic/twotone-close';
import {EmpleoAnteriorService} from 'src/app/modules/salud-ocupacional/antecedente-laboral/services/empleo-anterior.service';
import {enumSelector} from 'src/app/core/utils/enum-to-string-util';
import {RiesgoLaboral} from 'src/app/core/enums/riesgo-laboral.enum';
import icCalendarCheck from '@iconify/icons-fa-solid/calendar-check';
import icPerson from '@iconify/icons-ic/twotone-person';
import outlineWork from '@iconify/icons-ic/outline-work';
import icActivity from '@iconify/icons-fa-solid/book-open';
import icInfo from '@iconify/icons-fa-solid/info-circle';
import icBuilding from '@iconify/icons-fa-solid/building';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {EmpleoAnterior} from 'src/app/modules/salud-ocupacional/antecedente-laboral/models/empleo-anterior.model';
import {AntecedenteLaboralSharedService} from 'src/app/modules/salud-ocupacional/antecedente-laboral/services/antecedente-laboral-shared.service';


@Component({
  selector: 'vex-empleo-anterior-modal',
  templateUrl: './empleo-anterior-modal.component.html',
  styleUrls: ['./empleo-anterior-modal.component.scss'],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class EmpleoAnteriorModalComponent implements OnInit {
  form: FormGroup;
  mode: 'create' | 'update' = 'create';
  icClose = icClose;
  icCalendarCheck = icCalendarCheck;
  icPerson = icPerson;
  outlineWork = outlineWork;
  icActivity = icActivity;
  icBuilding = icBuilding;
  icInfo = icInfo;

  riesgos = enumSelector(RiesgoLaboral);

  public showSpinners = true;
  public touchUi = false;
  public enableMeridian = false;
  public stepSecond = 1;
  public color: ThemePalette = 'primary';
  loading = false;
  errorMessages = FORM_ERROR_MESSAGES;
  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: EmpleoAnterior,
    private dialogRef: MatDialogRef<EmpleoAnteriorModalComponent>,
    private fb: FormBuilder,
    private empleoService: EmpleoAnteriorService,
    private loadingService: LoadingService,
    private antecedenteSharedService: AntecedenteLaboralSharedService,
    private snackbar: MatSnackBar) { }

  ngOnInit(): void {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as EmpleoAnterior;
    }

    this.form = this.fb.group({
      id: [this.defaults.id || null],
      empresa: [this.defaults.empresa || null, Validators.required],
      puestoTrabajo: [this.defaults.puestoTrabajo || null , Validators.required],
      actividad: [this.defaults.actividad || '', Validators.required],
      meses: [this.defaults.meses || null, Validators.required],
      usabanSeguridad: [this.defaults.usabanSeguridad || false],
      vigilanciaSalud: [this.defaults.vigilanciaSalud || false],
      observacion: [this.defaults.observacion || null],
      riesgo: [this.defaults.riesgo || null, Validators.required],
      idAntecedenteLaboral: [null, Validators.required]
    });

    this.antecedenteSharedService.antecedenteLaboral$
      .subscribe((antecedente) => {
        if (antecedente) {
          this.form.patchValue({idAntecedenteLaboral: antecedente.id});
        }
      });
  }
  save() {
    const empleo: EmpleoAnterior = this.form.value;
    if (this.mode === 'create') {
      this.createEmpleo(empleo);
    } else if (this.mode === 'update') {
      this.updateEmpleo(empleo);
    }
  }

  createEmpleo(empleo: EmpleoAnterior): void {
    this.loadingService.showLoaderUntilCompleted(
      this.empleoService.createEmpleoAnterior(empleo)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      } else {
        this.showNotification('No se pudo crear el Empleo Anterior', 'CERRAR');
      }
    });
  }

  updateEmpleo(empleo: EmpleoAnterior): void {
    this.loadingService.showLoaderUntilCompleted(
      this.empleoService.updateEmpleoAnterior(empleo.id, empleo)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      } else {
        this.showNotification('No se pudo crear el empleo anterior', 'CERRAR');
      }
    });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }
}
