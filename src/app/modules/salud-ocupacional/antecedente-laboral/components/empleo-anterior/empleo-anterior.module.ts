import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmpleoAnteriorComponent } from './empleo-anterior.component';
import { EmpleoAnteriorModalComponent } from './empleo-anterior-modal/empleo-anterior-modal.component';
import {IconModule} from "@visurel/iconify-angular";
import {BreadcrumbsModule} from "../../../../../../@vex/components/breadcrumbs/breadcrumbs.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ExtendedModule, FlexModule} from "@angular/flex-layout";
import {MatButtonModule} from "@angular/material/button";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatIconModule} from "@angular/material/icon";
import {_MatMenuDirectivesModule, MatMenuModule} from "@angular/material/menu";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {ContainerModule} from "../../../../../../@vex/directives/container/container.module";
import {MatSortModule} from "@angular/material/sort";
import {MatDialogModule} from "@angular/material/dialog";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {LoadingModule} from "../../../../../shared/components/loading/loading.module";
import {MatInputModule} from "@angular/material/input";



@NgModule({
    declarations: [EmpleoAnteriorComponent, EmpleoAnteriorModalComponent],
  exports: [
    EmpleoAnteriorComponent,
    EmpleoAnteriorComponent
  ],
  imports: [
    CommonModule,
    IconModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    FlexModule,
    ExtendedModule,
    MatButtonModule,
    MatTooltipModule,
    MatIconModule,
    _MatMenuDirectivesModule,
    MatTableModule,
    MatPaginatorModule,
    MatMenuModule,
    FormsModule,
    MatCheckboxModule,
    ContainerModule,
    MatSortModule,
    MatDialogModule,
    MatFormFieldModule,
    MatSelectModule,
    LoadingModule,
    MatInputModule
  ]
})
export class EmpleoAnteriorModule { }
