import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatTableDataSource} from '@angular/material/table';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {DateUtil} from 'src/app/core/utils/date-utils';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import { AccidenteTrabajoService } from 'src/app/modules/salud-ocupacional/antecedente-laboral/services/accidente-trabajo.service';
import { AccidenteTrabajo } from 'src/app/modules/salud-ocupacional/antecedente-laboral/models/accidente-trabajo.model';
import { AccidenteTrabajoModalComponent } from './accidente-trabajo-modal/accidente-trabajo-modal.component';
import { DeleteModalComponent } from 'src/app/shared/components/delete-modal/delete-modal.component';


@Component({
  selector: 'vex-accidente-trabajo',
  templateUrl: './accidente-trabajo.component.html',
  styleUrls: ['./accidente-trabajo.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms,
    scaleFadeIn400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class AccidenteTrabajoComponent implements OnInit, AfterViewInit {

  accidentesTrabajo: AccidenteTrabajo[];

  @Input()
  columns: TableColumn<AccidenteTrabajo>[] = [
    {
      label: 'Calificado por el IESS',
      property: 'recibioAtencion',
      type: 'text',
      visible: true
    },
    {
      label: 'Lugar y dependencia del seguro social',
      property: 'lugarAtencion',
      type: 'text',
      visible: true
    },
    {
      label: 'Fecha',
      property: 'fecha',
      type: 'text',
      visible: true
    },
    {
      label: 'Riesgos laborales implicados',
      property: 'riesgoImplicado',
      type: 'text',
      visible: true
    },
    {
      label: 'Naturaleza de la lesión',
      property: 'naturaleza',
      type: 'text',
      visible: true
    },
    {
      label: 'Parte del cuerpo afectada',
      property: 'parteCuerpoAfectada',
      type: 'text',
      visible: true
    },
    {
      label: 'Empresa donde se sucitó el evento',
      property: 'empresa',
      type: 'text',
      visible: true
    },
    {
      label: 'Horas de reposo',
      property: 'horasReposo',
      type: 'text',
      visible: true
    },
    {
      label: 'Secuelas',
      property: 'secuelas',
      type: 'text',
      visible: true
    },
    {
      label: 'Observación',
      property: 'observaciones',
      type: 'text',
      visible: true
    },
    {label: 'Acciones', property: 'menu', type: 'button', visible: true}
  ];

  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<AccidenteTrabajo> | null;

  icEdit = icEdit;
  icSearch = icSearch;
  icMoreHoriz = icMoreHoriz;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;

  pacienteActual: Paciente;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    protected accidenteTrabajoService: AccidenteTrabajoService,
    private pacienteGlobalService: PacienteGlobalService
  ) { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente){
          this.pacienteActual = paciente;
          this.getAccidentes();
        }
      });
  }

  getAccidentes(): void{
    this.accidenteTrabajoService
      .getAccidenteTrabajo(this.pacienteActual.id)
      .subscribe(
        (data: AccidenteTrabajo[]) => {
          if (data) {
            this.accidentesTrabajo = data;
            this.dataSource.data = this.accidentesTrabajo;
          }
        }
      );
  }

  createAccidenteTrabajo(){
    this.dialog.open(AccidenteTrabajoModalComponent, {
        width: '800px',
        maxWidth: '100%'
      }).afterClosed()
      .subscribe((antecedente: AccidenteTrabajo) => {
        if (antecedente) {
          this.showNotification('Registro creado EXITOSAMENTE', 'OK');
          this.getAccidentes();
        }
      });
  }

  updateAccidenteTrabajo(antecedente: AccidenteTrabajo) {
    this.dialog.open(AccidenteTrabajoModalComponent, {
        data: antecedente,
        width: '800px',
        maxWidth: '100%'
      }).afterClosed()
      .subscribe(updatedAntecedente => {
        if (updatedAntecedente) {
          this.showNotification('Registro actualizado EXITOSAMENTE', 'OK');
          this.getAccidentes();
        }
      });
  }

  openDeleteDialog(antecedente: AccidenteTrabajo) {
    this.dialog.open(DeleteModalComponent, {
      data: '¿Estas seguro de eliminar este registro?',
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') { this.deleteAccidenteTrabajo(antecedente); }
    });
  }

  deleteAccidenteTrabajo(antecedente: AccidenteTrabajo) {
    console.log(antecedente);
    this.accidenteTrabajoService.deleteAccidenteTrabajo(antecedente.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification('Registro eliminado EXITOSAMENTE', 'CERRAR');
          this.accidentesTrabajo.splice(
            this.accidentesTrabajo.findIndex((existing) =>
              existing.id === antecedente.id), 1
          );
          this.dataSource.connect().next(this.accidentesTrabajo);
        } else {
          this.showNotification('No se pudo eliminar el registro', 'CERRAR');
        }
      });
  }

  get visibleColumns() {
    return this.columns
      .filter(column => column.visible)
      .map(column => column.property);
  }

  getFormattedDate(date: string){
    return DateUtil.showDateFormat(date);
  }

  getFueAtentidoIESS(recibioAtencion: boolean): string {
    if (recibioAtencion === true) {
      return "SÍ"
    } else {
      return "NO"
    }
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(column: TableColumn<T>) {
    return column.property;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

}
