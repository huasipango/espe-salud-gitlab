import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";

import icMoreVert from "@iconify/icons-ic/twotone-more-vert";
import icClose from "@iconify/icons-ic/twotone-close";
import icPrint from "@iconify/icons-ic/twotone-print";
import icDownload from "@iconify/icons-ic/twotone-cloud-download";
import icDelete from "@iconify/icons-ic/twotone-delete";
import icPhone from "@iconify/icons-ic/twotone-phone";
import icPerson from "@iconify/icons-ic/twotone-person";
import icMyLocation from "@iconify/icons-ic/twotone-my-location";
import icLocationCity from "@iconify/icons-ic/twotone-location-city";
import icEditLocation from "@iconify/icons-ic/twotone-edit-location";
import icNotesMedical from '@iconify/icons-fa-solid/notes-medical';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';
import icCheckCircle from '@iconify/icons-fa-solid/check-circle';
import twotoneReportProblem from '@iconify/icons-ic/twotone-report-problem';
import roundAccessibility from '@iconify/icons-ic/round-accessibility';
import baselineLocalHospital from '@iconify/icons-ic/baseline-local-hospital';
import icClock from '@iconify/icons-fa-solid/clock';
import outlineWork from '@iconify/icons-ic/outline-work';
import { UntilDestroy } from '@ngneat/until-destroy';
import { Paciente } from 'src/app/core/models/paciente/paciente.model';
import { AccidenteTrabajo } from '../../../models/accidente-trabajo.model';
import { PacienteGlobalService } from 'src/app/core/services/paciente-global.service';
import { DatePipe } from '@angular/common';
import { LoadingService } from 'src/app/core/services/loading.service';
import { AccidenteTrabajoService } from '../../../services/accidente-trabajo.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import { AntecedenteLaboralService } from '../../../services/antecedente-laboral.service';
import { AntecedenteLaboral } from '../../../models/antecedente-laboral.model';

@UntilDestroy()
@Component({
  selector: 'vex-accidente-trabajo-modal',
  templateUrl: './accidente-trabajo-modal.component.html',
  styleUrls: ['./accidente-trabajo-modal.component.scss']
})
export class AccidenteTrabajoModalComponent implements OnInit {

  static id = 100;
  maxDate=new Date();
  date = new Date();

  form: FormGroup;
  mode: "create" | "update" = "create";

  icMoreVert = icMoreVert;
  icClose = icClose;

  icPrint = icPrint;
  icDownload = icDownload;
  icDelete = icDelete;
  icNotesMedical = icNotesMedical;
  icClock = icClock;
  icPerson = icPerson;
  icMyLocation = icMyLocation;
  icLocationCity = icLocationCity;
  icEditLocation = icEditLocation;
  icPhone = icPhone;
  icBaseLineHospital = baselineLocalHospital;
  icBody = roundAccessibility;
  icBuilding = outlineWork;
  icDanger = twotoneReportProblem;
  icCheck = icCheckCircle;
  icArrowDropDown = icArrowDropDown;
  naturalezasLesion: any = [
    'Contusión',
    'Excoriación',
    'Deformidad o Masa',
    'Hematoma',
    'Esguince / Distensión',
    'Herida Penetrante',
    'Herida No Penetrante',
    'Fractura Expuesta',
    'Fractura Cerrada',
    'Amputación',
    'Hemorragia',
    'Mordedura',
    'Picadura',
    'Quemadura G-I',
    'Quemadura G-II',
    'Quemadura G-III',
    'Intoxiación',
    'Psíquico',
    'Sexual',
    'Otra'
  ];
  pacienteActual: Paciente;
  antecedenteLaboralActual: AntecedenteLaboral;
  messages = USER_MESSAGES;

  listaNaturaleza = new FormControl(
    (this.defaults ? (this.defaults.naturaleza ? this.defaults.naturaleza : null) : null),
    [Validators.required]
  );
  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: any,
    private dialogRef: MatDialogRef<AccidenteTrabajoModalComponent>,
    private fb: FormBuilder,
    private loadingService: LoadingService,
    private pacienteGlobalService: PacienteGlobalService,
    private antecedenteLaboral: AntecedenteLaboralService,
    private accidenteTrabajoService: AccidenteTrabajoService,
    public datapipe: DatePipe,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit() {
    this.pacienteGlobalService.pacienteGlobal$
    .subscribe((paciente: Paciente)=>{
      this.pacienteActual = paciente;
    });
    this.antecedenteLaboral.getAntecedenteLaboral(this.pacienteActual.id)
      .subscribe((antecedenteLaboral: AntecedenteLaboral)=>{
        this.antecedenteLaboralActual = antecedenteLaboral;
    });
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.date.setDate(this.date.getDate());
      this.defaults = {} as AccidenteTrabajo;
    }

    this.form = this.fb.group({
      id: this.defaults.id || null,
      fecha: this.defaults.fecha || this.maxDate,
      lugarAtencion: this.defaults.lugarAtencion || '',
      recibioAtencion: this.defaults.recibioAtencion,
      empresa: this.defaults.empresa || '',
      riesgoImplicado: this.defaults.riesgoImplicado || '',
      naturaleza: this.defaults.naturaleza || '',
      parteCuerpoAfectada: this.defaults.parteCuerpoAfectada || '',
      horasReposo: this.defaults.horasReposo || 0,
      secuelas: this.defaults.secuelas || '',
      observaciones: this.defaults.observaciones || '',
      idAntecedenteLaboral: this.defaults.idAntecedenteLaboral || ''
    });
  }

  save() {
    const accidente = this.form.value;
    if (this.mode === 'create') {
      this.createAccidenteTrabajo(accidente);
    } else if (this.mode === 'update') {
      this.updateAccidenteTrabajo(accidente);
    }
  }

  createAccidenteTrabajo(accidente: AccidenteTrabajo) {
    accidente.idAntecedenteLaboral = this.antecedenteLaboralActual.id;
    this.loadingService.showLoaderUntilCompleted(
      this.accidenteTrabajoService.createAccidenteTrabajo(accidente)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.createdFailedMessage, 'CERRAR');
    });
  }

  updateAccidenteTrabajo(accidente: AccidenteTrabajo) {
    this.loadingService.showLoaderUntilCompleted(
      this.accidenteTrabajoService.updateAccidenteTrabajo(accidente.id, accidente)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.createdFailedMessage, 'CERRAR');
    });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }
}