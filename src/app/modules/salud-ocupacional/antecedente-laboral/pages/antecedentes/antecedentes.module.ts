import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AntecedentesRoutingModule } from './antecedentes-routing.module';
import { AntecedentesComponent } from './antecedentes.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ContainerModule } from 'src/@vex/directives/container/container.module';
import { AccidenteTrabajoModule } from '../../components/accidente-trabajo/accidente-trabajo.module';
import {ActividadExtralaboralModule} from 'src/app/modules/salud-ocupacional/antecedente-laboral/components/actividad-extralaboral/actividad-extralaboral.module';
import {EnfermedadProfesionalModule} from 'src/app/modules/salud-ocupacional/antecedente-laboral/components/enfermedad-profesional/enfermedad-profesional.module';
import {EmpleoAnteriorModule} from '../../components/empleo-anterior/empleo-anterior.module';
import {IncidenteTrabajoModule} from 'src/app/modules/salud-ocupacional/antecedente-laboral/components/incidente-trabajo/incidente-trabajo.module';


@NgModule({
  declarations: [AntecedentesComponent],
    imports: [
        CommonModule,
        AntecedentesRoutingModule,
        FlexLayoutModule,
        ContainerModule,
        AccidenteTrabajoModule,
        ActividadExtralaboralModule,
        EnfermedadProfesionalModule,
        EmpleoAnteriorModule,
        IncidenteTrabajoModule
    ]
})
export class AntecedentesModule { }
