import { Component, OnInit } from '@angular/core';
import { fadeInRight400ms } from 'src/@vex/animations/fade-in-right.animation';
import { fadeInUp400ms } from 'src/@vex/animations/fade-in-up.animation';
import { scaleIn400ms } from 'src/@vex/animations/scale-in.animation';
import { stagger40ms } from 'src/@vex/animations/stagger.animation';
import { Paciente } from 'src/app/core/models/paciente/paciente.model';
import { AntecedenteLaboral } from '../../models/antecedente-laboral.model';
import { AntecedenteLaboralSharedService } from '../../services/antecedente-laboral-shared.service';

@Component({
  selector: 'vex-antecedentes',
  templateUrl: './antecedentes.component.html',
  styleUrls: ['./antecedentes.component.scss'],
  animations: [
    fadeInUp400ms,
    fadeInRight400ms,
    scaleIn400ms,
    stagger40ms
  ]
})
export class AntecedentesComponent implements OnInit {

  antecedente: AntecedenteLaboral;
  pacienteActual: Paciente;

  constructor(
    private antecedenteLaboralSharedService: AntecedenteLaboralSharedService
  ) { }

  ngOnInit(): void {
    this.antecedenteLaboralSharedService.antecedenteLaboral$
      .subscribe((antecedente)=> {
        this.antecedente = antecedente;
      });
  }

}
