import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActividadesRoutingModule } from './actividades-routing.module';
import { ActividadesComponent } from './actividades.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ContainerModule } from 'src/@vex/directives/container/container.module';


@NgModule({
  declarations: [ActividadesComponent],
  imports: [
    CommonModule,
    ActividadesRoutingModule,
    FlexLayoutModule,
    ContainerModule,
  ]
})
export class ActividadesModule { }
