import { Component, OnInit } from '@angular/core';
import { fadeInRight400ms } from 'src/@vex/animations/fade-in-right.animation';
import { fadeInUp400ms } from 'src/@vex/animations/fade-in-up.animation';
import { scaleIn400ms } from 'src/@vex/animations/scale-in.animation';
import { stagger40ms } from 'src/@vex/animations/stagger.animation';
import { AntecedenteLaboral } from '../../models/antecedente-laboral.model';
import { AntecedenteLaboralSharedService } from '../../services/antecedente-laboral-shared.service';

@Component({
  selector: 'vex-actividades',
  templateUrl: './actividades.component.html',
  styleUrls: ['./actividades.component.scss'],
  animations: [
    fadeInUp400ms,
    fadeInRight400ms,
    scaleIn400ms,
    stagger40ms
  ]
})
export class ActividadesComponent implements OnInit {

  antecedente: AntecedenteLaboral;
  
  constructor(
    private antecedenteLaboralSharedService: AntecedenteLaboralSharedService
  ) { }

  ngOnInit(): void {
    this.antecedenteLaboralSharedService.antecedenteLaboral$
      .subscribe((antecedente)=> {
        this.antecedente = antecedente;
      });
  }

}
