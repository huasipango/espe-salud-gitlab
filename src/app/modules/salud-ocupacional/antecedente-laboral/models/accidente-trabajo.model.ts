export class AccidenteTrabajo {
    id: number;
    empresa: string;
    fecha: string;
    lugarAtencion: string;
    observaciones: string;
    recibioAtencion: boolean;
    riesgoImplicado: string;
    secuelas: string;
    naturaleza: string;
    horasReposo: number;
    parteCuerpoAfectada: string;
    idAntecedenteLaboral: number;
}