export class IncidenteTrabajo{
    id: number;
    empresa: string;
    fecha: string;
    lugarAtencion: string;
    observaciones: string;
    recibioAtencion: true;
    riesgoImplicado: string;
    secuelas: string;
    horasReposo: number;
    naturaleza: string;
    parteCuerpoAfectada: string;
    idAntecedenteLaboral: number;
}