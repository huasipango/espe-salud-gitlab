export class EmpleoAnterior {
  id: number;
  idAntecedenteLaboral: number;
  empresa: string;
  puestoTrabajo: string;
  actividad: string;
  meses: number;
  usabanSeguridad: boolean;
  vigilanciaSalud: boolean;
  observacion: string;
  riesgo: string;

}
