import {TipoEnfermedadCIE10} from 'src/app/core/models/catalogo/tipo-enfermedad-cie10.model';

export class EnfermedadProfesional{
  id: number;
  empresa: string;
  fecha: string;
  lugarAtencion: string;
  observaciones: string;
  recibioAtencion: boolean;
  riesgoImplicado: string;
  secuelas: string;
  horasReposo: number;
  idDiagnostico: string;
  diagnostico: TipoEnfermedadCIE10;
  idAntecedenteLaboral: number;
}
