import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Link} from 'src/@vex/interfaces/link.interface';
import {AntecedenteLaboral} from './models/antecedente-laboral.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {LoadingService} from 'src/app/core/services/loading.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {UntilDestroy} from '@ngneat/until-destroy';
import { AntecedenteLaboralService } from './services/antecedente-laboral.service';
import { AntecedenteLaboralSharedService } from './services/antecedente-laboral-shared.service';

@UntilDestroy()
@Component({
  selector: 'vex-antecedente-laboral',
  templateUrl: './antecedente-laboral.component.html',
  styleUrls: ['./antecedente-laboral.component.scss']
})
export class AntecedenteLaboralComponent implements OnInit {
  layoutCtrl = new FormControl('full');
  links: Link[] = [
    {
      label: 'ANTECEDENTES',
      route: './antecedentes',
      routerLinkActiveOptions: {exact: true}
    },
    {
      label: 'ACTIVIDADES',
      route: './actividades'
    }
  ];

  pacienteActual: Paciente;
  antecedenteLaboral: AntecedenteLaboral;

  constructor(
    private pacienteGlobalService: PacienteGlobalService,
    private antecedenteService: AntecedenteLaboralService,
    private antecedenteSharedService: AntecedenteLaboralSharedService,
    public loadingService: LoadingService,
    private snackBar: MatSnackBar
  ) {
  }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getAntecedenteLaboral();
        }
      });
  }
  getAntecedenteLaboral(): void {
    this.antecedenteSharedService.antecedenteLaboral$
      .subscribe((antecedente: AntecedenteLaboral) => {
        this.antecedenteLaboral = antecedente;
      });
      this.loadingService.loadingOn();
      this.antecedenteService.getAntecedenteLaboral(this.pacienteActual.id)
        .subscribe((antecedente) => {
          if (antecedente) {
            this.loadingService.loadingOff();
            this.antecedenteSharedService.setAntecedenteLaboral(antecedente);
          }
        }, error => {
          this.loadingService.loadingOff();
          this.snackBar.open('No se ha registrado un antecedente laboral', 'CERRAR', {
            duration: 5000
          });
          this.antecedenteSharedService.setAntecedenteLaboral(null);
        });
  }

}
