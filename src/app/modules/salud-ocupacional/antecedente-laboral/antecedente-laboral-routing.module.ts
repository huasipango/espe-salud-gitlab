import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VexRoutes } from 'src/@vex/interfaces/vex-route.interface';
import { RoleGuard } from 'src/app/core/auth/role.guard';
import { RolEnum } from 'src/app/core/enums/rol-enum';
import { AntecedenteLaboralComponent } from './antecedente-laboral.component';

const routes: VexRoutes = [
  {
    path: '',
    component: AntecedenteLaboralComponent,
    canActivate: [RoleGuard],
    data: {
      toolbarShadowEnabled: true,
      containerEnabled: true,
      roles: [RolEnum.ROLE_OCUPACIONAL]
    },
    children: [
      {
        path: '',
      },
      {
        path: 'antecedentes',
        loadChildren: () => import('./pages/antecedentes/antecedentes.module').then(m => m.AntecedentesModule)
      },
      {
        path: 'actividades',
        loadChildren: () => import('./pages/actividades/actividades.module').then(m => m.ActividadesModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AntecedenteLaboralRoutingModule { }
