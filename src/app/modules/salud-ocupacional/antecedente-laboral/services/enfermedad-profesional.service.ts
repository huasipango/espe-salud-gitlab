import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {EnfermedadProfesional} from 'src/app/modules/salud-ocupacional/antecedente-laboral/models/enfermedad-profesional.model';

@Injectable({
  providedIn: 'root'
})
export class EnfermedadProfesionalService {
  private baseUrl: string = environment.baseUrl;
  private commonName = 'antecedentes-enfermedades-trabajo';

  constructor(private http: HttpClient) {}

  getEnfermedadProfesional(idPaciente: number): Observable<EnfermedadProfesional[]> {
    return this.http.get<EnfermedadProfesional[]>(this.baseUrl + this.commonName + '?idPaciente=' + idPaciente);
  }

  createEnfermedadProfesional(enfermedadProfesional): Observable<EnfermedadProfesional> {
    return this.http.post<EnfermedadProfesional>(this.baseUrl + this.commonName, JSON.stringify(enfermedadProfesional));
  }

  updateEnfermedadProfesional(id, data: EnfermedadProfesional): Observable<EnfermedadProfesional> {
    return this.http.put<EnfermedadProfesional>(
      this.baseUrl + this.commonName + '/' + id, JSON.stringify(data));
  }

  deleteEnfermedadProfesional(id: number): Observable<EnfermedadProfesional> {
    return this.http.delete<EnfermedadProfesional>(this.baseUrl + this.commonName + '/' + id);
  }
}
