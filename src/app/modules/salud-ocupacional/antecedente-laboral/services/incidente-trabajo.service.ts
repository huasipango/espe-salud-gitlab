import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { environment } from 'src/environments/environment';
import { IncidenteTrabajo } from '../models/incidente-trabajo.model';

@Injectable({
  providedIn: 'root'
})
export class IncidenteTrabajoService {
    private baseUrl: string = environment.baseUrl;
    private commonName = 'antecedentes-incidentes-trabajo';

    constructor(private http: HttpClient) {}

    getIncidentesTrabajo(idPaciente: number): Observable<IncidenteTrabajo[]> {
        return this.http.get<IncidenteTrabajo[]>(this.baseUrl + this.commonName + '?idPaciente=' + idPaciente);
    }

    createIncidenteTrabajo(incidenteTrabajo): Observable<IncidenteTrabajo> {
        return this.http.post<IncidenteTrabajo>(this.baseUrl + this.commonName, JSON.stringify(incidenteTrabajo));
    }

    updateIncidenteTrabajo(id, data: IncidenteTrabajo): Observable<IncidenteTrabajo> {
        return this.http.put<IncidenteTrabajo>(
        this.baseUrl + this.commonName + '/' + id, JSON.stringify(data));
    }

    deleteIncidenteTrabajo(id: number): Observable<IncidenteTrabajo> {
        return this.http.delete<IncidenteTrabajo>(this.baseUrl + this.commonName + '/' + id);
    }
}
