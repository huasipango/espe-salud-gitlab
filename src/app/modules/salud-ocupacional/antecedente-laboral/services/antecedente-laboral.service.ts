import {Injectable} from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AntecedenteLaboral} from '../models/antecedente-laboral.model';

@Injectable({
    providedIn: 'root'
    })
    export class AntecedenteLaboralService {
    private baseUrl: string = environment.baseUrl;
    private commonName = 'antecedentes-laborales';

    constructor(private http: HttpClient) {}

    getAntecedenteLaboral(idPaciente: number): Observable<AntecedenteLaboral> {
        return this.http.get<AntecedenteLaboral>(this.baseUrl + this.commonName + '?idPaciente=' + idPaciente);
    }

    createAntecedenteLaboral(antecedenteLaboral): Observable<AntecedenteLaboral> {
        return this.http.post<AntecedenteLaboral>(this.baseUrl + this.commonName, JSON.stringify(antecedenteLaboral));
    }

    updateAntecedenteLaboral(id, data: AntecedenteLaboral): Observable<AntecedenteLaboral> {
        return this.http.put<AntecedenteLaboral>(
        this.baseUrl + this.commonName + '/' + id, JSON.stringify(data));
    }
}
