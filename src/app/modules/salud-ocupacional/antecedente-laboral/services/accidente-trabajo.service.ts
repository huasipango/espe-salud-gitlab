import {Injectable} from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AccidenteTrabajo} from '../models/accidente-trabajo.model';

@Injectable({
    providedIn: 'root'
    })
export class AccidenteTrabajoService {
    private baseUrl: string = environment.baseUrl;
    private commonName = 'antecedentes-accidentes-trabajo';

    constructor(private http: HttpClient) {}

    getAccidenteTrabajo(idAntecedente: number): Observable<AccidenteTrabajo[]> {
        return this.http.get<AccidenteTrabajo[]>(this.baseUrl + this.commonName + '?idAntecedente=' + idAntecedente);
    }

    createAccidenteTrabajo(antecedenteLaboral): Observable<AccidenteTrabajo> {
        return this.http.post<AccidenteTrabajo>(this.baseUrl + this.commonName, JSON.stringify(antecedenteLaboral));
    }

    updateAccidenteTrabajo(id, data: AccidenteTrabajo): Observable<AccidenteTrabajo> {
        return this.http.put<AccidenteTrabajo>(
        this.baseUrl + this.commonName + '/' + id, JSON.stringify(data));
    }

    deleteAccidenteTrabajo(id: number): Observable<AccidenteTrabajo> {
        return this.http.delete<AccidenteTrabajo>(this.baseUrl + this.commonName + '/' + id);
    }
}
