import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {AntecedenteLaboral} from '../models/antecedente-laboral.model';
import {Paciente} from '../../../../core/models/paciente/paciente.model';

@Injectable({
    providedIn: 'root'
})
export class AntecedenteLaboralSharedService {
    constructor() { }

    private subject: BehaviorSubject<AntecedenteLaboral> =
        new BehaviorSubject<AntecedenteLaboral>(null);
    public antecedenteLaboral$: Observable<AntecedenteLaboral> = this.subject.asObservable();

    public setAntecedenteLaboral(antecedente: AntecedenteLaboral): void{
        this.subject.next(antecedente);
    }

}
