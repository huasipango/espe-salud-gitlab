import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ActividadExtralaboral} from 'src/app/modules/salud-ocupacional/antecedente-laboral/models/actividad-extralaboral.model';
import {ActividadEnfermeria} from 'src/app/modules/enfermeria/actividad-enfermeria/models/actividad.model';

@Injectable({
  providedIn: 'root'
})
export class ActvidadExtralaboralService {
  private baseUrl: string = environment.baseUrl;
  private commonName = 'actividades-extralaborales';

  constructor(private http: HttpClient) {}

  getActividadExtra(idPaciente: number): Observable<ActividadExtralaboral[]> {
    return this.http.get<ActividadExtralaboral[]>(this.baseUrl + this.commonName + '?idPaciente=' + idPaciente);
  }

  createActividadExtra(antecedenteLaboral): Observable<ActividadExtralaboral> {
    return this.http.post<ActividadExtralaboral>(this.baseUrl + this.commonName + '/', JSON.stringify(antecedenteLaboral));
  }

  updateAntecedenteLaboral(id, data: ActividadExtralaboral): Observable<ActividadExtralaboral> {
    return this.http.put<ActividadExtralaboral>(
      this.baseUrl + this.commonName + '/' + id, JSON.stringify(data));
  }

  // DELETE
  deleteActividadExtra(id: number): Observable<ActividadExtralaboral>  {
    const url = this.baseUrl + this.commonName + '/' + id;
    return this.http.delete<ActividadExtralaboral>(url);
  }
}
