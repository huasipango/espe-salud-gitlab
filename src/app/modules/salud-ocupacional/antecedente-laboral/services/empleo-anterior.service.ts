import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {EmpleoAnterior} from 'src/app/modules/salud-ocupacional/antecedente-laboral/models/empleo-anterior.model';

@Injectable({
  providedIn: 'root'
})
export class EmpleoAnteriorService {

  private baseUrl: string = environment.baseUrl;
  private commonName = 'antecedentes-empleos-anteriores/';

  constructor(private http: HttpClient) {}

  getEmpleoAnterior(idAntecedenteLaboral: number): Observable<EmpleoAnterior[]> {
    return this.http.get<EmpleoAnterior[]>(this.baseUrl + this.commonName + '?idAntecedente=' + idAntecedenteLaboral);
  }

  createEmpleoAnterior(antecedenteLaboral): Observable<EmpleoAnterior> {
    return this.http.post<EmpleoAnterior>(this.baseUrl + this.commonName, JSON.stringify(antecedenteLaboral));
  }

  updateEmpleoAnterior(id, data: EmpleoAnterior): Observable<EmpleoAnterior> {
    return this.http.put<EmpleoAnterior>(
      this.baseUrl + this.commonName + '/' + id, JSON.stringify(data));
  }

  // DELETE
  deleteEmpleoAnterior(id: number): Observable<EmpleoAnterior>  {
    const url = this.baseUrl + this.commonName + id;
    return this.http.delete<EmpleoAnterior>(url);
  }
}
