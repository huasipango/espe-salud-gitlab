import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {FormControl} from '@angular/forms';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {MatDialog} from '@angular/material/dialog';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icAdd from '@iconify/icons-ic/twotone-add';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icCheck from '@iconify/icons-fa-solid/clipboard-check';
import icDownload from '@iconify/icons-ic/twotone-cloud-download';
import icPrint from '@iconify/icons-ic/twotone-print';
import icSearch from '@iconify/icons-ic/twotone-search';
import {GestacionLactancia} from 'src/app/modules/enfermeria/gestacion-lactancia/models/gestacion-lactancia.model';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {GestacionLactanciaService} from 'src/app/modules/enfermeria/gestacion-lactancia/services/gestacion-lactancia.service';
import {DeleteModalComponent} from 'src/app/shared/components/delete-modal/delete-modal.component';
import {DateUtil, getLongDate, getShortDate, getShortDateTime} from 'src/app/core/utils/date-utils';
import {GestacionLactanciaModalComponent} from 'src/app/modules/enfermeria/gestacion-lactancia/gestacion-lactancia-modal/gestacion-lactancia-modal.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import icBaby from '@iconify/icons-fa-solid/baby';
import {Router} from '@angular/router';


@UntilDestroy()
@Component({
  selector: 'vex-gestacion-lactancia',
  templateUrl: './gestacion-lactancia.component.html',
  styleUrls: ['./gestacion-lactancia.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms,
    scaleIn400ms,
    fadeInRight400ms,
    scaleFadeIn400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class GestacionLactanciaComponent implements OnInit, AfterViewInit {

  gestaciones: GestacionLactancia[] = [];
  searchCtrl = new FormControl();
  dataSource: MatTableDataSource<GestacionLactancia> | null;
  icDelete = icDelete;
  icFilterList = icFilterList;
  icAdd = icAdd;
  icEdit = icEdit;
  icMoreHoriz = icMoreHoriz;
  icCheck = icCheck;
  icSearch = icSearch;
  icPrint = icPrint;
  icBaby = icBaby;
  pacienteActual: Paciente;
  icDownload = icDownload;

  @Input()
  columns: TableColumn<GestacionLactancia>[] = [
    {
      label: 'Tipo',
      property: 'tipoControlEmbarazo',
      type: 'text',
      visible: true
    },
    {
      label: 'Fecha control',
      property: 'fechaControl',
      type: 'datetime',
      visible: true
    },
    {
      label: 'Fecha últ. menstruación',
      property: 'fechaUltimaMenstruacion',
      type: 'date',
      visible: true
    },
    {
      label: 'S. Embarazo',
      property: 'semanasEmbarazo',
      type: 'text',
      visible: true
    },
    {
      label: 'Días falt. embarazo',
      property: 'diasFaltantesEmbarazo',
      type: 'text',
      visible: true
    },
    {
      label: 'F. probable de parto',
      property: 'fechaProbableParto',
      type: 'date',
      visible: true
    },
    {
      label: 'F. inicio de lactancia',
      property: 'fechaInicioLactancia',
      type: 'date',
      visible: true
    },
    {
      label: 'F. fin de latancia',
      property: 'fechaFinLactancia',
      type: 'date',
      visible: true
    },
    {
      label: 'observación',
      property: 'observacion',
      type: 'text',
      visible: true,
      cssClasses: ['text-wrap']
    },
    { label: 'Actions', property: 'actions', type: 'button', visible: true }
  ];

  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private snackbar: MatSnackBar,
    private pacienteGlobalService: PacienteGlobalService,
    private gestacionService: GestacionLactanciaService
  ) {
  }
  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente){
          this.pacienteActual = paciente;
          this.getGestaciones();
        }
      });
    this.searchCtrl.valueChanges.pipe(
      untilDestroyed(this)
    ).subscribe(value => this.onFilterChange(value));
  }

  getGestaciones(): void {
    this.gestacionService.getGestacionLactancia(this.pacienteActual.id)
      .subscribe((data: GestacionLactancia[]) => {
        this.gestaciones = data;
        this.dataSource.data = this.gestaciones;
      });
  }

  createGestacion() {
    this.dialog.open(GestacionLactanciaModalComponent, {
      width: '700px',
      maxWidth: '100%',
      disableClose: true
    }).afterClosed()
      .subscribe((gestacion: GestacionLactancia) => {
        if (gestacion) {
          this.showNotification(' Registro creado EXITOSAMENTE', 'OK');
          this.gestaciones.push(gestacion);
          this.dataSource.connect().next(this.gestaciones);
        }
      });
  }

  updateGestacion(gestacionLactancia: GestacionLactancia): void {
    this.dialog.open(GestacionLactanciaModalComponent, {
      data: gestacionLactancia,
      maxWidth: '100%',
      width: '600px',
      disableClose: true
    }).afterClosed()
      .subscribe((updatedGestacion) => {
        if (updatedGestacion) {
          this.showNotification('Registro actualizado EXITOSAMENTE', 'OK');
          const id = updatedGestacion.id;
          const index = this.gestaciones.findIndex((existing) => existing.id === id);
          this.gestaciones[index] = updatedGestacion;
          this.dataSource.connect().next(this.gestaciones);
        }
      });

  }

  deleteGestacion(gestacionLactancia: GestacionLactancia): void {
    this.gestacionService.deleteGestacionLactancia(gestacionLactancia.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification('Registro eliminado EXITOSAMENTE', 'CERRAR');
          this.gestaciones.splice(
            this.gestaciones.findIndex((existing) =>
              existing.id === gestacionLactancia.id), 1
          );
          this.dataSource.connect().next(this.gestaciones);
        } else {
          this.showNotification('No se pudo eliminar el registro', 'CERRAR');
        }
      });
  }


  openDeleteDialog(gestacionLactancia: GestacionLactancia): void {
    this.dialog.open(DeleteModalComponent, {
      data: '¿Estas seguro de eliminar este registro?',
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') { this.deleteGestacion(gestacionLactancia); }
    });
  }

  getFormattedDate(date: Date): string {
    return getShortDate(date);
  }

  onFilterChange(value: string) {
    if (!this.dataSource) {
      return;
    }
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }


  print() {

  }

  openSearchModal() {

  }

  getDate(date: Date): string {
    return getShortDate(date);
  }

  getFormattedDateTime(date: Date) {
    return getShortDateTime(date);
  }
}
