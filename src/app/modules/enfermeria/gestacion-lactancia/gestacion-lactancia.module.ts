import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GestacionLactanciaComponent } from './gestacion-lactancia.component';
import { GestacionLactanciaModalComponent } from './gestacion-lactancia-modal/gestacion-lactancia-modal.component';
import {GestacionLactanciaRoutingModule} from 'src/app/modules/enfermeria/gestacion-lactancia/gestacion-lactancia-routing.module';
import {ExtendedModule, FlexLayoutModule} from '@angular/flex-layout';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {IconModule} from '@visurel/iconify-angular';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatMenuModule} from '@angular/material/menu';
import {PageLayoutModule} from 'src/@vex/components/page-layout/page-layout.module';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import {EmptyPacienteModule} from 'src/app/core/components/empty-paciente/empty-paciente.module';
import {MatRadioModule} from "@angular/material/radio";



@NgModule({
  declarations: [GestacionLactanciaComponent, GestacionLactanciaModalComponent],
  imports: [
    CommonModule,
    GestacionLactanciaRoutingModule,
    FlexLayoutModule,
    ContainerModule,
    IconModule,
    BreadcrumbsModule,
    ExtendedModule,
    MatButtonModule,
    MatTooltipModule,
    MatIconModule,
    ReactiveFormsModule,
    MatMenuModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatCheckboxModule,
    FormsModule,
    MatDialogModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    LoadingModule,
    EmptyPacienteModule,
    MatStepperModule,
    MatRadioModule
  ]
})
export class GestacionLactanciaModule { }
