import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {VexRoutes} from 'src/@vex/interfaces/vex-route.interface';
import {GestacionLactanciaComponent} from 'src/app/modules/enfermeria/gestacion-lactancia/gestacion-lactancia.component';
import {RolEnum} from 'src/app/core/enums/rol.enum';
import {RoleGuard} from 'src/app/core/auth/role.guard';

const routes: VexRoutes = [
  {
    path: '',
    component: GestacionLactanciaComponent,
    canActivate: [RoleGuard],
    data: {
      toolbarShadowEnabled: true,
      containerEnabled: true,
      roles: [RolEnum.ROLE_ENFERMERO, RolEnum.ROLE_MEDICO]
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GestacionLactanciaRoutingModule {
}
