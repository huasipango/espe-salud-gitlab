import {TipoControlEmbarazoEnum} from 'src/app/core/enums/tipo-control-embarazo.enum';

export class GestacionLactancia {
  id: number;
  fechaControl: Date;
  fechaUltimaMenstruacion: Date;
  fechaInicioLactancia: Date;
  semanasEmbarazo: string;
  diasFaltantesEmbarazo: string;
  fechaProbableParto: Date;
  fechaFinLactancia: Date;
  observacion: string;
  idPaciente: number;
  tipoControlEmbarazo: TipoControlEmbarazoEnum;
}
