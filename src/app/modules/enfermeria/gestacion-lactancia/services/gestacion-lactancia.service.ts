import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GestacionLactancia} from 'src/app/modules/enfermeria/gestacion-lactancia/models/gestacion-lactancia.model';

@Injectable({
  providedIn: 'root'
})
export class GestacionLactanciaService {

  private baseUrl: string = environment.baseUrl;
  private readonly commonUrl: string;

  constructor(private http: HttpClient) {
    this.commonUrl = this.baseUrl + 'gestaciones-lactancias/';
  }

  // GET
  getGestacionLactancia(idPaciente: number): Observable<GestacionLactancia[]> {
    return this.http.get<GestacionLactancia[]>(this.commonUrl + 'paciente?idPaciente=' + idPaciente);
  }

  // POST
  createGestacionLactancia(data: GestacionLactancia): Observable<GestacionLactancia>  {
    return this.http.post<GestacionLactancia>(this.commonUrl, JSON.stringify(data));
  }

  // PUT
  updateGestacionLactancia(id: number, data): Observable<GestacionLactancia>  {
    const url = this.commonUrl + id;
    return this.http.put<GestacionLactancia>(url, JSON.stringify(data));
  }

  // DELETE
  deleteGestacionLactancia(id: number): Observable<boolean>  {
    const url = this.commonUrl + id;
    return this.http.delete<boolean>(url);
  }
}
