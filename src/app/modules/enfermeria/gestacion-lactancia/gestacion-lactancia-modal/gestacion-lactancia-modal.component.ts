import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {GestacionLactancia} from 'src/app/modules/enfermeria/gestacion-lactancia/models/gestacion-lactancia.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoadingService} from 'src/app/core/services/loading.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import icClose from '@iconify/icons-ic/twotone-close';
import icPrint from '@iconify/icons-ic/twotone-print';
import icDescription from '@iconify/icons-ic/outline-description';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icDoneAll from '@iconify/icons-ic/twotone-done-all';
import icBabyCarriage from '@iconify/icons-fa-solid/baby-carriage';
import {
  calculateDaysDiff,
  calculateWeeksDiff
} from 'src/app/core/utils/operations.util';
import {GestacionLactanciaService} from 'src/app/modules/enfermeria/gestacion-lactancia/services/gestacion-lactancia.service';
import {enumSelector} from 'src/app/core/utils/enum-to-string-util';
import {TipoControlEmbarazoEnum} from 'src/app/core/enums/tipo-control-embarazo.enum';
import {MatRadioChange} from '@angular/material/radio';

@Component({
  selector: 'vex-gestacion-lactancia-modal',
  templateUrl: './gestacion-lactancia-modal.component.html',
  styleUrls: ['./gestacion-lactancia-modal.component.scss']
})
export class GestacionLactanciaModalComponent implements OnInit {
  form: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: GestacionLactancia,
    private dialogRef: MatDialogRef<GestacionLactanciaModalComponent>,
    private pacienteGlobalService: PacienteGlobalService,
    private fb: FormBuilder,
    private loadingService: LoadingService,
    private gestacionService: GestacionLactanciaService,
    private snackbar: MatSnackBar
  ) { }

  maxDate = new Date();

  mode: 'create' | 'update' = 'create';
  paciente: Paciente;
  icClose = icClose;
  icPrint = icPrint;
  icDescription = icDescription;
  icDelete = icDelete;
  icBabyCarriage = icBabyCarriage;
  icDoneAll = icDoneAll;
  messages = USER_MESSAGES;
  errorMessages = FORM_ERROR_MESSAGES;
  tiposControlEmbarazo = enumSelector(TipoControlEmbarazoEnum);
  descipcionB: boolean;
  gestacion = false;
  lactancia = false;

  ngOnInit(): void {
    if (this.defaults) {
      this.mode = 'update';
      this.toggleDropdown(this.defaults.tipoControlEmbarazo);
    } else {
      this.defaults = {} as GestacionLactancia;
    }
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.paciente = paciente;
          this.initForm();
        }
      });
  }
  initForm(): void {
    this.form = this.fb.group({
      id: this.defaults.id || null,
      tipoControlEmbarazo: [this.defaults.tipoControlEmbarazo || null, Validators.required],
      fechaUltimaMenstruacion: [this.defaults.fechaUltimaMenstruacion || null],
      semanasEmbarazo: [{value: this.defaults.semanasEmbarazo || '', disabled: true}],
      diasFaltantesEmbarazo: [{value: this.defaults.diasFaltantesEmbarazo || '', disabled: true}],
      fechaProbableParto: [this.defaults.fechaProbableParto || null],
      idPaciente: [this.defaults.idPaciente || this.paciente.id, Validators.required],
      fechaInicioLactancia: [this.defaults.fechaInicioLactancia || null],
      fechaFinLactancia: [this.defaults.fechaFinLactancia || null],
      observacion: [this.defaults.observacion || null]
    });
  }

  save() {
    const gestacion = this.form.getRawValue();
    if (this.mode === 'create') {
      this.createGestacion(gestacion);
    } else if (this.mode === 'update') {
      this.updateGestacion(gestacion);
    }
  }

  createGestacion(gestacionLactancia: GestacionLactancia) {
    this.loadingService.showLoaderUntilCompleted(
      this.gestacionService.createGestacionLactancia(gestacionLactancia)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.createdFailedMessage, 'CERRAR');
    });
  }

  updateGestacion(gestacionLactancia: GestacionLactancia) {
    this.loadingService.showLoaderUntilCompleted(
      this.gestacionService.updateGestacionLactancia(gestacionLactancia.id, gestacionLactancia)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      }
    }, () => {
      this.showNotification(this.messages.updatedFailedMessage, 'CERRAR');
    });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  toggleDropdown(tipo: string): void {
    if (TipoControlEmbarazoEnum.GESTACION === tipo) {
      this.gestacion = true;
      this.lactancia = false;
    } else {
      this.lactancia = true;
      this.gestacion = false;
    }
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  calculateSemanas() {
    const fechaUltimaMenstruacion = this.form.get('fechaUltimaMenstruacion').value;
    if (!fechaUltimaMenstruacion) { return; }
    let diff = Math.round(calculateWeeksDiff(fechaUltimaMenstruacion));
    if (diff < 1) { diff = 0; }
    this.form.get('semanasEmbarazo').setValue(diff);
  }

  calculateDiasFaltantes() {
    const fechaProbableParto: Date = this.form.get('fechaProbableParto').value;
    if (!fechaProbableParto) { return; }
    let diff = Math.round(calculateDaysDiff(new Date(), fechaProbableParto));
    if (diff < 1) { diff = 0; }
    this.form.get('diasFaltantesEmbarazo').setValue(diff);
  }
}
