import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {VexRoutes} from 'src/@vex/interfaces/vex-route.interface';
import {NotasEnfermeriaListComponent} from 'src/app/modules/enfermeria/notas-enfermeria/pages/notas-enfermeria-list/notas-enfermeria-list.component';
import {RolEnum} from 'src/app/core/enums/rol.enum';
import {RoleGuard} from 'src/app/core/auth/role.guard';

const routes: VexRoutes = [
  {
    path: '',
    component: NotasEnfermeriaListComponent,
    canActivate: [RoleGuard],
    data: {
      toolbarShadowEnabled: false,
      containerEnabled: true,
      roles: [RolEnum.ROLE_ENFERMERO, RolEnum.ROLE_MEDICO]
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotasEnfermeriaRoutingModule {
}
