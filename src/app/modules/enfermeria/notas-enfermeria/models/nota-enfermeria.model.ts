import {SignoVital} from 'src/app/modules/medicina-general/evolucion/models/signo-vital.model';
import {Antropometria} from 'src/app/modules/medicina-general/evolucion/models/antropometria.model';
import {Evolucion} from 'src/app/modules/medicina-general/evolucion/models/evolucion.model';
import {Dispensario} from 'src/app/core/models/catalogo/dispensario.model';
import {AreaSalud} from 'src/app/core/models/catalogo/area-salud.model';
import {UsuarioSimple} from 'src/app/core/models/usuario/usuario-simple.model';

export class NotaEnfermeria {
  id: number;
  fechaInicio: Date;
  fechaFin: Date;
  estado: 'INICIADO' | 'ATENDIENDO' | 'FINALIZADO';
  idAreaSalud: number;
  areaSalud: AreaSalud;
  idDispensario: number;
  dispensario: Dispensario;
  idPaciente: number;
  idUsuario: number;
  usuario: UsuarioSimple;
  preConsulta: string;
  postConsulta: string;
  hasEvolucion: boolean;
  signoVital: SignoVital;
  antropometria: Antropometria;
  evolucion: Evolucion;
}
