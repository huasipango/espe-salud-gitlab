import {AfterViewInit, Component, OnInit} from '@angular/core';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {scaleIn400ms} from 'src/@vex/animations/scale-in.animation';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {scaleFadeIn400ms} from 'src/@vex/animations/scale-fade-in.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {FormControl} from '@angular/forms';
import {NotaEnfermeria} from 'src/app/modules/enfermeria/notas-enfermeria/models/nota-enfermeria.model';
import icBookMedical from '@iconify/icons-fa-solid/book-medical';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icDownload from '@iconify/icons-ic/cloud-download';
import icPrint from '@iconify/icons-ic/print';
import icSearch from '@iconify/icons-ic/twotone-search';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icAdd from '@iconify/icons-ic/twotone-add';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icCheck from '@iconify/icons-fa-solid/clipboard-check';
import icArrow from '@iconify/icons-fa-solid/arrow-alt-circle-right';
import {NotaEnfermeriaService} from 'src/app/modules/enfermeria/notas-enfermeria/services/nota-enfermeria.service';
import {MatDialog} from '@angular/material/dialog';
import {filter} from 'rxjs/operators';
import {NotaEnfermeriaCreateComponent} from 'src/app/modules/enfermeria/notas-enfermeria/components/nota-enfermeria-create/nota-enfermeria-create.component';
import {getShortDateTime} from 'src/app/core/utils/date-utils';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {UntilDestroy} from '@ngneat/until-destroy';
import {PopoverService} from 'src/@vex/components/popover/popover.service';
import {Router} from '@angular/router';
import {AuthService} from 'src/app/core/auth/auth.service';
import {Observable} from 'rxjs';
import {trackById} from 'src/@vex/utils/track-by';
import {ImagenUsuarioService} from 'src/app/core/services/imagen-usuario.service';
import {PostConsultaModalComponent} from 'src/app/modules/enfermeria/notas-enfermeria/components/post-consulta-modal/post-consulta-modal.component';
import {LoadingService} from 'src/app/core/services/loading.service';
import {SexoEnum} from 'src/app/core/enums/sexo.enum';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';
import {MatSnackBar} from '@angular/material/snack-bar';

@UntilDestroy()
@Component({
  selector: 'vex-notas-enfermeria-list',
  templateUrl: './notas-enfermeria-list.component.html',
  styleUrls: ['./notas-enfermeria-list.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms,
    scaleIn400ms,
    fadeInRight400ms,
    scaleFadeIn400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class NotasEnfermeriaListComponent implements OnInit, AfterViewInit {
  userMessages = USER_MESSAGES;
  notasEnfermeria: Observable<NotaEnfermeria[]>;
  trackById = trackById;

  searchCtrl = new FormControl();

  icBookMedical = icBookMedical;
  icDelete = icDelete;
  icDownload = icDownload;
  icPrint = icPrint;
  icSearch = icSearch;
  icFilterList = icFilterList;
  icAdd = icAdd;
  icEdit = icEdit;
  icMoreHoriz = icMoreHoriz;
  icCheck = icCheck;
  icArrow = icArrow;
  pacienteActual: Paciente;

  constructor(
    private notaEnfermeriaService: NotaEnfermeriaService,
    private dialog: MatDialog,
    private authService: AuthService,
    private popoverService: PopoverService,
    private snackbar: MatSnackBar,
    private router: Router,
    private pacienteGlobalService: PacienteGlobalService,
    private imagenUsuarioService: ImagenUsuarioService,
    private loadingService: LoadingService
  ) {
  }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.getNotasEnfermeria();
        }
      });
  }

  getNotasEnfermeria() {
    this.notasEnfermeria = this.loadingService.showLoaderUntilCompleted(
      this.notaEnfermeriaService.getAllNotasEnfermeria(this.pacienteActual.id)
    );
  }

  deleteNotasEnfermeria(notas: NotaEnfermeria[]): void {
  }

  deleteNota(nota: NotaEnfermeria): void {
  }

  print(): void {
  }

  openSearchModal(): void {

  }

  createNota(): void {
    this.dialog.open(NotaEnfermeriaCreateComponent, {
      width: '900px',
      maxWidth: '100%',
      disableClose: true
    }).beforeClosed().pipe(
      filter<NotaEnfermeria>(Boolean)
    ).subscribe(value => {
      if (value) {
        this.getNotasEnfermeria();
      }
    });
  }

  updateNota(nota: NotaEnfermeria): void {
    this.dialog.open(NotaEnfermeriaCreateComponent, {
      data: nota,
      maxWidth: '100%',
      width: '900px',
      disableClose: true
    }).beforeClosed().pipe(
      filter<NotaEnfermeria>(Boolean)
    ).subscribe(value => {
      if (value) {
        this.getNotasEnfermeria();
      }
    });
  }
  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  ngAfterViewInit() {
  }

  getDatetime(date: Date): string {
    return getShortDateTime(date);
  }

  atenderNotaEvolucion(notaEnfermeria: NotaEnfermeria) {
    this.router.navigate(['/medicina-general/evolucion/create'], {
      state: notaEnfermeria
    });
  }

  getUserImage(idBanner: string): string {
    return this.imagenUsuarioService.getUserImage(idBanner);
  }

  addPostConsulta(notaEnfermeria: NotaEnfermeria) {
    this.dialog.open(PostConsultaModalComponent, {
      data: notaEnfermeria,
      width: '500px',
      maxWidth: '100%',
    }).afterClosed()
      .subscribe(() => {
        this.getNotasEnfermeria();
      });
  }

  isFemenino(): boolean {
    if (!this.pacienteActual) {
      return false;
    }
    return this.pacienteActual.sexo === SexoEnum.MUJER;
  }
}
