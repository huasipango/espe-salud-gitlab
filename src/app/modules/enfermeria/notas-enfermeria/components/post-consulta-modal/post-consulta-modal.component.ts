import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import icClose from '@iconify/icons-ic/twotone-close';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {NotaEnfermeriaService} from 'src/app/modules/enfermeria/notas-enfermeria/services/nota-enfermeria.service';
import {NotaEnfermeria} from 'src/app/modules/enfermeria/notas-enfermeria/models/nota-enfermeria.model';
import {LoadingService} from 'src/app/core/services/loading.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'vex-post-consulta-modal',
  templateUrl: './post-consulta-modal.component.html',
  styleUrls: ['./post-consulta-modal.component.scss']
})
export class PostConsultaModalComponent implements OnInit {
  form: FormGroup;
  icClose = icClose;
  errorMessages = FORM_ERROR_MESSAGES;

  constructor(
    @Inject(MAT_DIALOG_DATA) private notaEnfermeria: NotaEnfermeria,
    private dialogRef: MatDialogRef<PostConsultaModalComponent>,
    private fb: FormBuilder,
    private notaEnfermeriaService: NotaEnfermeriaService,
    private loadingService: LoadingService,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      postConsulta: [this.notaEnfermeria.postConsulta || '', Validators.required]
    });
  }

  save() {
    const data = this.form.value;
    this.loadingService.showLoaderUntilCompleted(
      this.notaEnfermeriaService.patchNotaEnfermeria(data, this.notaEnfermeria.id)
    ).subscribe((response) => {
      if (response) {
        this.showNotification('Se ha agregado la nota de postconsulta', 'CERRAR');
        this.dialogRef.close();
      }
    });
  }
  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }
}
