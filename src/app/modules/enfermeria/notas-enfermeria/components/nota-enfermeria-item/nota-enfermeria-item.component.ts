import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NotaEnfermeria} from 'src/app/modules/enfermeria/notas-enfermeria/models/nota-enfermeria.model';
import {ImagenUsuarioService} from 'src/app/core/services/imagen-usuario.service';
import {getShortDateTime} from 'src/app/core/utils/date-utils';
import {fadeInRight400ms} from 'src/@vex/animations/fade-in-right.animation';
import {SexoEnum} from 'src/app/core/enums/sexo.enum';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';

@Component({
  selector: 'vex-nota-enfermeria-item',
  templateUrl: './nota-enfermeria-item.component.html',
  styleUrls: ['./nota-enfermeria-item.component.scss'],
  animations: [
    fadeInRight400ms
  ]
})
export class NotaEnfermeriaItemComponent implements OnInit {

  @Input() nota: NotaEnfermeria;
  @Input() canClick = true;
  @Output() selectNota = new EventEmitter();
  pacienteActual: Paciente;

  constructor(
    private imagenUserService: ImagenUsuarioService,
    private pacienteGlobalService: PacienteGlobalService
  ) { }

  ngOnInit(): void {
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
        }
      });
  }

  selectNotaEnfermeria() {
    if (!this.canClick) {
      return ;
    }
    this.selectNota.emit(this.nota);
  }

  getUserImage(idBanner: string): string {
    return this.imagenUserService.getUserImage(idBanner);
  }

  getDatetime(date: Date): string {
    return getShortDateTime(date);
  }

  isFemenino(): boolean {
    return this.pacienteActual.sexo === SexoEnum.MUJER;
  }
}
