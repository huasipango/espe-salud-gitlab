import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotaEnfermeriaItemComponent } from './nota-enfermeria-item.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatTooltipModule} from '@angular/material/tooltip';
import {StripHtmlModule} from 'src/@vex/pipes/strip-html/strip-html.module';



@NgModule({
  declarations: [NotaEnfermeriaItemComponent],
  exports: [
    NotaEnfermeriaItemComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatTooltipModule,
    StripHtmlModule
  ]
})
export class NotaEnfermeriaItemModule { }
