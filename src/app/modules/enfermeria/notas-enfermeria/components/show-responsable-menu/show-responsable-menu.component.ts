import { Component, OnInit } from '@angular/core';
import {PopoverRef} from 'src/@vex/components/popover/popover-ref';
import {SaludUserService} from 'src/app/core/services/salud-user.service';
import {Observable} from 'rxjs';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import {LoadingService} from 'src/app/core/services/loading.service';

@Component({
  selector: 'vex-show-responsable-menu',
  templateUrl: './show-responsable-menu.component.html',
  styleUrls: ['./show-responsable-menu.component.scss']
})
export class ShowResponsableMenuComponent implements OnInit {

  usuarioSalud: Observable<SaludUser>;

  constructor(
    private popoverRef: PopoverRef,
    private loadingService: LoadingService,
    private usuarioSaludService: SaludUserService) { }

  ngOnInit(): void {
    if (this.popoverRef.data) {
      this.usuarioSalud = this.loadingService.showLoaderUntilCompleted(
        this.usuarioSaludService.getUser(this.popoverRef.data)
      );
    }
  }

}
