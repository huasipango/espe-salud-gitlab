import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icClose from '@iconify/icons-ic/twotone-close';
import icPrint from '@iconify/icons-ic/twotone-print';
import icDownload from '@iconify/icons-ic/twotone-cloud-download';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icPerson from '@iconify/icons-ic/twotone-person';
import icLocationCity from '@iconify/icons-ic/twotone-location-city';
import icAdd from '@iconify/icons-ic/twotone-add';
import icDescription from '@iconify/icons-ic/twotone-description';
import icInsertComment from '@iconify/icons-ic/twotone-insert-comment';
import icDoneAll from '@iconify/icons-ic/twotone-done-all';
import icClinic from '@iconify/icons-fa-solid/clinic-medical';
import icHospital from '@iconify/icons-fa-solid/hospital';
import {calculateIMC, checkIfAllObjectValuesAreNull} from 'src/app/core/utils/operations.util';
import {SexoEnum} from 'src/app/core/enums/sexo.enum';
import {Paciente} from 'src/app/core/models/paciente/paciente.model';
import {PacienteGlobalService} from 'src/app/core/services/paciente-global.service';
import {Observable} from 'rxjs';
import {AreaSalud} from 'src/app/core/models/catalogo/area-salud.model';
import {SaludUserService} from 'src/app/core/services/salud-user.service';
import {AuthService} from 'src/app/core/auth/auth.service';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {NotaEnfermeriaService} from 'src/app/modules/enfermeria/notas-enfermeria/services/nota-enfermeria.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {LoadingService} from 'src/app/core/services/loading.service';
import {Dispensario} from 'src/app/core/models/catalogo/dispensario.model';
import {FORM_ERROR_MESSAGES, ONLY_NUMBERS_REGEX} from 'src/app/core/constants/constants';
import {NotaEnfermeria} from 'src/app/modules/enfermeria/notas-enfermeria/models/nota-enfermeria.model';
import {SignoVital} from 'src/app/modules/medicina-general/evolucion/models/signo-vital.model';
import {Antropometria} from 'src/app/modules/medicina-general/evolucion/models/antropometria.model';

@Component({
  selector: 'vex-nota-enfermeria-create',
  templateUrl: './nota-enfermeria-create.component.html',
  styleUrls: ['./nota-enfermeria-create.component.scss']
})
export class NotaEnfermeriaCreateComponent implements OnInit {

  antropometriaFormGroup: FormGroup;
  signoVitalFormGroup: FormGroup;
  preConsultaFormGroup: FormGroup;

  icMoreVert = icMoreVert;
  icClose = icClose;

  icPrint = icPrint;
  icDownload = icDownload;
  icDelete = icDelete;

  icPerson = icPerson;
  icLocationCity = icLocationCity;
  icPhone = icPhone;
  icAdd = icAdd;
  icDescription = icDescription;
  icInsertComment = icInsertComment;
  icDoneAll = icDoneAll;
  icClinic = icClinic;
  icHospital = icHospital;

  peso: number;
  talla: number;
  indiceMasaCorporal = 0.0;
  maxDate = new Date();
  pacienteActual: Paciente;
  usuarioSalud: SaludUser;
  mode: 'create' | 'update' = 'create';
  signoVital: SignoVital;
  antropometria: Antropometria;
  areasSalud: Observable<AreaSalud[]>;
  dispensarios: Observable<Dispensario[]>;
  errorMessages = FORM_ERROR_MESSAGES;
  onlyNumbersRegex = ONLY_NUMBERS_REGEX;
  constructor(
    private dialog: MatDialog,
    private fb: FormBuilder,
    private pacienteGlobalService: PacienteGlobalService,
    private usuarioService: SaludUserService,
    private authService: AuthService,
    private catalogoService: CatalogoService,
    private notaEnfermeriaService: NotaEnfermeriaService,
    private snackBar: MatSnackBar,
    private loadingService: LoadingService,
    private dialogRef: MatDialogRef<NotaEnfermeriaCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public defaults: NotaEnfermeria,
  ) {
  }

  ngOnInit(): void {
    this.areasSalud = this.usuarioService.getAreasSalud();
    this.dispensarios = this.catalogoService.getDispensarios();
    if (this.defaults) {
      this.mode = 'update';
      this.antropometria = this.defaults.antropometria;
      this.peso = this.antropometria.peso;
      this.talla = this.antropometria.talla;
      this.signoVital = this.defaults.signoVital;
    } else {
      this.defaults = {} as NotaEnfermeria;
      this.antropometria = {} as Antropometria;
      this.signoVital = {} as SignoVital;
    }
    this.antropometriaFormGroup = this.fb.group({
      id: [null],
      indiceMasaCorporal: [{value: this.antropometria.indiceMasaCorporal || null, disabled: true}],
      perimetroAbdominal: [this.antropometria.perimetroAbdominal || null, [Validators.min(0), Validators.max(10000)]],
      peso: [this.antropometria.peso || null, [Validators.min(0), Validators.max(10000)]],
      talla: [this.antropometria.talla || null,
        [Validators.min(0), Validators.max(1000), Validators.pattern(this.onlyNumbersRegex)]],
    });
    this.signoVitalFormGroup = this.fb.group({
      id: [null],
      fechaUltimaMenstruacion: [this.signoVital.fechaUltimaMenstruacion || null],
      frecuenciaCardiaca: [this.signoVital.frecuenciaCardiaca || null,
        [Validators.min(30),
          Validators.max(200),
          Validators.pattern(this.onlyNumbersRegex)]],
      frecuenciaRespiratoria: [this.signoVital.frecuenciaCardiaca || null,
        [Validators.min(1),
          Validators.max(60),
          Validators.pattern(this.onlyNumbersRegex)]],
      presionArterial: [this.signoVital.presionArterial || null,
        [Validators.min(0),
          Validators.max(200),
          Validators.pattern(this.onlyNumbersRegex)]],
      diastole: [this.signoVital.diastole || null,
        [Validators.min(0), Validators.max(200),
          Validators.pattern(this.onlyNumbersRegex)]],
      saturacionOxigeno: [this.signoVital.saturacionOxigeno || null,
        [Validators.min(20),
          Validators.max(100),
          Validators.pattern(this.onlyNumbersRegex)]],
      temperatura: [this.signoVital.temperatura || null,
        [Validators.min(35),
          Validators.max(42),
          Validators.pattern(this.onlyNumbersRegex)]]
    });
    this.preConsultaFormGroup = this.fb.group({
      id: [null],
      preConsulta: [this.defaults.preConsulta || null],
      idAreaSalud: [{value: this.defaults.idAreaSalud || null, disabled: true}, Validators.required],
      idDispensario: [{value: this.defaults.idDispensario || null, disabled: true}, Validators.required],
      idUsuario: [this.defaults.idUsuario || null, Validators.required],
      idPaciente: [this.defaults.idPaciente || null, Validators.required]
    });
    this.pacienteGlobalService.pacienteGlobal$
      .subscribe((paciente: Paciente) => {
        if (paciente) {
          this.pacienteActual = paciente;
          this.preConsultaFormGroup.patchValue({idPaciente: paciente.id});
        }
      });
    this.authService.saludUserData$
      .subscribe((saludUser) => {
        if (saludUser) {
          this.usuarioSalud = saludUser;
          this.preConsultaFormGroup.patchValue({idAreaSalud: saludUser.idAreaSalud});
          this.preConsultaFormGroup.patchValue({idDispensario: saludUser.idDispensario});
          this.preConsultaFormGroup.patchValue({idUsuario: saludUser.pidm});
        }
      });
  }

  save(): void {
    this.setIndiceMasaCorporal();
    const antropometria = this.antropometriaFormGroup.getRawValue();
    const signoVital = this.signoVitalFormGroup.value;
    const notaEnfermeria = this.preConsultaFormGroup.getRawValue();
    antropometria.indiceMasaCorporal = this.indiceMasaCorporal !== 0 ? this.indiceMasaCorporal : null;

    if (checkIfAllObjectValuesAreNull(signoVital)) {
      notaEnfermeria.signoVital = signoVital;
    } else {
      notaEnfermeria.signoVital = null;
    }

    if (checkIfAllObjectValuesAreNull(antropometria)) {
      notaEnfermeria.antropometria = antropometria;
    } else {
      notaEnfermeria.antropometria = null;
    }

    this.loadingService.showLoaderUntilCompleted(
      this.notaEnfermeriaService.createNotaEnfermeria(notaEnfermeria)
    ).subscribe((notaCreada) => {
      if (notaCreada) {
        this.snackBar.open('Registro creado EXITOSAMENTE', 'CERRAR', {
          duration: 5000
        });
        this.dialogRef.close(notaCreada);
      } else {
        this.snackBar.open('ERROR al crear el registro', 'CERRAR', {
          duration: 5000
        });
      }
    });
  }

  setPeso(event: any): void {
    this.peso = event.target.value;
    this.setIndiceMasaCorporal();
  }

  setTalla(event: any): void {
    this.talla = event.target.value;
    this.setIndiceMasaCorporal();
  }

  setIndiceMasaCorporal(): void {
    if (this.peso && this.talla) {
      const imc = calculateIMC(this.peso, this.talla);
      this.antropometriaFormGroup.get('indiceMasaCorporal').setValue(imc);
      this.indiceMasaCorporal = Number(imc);
    } else {
      this.indiceMasaCorporal = 0.0;
    }
  }

  isFemenino(): boolean {
    if (!this.pacienteActual) {
      return false;
    }
    return this.pacienteActual.sexo === SexoEnum.MUJER;
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }
}
