import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NotaEnfermeriaCreateComponent} from 'src/app/modules/enfermeria/notas-enfermeria/components/nota-enfermeria-create/nota-enfermeria-create.component';
import {FlexModule} from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {MatDialogModule} from '@angular/material/dialog';
import {MatStepperModule} from '@angular/material/stepper';
import {IconModule} from '@visurel/iconify-angular';
import {ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSelectModule} from '@angular/material/select';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import {MatSlideToggleModule} from "@angular/material/slide-toggle";


@NgModule({
  declarations: [NotaEnfermeriaCreateComponent],
    imports: [
        CommonModule,
        FlexModule,
        MatIconModule,
        MatDividerModule,
        MatDialogModule,
        MatStepperModule,
        IconModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatInputModule,
        MatDatepickerModule,
        MatSelectModule,
        LoadingModule,
        MatSlideToggleModule,
    ]
})
export class NotaEnfermeriaCreateModule { }
