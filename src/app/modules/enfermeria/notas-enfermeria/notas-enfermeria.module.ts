import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NotasEnfermeriaRoutingModule} from 'src/app/modules/enfermeria/notas-enfermeria/notas-enfermeria-routing.module';
import {FlexLayoutModule, FlexModule} from '@angular/flex-layout';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {IconModule} from '@visurel/iconify-angular';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatMenuModule} from '@angular/material/menu';
import {PageLayoutModule} from 'src/@vex/components/page-layout/page-layout.module';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import {MatDividerModule} from '@angular/material/divider';
import {MatInputModule} from '@angular/material/input';
import { NotaEnfermeriaCreateComponent } from './components/nota-enfermeria-create/nota-enfermeria-create.component';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import {EmptyPacienteModule} from 'src/app/core/components/empty-paciente/empty-paciente.module';
import {NotaEnfermeriaCreateModule} from 'src/app/modules/enfermeria/notas-enfermeria/components/nota-enfermeria-create/nota-enfermeria-create.module';
import { ShowResponsableMenuComponent } from './components/show-responsable-menu/show-responsable-menu.component';
import { EmbarazoModalComponent } from './components/embarazo-modal/embarazo-modal.component';
import {StripHtmlModule} from 'src/@vex/pipes/strip-html/strip-html.module';
import { PostConsultaModalComponent } from './components/post-consulta-modal/post-consulta-modal.component';
import { NotasEnfermeriaListComponent } from './pages/notas-enfermeria-list/notas-enfermeria-list.component';



@NgModule({
  declarations: [
    ShowResponsableMenuComponent,
    EmbarazoModalComponent,
    PostConsultaModalComponent,
    NotasEnfermeriaListComponent
  ],
  imports: [
    CommonModule,
    NotasEnfermeriaRoutingModule,
    NotaEnfermeriaCreateModule,
    FlexModule,
    ContainerModule,
    IconModule,
    BreadcrumbsModule,
    FlexLayoutModule,
    MatIconModule,
    MatButtonModule,
    MatTooltipModule,
    ReactiveFormsModule,
    MatMenuModule,
    PageLayoutModule,
    MatTableModule,
    MatSortModule,
    MatCheckboxModule,
    MatPaginatorModule,
    FormsModule,
    MatDialogModule,
    MatSelectModule,
    MatDividerModule,
    MatInputModule,
    MatStepperModule,
    MatDatepickerModule,
    LoadingModule,
    EmptyPacienteModule,
    StripHtmlModule
  ]
})
export class NotasEnfermeriaModule { }
