import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {NotaEnfermeria} from 'src/app/modules/enfermeria/notas-enfermeria/models/nota-enfermeria.model';

@Injectable({
  providedIn: 'root'
})
export class NotaEnfermeriaService {
  private baseUrl: string = environment.baseUrl;
  private readonly commonUrl: string;

  constructor(private http: HttpClient) {
    this.commonUrl = this.baseUrl + 'notas-enfermeria/';
  }

  getAllNotasEnfermeria(idPaciente: number): Observable<NotaEnfermeria[]> {
    return this.http.get<NotaEnfermeria[]>(this.commonUrl + 'paciente?idPaciente=' + idPaciente);
  }

  getNotasEnfermeriaByPacienteAndEvolucionEmpty(idPaciente: number): Observable<NotaEnfermeria[]> {
    return this.http.get<NotaEnfermeria[]>(this.commonUrl + 'empty-evolucion?idPaciente=' + idPaciente);
  }

  getNotaEnfermeria(id: number): Observable<NotaEnfermeria> {
    return this.http.get<NotaEnfermeria>(this.commonUrl + id);
  }

  createNotaEnfermeria(data: NotaEnfermeria): Observable<NotaEnfermeria>  {
    return this.http.post<NotaEnfermeria>(this.commonUrl, JSON.stringify(data));
  }

  patchNotaEnfermeria(data: any, id: number): Observable<NotaEnfermeria> {
    return this.http.patch<NotaEnfermeria>(this.commonUrl + id, JSON.stringify(data));
  }
}
