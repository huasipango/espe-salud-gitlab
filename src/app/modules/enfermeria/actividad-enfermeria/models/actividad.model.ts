import {TipoActividadEnfermeria} from 'src/app/core/models/catalogo/tipo-actividad-enfermeria';
import {UsuarioSimple} from 'src/app/core/models/usuario/usuario-simple.model';

export class ActividadEnfermeria {
  id: number;
  fecha: Date;
  descripcion: string;
  idUsuario: number;
  tipoActividadEnfermeria: TipoActividadEnfermeria;
  idTipoActividadEnfermeria: number;
  usuario: UsuarioSimple;
}
