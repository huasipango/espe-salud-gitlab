import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ActividadEnfermeria} from 'src/app/modules/enfermeria/actividad-enfermeria/models/actividad.model';

@Injectable({
  providedIn: 'root'
})
export class ActividadEnfermeriaService {

  baseUrl: string = environment.baseUrl;
  constructor(private http: HttpClient) {
  }

  // get
  getActividadEnfermeria(id: number): Observable<ActividadEnfermeria[]> {
    const url = this.baseUrl + 'actividades-enfermeria/usuario?idUsuario=' + id;
    return this.http.get<ActividadEnfermeria[]>(url);
  }

  createActividad(data: ActividadEnfermeria): Observable<ActividadEnfermeria>  {
    const url = this.baseUrl + 'actividades-enfermeria/';
    return this.http.post<ActividadEnfermeria> (url, JSON.stringify(data));
  }

  // PUT
  updateActividad(id: number, data): Observable<ActividadEnfermeria>  {
    const url = this.baseUrl + 'actividades-enfermeria/' + id;
    return this.http.put<ActividadEnfermeria>(url, JSON.stringify(data));
  }

  // DELETE
  deleteActividad(id: number): Observable<ActividadEnfermeria>  {
    const url = this.baseUrl + 'actividades-enfermeria/' + id;
    return this.http.delete<ActividadEnfermeria>(url);
  }

}
