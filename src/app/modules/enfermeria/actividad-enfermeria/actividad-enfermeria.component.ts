import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { fadeInUp400ms } from 'src/@vex/animations/fade-in-up.animation';
import { stagger40ms } from 'src/@vex/animations/stagger.animation';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions } from '@angular/material/form-field';
import { FormControl } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { TableColumn } from 'src/@vex/interfaces/table-column.interface';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icHeartBeat from '@iconify/icons-fa-solid/heartbeat';
import icDownload from '@iconify/icons-ic/cloud-download';
import icPrint from '@iconify/icons-ic/print';
import icCheck from '@iconify/icons-fa-solid/clipboard-check';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { DateUtil } from 'src/app/core/utils/date-utils';
import { DeleteModalComponent } from 'src/app/shared/components/delete-modal/delete-modal.component';
import { scaleFadeIn400ms } from 'src/@vex/animations/scale-fade-in.animation';
import { fadeInRight400ms } from 'src/@vex/animations/fade-in-right.animation';
import { scaleIn400ms } from 'src/@vex/animations/scale-in.animation';
import { SaludUser } from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import { TipoActividadEnfermeria } from 'src/app/core/models/catalogo/tipo-actividad-enfermeria';
import { AuthService } from 'src/app/core/auth/auth.service';
import {ActividadEnfermeria} from 'src/app/modules/enfermeria/actividad-enfermeria/models/actividad.model';
import {ActividadEnfermeriaService} from 'src/app/modules/enfermeria/actividad-enfermeria/services/actividad-enfermeria.service';
import {ActividadEnfermeriaModalComponent} from 'src/app/modules/enfermeria/actividad-enfermeria/components/actividad-enfermeria-modal/actividad-enfermeria-modal.component';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';

@UntilDestroy()
@Component({
  selector: 'vex-actividad-enfermeria',
  templateUrl: './actividad-enfermeria.component.html',
  styleUrls: ['./actividad-enfermeria.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms,
    scaleIn400ms,
    fadeInRight400ms,
    scaleFadeIn400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class ActividadEnfermeriaComponent implements OnInit, AfterViewInit {
  actividades: ActividadEnfermeria[] = [];
  usuarioSalud: SaludUser;
  messages = USER_MESSAGES;
  columns: TableColumn<ActividadEnfermeria>[] = [
    {
      label: 'Tipo actividad',
      property: 'tipoActividadEnfermeria',
      type: 'object',
      visible: true
    },
    {
      label: 'Descripción de la actividad',
      property: 'descripcion',
      type: 'text',
      visible: true
    },
    {
      label: 'Fecha',
      property: 'fecha',
      type: 'datetime',
      visible: true
    },
    { label: 'Actions', property: 'actions', type: 'button', visible: true }
  ];

  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<ActividadEnfermeria> | null;
  searchCtrl = new FormControl();

  icEdit = icEdit;
  icSearch = icSearch;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icMoreHoriz = icMoreHoriz;
  icHeartBeat = icHeartBeat;
  icDownload = icDownload;
  icPrint = icPrint;
  icCheck = icCheck;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private actividadService: ActividadEnfermeriaService,
    private authService: AuthService,
  ) {
  }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.getActividades();
    this.searchCtrl.valueChanges.pipe(
      untilDestroyed(this)
    ).subscribe(value => this.onFilterChange(value));
  }

  getActividades(): void {
    this.authService.saludUserData$
      .subscribe((saludUser) => {
        if (saludUser) {
          this.usuarioSalud = saludUser;
          this.actividadService.getActividadEnfermeria(saludUser.pidm)
            .subscribe((data: ActividadEnfermeria[]) => {
              this.actividades = data;
              this.dataSource.data = this.actividades;
            });
        }
      });
  }

  createActividad(): void {
    this.dialog.open(ActividadEnfermeriaModalComponent, {
      width: '500px',
      maxWidth: '100%',
      disableClose: true
    }).afterClosed()
      .subscribe((actividad: ActividadEnfermeria) => {
        if (actividad) {
          this.showNotification(this.messages.createdSuccessMessage, 'OK');
          this.getActividades();
        }
      });
  }

  updateActividad(actividad: ActividadEnfermeria): void {
    this.dialog.open(ActividadEnfermeriaModalComponent, {
      data: actividad,
      maxWidth: '100%',
      width: '600px',
      disableClose: true
    }).afterClosed()
      .subscribe((updatedActividad) => {
        if (updatedActividad) {
          this.showNotification(this.messages.updatedSuccessMessage, 'OK');
          this.getActividades();
        }
      });

  }

  deleteActividad(actividad: ActividadEnfermeria): void {
    this.actividadService.deleteActividad(actividad.id)
      .subscribe((success) => {
        if (success) {
          this.showNotification(this.messages.deletedSuccessMessage, 'CERRAR');
          this.getActividades();
        } else {
          this.showNotification(this.messages.deleteFailMessage, 'CERRAR');
        }
      });
  }

  openDeleteDialog(actividad: ActividadEnfermeria): void {
    this.dialog.open(DeleteModalComponent, {
      data: this.messages.beforeDeleteMessage,
      disableClose: false,
      width: '400px'
    }).afterClosed().subscribe(result => {
      if (result === 'si') { this.deleteActividad(actividad); }
    });
  }

  getFormattedDateTime(date: string) {
    return DateUtil.showDateTimeFormat(date);
  }

  onFilterChange(value: string) {
    if (!this.dataSource) {
      return;
    }
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  print() {
  }

  openSearchModal() {
  }

  getTipoActividades(tipo: TipoActividadEnfermeria): string {
    return tipo.nombre;
  }

}




