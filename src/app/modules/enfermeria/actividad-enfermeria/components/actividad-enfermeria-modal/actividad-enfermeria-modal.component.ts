import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ThemePalette} from '@angular/material/core';
import {Observable} from 'rxjs';
import {DescripcionActividadEnfermeria} from 'src/app/core/models/catalogo/descripcion-actividad-enfermeria';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import {TipoActividadEnfermeria} from 'src/app/core/models/catalogo/tipo-actividad-enfermeria';
import {FORM_ERROR_MESSAGES} from 'src/app/core/constants/constants';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ActividadEnfermeria} from 'src/app/modules/enfermeria/actividad-enfermeria/models/actividad.model';
import {AuthService} from 'src/app/core/auth/auth.service';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {LoadingService} from 'src/app/core/services/loading.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import icClose from '@iconify/icons-ic/twotone-close';
import icCheck from '@iconify/icons-ic/check';
import {ActividadEnfermeriaService} from 'src/app/modules/enfermeria/actividad-enfermeria/services/actividad-enfermeria.service';
import {USER_MESSAGES} from 'src/app/core/constants/user-messages';

@Component({
  selector: 'vex-actividad-enfermeria-modal',
  templateUrl: './actividad-enfermeria-modal.component.html',
  styleUrls: ['./actividad-enfermeria-modal.component.scss']
})
export class ActividadEnfermeriaModalComponent implements OnInit {

  form: FormGroup;
  mode: 'create' | 'update' = 'create';

  descipcionB: boolean;
  public showSpinners = true;
  public touchUi = false;
  public enableMeridian = false;
  public stepHour = 1;
  public stepMinute = 1;
  public stepSecond = 1;
  public color: ThemePalette = 'primary';

  descripciones: Observable<DescripcionActividadEnfermeria[]>;

  usuarioSalud: SaludUser;
  tipoActividad: Observable<TipoActividadEnfermeria[]>;
  icClose = icClose;
  icCheck = icCheck;

  loading = false;
  errorMessages = FORM_ERROR_MESSAGES;
  messages = USER_MESSAGES;
  constructor(
    @Inject(MAT_DIALOG_DATA) public defaults: ActividadEnfermeria,
    private dialogRef: MatDialogRef<ActividadEnfermeriaModalComponent>,
    private fb: FormBuilder,
    private authService: AuthService,
    private actividadService: ActividadEnfermeriaService,
    private catalogoService: CatalogoService,
    private loadingService: LoadingService,
    private snackbar: MatSnackBar,
  ) { }


  ngOnInit(): void {
    this.tipoActividad = this.catalogoService.getTipoActividad();
    if (this.defaults) {
      this.mode = 'update';
      this.descripciones = this.catalogoService.getDescripcionesByTipo(this.defaults.idTipoActividadEnfermeria);
    } else {
      this.defaults = {} as ActividadEnfermeria;
    }

    this.descipcionB = false;
    this.form = this.fb.group({
      id: [this.defaults.id || null],
      idTipoActividadEnfermeria: [this.defaults.idTipoActividadEnfermeria || null, Validators.required],
      fecha: [this.defaults.fecha || new Date(), Validators.required],
      descripcion: [this.defaults.descripcion || '', Validators.required],
      idUsuario: [this.defaults.idUsuario || null, Validators.required]
    });
    this.authService.saludUserData$
      .subscribe((saludUser) => {
        if (saludUser) {
          this.usuarioSalud = saludUser;
          this.form.patchValue({ idUsuario: saludUser.pidm });
        }
      });
  }

  save() {
    const actividad: ActividadEnfermeria = this.form.value;
    if (this.mode === 'create') {
      this.createActividad(actividad);
    } else if (this.mode === 'update') {
      this.updateActividad(actividad);
    }
  }

  createActividad(actividad: ActividadEnfermeria): void {
    this.loadingService.showLoaderUntilCompleted(
      this.actividadService.createActividad(actividad)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      } else {
        this.showNotification(this.messages.createdFailedMessage, 'CERRAR');
      }
    });
  }

  updateActividad(actividad: ActividadEnfermeria): void {
    this.loadingService.showLoaderUntilCompleted(
      this.actividadService.updateActividad(actividad.id, actividad)
    ).subscribe((response) => {
      if (response) {
        this.dialogRef.close(response);
      } else {
        this.showNotification(this.messages.updatedFailedMessage, 'CERRAR');
      }
    });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  showNotification(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 5000
    });
  }

  selectTipo() {
    const id = this.form.get('idTipoActividadEnfermeria').value;
    this.descripciones = this.catalogoService.getDescripcionesByTipo(id);
  }

  isDescripcion(): boolean{
    const id = this.form.get('idTipoActividadEnfermeria').value;
    this.descipcionB = id === 1;
    return this.descipcionB;
  }

}
