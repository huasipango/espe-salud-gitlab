import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActividadEnfermeriaComponent } from './actividad-enfermeria.component';
import { ActividadEnfermeriaModalComponent } from './components/actividad-enfermeria-modal/actividad-enfermeria-modal.component';
import {ExtendedModule, FlexModule} from '@angular/flex-layout';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {IconModule} from '@visurel/iconify-angular';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {
  NgxMatDatetimePickerModule,
  NgxMatNativeDateModule,
  NgxMatTimepickerModule
} from '@angular-material-components/datetime-picker';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import {ActividadEnfermeriaRoutingModule} from 'src/app/modules/enfermeria/actividad-enfermeria/actividad-enfermeria-routing.module';



@NgModule({
  declarations: [ActividadEnfermeriaComponent, ActividadEnfermeriaModalComponent],
  imports: [
    CommonModule,
    ActividadEnfermeriaRoutingModule,
    FlexModule,
    ContainerModule,
    IconModule,
    BreadcrumbsModule,
    ExtendedModule,
    MatButtonModule,
    MatTooltipModule,
    MatIconModule,
    ReactiveFormsModule,
    MatMenuModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatCheckboxModule,
    FormsModule,
    MatDialogModule,
    MatInputModule,
    MatSelectModule,
    NgxMatDatetimePickerModule,
    NgxMatNativeDateModule,
    NgxMatTimepickerModule,
    MatDatepickerModule,
    LoadingModule
  ]
})
export class ActividadEnfermeriaModule { }
