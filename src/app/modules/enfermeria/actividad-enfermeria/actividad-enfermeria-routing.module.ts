import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from 'src/@vex/interfaces/vex-route.interface';
import {ActividadEnfermeriaComponent} from 'src/app/modules/enfermeria/actividad-enfermeria/actividad-enfermeria.component';
import {RolEnum} from 'src/app/core/enums/rol.enum';
import {RoleGuard} from 'src/app/core/auth/role.guard';

const routes: VexRoutes = [
  {
    path: '',
    component: ActividadEnfermeriaComponent,
    canActivate: [RoleGuard],
    data: {
      toolbarShadowEnabled: true,
      containerEnabled: true,
      roles: [RolEnum.ROLE_ENFERMERO]
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActividadEnfermeriaRoutingModule { }
