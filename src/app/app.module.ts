import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {VexModule} from '../@vex/vex.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {CustomLayoutModule} from './custom-layout/custom-layout.module';
import {DatePipe, registerLocaleData} from '@angular/common';
import localeES from '@angular/common/locales/es-419';
import {MatPaginatorIntl} from '@angular/material/paginator';
import {getSpanishPaginatorIntl} from './core/utils/spanish-paginator-intl';
import {MAT_DATE_LOCALE} from '@angular/material/core';
import {OAuthModule} from 'angular-oauth2-oidc';
import {HttpConfigInterceptor} from 'src/app/core/interceptors/httpconfig.interceptor';
import {localString} from 'src/app/core/constants/constants';
import {QuicklinkModule} from 'ngx-quicklink';
import {environment} from 'src/environments/environment';
import {AngularSvgIconModule} from 'angular-svg-icon';

registerLocaleData(localeES, localString);


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    // Vex
    VexModule,
    CustomLayoutModule,
    QuicklinkModule,
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: [
          environment.baseUrl,
          environment.catalogoWebServiceUrl
        ],
        sendAccessToken: true
      }
    }),
    AngularSvgIconModule.forRoot()
  ],
  providers: [
    DatePipe,
    {provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl()},
    {provide: MAT_DATE_LOCALE, useValue: localString},
    {provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
