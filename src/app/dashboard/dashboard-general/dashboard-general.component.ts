import { Component, OnInit } from '@angular/core';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icExcel from '@iconify/icons-fa-solid/file-excel';
import icChart from '@iconify/icons-fa-solid/chart-area';
import {Link} from 'src/@vex/interfaces/link.interface';
import {Icon} from '@visurel/iconify-angular';
import { trackByRoute } from 'src/@vex/utils/track-by';
import {stagger40ms} from 'src/@vex/animations/stagger.animation';
import {fadeInUp400ms} from 'src/@vex/animations/fade-in-up.animation';

@Component({
  selector: 'vex-dashboard-general',
  templateUrl: './dashboard-general.component.html',
  styleUrls: ['./dashboard-general.component.scss'],
  animations: [
    stagger40ms,
    fadeInUp400ms
  ]
})
export class DashboardGeneralComponent implements OnInit {
  links: (Link & { icon: Icon })[] = [
    {
      label: 'Gráficos estadísticos',
      route: 'graficos',
      icon: icChart
    },
    {
      label: 'Reportes excel',
      route: 'reportes',
      icon: icExcel
    }
  ];

  trackByRoute = trackByRoute;
  icMoreVert = icMoreVert;
  icChart = icChart;
  constructor(
  ) { }

  ngOnInit(): void {
  }

}
