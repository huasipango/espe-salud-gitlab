import {Component, OnInit} from '@angular/core';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {TotalDashboardService} from 'src/app/core/services/total-dashboard.service';
import {LoadingService} from 'src/app/core/services/loading.service';
import {ReportDashboardService} from 'src/app/core/services/report-dashboard.service';
import icCloudDownload from '@iconify/icons-ic/twotone-cloud-download';
import icClose from '@iconify/icons-ic/twotone-close';
import {DashboardParams} from 'src/app/core/models/dashboard/dashboard-params.model';


interface AllCodigoCie {
  ene?: number;
  feb?: number;
  mar?: number;
  abr?: number;
  may?: number;
  jun?: number;
  jul?: number;
  ago?: number;
  sep?: number;
  oct?: number;
  nov?: number;
  dic?: number;
  tot?: number;
}
interface EncabezadoForm{
  cie10: string;
  name: string;
}
@Component({
  selector: 'vex-widget-table-all-patient-cie',
  templateUrl: './widget-table-all-patient-cie.component.html',
  styleUrls: ['./widget-table-all-patient-cie.component.scss']
})
export class WidgetTableAllPatientCieComponent implements OnInit {

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }
  title = 'Total de pacientes atendidos por código CIE10';
  dataFetched = false;
  icCloudDownload = icCloudDownload;
  icClose = icClose;
  year: number;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50, 100];

  data: any[] = [];
  columns: TableColumn<any>[] = [
    {label: 'CIE 10', property: 'cie10', type: 'text', visible: true},
    {label: 'ENFERMEDAD', property: 'name', type: 'text', visible: true},
    {label: 'ENE', property: 'ene', type: 'text', visible: true},
    {label: 'FEB', property: 'feb', type: 'text', visible: true},
    {label: 'MAR', property: 'mar', type: 'text', visible: true},
    {label: 'ABR', property: 'abr', type: 'text', visible: true},
    {label: 'MAY', property: 'may', type: 'text', visible: true},
    {label: 'JUN', property: 'jun', type: 'text', visible: true},
    {label: 'JUL', property: 'jul', type: 'text', visible: true},
    {label: 'AGO', property: 'ago', type: 'text', visible: true},
    {label: 'SEP', property: 'sep', type: 'text', visible: true},
    {label: 'OCT', property: 'oct', type: 'text', visible: true},
    {label: 'NOV', property: 'nov', type: 'text', visible: true},
    {label: 'DIC', property: 'dic', type: 'text', visible: true},
    {label: 'TOT', property: 'tot', type: 'text', visible: true}
  ];

  constructor(
    private totalDashboardService: TotalDashboardService,
    private loadingService: LoadingService,
    private reportDashboardService: ReportDashboardService
  ) { }
  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    this.getTotalAtenciones();
  }

  searchByYear(year: string): void {
    this.year = year !== '' ? Number(year) : null;
    this.getData();
  }

  createParams(): DashboardParams {
    return {
      year: this.year
    };
  }

  getTotalAtenciones(): void {
    this.totalDashboardService.getTotalAtencionesPorCie10(this.createParams())
      .subscribe((data: any[]) => {
        const dataEnvio = [];
        const datas = [];
        const series = [];
        let sum = [];
        let sumHor = 0;
        let sumTotal = 0;
        const sumTotVer = data.reduce((prev, next) => {
          return next.monthsData.map((value, index) => {
            return value + (prev[index] || 0);
          }, 0);
        }, []);
        const formTot: EncabezadoForm = {
          cie10: 'Total',
          name: ''
        };
        data.forEach((cie10) => {
          sumHor = 0;
          const form: EncabezadoForm = {
            cie10: cie10.cie10,
            name: cie10.name
          };
          sum = cie10.monthsData;
          sum.forEach((num: number) => {
            sumTotal += num;
            sumHor += num;
          });
          const formMes: AllCodigoCie = {
            ene: cie10.monthsData[0],
            feb: cie10.monthsData[1],
            mar: cie10.monthsData[2],
            abr: cie10.monthsData[3],
            may: cie10.monthsData[4],
            jun: cie10.monthsData[5],
            jul: cie10.monthsData[6],
            ago: cie10.monthsData[7],
            sep: cie10.monthsData[8],
            oct: cie10.monthsData[9],
            nov: cie10.monthsData[10],
            dic: cie10.monthsData[11],
            tot: sumHor
          };
          datas.push(formMes);
          series.push(form);
          series.push(datas);
          const dataUnido = Object.assign(formMes, form);
          dataEnvio.push(dataUnido);
        });
        const formTotMes: AllCodigoCie = {
          ene: sumTotVer[0],
          feb: sumTotVer[1],
          mar: sumTotVer[2],
          abr: sumTotVer[3],
          may: sumTotVer[4],
          jun: sumTotVer[5],
          jul: sumTotVer[6],
          ago: sumTotVer[7],
          sep: sumTotVer[8],
          oct: sumTotVer[9],
          nov: sumTotVer[10],
          dic: sumTotVer[11],
          tot: sumTotal
        };
        const dataTotUnido = Object.assign(formTotMes, formTot);
        dataEnvio.push(dataTotUnido);
        this.dataFetched = true;
        this.data = dataEnvio;
      });
  }

  generateReport() {
    this.loadingService.showLoaderUntilCompleted(
      this.reportDashboardService.getReportePorCIE10(this.createParams())
    ).subscribe();
  }
}
