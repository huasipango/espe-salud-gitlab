import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {Observable} from 'rxjs';
import {Dispensario} from 'src/app/core/models/catalogo/dispensario.model';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MONTHS, YEARS} from 'src/app/core/constants/constants';
import icExcel from '@iconify/icons-fa-solid/file-excel';
import {AuthService} from 'src/app/core/auth/auth.service';
import {PopoverRef} from 'src/@vex/components/popover/popover-ref';

@Component({
  selector: 'vex-filters-dashboard-menu',
  templateUrl: './filters-dashboard-menu.component.html',
  styleUrls: ['./filters-dashboard-menu.component.scss']
})
export class FiltersDashboardMenuComponent implements OnInit {
  dispensarios: Observable<Dispensario[]>;
  selected = 'all';
  form: FormGroup;
  years = YEARS();
  months = MONTHS;
  icExcel = icExcel;
  constructor(private catalogoService: CatalogoService,
              private fb: FormBuilder,
              private authService: AuthService,
              private popoverRef: PopoverRef<FiltersDashboardMenuComponent>) { }

  ngOnInit(): void {
    this.dispensarios = this.catalogoService.getDispensarios();
    this.form = this.fb.group({
      idDispensario: [-1],
      year: [new Date().getFullYear()],
      month: [new Date().getMonth() + 1]
    });
    this.authService.saludUserData$
      .subscribe((user) => {
        if (user) {
          this.form.patchValue({idDispensario: user.dispensario.id});
        }
      });
  }

  search() {
    const params = this.form.value;
    params.idDispensario = params.idDispensario === -1 ? null : params.idDispensario;
    params.year = params.year === -1 ? null : params.year;
    params.month = params.month === -1 ? null : params.month;
    this.popoverRef.close(params);
  }
}
