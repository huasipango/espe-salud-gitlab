import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DashboardGeneralComponent} from 'src/app/dashboard/dashboard-general/dashboard-general.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardGeneralComponent,
    data: {
      toolbarShadowEnabled: true
    },
    children: [
      {
        path: '',
        redirectTo: 'graficos'
      },
      {
        path: 'graficos',
        loadChildren: () => import('./pages/graficos-estadisticos-general/graficos-estadisticos-general.module')
          .then(m => m.GraficosEstadisticosGeneralModule)
      },
      {
        path: 'reportes',
        loadChildren: () => import('./pages/reportes-excel-general/reportes-excel-general.module')
          .then(m => m.ReportesExcelGeneralModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardGeneralRoutingModule {
}
