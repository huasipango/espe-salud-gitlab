import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {GraficosEstadisticosGeneralComponent} from 'src/app/dashboard/dashboard-general/pages/graficos-estadisticos-general/graficos-estadisticos-general.component';

const routes: Routes = [
  {
    path: '',
    component: GraficosEstadisticosGeneralComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GraficosEstadisticosGeneralRoutingModule { }
