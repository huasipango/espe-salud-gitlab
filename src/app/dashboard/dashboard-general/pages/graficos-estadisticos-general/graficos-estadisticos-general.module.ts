import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GraficosEstadisticosGeneralRoutingModule } from './graficos-estadisticos-general-routing.module';
import { GraficosEstadisticosGeneralComponent } from './graficos-estadisticos-general.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {TitlePageModule} from 'src/app/dashboard/components/title-page/title-page.module';


@NgModule({
  declarations: [GraficosEstadisticosGeneralComponent],
  imports: [
    CommonModule,
    GraficosEstadisticosGeneralRoutingModule,
    FlexLayoutModule,
    MatExpansionModule,
    MatListModule,
    MatIconModule,
    IconModule,
    TitlePageModule
  ]
})
export class GraficosEstadisticosGeneralModule { }
