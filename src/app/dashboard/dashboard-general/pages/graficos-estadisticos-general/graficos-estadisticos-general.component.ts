import { Component, OnInit } from '@angular/core';
import {Icon} from '@visurel/iconify-angular';
import icDescription from '@iconify/icons-ic/twotone-description';
import {MatDialog} from '@angular/material/dialog';
import {TipoReporte} from 'src/app/core/enums/tipo-reporte.enum';
import { trackById } from 'src/@vex/utils/track-by';
import {ChartType} from 'ng-apexcharts';
import {GeneralMonthChartModalComponent} from 'src/app/dashboard/components/general-month-chart-modal/general-month-chart-modal.component';
import {LayoutService} from 'src/@vex/services/layout.service';
import {WidgetTableAllPatientCieComponent} from 'src/app/dashboard/dashboard-general/components/widget-table-all-patient-cie/widget-table-all-patient-cie.component';

export interface Reporte {
  id: number;
  label: string;
  icon: Icon;
  type: TipoReporte;
  slug: string;
  filterByDispensario: boolean;
  chart: ChartType;
  onClick: (guide: Reporte) => void;
}

@Component({
  selector: 'vex-graficos-estadisticos-general',
  templateUrl: './graficos-estadisticos-general.component.html',
  styleUrls: ['./graficos-estadisticos-general.component.scss']
})
export class GraficosEstadisticosGeneralComponent implements OnInit {
  icDescription = icDescription;
  guides: Reporte[] = [
    {
      id: 1,
      label: 'Total de pacientes atendidos por dispensario',
      icon: icDescription,
      type: TipoReporte.monthly,
      slug: 'dispensario',
      chart: 'area',
      filterByDispensario: false,
      onClick: guide => this.openDialog(guide)
    },
    {
      id: 2,
      label: 'Total de pacientes atendidos por morbilidad y cronología',
      icon: icDescription,
      type: TipoReporte.monthly,
      slug: 'morbilidad',
      chart: 'bar',
      filterByDispensario: true,
      onClick: guide => this.openDialog(guide)
    },
    {
      id: 3,
      label: 'Total de pacientes atendidos según el motivo de atención',
      icon: icDescription,
      type: TipoReporte.monthly,
      slug: 'motivo-atencion',
      chart: 'area',
      filterByDispensario: true,
      onClick: guide => this.openDialog(guide)
    },
    {
      id: 4,
      label: 'Total de pacientes atendidos por prevención y cronología',
      icon: icDescription,
      type: TipoReporte.monthly,
      slug: 'prevencion',
      chart: 'bar',
      filterByDispensario: true,
      onClick: guide => this.openDialog(guide)
    },
    {
      id: 5,
      label: 'Total de atenciones por prevención vs morbilidad',
      icon: icDescription,
      type: TipoReporte.monthly,
      slug: 'prevencion-morbilidad',
      chart: 'bar',
      filterByDispensario: true,
      onClick: guide => this.openDialog(guide)
    },
    {
      id: 6,
      label: 'Total de pacientes atendidos por su relación con la ESPE',
      icon: icDescription,
      type: TipoReporte.monthly,
      slug: 'relaciones-espe',
      chart: 'area',
      filterByDispensario: true,
      onClick: guide => this.openDialog(guide)
    },
    {
      id: 7,
      label: 'Total de pacientes atendidos por seguro de salud',
      icon: icDescription,
      type: TipoReporte.monthly,
      slug: 'seguro-salud',
      chart: 'area',
      filterByDispensario: true,
      onClick: guide => this.openDialog(guide)
    },
    {
      id: 8,
      label: 'Total de pacientes atendidos por su sexo',
      icon: icDescription,
      type: TipoReporte.monthly,
      slug: 'sexo',
      chart: 'bar',
      filterByDispensario: true,
      onClick: guide => this.openDialog(guide)
    },
    {
      id: 9,
      label: 'Total de pacientes atendidos por el tipo de consulta',
      icon: icDescription,
      type: TipoReporte.monthly,
      slug: 'tipo-consulta',
      chart: 'bar',
      filterByDispensario: true,
      onClick: guide => this.openDialog(guide)
    },
  ];

  monthlyReports = this.guides.filter(guide => guide.type === TipoReporte.monthly);
  trackById = trackById;

  constructor(private dialog: MatDialog, private layoutService: LayoutService) { }

  ngOnInit(): void {
  }

  openDialog(reporte: Reporte) {
    this.dialog.open(GeneralMonthChartModalComponent, {
      data: reporte,
      width: this.layoutService.isMobile() ? '100%' : '80%',
      maxWidth: '100%'
    });
  }

  openCie10Table() {
    this.dialog.open(WidgetTableAllPatientCieComponent, {
      width: this.layoutService.isMobile() ? '100%' : '90%',
      maxWidth: '100%'
    });
  }
}
