import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportesExcelGeneralRoutingModule } from './reportes-excel-general-routing.module';
import { ReportesExcelGeneralComponent } from './reportes-excel-general.component';
import {TitlePageModule} from 'src/app/dashboard/components/title-page/title-page.module';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {MatSelectModule} from '@angular/material/select';
import {ReactiveFormsModule} from '@angular/forms';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';


@NgModule({
  declarations: [ReportesExcelGeneralComponent],
  imports: [
    CommonModule,
    ReportesExcelGeneralRoutingModule,
    TitlePageModule,
    MatListModule,
    MatIconModule,
    IconModule,
    FlexLayoutModule,
    MatButtonModule,
    MatMenuModule,
    MatSelectModule,
    ReactiveFormsModule,
    LoadingModule
  ]
})
export class ReportesExcelGeneralModule { }
