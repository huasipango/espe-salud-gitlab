import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ReportesExcelGeneralComponent} from 'src/app/dashboard/dashboard-general/pages/reportes-excel-general/reportes-excel-general.component';

const routes: Routes = [
  {
    path: '',
    component: ReportesExcelGeneralComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportesExcelGeneralRoutingModule { }
