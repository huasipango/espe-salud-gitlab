import { Component, OnInit } from '@angular/core';
import icExcel from '@iconify/icons-fa-solid/file-excel';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import {PopoverService} from 'src/@vex/components/popover/popover.service';
import {FiltersDashboardMenuComponent} from 'src/app/dashboard/dashboard-general/components/filters-dashboard-menu/filters-dashboard-menu.component';
import {TipoReporte} from 'src/app/core/enums/tipo-reporte.enum';
import {Icon} from '@visurel/iconify-angular';
import {Reporte} from 'src/app/dashboard/dashboard-general/pages/graficos-estadisticos-general/graficos-estadisticos-general.component';
import { trackById } from 'src/@vex/utils/track-by';
import {ReportDashboardService} from 'src/app/core/services/report-dashboard.service';
import {LoadingService} from 'src/app/core/services/loading.service';

export interface ReporteExcel {
  id: number;
  label: string;
  icon: Icon;
  type: TipoReporte;
  slug: string;
  filterByDispensario?: boolean;
  onClick: (origin: any, report: Reporte) => void;
}

@Component({
  selector: 'vex-reportes-excel-general',
  templateUrl: './reportes-excel-general.component.html',
  styleUrls: ['./reportes-excel-general.component.scss']
})
export class ReportesExcelGeneralComponent implements OnInit {
  icExcel = icExcel;
  icFilterList = icFilterList;

  reports: ReporteExcel[] = [
    {
      id: 1,
      label: 'Reporte consolidado de atenciones',
      icon: icExcel,
      type: TipoReporte.monthly,
      slug: 'consolidado',
      onClick: (origin: any, report: Reporte) => this.openFilterMenu(origin, report)
    }
  ];
  trackById = trackById;

  constructor(
    private popoverService: PopoverService,
    private reportDashboardService: ReportDashboardService,
    private loadingService: LoadingService) { }

  ngOnInit(): void {
  }

  openFilterMenu(origin: any, reporte: Reporte) {
    this.popoverService.open({
      content: FiltersDashboardMenuComponent,
      origin
    }).afterClosed$.subscribe((params) => {
      this.loadingService.showLoaderUntilCompleted(
        this.reportDashboardService.getReport(reporte.slug, params.data)
      ).subscribe();
    });
  }
}
