import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardGeneralComponent } from './dashboard-general.component';
import {DashboardGeneralRoutingModule} from 'src/app/dashboard/dashboard-general/dashboard-general-routing.module';
import {SecondaryToolbarModule} from 'src/@vex/components/secondary-toolbar/secondary-toolbar.module';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {MatButtonModule} from '@angular/material/button';
import {WidgetLargeChartModule} from 'src/@vex/components/widgets/widget-large-chart/widget-large-chart.module';
import {ChartModule} from 'src/@vex/components/chart/chart.module';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import {MatInputModule} from '@angular/material/input';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PickYearModule} from 'src/app/shared/components/pick-year/pick-year.module';
import {SharedModule} from 'src/app/shared/shared.module';
import {PickFiltroModule} from 'src/app/shared/components/pick-filtro/pick-filtro.module';
import {MatTooltipModule} from '@angular/material/tooltip';
import {NgApexchartsModule} from 'ng-apexcharts';
import {ChartNgModule} from 'src/@vex/components/chart-ng/chart-ng.module';
import { WidgetTableAllPatientCieComponent } from './components/widget-table-all-patient-cie/widget-table-all-patient-cie.component';
import {MatRippleModule} from '@angular/material/core';
import {WidgetTableModule} from 'src/@vex/components/widgets/widget-table/widget-table.module';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import { FiltersDashboardMenuComponent } from './components/filters-dashboard-menu/filters-dashboard-menu.component';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';



@NgModule({
  declarations: [
    DashboardGeneralComponent,
    WidgetTableAllPatientCieComponent,
    FiltersDashboardMenuComponent,
  ],
  imports: [
    CommonModule,
    DashboardGeneralRoutingModule,
    SecondaryToolbarModule,
    BreadcrumbsModule,
    MatIconModule,
    IconModule,
    FlexLayoutModule,
    ContainerModule,
    MatButtonModule,
    WidgetLargeChartModule,
    ChartModule,
    LoadingModule,
    MatInputModule,
    ReactiveFormsModule,
    PickYearModule,
    SharedModule,
    PickFiltroModule,
    MatTooltipModule,
    NgApexchartsModule,
    ChartNgModule,
    MatRippleModule,
    WidgetTableModule,
    MatDialogModule,
    MatDividerModule,
    MatSelectModule,
    MatDatepickerModule,
    FormsModule
  ]
})
export class DashboardGeneralModule { }
