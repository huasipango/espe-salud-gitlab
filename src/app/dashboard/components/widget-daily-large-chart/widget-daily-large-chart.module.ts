import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetDailyLargeChartComponent } from './widget-daily-large-chart.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import {IconModule} from '@visurel/iconify-angular';
import {ChartNgModule} from 'src/@vex/components/chart-ng/chart-ng.module';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';



@NgModule({
  declarations: [
    WidgetDailyLargeChartComponent
  ],
  exports: [
    WidgetDailyLargeChartComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    IconModule,
    ChartNgModule,
    LoadingModule
  ]
})
export class WidgetDailyLargeChartModule { }
