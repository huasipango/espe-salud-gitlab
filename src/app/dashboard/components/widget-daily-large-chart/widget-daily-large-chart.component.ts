import {Component, Input, OnInit} from '@angular/core';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icCloudDownload from '@iconify/icons-ic/twotone-cloud-download';
import faCaretUp from '@iconify/icons-fa-solid/caret-up';
import faCaretDown from '@iconify/icons-fa-solid/caret-down';
import {ApexAxisChartSeries, ApexNonAxisChartSeries} from 'ng-apexcharts';
import {ApexOptions} from 'src/@vex/components/chart-ng/chart-ng.component';
import {defaultDashboardChartNgOptions} from 'src/app/core/utils/default-dashboard-chart-ng-options';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import {LoadingService} from 'src/app/core/services/loading.service';
import {ReportDashboardService} from 'src/app/core/services/report-dashboard.service';
import {AuthService} from 'src/app/core/auth/auth.service';
import {PopoverService} from 'src/@vex/components/popover/popover.service';
import {FilterYearMonthComponent} from 'src/app/dashboard/dashboard-personal/components/filter-year-month/filter-year-month.component';
import {DashboardParams} from 'src/app/core/models/dashboard/dashboard-params.model';
import { calculatePercentageDifference } from 'src/app/core/utils/operations.util';


@Component({
  selector: 'vex-widget-daily-large-chart',
  templateUrl: './widget-daily-large-chart.component.html',
  styleUrls: ['./widget-daily-large-chart.component.scss']
})
export class WidgetDailyLargeChartComponent implements OnInit {
  @Input() total: number;
  @Input() pastValueTotal: number;
  @Input() labels: string[];
  @Input() series: ApexNonAxisChartSeries | ApexAxisChartSeries;
  @Input() options: ApexOptions = defaultDashboardChartNgOptions({
    grid: {
      show: true,
      strokeDashArray: 3,
      padding: {
        left: 16
      }
    },
    dataLabels: {
      enabled: false,
    },
    chart: {
      type: 'line',
      height: 300,
      sparkline: {
        enabled: false
      },
      zoom: {
        enabled: false
      },
      toolbar: {
        export: {
          png: {}
        }
      }
    },
    stroke: {
      width: 4
    },
    xaxis: {
      labels: {
        show: true
      }
    },
    yaxis: {
      labels: {
        show: true
      }
    },
    tooltip: {
      x: { show: true }
    },
  });

  icMoreHoriz = icMoreHoriz;
  icCloudDownload = icCloudDownload;
  faCaretUp = faCaretUp;
  faCaretDown = faCaretDown;
  tipoReporte = 'consolidado';
  today = new Date();
  user: SaludUser;
  constructor(
    private loadingService: LoadingService,
    private reportDashboardService: ReportDashboardService,
    private authService: AuthService,
    private popoverService: PopoverService,
  ) {}

  ngOnInit(): void {
    this.authService.saludUserData$.subscribe(user => this.user = user);
    this.options.labels = this.labels;
  }

  calculatePercentageDifference(): number {
    return calculatePercentageDifference(this.pastValueTotal, this.total);
  }

  downloadReporteConsolidado() {
    this.loadingService.showLoaderUntilCompleted(
      this.reportDashboardService.getReport(this.tipoReporte, {
        year: this.today.getFullYear(),
        month: this.today.getMonth() + 1, // Get Month return 0 - 11
        idDispensario: this.user.idDispensario,
        pidm:  this.user.pidm
      })
    ).subscribe();
  }

  openFilterMenu(origin: any) {
    this.popoverService.open({
      content: FilterYearMonthComponent,
      origin,
      position: [
        {
          originX: 'start',
          originY: 'bottom',
          overlayX: 'start',
          overlayY: 'top'
        },
        {
          originX: 'end',
          originY: 'bottom',
          overlayX: 'end',
          overlayY: 'top',
        },
      ]
    }).afterClosed$.subscribe((params) => {
      if (params.data) {
        const parameters:DashboardParams = params.data;
        parameters.pidm = this.user.pidm;
        this.loadingService.showLoaderUntilCompleted(
          this.reportDashboardService.getReport(this.tipoReporte, parameters)
        ).subscribe();
      }
    });
  }
}
