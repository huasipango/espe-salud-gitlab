import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetPatientTableComponent } from './widget-patient-table.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';



@NgModule({
  declarations: [
    WidgetPatientTableComponent
  ],
  exports: [
    WidgetPatientTableComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatButtonModule,
    MatIconModule,
    IconModule
  ]
})
export class WidgetPatientTableModule { }
