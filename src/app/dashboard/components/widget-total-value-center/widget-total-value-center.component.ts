import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {Icon} from '@visurel/iconify-angular';
import faCaretUp from '@iconify/icons-fa-solid/caret-up';
import faCaretDown from '@iconify/icons-fa-solid/caret-down';
import icHelp from '@iconify/icons-ic/help-outline';
import icShare from '@iconify/icons-ic/twotone-share';
import {scaleInOutAnimation} from 'src/@vex/animations/scale-in-out.animation';

@Component({
  selector: 'vex-widget-total-value-center',
  templateUrl: './widget-total-value-center.component.html',
  styleUrls: ['./widget-total-value-center.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [scaleInOutAnimation]
})
export class WidgetTotalValueCenterComponent implements OnInit {

  @Input() icon: Icon;
  @Input() value: string;
  @Input() label: string;
  @Input() change: number;
  @Input() helpText: string;
  @Input() iconClass: string;

  faCaretUp = faCaretUp;
  faCaretDown = faCaretDown;
  icHelp = icHelp;
  icShare = icShare;
  showButton: boolean;

  constructor() { }

  ngOnInit(): void {
  }

  openSheet() {
  }
}
