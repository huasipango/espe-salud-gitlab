import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetTotalValueCenterComponent } from './widget-total-value-center.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatButtonModule} from '@angular/material/button';



@NgModule({
  declarations: [
    WidgetTotalValueCenterComponent
  ],
  exports: [
    WidgetTotalValueCenterComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatIconModule,
    IconModule,
    MatTooltipModule,
    MatButtonModule
  ]
})
export class WidgetTotalValueCenterModule { }
