import {Component, Input, OnInit} from '@angular/core';
import {ApexAxisChartSeries, ApexNonAxisChartSeries} from 'ng-apexcharts';
import {ApexOptions} from 'src/@vex/components/chart-ng/chart-ng.component';
import {defaultDashboardChartNgOptions} from 'src/app/core/utils/default-dashboard-chart-ng-options';

@Component({
  selector: 'vex-widget-pie-chart',
  templateUrl: './widget-pie-chart.component.html',
  styleUrls: ['./widget-pie-chart.component.scss']
})
export class WidgetPieChartComponent implements OnInit {
  @Input() labels: string[];
  @Input() series: ApexNonAxisChartSeries | ApexAxisChartSeries;
  @Input() options: ApexOptions = defaultDashboardChartNgOptions({
    chart: {
      type: 'pie',
    },
    responsive: [
      {
        breakpoint: 480,
        options: {
          chart: {
            width: 200
          },
          legend: {
            position: "bottom"
          }
        }
      }
    ]
  });
  constructor() { }

  ngOnInit(): void {
    this.options.labels = this.labels;
  }

}
