import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetPieChartComponent } from './widget-pie-chart.component';
import {ChartNgModule} from 'src/@vex/components/chart-ng/chart-ng.module';
import {FlexLayoutModule} from '@angular/flex-layout';



@NgModule({
  declarations: [
    WidgetPieChartComponent
  ],
  exports: [
    WidgetPieChartComponent
  ],
  imports: [
    CommonModule,
    ChartNgModule,
    FlexLayoutModule
  ]
})
export class WidgetPieChartModule { }
