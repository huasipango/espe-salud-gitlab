import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetDailyLineChartComponent } from './widget-daily-line-chart.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {ChartNgModule} from 'src/@vex/components/chart-ng/chart-ng.module';
import {MatButtonModule} from '@angular/material/button';



@NgModule({
  declarations: [
    WidgetDailyLineChartComponent
  ],
  exports: [
    WidgetDailyLineChartComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatIconModule,
    IconModule,
    ChartNgModule,
    MatButtonModule
  ]
})
export class WidgetDailyLineChartModule { }
