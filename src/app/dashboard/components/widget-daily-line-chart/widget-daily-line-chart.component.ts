import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ApexAxisChartSeries, ApexNonAxisChartSeries} from 'ng-apexcharts';
import {ApexOptions} from 'src/@vex/components/chart-ng/chart-ng.component';
import {defaultDashboardChartNgOptions} from 'src/app/core/utils/default-dashboard-chart-ng-options';
import {Icon} from '@visurel/iconify-angular';
import {ShareBottomSheetComponent} from 'src/@vex/components/share-bottom-sheet/share-bottom-sheet.component';
import icShare from '@iconify/icons-ic/twotone-share';
import {scaleInOutAnimation} from 'src/@vex/animations/scale-in-out.animation';

@Component({
  selector: 'vex-widget-daily-line-chart',
  templateUrl: './widget-daily-line-chart.component.html',
  styleUrls: ['./widget-daily-line-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [scaleInOutAnimation]
})
export class WidgetDailyLineChartComponent implements OnInit {
  colors = [
    '#008FFB',
    '#00E396',
    '#FEB019',
    '#FF4560',
    '#775DD0',
    '#546E7A'
  ];
  @Input() icon: Icon;
  @Input() value: string;
  @Input() label: string;
  @Input() labels: string[];
  @Input() iconClass: string;
  @Input() options: ApexOptions = defaultDashboardChartNgOptions({
    colors: this.colors,
    grid: {
      show: false,
      padding: {
        top: 0,
        left: 0,
        right: 0
      }
    },
    chart: {
      parentHeightOffset: 0,
      type: 'area',
      height: 100,
      toolbar: {
        show: false,
      },
      sparkline: {
        enabled: true
      }
    },
    labels: [],
    xaxis: {
      type: 'datetime',
    },
    dataLabels: {
      enabled: false,
    },
    tooltip: {
      x: { show: true }
    },
    stroke: {
      curve: 'smooth',
      width: 2.5
    },
  });
  @Input() series: ApexNonAxisChartSeries | ApexAxisChartSeries;
  showButton: boolean;
  icShare = icShare;

  constructor() { }

  ngOnInit(): void {
    this.options.labels = this.labels;
  }

  openSheet() {
  }
}
