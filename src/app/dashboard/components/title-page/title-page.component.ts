import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'vex-title-page',
  templateUrl: './title-page.component.html',
  styleUrls: ['./title-page.component.scss']
})
export class TitlePageComponent implements OnInit {
  @Input() title: string;
  @Input() explanation: string;
  constructor() { }

  ngOnInit(): void {
  }

}
