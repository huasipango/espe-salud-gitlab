import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TitlePageComponent } from './title-page.component';



@NgModule({
  declarations: [TitlePageComponent],
  exports: [
    TitlePageComponent
  ],
  imports: [
    CommonModule
  ]
})
export class TitlePageModule { }
