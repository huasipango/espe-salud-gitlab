import {Component, Inject, Input, OnInit} from '@angular/core';
import {ApexAxisChartSeries, ApexNonAxisChartSeries} from 'ng-apexcharts';
import {ApexOptions} from 'src/@vex/components/chart-ng/chart-ng.component';
import {defaultDashboardChartNgOptions} from 'src/app/core/utils/default-dashboard-chart-ng-options';
import {MONTHS_OF_YEAR_ABR} from 'src/app/core/constants/constants';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Reporte} from 'src/app/dashboard/dashboard-general/pages/graficos-estadisticos-general/graficos-estadisticos-general.component';
import {LoadingService} from 'src/app/core/services/loading.service';
import {TotalDashboardService} from 'src/app/core/services/total-dashboard.service';
import {ReportDashboardService} from 'src/app/core/services/report-dashboard.service';
import icClose from '@iconify/icons-ic/twotone-close';
import icCloudDownload from '@iconify/icons-ic/twotone-cloud-download';
import {Dispensario} from 'src/app/core/models/catalogo/dispensario.model';
import {DashboardParams} from 'src/app/core/models/dashboard/dashboard-params.model';
@Component({
  selector: 'vex-general-month-chart-modal',
  templateUrl: './general-month-chart-modal.component.html',
  styleUrls: ['./general-month-chart-modal.component.scss']
})
export class GeneralMonthChartModalComponent implements OnInit {

  year: number;
  dispensario: Dispensario;
  idDispensario: number;

  icClose = icClose;
  icCloudDownload = icCloudDownload;
  colors = [
    '#008FFB',
    '#00E396',
    '#FEB019',
    '#FF4560',
    '#775DD0',
    '#546E7A'
  ];
  series: ApexNonAxisChartSeries | ApexAxisChartSeries = [];
  @Input() options: ApexOptions = defaultDashboardChartNgOptions({
    colors: this.colors,
    xaxis: {
      categories: MONTHS_OF_YEAR_ABR,
    },
    chart: {
      type: this.data.chart
    },
    fill: this.data.chart === 'area' ? {
      type: 'gradient',
      gradient: {
        shadeIntensity: 0.9,
        opacityFrom: 0.7,
        opacityTo: 0.5,
        stops: [0, 90, 100]
      }
    } : {},
    legend: {
      position: 'right',
      offsetY: 40
    },
  });
  dataFetched = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Reporte,
    private dialogRef: MatDialogRef<GeneralMonthChartModalComponent>,
    private loadingService: LoadingService,
    private totalDashboardService: TotalDashboardService,
    private reportDashboardService: ReportDashboardService
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    this.loadingService.showLoaderUntilCompleted(
      this.totalDashboardService.getDashboardData(this.data.slug, this.createParams())
    ).subscribe((response) => {
      this.series = response;
      this.dataFetched = true;
    });
  }

  searchByYear(year: string): void {
    this.year = year !== '' ? Number(year) : null;
    this.getData();
  }

  createParams(): DashboardParams {
    return {
      year: this.year,
      idDispensario: this.idDispensario
    };
  }

  generateReport() {
    this.loadingService.showLoaderUntilCompleted(
      this.reportDashboardService.getReport(this.data.slug, this.createParams())
    ).subscribe();
  }

  searchByIdDispensario(dispensario: Dispensario): void {
    this.idDispensario = dispensario !== null ? dispensario.id : null;
    this.dispensario = dispensario;
    this.getData();
  }
}
