import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneralMonthChartModalComponent } from './general-month-chart-modal.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {ChartNgModule} from 'src/@vex/components/chart-ng/chart-ng.module';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import {MatTooltipModule} from '@angular/material/tooltip';
import {PickYearModule} from 'src/app/shared/components/pick-year/pick-year.module';
import {PickFiltroModule} from 'src/app/shared/components/pick-filtro/pick-filtro.module';

@NgModule({
  declarations: [GeneralMonthChartModalComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    MatDividerModule,
    FlexLayoutModule,
    MatIconModule,
    IconModule,
    ChartNgModule,
    LoadingModule,
    MatTooltipModule,
    PickYearModule,
    PickFiltroModule
  ],
  exports: [GeneralMonthChartModalComponent]
})
export class GeneralMonthChartModalModule { }
