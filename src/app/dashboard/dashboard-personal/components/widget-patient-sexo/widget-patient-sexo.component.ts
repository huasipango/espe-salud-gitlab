import { Component, OnInit } from '@angular/core';
import {SaludUser} from 'src/@vex/layout/toolbar/toolbar-user/interfaces/salud-user.interface';
import {ApexAxisChartSeries, ApexNonAxisChartSeries} from 'ng-apexcharts';
import {ApexOptions} from 'src/@vex/components/chart-ng/chart-ng.component';
import {defaultDashboardChartNgOptions} from 'src/app/core/utils/default-dashboard-chart-ng-options';
import {MONTHS_OF_YEAR_ABR} from 'src/app/core/constants/constants';
import {TotalDashboardService} from 'src/app/core/services/total-dashboard.service';
import {ReportDashboardService} from 'src/app/core/services/report-dashboard.service';
import {LoadingService} from 'src/app/core/services/loading.service';
import {AuthService} from 'src/app/core/auth/auth.service';
import icCloudDownload from '@iconify/icons-ic/twotone-cloud-download';
import {DashboardParams} from 'src/app/core/models/dashboard/dashboard-params.model';

@Component({
  selector: 'vex-widget-patient-sexo',
  templateUrl: './widget-patient-sexo.component.html',
  styleUrls: ['./widget-patient-sexo.component.scss']
})
export class WidgetPatientSexoComponent implements OnInit {

  year: number;
  title = 'Pacientes atendidos en Espe según el sexo por';
  dataFetched = false;
  saludUser: SaludUser;
  icCloudDownload = icCloudDownload;
  series: ApexNonAxisChartSeries | ApexAxisChartSeries = [];
  options: ApexOptions = defaultDashboardChartNgOptions({
    chart: {
      type: 'bar',
      stacked: false,
    },
    xaxis: {
      categories: MONTHS_OF_YEAR_ABR,
    }
  });
  constructor(
    private totalDashboardService: TotalDashboardService,
    private reportDashboardService: ReportDashboardService,
    private loadingService: LoadingService,
    private authService: AuthService
  ) {
  }

  ngOnInit(): void {
    this.authService.saludUserData$
      .subscribe((saludUser) => {
        if (saludUser) {
          this.saludUser = saludUser;
          this.getData();
        }
      });
  }

  getData(): void {
    this.totalDashboardService.getTotalAtencionesPorSexo(this.createParams())
      .subscribe((data) => {
        this.series = data;
        this.dataFetched = true;
      });
  }

  searchByYear(year: string): void {
    this.year = year !== '' ? Number(year) : null;
    this.getData();
  }

  createParams(): DashboardParams {
    return {
      year: this.year,
      pidm: this.saludUser.pidm
    };
  }

  generateReport() {
    this.loadingService.showLoaderUntilCompleted(
      this.reportDashboardService.getReportePorSexo(this.createParams())
    ).subscribe();
  }


}
