import { Component, OnInit } from '@angular/core';
import {CatalogoService} from 'src/app/core/services/catalogo.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {PopoverRef} from 'src/@vex/components/popover/popover-ref';
import {MONTHS, YEARS} from 'src/app/core/constants/constants';
import icExcel from '@iconify/icons-fa-solid/file-excel';

@Component({
  selector: 'vex-filter-year-month',
  templateUrl: './filter-year-month.component.html',
  styleUrls: ['./filter-year-month.component.scss']
})
export class FilterYearMonthComponent implements OnInit {
  selected = 'all';
  form: FormGroup;
  years = YEARS();
  months = MONTHS;
  icExcel = icExcel;

  constructor(
    private catalogoService: CatalogoService,
    private fb: FormBuilder,
    private popoverRef: PopoverRef<FilterYearMonthComponent>
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      year: [new Date().getFullYear()],
      month: [new Date().getMonth() + 1]
    });
  }

  search() {
    const params = this.form.value;
    params.year = params.year === -1 ? null : params.year;
    params.month = params.month === -1 ? null : params.month;
    this.popoverRef.close(params);
  }
}
