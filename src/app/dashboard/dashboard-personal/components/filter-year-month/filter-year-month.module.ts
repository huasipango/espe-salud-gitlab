import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterYearMonthComponent } from './filter-year-month.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {IconModule} from '@visurel/iconify-angular';



@NgModule({
  declarations: [
    FilterYearMonthComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    IconModule
  ]
})
export class FilterYearMonthModule { }
