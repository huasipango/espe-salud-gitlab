import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DashboardPersonalComponent} from 'src/app/dashboard/dashboard-personal/dashboard-personal.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardPersonalComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardPersonalRoutingModule {
}
