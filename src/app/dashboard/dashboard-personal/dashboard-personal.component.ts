import { Component, OnInit } from '@angular/core';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icGroup from '@iconify/icons-ic/twotone-group';

import {TotalDashboardService} from 'src/app/core/services/total-dashboard.service';
import {AuthService} from 'src/app/core/auth/auth.service';
import {EMPTY} from 'rxjs';
import {PersonalDashboard} from 'src/app/core/models/dashboard/personal-dashboard.model';
import {switchMap} from 'rxjs/operators';
import icPaciente from '@iconify/icons-fa-solid/user-injured';
import {TableColumn} from 'src/@vex/interfaces/table-column.interface';
import {PacienteSimple} from 'src/app/core/models/paciente/paciente-simple.model';

@Component({
  selector: 'vex-dashboard-personal',
  templateUrl: './dashboard-personal.component.html',
  styleUrls: ['./dashboard-personal.component.scss']
})
export class DashboardPersonalComponent implements OnInit {
  icMoreVert = icMoreVert;
  icGroup = icGroup;
  icPaciente = icPaciente;
  personalDashboard: PersonalDashboard;
  patientsRegisteredAtCurrentMonthSeries: ApexAxisChartSeries = [{
    name: 'Pacientes registrados',
    data: []
  }];
  attentionsAtCurrentMonthSeries: ApexAxisChartSeries = [{
    name: 'Atenciones',
    data: []
  }];
  patientTableColumns: TableColumn<PacienteSimple>[] = [
    {
      label: 'NÚMERO ARCHIVO',
      property: 'numeroArchivo',
      type: 'text',
      cssClasses: ['font-medium']
    },
    {
      label: 'NOMBRES',
      property: 'nombres',
      type: 'text'
    },
    {
      label: 'APELLIDOS',
      property: 'apellidos',
      type: 'text'
    },
    {
      label: 'DISPENSARIO',
      property: 'dispensario',
      type: 'text',
      cssClasses: ['text-secondary']
    }
  ];
  constructor(
    private totalDashboardService: TotalDashboardService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.authService.saludUserData$.pipe(
      switchMap(user => {
        if (user) {
          return this.totalDashboardService.getPersonalUserDashboard(user.pidm);
        }
        return EMPTY;
      })
    ).subscribe((dashboard) => {
      if (dashboard) {
        this.patientsRegisteredAtCurrentMonthSeries[0].data = dashboard.patientsRegisteredDailyAtCurrentMonth.series;
        this.attentionsAtCurrentMonthSeries[0].data = dashboard.attentionsDailyAtCurrentMonth.series;
        this.personalDashboard = dashboard;
      }
    })
  }
}
