import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardPersonalComponent } from './dashboard-personal.component';
import {DashboardPersonalRoutingModule} from 'src/app/dashboard/dashboard-personal/dashboard-personal-routing.module';
import { WidgetPatientMorbilidadComponent } from './components/widget-patient-morbilidad/widget-patient-morbilidad.component';
import {MatIconModule} from '@angular/material/icon';
import {IconModule} from '@visurel/iconify-angular';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ContainerModule} from 'src/@vex/directives/container/container.module';
import {BreadcrumbsModule} from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import {SecondaryToolbarModule} from 'src/@vex/components/secondary-toolbar/secondary-toolbar.module';
import {PickYearModule} from 'src/app/shared/components/pick-year/pick-year.module';
import {MatTooltipModule} from '@angular/material/tooltip';
import {ChartNgModule} from 'src/@vex/components/chart-ng/chart-ng.module';
import {LoadingModule} from 'src/app/shared/components/loading/loading.module';
import {MatButtonModule} from '@angular/material/button';
import {NgApexchartsModule} from 'ng-apexcharts';
import { WidgetPatientMotivoAtencionComponent } from './components/widget-patient-motivo-atencion/widget-patient-motivo-atencion.component';
import { WidgetPatientPrevencionComponent } from './components/widget-patient-prevencion/widget-patient-prevencion.component';
// eslint-disable-next-line max-len
import { WidgetPatientPrevencionMorbilidadComponent } from './components/widget-patient-prevencion-morbilidad/widget-patient-prevencion-morbilidad.component';
import { WidgetPatientRelacionEspeComponent } from './components/widget-patient-relacion-espe/widget-patient-relacion-espe.component';
import { WidgetPatientSeguroSaludComponent } from './components/widget-patient-seguro-salud/widget-patient-seguro-salud.component';
import { WidgetPatientSexoComponent } from './components/widget-patient-sexo/widget-patient-sexo.component';
import { WidgetPatientTipoConsultaComponent } from './components/widget-patient-tipo-consulta/widget-patient-tipo-consulta.component';
import { WidgetTablePatientCieComponent } from './components/widget-table-patient-cie/widget-table-patient-cie.component';
import {WidgetTableModule} from 'src/@vex/components/widgets/widget-table/widget-table.module';
import {WidgetDailyLineChartModule} from 'src/app/dashboard/components/widget-daily-line-chart/widget-daily-line-chart.module';
import {WidgetTotalValueCenterModule} from 'src/app/dashboard/components/widget-total-value-center/widget-total-value-center.module';
import {WidgetDailyLargeChartModule} from 'src/app/dashboard/components/widget-daily-large-chart/widget-daily-large-chart.module';
import {WidgetPatientTableModule} from 'src/app/dashboard/components/widget-patient-table/widget-patient-table.module';
import {WidgetPieChartModule} from 'src/app/dashboard/components/widget-pie-chart/widget-pie-chart.module';



@NgModule({
  declarations: [
    DashboardPersonalComponent,
    WidgetPatientMorbilidadComponent,
    WidgetPatientMotivoAtencionComponent,
    WidgetPatientPrevencionComponent,
    WidgetPatientPrevencionMorbilidadComponent,
    WidgetPatientRelacionEspeComponent,
    WidgetPatientSeguroSaludComponent,
    WidgetPatientSexoComponent,
    WidgetPatientTipoConsultaComponent,
    WidgetTablePatientCieComponent],
  imports: [
    CommonModule,
    DashboardPersonalRoutingModule,
    MatIconModule,
    IconModule,
    FlexLayoutModule,
    ContainerModule,
    BreadcrumbsModule,
    SecondaryToolbarModule,
    PickYearModule,
    MatTooltipModule,
    ChartNgModule,
    LoadingModule,
    MatButtonModule,
    NgApexchartsModule,
    WidgetTableModule,
    WidgetDailyLineChartModule,
    WidgetTotalValueCenterModule,
    WidgetDailyLargeChartModule,
    WidgetPatientTableModule,
    WidgetPieChartModule
  ]
})
export class DashboardPersonalModule { }
