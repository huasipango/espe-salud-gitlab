import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CustomLayoutComponent} from './custom-layout/custom-layout.component';
import {VexRoutes} from 'src/@vex/interfaces/vex-route.interface';
import {QuicklinkStrategy} from 'ngx-quicklink';
import {AuthGuard} from 'src/app/core/auth/auth.guard';

const routes: VexRoutes = [
  {
    path: 'login',
    loadChildren: () => import('./core/auth/login/login.module').then(m => m.LoginModule),
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./core/auth/forgot-password/forgot-password.module').then(m => m.ForgotPasswordModule),
  },
  {
    path: '',
    component: CustomLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        loadChildren: () => import('./modules/home/home.module')
          .then(m => m.HomeModule)
      },
      {
        path: 'dashboard',
        children: [
          {
            path: 'general',
            loadChildren: () => import('./dashboard/dashboard-general/dashboard-general.module')
              .then(m => m.DashboardGeneralModule),
          },
          {
            path: 'personal',
            loadChildren: () => import('./dashboard/dashboard-personal/dashboard-personal.module')
              .then(m => m.DashboardPersonalModule),
          }
        ]
      },
      {
        path: 'enfermeria',
        children: [
          {
            path: 'notas-enfermeria',
            loadChildren: () => import('./modules/enfermeria/notas-enfermeria/notas-enfermeria.module')
              .then(m => m.NotasEnfermeriaModule)
          },
          {
            path: 'gestacion-lactancia',
            loadChildren: () => import('./modules/enfermeria/gestacion-lactancia/gestacion-lactancia.module')
              .then(m => m.GestacionLactanciaModule)
          },
          {
            path: 'actividades-enfermeria',
            loadChildren: () => import('./modules/enfermeria/actividad-enfermeria/actividad-enfermeria.module')
              .then(m => m.ActividadEnfermeriaModule)
          }
        ]
      },
      {
        path: 'medicina-general',
        children: [
          {
            path: 'estudio-complementario',
            loadChildren: () => import('./modules/medicina-general/estudio-complementario/estudio-complementario.module')
              .then(m => m.EstudioComplementarioModule)
          },
          {
            path: 'pedidos-examen',
            loadChildren: () => import('./modules/medicina-general/examen-laboratorio/examen-laboratorio.module')
              .then(m => m.ExamenLaboratorioModule)
          },
          {
            path: 'antecedente-laboratorio',
            loadChildren: () => import('./modules/medicina-general/antecedente-laboratorio/antecedente-laboratorio.module')
              .then(m => m.AntecedenteLaboratorioModule)
          },
          {
            path: 'hospitalizacion',
            loadChildren: () => import('./modules/medicina-general/hospitalizacion/hospitalizacion.module')
              .then(m => m.HospitalizacionModule)
          },
          {
            path: 'revision-organos-sistemas',
            loadChildren: () => import('./modules/medicina-general/organos-sistemas/organos-sistemas.module')
              .then(m => m.OrganosSistemasModule)
          },
          {
            path: 'examenes-fisico-regionales',
            loadChildren: () => import('./modules/medicina-general/examen-fisico-regional/examen-fisico-regional.module')
              .then(m => m.ExamenFisicoRegionalModule)
          },
          {
            path: 'evolucion',
            loadChildren: () => import('./modules/medicina-general/evolucion/evolucion.module')
              .then(m => m.EvolucionModule)
          },
          {
            path: 'perfil-paciente',
            loadChildren: () => import('./modules/medicina-general/perfil-paciente/perfil-paciente.module')
              .then(m => m.PerfilPacienteModule)
          },
          {
            path: 'registro-paciente',
            loadChildren: () => import('./modules/medicina-general/registro-paciente/registro-paciente.module')
              .then(m => m.RegistroPacienteModule)
          },
          {
            path: 'antecedentes-personales',
            loadChildren: () => import('./modules/medicina-general/antecedente-clinico/antecedente-clinico.module')
              .then(m => m.AntecedenteClinicoModule)
          },
          {
            path: 'certificados',
            loadChildren: () => import('./modules/medicina-general/certificado/certificado.module')
              .then(m => m.CertificadoModule)
          },
          {
            path: 'vacunacion',
            loadChildren: () => import('src/app/modules/medicina-general/vacunacion/vacuna.module')
              .then(m => m.VacunaModule)
          }
        ]
      },
      {
        path: 'salud-ocupacional',
        children: [
          {
            path: 'antecedente-laboral',
            loadChildren: () => import('./modules/salud-ocupacional/antecedente-laboral/antecedente-laboral.module')
              .then(m => m.AntecedenteLaboralModule)
          }
        ]
      },
      // {
      //   path: 'odontologia',
      //   children: [
      //     {
      //       path: 'historia-clinica',
      //       loadChildren: () =>
      //         import('./modules/medicina-general/odontologia/historiaclinica/historia-clinica.module')
      //           .then(m => m.HistoriaClinicaModule)
      //     },
      //     {
      //       path: 'odontograma',
      //       loadChildren: () =>
      //         import('./modules/medicina-general/odontologia/odontograma/odontograma.module')
      //           .then(m => m.OdontogramaModule)
      //     }
      //   ]
      // },
      {
        path: 'empty-paciente',
        loadChildren: () =>
          import('src/app/core/components/empty-paciente/empty-paciente.module')
            .then(m => m.EmptyPacienteModule)
      },
      {
        path: 'perfil-usuario',
        loadChildren: () => import('src/app/core/auth/perfil-usuario/perfil-usuario.module')
          .then(m => m.PerfilUsuarioModule)
      },
      {
        path: 'administracion',
        children: [
          {
            path: 'usuarios',
            loadChildren: () => import('src/app/modules/administracion/usuario/usuario.module')
              .then(m => m.UsuarioModule)
          }
        ]
      },
      {
        path: 'catalogos',
        loadChildren: () => import('src/app/modules/administracion/catalogo/catalogo.module').then(m => m.CatalogoModule),
      },
      {
        path: 'docs',
        loadChildren: () => import('./documentation/documentation.module').then(m => m.DocumentationModule),
      },
      {
        path: 'error-403',
        loadChildren: () => import('./core/errors/error-403/error-403.module').then(m => m.Error403Module)
      },
      {
        path: '**',
        loadChildren: () => import('./core/errors/error-404/error-404.module').then(m => m.Error404Module)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled',
    relativeLinkResolution: 'corrected',
    anchorScrolling: 'enabled',
    paramsInheritanceStrategy: 'always',
    preloadingStrategy: QuicklinkStrategy
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
