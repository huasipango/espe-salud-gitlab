export const environment = {
  production: true,
  baseUrl: 'https://salud-api-test.espe.edu.ec/api/v1.0/',
  catalogoWebServiceUrl : 'https://servicios.espe.edu.ec:8443/salud-jdbc-1.0.0/',
  bannerWebServiceUrl: 'https://servicios.espe.edu.ec:8443/Data-0.0.1-SNAPSHOT/DataEspe/persona/',
  bannerWebServiceToken: 'acd1ecc87c441428c2192416864fb7b7',
  imagesWebService: 'https://servicios.espe.edu.ec:8443/userimages-0.0.1-SNAPSHOT/imagen/',
  sso: {
    serverUrl: 'https://srvcas.espe.edu.ec',
    clientId: 'L9G1CRO6Itpk_uHGXwWbvgagft4a',
    requireHttps: true,
  }
};
