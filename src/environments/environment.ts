// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl: 'http://localhost:8080/api/v1.0/',
  catalogoWebServiceUrl : 'https://servicios.espe.edu.ec:8443/salud-jdbc-1.0.0/',
  bannerWebServiceUrl: 'https://servicios.espe.edu.ec:8443/Data-0.0.1-SNAPSHOT/DataEspe/persona/',
  bannerWebServiceToken: 'acd1ecc87c441428c2192416864fb7b7',
  imagesWebService: 'https://servicios.espe.edu.ec:8443/userimages-0.0.1-SNAPSHOT/imagen/',

  sso: {
    serverUrl: 'https://srvcas.espe.edu.ec',
    clientId: 'F_13VTNPdVHPSstUZtYmldfl2UYa',
    requireHttps: false,
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
// regexp=(http://localhost:8080/playground2/oauth2client|http://localhost:8080/playground2/logout)
