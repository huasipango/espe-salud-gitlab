<img style="float: left; height: 40px; width: unset;" src="/assets/img/espe/ESPESALUD.png" alt="Espe Logo"/>
<br>

# Registro de cambios


> <span style="font-weight: 600;">1.0.6 (10/08/2021)</span>

### Caracteristicas
- Se agrego la opción para descargar el reporte consolidado personal.

### Correciones
- En los estadísticas personales en la sección de evoluciones se corrigió las etiquetas en el gráfico.

> <span style="font-weight: 600;">1.0.5 (03/08/2021)</span>

### Caracteristicas
- El campo correo personal en el registro de paciente externo ya no es requerido.
- El campo contactos de emergencia en el registro de pacientes externos ya no es requerido.
- La nota de evolución se guarda automaticamente luego del paso 2, asi como los diagnósticos y prescripciones.
- Si no se ha finalizado la nota de evolución esta presentará el estado de "BORRADOR". Para finalizar la atención el usuario deberá completar los datos y luego dar click en botón "Finalizar atención".

### Correciones
- Se ha corregido los botones de accesso rápido ubicados en la parte superior de la pantalla. Ahora ya presentan funcionalidad.

> <span style="font-weight: 600;">1.0.4 (22/07/2021)</span>

### Caracteristicas
- Monstrar las alergias en el panel derecho.
- Agregar consentimiento informado en el registro.
- Nuevo formato en la receta médica.
- En datos del paciente se muestra el sexo por el nombre completo en lugar de las iniciales.
- Se permite la modificación de una nota de evolución solo hasta el final de día `23:59:59 hrs`
- Se permite la modificación de un registro de signos vitales. Al modificar el registro se crea uno nuevo en lugar de modificar el anterior.
- Se agrega la opción para eliminar una nota de enfermeria en la creación de la nota de evolución. (Nota: Solo de la lista que se visualiza mas no en la Base de Datos)
- Reporte consolidado con formato del RADACCA completado.
- Dashboard personal con nuevos gráficos.
- Se agrega el campo fecha de control y observacion en el registro de gestacion lactancia.
- Se agrega la funcionalidad para subir una imagen de la firma del usuario.
- Se agrega la firma del usuario a la receta médica, previo a la funcionalidad de envio por correo.

### Correciones
- Error en la validación para editar los usuarios.
- Error en la modificación de notas de evolución que eliminaba la relación con prescripciones y diagnosticos.
- Validación de frecuencia respiratoria entre 1 y 60
- Error al momento de seleccionar la nota de enfermeria en evolución.
- Error al generar el reporte consolidado.

> <span style="font-weight: 600;">1.0.3 (30/06/2021)</span>

### Características
- Vista de antecedentes de laboratorio.
- Nuevo menú con el tiempo restante de la sesión.
- Al pasar 1 hora el usuario debera iniciar sesión nuevamente.

> <span style="font-weight: 600;">1.0.2 (28/06/2021)</span>

### Características
- Agregar panel de estadísticas con 13 reportes y gráficos.
- Reporte consolidado en Excel.
- Nuevo editor WYSIWYG para la nota de evolución.
- Agregar solicitud de examenes a la nota de evolución.
- Mejorar la vista del panel de administración de usuarios.

### Correcciones
- Agregar validación (Por lo menos un diagnóstico en la nota de evolución)

> <span style="font-weight: 600;">1.0.2 (21/06/2021)</span>

### Características
- Version inicial
